//
//  BridgeManager.h
//  girnarsoft_classified_react_native
//
//  Created by Akhilendra Chauhan on 09/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
NS_ASSUME_NONNULL_BEGIN

@interface BridgeManager : NSObject <RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
