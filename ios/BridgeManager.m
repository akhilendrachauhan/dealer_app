//
//  BridgeManager.m
//  girnarsoft_classified_react_native
//
//  Created by Akhilendra Chauhan on 09/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "BridgeManager.h"
#import <React/RCTLog.h>
@implementation BridgeManager
RCT_EXPORT_MODULE(BridgeManager);
RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
{
  RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
}
@end
