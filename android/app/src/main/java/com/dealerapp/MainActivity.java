package com.dealerapp;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import com.facebook.react.bridge.ReactContext;
import android.content.Intent;
import android.net.Uri;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "DealerApp";
  }
  @Override
 protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
       return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
    };
  }

  @Override
  public void onNewIntent(Intent intent) {
      super.onNewIntent(intent);
      if(intent.getData() != null) {
          intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
          Uri deepLinkURL = intent.getData();
          ReactContext reactContext = getReactNativeHost().getReactInstanceManager().getCurrentReactContext();
          if(reactContext != null)
              sendEvent(reactContext,"deepLinking", deepLinkURL.toString());
      }
      setIntent(intent);
  }
  private void sendEvent(ReactContext reactContext,
                          String eventName,
                          String str) {
      reactContext
              .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
              .emit(eventName, str);
  }
}
