import Toast from 'react-native-simple-toast'

export function showToast(message) {
    Toast.show(message);
}