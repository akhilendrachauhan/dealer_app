import * as Strings from '../values/strings'
import * as Colors from '../values/colors'

export const naviLeadStatus = "naviLeadStatus"

//Navigation params
export const NavigationData = 'Navigation_data'
export const dateFormatUI = 'd mmm, yyyy'
export const timeFormatUI = 'h:MM TT'
export const workshop = 'workshop'
export const dealer_location = 'location'
export const showroom = 'showroom'
export const customer_location = 'customer_location'
export const dealer_showroom = 'dealer_showroom'
export const dealer_workshop = 'dealer_workshop'

export const ICON_MAP = {
    one: require('../../lms/assets/drawable/one.png'),
    two: require('../../lms/assets/drawable/two.png'),
    three: require('../../lms/assets/drawable/three.png'),
    four: require('../../lms/assets/drawable/four.png'),
    five: require('../../lms/assets/drawable/five.png'),
    six: require('../../lms/assets/drawable/six.png'),
    seven: require('../../lms/assets/drawable/seven.png'),
    eight: require('../../lms/assets/drawable/eight.png'),
    nine: require('../../lms/assets/drawable/nine.png'),
    ten: require('../../lms/assets/drawable/ten.png'),
    eleven: require('../../lms/assets/drawable/eleven.png'),
    twelve: require('../../lms/assets/drawable/twelve.png'),
    thirteen: require('../../lms/assets/drawable/thirteen.png'),
    fourteen: require('../../lms/assets/drawable/fourteen.png'),
    fivteen: require('../../lms/assets/drawable/fifteen.png'),
    sixteen: require('../../lms/assets/drawable/sixteen.png'),
    seventen: require('../../lms/assets/drawable/seventeen.png'),
    eighteen: require('../../lms/assets/drawable/eighteen.png'),
    ninteen: require('../../lms/assets/drawable/ninteen.png'),
    twenty: require('../../lms/assets/drawable/one.png'),
    twentyone: require('../../lms/assets/drawable/twentyone.png'),
    twentytwo: require('../../lms/assets/drawable/twentytwo.png'),
    twentythree: require('../../lms/assets/drawable/twentythree.png'),
    twentyfour: require('../../lms/assets/drawable/twentyfour.png'),
    twentyfive: require('../../lms/assets/drawable/twentyfive.png'),
    twentysix: require('../../lms/assets/drawable/twentysix.png'),
    twentyseven: require('../../lms/assets/drawable/twentyseven.png')
};

export const FOLLOW_UP_TYPE = {
    leadRating: 'Lead_Rating',
    customerOffer: 'Customer_Offer',
    normal: 'Normal'
};

