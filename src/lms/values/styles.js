import * as Colors from "./colors";
import {StyleSheet} from 'react-native';
import * as Dimes from '../values/Dimens';
import * as Strings from "./strings";

export const bottom = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        color: Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: 15,
        marginTop: -20,
    },

    sub_title: {
        fontSize: 14,
        color: Colors.black,
        marginBottom: 10,
        fontFamily:Strings.APP_FONT,
        marginTop: 4,
    },
    buttonStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.buttonColor,
        height: Dimes.btnHeight
    },
    bottomBtntext: {
        fontSize: 16,
        fontFamily:Strings.APP_FONT,
        fontWeight: 'bold'
    },
})
