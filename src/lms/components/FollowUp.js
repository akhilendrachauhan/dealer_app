import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TouchableOpacity, TextInput, Platform} from 'react-native'
import * as Colors from "../values/colors";
import * as Strings from '../values/strings'
import TextInputLayout from "../granulars/TextInputLayout";
import DateTimePicker from 'react-native-modal-datetime-picker'
import * as Constants from '../utils/Constants'
import * as Styles from '../values/styles'
import * as Dimens from '../values/Dimens'
import * as Images from '../values/Images'
import * as Tasks from '../utils/Tasks'
import * as  Utility from '../utils/Utility'

let dateFormat = require('../utils/DateFormat');

class FollowUp extends Component {
    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            date: '',
            time: '',
            dateValue: '',
            timeValue: '',
            comment: '',
            isDateTimePickerVisible: false,
            isTimePickerVisible: false,
            showRating: props.data && props.data.followUpType && (props.data.followUpType == Constants.FOLLOW_UP_TYPE.leadRating),
            showOffer: props.data && props.data.followUpType && (props.data.followUpType == Constants.FOLLOW_UP_TYPE.customerOffer),
            offer: '',
            rating: '',
            ratingData: [
                {
                    id: '1',
                    value: Strings.hot,
                    color: Colors.hotColor
                },
                {
                    id: '2',
                    value: Strings.warm,
                    color: Colors.warmColor
                },
                {
                    id: '3',
                    value: Strings.cold,
                    color: Colors.coldColor
                }
            ],
            selectedTimeIndex: 0
        };
    }

    twoHoursClicked = () => {
        let currentDate = new Date();
        let date = currentDate.setHours(currentDate.getHours() + 2)
        this.setState({
            date: dateFormat(date, Constants.dateFormatUI),
            time: dateFormat(date, Constants.timeFormatUI),
            dateValue: dateFormat(date, 'yyyy-mm-dd'),
            timeValue: dateFormat(date, 'HH:MM:ss'),
            selectedTimeIndex: 1
        })
    }
    tomorrowClicked = () => {
        let currentDate = new Date();
        let date = currentDate.setDate(currentDate.getDate() + 1)
        this.setState({
            date: dateFormat(date, Constants.dateFormatUI),
            time: dateFormat(date, Constants.timeFormatUI),
            dateValue: dateFormat(date, 'yyyy-mm-dd'),
            timeValue: dateFormat(date, 'HH:MM:ss'),
            selectedTimeIndex: 2
        })
    }
    afterTwoDays = () => {
        let currentDate = new Date();
        let date = currentDate.setDate(currentDate.getDate() + 2)
        this.setState({
            date: dateFormat(date, Constants.dateFormatUI),
            time: dateFormat(date, Constants.timeFormatUI),
            dateValue: dateFormat(date, 'yyyy-mm-dd'),
            timeValue: dateFormat(date, 'HH:MM:ss'),
            selectedTimeIndex: 3
        })
    }

    onRatingPress = (item) => {
        let ratingData = this.state.ratingData;
        ratingData.forEach(row => {
            row['selected'] = item.id === row.id;
        });
        this.setState({ratingData: ratingData, rating: item.id})
    };

    renderRating = () => {
        return (
            <View>
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    marginTop: 15,
                    fontFamily: Strings.APP_FONT,
                }}>{Strings.select_lead_rating}</Text>
                <View style={{flexDirection: 'row', marginTop: 15}}>
                    {this.state.ratingData.map((item, index) => {
                        return (
                            <TouchableOpacity onPress={() => this.onRatingPress(item)} style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: 40,
                                borderRadius: 20,
                                borderWidth: 2,
                                borderColor: item.color,
                                backgroundColor: item.selected ? item.color : Colors.WHITE,
                                marginRight: 10
                            }}>
                                <Text style={{paddingHorizontal: 22}}>{item.value}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>)

    }

    render() {
        return (
            <View style={{width: '100%'}}>
                <View style={styles.container}>
                    <Text style={styles.title}> {Strings.schedule_FollowUp} </Text>
                    <View style={{
                        flexDirection: 'row',
                        paddingTop: Dimens.dimen_10,
                        paddingBottom: Dimens.dimen_10,
                        justifyContent: 'space-between'
                    }}>
                        <TouchableOpacity style={styles.btnBackground}
                                          onPress={() => this.twoHoursClicked()}><Text>{Strings.after_two_hr}</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.btnBackground}
                                          onPress={() => this.tomorrowClicked()}><Text>{Strings.tomarrow}</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.btnBackground}
                                          onPress={() => this.afterTwoDays()}><Text>{Strings.after_2_days}</Text></TouchableOpacity>
                    </View>

                    <View style={{paddingTop: Dimens.dimen_10, paddingBottom: Dimens.dimen_10, flexDirection: 'row'}}>

                        <View style={{flex: 1, marginEnd: Dimens.dimen_10}}>
                            <TouchableOpacity onPress={() => this.dateClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.dateRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                               onTouchStart={() => Platform.OS === 'ios' ? this.dateClicked() : null}
                                               value={this.state.date}
                                               ref={(input) => this.dateRef = input}
                                               placeholder={Strings.date}
                                               editable={false}
                                        // onChangeText={(text) => this.setState({date: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>

                        </View>
                        <View style={{flex: 1, marginStart: Dimens.dimen_10}}>
                            <TouchableOpacity onPress={() => this.timeClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.timeRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.clockIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                               editable={false}
                                               onTouchStart={() => Platform.OS === 'ios' ? this.timeClicked() : null}
                                               value={this.state.time}
                                               ref={(input) => this.timeRef = input}
                                               placeholder={Strings.time}
                                               onChangeText={(text) => this.setState({offer: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TextInputLayout
                        style={{marginTop: Dimens.dimen_10}}
                        ref={(input) => this.commentRefLayout = input}
                        hintColor={Colors.black_54}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                                   value={this.state.comment}
                                   ref={(input) => this.commentRef = input}
                                   placeholder={Strings.add_comment}
                                   onChangeText={(text) => {
                                       this.setState({comment: text})
                                       this.commentRefLayout.setError('')
                                   }}
                        />

                    </TextInputLayout>
                    {this.state.showRating && this.renderRating()}

                    {this.state.showOffer && <TextInputLayout
                        style={{marginTop: Dimens.dimen_10}}
                        ref={(input) => this.offerRefLayout = input}
                        hintColor={Colors.black_54}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                                   value={this.state.offer}
                                   ref={(input) => this.offerRef = input}
                                   placeholder={Strings.customer_offer}
                                   onChangeText={(text) => {
                                       this.setState({offer: text})
                                       this.commentRefLayout.setError('')
                                   }}
                        />

                    </TextInputLayout>}
                </View>
                <View style={styles.bottmBtnStyle}>
                    <TouchableOpacity style={styles.cancelStyle} onPress={() => this.cancelBottonmSheet()}>
                        <Text style={[styles.bottomBtntext, {color: Colors.buttonColor}]}>
                            {Strings.cancel}
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.followUpClicked()}>
                        <Text style={[styles.bottomBtntext, {color: 'white'}]}>
                            {Strings.set_follow_up}
                        </Text>
                    </TouchableOpacity>
                </View>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    is24Hour={false}
                    mode={'time'}
                />
            </View>
        )
    }

    dateClicked = () => {
        this.showDateTimePicker()
    }
    timeClicked = () => {
        this.showTimePicked();

    }

    showDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: true});
    };

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    showTimePicked = () => {
        this.setState({isTimePickerVisible: true});
    };

    hideTimePicker = () => {
        this.setState({isTimePickerVisible: false});
    };

    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({
            date: dateFormat(selectedDate, Constants.dateFormatUI),
            dateValue: dateFormat(selectedDate, 'yyyy-mm-dd')
        })
        this.dateRefLayout.setError('')
    };

    handleTimePicked = selectedTime => {

        var currentMS = new Date().getTime()
        var walkInTime = dateFormat(selectedTime.toString(), 'HH:MM')
        var walkInDate = dateFormat(this.state.date, 'mmmm dd, yyyy')
        var walkInMS = new Date(walkInDate + ' ' + walkInTime).getTime()

        if (walkInMS < currentMS) {
            Utility.showToast(Strings.GREATER_THAN_CURRENT_TIME)
            this.setState({ time: '', timeValue: '' })
        }
        else {
            this.setState({
                time: dateFormat(selectedTime, Constants.timeFormatUI),
                timeValue: dateFormat(selectedTime, 'HH:MM:ss')
            })
            this.timeRefLayout.setError()
        }
        this.hideTimePicker();
    };

    followUpClicked = () => {
        if (this.state.date == '') {
            this.dateRefLayout.setError(Strings.please_enter_date)
            return
        }
        if (this.state.time == '') {
            this.timeRefLayout.setError(Strings.please_enter_time)
            return
        }
        if (this.state.showRating) {

        }
        if (this.state.showOffer) {

        }

        this.lmsData.task_perform = Tasks.Follow_up;
        this.lmsData.date = this.state.dateValue;
        this.lmsData.time = this.state.timeValue;
        this.lmsData.comment = this.state.comment;
        this.lmsData.NewLeadState = this.props.data.NewLeadState;
        if (this.state.showRating) {
            this.lmsData.rating = this.state.rating
        }
        if (this.state.showOffer) {
            this.lmsData.amount = this.state.offer
        }
        this.props.onCompleteCallBack(true, this.lmsData);
        this.props.closeBottomSheet(true)
        // alert(this.state.date)

    };

    cancelBottonmSheet = () => {
        this.props.closeBottomSheet()
    }
}

const styles = StyleSheet.create({
    container: {
        paddingEnd: Dimens.dimen_20,
        paddingStart: Dimens.dimen_20,
        flex: 1
    },
    title: {
        fontWeight: 'bold',
        fontSize: Dimens.dimen_18,
        color: Colors.black,
        fontFamily: Strings.APP_FONT,
        marginBottom: Dimens.dimen_15,
        marginTop: -10,
    },
    btnBackground: {
        borderRadius: Dimens.dimen_25,
        borderWidth: Dimens.dimen_1,
        padding: Dimens.dimen_10,
        borderColor: "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex: 1, height: Dimens.dimen_60,
        color: Colors.black,
        paddingBottom: Dimens.dimen_10,
    },
    iconStyle: {
        tintColor: Colors.black,
        alignSelf: 'flex-end',
        width: Dimens.dimen_22,
        height: Dimens.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontSize: Dimens.dimen_16,
        fontFamily: Strings.APP_FONT,
        height: Dimens.dimen_36,
    },

    dateTimeRowStyle: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-start', padding: Dimens.dimen_5
    },
    followUpStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        height: Dimens.dimen_60
    },
    bottomBtntext: {
        fontSize: Dimens.dimen_16,
        fontFamily: Strings.APP_FONT,
        fontWeight: 'bold'
    },
    cancelStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: Dimens.dimen_26,
        height: Dimens.dimen_60,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT,
        borderWidth: 2,
        borderColor: Colors.buttonColor
    },
    bottmBtnStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-end',
        marginBottom: Dimens.dimen_0,
        marginTop: Dimens.dimen_20
    }
})

export default FollowUp

