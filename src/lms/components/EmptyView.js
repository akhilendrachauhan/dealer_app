import  React , {Component} from 'react'
import {Text,View} from 'react-native'
import  * as String from '../values/strings'
export default class EmptyView extends Component{


    render() {
        return (
            <View style={{flex : 1,height : 200,justifyContent : 'center', alignItems : 'center'}}>
                <Text style={{fontSize: 20, fontFamily:String.APP_FONT}}>{String.comming_soon}</Text>
            </View>
        );
    }
}