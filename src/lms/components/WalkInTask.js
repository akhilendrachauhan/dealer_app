import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, TextInput, Platform } from 'react-native'
import * as Colors from "../values/colors";
import * as Strings from '../values/strings'
import TextInputLayout from "../granulars/TextInputLayout";
import DateTimePicker from 'react-native-modal-datetime-picker'
import * as Constants from '../utils/Constants'
import * as Styles from '../values/styles'
import * as Dimens from '../values/Dimens'
import * as Images from '../values/Images'
import RadioButton from "./RadioButton1";
import * as Tasks from "../utils/Tasks";
import * as  Utility from '../utils/Utility'

let dateFormat = require('../utils/DateFormat');
export default class WalkInTask extends Component {
    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            date: '',
            time: '',
            dateValue: '',
            timeValue: '',
            comment: '',
            reminderTime: '',
            reminderDate: '',
            reminderTimeValue: '',
            reminderDateValue: '',
            isDateTimePickerVisible: false,
            isTimePickerVisible: false,
            isReminderDateTimePickerVisible: false,
            isReminderTimePickerVisible: false,
            reminder: false
        }
    }

    render() {
        return (
            <View style={{ width: '100%' }}>
                <View style={styles.container}>
                    <Text style={Styles.bottom.title}>{this.props.data.Title}</Text>
                    {this.props.data.Heading && <Text style={Styles.bottom.sub_title}>{this.props.data.Heading}</Text>}
                    <View style={{ paddingTop: Dimens.dimen_10, paddingBottom: Dimens.dimen_10, flexDirection: 'row' }}>

                        <View style={{ flex: 1, marginEnd: Dimens.dimen_10 }}>
                            <TouchableOpacity onPress={() => this.dateClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.dateRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                        value={this.state.date}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.dateClicked() : console.log('')}
                                        ref={(input) => this.dateRef = input}
                                        placeholder={Strings.date}
                                        editable={false}
                                    // onChangeText={(text) => this.setState({date: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>

                        </View>
                        <View style={{ flex: 1, marginStart: Dimens.dimen_10 }}>
                            <TouchableOpacity onPress={() => this.timeClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.timeRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.clockIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                        editable={false}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.timeClicked() : console.log('')}
                                        value={this.state.time}
                                        ref={(input) => this.timeRef = input}
                                        placeholder={Strings.time}
                                        onChangeText={(text) => this.setState({ time: text })}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TextInputLayout
                        style={{ marginTop: Dimens.dimen_10 }}
                        ref={(input) => this.commentRefLayout = input}
                        hintColor={Colors.black_54}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                            value={this.state.comment}
                            ref={(input) => this.commentRef = input}
                            placeholder={Strings.add_comment}
                            onChangeText={(text) => {
                                this.setState({ comment: text })
                                this.commentRefLayout.setError('')
                            }}
                        />

                    </TextInputLayout>

                    <TouchableOpacity style={styles.rowStyle}
                        onPress={() => this.onReminderClick()}>
                        <RadioButton
                            animation={'bounceIn'}
                            innerColor={Colors.radioBtnSelectedColor}
                            outerColor={Colors.radioBtnSelectedColor}
                            isSelected={this.state.reminder}
                            onPress={() => this.onReminderClick()} />
                        <Text style={styles.textStyle}>{Strings.set_reminder}</Text>
                    </TouchableOpacity>
                    {this.state.reminder && <View style={{ paddingTop: Dimens.dimen_10, paddingBottom: Dimens.dimen_10, flexDirection: 'row' }}>
                        <View style={{ flex: 1, marginEnd: Dimens.dimen_10 }}>
                            <TouchableOpacity onPress={() => this.reminderDateClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.dateReminderRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                        value={this.state.reminderDate}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.reminderDateClicked() : console.log('')}
                                        ref={(input) => this.dateReminderRef = input}
                                        placeholder={Strings.reminder_date}
                                        editable={false}
                                    // onChangeText={(text) => this.setState({date: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>

                        </View>
                        <View style={{ flex: 1, marginStart: Dimens.dimen_10 }}>
                            <TouchableOpacity onPress={() => this.reminderTimeClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.timeReminderRefLayout = input}
                                    hintColor={Colors.black_54}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.clockIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                        editable={false}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.reminderTimeClicked() : console.log('')}
                                        value={this.state.reminderTime}
                                        ref={(input) => this.timeReminderRef = input}
                                        placeholder={Strings.reminder_time}
                                    // onChangeText={(text) => this.setState({reminderTime: text})
                                    // }
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>
                        </View>
                    </View>}

                </View>

                <View style={styles.bottmBtnStyle}>
                    <TouchableOpacity style={styles.cancelStyle} onPress={() => this.cancelBottonmSheet()}>
                        <Text style={[styles.bottomBtntext, { color: Colors.buttonColor }]}>
                            {Strings.cancel}
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.followUpClicked()}>
                        <Text style={[styles.bottomBtntext, { color: 'white' }]}>
                            {Strings.set_dealership_visit}
                        </Text>
                    </TouchableOpacity>
                </View>

                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    is24Hour={false}
                    mode={'time'}
                />

                <DateTimePicker
                    isVisible={this.state.isReminderDateTimePickerVisible}
                    onConfirm={this.reminderHandleDatePicked}
                    onCancel={this.reminderHideDateTimePicker}
                    minimumDate={new Date()}
                    maximumDate={this.state.date != '' ? new Date(this.state.date) : undefined}
                />
                <DateTimePicker
                    isVisible={this.state.isReminderTimePickerVisible}
                    onConfirm={this.reminderHandleTimePicked}
                    onCancel={this.reminderHideTimePicker}
                    is24Hour={false}
                    mode={'time'}
                />
            </View>
        )
    }

    dateClicked = () => {
        this.showDateTimePicker()
    }
    timeClicked = () => {
        this.showTimePicked();
    }

    reminderDateClicked = () => {
        this.reminderShowDateTimePicker()
    }
    reminderTimeClicked = () => {
        this.reminderShowTimePicked();
    }

    onReminderClick = () => {
        this.setState({ reminder: !this.state.reminder })
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    showTimePicked = () => {
        this.setState({ isTimePickerVisible: true });
    };

    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };

    reminderShowDateTimePicker = () => {
        this.setState({ isReminderDateTimePickerVisible: true });
    };

    reminderHideDateTimePicker = () => {
        this.setState({ isReminderDateTimePickerVisible: false });
    };

    reminderShowTimePicked = () => {
        this.setState({ isReminderTimePickerVisible: true });
    };

    reminderHideTimePicker = () => {
        this.setState({ isReminderTimePickerVisible: false });
    };

    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({ date: dateFormat(selectedDate, Constants.dateFormatUI), dateValue: dateFormat(selectedDate, 'yyyy-mm-dd') })
        this.dateRefLayout.setError('')
    };

    handleTimePicked = selectedTime => {
        var currentMS = new Date().getTime()
        var walkInTime = dateFormat(selectedTime.toString(), 'HH:MM')
        var walkInDate = dateFormat(this.state.date, 'mmmm dd, yyyy')
        var walkInMS = new Date(walkInDate + ' ' + walkInTime).getTime()

        if (walkInMS < currentMS) {
            Utility.showToast(Strings.GREATER_THAN_CURRENT_TIME)
            this.setState({ time: '', timeValue: '' })
        }
        else {
            this.setState({ time: dateFormat(selectedTime, Constants.timeFormatUI), timeValue: dateFormat(selectedTime, 'HH:MM:ss') })
            this.timeRefLayout.setError()
        }
        this.hideTimePicker();
    };
    reminderHandleDatePicked = selectedDate => {
        this.reminderHideDateTimePicker();
        this.setState({ reminderDate: dateFormat(selectedDate, Constants.dateFormatUI), reminderDateValue: dateFormat(selectedDate, 'yyyy-mm-dd') })
        this.dateReminderRefLayout.setError('')
    };

    reminderHandleTimePicked = selectedTime => {
        var currentMS = new Date().getTime()
        var reminderTime = dateFormat(selectedTime.toString(), 'HH:MM')
        var reminderDate = dateFormat(this.state.reminderDate, 'mmmm dd, yyyy')
        var reminderMS = new Date(reminderDate + ' ' + reminderTime).getTime()

        var walkInDate = dateFormat(this.state.dateValue.toString(), 'mmmm dd, yyyy')
        var walkInMS = new Date(walkInDate + ' ' + this.state.timeValue).getTime()

        if (reminderMS < currentMS) {
            Utility.showToast(Strings.GREATER_THAN_CURRENT_TIME)
            this.setState({ reminderTime: '', reminderTimeValue: '' })
        }
        else if (reminderMS > walkInMS) {
            Utility.showToast(Strings.SMALLER_TIME)
            this.setState({ reminderTime: '', reminderTimeValue: '' })
        }
        else {
            this.setState({ reminderTime: dateFormat(selectedTime, Constants.timeFormatUI), reminderTimeValue: dateFormat(selectedTime, 'HH:MM:ss') })
            this.timeReminderRefLayout.setError()
        }
        this.reminderHideTimePicker();
    };

    followUpClicked = () => {
        if (this.state.date == '') {
            this.dateRefLayout.setError(Strings.please_enter_date)
            return
        } else if (this.state.time == '') {
            this.timeRefLayout.setError(Strings.please_enter_time)
            return
        } else if (this.state.reminder) {
            if (this.state.reminderDate == '') {
                this.dateReminderRefLayout.setError(Strings.please_enter_time)
                return
            }
            else if (this.state.reminderTime == '') {
                this.timeReminderRefLayout.setError(Strings.please_enter_time)
                return
            }
        }

        this.lmsData.task_perform = Tasks.WalkIn;
        this.lmsData.date = this.state.dateValue
        this.lmsData.time = this.state.timeValue
        this.lmsData.comment = this.state.comment
        this.lmsData.setReminder = this.state.reminder
        if (this.state.reminder) {
            this.lmsData.reminderdate = this.state.reminderDateValue
            this.lmsData.remindertime = this.state.reminderTimeValue
        } else {
            this.lmsData.reminderdate = this.state.dateValue
            this.lmsData.reminderTime = this.state.timeValue
        }
        this.props.onCompleteCallBack(true, this.lmsData)
        this.props.closeBottomSheet(true)
    }

    cancelBottonmSheet = () => {
        this.props.closeBottomSheet()
    }
}

const styles = StyleSheet.create({
    rowStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: Dimens.dimen_15,
    },
    textStyle: {
        justifyContent: 'center',
        marginStart: Dimens.dimen_10,
        fontFamily: Strings.APP_FONT,
        fontSize: Dimens.dimen_16
    },
    container: {
        paddingEnd: Dimens.dimen_20,
        paddingStart: Dimens.dimen_20,
        flex: 1
    },
    title: {
        fontWeight: 'bold',
        fontSize: Dimens.dimen_18,
        color: Colors.black,
        fontFamily: Strings.APP_FONT,
        marginBottom: Dimens.dimen_15,
        marginTop: -10,
    },
    btnBackground: {
        borderRadius: Dimens.dimen_25,
        borderWidth: Dimens.dimen_1,
        padding: Dimens.dimen_10,
        borderColor: "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex: 1, height: Dimens.dimen_60,
        color: Colors.black,
        paddingBottom: Dimens.dimen_10,
    },
    iconStyle: {
        tintColor: Colors.black,
        alignSelf: 'flex-end',
        width: Dimens.dimen_22,
        height: Dimens.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontSize: Dimens.dimen_16,
        fontFamily: Strings.APP_FONT,
        height: Dimens.dimen_36,
    },

    dateTimeRowStyle: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-start', padding: Dimens.dimen_5
    },
    followUpStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        height: Dimens.dimen_60
    },
    bottomBtntext: {
        fontSize: Dimens.dimen_16,
        fontFamily: Strings.APP_FONT,
        fontWeight: 'bold'
    },
    cancelStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: Dimens.dimen_26,
        height: Dimens.dimen_60,
        fontWeight: 'bold',
        borderWidth: 2,
        fontFamily: Strings.APP_FONT,
        borderColor: Colors.buttonColor
    },
    bottmBtnStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-end',
        marginBottom: Dimens.dimen_0,
        marginTop: Dimens.dimen_20
    }
})



