import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, TextInput} from 'react-native'
import * as  Colors from '../values/colors'
import * as  Strings from '../values/strings'
import * as  Constants from '../utils/Constants'
import * as  Styles from '../values/styles'
import * as Dimens from "../values/Dimens";
import TextInputLayout from "../granulars/TextInputLayout";

export default class PriceComponent extends Component {

    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            price: ''
        }
    }

    render() {
        let placeholder = this.props.data.helpText;
        if (placeholder == '')
            placeholder = Strings.offer_price
        return (
            <View style={{width: '100%'}}>
                <View style={{flex: 1, padding: Dimens.dimen_20}}>
                    <Text style={Styles.bottom.title}>{this.props.data.Title}</Text>
                    {this.props.data.Heading && <Text style={Styles.bottom.sub_title}>{this.props.data.Heading}</Text>}
                    <View style={{flex: 1, marginEnd: Dimens.dimen_10}}>
                        <TextInputLayout
                            style={{}}
                            ref={(input) => this.priceRefLayout = input}
                            hintColor={Colors.black}
                            errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                            focusColor={Colors.black}
                            errorColorMargin={Dimens.dimen_20}>
                            <TextInput style={StylesOwn.textInput}
                                       value={this.state.price}
                                       ref={(input) => this.priceRef = input}
                                       placeholder={placeholder}
                                       editable={true}
                                       onChangeText={(input) => {
                                           this.setState({price: input})
                                           this.priceRefLayout.setError("")
                                       }}
                            />
                        </TextInputLayout>
                    </View>

                </View>
                <View style={{flexDirection: 'row'}}>
                    {this.props.data.skip &&
                    <TouchableOpacity style={StylesOwn.skipButtonStyle} onPress={() => this.onSkipClicked()}>
                        <Text style={[StylesOwn.skipBtntext, {color: Colors.buttonColor}]}>
                            {Strings.skip}
                        </Text>
                    </TouchableOpacity>}
                    <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onSubmitClicked()}>
                        <Text style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>
                            {Strings.submit}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    onSubmitClicked = () => {
        if (this.state.price == '') {
            this.priceRefLayout.setError(Strings.please_enter_price)
            return
        } else {
            this.lmsData.price=this.state.price
            this.lmsData.NewLeadState=this.props.data.NewLeadState
            // console.log("TASk DATA " + JSON.stringify(this.props.data))
            if (this.props.data.skip) {
                this.props.callBackMethod(this.props.data.offer_given)
            } else {
                this.props.callBackMethod(this.props.data.action_task_id)
            }
        }

    }

    onSkipClicked = () => {
        if (this.state.price == '') {
            this.priceRefLayout.setError(Strings.please_enter_price)
            return
        } else {
            this.lmsData.NewLeadState=this.props.data.NewLeadState
            // console.log("SKIP TASk DATA " + JSON.stringify(this.props.data))
            if (this.props.data.skip) {
                this.props.callBackMethod(this.props.data.offer_not_given)
            } else {
                this.props.callBackMethod(this.props.data.action_task_id)
            }

        }

    }

}

const StylesOwn = StyleSheet.create({
        textInput: {
            alignSelf: 'stretch',
            textAlign: 'center',
            color: Colors.black,
            fontFamily:Strings.APP_FONT,
            fontSize: Dimens.dimen_16,
            height: Dimens.dimen_36,
        },
        skipButtonStyle: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.WHITE,
            height: Dimens.btnHeight,
            marginEnd: Dimens.dimen_10,
            borderColor: Colors.buttonColor,
            borderWidth:2
        },
        skipBtntext: {
            fontSize: 16,
            fontFamily:Strings.APP_FONT,
            fontWeight: 'bold'
        },
    }
)