import React, {Component} from 'react'
import {View, TouchableOpacity, Image, StyleSheet, Modal} from 'react-native'
import * as Strings from "../values/strings";
import Question from "./Question"
import * as Dimens from '../values/Dimens'
import * as Task from '../utils/Tasks'
import FollowUp from './FollowUp'
import EmptyView from './EmptyView'
import EvaluationSchedule from './EvaluationSchedule'
import CloseTask from './CloseTask'
import PriceComponent from './PriceComponent'
import CheckListComponent from './CheckListComponent'
import ConfirmTradeOut from './ConfirmTradeOut'
import ConfirmTradeIn from './ConfirmTradeIn'
import WalkInTask from './WalkInTask'
import * as Images from '../values/Images'
import * as AppConstants from '../../util/Constants';
import * as AppUtility from '../../util/Utility';

const JsonFile = require('../assets/tradeOut');
const JsonFileBhasa = require('../assets/tradeOut_id');
let taskIds = [];
let taskDataModel = {
    "tracking": '',
    "amount": '',
    "comment": '',
    "date": '',
    "followUpType": '',
    "NewLeadState": '',
    "task_id": '',
    "task_perform": '',
    "task_type_name": '',
    "time": '',
    "Title": '',
    "leadRating": '',
    "reminderdate": '',
    "remindertime": '',
    "setReminder": false,
    "closeReason": '',
    "option_id": '',
    "returnstatus": false,
    "return_status_name": '',
    "return_task_id": '',
    "shareType": '',
    "task_type": '',
}

class LMSParser extends Component {
    lmsData;
    leadStates = []
    currentState = {}

    constructor(props) {
        super(props);
        taskIds = [];
        this.lmsData = {};
        this.leadStates = []
        this.currentState = {}
        this.isBackPressed = false;
        this.initiateLms = this.initiateLms.bind(this);
        this.state = {
            isVisible: false,
           
        }
        /*for (var j = 0; j < JsonFile.leadsStates.length; j++) {
            this.leadStates.push(JsonFile.leadsStates[j]);
        }
        console.log('here', this.props.leadData);
        let leadState = '';
        let idForReturnTask = undefined;
        const {leadStatus, walkInDate, returnStatus, returnTaskId} = this.props.leadData;
        if (returnStatus && returnStatus != '') {
            leadState = returnStatus;
            idForReturnTask = returnTaskId;
        } else {
            leadState = leadStatus
        }
        let currentDate = new Date();
        const walkInDateValue = walkInDate ? walkInDate : currentDate;
        for (var i = 0; i < this.leadStates.length; i++) {
            console.log('here toooooooo')
            if (leadState != null && leadState === this.leadStates[i].Status_name) {
                console.log('here found', this.leadStates[i])
                this.currentState = this.leadStates[i]
                if (this.leadStates[i].Status_name == Task.WalkIn) {
                    let before = walkInDateValue >= currentDate;
                    let taskId = idForReturnTask ? parseInt(idForReturnTask) : (before ? this.leadStates[i].BeforeWalkInTask : this.leadStates[i].AfterWalkInTask)
                    taskIds.push(before ? this.leadStates[i].BeforeWalkInTask : this.leadStates[i].AfterWalkInTask)
                    this.state = {
                        taskActionId: taskId,
                        statetaskIds: taskIds
                    }

                } else {
                    let taskId = idForReturnTask ? parseInt(idForReturnTask) : this.leadStates[i].action_task_id;
                    taskIds.push(taskId);
                    this.state = {
                        taskActionId: taskId,
                        statetaskIds: taskIds
                    }
                }
                break
            }
        }
        console.log('taskActionId', this.state.taskActionId)*/
    }

    
    initiateLms(leadData) {
        // console.log('initiated', leadData);
        let lmsJson;
        AppUtility.getValueFromAsyncStorage(AppConstants.LANGUAGE_CODE).then(value => {
            if(value && value == AppConstants.LANGUAGE_CODE_BHASA){
               lmsJson = JsonFileBhasa.leadsStates;
            }else {
                lmsJson = JsonFile.leadsStates;
            }
            taskIds = [];
            this.lmsData = {};
            this.leadStates = []
            this.currentState = {}
            this.isBackPressed = false;
            for (var j = 0; j < lmsJson.length; j++) {
                this.leadStates.push(lmsJson[j]);
            }
            let leadState = '';
            let idForReturnTask = undefined;
            const {leadStatus, walkInDate, returnStatus, returnTaskId} = leadData;
            if (returnStatus && returnStatus != '') {
                leadState = returnStatus;
                idForReturnTask = returnTaskId;
            } else {
                leadState = leadStatus
            }
            let currentDate = new Date();
            currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0)
            let walkInDateValue = currentDate;
            if (walkInDate && walkInDate !== '') {
                // console.log('Walk-in date original', walkInDate);
                walkInDateValue = new Date(walkInDate);
                walkInDateValue = new Date(walkInDateValue.getFullYear(), walkInDateValue.getMonth(), walkInDateValue.getDate(), 0, 0, 0);
            }
            // console.log('Walk-in date', walkInDateValue.toString(), currentDate.toString());
            for (var i = 0; i < this.leadStates.length; i++) {
                if (leadState != null && leadState === this.leadStates[i].Status_name) {
                    this.currentState = this.leadStates[i]
                    if (this.leadStates[i].Status_name == Task.WalkIn) {
                        let before = walkInDateValue >= currentDate;
                        // console.log('Before', before);
                        let taskId = idForReturnTask ? parseInt(idForReturnTask) : (before ? this.leadStates[i].BeforeWalkInTask : this.leadStates[i].AfterWalkInTask)
                        taskIds.push(before ? this.leadStates[i].BeforeWalkInTask : this.leadStates[i].AfterWalkInTask)
                        this.setState({
                            taskActionId: taskId,
                            statetaskIds: taskIds,
                            isVisible: true
                        })

                    } else {
                        let taskId = idForReturnTask ? parseInt(idForReturnTask) : this.leadStates[i].action_task_id;
                        taskIds.push(taskId);
                        this.setState({
                            taskActionId: taskId,
                            statetaskIds: taskIds,
                            isVisible: true
                        })
                    }
                    break
                }
            }
        })
    };

    render() {
        return (
            <View style={{flex: 1, marginTop: Dimens.dimen_50, alignItems: "center", backgroundColor: 'white'}}>
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => {
                        this.closeBottomSheet()
                    }}>
                    <View style={{flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'flex-end'}}>
                        <View style={styls.modal}>
                            <TouchableOpacity style={{alignSelf: 'flex-end'}}
                                              onPress={() => this.closeBottomSheet(true)}>
                                <Image style={styls.imageStyle} source={Images.close}/>
                            </TouchableOpacity>
                            {this.state.isVisible && this.getViews(this.getTask(this.getTaskId(this.state.taskActionId)))}
                        </View>
                    </View>
                    
                </Modal>
            </View>
        )
    }

    closeBottomSheet = (flag) => {
        if (!flag) {
            taskIds.pop()
            this.isBackPressed = true
            if (taskIds.length === 0) {
                this.props.lmsResetCallBack()
                this.setState({isVisible: false})
            } else
                this.setState({isVisible: true, statetaskIds: taskIds})
        } else {
            taskIds = []
            this.setState({isVisible: false})
            this.props.lmsResetCallBack()
        }
    };


    onLmsCompleted = (flag, lmsData) => {
        this.lmsData = {...this.lmsData, ...lmsData};
        taskIds.push(Task.LeafNode);
        this.lmsData.tracking = taskIds.toString();
        this.props.onLmsCompleted(true, this.lmsData);
    };

    getToNextTask = (taskId) => {
        this.setState({isVisible: true, statetaskIds: taskId})
    };

    getTask = (taskId) => {

        if (taskId && taskId !== '') {
            for (let i = 0; i < this.currentState.Tasks.length; i++) {
                if (taskId === this.currentState.Tasks[i].task_id) {
                    return this.currentState.Tasks[i];
                }
            }
        } else {
            console.warn('LMS JSON parse error')
        }

    };

    getTaskId = (taskId) => {
        let id = taskId;
        if (this.isBackPressed) {
            id = taskIds[taskIds.length - 1]
        }
        return id;
    };

    getViews = (task) => {
        this.lmsData = {...taskDataModel, ...task};
        if (task.task_type_name == undefined || task.task_type_name == '') {
            return <EmptyView/>
        } else if (task.task_type_name === Task.LeafNode) {
            taskIds.push(Task.LeafNode)
            this.lmsData.tracking = taskIds.toString();
            this.props.onLmsCompleted(true, this.lmsData, task);
            return null;
        } else if (task.task_type_name === Task.WalkIn)
            return <WalkInTask data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                               callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Question)
            return <Question data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                             callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Follow_up)
            return <FollowUp data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                             callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Evaluation)
            return <EvaluationSchedule data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                                       callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Price)
            return <PriceComponent data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                                   callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Close)
            return <CloseTask data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                              callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Checklist)
            return <CheckListComponent data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                                       callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.Confirm_Trade_In)
            return <ConfirmTradeIn data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                                   callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else if (task.task_type_name === Task.ConfirmTradeOut)
            return <ConfirmTradeOut data={task} lmsData={this.lmsData} closeBottomSheet={this.closeBottomSheet}
                                    callBackMethod={this.callBackClicked} onCompleteCallBack={this.onLmsCompleted}/>
        else {
            return <EmptyView/>
        }
    }

    callBackClicked = (taskId) => {
        this.isBackPressed = false;
        taskIds.push(taskId);
        let task = this.getTask(taskId);
        if (task && task.task_type_name === Task.LeafNode) {
            this.lmsData = {...taskDataModel, ...task};
            taskIds.push(Task.LeafNode);
            this.lmsData.tracking = taskIds.toString();
            this.closeBottomSheet(true);
            this.props.onLmsCompleted(true, this.lmsData, task);
        } else {
            this.setState({taskActionId: taskId, isVisible: true, statetaskIds: taskIds})
        }
    }
}

const styls = StyleSheet.create({
    imageStyle: {
        margin: Dimens.dimen_10,
        width: Dimens.dimen_25,
        height: Dimens.dimen_25
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        width: '100%',
        bottom: Dimens.dimen_0,
        position: 'absolute'
    },
});

export default LMSParser
