import React, {Component} from 'react'
import {Text, View, StyleSheet, TouchableOpacity, FlatList} from 'react-native'
import * as Dimens from "../values/Dimens";
import * as Color from "../values/colors";
import * as Images from "../values/Images";
import CheckBox from 'react-native-check-box'
import * as Styles from "../values/styles";
import * as Strings from "../values/strings";

export default class CheckListComponent extends Component {

    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            isChecked: false,
            dataSource: this.getChcekList()
        }
    }

    render() {
        return (
            <View style={{width: '100%'}}>
                <View style={styles.container}>
                    <Text style={styles.title}>{this.props.data.Title}</Text>
                    <Text style={styles.sub_title}>{this.props.data.Title}</Text>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({item}) =>
                            <TouchableOpacity onPress={() => {
                                this.onCheckBoxClicked(item)
                            }}>
                                <CheckBox
                                    style={{flex: 1, padding: 10}}
                                    onClick={() => {
                                        this.onCheckBoxClicked(item)
                                    }}
                                    isChecked={item.isChecked}
                                    rightText={item.name}
                                    checkBoxColor={Color.buttonColor}
                                /></TouchableOpacity>
                        }
                        keyExtractor={item => item.id}
                        extraData={this.state}
                    />
                </View>
                <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onSubmitClicked()}>
                    <Text style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>
                        {Strings.submit}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    onCheckBoxClicked = (item) => {
        // console.log("befor  " + item.isChecked)
        item.isChecked = !item.isChecked
        // console.log("after  " + item.isChecked)

        this.setState({isChecked: item.isChecked})
    }

    onSubmitClicked = () => {
        let ischecked = false;
        for (var i = 0; i < this.state.dataSource.length; i++) {
            if (this.state.dataSource[i].isChecked) {
                ischecked = true;
                break
            }
        }

        if (!ischecked) {
            alert(Strings.please_select_at_least_one)
        } else {
            this.lmsData.NewLeadState = this.props.data.NewLeadState
            this.props.onCompleteCallBack(true, this.lmsData)
            this.props.closeBottomSheet(true)
        }
    }
    getChcekList = () => {
        let checkList = [{name: "Voter Id", id: "1", isChecked: false}, {
            name: "Driving Licence",
            id: "2",
            isChecked: false
        }, {name: "Passport", id: "3", isChecked: false}, {
            name: "Adhaar",
            id: "4",
            isChecked: false
        }, {name: "Pan Number", id: "5", isChecked: false}, {name: "RC", id: "6", isChecked: false}]
        checkList.push({name: "Naarega Card", id: "7", isChecked: false})
        checkList.push({name: "Other Documents", id: "8", isChecked: false})
        return checkList
    }
}


const styles = StyleSheet.create({
    container: {
        padding: Dimens.dimen_20,
    },
    imageStyle: {
        marginTop: Dimens.dimen_10,
        marginEnd: Dimens.dimen_10,
        width: Dimens.dimen_20,
        height: Dimens.dimen_20,
        alignSelf: 'flex-end'
    },
    title: {
        fontWeight: 'bold',
        fontSize: Dimens.dimen18,
        color: Color.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_15,
        marginTop: -10,
    },
    sub_title: {
        fontSize: Dimens.dimen_14,
        color: Color.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_10,
        marginTop: Dimens.dimen_4,
    },
    item: {
        paddingBottom: Dimens.dimen_10,
        paddingTop: Dimens.dimen_10,
    },
})
