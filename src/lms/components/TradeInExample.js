import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,TouchableOpacity
} from 'react-native';
import  LMSParser from '../components/LMSParser'
import JsonFile from '../assets/tradeIn'
import * as Strings from '../values/strings'
import * as LeadStatus from '../utils/LeadStatus'

class TradeInExample extends Component {
    leadStatus='leadStatus'
    static navigationOptions = {
        title: Strings.appScreenName,
    };

    constructor(){
        super()
        // console.log("JSON File= "+JSON.stringify(JsonFile))
        this.state={
            lmsShow:false,
            leadStatus : ''
        }
    }
    render() {
        return (
            <ScrollView>
            <View style={styles.container}>

                <TouchableOpacity onPress={()=>this.newLead()}>
                    <Text style={styles.btn}> {Strings.new_lead}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.followUplead()}>
                    <Text style={styles.btn}> {Strings.follow_up_lead}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.interesteeLead()}>
                    <Text style={styles.btn}> {Strings.interested_lead}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.walkinClicked()}>
                    <Text style={styles.btn}> {Strings.walkin}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.walkedInClicked()}>
                    <Text style={styles.btn}> {Strings.WalkedIn}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.customerOfferClicked()}>
                    <Text style={styles.btn}> {Strings.customer_offer}</Text>
                </TouchableOpacity>



                <TouchableOpacity onPress={()=>this.newEnquiry()}>
                    <Text style={styles.btn}> {Strings.new_Enquiries}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.appointmentPending()}>
                    <Text style={styles.btn}> {Strings.appointment_Pending}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.evaluationScheduled()}>
                    <Text style={styles.btn}> {Strings.evaluation_Scheduled}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.quotePending()}>
                    <Text style={styles.btn}> {Strings.quote_Pending}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.priceNegotiation()}>
                    <Text style={styles.btn}> {Strings.price_Negotiation}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.tradeInPending()}>
                    <Text style={styles.btn}> {Strings.trade_in_Pending}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.documentPending()}>
                    <Text style={styles.btn}> {Strings.document_Pending}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.appEvaluation()}>
                    <Text style={styles.btn}> {Strings.appEvaluation}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.appOffer()}>
                    <Text style={styles.btn}> {Strings.appOffer}</Text>
                </TouchableOpacity>
                {this.state.lmsShow && <LMSParser lmsResetCallBack={this.resetLmsVisibilty} leadStatus={this.state.leadStatus}/>}
            </View>
            </ScrollView>
        )
    }

    newLead=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.New})
    }
    followUplead=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Follow_up})
    }
    interesteeLead=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Interested})
    }

    walkinClicked=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.WalkIn})
    }
    walkedInClicked=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.WalkedIn})
    }

    customerOfferClicked=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.WalkedIn})
    }





    newEnquiry=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.New_Enquiries})
    }

    resetLmsVisibilty=()=>{
        this.setState({lmsShow:false})
    }

    appointmentPending=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Appointment_Pending})
    }

    evaluationScheduled=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Evaluation_Scheduled})
    }

    quotePending=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Quote_Pending})
    }

    priceNegotiation=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Price_Negotiation})
    }
    tradeInPending=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Trade_in_Pending})
    }
    documentPending=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.Document_Pending})
    }
    appEvaluation=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.AppEvaluation})
    }
    appOffer=()=>{
        this.setState({lmsShow:true,leadStatus : LeadStatus.AppOffer})
    }
}

const  styles=StyleSheet.create({
    container:{
        flex : 1,
        padding : 10,
    },
    btn:{
        fontSize : 16,
        padding : 8,
        justifyContent : 'center',
        alignItems : 'center',
        color : 'black',
        fontFamily:Strings.APP_FONT,
        backgroundColor : '#a3a2a2',
        borderRadius : 5,
        elevation : 8,
        margin : 10,
    }
})
export default TradeInExample;
