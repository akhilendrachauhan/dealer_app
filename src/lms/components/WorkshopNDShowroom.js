import React,{Component} from  'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import * as Colors from "../values/colors";
import TextInputLayout from "../granulars/TextInputLayout";
import * as Strings from "../values/strings";
import * as Constants from "../utils/Constants";
import * as Dimen from "../values/Dimens";
import DateTimePicker from "react-native-modal-datetime-picker";
let dateFormat = require('../utils/DateFormat');
import * as Images from '../values/Images'
export default class WorkshopNDShowroom extends Component{

    constructor(){
        super()
        this.state={
            date : '',
            time : '',
            comment : ''
        }
    }

    render() {
        return(
            <View>
                <View style={{paddingTop : Dimen.dimen_0, paddingBottom : Dimen.dimen_10,flexDirection: 'row'}}>

                    <View style={{flex : 1,marginEnd : Dimen.dimen_10}}>
                        <TouchableOpacity onPress={()=>this.dateClicked()}>
                            <TextInputLayout
                                style={{}}
                                ref={(input) => this.workShowroomDateRefLayout = input}
                                hintColor={Colors.black}
                                errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                focusColor={Colors.black}
                                showRightDrawable={true}
                                rightDrawable ={Images.calendarIcon}
                                errorColorMargin={Dimen.dimen_20}>
                                <TextInput style={styles.textInput}
                                           value={this.state.date}
                                           ref={(input) => this.workShowroomDateRef = input}
                                           placeholder={Strings.date}
                                           editable={false}
                                    // onChangeText={(text) => this.setState({date: text})}
                                />

                            </TextInputLayout>
                        </TouchableOpacity>

                    </View>
                    <View style={{flex : 1, marginStart : Dimen.dimen_10}}>
                        <TouchableOpacity onPress={()=>this.timeClicked()}>
                            <TextInputLayout
                                style={{}}
                                ref={(input) => this.workShowroomTimeRefLayout = input}
                                hintColor={Colors.black}
                                errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                focusColor={Colors.black}
                                showRightDrawable={true}
                                rightDrawable ={Images.calendarIcon}
                                errorColorMargin={Dimen.dimen_20}>
                                <TextInput style={styles.textInput}
                                           editable={false}
                                           value={this.state.time}
                                           ref={(input) => this.workShowroomTimeRef = input}
                                           placeholder={Strings.time}
                                           onChangeText={(text) => this.setState({time: text})}
                                />

                            </TextInputLayout>
                        </TouchableOpacity>
                    </View>
                </View>

                <TextInputLayout
                    style={{marginTop: Dimen.dimen_10,marginBottom : Dimen.dimen_20}}
                    ref={(input) => this.workShowroomCommentRefLayout = input}
                    hintColor={Colors.black}
                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                    focusColor={Colors.black}
                    errorColorMargin={Dimen.dimen_20}>
                    <TextInput style={styles.textInput}
                               value={this.state.comment}
                               ref={(input) => this.workShowroomCoomentRef = input}
                               placeholder={Strings.add_comment}
                               onChangeText={(text) =>{ this.setState({comment: text})
                                   this.workShowroomCommentRefLayout.setError('')
                               }}
                    />
                </TextInputLayout>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    mode={'time'}
                />
            </View>
        )
    }
    dateClicked =()=>{
        this.showDateTimePicker()
    }
    timeClicked=()=>{
        this.showTimePicked();

    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    showTimePicked = () => {
        this.setState({ isTimePickerVisible: true });
    };

    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };

    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({date: dateFormat(selectedDate,Constants.dateFormatUI)})
        this.workShowroomDateRefLayout.setError('')
    };

    handleTimePicked = selectedTime => {
        this.hideTimePicker();
        this.setState({time: dateFormat(selectedTime,Constants.timeFormatUI)})
        this.workShowroomTimeRefLayout.setError()
    };
}

const  styles=StyleSheet.create({
    container : {
        padding : Dimen.dimen_10,
    },
    imageStyle : {
        marginTop : Dimen.dimen_10,
        marginEnd : Dimen.dimen_10,
        width : Dimen.dimen_20,
        height : Dimen.dimen_20,
        alignSelf : 'flex-end'
    },
    title : {
        fontWeight : 'bold',
        fontSize : Dimen.dimen18,
        color : Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom : Dimen.dimen_15,
        marginTop: -10,
    },
    sub_title : {
        fontSize : Dimen.dimen_14,
        color : Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom :  Dimen.dimen_10,
        marginTop: Dimen.dimen_4,
    },
    btnBackground : {
        borderRadius : Dimen.dimen_25,
        borderWidth : Dimen.dimen_1,
        padding : Dimen.dimen_10,
        borderColor : "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex :1,height:Dimen.dimen_60,
        color: Colors.black,
        paddingBottom: Dimen.dimen_10,
    },
    iconStyle: {
        tintColor:Colors.black,
        alignSelf : 'flex-end',
        width : Dimen.dimen_22,
        height:  Dimen.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontSize: Dimen.dimen_16,
        fontFamily:Strings.APP_FONT,
        height: Dimen.dimen_36,
        // fontFamily: 'Roboto-Regular'
    },

    dateTimeRowStyle:{
        flex :1 ,flexDirection: 'row',justifyContent : 'flex-start',padding : Dimen.dimen_5
    }

})