import React,{Component} from  'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import * as Colors from "../values/colors";
import TextInputLayout from "../granulars/TextInputLayout";
import * as Strings from "../values/strings";
import * as Dimens from "../values/Dimens";
import * as Constants from "../utils/Constants";
import DateTimePicker from "react-native-modal-datetime-picker";
let dateFormat = require('../utils/DateFormat');
import * as Images from '../values/Images'
export default class CustomerLocatiom extends Component{

    constructor(){
        super()
        this.state={
            address:'',
            date : '',
            time : '',
            comment : ''
        }
    }

    render() {
        return(
            <View>
                <TextInputLayout
                    style={{marginTop: Dimens.dimen_0}}
                    ref={(input) => this.custAddreeRefLayout = input}
                    hintColor={Colors.black}
                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                    focusColor={Colors.black}
                    errorColorMargin={Dimens.dimen_20}>
                    <TextInput style={styles.textInput}
                               value={this.state.address}
                               ref={(input) => this.custAddressRef = input}
                               placeholder={Strings.address}
                               onChangeText={(text) =>{ this.setState({address: text})
                                   this.custAddreeRefLayout.setError('')
                               }}
                    />
                </TextInputLayout>

                <View style={{paddingTop : Dimens.dimen_10, paddingBottom : Dimens.dimen_10,flexDirection: 'row'}}>

                    <View style={{flex : 1,marginEnd : Dimens.dimen_10}}>
                        <TouchableOpacity onPress={()=>this.dateClicked()}>
                            <TextInputLayout
                                style={{}}
                                ref={(input) => this.dateRefLayout = input}
                                hintColor={Colors.black}
                                errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                focusColor={Colors.black}
                                showRightDrawable={true}
                                rightDrawable ={Images.calendarIcon}
                                errorColorMargin={Dimens.dimen_20}>
                                <TextInput style={styles.textInput}
                                           value={this.state.date}
                                           ref={(input) => this.dateRef = input}
                                           placeholder={Strings.date}
                                           editable={false}
                                    // onChangeText={(text) => this.setState({date: text})}
                                />

                            </TextInputLayout>
                        </TouchableOpacity>

                    </View>
                    <View style={{flex : 1, marginStart : Dimens.dimen_10}}>
                        <TouchableOpacity onPress={()=>this.timeClicked()}>
                            <TextInputLayout
                                style={{}}
                                ref={(input) => this.timeRefLayout = input}
                                hintColor={Colors.black}
                                errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                focusColor={Colors.black}
                                showRightDrawable={true}
                                rightDrawable ={Images.calendarIcon}
                                errorColorMargin={Dimens.dimen_20}>
                                <TextInput style={styles.textInput}
                                           editable={false}
                                           value={this.state.time}
                                           ref={(input) => this.timeRef = input}
                                           placeholder={Strings.time}
                                           onChangeText={(text) => this.setState({time: text})}
                                />

                            </TextInputLayout>
                        </TouchableOpacity>
                    </View>
                </View>

                <TextInputLayout
                    style={{marginTop: Dimens.dimen_10,marginBottom : Dimens.dimen_20}}
                    ref={(input) => this.commentRefLayout = input}
                    hintColor={Colors.black}
                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                    focusColor={Colors.black}
                    errorColorMargin={Dimens.dimen_20}>
                    <TextInput style={styles.textInput}
                               value={this.state.comment}
                               ref={(input) => this.commentRef = input}
                               placeholder={Strings.add_comment}
                               onChangeText={(text) =>{ this.setState({comment: text})
                                   this.commentRefLayout.setError('')
                               }}
                    />
                </TextInputLayout>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    mode={'time'}
                />
            </View>
        )
    }

    dateClicked =()=>{
        this.showDateTimePicker()
    }
    timeClicked=()=>{
        this.showTimePicked();

    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    showTimePicked = () => {
        this.setState({ isTimePickerVisible: true });
    };

    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false });
    };

    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({date: dateFormat(selectedDate,Constants.dateFormatUI)})
        this.dateRefLayout.setError('')
    };

    handleTimePicked = selectedTime => {
        this.hideTimePicker();
        this.setState({time: dateFormat(selectedTime,Constants.timeFormatUI)})
        this.timeRefLayout.setError()
    };
}

const  styles=StyleSheet.create({
    container : {
        padding : Dimens.dimen_20,
    },
    imageStyle : {
        marginTop : Dimens.dimen_10,
        marginEnd : Dimens.dimen_10,
        width : Dimens.dimen_20,
        height : Dimens.dimen_20,
        alignSelf : 'flex-end'
    },
    title : {
        fontWeight : 'bold',
        fontSize : Dimens.dimen_18,
        color : Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom : Dimens.dimen_15,
        marginTop: -10,
    },
    sub_title : {
        fontSize : Dimens.dimen_14,
        color : Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom : Dimens.dimen_10,
        marginTop: Dimens.dimen_4,
    },
    btnBackground : {
        borderRadius : Dimens.dimen_25,
        borderWidth : Dimens.dimen_1,
        padding : Dimens.dimen_10,
        borderColor : "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex :1,height:Dimens.dimen_60,
        color: Colors.black,
        paddingBottom: Dimens.dimen_10,
    },
    iconStyle: {
        tintColor:Colors.black,
        alignSelf : 'flex-end',
        width : Dimens.dimen_22,
        height:  Dimens.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontFamily:Strings.APP_FONT,
        fontSize: Dimens.dimen_16,
        height: Dimens.dimen_36,
        // fontFamily: 'Roboto-Regular'
    },

    dateTimeRowStyle:{
        flex :1 ,flexDirection: 'row',justifyContent : 'flex-start',padding : Dimens.dimen_5
    }

})