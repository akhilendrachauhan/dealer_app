import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, TouchableOpacity
} from 'react-native';
import LMSParser from '../components/LMSParser'
import JsonFile from '../assets/tradeOut'
import * as Strings from '../values/strings'
import * as LeadStatus from '../utils/LeadStatus'
import {Colors, Dimens} from "../../values";

class App extends Component {
    leadStatus = 'leadStatus'
    static navigationOptions = {
        title: Strings.appScreenName,
    };

    constructor() {
        super()
        // console.log("JSON File= " + JSON.stringify(JsonFile))
        this.state = {
            lmsShow: false,
            leadStatus: ''
        }
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>

                    <TouchableOpacity onPress={() => this.newLead()}>
                        <Text style={styles.btn}> {Strings.new_lead}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.followUplead()}>
                        <Text style={styles.btn}> {Strings.follow_up_lead}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.interesteeLead()}>
                        <Text style={styles.btn}> {Strings.interested_lead}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.walkinClicked()}>
                        <Text style={styles.btn}> {Strings.walkin}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.walkedInClicked()}>
                        <Text style={styles.btn}> {Strings.WalkedIn}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.customerOfferClicked()}>
                        <Text style={styles.btn}> {Strings.customer_offer}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.booked()}>
                        <Text style={styles.btn}>{Strings.booked}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.appBooked()}>
                        <Text style={styles.btn}>{Strings.app_booked}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.appOffer()}>
                        <Text style={styles.btn}> {Strings.appOffer}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.close()}>
                        <Text style={styles.btn}>{Strings.close}</Text>
                    </TouchableOpacity>

                    <LMSParser
                        ref={'lms'}
                        lmsResetCallBack={this.resetLmsVisibilty}
                        onLmsCompleted={this.onLmsCompleted}/>
                </View>
            </ScrollView>
        )
    }

    onLmsCompleted = (flag, lmsData) => {
        /*this.setState({
            lmsShow: false
        });*/
        // console.log(lmsData)
    };

    newLead = () => {
        this.initiateLms(LeadStatus.New)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.New})
    };
    followUplead = () => {
        this.initiateLms(LeadStatus.Follow_up)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.Follow_up})
    };
    interesteeLead = () => {
        this.initiateLms(LeadStatus.Interested)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.Interested})
    };

    walkinClicked = () => {
        this.initiateLms(LeadStatus.WalkIn)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.WalkIn})
    };
    walkedInClicked = () => {
        this.initiateLms(LeadStatus.WalkedIn)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.WalkedIn})
    };

    customerOfferClicked = () => {
        this.initiateLms(LeadStatus.CustomerOffer)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.CustomerOffer})
    };

    resetLmsVisibilty = () => {
        //this.setState({lmsShow: false})
    };

    appOffer = () => {
        this.initiateLms(LeadStatus.AppOffer)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.AppOffer})
    };

    close = () => {
        this.initiateLms(LeadStatus.Close)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.Close})
    };

    appBooked = () => {
        this.initiateLms(LeadStatus.AppBooked)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.AppBooked})
    };

    booked = () => {
        this.initiateLms(LeadStatus.Booked)
        //this.setState({lmsShow: true, leadStatus: LeadStatus.Booked})
    };

    initiateLms = (status) => {
        let lmsLeadInitializationData = {};
        lmsLeadInitializationData['leadStatus'] = status;
        lmsLeadInitializationData['walkInDate'] = new Date().toString();
        lmsLeadInitializationData['returnStatus'] = '';
        lmsLeadInitializationData['returnTaskId'] = '';
        this.refs['lms'].initiateLms(lmsLeadInitializationData);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    btn: {
        fontSize: 16,
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'black',
        backgroundColor: '#a3a2a2',
        fontFamily: Strings.APP_FONT,
        borderRadius: 5,
        elevation: 8,
        margin: 10,
    }
})
export default App;

var Task = {
    "tracking": '',
    "amount": '',
    "comment": '',
    "date": '',
    "followUpType": '',
    "NewLeadState": '',
    "task_id": '',
    "task_perform": '',
    "task_type_name": '',
    "time": '',
    "Title": '',
    "leadRating": '',
    "reminderdate": '',
    "remindertime": '',
    "setReminder": '',
    "closeReason": '',
    "option_id": '',
    "returnstatus": '',
    "return_status_name": '',
    "return_task_id": '',
    "shareType": '',
    "task_type": '',
};
