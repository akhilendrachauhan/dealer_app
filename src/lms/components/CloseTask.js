import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, FlatList, TextInput} from 'react-native'
import * as  Colors from '../values/colors'
import * as  Strings from '../values/strings'
import * as  Constants from '../utils/Constants'
import * as  Styles from '../values/styles'
import * as Dimens from "../values/Dimens";
import TextInputLayout from "../granulars/TextInputLayout";
import * as Dimes from "../values/Dimens";
import RadioButton from "./RadioButton1";
import * as Tasks from "../utils/Tasks";

export default class CloseTask extends Component {

    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            SelectedId: 0,
            comment: '',
            selectedValue:''
        }
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#000",
                }}
            />
        );
    };

    render() {
        return (
            <View style={{width: '100%'}}>
                <View style={{flex: 1, padding: Dimens.dimen_20}}>
                    <Text style={Styles.bottom.title}>{this.props.data.Title}</Text>
                    {this.props.data.Heading && <Text style={Styles.bottom.sub_title}>{this.props.data.Heading}</Text>}

                    <FlatList
                        data={this.props.data.Options}
                        keyExtractor={item => item.option_id + ""}
                        renderItem={({item}) =>
                            <View>
                                <TouchableOpacity style={style.rowStyle}
                                                  onPress={() => this.onItemClick(item.option_id)}>
                                    <RadioButton
                                        animation={'bounceIn'}
                                        innerColor={Colors.radioBtnSelectedColor}
                                        outerColor={Colors.radioBtnSelectedColor}
                                        isSelected={this.state.SelectedId === item.option_id}
                                        onPress={() => this.onItemClick(item.option_id, item.value)}/>
                                    <Text style={style.textStyle}>{item.value}</Text>
                                </TouchableOpacity>

                                {this.state.SelectedId === item.option_id &&
                                <View style={{flex: 1, marginBottom: Dimens.dimen_20}}>
                                    <TextInputLayout
                                        style={{}}
                                        ref={(input) => this.commentRefLayout = input}
                                        hintColor={Colors.black_54}
                                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                        focusColor={Colors.black}
                                        errorColorMargin={Dimens.dimen_20}>
                                        <TextInput style={style.textInput}
                                                   value={this.state.SelectedId === item.option_id ? this.state.comment : ""}
                                                   ref={(input) => this.commentRef = input}
                                                   placeholder={Strings.add_comment}
                                                   editable={true}
                                                   onChangeText={(input) => {
                                                       this.setState({comment: input})
                                                       // this.commentRefLayout.setError("")
                                                   }}
                                        />
                                    </TextInputLayout>
                                </View>
                                }
                            </View>

                        }
                        extraData={this.state}
                        ItemSeparatorComponent={this.renderSeparator}
                    />
                </View>

                <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onSubmitClicked()}>
                    <Text style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>
                        {Strings.submit}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    onItemClick = (option_id, value) => {
        this.setState({SelectedId: option_id, comment: "", selectedValue: value})
    }

    onSubmitClicked = () => {
        if (this.state.SelectedId == '') {
            alert(Strings.please_select_at_least_one)
        } else {
            if (this.state.comment != '') {
                this.lmsData.closeReason = this.state.selectedValue + " | "+ this.state.comment;
            } else {
                this.lmsData.closeReason = this.state.comment
            }
            this.lmsData.option_id = this.state.SelectedId
            this.lmsData.task_type_name = "LeafNode"
            this.lmsData.NewLeadState = "Close"
            this.lmsData.task_perform = Tasks.Close;
            this.props.onCompleteCallBack(true, this.lmsData)
            this.props.closeBottomSheet(true)
        }
    }
}

const style = StyleSheet.create({
        rowStyle: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginTop: Dimes.dimen_15,
            marginBottom: Dimes.dimen_15
        },
        textStyle: {
            justifyContent: 'center',
            marginStart: Dimes.dimen_10,
            fontFamily:Strings.APP_FONT,
            fontSize: Dimes.dimen_16
        },
        textInput: {
            alignSelf: 'stretch',
            textAlign: 'center',
            color: Colors.black,
            fontFamily:Strings.APP_FONT,
            fontSize: Dimens.dimen_16,
            height: Dimens.dimen_36,
        },
    },
)
