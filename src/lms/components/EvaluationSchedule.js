import React, {Component} from 'react';
import RadioButton from '../components/RadioButton1'
import LMSParser from '../components/LMSParser'
import * as  Colors from '../values/colors'
import * as  String from '../values/strings'
import * as  Constants from '../utils/Constants'
import * as  Styles from '../values/styles'
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native'
import * as Dimes from "../values/Dimens";
import CustomerLocatiom from '../components/CustomerLocatiom'
import WorkshopNDShowroom from '../components/WorkshopNDShowroom'

class EvaluationSchedule extends Component {
    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            checked: ''
        };
    }

    render() {
        const {checked} = this.state;
        return (
            <View style={{width: '100%'}}>
                <View style={{padding: Dimes.dimen_20}}>
                    <Text style={Styles.bottom.title}>{this.props.data.Title}</Text>
                    <Text style={Styles.bottom.sub_title}>{this.props.data.Title}</Text>

                    <TouchableOpacity style={style.rowStyle} onPress={() => this.locationClicked()}>
                        <RadioButton
                            animation={'bounceIn'}
                            innerColor={Colors.radioBtnSelectedColor}
                            outerColor={Colors.radioBtnSelectedColor}
                            isSelected={this.state.checked === Constants.dealer_location}
                            onPress={() => this.locationClicked()}
                        />
                        <Text style={style.textStyle}>{String.customer_location}</Text>
                    </TouchableOpacity>
                    {this.state.checked === Constants.dealer_location &&
                    <CustomerLocatiom ref={'customerLocatiomRef'}/>}

                    <TouchableOpacity style={style.rowStyle} onPress={() => this.workshopClicked()}>
                        <RadioButton
                            animation={'bounceIn'}
                            innerColor={Colors.radioBtnSelectedColor}
                            outerColor={Colors.radioBtnSelectedColor}
                            isSelected={this.state.checked === Constants.workshop}
                            onPress={() => this.workshopClicked()}
                        />
                        <Text style={style.textStyle}>{String.dealer_workshop}</Text>
                    </TouchableOpacity>

                    {this.state.checked === Constants.workshop && <WorkshopNDShowroom ref={'workshopNDShowroomRef'}/>}

                    <TouchableOpacity style={style.rowStyle} onPress={() => this.showRoomClicked()}>
                        <RadioButton
                            animation={'bounceIn'}
                            innerColor={Colors.radioBtnSelectedColor}
                            outerColor={Colors.radioBtnSelectedColor}
                            isSelected={this.state.checked === Constants.showroom}
                            onPress={() => this.showRoomClicked()}
                        />
                        <Text style={style.textStyle}>{String.dealer_showroom}</Text>
                    </TouchableOpacity>

                    {this.state.checked === Constants.showroom && <WorkshopNDShowroom ref={'workshopNDShowroomRef'}/>}

                </View>
                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <View style={{height: Dimes.btnHeight}}>
                        <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onBtnClicked()}>
                            {/*<TouchableOpacity style={{backgroundColor : 'green',alignSelf: 'baseline' , height : 50,width:'100%',justifyContent : 'center',alignItems: 'center'}}>*/}
                            <Text
                                style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>{String.schedule_evaluation}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    locationClicked = () => {
        this.setState({checked: Constants.dealer_location})
    }

    showRoomClicked = () => {
        this.setState({checked: Constants.showroom})
    }

    workshopClicked = () => {
        this.setState({checked: Constants.workshop})
    }

    onBtnClicked = () => {
        if (this.state.checked == '') {
            alert(String.please_select_at_least_one)
        } else if (this.state.checked == Constants.dealer_location) {
            // console.log("address = " + this.refs['customerLocatiomRef'].state.address)
            if (this.refs['customerLocatiomRef'].state.address == '') {
                alert(String.please_enter_addrress)
                this.refs['customerLocatiomRef'].custAddreeRefLayout.setError(String.please_enter_addrress)
                return
            } else if (this.refs['customerLocatiomRef'].state.date == '') {
                alert(String.please_enter_date)
                this.refs['customerLocatiomRef'].dateRefLayout.setError(String.please_enter_date)
                return
            } else if (this.refs['customerLocatiomRef'].state.time == '') {
                alert(String.please_enter_time)
                this.refs['customerLocatiomRef'].timeRefLayout.setError(String.please_enter_time)
                return
            }
            this.schedulEvaluation(Constants.customer_location)
        } else if (this.state.checked == Constants.showroom) {
            if (this.refs['workshopNDShowroomRef'].state.date == '') {
                alert(String.please_enter_addrress)
                this.refs['workshopNDShowroomRef'].workShowroomDateRefLayout.setError(String.please_enter_date)
                return
            } else if (this.refs['workshopNDShowroomRef'].state.time == '') {
                alert(String.please_enter_time)
                this.refs['workshopNDShowroomRef'].workShowroomTimeRefLayout.setError(String.please_enter_time)
                return
            }

            this.schedulEvaluation(Constants.dealer_showroom)
        } else if (this.state.checked == Constants.workshop) {
            if (this.refs['workshopNDShowroomRef'].state.date == '') {
                alert(String.please_enter_addrress)
                this.refs['workshopNDShowroomRef'].workShowroomDateRefLayout.setError(String.please_enter_date)
                return
            } else if (this.refs['workshopNDShowroomRef'].state.time == '') {
                alert(String.please_enter_time)
                this.refs['workshopNDShowroomRef'].workShowroomTimeRefLayout.setError(String.please_enter_time)
                return
            }
            this.schedulEvaluation(Constants.dealer_workshop)
        }
    }

    schedulEvaluation = (inspectionAt) => {
        // console.log("Selected inspetion = " + JSON.stringify(inspectionAt))
        this.lmsData.inspectionAt = inspectionAt
        if (this.state.checked == Constants.dealer_location) {
            this.lmsData.inspectionAddress = this.refs['customerLocatiomRef'].state.address
            this.lmsData.inspectionDate = this.refs['customerLocatiomRef'].state.date
            this.lmsData.inspectionTime = this.refs['customerLocatiomRef'].state.time
            this.lmsData.inspectionComment = this.refs['customerLocatiomRef'].state.comment
        } else {
            this.lmsData.inspectionDate = this.refs['workshopNDShowroomRef'].state.date
            this.lmsData.inspectionTime = this.refs['workshopNDShowroomRef'].state.time
            this.lmsData.inspectionComment = this.refs['workshopNDShowroomRef'].state.comment
        }
        this.lmsData.NewLeadState = this.props.data.NewLeadState
        this.props.onCompleteCallBack(true, this.lmsData)
        this.props.closeBottomSheet(true)
    }

}

const style = StyleSheet.create({
        rowStyle: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginTop: Dimes.dimen_15,
            marginBottom: Dimes.dimen_15
        },
        textStyle: {
            justifyContent: 'center',
            marginStart: Dimes.dimen_10,
            fontFamily: String.APP_FONT,
            fontSize: Dimes.dimen_16
        }
    }
)

export default EvaluationSchedule