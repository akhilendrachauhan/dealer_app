import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TouchableOpacity, TextInput} from 'react-native'
import * as Colors from "../values/colors";
import * as Strings from '../values/strings'
import TextInputLayout from "../granulars/TextInputLayout";
import DateTimePicker from 'react-native-modal-datetime-picker'
import * as Constants from '../utils/Constants'
import * as Styles from '../values/styles'
import * as Dimens from '../values/Dimens'
import * as Images from '../values/Images'
let dateFormat = require('../utils/DateFormat');

export default class ConfirmTradeOut extends Component {
    constructor(props) {
        super(props)
            this.lmsData = props.lmsData
        this.state = {
            date: '',
            time: '',
            audometer: '',
            isDateTimePickerVisible: false,
            isTimePickerVisible: false
        }
    }


    render() {
        return (
            <View style={{width: '100%'}}>
                <View style={styles.container}>
                    <Text style={Styles.bottom.title}>{Strings.confirm_trade_in_detail}</Text>
                    {this.props.data.Heading && <Text style={Styles.bottom.sub_title}>{this.props.data.Heading}</Text>}


                    <View style={{paddingTop: Dimens.dimen_10, paddingBottom: Dimens.dimen_10, flexDirection: 'row'}}>

                        <View style={{flex: 1, marginEnd: Dimens.dimen_10}}>
                            <TouchableOpacity onPress={() => this.dateClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.dateRefLayout = input}
                                    hintColor={Colors.black}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                               value={this.state.date}
                                               ref={(input) => this.dateRef = input}
                                               placeholder={Strings.date}
                                               editable={false}
                                        // onChangeText={(text) => this.setState({date: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>

                        </View>
                        <View style={{flex: 1, marginStart: Dimens.dimen_10}}>
                            <TouchableOpacity onPress={() => this.timeClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.timeRefLayout = input}
                                    hintColor={Colors.black}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                               editable={false}
                                               value={this.state.time}
                                               ref={(input) => this.timeRef = input}
                                               placeholder={Strings.time}
                                               onChangeText={(text) => this.setState({time: text})}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TextInputLayout
                        style={{marginTop: Dimens.dimen_10}}
                        ref={(input) => this.audometerRefLayout = input}
                        hintColor={Colors.black}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                                   value={this.state.audometer}
                                   ref={(input) => this.audometerRef = input}
                                   placeholder={Strings.odometer_reading}
                                   onChangeText={(text) => {
                                       this.setState({audometer: text})
                                       this.audometerRefLayout.setError('')
                                   }}
                        />

                    </TextInputLayout>
                </View>
                <View style={styles.bottmBtnStyle}>
                    <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onSubmitClicked()}>
                        <Text style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>
                            {Strings.submit}
                        </Text>
                    </TouchableOpacity>
                </View>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideTimePicker}
                    mode={'time'}
                />
            </View>
        )
    }

    dateClicked = () => {
        this.showDateTimePicker()
    }
    timeClicked = () => {
        this.showTimePicked();

    }

    showDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: true});
    };

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    showTimePicked = () => {
        this.setState({isTimePickerVisible: true});
    };

    hideTimePicker = () => {
        this.setState({isTimePickerVisible: false});
    };

    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({date: dateFormat(selectedDate, Constants.dateFormatUI)})
        this.dateRefLayout.setError('')
    };

    handleTimePicked = selectedTime => {
        this.hideTimePicker();
        this.setState({time: dateFormat(selectedTime, Constants.timeFormatUI)})
        this.timeRefLayout.setError()
    };

    onSubmitClicked = () => {
        if (this.state.date == '') {
            this.dateRefLayout.setError(Strings.please_enter_date)
            return
        } else if (this.state.time == '') {
            this.timeRefLayout.setError(Strings.please_enter_time)
            return
        } else if (this.state.audometer == '') {
            this.audometerRefLayout.setError(Strings.please_enter_odometer)
            return
        }

        this.lmsData.sale_date=this.state.date+ " "+this.state.time
        this.lmsData.km_driven=this.state.audometer
        this.lmsData.NewLeadState=this.props.data.NewLeadState
        this.props.onCompleteCallBack(true,this.lmsData)
        this.props.closeBottomSheet(true)
    }

    cancelBottonmSheet = () => {
        this.props.closeBottomSheet()
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Dimens.dimen_20,
        paddingEnd: Dimens.dimen_20,
        paddingStart: Dimens.dimen_20,
        flex: 1
    },
    title: {
        fontWeight: 'bold',
        fontSize: Dimens.dimen_18,
        color: Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_15,
        marginTop: -10,
    },
    btnBackground: {
        borderRadius: Dimens.dimen_25,
        borderWidth: Dimens.dimen_1,
        padding: Dimens.dimen_10,
        borderColor: "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex: 1, height: Dimens.dimen_60,
        color: Colors.black,
        paddingBottom: Dimens.dimen_10,
    },
    iconStyle: {
        tintColor: Colors.black,
        alignSelf: 'flex-end',
        width: Dimens.dimen_22,
        height: Dimens.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontSize: Dimens.dimen_16,
        fontFamily:Strings.APP_FONT,
        height: Dimens.dimen_36,
    },

    dateTimeRowStyle: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-start', padding: Dimens.dimen_5
    },
    followUpStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        height: Dimens.dimen_60
    },
    bottomBtntext: {
        fontSize: Dimens.dimen_16,
        fontFamily:Strings.APP_FONT,
        fontWeight: 'bold'
    },
    cancelStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: Dimens.dimen_26,
        fontFamily:Strings.APP_FONT,
        height: Dimens.dimen_60,
        fontWeight: 'bold'
    },
    bottmBtnStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-end',
        marginBottom: Dimens.dimen_0,
        marginTop: Dimens.dimen_20
    }
})


