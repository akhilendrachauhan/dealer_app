import React ,{Component} from 'react'
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import * as Constants from "../utils/Constants";
import * as Strings from "../values/strings";
import * as Dimens from "../values/Dimens";
import TextInputLayout from "../granulars/TextInputLayout";
import * as Colors from "../values/colors";
import * as Images from "../values/Images";
import * as Styles from "../values/styles";
import DateTimePicker from "react-native-modal-datetime-picker";
let dateFormat = require('../utils/DateFormat');
export  default class ConfirmTradeIn extends Component{

    constructor(props) {
        super(props)
        this.lmsData = props.lmsData
        this.state = {
            date: '',
            comment: '',
            tradeInPrice: '',
            dmsEnquiryId: '',
            isDateTimePickerVisible: false,
        }
    }

    render() {
        return (
            <View style={{width: '100%'}}>
                <View style={styles.container}>
                    <Text style={Styles.bottom.title}>{Strings.confirm_trade_in_detail}</Text>
                    {this.props.data.Heading && <Text style={Styles.bottom.sub_title}>{this.props.data.Heading}</Text>}


                    <View style={{paddingTop: Dimens.dimen_10, paddingBottom: Dimens.dimen_10, flexDirection: 'row'}}>

                        <View style={{flex: 1, marginEnd: Dimens.dimen_10}}>
                            <TouchableOpacity onPress={() => this.dateClicked()}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.dateRefLayout = input}
                                    hintColor={Colors.black}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    errorColorMargin={Dimens.dimen_20}
                                    showRightDrawable={true}
                                    rightDrawable={Images.calendarIcon}>
                                    <TextInput style={styles.textInput}
                                               editable={false}
                                               value={this.state.date}
                                               ref={(input) => this.dateRef = input}
                                               placeholder={Strings.date}
                                               editable={false}
                                    />

                                </TextInputLayout>
                            </TouchableOpacity>

                        </View>
                        <View style={{flex: 1, marginStart: Dimens.dimen_10}}>
                                <TextInputLayout
                                    style={{}}
                                    ref={(input) => this.tradeInPriceRefLayout = input}
                                    hintColor={Colors.black}
                                    errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                                    focusColor={Colors.black}
                                    errorColorMargin={Dimens.dimen_20}>
                                    <TextInput style={styles.textInput}
                                               value={this.state.tradeInPrice}
                                               ref={(input) => this.tradeInPriceRef = input}
                                               placeholder={Strings.trade_in_price}
                                               onChangeText={(text) => {
                                                   this.setState({tradeInPrice: text})
                                                   this.tradeInPriceRefLayout.setError()}
                                               }

                                    />

                                </TextInputLayout>
                        </View>
                    </View>
                    <TextInputLayout
                        style={{marginTop: Dimens.dimen_10}}
                        ref={(input) => this.dmsEnquiryRefLayout = input}
                        hintColor={Colors.black}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                                   value={this.state.dmsEnquiryId}
                                   ref={(input) => this.dmsEnquiryRef = input}
                                   placeholder={Strings.dms_enquiry_id}
                                   onChangeText={(text) => {
                                       this.setState({dmsEnquiryId: text})
                                       this.dmsEnquiryRefLayout.setError('')
                                   }}
                        />

                    </TextInputLayout>

                    <TextInputLayout
                        style={{marginTop: Dimens.dimen_10}}
                        ref={(input) => this.commentRefLayout = input}
                        hintColor={Colors.black}
                        errorColor={Colors.DEFAULT_LABEL_ERROR_COLOR}
                        focusColor={Colors.black}
                        errorColorMargin={Dimens.dimen_20}>
                        <TextInput style={styles.textInput}
                                   value={this.state.comment}
                                   ref={(input) => this.commentRef = input}
                                   placeholder={Strings.add_comment}
                                   onChangeText={(text) => {
                                       this.setState({comment: text})
                                       this.commentRefLayout.setError('')
                                   }}
                        />

                    </TextInputLayout>
                </View>
                <View style={styles.bottmBtnStyle}>
                    <TouchableOpacity style={Styles.bottom.buttonStyle} onPress={() => this.onSubmitClicked()}>
                        <Text style={[Styles.bottom.bottomBtntext, {color: 'white'}]}>
                            {Strings.submit}
                        </Text>
                    </TouchableOpacity>
                </View>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={new Date()}
                />
            </View>
        )
    }

    dateClicked = () => {
        this.showDateTimePicker()
    }

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    showDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: true});
    };
    handleDatePicked = selectedDate => {
        this.hideDateTimePicker();
        this.setState({date: dateFormat(selectedDate, Constants.dateFormatUI)})
        this.dateRefLayout.setError('')
    };


    onSubmitClicked = () => {
        if (this.state.date == '') {
            this.dateRefLayout.setError(Strings.please_enter_date)
            return
        } else if (this.state.tradeInPrice == '') {
            this.tradeInPriceRefLayout.setError(Strings.please_enter_price)
            return
        } else if (this.state.dmsEnquiryId == '') {
            this.dmsEnquiryRefLayout.setError(Strings.please_enter_dms_enquiry_id)
            return
        }
        this.lmsData.comment=this.state.comment
        this.lmsData.trade_in_date=this.state.date
        this.lmsData.amount=this.state.tradeInPrice
        this.lmsData.dms_enquiry_id=this.state.dmsEnquiryId
        this.lmsData.NewLeadState=this.props.data.NewLeadState
        this.props.onCompleteCallBack(true,this.lmsData)
        this.props.closeBottomSheet(true)
    }

    cancelBottonmSheet = () => {
        this.props.closeBottomSheet()
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Dimens.dimen_20,
        paddingEnd: Dimens.dimen_20,
        paddingStart: Dimens.dimen_20,
        flex: 1
    },
    title: {
        fontWeight: 'bold',
        fontSize: Dimens.dimen_18,
        color: Colors.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_15,
        marginTop: -10,
    },
    btnBackground: {
        borderRadius: Dimens.dimen_25,
        borderWidth: Dimens.dimen_1,
        padding: Dimens.dimen_10,
        borderColor: "rgba(0,0,0,0.33)"
    },
    textInputStyle: {
        flex: 1, height: Dimens.dimen_60,
        color: Colors.black,
        paddingBottom: Dimens.dimen_10,
    },
    iconStyle: {
        tintColor: Colors.black,
        alignSelf: 'flex-end',
        width: Dimens.dimen_22,
        height: Dimens.dimen_22
    },
    textInput: {
        alignSelf: 'stretch',
        textAlign: 'center',
        color: Colors.black,
        fontSize: Dimens.dimen_16,
        fontFamily:Strings.APP_FONT,
        height: Dimens.dimen_36,
    },

    dateTimeRowStyle: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-start', padding: Dimens.dimen_5
    },
    followUpStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        height: Dimens.dimen_60
    },
    bottomBtntext: {
        fontSize: Dimens.dimen_16,
        fontFamily:Strings.APP_FONT,
        fontWeight: 'bold'
    },
    cancelStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: Dimens.dimen_26,
        fontFamily:Strings.APP_FONT,
        height: Dimens.dimen_60,
        fontWeight: 'bold'
    },
    bottmBtnStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-end',
        marginBottom: Dimens.dimen_0,
        marginTop: Dimens.dimen_20
    }
})
