import React, {Component} from 'react'
import * as Color from '../values/colors'
import * as Dimens from '../values/Dimens'
import * as Images from '../values/Images'
import * as Constants from '../utils/Constants'
import {View, Text, FlatList, Image, StyleSheet, TouchableOpacity} from 'react-native'
import * as Strings from "../values/strings";

class Question extends Component {
    constructor(props) {
        super(props);
        this.lmsData = props.lmsData
    }

    render() {
        return (
            <View style={{width: '100%', paddingHorizontal: Dimens.dimen_20, paddingBottom: Dimens.dimen_20}}>
                <View style={styles.container}>
                    <Text style={styles.title}>{this.props.data.Title}</Text>
                    <FlatList
                        data={this.props.data.Options}
                        renderItem={({item}) => <Item item={item} title={item.value}
                                                      callBack={() => this.props.callBackMethod(item.action_task_id)}
                                                      id={item.action_task_id}/>}
                        keyExtractor={item => item.option_id + ""}
                    />
                </View>
            </View>
        )
    }

    callMethod = () => {
        this.props.closeBottomSheet()
    }
    rowItemClick = (taskid) => {
        // alert("clicekd" + taskid)
    }
}

function Item({item, title, callBack, id}) {
    return (
        <View style={styles.item}>
            <TouchableOpacity onPress={() => callBack(id)}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        {item.icon_URL && item.icon_URL != '' && Constants.ICON_MAP[item.icon_URL] && <Image
                            source={Constants.ICON_MAP[item.icon_URL]}
                            style={{
                                width: Dimens.dimen_20,
                                height: Dimens.dimen_20,
                                marginRight: Dimens.dimen_20
                            }}/>}
                        <Text style={{color: Color.black_85, fontSize: Dimens.dimen_14, fontFamily:Strings.APP_FONT,}}>{title}</Text>
                    </View>
                    <Image source={Images.right_arrow}
                           style={{width: Dimens.dimen_14, height: Dimens.dimen_14, tintColor: Color.black_40}}/>
                </View>
                <View style={{
                    backgroundColor: Color.lineColor,
                    height: Dimens.dimen_1,
                    marginTop: Dimens.dimen_15
                }}/>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: Dimens.dimen_0,
    },
    imageStyle: {
        marginTop: Dimens.dimen_10,
        marginEnd: Dimens.dimen_10,
        width: Dimens.dimen_20,
        height: Dimens.dimen_20,
        alignSelf: 'flex-end'
    },
    title: {
        fontWeight:'bold',
        fontSize: Dimens.dimen18,
        color: Color.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_20
    },
    sub_title: {
        fontSize: Dimens.dimen_14,
        color: Color.black,
        fontFamily:Strings.APP_FONT,
        marginBottom: Dimens.dimen_10,
        marginTop: Dimens.dimen_4,
    },
    item: {
        paddingBottom: Dimens.dimen_10,
        paddingTop: Dimens.dimen_10,
    },
})
export default Question