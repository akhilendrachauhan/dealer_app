import React, { Component } from "react";
import PropsTypes from 'prop-types'
import { Image, Platform, View, StyleSheet } from "react-native";
import ActionBarWrapper from '../granulars/ActionBarWrapper'


class BaseComponent extends Component {

  constructor(props) {

    super(props)

    this.actionBarProps = {

      values: { title: (this.props.title != null) ? this.props.title : "" },
      rightIcons: [],
      styleAttr: {
        leftIconImage: require("../assets/drawable/ic_back.png"),
        disableShadows: true
      },
      actions: {
        onLeftPress: this.baseClassGoback
      }
    }

  }

  baseClassGoback = () => {

    if (this.props.navigation != null) {

      this.props.navigation.goBack()

    } else {

      console.error("Navigation Bar Object null")
    }
  }

  render() {

    const actionBar = (this.props.actionBarProps == null) ? this.actionBarProps : this.props.actionBarProps
    if (this.props.title != null)
      actionBar.values.title = this.props.title


    return (

      <View style={[this.props.style]}>

        {(this.props.bgImage != null) && <Image style={styles.background_style}
          source={this.props.bgImage} />}

        <ActionBarWrapper
          values={actionBar.values}
          actions={actionBar.actions}
          iconMap={actionBar.rightIcons}
          styleAttributes={actionBar.styleAttr}
        />
        {this.props.children}

      </View>
    )
  }
}

BaseComponent.PropsTypes = {

  actionBarProps: PropsTypes.object,
  bgImage: PropsTypes.require,
  title: PropsTypes.string
}

const styles = StyleSheet.create({

  background_style: {

    position: 'absolute',
    width: '100%',
    height: '100%'
  }

})

export default BaseComponent;
