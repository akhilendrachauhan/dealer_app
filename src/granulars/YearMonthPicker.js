import React, {Component} from 'react';
import {View, Text, TouchableOpacity,TouchableWithoutFeedback,Keyboard, Platform} from 'react-native';
import {Colors, Strings} from "../values";
import {WheelPicker} from "react-native-wheel-picker-android";
import {Utility} from "../util";

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
export default class YearMonthPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedMonth: 0,
            selectedYear: 0,
            years: [],
            months: [],
            startDate: props.startDate ? props.startDate : new Date(2000, 2, 1),
            endDate: props.endDate ? props.endDate : new Date()
        }
    }

    componentDidMount() {
        let startDate = this.state.startDate;
        let endDate = this.state.endDate;
        let years = [];
        for (let i = startDate.getFullYear(); i <= endDate.getFullYear(); i++) {
            years.push(('' + i));
        }
        let monthsList = [];
        for (let i = startDate.getMonth(); i < 12; i++) {
            monthsList.push(months[i])
        }
        this.setState({years: years, months: monthsList, startDate: startDate, endDate: endDate, selectedYear : years.length-1});
        Utility.log(years);
        Utility.log(monthsList)
        //this.onItemSelected(0,0);
    }

    onItemSelected = (selectedItem, index) => {
        if (index === 0) {
            let monthsList = [];
            if(this.state.years.length > 1){
                if (selectedItem === 0) {
                    for (let i = this.state.startDate.getMonth(); i < 12; i++) {
                        monthsList.push(months[i])
                    }
                } else if (selectedItem === (this.state.years.length - 1)) {
                    for (let i = 0; i <= this.state.endDate.getMonth(); i++) {
                        monthsList.push(months[i])
                    }
                } else {
                    monthsList = months;
                }
            }
            else{
                for (let i = 0; i <= this.state.endDate.getMonth(); i++) {
                    monthsList.push(months[i])
                }
            }
            this.setState({selectedYear: selectedItem, selectedMonth: 0, months: monthsList})
        } else if (index === 1) {
            this.setState({selectedMonth: selectedItem})
        }
    };

    render() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.BLACK_50
            }}>
                <View style={{backgroundColor: Colors.WHITE, width: '100%'}}>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{paddingVertical: 10}}>
                            {this.props.showMonth ? this.state.months[this.state.selectedMonth]+"," : ''} {this.state.years[this.state.selectedYear]}
                        </Text>
                        <View style={{height: 3, backgroundColor: Colors.PRIMARY, width: '100%', marginBottom: 20}}/>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: Colors.WHITE
                    }}>
                        <View style={{flex: 1, alignItems: 'center'}}>
                            <WheelPicker
                                indicatorWidth={2}
                                selectedItemTextSize={18}
                                itemTextSize={16}
                                indicatorColor={Colors.PRIMARY}
                                selectedItem={this.state.selectedYear}
                                data={this.state.years}
                                onItemSelected={(selectedItem) => this.onItemSelected(selectedItem, 0)}/>
                        </View>
                        { this.props.showMonth ? <View style={{flex: 1, alignItems: 'center'}}>
                            <WheelPicker
                                indicatorWidth={2}
                                selectedItemTextSize={18}
                                itemTextSize={16}
                                indicatorColor={Colors.PRIMARY}
                                selectedItem={this.state.selectedMonth}
                                data={this.state.months}
                                onItemSelected={(selectedItem) => this.onItemSelected(selectedItem, 1)}/>
                        </View> : <View></View>}

                    </View>
                    <View style={{height: 1, backgroundColor: Colors.PRIMARY, width: '100%', marginTop: 20}}/>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: Colors.WHITE
                    }}>
                        <TouchableOpacity onPress={this.props.onDateCancel} style={{flex: 1, alignItems: 'center'}}>
                            <Text style={{paddingVertical: 12, fontSize: 16}}>{Strings.CANCEL}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.props.onDateSelected(this.state.years[this.state.selectedYear], months.indexOf(this.state.months[this.state.selectedMonth]),months[months.indexOf(this.state.months[this.state.selectedMonth])])} style={{flex: 1, alignItems: 'center'}}>
                            <Text style={{paddingVertical: 12, fontSize: 16}}>OK</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </View>
        )
    }
}