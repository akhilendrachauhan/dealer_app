import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native'
import { Utility, Constants } from '../util'
import { Colors, Strings, Dimens } from '../values'
import * as ImageAssets from '../assets/ImageAssets'
import DotView from './DotView'

export default class StockOptionCard extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let item = this.props.item
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                disabled={this.props.isCarDetail ? true : false}
                onPress={() => this.props.onStockItemPress(item)}>

                <View style={this.props.isCarDetail ? Styles.containerItem1 : Styles.containerItem}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View>
                            <View style={Styles.imageContainer}>
                                <Image style={Styles.image}
                                    source={item.car_profile_image == '' ? ImageAssets.icon_no_image : { uri: item.car_profile_image }} />

                                {item.car_status == Constants.STOCK_CAR_STATUS.CAR_SOLD &&
                                    <View style={Styles.soldContainer}>
                                        <Text numberOfLines={1} style={Styles.soldText}>{Strings.SOLD}</Text>
                                    </View>
                                }
                            </View>

                            {item.isclassified && item.isclassified == 1 &&
                                <View style={[Styles.containerbadge, { backgroundColor: Colors.LEAD_BADGE_BLUE }]}>
                                    <Text numberOfLines={1} style={Styles.badgeText}>{Strings.CLASSIFIED}</Text>
                                </View>
                            }
                            {/* {item.isoffer &&
                                <View style={[Styles.containerbadge, { backgroundColor: Colors.LEAD_BADGE_YELLOW }]}>
                                    <Text numberOfLines={1} style={Styles.badgeText}>{Strings.OFFER}</Text>
                                </View>
                            }
                            {item.isbooked &&
                                <View style={[Styles.containerbadge, { backgroundColor: Colors.LEAD_BADGE_GREEN }]}>
                                    <Text numberOfLines={1} style={Styles.badgeText}>{Strings.BOOKED}</Text>
                                </View>
                            } */}
                        </View>

                        <View style={Styles.detailContainer}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text numberOfLines={2} style={Styles.headText}>{item.make + ' ' + item.modelVersion}</Text>

                                {this.getIcons(item)}
                            </View>

                            <View>
                                <Text numberOfLines={1} style={Styles.boldText}>{Utility.formatCurrency(item.car_price)}</Text>

                                {this.props.isCarDetail ?
                                    <Text numberOfLines={1} style={Styles.descText}>{Strings.ON_ROAD_PRICE} : {Utility.formatCurrency(item.car_price)}</Text>
                                    :
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: Dimens.margin_xx_small }}>
                                        <Text style={Styles.smallText}>{item.offer_count > 0 ? item.offer_count + ' ' + Strings.OFFERS : Strings.NO_OFFER}</Text>
                                        {item.leadCount > 0 && <DotView />}
                                        {item.leadCount > 0 &&
                                            <Text style={Styles.smallText}>{item.leadCount + ' ' + Strings.LEADS.toLowerCase()}</Text>
                                        }
                                    </View>
                                }
                            </View>
                        </View>
                    </View>

                    <View style={Styles.divider} />

                    {this.props.isCarDetail ?
                        <View style={{ flexDirection: 'row', alignSelf: 'flex-start', alignItems: 'center', marginTop: Dimens.margin_xx_small }}>
                            <Text style={Styles.smallText}>{item.offer_count > 0 ? item.offer_count + ' ' + Strings.OFFERS : Strings.NO_OFFER}</Text>
                            {item.leadCount > 0 && <DotView />}
                            {item.leadCount > 0 &&
                                <Text style={Styles.smallText}>{item.leadCount + ' ' + Strings.LEADS.toLowerCase()}</Text>
                            }
                        </View>
                        :
                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={Styles.descText}>{item.km_driven == '' ? Strings.NA : item.km_driven + ' ' + Strings.KM}</Text>
                            <DotView />
                            <Text style={Styles.descText}>{item.make_year || Strings.NA}</Text>
                            <DotView />
                            <Text style={Styles.descText}>{item.fuel_type || Strings.NA}</Text>
                            <DotView />
                            <Text style={Styles.descText}>{item.reg_no || Strings.NA}</Text>
                        </View>
                    }
                </View>
            </TouchableOpacity>
        )
    }

    getIcons = (item) => {
        return (
            <View style={{ marginLeft: Dimens.margin_normal }}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                        if (item.is_favourite == 1)
                            this.props.updateFavourite(0, item.id) //0 for remove from Favourite
                        else
                            this.props.updateFavourite(1, item.id) //1 for add Favourite
                    }}>

                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }}
                        source={item.is_favourite == 1 ? ImageAssets.icon_heart_fill : ImageAssets.icon_heart_unfill} />
                </TouchableOpacity>

                {/* <Image style={{ width: 32, height: 24, resizeMode: 'contain' }}
                    source={ImageAssets.icon_cd_logo} /> */}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    containerItem: {
        overflow: 'hidden',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_xx_large,
        borderRadius: Dimens.border_radius,
        width: Dimensions.get('window').width - 40
    },
    containerItem1: { // bottom Radius 0
        overflow: 'hidden',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_xx_large,
        borderTopEndRadius: Dimens.border_radius,
        borderTopStartRadius: Dimens.border_radius,
        width: Dimensions.get('window').width - 40
    },
    detailContainer: {
        flex: 1,
        height: 88,
        justifyContent: 'space-between',
        marginLeft: Dimens.margin_xx_large,
    },
    imageContainer: {
        width: 131,
        height: 88
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    },
    soldContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLACK_54
    },
    soldText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.WHITE,
        fontFamily: Strings.APP_FONT
    },
    containerbadge: {
        width: '100%',
        position: 'absolute',
        top: -5, left: -60,
        transform: [{ rotate: '315deg' }]
    },
    badgeText: {
        color: Colors.WHITE,
        textAlign: 'center',
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_xx_small,
    },
    headText: {
        flex: 1,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    boldText: {
        color: Colors.BLACK_87,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT
    },
    divider: {
        height: 0.9, width: '100%',
        backgroundColor: Colors.DIVIDER,
        marginVertical: Dimens.padding_large
    }
})