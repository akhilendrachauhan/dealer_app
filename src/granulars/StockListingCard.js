import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity,BackHandler } from 'react-native'
import { Utility, Constants } from '../util'
import { Colors, Strings, Dimens } from '../values'
import {
    more_black_icon, icon_classified, icon_featured, icon_trustmark,
    icon_inspected, icon_no_image, icon_heart_fill, icon_heart_unfill,icon_bumped_up
} from './../assets/ImageAssets'
import DotView from './DotView'
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    MenuProvider
} from 'react-native-popup-menu'

export default class StockListingCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            opened : false,
        }
    }
    onTriggerPress = () =>{
        this.setState({ opened: true });
    }
    
    onBackdropPress = () => {
        this.setState({ opened: false });
    }
    onOptionSelect = (value) =>{
        this.setState({ opened: false });
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }
    
    handleBackPress = () => {
        if(this.state.opened){
            this.setState({opened : false})
            return true
        }
        return false
        
    }
    
    render() {
        let item = this.props.item
        return (
            <View style={styles.container}>

                <TouchableOpacity
                    style={{ flexDirection: 'row', flex: 1 }}
                    onPress={() => this.props.onListItemClick(item)}
                    activeOpacity={0.8}>

                    <View>
                        <Image style={styles.image}
                            source={item.car_profile_image == "" ? icon_no_image : { uri: item.car_profile_image }} />

                        {!this.props.isShowFavourites && item.leadCount > 0 &&
                            <View style={{ position: 'absolute', top: 0, right: 0, paddingHorizontal: 5, paddingVertical: 2, backgroundColor: Colors.BLACK_12 }}>
                                <Text style={{ fontFamily: Strings.APP_FONT, fontSize: Dimens.text_small, color: Colors.WHITE_87 }}>{item.leadCount > 1 ? item.leadCount + ' ' + Strings.LEADS.toLowerCase() : item.leadCount + ' ' + Strings.LEAD.toLowerCase()}</Text>
                            </View>
                        }

                        {/* <View style={{ height: 20, width: '100%', backgroundColor: Colors.WHITE_70, position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 90, height: 20, resizeMode: 'contain' }}
                                source={icon_trustmark} />
                        </View> */}
                        { item.certification_status == Constants.STOCK_INSPECTED ? <View style={{ height: 20, width: '100%', backgroundColor: Colors.WHITE_70, position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Image style={{ width: 90, height: 20, resizeMode: 'contain' }}
                                source={icon_inspected} /> */}
                            <Text style={{fontFamily:Strings.APP_FONT,fontSize: Dimens.text_normal}}>{Strings.INSPECTED.toUpperCase()}</Text>
                        </View>: <View></View>}
                        {/* { Constants.APP_TYPE == Constants.PHILIPPINES && item.is_bump_enable == 1 ? <View style={{ height: 20, width: '100%', backgroundColor: Colors.WHITE_70, position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            
                            <Text style={{fontFamily:Strings.APP_FONT,fontSize: Dimens.text_normal}}>{Strings.BUMPED_UP.toUpperCase()}</Text>
                        </View> : <View></View>} */}
                    </View>

                    <View style={styles.detailContainer}>
                        <Text numberOfLines={1} style={styles.headText}>{item.make + ' ' + item.modelVersion || Strings.NA}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text numberOfLines={1} style={styles.smallText}>{item.reg_no || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.fuel_type || Strings.NA}</Text>
                        </View>
                        <Text numberOfLines={1} style={[styles.headText, { color: Colors.BLACK_87 }]}>{Utility.displayCurrency(item.car_price) || Strings.NA}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.smallText}>{ (item.km_driven ? Utility.formatNumber(item.km_driven) : "0" )+ ' ' + Strings.KM || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.make_year || Strings.NA}</Text>
                            {/* <DotView />
                            <Text style={styles.smallText}>{item.fuel_type || Strings.NA}</Text> */}
                        </View>
                        
                    </View>
                </TouchableOpacity>

                {!this.props.isShowFavourites ?
                    this.props.activeStockShow ?
                        <Menu opened={this.state.opened}
                              onBackdropPress={() => this.onBackdropPress()}
                              >
                            <MenuTrigger
                            onPress={() => this.onTriggerPress()}>
                                <Image source={more_black_icon}
                                    style={{ resizeMode: 'contain', height: 20, width: 20 }} />
                            </MenuTrigger>
                            <MenuOptions style={{ padding: 10 }}>
                                <MenuOption onSelect={() =>{ 
                                    this.props.clickOnEdit(item)
                                    this.onBackdropPress()
                                    }
                                    } >
                                    <Text style={styles.menuText}>{Strings.EDIT}</Text>
                                </MenuOption>
                                {/* <MenuOption onSelect={() => this.props.clickOnPostFacebook()} >
                                    <Text style={styles.menuText}>{Strings.POST_TO_FACEBOOK}</Text>
                                </MenuOption> */}
                                
                                
                                {/* <MenuOption onSelect={() => { 
                                    this.props.clickOnMore(item)
                                    this.onBackdropPress()
                                    }
                                } >
                                    <Text style={[styles.menuText, { marginBottom: 0 }]}>{Strings.MORE}</Text>
                                </MenuOption> */}
                                <MenuOption onSelect={() => { 
                                    //this.props.clickOnMore(item)
                                    if (item && item.isclassified == Constants.STOCK_CLASSIFIED)
                                        this.props.setStockPremium(item, 'false', '', null);
                                    else
                                        this.props.setStockPremium(item, 'true', '', null);
                                    this.onBackdropPress()
                                    }
                                } >
                                    <Text style={[styles.menuText]}>{item ? (item.isclassified == Constants.STOCK_CLASSIFIED ? Strings.REMOVE_FROM_CLASSIFIED : Strings.ADD_TO_CLASSIFIED) : ''}</Text>
                                </MenuOption>
                                <MenuOption onSelect={() => { 
                                    if (item && item.ispremium == Constants.STOCK_PREMIUM)
                                        this.props.setStockPremium(item, '', 'false', null);
                                    else
                                        this.props.setStockPremium(item, '', 'true', null);
                                    this.onBackdropPress()
                                    }
                                } >
                                    <Text style={[styles.menuText]}>{item ? (item.ispremium == Constants.STOCK_PREMIUM ? Strings.REMOVE_FEATURED : Strings.ADD_TO_FEATURED) : ''}</Text>
                                </MenuOption>
                                <MenuOption disabled = {item.isclassified && item.isclassified == Constants.STOCK_CLASSIFIED ? false : true} onSelect={() => { 
                                    this.props.addToBumpup(item)
                                    this.onBackdropPress()
                                    }
                                } >
                                    <Text style={item.isclassified && item.isclassified == Constants.STOCK_CLASSIFIED ? styles.menuText : styles.disabledMenuText}>{Strings.ADD_TO_BUMPUP}</Text>
                                </MenuOption>
                                <MenuOption disabled = {item.show_weburl ? false : true} onSelect={() => { 
                                    this.props.clickOnSeeOnWeb(item)
                                    this.onBackdropPress()
                                    }} >
                                    <Text style={item.show_weburl ? styles.menuText : styles.disabledMenuText}>{Strings.WEB_URL}</Text>
                                </MenuOption>
                                <MenuOption onSelect={() =>{ 
                                    this.props.clickOnRemoveStock(item)
                                    this.onBackdropPress()
                                }} >
                                    <Text style={styles.menuText}>{Strings.REMOVE_STOCK}</Text>
                                </MenuOption>
                                {/* <MenuOption onSelect={() => this.props.clickOnWhatsapp()} >
                                    <Text style={styles.menuText}>{Strings.WHATSAPP}</Text>
                                </MenuOption> */}
                                
                            </MenuOptions>
                        </Menu>
                        :
                        <Menu opened={this.state.opened}
                              onBackdropPress={() => this.onBackdropPress()}
                              >
                            <MenuTrigger
                            onPress={() => this.onTriggerPress()}>
                                <Image source={more_black_icon}
                                    style={{ resizeMode: 'center', height: 24, width: 24 }} />
                            </MenuTrigger>
                            <MenuOptions style={{ padding: 20 }}>
                                <MenuOption onSelect={() =>{
                                     this.props.clickOnAddToStock(item)
                                     this.onBackdropPress()
                                     } 
                                     
                                     }>
                                    <Text style={styles.menuText}>{Strings.ADD_TO_STOCK}</Text>
                                </MenuOption>
                                <MenuOption onSelect={() =>{ 
                                    this.props.clickOnEdit(item)
                                    this.onBackdropPress()
                                }
                                
                                } >
                                    <Text style={[styles.menuText, { marginBottom: 5 }]}>{Strings.EDIT}</Text>
                                </MenuOption>
                            </MenuOptions>
                        </Menu>
                    :
                    this.getHeartIcon(item)
                }

                {item.isclassified == Constants.STOCK_CLASSIFIED &&
                    <View style={{ position: 'absolute', top: 0, left: 0 }}>
                        <Image style={{ width: 50, height: 50, resizeMode: 'contain' }}
                            source={icon_classified} />
                    </View>
                }
                {item.ispremium == Constants.STOCK_PREMIUM && <View style={{ position: 'absolute', top: 14, left: 20 }}>
                    <Image style={{ width: 30, height: 30, resizeMode: 'contain' }}
                        source={icon_featured} />
                </View>
                }

                { item.is_bump_enable == Constants.STOCK_BUMPUP_ENABLE ?
                    <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
                        <Image style={{ width: 50, height: 50, resizeMode: 'contain' }}
                            source={icon_bumped_up} />
                    </View>: <View></View>
                }
                
            </View>
        )
    }

    getHeartIcon = (item) => {
        return (
            <View style={{ marginLeft: 5 }}>
                <TouchableOpacity
                    onPress={() => this.props.onListItemClick(item)}
                    activeOpacity={0.8}>

                    <Image style={{ width: 24, height: 24, resizeMode: 'contain' }}
                        source={this.props.favouriteItems.includes(item.id) ? icon_heart_fill : icon_heart_unfill} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        justifyContent: 'space-between',
        padding: Dimens.padding_xx_large,
        marginBottom: Dimens.margin_normal,
        borderRadius: Dimens.border_radius
    },
    detailContainer: {
        flex: 1,
        height: 88,
        justifyContent: 'space-between',
        marginLeft: 15,
    },
    image: {
        width: 131,
        height: 88,
        resizeMode: 'cover'
    },
    headText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    menuText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        marginBottom: 10,
        fontFamily: Strings.APP_FONT
    },
    disabledMenuText : {
        color: Colors.BLACK_25,
        fontSize: Dimens.text_normal,
        marginBottom: 10,
        fontFamily: Strings.APP_FONT
    }
})