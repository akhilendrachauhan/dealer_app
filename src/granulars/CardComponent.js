import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Colors, Strings, Dimens } from '../values';

export default class CardComponent extends Component {

    render() {
        let { id, label, value, text_color, indicator_color } = this.props.data;
        let textsize = this.props.textSize;
        let containerHeight = this.props.containerHeight;
        let hideIndicator = this.props.hideIndicator;
        let labelSize = this.props.labelSize;
        return (
            <View style={{ height: containerHeight, width: '100%' }}>
                <View style={[styles.container]}>
                    <Text style={[styles.textCount, { color: text_color, fontSize: textsize }]}>{value || 0}</Text>
                    <Text style={[styles.textLabel, { fontSize: labelSize }]}>{label}</Text>
                </View>
                {hideIndicator ? null : <View style={{ height: 4, backgroundColor: indicator_color }} />}
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    textLabel: {
        fontSize: Dimens.text_normal,
        color: Colors.BLACK_54,
        alignSelf: 'stretch',
        textAlign: 'center',
        fontFamily: Strings.APP_FONT
    },
    textCount: {
        fontWeight: '500',
        fontFamily: Strings.APP_FONT
    }
});