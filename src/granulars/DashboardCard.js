import React, {Component} from 'react';
import {View, FlatList, StyleSheet, TouchableOpacity, Image} from 'react-native';
import CardHeader from './DashboardCardHeader';
import CardComponent from './CardComponent';
import {Colors, Dimens} from '../values';
import {icon_hz_dotted_line} from './../assets/ImageAssets'

class DashboardCard extends Component {
    constructor(props) {
        super(props);
        this.extraData = false;
    }

    renderItem(item, textSize) {
        item['text_color'] = Colors.BLACK_85;
        return (
            <TouchableOpacity activeOpacity={0.9} style={{flex: 1}}
                              onPress={() => this.props.onCountPress(item)}>
                <CardComponent
                    data={item}
                    textSize={textSize}
                    containerHeight={94}
                    hideIndicator={false}
                    labelSize={12}
                />
            </TouchableOpacity>
        )

    }

    renderChild(childData) {
        this.extraData = !this.extraData;
        if (childData.length > 0) {
            return (
                <View style={{height: 95, width: '100%'}}>
                    <Image source={icon_hz_dotted_line}
                           style={{height: 1, width: '100%', marginHorizontal: Dimens.margin_large}}/>
                    <FlatList
                        extraData={this.extraData}
                        numColumns={childData.length}
                        data={childData}
                        keyExtractor={(item, index) => {
                            return item.itemId
                        }}
                        renderItem={({item}) => this.renderItem(item, Dimens.text_28)}

                    />
                </View>
            )
        }
        return null
    }

    render() {
        let {title, card_details} = this.props.data;
        let countTextSize = card_details['child'].length > 0 ? 65 : 85;
        let hideIndicator = card_details['child'].length > 0;
        return (
            <View style={styles.container}>
                <CardHeader title={title}/>
                <View style={{flex: 1,backgroundColor: Colors.WHITE}}>
                    <TouchableOpacity activeOpacity={0.9} style={{flex: 1}}
                                      onPress={() => this.props.onCountPress(card_details['parent'])}>
                        <CardComponent
                            data={card_details['parent']}
                            textSize={countTextSize}
                            containerHeight={hideIndicator ? 142 : 236}
                            hideIndicator={hideIndicator}
                            labelSize={14}/>
                    </TouchableOpacity>
                    {this.renderChild(card_details['child'])}

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 288
    }
});

export default DashboardCard;