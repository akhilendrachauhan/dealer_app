import React,{Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Colors, Strings} from '../values';

export default class CardHeader extends Component{

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>{this.props.title ? this.props.title : ''}</Text>
                </View>
                <View style={{height:1, backgroundColor:Colors.BLACK_11}}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height:52,
        backgroundColor:Colors.CARD_HEADER_BG
    },
    textContainer:{
        alignItems:'center',
        justifyContent:'center',
        height:51
    },
    text:{
        fontSize: 16,
        fontWeight: '500',
        color:Colors.BLACK_85,
        fontFamily: Strings.APP_FONT
    }
});