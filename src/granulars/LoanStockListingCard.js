import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Utility, Constants } from '../util'
import { Colors, Strings, Dimens } from '../values'
import {
    icon_classified, icon_featured,icon_no_image
} from './../assets/ImageAssets'
import DotView from './DotView'
export default class LoanStockListingCard extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let item = this.props.item
        return (
            <View style={styles.container}>

                <TouchableOpacity
                    style={{ flexDirection: 'row', flex: 1 }}
                    onPress={() => this.props.onListItemClick(item)}
                    activeOpacity={0.8}>

                    <View>
                        <Image style={styles.image}
                            source={item.car_profile_image == "" ? icon_no_image : { uri: item.car_profile_image }} />
                        { item.certification_status == Constants.STOCK_INSPECTED && <View style={{ height: 20, width: '100%', backgroundColor: Colors.WHITE_70, position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        {/* <Image style={{ width: 90, height: 20, resizeMode: 'contain' }}
                            source={icon_inspected} /> */}
                        <Text style={{fontFamily:Strings.APP_FONT,fontSize: Dimens.text_normal}}>{Strings.INSPECTED.toUpperCase()}</Text>
                        </View>}
                    </View>

                    <View style={styles.detailContainer}>
                        <Text numberOfLines={1} style={styles.headText}>{item.make + ' ' + item.modelVersion || Strings.NA}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text numberOfLines={1} style={styles.smallText}>{item.reg_no || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.transmission || Strings.NA}</Text>
                        </View>
                        <Text numberOfLines={1} style={[styles.headText, { color: Colors.BLACK_87 }]}>{Utility.formatCurrency(item.car_price) || Strings.NA}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.smallText}>{item.km_driven + ' ' + Strings.KM || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.make_year || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.fuel_type || Strings.NA}</Text>
                        </View>
                    </View>
                </TouchableOpacity>


                {item.isclassified == Constants.STOCK_CLASSIFIED &&
                    <View style={{ position: 'absolute', top: 0, left: 0 }}>
                        <Image style={{ width: 50, height: 50, resizeMode: 'contain' }}
                            source={icon_classified} />
                    </View>
                }
                {item.ispremium == Constants.STOCK_PREMIUM && <View style={{ position: 'absolute', top: 14, left: 20 }}>
                    <Image style={{ width: 30, height: 30, resizeMode: 'contain' }}
                        source={icon_featured} />
                </View>
                }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        justifyContent: 'space-between',
        padding: Dimens.padding_xx_large,
        marginBottom: Dimens.margin_normal,
        borderRadius: Dimens.border_radius
    },
    detailContainer: {
        flex: 1,
        height: 88,
        justifyContent: 'space-between',
        marginLeft: 15,
    },
    image: {
        width: 131,
        height: 88,
        resizeMode: 'cover'
    },
    headText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    menuText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        marginBottom: 10,
        fontFamily: Strings.APP_FONT
    }
})