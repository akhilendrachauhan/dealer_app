/**
 * Read About Action bar here:
 *      https://github.com/Osedea/react-native-action-bar
 **/
import {_ActionBarStyle} from "../values/Styles";
import {Colors, Strings} from "../values";
import ActionBar from 'react-native-action-bar';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, View} from 'react-native';
import * as Utility from "../util/Utility";

let self = undefined;

/**
 * Author: Shashank
 *
 * Description:
 *      A common action bar for all the screens. The props sent to the component can be gracefully
 *      skipped. It uses default values for all styling, icons, and even the TradeInExample Title.
 *      The parameters are divided in 4 categories, namely :
 *          values              :   String values of title and Subtitle. (yet to implement Subtitle)
 *          iconMap             :   Array of icons, along with corresponding callbacks, to be rendered on Right hand side
 *          styleAttributes,    :   ActionBar styling attributes. StyleAttributes also
 *                                  include the option to add LeftImageIcon.
 *          actions             :   Callback events for titlePress and leftIcon pressed events
 *
 *      Prop: values            {title, subtitle}
 *      Prop: iconMap           [{image, onPress}] array
 *      Prop: styleAttributes   {}
 *      Prop: actions           {onLeftPress, onTitlePress}
 */
export default class ActionBarWrapper extends Component {

    updateValues = () => {
        let def_style_attr = {
            containerStyle: _ActionBarStyle.content,
            titleContainerStyle: _ActionBarStyle.titleContainer,
            titleStyle: _ActionBarStyle.title,
            subtitleStyle: _ActionBarStyle.subtitle,
            iconImageStyle: _ActionBarStyle.icon,
            leftIconImageStyle : _ActionBarStyle.iconBack,
            disableShadows: false,
            tintColor: Colors.PRIMARY_DARK,
            badgeColor: Colors.DASHBOARD_CARD_TEXT,
            badgeTextColor: Colors.WHITE,
            leftZoneContentContainerStyle: _ActionBarStyle.leftZoneContentContainerStyle
        };

        let def_values = {
            title: "Action Bar",
            subtitle: undefined
        };

        this.style_attr = {...def_style_attr, ...this.props.styleAttributes};
        this.new_values = {...def_values, ...this.props.values};

        this.new_actions = {
            onLeftPress: () => {

                if (this.props.actions && typeof this.props.actions.onLeftPress === 'function'){

                    //alert(this.props.actions.onLeftPress)
                    this.props.actions.onLeftPress();
                }
                else{

                    //alert("Test Else")
                    Utility.log("CB not handled !");
                } 
            },
            onTitlePress: () => {
                if (this.props.actions && typeof this.props.actions.onTitlePress === 'function')
                    this.props.actions.onTitlePress();
                else Utility.log("CB not handled !");
            },
            onRightTextPress: () => {
                if (this.props.actions && typeof this.props.actions.onRightTextPress === 'function')
                    this.props.actions.onRightTextPress();
                else Utility.log("CB not handled !");
            }
        };
    };

    checkInternetConnection = () => {
        Utility.getNetInfo().then(isConnected => {
            this.state.noInternet = !isConnected;
        });
    };

    constructor(props) {
        super(props);
        //this = this;
        this.state = {
            noInternet: false,
            style_attr: {},
            new_values: {},
            new_actions: {}
        };
        this.updateValues();
    }

    render() {
        
        this.updateValues();
        //this.checkInternetConnection();
        return (
            <View>
                {this.state.noInternet && <Text style={{
                    width: "100%",
                    backgroundColor: Colors.color_primaryDark,
                    paddingVertical: 3,
                    color: "white",
                    fontSize: 14,
                    alignContent: "center",
                    textAlign: "center",
                    fontFamily: Strings.default_font_family_Roboto
                }}>No Internet Connection !</Text>}
                <ActionBar
                    containerStyle={this.style_attr.containerStyle}
                    title={this.new_values.title}
                    subtitle={this.new_values.subtitle}
                    titleContainerStyle={this.style_attr.titleContainerStyle}
                    titleStyle={this.style_attr.titleStyle}
                    leftIconImage={this.style_attr.leftIconImage}
                    leftIconImageStyle={this.style_attr.leftIconImageStyle}
                    iconContainerStyle={this.style_attr.iconContainerStyle}
                    iconImageStyle={this.style_attr.iconImageStyle}
                    disableShadows={this.style_attr.disableShadows}
                    elevation={this.style_attr.elevation}
                    onTitlePress={() => this.new_actions.onTitlePress()}
                    rightIcons={this.props.iconMap}
                    rightText={this.style_attr.rightText}
                    rightTextStyle={this.style_attr.rightTextStyle}
                    onRightTextPress={this.new_actions.onRightTextPress}
                    badgeColor={this.style_attr.badgeColor}
                    badgeTextColor={this.style_attr.badgeTextColor}
                    onLeftPress={() => this.new_actions.onLeftPress()}
                    leftZoneContentContainerStyle={this.style_attr.leftZoneContentContainerStyle}
                    backgroundColor={this.style_attr.tintColor}
                />
            </View>
        )
    }

    componentDidMount() {
        // Utility.getNetInfo().then(isConnected => {
        //     this.setState({noInternet: !isConnected});
        // });
    }
}

ActionBarWrapper.propTypes = {
    values: PropTypes.object,
    iconMap: PropTypes.array,
    styleAttributes: PropTypes.object,
    actions: PropTypes.object
};