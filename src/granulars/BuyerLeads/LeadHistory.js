import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import Timeline from '../../components/TimelineComponent'

export default class LeadHistory extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={Styles.container}>

                <View style={{ flex: 1, paddingHorizontal: Dimens.padding_xx_large }}>
                    <Timeline
                        data={this.props.data}
                    />
                </View>

                <View style={Styles.divider}></View>

                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.onViewAllHistory()}>

                        <Text style={Styles.viewAllText}>{Strings.VIEW_ALL}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    divider: {
        height: 0.9,
        backgroundColor: Colors.DIVIDER,
    },
    viewAllText: {
        color: Colors.PRIMARY,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        padding: Dimens.padding_xx_large
    },
})
