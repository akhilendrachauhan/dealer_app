import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native'
import DotView from '../DotView'
import { Utility } from '../../util'
import { Strings, Colors, Dimens } from '../../values'
import DBFunctions from "../../database/DBFunctions"
import * as DBConstants from '../../database/DBConstants'

export default class LeadDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            locality: ''
        }
    }

    componentDidMount() {
        let dbFunctions = new DBFunctions()
        dbFunctions.getLocalityById(this.props.data.location_id).then((result) => {
            let location = result && result[0] && result[0][DBConstants.LOCALITY] ? result[0][DBConstants.LOCALITY] : ''
            this.setState({ locality: location })
        })
    }

    render() {
        return (
            <View style={{ flex: 1, padding: Dimens.padding_xx_large }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        {/* <Text numberOfLines={1} style={Styles.normalText}>{DateFormat(this.props.data.lead_created_date, 'mmm dd, ddd') || Strings.NA}</Text> */}
                        <Text numberOfLines={1} style={Styles.normalText}>{Utility.getFormatedDateTime(this.props.data.lead_created_date, 'mmm dd, ddd')}</Text>
                        <Text style={Styles.smallText}>{Strings.CREATE_ON}</Text>
                    </View>
                    <View>
                        <Text numberOfLines={1} style={Styles.normalText}>{this.state.locality || Strings.NA}</Text>
                        <Text style={Styles.smallText}>{Strings.LOCATION}</Text>
                    </View>
                    <View>
                        <Text numberOfLines={1} style={Styles.normalText}>{this.props.data.lead_source || Strings.NA}</Text>
                        <Text style={Styles.smallText}>{Strings.SOURCE}</Text>
                    </View>
                </View>

                <View style={Styles.divider}></View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={Styles.boldText}>{Strings.CUSTOMER_REQUIREMENTS}</Text>

                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.onEditClick()}>

                        {this.props.data.price_from == '' && this.props.data.body_type == '' && this.props.data.fuel_type == '' && this.props.data.transmission_type == '' ?
                            <Text style={[Styles.normalText, { color: Colors.PRIMARY }]}>{Strings.ADD}</Text>
                            :
                            <Text style={[Styles.normalText, { color: Colors.PRIMARY }]}>{Strings.EDIT1}</Text>
                        }
                    </TouchableOpacity>
                </View>

                {this.props.data.price_from == '' && this.props.data.fuel_type == '' && this.props.data.transmission_type == '' && this.props.data.model == '' && this.props.data.body_type == '' ?
                    <View style={{ marginTop: 10 }}>
                        <Text style={Styles.simpleText}>{Strings.ADD_REQUIREMENTS}</Text>
                    </View>
                    :
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={Styles.simpleText}>{Utility.formatCurrency(this.props.data.price_from) || Strings.NA}</Text>

                            {this.props.data.fuel_type != '' && <DotView />}
                            {this.props.data.fuel_type != '' &&
                                <Text style={Styles.simpleText}>{this.props.data.fuelType || Strings.NA}</Text>
                            }

                            {this.props.data.transmission_type != '' && <DotView />}
                            {this.props.data.transmission_type != '' &&
                                <Text style={Styles.simpleText}>{this.props.data.transmissionType || Strings.NA}</Text>
                            }

                            {this.props.data.model != '' && <DotView />}
                            {this.props.data.model != '' &&
                                <Text style={Styles.simpleText}>{this.props.data.makeModel || Strings.NA}</Text>
                            }

                            {this.props.data.body_type != '' && <DotView />}
                            {this.props.data.body_type != '' &&
                                <Text style={Styles.simpleText}>{this.props.data.bodyType || Strings.NA}</Text>
                            }
                        </View>
                    </ScrollView>
                }

                <View style={Styles.divider}></View>

                {/* <Text style={Styles.simpleText}>{'Looking at 3 more cars'}</Text> */}
                <Text style={Styles.simpleText}>{Strings.EXCLUSIVE_LEAD}</Text>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    headText: {
        fontSize: Dimens.text_x_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    countText: {
        fontSize: Dimens.text_40,
        fontFamily: Strings.APP_FONT
    },
    containerCall: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.GREEN_CALL
    },
    containerUpdate: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
    },
    updateText: {
        marginLeft: 10,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        fontFamily: Strings.APP_FONT
    },
    callText: {
        marginLeft: 10,
        fontSize: Dimens.text_large,
        color: Colors.WHITE,
        fontFamily: Strings.APP_FONT
    },
    containerTab: {
        elevation: 0,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.DIVIDER,
        backgroundColor: Colors.WHITE
    },
    normalText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        marginTop: 5,
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    boldText: {
        color: Colors.BLACK_87,
        fontWeight: 'bold',
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    divider: {
        height: 0.5,
        backgroundColor: Colors.DIVIDER,
        marginVertical: Dimens.padding_xx_large
    },
    simpleText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
})
