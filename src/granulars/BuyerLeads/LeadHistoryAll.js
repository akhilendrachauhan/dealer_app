import React, { Component } from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import Timeline from '../../components/TimelineComponent'

export default class LeadHistoryAll extends Component {

    constructor(props) {
        super(props)
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>

                    <ActionBarWrapper
                        values={{ title: Strings.HISTORY }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <View style={{ flex: 1, paddingHorizontal: Dimens.padding_xx_large }}>
                        <Timeline
                            data={this.props.navigation.state.params.data}
                        />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    }
})
