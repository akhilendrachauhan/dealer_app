import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { GridView } from './GridView';
import { Colors, Dimens } from '../values';
import * as Strings from "../values/Strings";
import { Utility } from '../util';

/* *    AUTHOR :
*    Rajeev
*
*    CUSTOMIZABLE ITEMS :
*    textColor
 * (default #000000), padding in the items (default 10), backgroundColor
 * (default #ffffff)
*
*    DEPENDENCY :
*    props : gridData An array of item
 * consisting of icon, text, action attributes
*
 */
var padding;
var fontColor;
var backgroundColor;
var gridData;

class HomeGridView extends Component {

    constructor(props) {
        super(props);

        gridData = props.gridData;
        padding = props.padding
            ? parseInt(props.padding)
            : 8;
        fontColor = props.fontColor
            ? props.fontColor
            : Colors.black_85;
        backgroundColor = props.backgroundColor
            ? props.backgroundColor
            : Colors.WHITE;

        itemCount = gridData.length;
        const data = generateData(gridData);
        var rowCount = props.rowCount
            ? props.rowCount
            : 2;
        var columnCount = Math.ceil(itemCount / rowCount);
        

        this.state = {
            data,
            dataSource: null,
            itemCount,
            itemsPerRow: columnCount
        };
    }

    componentWillReceiveProps(props) {
        var gridData = props.gridData;
        var itemCount = gridData.length;
        var data = generateData(gridData);
        var rowCount = props.rowCount
            ? props.rowCount
            : 2;
        var columnCount = Math.ceil(itemCount / rowCount);
        this.setState({ data, dataSource: null, itemCount, itemsPerRow: columnCount })
    }

    onPageLayout = (event) => {
        const { width, height } = event.nativeEvent.layout;
        this.setState({ width, height })
    };

    render() {
        Utility.log("Grid Data:" + JSON.stringify(this.state.data));
        return (
            <View style={{
                flex: 1,
                flexDirection:'row'
            }}>
                {
                    this.props.heading
                        ? <Text style={styles.headingStyle}>{this.props.heading}</Text>
                        : null
                }
                {/* <GridView
                    style={{
                        flex: 1
                    }}
                    data={this.state.data}
                    dataSource={this.state.dataSource}
                    itemsPerRow={this.state.itemsPerRow}
                    rowStyle={{
                        backgroundColor: backgroundColor
                    }}
                    renderItem={(item, sectionID, rowID, itemIndex, itemID) => {
                        return (
                            <View style={{
                                flex: 1
                            }}>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={()=>item.action(item)}
                                    acti
                                    style={[
                                        styles.container, {
                                            paddingTop: padding,
                                            paddingBottom: padding
                                        }
                                    ]}>
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Image style={styles.imageStyle} source={item.icon}/>
                                        <Text
                                            style={[
                                                styles.textStyle, {
                                                    color: fontColor
                                                }
                                            ]}>{item.text}</Text>
                                        {
                                            item.subText
                                                ? <Text style={styles.subTextStyle}>{item.subText}</Text>
                                                : null
                                        }
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.horizontalLine}/>
                            </View>
                        );
                    }}/> */}
                <FlatList
                    data={this.state.data}
                    renderItem={({ item,index }) => {
                        return (
                            <View style={{
                                flex: 1,
                                flexDirection: 'row'
                            }}>
                                <View style={{flex:1}}>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => item.action(item)}
                                    style={[
                                        styles.container, {
                                            paddingTop: padding,
                                            paddingBottom: padding
                                        }
                                    ]}>
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Image style={styles.imageStyle} source={item.icon} />
                                        <Text numberOfLines={1}
                                            style={[
                                                styles.textStyle, {
                                                    color: fontColor
                                                }
                                            ]}>{item.text}</Text>
                                        {
                                            item.subText
                                                ? <Text style={styles.subTextStyle}>{item.subText}</Text>
                                                : null
                                        }
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.horizontalLine} />
                            </View>
                            {(index + 1) % this.state.itemsPerRow !== 0 && (index + 1) !== this.state.data.length && <View style={{width:1, backgroundColor: Colors.DIVIDER}}/>}
                        </View>
                        );
                    }}
                    //Setting the number of column
                    numColumns={this.state.itemsPerRow}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

function generateData(gridData) {

    var itemCount = gridData.length;

    return Array(itemCount).fill(null).map((item, index) => {
        return {
            key: index,
            text: gridData[index].text,
            icon: gridData[index].icon,
            subText: gridData[index].subtext,
            action: gridData[index].action
        };
    });
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: Dimens.padding_normal,
        paddingBottom: Dimens.padding_normal
    },
    imageStyle: {
        width: Dimens.icon_large,
        height: Dimens.icon_large,
        resizeMode: 'contain'
    },
    textStyle: {
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginTop: 5,
        paddingHorizontal: 5
    },
    horizontalLine: {
        height: 1,
        backgroundColor: Colors.DIVIDER,
        flexDirection: 'row'
    },
    headingStyle: {
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        padding: Dimens.padding_normal,
        paddingLeft: Dimens.padding_xxx_large,
        backgroundColor: Colors.WHITE,
    },
    subTextStyle: {
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    }
});
export default HomeGridView;
