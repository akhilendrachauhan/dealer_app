import React, {Component} from 'react'
import {StyleSheet, Text, View} from 'react-native'
import {Colors, Strings} from "../values";

class NotificationSection extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>{this.props.title}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        paddingBottom: 5,
        paddingLeft: 20,
        paddingRight: 5,
        backgroundColor: 'transparent'
    },
    title: {
        color: Colors.BLACK,
        fontWeight: '400',
        fontSize: 12,
        fontFamily: Strings.default_font_family_Roboto
    }
});

export default NotificationSection;