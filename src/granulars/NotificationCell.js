import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import {Colors, Strings} from "../values";

class NotificationCell extends Component {

    constructor(props) {
        super(props)
    }

    static defaultProps = {
        onSelectNotificationCell: null
    };
    //this.props.data['business_tag' == 'Dealer']

    render() {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                    if (this.props.onSelectNotificationCell != null)
                        this.props.onSelectNotificationCell(this.props.data, this.props.index, this.props.section)
                }}>
                <View style={styles.container}>
                    <View style={[styles.card,
                    this.props.data['business_tag'] == 'Loan' ? { backgroundColor: '#ffff8f' } : { backgroundColor: Colors.GRAY_LIGHT }
                     ]}>
                        <View style={styles.heading_container}>
                            <Text style={[styles.title, { flex: 1 }]}>{this.props.data['notification_title']}</Text>
                            
                            {/* { this.props.data['business_tag'] ? <View style={{ backgroundColor: Colors.PRIMARY, padding: 5, paddingHorizontal:10, borderRadius: 5 }}>
                                <Text style={styles.tagText}>{this.props.data['business_tag']}</Text>
                            </View> : <View/>} */}
                            {/* <Text style={[styles.time]}>{this.props.data['localDate']}</Text> */}
                        </View>
                        <View>
                            <Text style={[styles.description, this.props.data['is_read'] == '0' ? { color: Colors.BLACK_85 } : { color: Colors.BLACK_40,textAlign : 'justify' } ]}>{this.props.data['notification_msg']}</Text>
                        </View>

                        <View style={{ flexDirection: 'row-reverse', marginTop: 5 }}>
                        <Text style={[styles.time]}>{this.props.data['display_date']}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

        )
    }
}

NotificationCell.propTypes = {
    onSelectNotificationCell: PropTypes.func
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 12,
        paddingRight: 12,
        backgroundColor: 'transparent'
    },
    card: {
        flex: 1,
        padding: 20,
        backgroundColor: Colors.GRAY_LIGHT,
        borderRadius: 8,
    },
    heading_container: {
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    title: {
        color: Colors.BLACK_85,
        fontWeight: '500',
        fontSize: 16,
        fontFamily: Strings.default_font_family_Roboto
    },
    time: {
        color: Colors.BLACK_54,
        fontWeight: '400',
        fontSize: 12,
        fontFamily: Strings.default_font_family_Roboto
    },
    tagText:{
        color: Colors.WHITE,
        fontWeight: '500',
        fontSize: 12,
        fontFamily: Strings.default_font_family_Roboto
    },
    description: {
        color: Colors.BLACK_40,
        fontWeight: '400',
        fontSize: 14,
        fontFamily: Strings.default_font_family_Roboto
    },
    heading_container_title: {
        flex: 0,
        flexGrow: 0
    },
    heading_container_date: {
        flex: 0,
        flexGrow: 1
    }
});

export default NotificationCell;