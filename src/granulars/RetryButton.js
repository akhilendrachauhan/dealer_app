import React, {Component} from 'react';
import {Colors, Strings} from "../values";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

export default class RetryButton extends Component {
    buttonClick = () => {
        this.props.onButtonPressed();
    };

    render() {
        return (
            <TouchableOpacity style={styles.submit}
                activeOpacity={0.8}
                onPress={() => {
                    this.buttonClick();
                }}
                >
                <Text style={[styles.submitText,{fontSize:this.props.textSize,width:"100%"}]}>{this.props.title.toUpperCase()}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    
    
    submit:{
        marginTop:10,
        paddingTop: 10,
        paddingRight:20,
        paddingLeft:20,
        paddingBottom:10,
        backgroundColor: Colors.PRIMARY,
        borderRadius:5,
        //flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height:40
     },
      submitText:{
          color: Colors.WHITE,
          alignSelf:'center',
          textAlign : 'center',
          fontFamily : Strings.APP_FONT
        }
});
