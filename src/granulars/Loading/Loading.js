import React, { Component } from 'react';
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native';
import { Colors } from '../../values/index';

class Loading extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[Styles.container, { backgroundColor: this.props.bgColor || Colors.BLACK_40 }]}>
                <ActivityIndicator size="large" color={Colors.PRIMARY} visible={this.props.isLoading} />
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.BLACK_40,
        position: 'absolute',
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        top: 0,
        right: 0,
        left: 0
    }
});

export default Loading;
