import React,{Component} from "react";
import {icon_back_arrow,icon_cancel} from "../assets/ImageAssets";
import {Strings,Colors,Dimens} from './../values'
import {Platform , View, Image, TouchableHighlight, TextInput,SafeAreaView} from "react-native";


export default class ActionBarSearchComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isShowCross: false,
            inputText: ''
        }
    }

    static defaultProps = {
        paddingLeft: Dimens.padding_small,
        paddingRight: Dimens.padding_small,
        paddingTop: (Platform.OS === 'ios')
            ? 23
            : Dimens.padding_small,
        paddingBottom: Dimens.padding_small,
        width: null,
        flex: 1,
        inputBackgroundColor: Colors.WHITE,
        backgroundColor: Colors.SCREEN_BACKGROUND,
        placeholderTextColor: Colors.black_54,
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        cancelButtonTextSize: Dimens.text_normal,
        cancelColor: Colors.WHITE,
        tintColor: Colors.black_54,
        isShowCancel: false,
    };

    onChangeText(text) {
        if (this.props.OnSearchTextChange) {
            this.props.OnSearchTextChange(text);
        }
        if (text.length > 0) {
            this.setState({isShowCross: true, inputText: text});
        } else {
            this.setState({isShowCross: false, inputText: text});
        }
    }

    componentDidMount() {
        this.textInput.focus();
    }

    render() {
        return (

            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    height:56,
                    backgroundColor:'white',
                    position:'absolute', left:0, right:0, elevation:5
                }}>

                <View
                    style={{
                        width: this.props.width,
                        height: this.props.height,
                        flex: this.props.flex,
                        alignItems: 'center',
                        backgroundColor: this.props.inputBackgroundColor,
                        flexDirection: 'row'
                    }}>
                    <TouchableHighlight
                        onPress={this.props.onSearchBackPress}
                        underlayColor={Colors.GRAY_LIGHT}>
                    <Image
                        style={{
                            margin: Dimens.margin_small,
                            tintColor: this.props.tintColor,
                            width: Dimens.icon_x_small,
                            height: Dimens.icon_x_small
                        }}
                        source={icon_back_arrow}/>
                    </TouchableHighlight>

                    <TextInput
                        ref={input => {
                            this.textInput = input
                        }}
                        style={{
                            fontFamily: Strings.APP_FONT,
                            fontSize: this.props.fontSize,
                            color: this.props.color,
                            flex: 1,
                            width: null
                        }}
                        returnKeyType='search'
                        underlineColorAndroid={Colors.TRANSPARENT}
                        placeholder={this.props.text}
                        placeholderTextColor={this.props.placeholderTextColor}
                        autoCorrect={false}
                        autoFocus={this.props.autoFocus}
                        onSubmitEditing={() => this.props.onSubmitEditing(this.state.inputText)}
                        onChangeText={(text) => this.onChangeText(text)}/>

                    {
                        this.state.isShowCross
                            ? this.getCrossButton()
                            : null
                    }
                </View>
</View>
           
        );
    }

    getCrossButton() {
        return (
            <TouchableHighlight
                onPress={() => {
                    this.textInput.clear();
                    this.textInput.focus();
                    this.onChangeText('')
                }}
                underlayColor={Colors.GRAY_LIGHT}>
                <Image
                    style={{
                        margin: Dimens.margin_small,tintColor: this.props.tintColor, width: Dimens.icon_x_small,
                            height: Dimens.icon_x_small
                    }}
                    source={icon_cancel}/>
            </TouchableHighlight>
        );
    }
}
