import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'

export default class CarInfo extends Component {

    constructor(props) {
        super(props)

    }

    render() {
        return (
            <View style={{flex:1}}>
                <FlatList
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View style={[styles.separator, highlighted]} />
                    )}
                    showsVerticalScrollIndicator={false}
                    data={this.props.carInfo}
                    renderItem={({ item, index, separators }) => (
                        <View style={{ flex: 1, margin: Dimens.margin_xxx_large }}>
                            <Text style={styles.modalText}>{item.value || Strings.NA}</Text>
                            <Text style={styles.labelText}>{item.label}</Text>
                        </View>
                    )}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    separator: {
        height: 1,
        backgroundColor: Colors.BLACK_12,
    },
    modalText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
    },
    labelText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    }
})
