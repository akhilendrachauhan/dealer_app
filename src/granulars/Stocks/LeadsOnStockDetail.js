import React, { Component } from 'react';
import { StyleSheet, Text, View ,FlatList,TouchableOpacity,Image} from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import {icon_lg_status,icon_time,icon_dash_separator} from './../../assets/ImageAssets'
import { Utility } from '../../util';
import * as Constants from './../../util/Constants'
import {getLeadListByCarId} from './../../api/APICalls'
import LoadingComponent from './../../granulars/LoadingComponent'
import RetryButton from './../../granulars/RetryButton';
import BuyerLeadTupleComponent from "../../components/BuyerLeadTupleComponent"
export default class LeadsOnStockDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leadList : [
                //{
                //     "lead_id": 4596038,
                //     "id": 105844,
                //     "lead_customer_id": 48785,
                //     "dealer_id": 69,
                //     "customer_name": "Testmonanewlead",
                //     "customer_email": "mona@girnarsoft.com",
                //     "customer_alt_mobile": "9830971156",
                //     "customer_alt_email": "",
                //     "lead_status_id": 3,
                //     "lead_sub_status_id": 0,
                //     "lead_rating": "",
                //     "lead_follow_date": "2019-08-20T10:47:00.000Z",
                //     "lead_walkin_date": "2019-08-20T10:47:00.000Z",
                //     "lead_source": "WALK-IN",
                //     "lead_sub_source": "Mobile",
                //     "lead_created_date": "2019-08-19T10:48:26.000Z",
                //     "lead_update_date": "2019-08-19T11:38:00.000Z",
                //     "location_id": 1,
                //     "is_active": 1,
                //     "amount": 0,
                //     "car_id": 0,
                //     "make": "",
                //     "model": "",
                //     "price_from": 175000,
                //     "price_to": 175000,
                //     "fuel_type": "Diesel",
                //     "transmission_type": "Automatic",
                //     "body_type": "",
                //     "is_latest": 1,
                //     "created_at": "2019-08-19T10:48:26.000Z",
                //     "updated_at": "2019-08-19T13:08:00.000Z",
                //     "is_finance": 0,
                //     "lead_score": 0,
                //     "mobile": "8240724303",
                //     "opt_verified": 0,
                //     "gaadi_common": 0,
                //     "status_name": "Interested",
                //     "stock_car_id": 3110300
                // },
                // {
                //     "lead_id": 4596040,
                //     "id": 105846,
                //     "lead_customer_id": 48787,
                //     "dealer_id": 69,
                //     "customer_name": "TestPriyaDemoLead",
                //     "customer_email": "",
                //     "customer_alt_mobile": "",
                //     "customer_alt_email": "",
                //     "lead_status_id": 9,
                //     "lead_sub_status_id": 0,
                //     "lead_rating": "",
                //     "lead_follow_date": "2019-08-29T03:31:00.000Z",
                //     "lead_walkin_date": "2019-08-29T03:31:00.000Z",
                //     "lead_source": "Cardekho",
                //     "lead_sub_source": "Mobile",
                //     "lead_created_date": "2019-08-20T03:34:25.000Z",
                //     "lead_update_date": "2019-08-27T11:43:28.000Z",
                //     "location_id": 0,
                //     "is_active": 1,
                //     "amount": 0,
                //     "car_id": 0,
                //     "make": "4",
                //     "model": "",
                //     "price_from": 250000,
                //     "price_to": 250000,
                //     "fuel_type": "",
                //     "transmission_type": "",
                //     "body_type": "",
                //     "is_latest": 1,
                //     "created_at": "2019-08-20T03:34:25.000Z",
                //     "updated_at": "2019-08-27T13:13:28.000Z",
                //     "is_finance": 0,
                //     "lead_score": 0,
                //     "mobile": "9830971199",
                //     "opt_verified": 0,
                //     "gaadi_common": 0,
                //     "status_name": "Walk-in Done",
                //     "stock_car_id": 3110300
                // },
                // {
                //     "lead_id": 4596041,
                //     "id": 105847,
                //     "lead_customer_id": 48788,
                //     "dealer_id": 69,
                //     "customer_name": "Banda",
                //     "customer_email": "bandi.gl001@gmail.com",
                //     "customer_alt_mobile": "6787655677",
                //     "customer_alt_email": "",
                //     "lead_status_id": 3,
                //     "lead_sub_status_id": 0,
                //     "lead_rating": "Warm",
                //     "lead_follow_date": "2019-09-03T08:59:00.000Z",
                //     "lead_walkin_date": "2019-09-03T08:59:00.000Z",
                //     "lead_source": "",
                //     "lead_sub_source": "Mobile",
                //     "lead_created_date": "2019-08-20T03:57:11.000Z",
                //     "lead_update_date": "2019-08-27T09:08:41.000Z",
                //     "location_id": 2,
                //     "is_active": 1,
                //     "amount": 0,
                //     "car_id": 0,
                //     "make": "",
                //     "model": "",
                //     "price_from": 0,
                //     "price_to": 0,
                //     "fuel_type": "",
                //     "transmission_type": "",
                //     "body_type": "",
                //     "is_latest": 1,
                //     "created_at": "2019-08-20T03:57:12.000Z",
                //     "updated_at": "2019-08-27T10:38:41.000Z",
                //     "is_finance": 0,
                //     "lead_score": 0,
                //     "mobile": "8750833843",
                //     "opt_verified": 0,
                //     "gaadi_common": 0,
                //     "status_name": "Interested",
                //     "stock_car_id": 3110300
                // }
            ],
            loading : false
        }
    }
    componentDidMount() {
        if(this.props.car_id)
            this.getLeadListing()
    }

    getLeadListing = () => {
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getLeadListByCarId(dealer_id,this.props.car_id ,this.onSuccess, this.onFailure, this.props)
            } catch (error) {
                Utility.log(error)
            }
            this.setState({loading:true})
            
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
        
    }

    onSuccess = (response) => {
        let key = 'car_'+this.props.car_id
        if (response && response.data && response.data[key] && response.data[key].length > 0)
            this.setState({
                loading: false,
                leadList: response.data[key],
            })
        else
            this.setState({ loading: false })

        
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        Utility.log('onFailureLeadListing===> ', JSON.stringify(response))
        this.setState({ loading: false })
    }
    // renderItem = ({ item, index }) => {
    //     return (
    //         <TouchableOpacity
    //             activeOpacity={0.8}
    //             onPress={() => this.props.onItemPress(item)}>

    //             <View style={[styles.containerItem, { borderLeftColor: Colors.GREEN }]}>

    //                 <View style={{ flex: 0.25, alignItems: 'center', justifyContent: 'center' }}>
    //                     <Image style={{ width: Dimens.icon_normal, height: Dimens.icon_normal, tintColor: Colors.GREEN }}
    //                         source={icon_time} />

    //                     <Text style={styles.timeText}>11:30 AM</Text>
    //                     <Text style={styles.smallText}>Follow up</Text>
    //                 </View>

    //                 <View style={{ width: 5, marginVertical: 8 }}>
    //                     <Image style={{ width: 2, height: '100%' }}
    //                         source={icon_dash_separator} />
    //                 </View>

    //                 <View style={{ flex: 0.75, paddingLeft: Dimens.padding_xx_large }}>
    //                     <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
    //                         <Text numberOfLines={1} style={styles.headText}>Ravinder Singh</Text>

    //                         <View style={{ alignItems: 'center', justifyContent: 'center' }}>
    //                             <Image style={{ width: 100, tintColor: Colors.GREEN, resizeMode: 'contain' }}
    //                                 source={icon_lg_status} />

    //                             <Text style={styles.tagText}>Customer Offer</Text>
    //                         </View>
    //                     </View>
    //                     <Text style={styles.normalText}>Mahindra XUV 500</Text>
    //                     <Text style={styles.normalText}>Offer : {Strings.RUPEE_SYMBOL}9 Lakh</Text>
    //                 </View>
    //             </View>
    //         </TouchableOpacity>
    //     )
    // }

    renderItem = ({ item, index }) => {
        return (
            <BuyerLeadTupleComponent
                item={item}
                index={index}
                callback={this.props.onItemPress}
            />
        )
    }
    render() {
        return (
            <View style={{ flex: 1}} >
               { this.state.leadList.length > 0 ? 
               <FlatList
                    style={{ marginHorizontal: Dimens.margin_small }}
                    extraData={this.state}
                    data={this.state.leadList}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => 'A' + index}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.getLeadListing}
                    refreshing={this.state.loading}
                /> : 
                <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontFamily: Strings.APP_FONT,fontSize:Dimens.text_normal,color: Colors.BLACK}}>{Strings.NO_LEADS_AVAILABLE}</Text>
                    <RetryButton 
                        title = {Strings.RETRY} 
                        onButtonPressed = {this.getLeadListing}
                        textSize = {14}
                    />
                </View>
                }
                {this.state.loading && <LoadingComponent backgroundColor = {Colors.TRANSPARENT}/>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerItem: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 5,
        borderLeftWidth: 5,
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_normal,
        marginVertical: Dimens.margin_x_small
    },
    headText: {
        flex: 1,
        fontWeight: '600',
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
    },
    timeText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    smallText: {
        fontSize: Dimens.text_x_small,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    tagText: {
        position: 'absolute',
        right: 8,
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    }
})
