import React, { Component } from 'react'
import {
    View, Text, Image, TouchableOpacity, StyleSheet, Modal, FlatList, ActivityIndicator,
    TouchableWithoutFeedback, Keyboard, TextInput
} from 'react-native'
import { Utility, Constants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import LoadingComponent from '../LoadingComponent'
import TextInputLayout from "../../granulars/TextInputLayout"

export default class ShareScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultIndex: 1
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.props.isVisible}
                    onRequestClose={() => { this.props.closeBottomSheet('closed', '') }}>

                    <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'flex-end' }}>
                        <View style={Styles.modal}>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text style={Styles.headText}>{Strings.SHARE_LEAD}</Text>

                                <TouchableOpacity
                                    onPress={() => this.props.closeBottomSheet('closed', '')}>
                                    <Image style={Styles.imageStyle} source={ImageAssets.icon_cancel} />
                                </TouchableOpacity>
                            </View>

                            <FlatList
                                style={{ marginTop: 10 }}
                                data={this.props.shareLead}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => "" + index}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.props.showLoading}
                    onRequestClose={() => { }}>

                    <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'row', backgroundColor: Colors.WHITE, padding: 25, alignItems: 'center' }}>
                            <ActivityIndicator size="large" color={Colors.PRIMARY} />

                            <Text style={[Styles.headText, { marginLeft: 16 }]}>{Strings.DOWNLOADING}</Text>
                        </View>
                    </View>
                </Modal>

                {this.EmailModelRender()}

                {this.props.isLoading && <LoadingComponent msg={this.props.loadingMsg} />}
            </View>
        )
    }

    renderItem = ({ item, index }) => {
        return (
            (this.props.carName != '' || item.id == 1 || item.id == 2) &&
            (this.props.carData && this.props.carData.car_image.length > 0 || item.id != 4) &&
            <View>
                <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 10 }}
                    activeOpacity={0.8}
                    onPress={() => this.setState({ defaultIndex: item.id })}>

                    {this.state.defaultIndex == item.id ?
                        <View style={Styles.circleFill}>
                            <View style={Styles.fill} />
                        </View>
                        :
                        <View style={Styles.circle} />
                    }
                    {(item.id == 3 || item.id == 4) ?
                        <Text style={Styles.itemText}>{this.props.carName + ' ' + item.title}</Text>
                        :
                        <Text style={Styles.itemText}>{item.title}</Text>
                    }
                </TouchableOpacity>

                {this.state.defaultIndex == item.id &&
                    <View style={{ flexDirection: 'row', marginBottom: 10, alignItems: 'center', justifyContent: 'space-around' }}>
                        {item.options.map((data) => {
                            return (
                                (Constants.APP_TYPE == Constants.PHILIPPINES && data.option_id != 1) || // option_id == 1 is for whatsapp
                                    Constants.APP_TYPE == Constants.INDONESIA ?
                                    <TouchableOpacity key={data.option_id}
                                        style={{ alignItems: 'center' }}
                                        activeOpacity={0.8}
                                        onPress={() => data.onPress(item.id)}>

                                        <Image source={data.icon}
                                            style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                                        <Text style={Styles.optionText}>{data.value}</Text>
                                    </TouchableOpacity>
                                    :
                                    null
                            )
                        })}
                    </View>
                }
            </View>
        )
    }

    EmailModelRender() {
        return (
            <View>
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.props.showEmailModel}
                    onRequestClose={() => { this.props.closeEmailModel() }}>

                    <TouchableWithoutFeedback onPress={() => {
                        Keyboard.dismiss()
                    }}>
                        <View style={Styles.modelContainer}>
                            <View style={Styles.modelInnerContainer}>
                                <Text style={Styles.headText}>{Strings.ENTER_EMAIL_IDS}</Text>

                                <TextInputLayout
                                    style={{ height: 50, marginTop: Dimens.margin_xxx_large }}
                                    ref={(input) => this.TL_Email = input}
                                    hintColor={Colors.BLACK_85}
                                    focusColor={Colors.PRIMARY}
                                    errorColor={Colors.PRIMARY}>

                                    <TextInput style={Styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='go'
                                        keyboardType='email-address'
                                        value={this.props.customerEmail}
                                        ref={(input) => this.EmailInput = input}
                                        placeholder={Strings.EMAIL_ADDRESS}
                                        onSubmitEditing={() => this.props.onDoneEmail()}
                                        onChangeText={(text) => this.props.onEmailTextChange(text)} />
                                </TextInputLayout>

                                <View style={{ flexDirection: 'row', marginTop: Dimens.margin_xxx_large, justifyContent: 'flex-end', }}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => { this.props.closeEmailModel() }}>

                                        <Text style={[Styles.TextStyle, { marginRight: Dimens.margin_xxx_large }]}>{Strings.CANCEL}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.onDoneEmail()}>

                                        <Text style={Styles.TextStyle}>{Strings.DONE}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        marginTop: 50,
        backgroundColor: Colors.WHITE
    },
    headText: {
        fontSize: Dimens.text_x_large,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    imageStyle: {
        width: 24,
        height: 24
    },
    modal: {
        padding: Dimens.padding_xx_large,
        backgroundColor: Colors.WHITE,
    },
    radioButtonContainer: {
        marginTop: Dimens.margin_xx_large,
    },
    radioButtonItem: {
        marginVertical: Dimens.margin_normal
    },
    circle: {
        width: 20,
        height: 20,
        padding: 3,
        marginEnd: 10,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: Colors.BLACK_54
    },
    fill: {
        flex: 1,
        borderRadius: 10,
        backgroundColor: Colors.PRIMARY,
    },
    circleFill: {
        width: 20,
        height: 20,
        padding: 3,
        marginEnd: 10,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: Colors.PRIMARY
    },
    itemText: {
        color: Colors.BLACK_85,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    optionText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    modelContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLACK_54,
    },
    modelInnerContainer: {
        width: '85%',
        backgroundColor: Colors.WHITE,
        marginHorizontal: Dimens.margin_xx_large,
        padding: Dimens.padding_xx_large,
        borderRadius: Dimens.border_radius
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    TextStyle: {
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        color: Colors.PRIMARY,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
})