import React, { Component } from 'react'
import ShareScreen from './ShareScreen'
import { View, SafeAreaView, StyleSheet, Linking, PermissionsAndroid, Keyboard, Platform } from 'react-native'
import { Utility, Constants,AnalyticsConstants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import Share from 'react-native-share'


export default class ShareContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showEmailModel: false,
            showLoading: false,
            isLoading: false,
            loadingMsg: '',
            isVisible: false,
            defaultIndex: 1,
            usreName: '',
            usreEmail: '',
            usrePhone: '',
            dealerName: '',
            dealerAddress: '',
            dealerLat: '',
            dealerLang: '',
            carData: {},
            leadData: {},
            carName: '',
            customerEmail: '',
            emailShareType: '',
            shareLead: [
                {
                    "id": 1,
                    "title": Strings.WELCOME_MESSAGE,
                    "options": [
                        {
                            "option_id": 1,
                            "value": Strings.WHATSAPP,
                            "icon": ImageAssets.icon_whatsapp_logo,
                            'onPress': this.onWhatsAppShare
                        },
                        {
                            "option_id": 2,
                            "value": Strings.SMS,
                            "icon": ImageAssets.icon_sms,
                            'onPress': this.onSmsShare
                        }
                    ]
                },
                {
                    "id": 2,
                    "title": Strings.DEALERSHIP_LOCATION,
                    "options": [
                        {
                            "option_id": 1,
                            "value": Strings.WHATSAPP,
                            "icon": ImageAssets.icon_whatsapp_logo,
                            'onPress': this.onWhatsAppShare
                        },
                        {
                            "option_id": 2,
                            "value": Strings.SMS,
                            "icon": ImageAssets.icon_sms,
                            'onPress': this.onSmsShare
                        }
                    ]
                },
                {
                    "id": 3,
                    "title": Utility.strToTitleCase(Strings.DETAILS),
                    "options": [
                        {
                            "option_id": 1,
                            "value": Strings.WHATSAPP,
                            "icon": ImageAssets.icon_whatsapp_logo,
                            'onPress': this.onWhatsAppShare
                        },
                        {
                            "option_id": 2,
                            "value": Strings.SMS,
                            "icon": ImageAssets.icon_sms,
                            'onPress': this.onSmsShare
                        },
                        {
                            "option_id": 3,
                            "value": Strings.EMAIL,
                            "icon": ImageAssets.icon_mail,
                            'onPress': this.onEmailShare
                        }
                    ]
                },
                {
                    "id": 4,
                    "title": Strings.PHOTO,
                    "options": [
                        {
                            "option_id": 1,
                            "value": Strings.WHATSAPP,
                            "icon": ImageAssets.icon_whatsapp_logo,
                            'onPress': this.onWhatsAppShare
                        },
                        {
                            "option_id": 3,
                            "value": Strings.EMAIL,
                            "icon": ImageAssets.icon_mail,
                            'onPress': this.onEmailShare
                        }
                    ]
                }
            ]
        }
    }

    async componentDidMount() {
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.NAME, Constants.EMAIL,
        Constants.PHONE_NO, Constants.DEALER_NAME, Constants.DEALER_ADDRESS, Constants.DEALER_LAT, Constants.DEALER_LANG])
        this.setState({
            usreName: values[0][1],
            usreEmail: values[1][1],
            usrePhone: values[2][1],
            dealerName: values[3][1],
            dealerAddress: values[4][1],
            dealerLat: values[5][1],
            dealerLang: values[6][1]
        })
    }

    initiateShare = () => {
        this.setState({
            isVisible: true,
            leadData: this.props.leadData,
            carData: this.props.carData != null ? this.props.carData : {},
            carName: this.props.carData != null ? this.props.carData.make + ' ' + this.props.carData.modelVersion : '',
            customerEmail: this.props.carData != null ? this.props.leadData.customer_email : ''
        })
    }

    closeBottomSheet = (shareBy, ShareType) => {
        this.setState({ isVisible: false }, () => {
            if (shareBy != 'closed') {
                this.props.sharelead(shareBy, ShareType)
            }
        })
    }

    closeEmailModel = () => {
        this.setState({ showEmailModel: false })
    }

    getWelcomeMessage = () => {
        let carName = this.state.carName == '' ? 'a used car' : this.state.carName
        let message = 'Hi ' + this.props.leadData.customer_name + '\n\n' +
            'I am ' + this.state.usreName + ' from ' + this.state.dealerName + '. ' +
            'You are interested in buying ' + carName + '. Please suggest suitable time to get in touch with you ' +
            'or call me @ ' + this.state.usrePhone + '.\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    getDealerLocation = () => {
        let message = 'Hi ' + this.props.leadData.customer_name + '\n\n' +
            // 'I am ' + this.state.usreName + ' from ' + this.state.dealerName + '. ' +
            'We are located at http://maps.google.com/maps?q=' +
            this.state.dealerLat + ',' + this.state.dealerLang + '.\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    getCarDetails = () => {
        let message = 'Hi ' + this.props.leadData.customer_name + '\n\n' +
            // 'I am ' + this.state.usreName + ' from ' + this.state.dealerName + '. ' +
            'Please check out my ' + this.props.carData.reg_year + ' ' + this.state.carName + ' ' +
            this.props.carData.fuel_type + ' ' + this.props.carData.uc_colour + ' color with ' +
            this.props.carData.km_driven + ' kms driven priced at ' + Utility.displayCurrency(this.props.carData.car_price) + '.\n\n' +
            this.props.carData.web_url[0].url + '\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    request_storage_runtime_permission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'Storage Permission',
                    'message': Strings.APP_NAME + ' App needs access to your storage to download Photos.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                Utility.showToast(Strings.STORAGE_PERMISSION_GRANTED)
            }
            else {
                Utility.showToast(Strings.STORAGE_PERMISSION_NOT_GRANTED)
            }
        } catch (err) {
            Utility.log(err)
        }
    }

    getCarPhoto = async (ShareType) => {
        // await this.request_storage_runtime_permission()
        // Utility.downloadSingleImage(this.props.carData.car_profile_image)
        this.closeBottomSheet('closed', '')
        let imageArr = await this.props.carData.car_image

        if (imageArr.length > 0) {
            // this.setState({ showLoading: true })
            // Utility.downloadMultiImage(imageArr).then((images) => {
            //     Utility.log("images===>>>>", images)
            //     Utility.showToast(Strings.IMAGE_DOWNLOADED)
            //     this.setState({ showLoading: false })

            this.closeBottomSheet('WHATSAPP', ShareType)

            Utility.convertImageToBase64(imageArr).then((base64Data) => {
                Utility.log('base64Data==>', base64Data)

                const shareOptions = {
                    // title: '',
                    // message: '',
                    urls: base64Data,
                    social: Share.Social.WHATSAPP,
                    whatsAppNumber: ''
                    // filename: 'test',
                };
                if (Platform.OS == 'android')
                    Share.shareSingle(shareOptions)
                else
                    Share.open(shareOptions)
            })
            // })
        }
    }

    onSmsShare = async (typeId) => {
        let mobileNo = await this.props.leadData.mobile
        let message = '', ShareType = ''
        if (typeId == 1) {
            Utility.sendEvent(AnalyticsConstants.LEAD_WELCOME_SMS_SHARE)
            message = await this.getWelcomeMessage()
            ShareType = Constants.WELCOME_SHARE_TYPE
        }
        else if (typeId == 2) {
            Utility.sendEvent(AnalyticsConstants.LEAD_LOCATION_SMS_SHARE)
            message = await this.getDealerLocation()
            ShareType = Constants.LOCATION_SHARE_TYPE
        }
        else if (typeId == 3) {
            Utility.sendEvent(AnalyticsConstants.LEAD_CAR_DETAIL_SMS_SHARE)
            message = await this.getCarDetails()
            ShareType = Constants.CAR_DETAILS_SHARE_TYPE
        }
        else {
            this.closeBottomSheet('closed', '')
            return
        }

        let url = Platform.OS == 'ios' ? `sms:${mobileNo}&body=${message}` : `sms:${mobileNo}?body=${message}`

        // check if we can use this link
        const canOpen = await Linking.canOpenURL(url)

        if (!canOpen) {
            throw new Error('Provided URL can not be handled')
        }

        this.closeBottomSheet('SMS', ShareType)
        return Linking.openURL(url)
    }

    onWhatsAppShare = async (typeId) => {
        let mobileNo = await this.props.leadData.mobile
        let message = '', ShareType = ''
        if (typeId == 1) {
            Utility.sendEvent(AnalyticsConstants.LEAD_WELCOME_WHATSAPP_SHARE)
            message = await this.getWelcomeMessage()
            ShareType = Constants.WELCOME_SHARE_TYPE
        }
        else if (typeId == 2) {
            Utility.sendEvent(AnalyticsConstants.LEAD_LOCATION_WHATSAPP_SHARE)
            message = await this.getDealerLocation()
            ShareType = Constants.LOCATION_SHARE_TYPE
        }
        else if (typeId == 3) {
            Utility.sendEvent(AnalyticsConstants.LEAD_CAR_DETAIL_WHATSAPP_SHARE)
            message = await this.getCarDetails()
            ShareType = Constants.CAR_DETAILS_SHARE_TYPE
        }
        else {
            ShareType = Constants.CAR_PHOTOS_SHARE_TYPE
            this.getCarPhoto(ShareType)
            return
            // message = await this.getCarDetails()
            // ShareType = Constants.CAR_DETAILS_SHARE_TYPE
        }

        let url = `whatsapp://send?text=${message}&phone=${mobileNo}`

        const canOpen = await Linking.canOpenURL(url)

        if (!canOpen) {
            Utility.showToast(Strings.WHATSAPP_NOT_INSTALL)
            throw new Error('Provided URL can not be handled')
        }

        this.closeBottomSheet('WHATSAPP', ShareType)
        return Linking.openURL(url)
    }



    onEmailShare = async (typeId) => {
        if (typeId == 3) {
            this.setState({ emailShareType: Constants.CAR_DETAILS_SHARE_TYPE })
        }
        else {
            this.setState({ emailShareType: Constants.CAR_DETAILS_SHARE_TYPE })
        }

        this.closeBottomSheet('closed', '')
        this.setState({ showEmailModel: true })
    }

    onDoneEmail = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_EMAIL_SHARE_CLICK)
        if (Utility.isValueNullOrEmpty(this.state.customerEmail)) {
            Utility.showToast(Strings.ENTER_EMAIL_IDS)
        }
        else if (!Utility.validateEmail(this.state.customerEmail)) {
            Utility.showToast(Strings.ENTER_EMAIL)
        }
        else {
            Keyboard.dismiss()
            try {
                APICalls.shareLeadEmail(this.props.leadData.lead_id, this.props.carData.id, this.state.emailShareType, this.state.customerEmail, this.onSuccessShare, this.onFailureShare, this.props)
            } catch (error) {
                Utility.log(error)
            }

            this.setState({ isLoading: true })
            this.closeBottomSheet('EMAIL', this.state.emailShareType)
        }
    }

    onSuccessShare = (response) => {
        if (response && response.data) {
            Utility.sendEvent(AnalyticsConstants.LEAD_EMAIL_SHARE_SUCCESS)
            Utility.showToast(response.data)
        }
        this.setState({ isLoading: false, showEmailModel: false })
        Utility.log('onSuccessShareLeadEmail===> ', JSON.stringify(response))
    }

    onFailureShare = (response) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_EMAIL_SHARE_FAILURE)
        if (response && response.message) {
            Utility.showToast(response.message)
        }
        this.setState({ isLoading: false, showEmailModel: false })
        Utility.log('onFailureShareLeadEmail===> ', JSON.stringify(response))
    }

    onEmailTextChange = (text) => {
        this.setState({ customerEmail: text })
    }

    render() {
        return (
            <SafeAreaView style={Styles.container}>
                <View style={Styles.container}>
                    <ShareScreen
                        isVisible={this.state.isVisible}
                        closeBottomSheet={this.closeBottomSheet}
                        shareLead={this.state.shareLead}
                        carName={this.state.carName}
                        carData={this.props.carData}
                        leadData={this.props.leadData}
                        isLoading={this.state.isLoading}
                        loadingMsg={this.state.loadingMsg}
                        showLoading={this.state.showLoading}
                        showEmailModel={this.state.showEmailModel}
                        customerEmail={this.state.customerEmail}
                        closeEmailModel={this.closeEmailModel}
                        onEmailTextChange={this.onEmailTextChange}
                        onDoneEmail={this.onDoneEmail}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1
    }
})