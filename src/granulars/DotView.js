import React from 'react';
import { View} from 'react-native';
import {BLACK_54} from '../values/Colors'

const DotView = () => {
    return(
        <View style={{marginHorizontal:10, height:5,width:5,backgroundColor:BLACK_54,borderRadius:2.5}}/>
    )
} ;

export default DotView;

