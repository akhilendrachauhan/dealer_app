import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Utility, Constants } from '../util'
import { Colors, Strings, Dimens } from '../values'
import {icon_no_image} from './../assets/ImageAssets'
export default class FinancierListCard extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let item = this.props.item
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={{ flexDirection: 'row', flex: 1 ,justifyContent:'center',alignItems:'center'}}
                    onPress={() => this.props.onListItemClick(item)}
                    activeOpacity={0.8}>

                    <Image style={styles.image}
                            source={item.logo && item.logo == "" ? icon_no_image : { uri: item.logo}} />
                    <View style={styles.detailContainer}>
                        <Text numberOfLines={1} style={styles.headText}>{item.name || Strings.NA}</Text>
                        {/* <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text numberOfLines={1} style={styles.smallText}>{item.reg_no.substring(0, 4) || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.transmission || Strings.NA}</Text>
                        </View>
                        <Text numberOfLines={1} style={[styles.headText, { color: Colors.BLACK_87 }]}>{Utility.formatCurrency(item.car_price) || Strings.NA}</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.smallText}>{item.km_driven + ' ' + Strings.KM || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.make_year || Strings.NA}</Text>
                            <DotView />
                            <Text style={styles.smallText}>{item.fuel_type || Strings.NA}</Text>
                        </View> */}
                    </View>
                </TouchableOpacity>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: Dimens.padding_xx_large,
        marginBottom: Dimens.margin_normal,
        borderRadius: Dimens.border_radius
    },
    detailContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
        marginLeft: 15,
    },
    image: {
        width: 150,
        height: 50,
        resizeMode: 'contain'
    },
    headText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        fontWeight: 'bold'
    },
    smallText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    menuText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        marginBottom: 10,
        fontFamily: Strings.APP_FONT
    }
})