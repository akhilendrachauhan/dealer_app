import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, Platform } from 'react-native'
import { Colors, Dimens, Strings } from '../../values/index'
import { ScreenStates, ScreenName } from "../../util/Constants"
import * as ImageAssets from '../../assets/ImageAssets'


class ErrorMessage extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let { image, title, message } = this.getErrorTypeMessage(this.props.ScreenStatesError)
        //console.log("render ------------------------------------------------->", this.props.ScreenStatesError)

        return (
            <View style={Styles.viewStyle}>
                <View style={{ alignItems: 'center' }}>
                    {image &&
                        <Image
                            resizeMode={'cover'}
                            alignSelf={'center'}
                            opacity={0.5}
                            style={{ height: 80, width: 100, tintColor: Colors.ORANGE_COLOR }}
                            source={image} />
                    }

                    <Text style={Styles.textStyle}>{title}</Text>

                    <Text style={Styles.subTextStyle}>{message}</Text>

                    {this.props.onRetry &&
                        <TouchableOpacity style={[Styles.button]}
                            activeOpacity={0.8}
                            onPress={this.props.onRetry}>

                            <Text style={Styles.button_text}>Retry</Text>
                        </TouchableOpacity>
                    }

                    {/* {(this.props.screenName == ScreenName.SCREEN_VDP && Platform.OS == 'ios')
                        ?
                        <TouchableOpacity style={[Styles.button]}
                            onPress={this.props.onBack}>

                            <Text style={Styles.button_text}>Back</Text>
                        </TouchableOpacity>
                        :
                        null
                    } */}
                </View>
            </View>
        )
    }

    getErrorTypeImage = (error) => {
        switch (error) {

            case ScreenStates.NO_SEARCH_DATA_FOUND:
                return ImageAssets.no_search_result
                break;

            case ScreenStates.INTERNET_NOT_AVAILABLE:
                return ImageAssets.internet_error
                break;
            case ScreenStates.NO_DATA_FOUND:
                return ImageAssets.internet_error
                break;

            case ScreenStates.NO_DATA_FOUND:
                return ImageAssets.internet_error
                break;

            default:
                return ImageAssets.no_search_result
                break;
        }
    }

    getErrorTypeMessage = (error) => {
        switch (error) {

            case ScreenStates.NO_SEARCH_DATA_FOUND:
                return this.noSearchDataForScreen(this.props.screenName)
                break;

            case ScreenStates.INTERNET_NOT_AVAILABLE:
                return this.noInternetConnectionForScreen(this.props.screenName)
                break;

            case ScreenStates.NO_DATA_FOUND:
                return this.noDataForScreen(this.props.screenName)
                break;

            case ScreenStates.SERVER_ERROR:
                return this.serverErrorForScreen(this.props.screenName)
                break;

            default:
                return { image: ImageAssets.no_activity, title: "No Result Found", message: "Try again!" }
                break;
        }

    }

    noDataForScreen(screenName) {
        switch (screenName) {

            case ScreenName.SCREEN_YOUR_BIDS:
                return { image: ImageAssets.no_bid, title: "No Vehicles to Show", message: "Start bidding on vehicles to see them here" }
                break;

            case ScreenName.SCREEN_WATCH_LIST:
                return { image: ImageAssets.no_watchlist, title: "No Vehicle to Show", message: "Please check your connection and try agan" }
                break;

            case ScreenName.SCREEN_LIVE_AUCTION:
                return { image: ImageAssets.no_bid, title: "No Live Auctions", message: "Keep checking this page frequently" }
                break;

            case ScreenName.SCREEN_UPCOMING_AUCTION:
                return { image: ImageAssets.no_bid, title: "No Upcoming Auctions", message: "Keep checking this page frequently" }
                break;

            case ScreenName.SCREEN_YOUR_WINS:
                return { image: ImageAssets.no_watchlist, title: "No Vehicle to Show", message: "You haven't won any vehicles recently" }
                break;

            case ScreenName.SCREEN_ACTIVITY:
                return { image: ImageAssets.no_activity, title: "No Vehicle to Show", message: "You haven't bid for sometime. Start now!" }
                break;


            default:
                return { image: ImageAssets.no_activity, title: "No Result Found", message: "Try again!" }
                break;
        }
    }

    noSearchDataForScreen(screenName) {
        switch (screenName) {

            case ScreenName.SCREEN_WATCH_LIST:
                return { image: ImageAssets.no_search_result, title: "No Result Found", message: "Try again!" }
                break;

            default:
                return { image: ImageAssets.no_search_result, title: "No Result Found", message: "Try again!" }
                break;
        }
    }

    noInternetConnectionForScreen(screenName) {
        switch (screenName) {

            case ScreenName.SCREEN_DASHBOARD:
                return { image: ImageAssets.internet_error, title: "No Internet Connection", message: "Please check your connection and try again" }
                break;

            default:
                return { image: ImageAssets.internet_error, title: "No Internet Connection", message: "Please check your connection and try again" }
                break;
        }
    }

    serverErrorForScreen(screenName) {
        switch (screenName) {

            case ScreenName.SCREEN_WATCH_LIST:
                return { image: ImageAssets.server_error, title: "Server Error", message: "Sorry! server failed to respond. Please try again" }
                break;

            default:
                return { image: ImageAssets.server_error, title: "Server Error", message: "Sorry! server failed to respond. Please try again" }
                break;
        }
    }
}

const Styles = StyleSheet.create({
    viewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        color: Colors.PRIMARY,
        fontSize: Dimens.text_xx_large,
        fontWeight: 'bold',
        marginTop: Dimens.margin_xxx_large,
        fontFamily: Strings.APP_FONT
    },
    subTextStyle: {
        color: Colors.PRIMARY,
        // marginTop: Dimens.margin_small,
        fontSize: Dimens.text_small,
        fontWeight: '100',
        fontFamily: Strings.APP_FONT
    },
    button: {
        backgroundColor: Colors.PRIMARY,
        paddingVertical: Dimens.padding_normal,
        paddingHorizontal: Dimens.padding_xxx_large,
        marginTop: Dimens.margin_xx_large,
        borderRadius: 5
    },
    button_text: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    }
})
export default ErrorMessage;
