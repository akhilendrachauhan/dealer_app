import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Animated, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Colors} from '../values/index';

/* CustomViewPageIndicator : this is used as dots with viewpager that shows the
 * number of pages
or the current position of page we are on.

CustomableItem :
 * All the customable props can be seen in propTypes.
 */

export default class CustomViewPageIndicator extends Component {
  static propTypes = {
    goToPage: PropTypes.func,
    activePage: PropTypes.number,
    pageCount: PropTypes.number,
    scrollOffset: PropTypes.number,
    scrollValue: PropTypes.object,
    colorActive: PropTypes.string,
    position: PropTypes.string,
    positionOffset: PropTypes.number,
    dotSize: PropTypes.number,
    dotSpace: PropTypes.number
  };

  static defaultProps = {
    colorActive: Colors.PRIMARY,
    position: 'top',
    positionOffset: 0,
    dotSize: 6,
    dotSpace: 4,
    dotColor: Colors.TRANSPARENT,
    dotBorderWidth: 1,
    dotBorderColor: Colors.WHITE
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      viewWidth: 0
    };
  }

    renderIndicator(page, dotSizeStyle, borderRadiusStyle, marginSideSpace) {
        return (
            <TouchableOpacity
                activeOpacity={1}
                style={styles.tab}
                key={'idc_' + page}
                onPress={() => {}}>
                <View style={[dotSizeStyle, borderRadiusStyle, marginSideSpace, {backgroundColor: this.props.dotColor}]}/>
            </TouchableOpacity>
        );
    }

 /* renderIndicator(page, dotSizeStyle, borderRadiusStyle, marginSideSpace) {
    return (
      <TouchableOpacity
        style={styles.tab}
        key={'idc_' + page}
        onPress={() => this.props.goToPage(page)}>
        <View style={[dotSizeStyle, borderRadiusStyle, marginSideSpace, {backgroundColor: this.props.dotColor}]}/>
      </TouchableOpacity>
    );
  }*/

  render() {
    const {
      colorActive,
      position,
      positionOffset,
      activePage,
      scrollOffset,
      scrollValue,
      dotSize,
      dotSpace,
      dotBorderWidth,
      dotBorderColor
    } = this.props;

    //Styles related to dotSize and dotSpace
    const dotSizeStyle = {
      width: dotSize,
      height: dotSize
    };
    const borderRadiusStyle = {
      borderRadius: dotSize / 2,
      borderWidth: dotBorderWidth,
      borderColor: dotBorderColor
    };
    const marginSideSpace = {
      marginLeft: dotSpace,
      marginRight: dotSpace
    };
    const marginSpace = {
      marginLeft: dotSpace
    };

    let pageCount = this.props.pageCount;
    let itemWidth = dotSize + (dotSpace * 2);
    // let offset = (this.state.viewWidth - itemWidth * pageCount) / 2 + itemWidth *
    // this.props.activePage;

    let offsetX = itemWidth * (activePage - scrollOffset);
    let left = scrollValue.interpolate({
      inputRange: [
        0, 1
      ],
      outputRange: [
        offsetX, offsetX + itemWidth
      ]
    });

    let indicators = [];
    for (let i = 0; i < pageCount; i++) {
      indicators.push(
        this.renderIndicator(i, dotSizeStyle, borderRadiusStyle, marginSideSpace)
      );
    }

    var circlePosition;
    if (position === 'top') {
      circlePosition = {
        top: positionOffset
      };
    } else {
      circlePosition = {
        bottom: positionOffset
      };
    }

    return (
      <View style={[styles.indicators, circlePosition]}>
        <View
          style={styles.tabs}
          onLayout={(event) => {
            let viewWidth = event.nativeEvent.layout.width;
            if (!viewWidth || this.state.viewWidth === viewWidth) {
              return;
            }
            this.setState({viewWidth: viewWidth});
          }}>
          {indicators}
          <Animated.View
            style={[
              styles.curDot,
              {
                left
              }, {
                backgroundColor: colorActive
              },
              dotSizeStyle,
              borderRadiusStyle,
              marginSpace
            ]}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tab: {
    alignItems: 'center'
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  curDot: {
    position: 'absolute',
    bottom: 0
  },
  indicators: {
    flex: 1,
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    backgroundColor: Colors.TRANSPARENT
  }
});
