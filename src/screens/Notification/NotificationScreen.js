import React, { Component } from 'react';
import {
    SectionList,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Colors, Strings, Dimens } from '../../values';
import NotificationCell from '../../granulars/NotificationCell';

import NotificationSection from '../../granulars/NotificationSection';
import * as ImageAssets from '../../assets/ImageAssets';
import { TabView, TabBar } from 'react-native-tab-view';
import { Constants} from './../../util'
import LoadingComponent from './../../granulars/LoadingComponent'

let extraData = false;

class NotificationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            data: props.data,
            isReadVisible: false,
            routes: [
                { key: Constants.IS_UNREAD, title: Strings.IS_UNREAD },
                { key: Constants.IS_READ, title: Strings.IS_READ },
            ],
        };
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     this.state.selected = nextProps.selected;
    //     return true;
    // }

    renderScene = ({ route, jumpTo }) => {
        switch (route.key) {
            case Constants.IS_UNREAD:
                return this.renderList(true); 
            case Constants.IS_READ:
                return this.renderList(false);
        }
    };

    renderList = (showUnread) => {
        let conditionMatch = false
        conditionMatch = showUnread ? this.props.data.unreadRecords[0].data.length > 0 || this.props.data.unreadRecords[1].data.length > 0 : this.props.data.readRecords[0].data.length > 0 || this.props.data.readRecords[1].data.length > 0
        //alert(conditionMatch)
        return ( conditionMatch ? 
            <SectionList
                extraData={extraData}
                renderItem={this.getSectionListCell}
                renderSectionHeader={this.getSectionListSection}
                sections={showUnread?this.props.data.unreadRecords : this.props.data.readRecords}
                keyExtractor={(item, index) => item + index}
                showsVerticalScrollIndicator = {false}
                onRefresh={() => this.props.getData(true)}
                refreshing={this.props.refreshing}
                onEndReached={() => this.props.handleLoadMore()}
                onEndReachedThreshold={0.9}
            /> : 
            <View style={{flex: 1,justifyContent :'center',alignItems:'center'}}><Text style={{fontSize: Dimens.text_normal, fontFamily : Strings.APP_FONT}}>{Strings.NO_DATA_FOUND}</Text></View>
        )
    };

    render() {
        extraData = !extraData;

        return (
            <View style={Styles.container}>
                
                <View
                    style={{
                        backgroundColor: Colors.WHITE,
                        borderRadius: 3,
                        marginHorizontal: Dimens.margin_small,
                        paddingHorizontal: Dimens.padding_xxx_large,
                        marginTop: Dimens.margin_small,
                        flex:1,
                    }}>
                    <TabView
                        navigationState={this.state}
                        renderScene={this.renderScene}
                        renderTabBar={props => (
                            <TabBar
                                {...props}
                                style={Styles.containerTab}
                                pressColor={Colors.BLACK_25}
                                activeColor={Colors.BLACK}
                                inactiveColor={Colors.BLACK_54}
                                indicatorStyle={{
                                    backgroundColor: Colors.DASHBOARD_CARD_TEXT,
                                }}
                            />
                        )}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width}}
                    />
                    {this.props.showLoading && <LoadingComponent />}
                </View>
            </View>
        );
    }

    getSectionListCell = ({ item, index, section }) => {
        return (
            <NotificationCell
                onSelectNotificationCell={() =>
                    this.props.onSelectNotificationCell(item, index, section)
                }
                data={item}
                index={index}
                section={section.section}
            />
        );
    };

    getSectionListSection = ({ section : { title,data } }) => {
        return data.length > 0 ? <NotificationSection title={title} /> : null;
    };
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND,
    },
    containerTab: {
        elevation: 0,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.DIVIDER,
        backgroundColor: Colors.WHITE,
    },
});

export default NotificationScreen;
