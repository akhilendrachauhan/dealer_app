import React, {Component} from 'react';
import {View, BackHandler, StyleSheet, SafeAreaView} from 'react-native';
import NotificationScreen from './NotificationScreen';
import * as ImageAssets from '../../assets/ImageAssets';
import {Utility,Constants} from '../../util';
import {Colors,Strings} from "../../values";
import { _ActionBarStyle } from '../../values/Styles';
import * as APICalls from '../../api/APICalls'
import { ScreenStates, ScreenName } from '../../util/Constants'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import ActionBarWrapper from '../../granulars/ActionBarWrapper';
import { removeAllNotification } from "../../util/FirebaseListeners";
let selectedId
class NotificationContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            notifData : {
                "readRecords": [
                  {
                    "title": Strings.TODAY,
                    "data": [
                      
                    ]
                  },
                  {
                    "title": Strings.PREVIOUS,
                    "data": [
                      
                    ]
                  }
                ],
                "unreadRecords": [
                  {
                    "title": Strings.TODAY,
                    "data": []
                      
                  },
                  {
                    "title": Strings.PREVIOUS,
                    "data": [
                      
                    ]
                  }
                ]
              },
            screenState: ScreenStates.IS_LOADING,
            notificationCount: 2,//props.navigation.state.params.notificationCount,
            page_no : 1,
            refreshing : false,
            nextPage : false,
            showLoading : false,
        };
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.getData(false);
        removeAllNotification()
        //this.getModifiedData(this.state.data)
    }

    onRetryClick = ()=>{
        this.getData(false)
    }

    getData = async (fromRefresh) => {
        if(fromRefresh){
            this.state.page_no = 1;
        }
        const values = await Utility.getMultipleValuesFromAsyncStorage([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            if(fromRefresh){
                this.setState({
                    refreshing : true,
                })
            }
            else{
                this.setState({
                    screenState: ScreenStates.IS_LOADING,
                })
            }
            APICalls.getNotificationData(values[0][1],this.state.page_no, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    };

    onSuccess = (response) => {
        if (response && response.data) {
            this.setState({screenState: ScreenStates.NO_ERROR,refreshing : false,nextPage : response.data.nextpage,showLoading : false,page_no : response.data.nextpage ? this.state.page_no + 1 : this.state.page_no,
                data : this.state.page_no == 1 ? response.data.datalist : [...this.state.data, ...response.data.datalist]
            },()=>{
                this.getModifiedData(this.state.data);
            })
        } else if(this.state.data.length == 0){
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, isLoading: false })
        }
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        this.setState({screenState: ScreenStates.SERVER_ERROR,refreshing: false,showLoading : false,})
    }

    handleLoadMore = () => {
        if(this.state.nextPage && this.state.showLoading == false){
           this.getMoreData();
        }
    }

    getMoreData = async () => {
    
        const values = await Utility.getMultipleValuesFromAsyncStorage([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            
            this.setState({
                showLoading: true,
            })
            
            APICalls.getNotificationData(values[0][1],this.state.page_no, this.onMoreSuccess, this.onMoreFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    };

    onMoreSuccess = (response) => {
        if (response && response.data) {
            this.setState({screenState: ScreenStates.NO_ERROR,refreshing : false,nextPage : response.data.nextpage,showLoading : false, page_no : response.data.nextpage ? this.state.page_no + 1 : this.state.page_no,
                data : this.state.page_no == 1 ? response.data.datalist : [...this.state.data, ...response.data.datalist]
            },()=>{
                this.getModifiedData(this.state.data);
            })
        } else if(this.state.data.length == 0){
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, isLoading: false })
        }
    }

    onMoreFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        this.setState({showLoading : false,})
    }

    getModifiedData = (data,fromStatusUpdate) => {
        
        let list1 = [],list2 = []
        
        data.map((item) => {
            let forToday = Utility.checkDateForToday(item.created_date)
            item.display_date = Utility.getDateTimeISO(item.created_date)
            if( forToday == 0 ){
                //item.display_date = this.getFormattedTime(item.created_date,true)
                list1.push(item)
            }else{
                //item.display_date = this.getFormattedTime(item.created_date,false)
                list2.push(item)
            }
            return item
        });

        // let sort = listData[0].data.sort(function (a, b) {
        //     return new Date(b.dateTime) - new Date(a.dateTime);
        // });
        // listData[0].data = sort;

        // sort = listData[1].data.sort(function (a, b) {
        //     return new Date(b.dateTime) - new Date(a.dateTime);

        // });
        
        // listData[1].data = sort;
        
        let unRead = [
            { "title": Strings.TODAY, "data": list1.filter(function(record){return (record.is_read == "0");}) },
            { "title": Strings.PREVIOUS, "data": list2.filter(function(record){return (record.is_read == "0");}) }
        ];
        let read = [
            { "title": Strings.TODAY, "data": list1.filter(function(record){return (record.is_read == "1");}) },
            { "title": Strings.PREVIOUS, "data": list2.filter(function(record){return (record.is_read == "1");}) }
        ];

        this.setState({ notifData: {readRecords:read, unreadRecords: unRead}, notificationCount: 2,screenState : ScreenStates.NO_ERROR },()=>{
            Utility.log('datata'+JSON.stringify(this.state.notifData))
        });
        

    };

    

    render() {
        let actionBarProps = {
            values: {
                title: Strings.NOTIFICATIONS //' /*+ '(' + this.state.notificationCount + ')'*/
            },
            rightIcons: [{
                image: "",
                onPress: () => this.onRetryClick()
            }],
            styleAttr: {
                leftIconImage: ImageAssets.icon_back_arrow,
                rightText: Strings.REFRESH,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }],
                rightTextStyle: {
                    color: 'white',
                    fontWeight: 'bold'
                }},
            actions: {
                onLeftPress: this.onBackPress,
                onRightTextPress: this.onRetryClick
            }
        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
               <View style={Styles.container}>
               <ActionBarWrapper
                    values={actionBarProps.values}
                    actions={actionBarProps.actions}
                    iconMap={actionBarProps.rightIcons}
                    styleAttributes={actionBarProps.styleAttr}
                />
               <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>
                <NotificationScreen
                    data={this.state.notifData}
                    actionBarProps={actionBarProps}
                    onSelectNotificationCell={this.onSelectNotificationCell}
                    showLoading = {this.state.showLoading}
                    refreshing = {this.state.refreshing}
                    handleLoadMore = {this.handleLoadMore}
                    getData = {this.getData}
                />
                </WithLoading>
                </View>
            </SafeAreaView>
            )
    }

    onSelectNotificationCell = async (item, index, section) => {
        if(item.is_read == "0"){
            this.updateNotificationStatus(item.id)
        }
        if(item.lead_id && item.lead_id != "0")
            this.props.navigation.navigate('leadDetail',{leadId : item.lead_id})
    };



    onBackPress = () => {
        this.props.navigation.goBack();
    };

    

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () =>{
        this.onBackPress();
        return true;
    }

    

    updateNotificationStatus = (id) =>{
        selectedId = id
        try {
            APICalls.updateNotificationStatus(id, this.onUpdateSuccess, this.onUpdateFailure, this.props,false)
        } catch (error) {
            Utility.log(error)
        }
    };

    onUpdateSuccess = (response) => {
        if (response && response.message) {
            //Utility.showToast(response.message)
            this.state.data.map((item) => {
                if(item.id == selectedId){
                    item.is_read = "1"
                }
                return item
            });

            this.getModifiedData(this.state.data)
        
        } else {
            //this.setState({ screenState: ScreenStates.NO_DATA_FOUND, isLoading: false })
        }
    }

    onUpdateFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
     }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})
export default NotificationContainer;
