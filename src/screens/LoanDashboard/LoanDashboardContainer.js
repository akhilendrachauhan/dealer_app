import React, { Component } from 'react'
import LoanDashboardScreen from './LoanDashboardScreen'
import { View } from 'react-native'
import { icon_loan_status, icon_pending_action, icon_loan_box } from '../../assets/ImageAssets';
import { ScreenStates, ScreenName } from '../../util/Constants'
import { getLoanDashboard } from '../../api/APICalls'
import * as ScreenHoc from '../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import AsyncStore from '../../util/AsyncStore'
import { Utility, Constants,AnalyticsConstants } from '../../util'
import { Strings } from '../../values';
import DBFunctions from "../../database/DBFunctions";
import ImageUploadFile from '../../util/ImageUploadFile'
import DocumentImageUploadFile from '../../util/DocumentImageUploadFile'
import * as DBConstants from "../../database/DBConstants";
let dbFunctions;

export default class LoanDashboardContainer extends Component {

   constructor(props) {
      super(props);
      dbFunctions = new DBFunctions()
      gridData = [
         { text: Strings.LOAN_STATUS, icon: icon_loan_status, action: this.onLoanStatusClick },
         { text: Strings.CHECK_OFFERS, icon: icon_loan_box, action: this.onOffersClick },
         { text: Strings.PENDING_ACTION, icon: icon_pending_action, action: this.onPendingActionClick },
         { text: Strings.SHARE_QUOTE, icon: icon_loan_status, action: this.onShareLoanClick},
      ];
      this.state = {
         showLoading: false,
         screenState: ScreenStates.NO_ERROR,
         data: [],
         gridData: gridData
      }

      this.subs = [
         this.props.navigation.addListener('didFocus', () => {
            this.getDashboardData()
         })
      ]
   }

   componentDidMount() {
      Utility.sendCurrentScreen(AnalyticsConstants.LOAN_DASHBOARD_SCREEN)
      //this.getDashboardData()
      // NetInfo.isConnected.addEventListener(
      //    Constants.CONNECTION_CHANGE,
      //    this._handleConnectivityChange

      // );

      // Utility.getNetInfo().then(async isConnected => {
      //    if (isConnected) {
      //       this.retryImageUpload()
      //    }
      // })
   }

   _handleConnectivityChange = (isConnected) => {
      if (isConnected == true) {
         this.retryImageUpload()
         this.retryDocumentImageUpload()
      }
   };

   componentWillUnmount() {
      // NetInfo.isConnected.removeEventListener(
      //    Constants.CONNECTION_CHANGE,
      //    this._handleConnectivityChange
      // );
   }

   retryImageUpload = () => {
      let imageUploadFile = new ImageUploadFile()
      try {
         dbFunctions.getDistintCarIds().then(result => {
            Utility.log("Distinct Ids:" + JSON.stringify(result));
            let i = 0;
            if (result.length > 0) {
               for (i = 0; i < result.length; i++) {
                  Utility.log("Distinct Ids:" + result[i][DBConstants.CAR_ID]);
                  imageUploadFile.checkAndUpload(result[i][DBConstants.CAR_ID])
               }
            }
         });

      } catch (error) {
         Utility.log("==================> " + error);
      }
   }

   retryDocumentImageUpload = () => {
      let imageUploadFile = new DocumentImageUploadFile()
      try {
         dbFunctions.getDistintLeadIds().then(result => {
            Utility.log("Distinct Lead Ids:" + JSON.stringify(result));
            let i = 0;
            if (result.length > 0) {
               for (i = 0; i < result.length; i++) {
                  Utility.log("Distinct Ids:" + result[i][DBConstants.LEAD_ID]);
                  imageUploadFile.checkAndUpload(result[i][DBConstants.LEAD_ID])
               }
            }
         });

      } catch (error) {
         Utility.log("==================> " + error);
      }
   }

   getDashboardData = async () => {
      var storeObject = new AsyncStore()
      const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID, Constants.USER_ID])
      Utility.log('DEALER_ID===>' + values[0][1])
      Utility.log('USER_ID===>' + values[1][1])

      try {
         getLoanDashboard(values[0][1], values[1][1], this.onSuccessCallback, this.onFailureCallback, this.props)
      } catch (error) {
         Utility.log(error);
      }

      this.setState({ showLoading: true })
   }

   onSuccessCallback = (response) => {
      this.setState({ data: response.data, showLoading: false })
   }
   onFailureCallback = (response) => {
      this.setState({ showLoading: false })
      if (response && response.message) {
         Utility.showToast(response.message);
      } else {
         Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
      }

   };

   onCountPress = (item) => {
      Utility.log("Dashboard", JSON.stringify(item));
      Utility.log("id", item._key);
      if (item.value && item.value != 0) {
         switch (item._key) {
            case Constants.DASHBOARD_CARD_CAR_COUNT:
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_NON_CLASSIFIED:
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.NON_CLASSIFIED_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_WITHOUT_PHOTOS:
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.WITHOUT_PHOTOS_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_45D_OLD:
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.AGE_45D_STOCK });
               break;
            case Constants.DASHBOARD_CARD_LEAD_CONVERSION:
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_RECEIVED:
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_WALKIN:
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_PENDING:
               this.props.navigation.navigate('buyerLead');
               break;

         }
      }
   }

   onLoanStatusClick = () => {
      Utility.sendEvent(AnalyticsConstants.LOAN_STATUS_CLICK)
      this.props.navigation.navigate("loanLead")
   }

   onOffersClick = () => {
      Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_START);//----- log loan event
      this.props.navigation.navigate("loanStockListing")
   }

   onPendingActionClick = () => {
      Utility.sendEvent(AnalyticsConstants.LOAN_PENDING_ACTIONS_CLICK)
      this.props.navigation.navigate("loanPendingLead")
   }

   onShareQuote = () =>{
      Utility.sendEvent(AnalyticsConstants.FINANCIER_LIST_SCREEN )
      this.props.navigation.navigate("financierList",{ item: null, isLoanShareQuote: true })
   }

   onRetryClick = () => {

   }

   onBackPress = () => {
      this.props.navigation.goBack()
   }

   render() {
      return (
         // <WithLoading
         //    screenState={this.state.screenState}
         //    screenName={ScreenName.SCREEN_DASHBOARD}
         //    onRetry={this.onRetryClick}>

         <LoanDashboardScreen
            onRetryClick={this.onRetryClick}
            screenState={this.state.screenState}
            gridData={this.state.gridData}
            onCountPress={this.onCountPress}
            data={this.state.data}
            showLoading={this.state.showLoading}
            onLoanStatusClick={this.onLoanStatusClick}
            onOffersClick={this.onOffersClick}
            onPendingActionClick={this.onPendingActionClick}
            onShareQuote={this.onShareQuote}
            onBackPress={this.onBackPress}
         />
         // </WithLoading>
      )
   }
}