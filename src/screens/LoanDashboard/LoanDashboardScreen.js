import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { _ActionBarStyle } from '../../values/Styles'
import { Colors, Dimens, Strings } from '../../values'
import ViewPager from '../../nodeModuleChanges/react_native_viewpager/ViewPager'
import DashboardCard from "../../granulars/DashboardCard"
import CustomViewPageIndicator from "../../granulars/CustomViewPagerIndicator"
import { dashboard_bg, icon_loan_status, icon_pending_action, icon_loan_box, icon_back_arrow } from '../../assets/ImageAssets'
import HomeGridView from '../../granulars/HomeGridView'
import LoadingComponent from '../../granulars/LoadingComponent'

const dataSource = new ViewPager.DataSource({
    pageHasChanged: (p1, p2) => p1 !== p2,
});
let onCountPress


export default class LoanDashboardScreen extends Component {

    constructor(props) {
        super(props);
        onCountPress = props.onCountPress;
    }
    renderPage(item) {
        return (
            <View style={{ flex: 1, paddingHorizontal: Dimens.padding_15 }}>
                <DashboardCard
                    onCountPress={() => { }}
                    data={item} />
            </View>

        );
    }

    renderPageIndicators(size) {
        return (
            <CustomViewPageIndicator
                positionOffset={305}
                dotSize={8}
                dotSpace={6}
                dotBorderWidth={1}
                dotBorderColor={Colors.WHITE} />
        );
    }
    render() {
        let gridData = [
            { text: Strings.LOAN_STATUS, icon: icon_loan_status, action: this.props.onLoanStatusClick },
            { text: Strings.SUBMIT_APPLICATION, icon: icon_loan_box, action: this.props.onOffersClick },
            { text: Strings.PENDING_ACTION, icon: icon_pending_action, action: this.props.onPendingActionClick },
            { text: Strings.SHARE_QUOTE, icon: icon_loan_status, action: this.props.onShareQuote },
        ]

        let data = dataSource.cloneWithPages(this.props.data);
        let actionBarProps = {
            values: { title: Strings.LOAN_DASHBOARD },
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            }
            ,
            actions: {
                onLeftPress: () => this.props.onBackPress()
            }
        }

        let noOfRow = parseInt(gridData.length / 4) + 1
        let height = 70 * noOfRow
        //let height = Platform.OS === 'ios' ? 190 : 200;
        if (gridData.length <= 4) {
            noOfRow = 2
            height = 140
        }
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK, marginBottom: Dimens.margin_bottom_iphone }}>
                <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                    <View style={{ flex: 1 }}>
                        <Image style={styles.backgroundContainer}
                            source={dashboard_bg} />
                        <View style={styles.cardContainer}>
                            <ActionBarWrapper
                                values={actionBarProps.values}
                                actions={actionBarProps.actions}
                                iconMap={actionBarProps.rightIcons}
                                styleAttributes={actionBarProps.styleAttr}
                            />
                            <View style={styles.viewPagerContainer}>
                                {this.props.data.length > 0 ?
                                    <View style={styles.viewPager}>
                                        <ViewPager
                                            dataSource={data}
                                            renderPage={this.renderPage}
                                            isLoop={false}
                                            autoPlay={false}
                                            renderPageIndicator={() => this.renderPageIndicators(this.props.data.length)}
                                        />
                                    </View> : <LoadingComponent />}
                            </View>

                        </View>
                    </View>
                    {/* { this.props.showLoading && <LoadingComponent/>} */}
                    <View style={{ height: height, justifyContent: 'space-between' }}>
                        <HomeGridView style={{ flex: 1 }} rowCount={noOfRow}
                            gridData={gridData} />

                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    backgroundContainer: {
        height: '100%',
        width: '100%',
        resizeMode: 'cover'
    },
    cardContainer: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    viewPagerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewPager: {
        height: 320,
        width: '100%'
    }
});