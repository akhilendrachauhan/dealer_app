import React from 'react';
import { BackHandler, Image, FlatList, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions,SafeAreaView } from 'react-native';
import {NeonHandler} from "./NeonHandler";
import {Strings, Colors} from "../../values";
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import * as RNFS from "react-native-fs";
import { Utility } from '../../util';
import CameraRoll from "@react-native-community/cameraroll";

export default class extends React.PureComponent {
    static defaultProps = {
        maxSize: 1,
        autoConvertPath: false,
        assetType: 'Photos',
        groupTypes: 'All',
        okLabel: 'OK',
        cancelLabel: 'Cancel',
        deleteLabel: 'Delete',
        useVideoLabel: 'Use Video',
        usePhotoLabel: 'Use Photo',
        previewLabel: 'Preview',
        choosePhotoTitle: 'Choose Photo',
        maxSizeChooseAlert: (number) => 'You can only choose ' + number + ' photos at most',
        maxSizeTakeAlert: (number) => 'You can only take ' + number + ' photos at most',
        supportedOrientations: ['portrait', 'landscape']
    };

    constructor(props) {
        super(props);
        //this.options = NeonHandler.getOptions();
        this.state = {
            data: [],
            selectedItems: props.navigation.state.params.selectedItems ? props.navigation.state.params.selectedItems : [],
            maxSize : props.navigation.state.params.maxSize ? props.navigation.state.params.maxSize : null,
            selectedDocument : props.navigation.state.params.selectedDocument ? props.navigation.state.params.selectedDocument : null,
            isChild : props.navigation.state.params.isChild ? props.navigation.state.params.isChild : false,
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        CameraRoll.getPhotos({
            first: 1000000,
            groupTypes: Platform.OS === 'ios' ? 'All' : undefined,
            assetType: 'Photos',
        }).then((result) => {
            const arr = result.edges.map(item => item.node);
            const dict = arr.reduce((prv, cur) => {
                const curValue = {
                    type: cur.type,
                    location: cur.location,
                    timestamp: cur.timestamp,
                    ...cur.image,
                };
                if (!prv[cur.group_name]) {
                    prv[cur.group_name] = [curValue];
                } else {
                    prv[cur.group_name].push(curValue);
                }
                return prv;
            }, {});
            const data = Object.keys(dict)
                .sort((a, b) => {
                    const rootIndex = 'Camera Roll';
                    if (a === rootIndex) {
                        return -1;
                    } else if (b === rootIndex) {
                        return 1;
                    } else {
                        return a < b ? -1 : 1;
                    }
                })
                .map(key => ({name: key, value: dict[key]}));
            this.setState({data});
            Utility.log("data:"+JSON.stringify(data))
        });
    }

    handleBackPress = () => {
        this.onBackPress()
        return true;
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    };

    componentWillUnmount() {
        this.backHandler.remove()
    }

    _clickOk = () => {
        if (this.state.selectedItems.length > 0) {
            if(this.state.selectedDocument)
                this.props.navigation.state.params.galleryCallback && this.props.navigation.state.params.galleryCallback(this.state.selectedItems,this.state.selectedDocument,this.state.isChild);
            else
                this.props.navigation.state.params.galleryCallback && this.props.navigation.state.params.galleryCallback(this.state.selectedItems);
            this.onBackPress();
            //this._onFinish(this.state.selectedItems)
        }
    };

    _onFinish = (data) => {
        if (Platform.OS === 'ios') {
            const promises = data.map((item, index) => {
                const {uri} = item;
                // const params = uri.split('?');
                // if (params.length < 1) {
                //     throw new Error('Unknown URI：' + uri);
                // }
                // const keyValues = params[1].split('&');
                // if (keyValues.length < 2) {
                //     throw new Error('Unknown URI：' + uri);
                // }
                // const kvMaps = keyValues.reduce((prv, cur) => {
                //     const kv = cur.split('=');
                //     prv[kv[0]] = kv[1];
                //     return prv;
                // }, {});
                // const itemId = kvMaps.id;
                // const ext = kvMaps.ext.toLowerCase();
                // const destPath = RNFS.CachesDirectoryPath + '/' + itemId + '.' + ext;
                // let promise;
                // if (item.type === 'ALAssetTypePhoto') {
                //     promise = RNFS.copyAssetsFileIOS(uri, destPath, 0, 0);
                // } else if (item.type === 'ALAssetTypeVideo') {
                //     promise = RNFS.copyAssetsVideoIOS(uri, destPath);
                // } else {
                //     throw new Error('Unknown URI：' + uri);
                // }
                var regex = /:\/\/(.{36})\//i;
                var result = uri.match(regex);
                const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
                
                promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
                return promise
                    .then((resultUri) => {
                        data[index].uri = resultUri;
                    });
            });
            Promise.all(promises)
                .then(() => {
                    this.props.navigation.state.params.galleryCallback && this.props.navigation.state.params.galleryCallback(this.state.selectedItems);
                    this.onBackPress();
                });
        } else if (Platform.OS === 'android') {
            const promises = data.map((item, index) => {
                return RNFS.stat(item.uri)
                    .then((result) => {
                        data[index].uri = result.originalFilepath;
                    });
            });
            Promise.all(promises)
                .then(() => {
                    this.props.navigation.state.params.galleryCallback && this.props.navigation.state.params.galleryCallback(this.state.selectedItems);
                    this.onBackPress();
                });
        } else {
            this.props.navigation.state.params.galleryCallback && this.props.navigation.state.params.galleryCallback(this.state.selectedItems);
            this.onBackPress();
        }
    };

    render() {
        let actionBarProps = {
            values: {title: Strings.GALLERY},
            rightIcons: this.state.selectedItems.length > 0 ? [{
                image: require('../../assets/drawable/right.png'),
                onPress: () => this._clickOk()
            }]: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png')
            }
            ,
            actions: {
                onLeftPress: this.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex:1,backgroundColor:Colors.PRIMARY_DARK}}>
            <View style={styles.view}>
                <ActionBarWrapper
                    values={actionBarProps.values}
                    actions={actionBarProps.actions}
                    iconMap={actionBarProps.rightIcons}
                    styleAttributes={actionBarProps.styleAttr}
                />
                <FlatList
                    style={[styles.listView]}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={(item) => item.name}
                    extraData={this.state}
                />
            </View>
            </SafeAreaView>
        );
    }

    _renderItem = ({item}) => {
        const itemUris = new Set(item.value.map(i => i.uri));
        const selectedItems = this.state.selectedItems
            .filter(i => itemUris.has(i.uri));
        const selectedCount = selectedItems.length;
        return (
            <TouchableOpacity onPress={this._clickRow.bind(this, item)}>
                <View style={styles.cell}>
                    <View style={styles.left}>
                        <Image
                            source={{uri: item.value[0].uri}}
                            style={styles.image}
                            resizeMode={'cover'}
                        />
                        <Text style={styles.text}>
                            {item.name + ' (' + item.value.length + ')'}
                        </Text>
                    </View>
                    <View style={styles.right}>
                        {selectedCount > 0 && (
                            <Text style={styles.selectedcount}>
                                {'' + selectedCount}
                            </Text>
                        )}
                        <Image
                            source={require('../../assets/drawable/arrow.png')}
                            style={styles.arrow}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    _onBackFromAlbum = (items) => {
        this.setState({selectedItems: [...items]});
    };

    _clickCancel = () => {
        this.props.callback && this.props.callback([]);
    };

    _clickRow = (item) => {
        this.props.navigation.navigate('galleryFiles', {
            ...this.props,
            groupName: item.name,
            photos: item.value,
            selectedItems: this.state.selectedItems,
            onBack: this._onBackFromAlbum,
            maxSize : this.state.maxSize
        });
    };
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: 'white',
    },
    safeView: {
        flex: 1,
    },
    listView: {
        flex: 1,
    },
    cell: {
        height: 60,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 16,
        paddingRight: 16,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#e6e6ea',
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        overflow: 'hidden',
        width: 44,
        height: 44,
    },
    text: {
        fontSize: 16,
        color: 'black',
        marginLeft: 10,
    },
    right: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    selectedcount: {
        width: 18,
        height: 18,
        ...Platform.select({
            ios: {lineHeight: 18},
            android: {textAlignVertical: 'center'},
        }),
        fontSize: 11,
        textAlign: 'center',
        color: 'white',
        backgroundColor: '#e15151',
        borderRadius: 9,
        overflow: 'hidden',
    },
    arrow: {
        width: 13,
        height: 16,
        marginLeft: 10,
        marginRight: 0,
        tintColor:'gray'
    },
});
