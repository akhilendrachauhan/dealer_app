import React from 'react';
import {BackHandler, SafeAreaView, Alert, Dimensions, FlatList, Image, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import * as RNFS from 'react-native-fs';
import {NeonHandler} from "./NeonHandler";
import {Colors, Strings, Dimens} from "../../values";
import ActionBarWrapper from "../../granulars/ActionBarWrapper";

let galleryFileFormat = {
    type: 'image/jpeg',
    location: {latitude: 37.421998333333335, longitude: -122.08400000000002},
    timestamp: 1569234158.672,
    height: 1280,
    width: 960,
    uri: 'file:///storage/emulated/0/DCIM/Camera/IMG_20190923_155238.jpg'
};


export default class extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selectedItems: [...this.props.navigation.state.params.selectedItems],
            maxSize : props.navigation.state.params.maxSize ? props.navigation.state.params.maxSize : null,
        };
    }

    componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    };
    handleBackPress = () => {
        this.onBackPress()
        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }
    render() {
        let actionBarProps = {
            values: {title: Strings.PHOTOS},
            rightIcons: [{
                image: require('../../assets/drawable/right.png'),
                onPress: () => this._clickBack()
            }] ,
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png')
            }
            ,
            actions: {
                onLeftPress: this.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex:1,backgroundColor:Colors.PRIMARY_DARK}}>
            <View style={styles.view}>
                <ActionBarWrapper
                    values={actionBarProps.values}
                    actions={actionBarProps.actions}
                    iconMap={actionBarProps.rightIcons}
                    styleAttributes={actionBarProps.styleAttr}
                />
                <FlatList
                    key={this._column()}
                    style={[styles.list]}
                    renderItem={this._renderItem}
                    data={this.props.navigation.state.params.photos}
                    keyExtractor={item => item.uri}
                    numColumns={this._column()}
                    extraData={this.state}
                />
            </View>
            </SafeAreaView>
        );
    }

    _renderItem = ({item, index}) => {
        const edge = (Dimensions.get('window').width - 40) / 3;
        const isSelected = this.state.selectedItems.some(obj => obj.uri === item.uri);
        const backgroundColor = isSelected ? Colors.PRIMARY : 'transparent';
        const hasIcon = isSelected || this.state.selectedItems.length < NeonHandler.getOptions().maxSize;
        return (
            <TouchableOpacity onPress={this._clickCell.bind(this, item)}>
                <View style={{padding: 5}}>
                    <Image
                        key={index}
                        source={{uri: item.uri}}
                        style={{width: edge, height: edge, overflow: 'hidden'}}
                        resizeMode={'cover'}
                    />
                    {hasIcon && (
                        <View style={styles.selectView}>
                            <View style={[styles.selectIcon, {backgroundColor}]}>
                                {isSelected && (
                                    <Image
                                        resizeMode={'contain'}
                                        source={require('../../assets/drawable/right.png')}
                                        style={styles.selectedIcon}
                                    />
                                )}
                            </View>
                        </View>
                    )}
                </View>
            </TouchableOpacity>
        );
    };

    _onFinish = (data) => {
        if (NeonHandler.getOptions().autoConvertPath && Platform.OS === 'ios') {
            const promises = data.map((item, index) => {
                const {uri} = item;
                const params = uri.split('?');
                if (params.length < 1) {
                    throw new Error('Unknown URI：' + uri);
                }
                const keyValues = params[1].split('&');
                if (keyValues.length < 2) {
                    throw new Error('Unknown URI：' + uri);
                }
                const kvMaps = keyValues.reduce((prv, cur) => {
                    const kv = cur.split('=');
                    prv[kv[0]] = kv[1];
                    return prv;
                }, {});
                const itemId = kvMaps.id;
                const ext = kvMaps.ext.toLowerCase();
                const destPath = RNFS.CachesDirectoryPath + '/' + itemId + '.' + ext;
                let promise;
                if (item.type === 'ALAssetTypePhoto') {
                    promise = RNFS.copyAssetsFileIOS(uri, destPath, 0, 0);
                } else if (item.type === 'ALAssetTypeVideo') {
                    promise = RNFS.copyAssetsVideoIOS(uri, destPath);
                } else {
                    throw new Error('Unknown URI：' + uri);
                }
                return promise
                    .then((resultUri) => {
                        data[index].uri = resultUri;
                    });
            });
            Promise.all(promises)
                .then(() => {
                    this.props.callback && this.props.callback(data);
                });
        } else if (NeonHandler.getOptions().autoConvertPath && Platform.OS === 'android') {
            const promises = data.map((item, index) => {
                return RNFS.stat(item.uri)
                    .then((result) => {
                        data[index].uri = result.originalFilepath;
                    });
            });
            Promise.all(promises)
                .then(() => {
                    this.props.callback && this.props.callback(data);
                });
        } else {
            this.props.callback && this.props.callback(data);
        }
    };

    _clickBack = () => {
        this.props.navigation.state.params.onBack && this.props.navigation.state.params.onBack(this.state.selectedItems);
        this.onBackPress();
    };

    _clickCell = (itemuri) => {
        // if (this.state.selectedItems.length >= this.state.maxSize) {
        //     Alert.alert('', "can not");
        //     return
        // }
        //alert(this.state.maxSize) 
        const isSelected = this.state.selectedItems.some(item => item.uri === itemuri.uri);
        if (isSelected) {
            const selectedItems = this.state.selectedItems.filter(item => item.uri !== itemuri.uri);
            this.setState({
                selectedItems: [...selectedItems]
            });
        } 
        else if (this.state.maxSize && this.state.selectedItems.length >= this.state.maxSize) {
            Alert.alert('', Strings.MAX_SIZE_CHOOSE_MSG1+this.state.maxSize+Strings.MAX_SIZE_CHOOSE_MSG2);
        } 
        else {
            this.setState({
                selectedItems: [...this.state.selectedItems, itemuri]
            });
        }
    };

    _clickOk = () => {
        if (this.state.selectedItems.length > 0) {
            this._onFinish(this.state.selectedItems);
        }
    };

    _column = () => {
        return 3;
    };
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: 'white',
    },
    safeView: {
        flex: 1,
    },
    list: {
        flex: 1,
        padding: 5
    },
    selectView: {
        position: 'absolute',
        top: 4,
        right: 4,
        width: 30,
        height: 30,
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
    },
    selectIcon: {
        marginTop: 2,
        marginRight: 2,
        width: 20,
        height: 20,
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 10,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    selectedIcon: {
        width: 13,
        height: 13,
        tintColor: Colors.WHITE
    },
    bottom: {
        height: 44,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopWidth: StyleSheet.hairlineWidth,
        borderTopColor: '#e6e6ea',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#e6e6ea',
    },
    previewButton: {
        marginLeft: 10,
        padding: 5,
        fontSize: 16,
        color: '#666666',
    },
    okButton: {
        marginRight: 15,
        paddingHorizontal: 15,
        height: 30,
        ...Platform.select({
            ios: {lineHeight: 30},
            android: {textAlignVertical: 'center'}
        }),
        borderRadius: 6,
        overflow: 'hidden',
        fontSize: 16,
        color: 'white',
        backgroundColor: '#e15151',
    },
});
