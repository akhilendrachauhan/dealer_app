/*
* Params required(all mandatory)
* 1. callback - will return row data
* 2. hintText - placeHolder for textInput
* 3. dbQuery - query to get data from database
* */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import * as Utility from "../util/Utility";
import {
    FlatList,
    Keyboard,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Image,
    BackHandler
} from "react-native";
import * as Color from "../values/Colors";
import * as Strings from '../values/Strings';
import DBFunctions from "../database/DBFunctions";
import * as DBConstants from '../database/DBConstants';
import { Colors } from '../values';
import LoadingComponent from '../components/LoadingComponent'

let self;
export default class SearchContainer extends Component {

    static defaultProps = {
        title: '',
        callback: function (item) {
            Utility.log("Unhandled Callback");
        }
    };

    constructor(props) {
        super(props);
        self = this;
        this.state = {
            query: "",
            data: [],
            filteredData: [],
            isLoading: true,
            loadingMsg: '',
        };
        this.navProps = this.props.navigation.state.params;
        if (this.navProps) {
            this.hintText = this.navProps.hintText ? this.navProps.hintText : 'Type to search';
            this.dbQuery = this.navProps.dbQuery
        }
    }

    componentDidMount() {
        this.findData();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }


    findData = (makeQuery) => {
        let dbFunctions = new DBFunctions();
        if (this.dbQuery) {
            dbFunctions.getDataByQuery(this.dbQuery).then(result => {
                Utility.log('DATA length', result.length);
                this.setState({
                    data: result,
                    isLoading: false
                })
            });
        } else {
            console.warn('Query not found')
        }
    };

    itemSelected = (item) => {
        Keyboard.dismiss();
        if (this.navProps.callback)
            this.navProps.callback(item);
        this.props.navigation.goBack(null);
    };

    getDisplayText = (item) => {
        let val = item.text;
        return <Text allowFontScaling={false}
            style={styles.searchListItem}>{Utility.getHighlightedText(this.state.query, val)}</Text>
    };

    onTextChange = (searchText) => {
        let query = searchText.replace(/[^a-zA-Z0-9- ]/g, "");
        const data = this.state.data;
        const regex = new RegExp(`${query.trim()}`, 'i');
        let filteredData = data.filter(film => film['text'].search(regex) >= 0);
        this.setState({
            query: query,
            render: !this.state.render,
            filteredData: filteredData
        })
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={styles.container}>
                    <View style={styles.searchText}>
                        <TouchableOpacity onPress={() => this.onBackPress()}>
                            <Image
                                style={{ tintColor: Color.BLACK, height: 18, width: 18, marginLeft: 12, marginRight: 10 }}
                                source={require('../assets/drawable/back.png')} />
                        </TouchableOpacity>
                        <View style={{ flex: 1 }}>
                            <TextInput
                                ref={(input) => this.SearchInput = input}
                                style={styles.textInput}
                                autoFocus = {true}
                                placeholder={this.hintText}
                                value={this.state.query}
                                autoFocus={true}
                                underlineColorAndroid={Color.TRANSPARENT}
                                // keyboardType={'default'}
                                onChangeText={(query) => this.onTextChange(query)}
                                allowFontScaling={false}
                            />
                        </View>
                    </View>
                    <View style={{
                        marginHorizontal: 8,
                        marginBottom: 5
                    }}>
                        <Text allowFontScaling={false} style={{
                            marginTop: 16,
                            color: Color.BLACK_54,
                            width: "100%",
                            fontWeight: "bold"
                        }}>Search Results</Text>
                    </View>
                    <FlatList
                        style={{ marginHorizontal: 8, paddingTop: 10, marginBottom: 10 }}
                        data={this.state.filteredData}
                        renderItem={
                            ({ item }) => <TouchableOpacity onPress={() => this.itemSelected(item)}
                            >
                                <View>{this.getDisplayText(item)}</View>
                            </TouchableOpacity>}
                        extraData={this.state.query}
                        keyboardShouldPersistTaps="handled"
                        keyExtractor={(item, index) => index.toString()}
                    />

                    {this.state.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
                </View>
            </SafeAreaView>
        );
    }
}

SearchContainer.propTypes = {
    callback: PropTypes.func
};

const styles = StyleSheet.create({
    container: { flexDirection: "column", flex: 1, backgroundColor: Colors.WHITE },
    searchText: {
        borderWidth: 0.5,
        backgroundColor: "white",
        paddingVertical: 0,
        flexDirection: "row",
        alignItems: "center",
        height: 56,
    },
    divider: { height: 0.5, width: "100%", marginBottom: 10, backgroundColor: "#9E9E9E" },
    searchListItem: {
        width: "100%",
        paddingHorizontal: 16,
        paddingVertical: 10,
        fontSize: 16,
        borderWidth: 0.5,
        borderColor: "transparent",
        borderBottomColor: "#BDBDBD",
        fontFamily: Strings.APP_FONT
    },
    textInput: {
        width: "100%",
        borderColor: 'gray',
        color: Color.BLACK_85,
        paddingHorizontal: 8,
        fontSize: 16,
        fontFamily: Strings.APP_FONT
    }
});
