import React, { Component } from 'react'
import DashboardScreen from './DashboardScreen'
import { View, Linking, AppState, DeviceEventEmitter, Platform } from 'react-native'
import { icon_my_stock, icon_add_lead, icon_loan_box,icon_pending_action } from './../../assets/ImageAssets';
import { ScreenStates, ScreenName } from '../../util/Constants'
import { default as APICalls, getDashboardData, getMMV, getAllCity, getDealerLocality, getFinancierMMV } from '../../api/APICalls'
import * as ScreenHoc from './../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import AsyncStore from '../../util/AsyncStore'
import { Utility, Constants, AnalyticsConstants } from './../../util'
import { Strings } from '../../values';
import DBFunctions from "../../database/DBFunctions";
import ImageUploadFile from '../../util/ImageUploadFile'
import DocumentImageUploadFile from '../../util/DocumentImageUploadFile'
import NetInfo from "@react-native-community/netinfo"
import * as DBConstants from "../../database/DBConstants";
import firebase from 'react-native-firebase';
import { registerAppListener, unRegisterNotificationListener } from "../../util/FirebaseListeners";
let dbFunctions;
let that;
export default class DashboardContainer extends Component {

   constructor(props) {
      super(props);
      dbFunctions = new DBFunctions()
      gridData = [
         { text: Strings.STOCK, icon: icon_my_stock, action: this.onStockClick },
         { text: Strings.BUYER_LEADS, icon: icon_add_lead, action: this.onLeadsClick },
         { text: Strings.LOAN, icon: icon_loan_box, action: this.onLoanClick },
         { text: Strings.PACKAGE_DETAIL, icon: icon_pending_action, action: this.onPackageDetailClick},
      ];
      this.state = {
         showLoading: false,
         isShowTab: false,
         isShowLoanTab: false,
         screenState: ScreenStates.NO_ERROR,
         data: [],
         gridData: gridData,
         appState: AppState.currentState,
         showSyncLoading: false,
         notificationUnreadCount: 0,
         showSnackbar: false,
         snackBarText: 'Syncing data...',
         isSyncCall: this.props.navigation.state.params && this.props.navigation.state.params.isSyncCall
      }
      that = this

      this.subs = [
         this.props.navigation.addListener('didFocus', () => {
            this.getDashboardData()
            // Linking.getInitialURL().then(url => {
            //    alert(url)
            // });
         })
      ]
   }

   async componentDidMount() {
      let total_financier_mmv = 0
      Utility.sendCurrentScreen(AnalyticsConstants.DASHBOARD_SCREEN)
      Utility.getMultipleValuesFromAsyncStorage([Constants.USER_ID, Constants.NAME, Constants.EMAIL, Constants.SFA_USER_DATA,Constants.TOTAL_FINANCIER_MMV]).then(values => {
         let sfa = values[3][1] ? JSON.parse(values[3][1]) : null
         total_financier_mmv = values[4][1] ? values[4][1] : 0
         let sfa_email_id = ''
         if (sfa) {
            sfa_email_id = sfa['email']
         }
         // Utility.setUserProperties(AnalyticsConstants.USER_INFO, {
         //       'user_id' : values[0][1],
         //       'user_name' : values[1][1],
         //       'user_email' : values[2][1],
         //       'sfa_email' : sfa_email_id
         // })

         Utility.setUserProperty('user_id', values[0][1])
         Utility.setUserProperty('user_name', values[1][1])
         Utility.setUserProperty('user_email', values[2][1])
         Utility.setUserProperty('sfa_email', sfa_email_id)
         Utility.sendUserId(values[0][1])
         //Utility.logAppOpenEvent()

      });

      if(Platform.OS == 'ios'){
         this.firebaseNotificationPermission()
      }

      const channel = new firebase.notifications.Android.Channel(Constants.NOTIFICATION_CHANNEL_ID, Constants.NOTIFICATION_CHANNEL_DESC, firebase.notifications.Android.Importance.Max)
         .setDescription(Constants.NOTIFICATION_CHANNEL_DESC);

      // Create the channel
      firebase.notifications().android.createChannel(channel);

      registerAppListener(this);



      this.unsubscribe = NetInfo.addEventListener(state => {
         this._handleConnectivityChange(state.isConnected)
      });

      this.subscription = DeviceEventEmitter.addListener('deepLinking', function (e) {
         if (e)
            that.props.navigation.navigate("splash", { url: e })
      })

      let isShowFinance = await Utility.isShowFinance()
      Utility.log('isShowFinance===>>>>', isShowFinance)
      if (isShowFinance) {
         this.setState({ isShowLoanTab: true, isShowTab: true })
      }
      else {
         this.setState({ isShowLoanTab: false, isShowTab: true })
      }

      if (Constants.APP_TYPE == Constants.INDONESIA && this.state.isSyncCall) {
         let page = 1;
         dbFunctions.getFinancierMMVTotalCount().then(result => {
            if(result == 0 || result < total_financier_mmv ){
            //dbFunctions.deleteMYP().then(result => {
               this.setState({ showSnackbar: true },()=>{
                  this.checkAndSyncFinancierMMV(page)
               })
               
            //})
            }
         })
      }
   }

   firebaseNotificationPermission = async ()=>{
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {
         // user has permissions
      } else {
         try {
            await firebase.messaging().requestPermission();
            // User has authorised
        } catch (error) {
            // User has rejected permissions
        }
      }
   }

   _handleAppStateChange = (nextAppState) => {
      if (
         this.state.appState.match(/inactive|background/) &&
         nextAppState === 'active'
      ) {
         Linking.getInitialURL().then(url => {
            // alert(url)
         });
         // console.log('App has come to the foreground!');
      }
      // alert(nextAppState)
      this.setState({ appState: nextAppState });
   };

   _handleConnectivityChange = (isConnected) => {
      if (isConnected == true) {
         Utility.retryImageUpload()
         Utility.retryLoanDocumentImageUpload();
         //this.retryDocumentImageUpload()
      }
   };

   componentWillUnmount() {
      this.unsubscribe()
      AppState.removeEventListener('change', this._handleAppStateChange);
   }

   retryImageUpload = () => {
      let imageUploadFile = new ImageUploadFile()
      try {
         dbFunctions.getDistintCarIds().then(result => {
            Utility.log("Distinct Ids:1" + JSON.stringify(result));
            let i = 0;
            if (result.length > 0) {
               for (i = 0; i < result.length; i++) {
                  Utility.log("Distinct Ids:" + result[i][DBConstants.CAR_ID]);
                  imageUploadFile.checkAndUpload(result[i][DBConstants.CAR_ID])
               }
            }
         });

      } catch (error) {
         Utility.log("==================> " + error);
      }
   }

   retryDocumentImageUpload = () => {
      let imageUploadFile = new DocumentImageUploadFile()
      try {
         dbFunctions.getDistintLeadIds().then(result => {
            Utility.log("Distinct Lead Ids:" + JSON.stringify(result));
            let i = 0;
            if (result.length > 0) {
               for (i = 0; i < result.length; i++) {
                  Utility.log("Distinct Ids:" + result[i][DBConstants.LEAD_ID]);
                  imageUploadFile.checkAndUpload(result[i][DBConstants.LEAD_ID])
               }
            }
         });

      } catch (error) {
         Utility.log("==================> " + error);
      }
   }

   getDashboardData = (fromRefresh) => {
      var storeObject = new AsyncStore();
      storeObject.getAsyncValueInPersistStore(Constants.DEALER_ID).then((dealer_id) => {
         try {
            this.setState({ showLoading: this.state.data.length === 0 });
            getDashboardData(dealer_id, this.onSuccessCallback, this.onFailureCallback, this.props)
         } catch (error) {
            Utility.log(error);
         }

      }).catch((error) => {
         Utility.log("errrrrrrrrrrrrrrrrrrrrr", 'error');
      });

   }

   onSuccessCallback = (response) => {
      this.setState({ data: response.data, notificationUnreadCount: response.notification_unread_count ? response.notification_unread_count : 0, showLoading: false }, () => {
         //firebase.notifications().setBadge(this.state.notificationUnreadCount)
      })
   }

   onFailureCallback = (response) => {
      this.setState({ showLoading: false })
      if (response && response.message) {
         Utility.showToast(response.message);
      } else {
         //Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
      }

   };

   onCountPress = (item) => {
      Utility.log("Dashboard", JSON.stringify(item));
      Utility.log("id", item._key);
      if (item.value && item.value != 0) {
         switch (item._key) {
            case Constants.DASHBOARD_CARD_CAR_COUNT:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_STOCK_CARD_CLICK)
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_NON_CLASSIFIED:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_NON_CLASSIFIED_CLICK)
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.NON_CLASSIFIED_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_WITHOUT_PHOTOS:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_WITHOUT_PHOTOS_CLICK)
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.WITHOUT_PHOTOS_STOCK });
               break;
            case Constants.DASHBOARD_CARD_CAR_45D_OLD:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_OLD_STOCK_CLICK)
               this.props.navigation.navigate('stockListing', { stockType: Constants.ACTIVE_STOCK, activeStockType: Constants.AGE_45D_STOCK });
               break;
            case Constants.DASHBOARD_CARD_LEAD_CONVERSION:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_LEAD_CONVERSION_CLICK)
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_RECEIVED:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_LEAD_RECEIVED_CLICK)
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_WALKIN:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_LEAD_WALKIN_CLICK)
               this.props.navigation.navigate('buyerLeadList', { item: item });
               break;
            case Constants.DASHBOARD_CARD_LEAD_PENDING:
               Utility.sendEvent(AnalyticsConstants.DASHBOARD_LEAD_PENDING_CLICK)
               this.props.navigation.navigate('buyerLead');
               break;

         }
      }
   }

   onLeadsClick = () => {
      Utility.sendEvent(AnalyticsConstants.DASHBOARD_LEAD_BOTTOM_CLICK)
      this.props.navigation.navigate("buyerLead")
   }

   onStockClick = () => {
      Utility.sendEvent(AnalyticsConstants.DASHBOARD_STOCK_BOTTOM_CLICK)
      this.props.navigation.navigate("stockListing")
   }

   onLoanClick = () => {
      // this.props.navigation.navigate("loanStockListing")
      Utility.sendEvent(AnalyticsConstants.DASHBOARD_LOAN_BOTTOM_CLICK)
      this.props.navigation.navigate("loanDashboard")
   }

   onPackageDetailClick = () =>{
      Utility.sendEvent(AnalyticsConstants.DASHBOARD_PACKAGE_DETAIL_CLICK)
      this.props.navigation.navigate("packDetail")
   }

   onDrawerIconPress = () => {
      this.props.navigation.toggleDrawer();
   }

   onRetryClick = () => {

   }

   onNotificationIconClick = () => {
      this.props.navigation.navigate("notificationListing")
   }


   onSyncPress = () => {

      if (!this.state.showLoading) {
         Utility.sendEvent(AnalyticsConstants.DASHBOARD_SYNC_CLICK)
         this.setState({ showSnackbar: true })
         // Get All Master(Config) Data...
         Utility.getMasterData().then((result) => {
            Utility.log('result===>', result)

            // Get and insert MMV
            getMMV((response) => {
               if (response.data && response.data.version && response.data.version.length > 0) {
                  dbFunctions.deleteMMV().then(result => {
                     dbFunctions.insertMMV(response.data.version).then((result) => {
                        Utility.log('MMV insertion count', result)
                        if (Constants.APP_TYPE == Constants.PHILIPPINES) {
                           this.setState({ showSnackbar: false })
                           Utility.showToast(Strings.SYNC_DONE)
                        }

                     })
                  })
               } else {
                  this.setState({ showSnackbar: false })
                  Utility.showToast(Strings.SYNC_FAILED)
               }
            }, (error) => {
               this.setState({ showSnackbar: false })
               Utility.showToast(Strings.SYNC_FAILED)
            })

            // Get and insert Financier MMV
            if (Constants.APP_TYPE == Constants.INDONESIA) {
               let page = 1;
               dbFunctions.deleteFinancierMMV().then(result => {
                  dbFunctions.deleteMYP().then(result => {
                     this.checkAndSyncFinancierMMV(page)
                  })
               })
            }

            // Get and insert City
            getAllCity((response) => {
               if (response.data && response.data.city && response.data.city.length > 0) {
                  dbFunctions.deleteCity().then(rs => {
                     dbFunctions.insertCity(response.data.city).then((result) => {
                        Utility.log('City insertion count', result);
                     })
                  })
               }
            }, (error) => {
               Utility.log('CityInsertionError==>', error)
            })

            // Get and insert Locality
            let cityArr = []
            Utility.getValueFromAsyncStorage(Constants.DEALER_CITY).then((dealer_city) => {
               Utility.log('dealer_city==>', dealer_city)
               cityArr.push(dealer_city)
               let param = {
                  city: cityArr
               }
               getDealerLocality(param, (response) => {
                  if (response.data && response.data.length > 0) {
                     dbFunctions.deleteLocality().then(result => {
                        dbFunctions.insertLocality(response.data).then((result) => {
                           Utility.log('Locality insertion count', result)
                        })
                     })
                  }
               }, (error) => {
                  Utility.log('LocalityInsertionError==>', error)
               })
            }).catch((error) => {
               Utility.log("dealer_city", error)
            })
         })
      } else {
         Utility.showToast(Strings.PLEASE_WAIT)
      }
   };

   checkAndSyncFinancierMMV = (page) => {
      getFinancierMMV(page, (response) => {
         // Utility.log('getFinancierMMV==>', response.data.nextpage)
         if (response.data && response.data.version && response.data.version.length > 0) {
            dbFunctions.insertFinancierMMV(response.data.version).then((result) => {
               Utility.log('Financier MMV insertion count', result)
               dbFunctions.insertMYP(response.data.make_year_price).then((result) => {
                  Utility.log('MYP insertion count', result)
                  if(page == 1){
                     //alert(response.data.version_count)
                     Utility.setValueInAsyncStorage(Constants.TOTAL_FINANCIER_MMV,response.data.version_count ? (response.data.version_count).toString() : '0')
                  }
                  // response.data.version_count
                  if (response.data.nextpage) {
                     let nextPage = page + 1;
                     this.checkAndSyncFinancierMMV(nextPage)
                  }
                  else {
                     // dbFunctions.getFinancierMMVTotalCount().then((result) => {
                     //    Utility.log('FinancierMMVTotalCount==>', result)
                     this.setState({ showSnackbar: false })
                     Utility.showToast(Strings.SYNC_DONE)
                     // })
                  }
               })
            })
         } else {
            // this.setState({ showSnackbar: false })
            // Utility.showToast(Strings.SYNC_FAILED)
            this.handleSyncFinancierMMV(page)
         }
      }, (error) => {
         Utility.log('FinancierMMVInsertionError==>', error)
         // this.setState({ showSnackbar: false })
         // Utility.showToast(Strings.SYNC_FAILED)
         this.handleSyncFinancierMMV(page)
      })
   }

   handleSyncFinancierMMV = (page) => {
      this.checkAndSyncFinancierMMV(page)
   }

   render() {
      return (
         // <WithLoading
         //    screenState={this.state.screenState}
         //    screenName={ScreenName.SCREEN_DASHBOARD}
         //    onRetry={this.onRetryClick}>

         <DashboardScreen
            onRetryClick={this.onRetryClick}
            screenState={this.state.screenState}
            onDrawerIconPress={this.onDrawerIconPress}
            gridData={this.state.gridData}
            onCountPress={this.onCountPress}
            data={this.state.data}
            showLoading={this.state.showLoading}
            onStockClick={this.onStockClick}
            onLeadsClick={this.onLeadsClick}
            onLoanClick={this.onLoanClick}
            isShowTab={this.state.isShowTab}
            isShowLoanTab={this.state.isShowLoanTab}
            onSyncPress={this.onSyncPress}
            onPackageDetailClick = {this.onPackageDetailClick}
            showSyncLoading={that.state.showSyncLoading}
            onNotificationIconClick={this.onNotificationIconClick}
            notificationUnreadCount={this.state.notificationUnreadCount}
            showSnackbar={this.state.showSnackbar}
            snackBarText={this.state.snackBarText}
         />
         // </WithLoading>
      )
   }
}
