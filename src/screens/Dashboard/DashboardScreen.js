import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { _ActionBarStyle } from '../../values/Styles'
import { Colors, Dimens, Strings } from './../../values'
import ViewPager from './../../nodeModuleChanges/react_native_viewpager/ViewPager'
import DashboardCard from "../../granulars/DashboardCard"
import CustomViewPageIndicator from "../../granulars/CustomViewPagerIndicator"
import { dashboard_bg, drawer_icon, icon_my_stock, icon_add_lead, icon_loan_box,notification_white_icon,icon_pending_action } from './../../assets/ImageAssets'
import HomeGridView from '../../granulars/HomeGridView'
import LoadingComponent from './../../granulars/LoadingComponent'
import Snackbar from './../../components/Snackbar'
import { Constants } from '../../util'

const dataSource = new ViewPager.DataSource({
    pageHasChanged: (p1, p2) => p1 !== p2,
});
let onCountPress

export default class DashboardScreen extends Component {

    constructor(props) {
        super(props);
        onCountPress = props.onCountPress;
    }

    renderPage(item) {
        return (
            <View style={{ flex: 1, paddingHorizontal: Dimens.padding_15 }}>
                <DashboardCard
                    onCountPress={onCountPress}
                    data={item} />

            </View>

        );
    }

    renderPageIndicators(size) {
        return (
            <CustomViewPageIndicator
                positionOffset={305}
                dotSize={8}
                dotSpace={6}
                dotBorderWidth={1}
                dotBorderColor={Colors.WHITE} />
        );
    }

    render() {
        let gridData = []
        this.props.isShowLoanTab ?
            gridData = [
                { text: Strings.STOCK, icon: icon_my_stock, action: this.props.onStockClick },
                { text: Strings.BUYER_LEADS, icon: icon_add_lead, action: this.props.onLeadsClick },
                { text: Strings.LOAN, icon: icon_loan_box, action: this.props.onLoanClick },
                { text: Strings.PACKAGE_DETAIL, icon: icon_pending_action, action: this.props.onPackageDetailClick},
            ]
            :
            gridData = [
                { text: Strings.STOCK, icon: icon_my_stock, action: this.props.onStockClick },
                { text: Strings.BUYER_LEADS, icon: icon_add_lead, action: this.props.onLeadsClick },
                // { text: Strings.LOAN, icon: icon_loan_box, action: this.props.onLoanClick },
            ]
        let data = dataSource.cloneWithPages(this.props.data);
        let actionBarProps = {
            values: { title: Strings.APP_NAME },
            // rightIcons: [{
            //     image: ImageAssets.icon_sync_white,
            //     onPress: () => this.props.onSync()
            // }, {
            //     image: ImageAssets.icon_notification,
            //     badge: this.props.notificationCount === 0 ? undefined : this.props.notificationCount,
            //     onPress: () => this.props.openNotification()
            // }],
            styleAttr: {
                leftIconImage: drawer_icon,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.TRANSPARENT }]
            }
            ,
            actions: {
                onLeftPress: () => this.props.onDrawerIconPress()
            },
            rightIcons: [
                {
                    image: Constants.APP_TYPE == Constants.PHILIPPINES ? notification_white_icon : undefined ,
                    onPress: () => this.props.onNotificationIconClick(),
                    badge: Constants.APP_TYPE == Constants.PHILIPPINES && this.props.notificationUnreadCount > 0 ? this.props.notificationUnreadCount : undefined 
                    
                },
                {
                    image: require('../../assets/drawable/sync_white_new.png'),
                    onPress: () => this.props.onSyncPress()
            }]
        }


        // let noOfRow = parseInt(gridData.length / 4) + 1
        // let height = 70 * noOfRow
        // //let height = Platform.OS === 'ios' ? 190 : 200;
        // if (gridData.length <= 4) {
        //     noOfRow = 2
        //     height = 140
        // }
        let noOfRow = parseInt(gridData.length / 4) + 1
        let height = 70 * noOfRow
        //let noOfRow = 2;
        //let height = Platform.OS === 'ios' ? 190 : 200;
        if (gridData.length <= 2) {
            noOfRow = 1
            height = 70
        }
        else{
            noOfRow = 2
            height = 140
        }
        return (
            <SafeAreaView
                style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK, marginBottom: Dimens.margin_bottom_iphone }}>
                <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                    <View style={{ flex: 1 }}>
                        <Image style={styles.backgroundContainer}
                            source={dashboard_bg} />
                        <View style={styles.cardContainer}>
                            <ActionBarWrapper
                                values={actionBarProps.values}
                                actions={actionBarProps.actions}
                                iconMap={actionBarProps.rightIcons}
                                styleAttributes={actionBarProps.styleAttr}
                            />
                            <View style={styles.viewPagerContainer}>
                                {this.props.data.length > 0 ?
                                    <View style={styles.viewPager}>
                                        <ViewPager
                                            dataSource={data}
                                            renderPage={this.renderPage}
                                            isLoop={false}
                                            autoPlay={false}
                                            renderPageIndicator={() => this.renderPageIndicators(this.props.data.length)}
                                        />
                                    </View> : null}
                            </View>

                        </View>
                    </View>
                    {this.props.showLoading && <LoadingComponent />}

                    {this.props.isShowTab &&
                        <View style={{ height: height }}>
                            <HomeGridView rowCount={noOfRow} gridData={gridData} />
                        </View>
                    }
                    {this.props.showSyncLoading && <LoadingComponent />}
                    {this.props.showSnackbar ? <Snackbar text={this.props.snackBarText}/> : null}
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    backgroundContainer: {
        height: '100%',
        width: '100%',
        resizeMode: 'cover'
    },
    cardContainer: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    viewPagerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewPager: {
        height: 320,
        width: '100%'
    }
});