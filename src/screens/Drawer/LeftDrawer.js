import React, { Component } from 'react';
import { StyleSheet, SafeAreaView, Platform, Image, TouchableOpacity, View, Text, ScrollView, Alert } from 'react-native';
//import { DrawerItems, createDrawerNavigator } from "react-navigation";
import { DashboardContainer, StockListingContainer } from './../'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import DrawerHead from './DrawerHead'
import { createDrawerNavigator,DrawerItems } from 'react-navigation-drawer';

export default class CustomDrawerContentComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showFinance: false
        }
    }

    async componentDidMount() {
        let ff = await Utility.isShowFinance()
        this.setState({ showFinance: ff })
    }

    render() {
        return (
            <SafeAreaView
                style={{ flex: 1, backgroundColor: Colors.PRIMARY }}
                forceInset={{ top: 'always', horizontal: 'never' }}>

                <ScrollView
                    alwaysBounceVertical={false}
                    style={{ backgroundColor: Colors.WHITE }}>

                    <TouchableOpacity
                        activeOpacity={1}
                        style={{ flex: 1 }}
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_PROFILE_CLICK)
                            Utility.openScreen(this.props, 'profile') 
                        }}>

                        <DrawerHead />
                    </TouchableOpacity>

                    <DrawerItems
                        contentOptions={{
                            activeBackgroundColor: Colors.PRIMARY,
                            inactiveBackgroundColor: Colors.INACTIVE_BACKGROUND_DRAWER
                        }} {...this.props}
                    />
                    {/* { Constants.APP_TYPE == Constants.PHILIPPINES ? <TouchableOpacity
                        style={{ marginBottom: 10}}
                        onPress={() => { Utility.openScreen(this.props, 'notificationListing') }}>

                        <View style={Styles.itemStyle}>
                            <Image style={[Styles.iconStyle,{tintColor: Colors.BLACK_85}]}
                                source={ImageAssets.notification_grey_icon} />
                            <Text style={Styles.textStyle}>{Strings.Notifications}</Text>
                        </View>
                    </TouchableOpacity> : <View/>} */}

                    <View style={[Styles.divderStyle, { marginTop: 0 }]}></View>

                    <Text style={Styles.headTextStyle}>{Strings.BUYER_LEAD}</Text>

                    <TouchableOpacity
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_ADD_LEAD_CLICK)
                            Utility.openScreen(this.props, 'addBuyerLead') 
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconStyle}
                                source={ImageAssets.icon_add_lead} />

                            <Text style={Styles.textStyle}>{Strings.ADD_LEAD}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_TODAY_WORK_CLICK)
                            Utility.openScreen(this.props, 'buyerLead') 
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconStyle}
                                source={ImageAssets.icon_todays_work} />

                            <Text style={Styles.textStyle}>{Strings.TODAY_WORK}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_LEAD_FINDER_CLICK)
                            Utility.openScreen(this.props, 'buyerLeadFinder') 
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconStyle}
                                source={ImageAssets.icon_lead_finder} />

                            <Text style={Styles.textStyle}>{Strings.LEAD_FINDER}</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={Styles.divderStyle}></View>

                    <Text style={Styles.headTextStyle}>{Strings.STOCK}</Text>

                    <TouchableOpacity
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_MY_STOCK_CLICK)
                            Utility.openScreen(this.props, 'stockListing') 
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconStyle}
                                source={ImageAssets.icon_my_stock} />

                            <Text style={Styles.textStyle}>{Strings.MY_STOCK}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { 
                            Utility.sendEvent(AnalyticsConstants.DRAWER_ADD_STOCK_CLICK)
                            Utility.openScreen(this.props, 'addEditStock') 
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconStyle}
                                source={ImageAssets.icon_add_stock} />

                            <Text style={Styles.textStyle}>{Strings.ADD_STOCK}</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={Styles.divderStyle}></View>

                    {this.state.showFinance ?
                        <View>
                            <Text style={Styles.headTextStyle}>{Strings.LOAN}</Text>

                            <TouchableOpacity
                                onPress={() => { 
                                    Utility.sendEvent(AnalyticsConstants.DRAWER_LOAN_DASHBORAD_CLICK)
                                    Utility.openScreen(this.props, 'loanDashboard') 
                                }}>

                                <View style={Styles.itemStyle}>
                                    <Image style={Styles.iconStyle}
                                        source={ImageAssets.icon_loan_box} />

                                    <Text style={Styles.textStyle}>{Strings.LOAN_DASHBOARD}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={Styles.divderStyle}></View>
                        </View>
                        :
                        null
                    }
                    
                    <TouchableOpacity
                        style={{ marginBottom: 50 }}
                        onPress={() => {
                            Alert.alert(
                                Strings.LOGOUT,
                                Strings.WANT_LOGOUT,
                                [
                                    { text: Strings.CANCEL, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                    { text: Strings.OK, onPress: () => { 
                                        Utility.sendEvent(AnalyticsConstants.LOGOUT_FROM_CLICK);
                                        Utility.logout(this.props) 
                                    }},
                                ],
                                { cancelable: false }
                            )
                        }}>

                        <View style={Styles.itemStyle}>
                            <Image style={Styles.iconLogout}
                                source={ImageAssets.icon_logout} />

                            <Text style={Styles.textStyle}>{Strings.LOGOUT}</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const LeftDrawer = createDrawerNavigator({
    dashboard: {
        screen: DashboardContainer,
        navigationOptions: {
            title: Strings.DASHBOARD_TITLE,
            drawerIcon: ({ tintColor }) => (
                <Image
                    source={ImageAssets.icon_home}
                    style={[Styles.icon]}
                />
            )
        },
    }
},
    {
        contentComponent: CustomDrawerContentComponent,
        contentOptions: {
            labelStyle: {
                fontSize: Dimens.text_normal,
                marginLeft: Dimens.margin_x_small,
                fontWeight: '400',
                color: Colors.BLACK_85,
                fontFamily: Strings.APP_FONT
            },
            activeTintColor: Colors.BLACK,
            activeBackgroundColor: Colors.TRANSPARENT,
            inactiveBackgroundColor: Colors.INACTIVE_BACKGROUND_DRAWER
        }
    }
)

const Styles = StyleSheet.create({
    icon: {
        width: 26,
        height: 26,
    },
    headTextStyle: {
        fontSize: Dimens.text_normal,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_xx_large,
        marginBottom: Dimens.margin_xx_large,
    },
    divderStyle: {
        height: 1,
        backgroundColor: Colors.BLACK_11,
        margin: Dimens.margin_xx_large
    },
    itemStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: Dimens.margin_x_small,
        marginBottom: Dimens.margin_normal
    },
    iconStyle: {
        width: 26,
        height: 26,
        marginLeft: Dimens.margin_xx_large
    },
    iconLogout: {
        width: 22,
        height: 20,
        tintColor: Colors.BLACK_85,
        marginLeft: Dimens.margin_xxx_large
    },
    textStyle: {
        fontSize: Dimens.text_normal,
        marginLeft: Dimens.margin_xxx_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    headerHeadText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    headersmallText: {
        color: Colors.WHITE_70,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    headerTagView: {
        width: 46, height: 46,
        borderRadius: 23,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
    },
    headerLargeText: {
        color: Colors.PRIMARY,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT
    },
    headerNormalText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    }
})

export { LeftDrawer }
//export default LeftDrawer;
