import React, { Component } from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants } from '../../util'
import AsyncStore from '../../util/AsyncStore'

export default class DrawerHead extends Component {
    constructor(props) {
        super(props)

        this.state = {
            dealerName: '',
            usreName: '',
            usrePhone: '',
            usreEmail: '',
            gcdCode: ''
        }
    }

    async componentDidMount() {
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.NAME, Constants.EMAIL, Constants.PHONE_NO, Constants.DEALER_NAME, Constants.GCD_CODE])
        this.setState({
            usreName: values[0][1],
            usreEmail: values[1][1],
            usrePhone: values[2][1],
            dealerName: values[3][1],
            gcdCode: values[4][1]
        })
    }

    render() {
        return (
            <View style={{ height: 146, backgroundColor: Colors.TRANSPARENT, justifyContent: 'center' }}>

                <Image style={{ width: '100%', height: '100%', resizeMode: 'cover', position: 'absolute' }}
                    source={ImageAssets.icon_nev_header} />

                <View style={{ padding: Dimens.padding_xx_large }}>
                    <Text style={Styles.headerHeadText}>{this.state.dealerName || Strings.NA}</Text>
                    <Text style={Styles.headersmallText}>({this.state.gcdCode})</Text>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: Dimens.margin_xxx_large }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={Styles.headerTagView}>
                                <Text style={Styles.headerLargeText}>{this.state.usreName ? Utility.getStrFirstLetter(this.state.usreName) : '' }</Text>
                            </View>

                            <View style={{ marginLeft: Dimens.margin_xx_large }}>
                                <Text style={Styles.headerNormalText}>{this.state.usreName || Strings.NA}</Text>
                                <Text style={Styles.headersmallText}>{this.state.usrePhone || Strings.NA}</Text>
                            </View>
                        </View>

                        <Image style={{ width: 16, height: 16 }}
                            source={ImageAssets.icon_arrow} />
                    </View>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    headerHeadText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    headersmallText: {
        color: Colors.WHITE_70,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    headerTagView: {
        width: 46, height: 46,
        borderRadius: 23,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
    },
    headerLargeText: {
        color: Colors.PRIMARY,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT
    },
    headerNormalText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    }
})