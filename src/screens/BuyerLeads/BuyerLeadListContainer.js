import React, { Component } from 'react'
import BuyerLeadListScreen from './BuyerLeadListScreen'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../values/Styles'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../util/Constants'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import ActionBarSearchComponent from "../../granulars/ActionBarSearchComponent"
import DBFunctions from "../../database/DBFunctions"
import * as DBConstants from '../../database/DBConstants'
import { EventRegister } from '../../util/EventRegister'

export default class BuyerLeadListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leadsData: [],
            leadCount: 0,
            refreshing: false,
            showSearchBar: false,
            page: 1,
            hasNext: false,
            isApiCalling: false,
            screenState: ScreenStates.IS_LOADING,
        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.BUYER_LEAD_LIST_SCREEN)
        EventRegister.addEventListener(Constants.LMS_SYNC, (data) => {
            if (data) {
                this.getLeadListing('', true)
            }
        })

        this.getLeadListing('', true)
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(Constants.LMS_SYNC)
    }

    onRetryClick = () => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1, showSearchBar: false }, () => {
            this.getLeadListing('', true)
        })
    }

    handleLoadMore = () => {
        if (this.state.hasNext && !this.state.isApiCalling) {
            let nextPage = this.state.page + 1
            this.setState({ refreshing: true, page: nextPage }, () => {
                this.getLeadListing('', false)
            })
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true, page: 1, showSearchBar: false }, () => {
            this.getLeadListing('', true)
        })
    }

    onSubmitEditing = (text) => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
            this.getLeadListing(text, true)
        })
    }

    onSearchTextChange = (text) => {
        // this.setState({ screenState: ScreenStates.IS_LOADING }, () => {
        //     this.getLeadListing(text, true)
        // })
    }

    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
    }

    onSearch = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_LIST_SEARCH_CLICK)
        this.setState({ showSearchBar: true })
    }

    getLeadListing = async (text, isRefresh) => {

        if (isRefresh)
            this.setState({ isApiCalling: true, leadsData: [] })
        else
            this.setState({ isApiCalling: true })

        var leadKey = this.props.navigation.state.params.item.key || ''
        var leadDate = this.props.navigation.state.params.leadDate || ''
        var dashTabKey = this.props.navigation.state.params.item._key || ''

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            APICalls.getLeadListing(values[0][1], leadKey, leadDate, dashTabKey, text, this.state.page, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = async (response) => {
        if (response && response.data && response.data.lead_data.length > 0) {
            let dbFunctions = new DBFunctions()
            var i = 0;
            for (var index in response.data.lead_data) {
                var itemTemp = response.data.lead_data[i]
                if (itemTemp.location_id && itemTemp.location_id != 0) {
                    await dbFunctions.getLocalityById(itemTemp.location_id).then((result) => {
                        if (result && result.length > 0)
                            itemTemp['locality'] = result[0][DBConstants.LOCALITY]
                        else
                            itemTemp['locality'] = ''
                    })
                }

                if (itemTemp.model && itemTemp.model != 0) {
                    await dbFunctions.getMMVByParentModelId(itemTemp.model).then((result) => {
                        if (result && result.length > 0)
                            itemTemp['makeModel'] = result[0][DBConstants.MAKE_MODEL]
                        else
                            itemTemp['makeModel'] = ''
                    })
                }
                response.data.lead_data[i] = itemTemp
                i++;
            }

            this.setState({
                refreshing: false,
                isApiCalling: false,
                hasNext: response.data.next_page,
                screenState: ScreenStates.NO_ERROR,
                leadCount: response.data.total_records,
                // leadsData: response.data.lead_data,
                leadsData: [...this.state.leadsData, ...response.data.lead_data],
            })
        }
        else
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, refreshing: false, isApiCalling: false, hasNext: false })
        Utility.log('onSuccessLeadListing===> ', JSON.stringify(response))
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        Utility.log('onFailureLeadListing===> ', JSON.stringify(response))
        this.setState({
            hasNext: false,
            refreshing: false, isApiCalling: false,
            screenState: ScreenStates.SERVER_ERROR
        })
    }

    onItemClick = (item) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_LIST_ITEM_CLICK)
        this.props.navigation.navigate('leadDetail', {
            leadId: item.lead_id
        })
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_LEAD_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        let title = this.props.navigation.state.params.item.label + ' (' + this.state.leadCount + ')'
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: title }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: ImageAssets.icon_search,
                            onPress: () => this.onSearch()
                        }]}
                    />

                    {this.state.showSearchBar ? this.openActionSearchBar() : null}

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <BuyerLeadListScreen
                            leadsData={this.state.leadsData}
                            refreshing={this.state.refreshing}
                            onItemPress={this.onItemClick}
                            onRefresh={this.onRefresh}
                            handleLoadMore={this.handleLoadMore}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})