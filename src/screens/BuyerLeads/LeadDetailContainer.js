import React, { Component } from 'react'
import LeadDetailScreen from './LeadDetailScreen'
import { View, SafeAreaView, StyleSheet, Linking,BackHandler } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../values/Styles'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../util/Constants'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import Toast from 'react-native-simple-toast'
import DBFunctions from "../../database/DBFunctions"
import * as DBConstants from '../../database/DBConstants'
import { EventRegister } from '../../util/EventRegister'

let dateFormat = require('../../util/DateFormat');

export default class LeadDetailContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            screenState: ScreenStates.IS_LOADING,
            stockData: [],
            historyData: [],
            shortHistoryData: [],
            leadData: {},
            carData: undefined,
            selectedStockIndex: 0,
            isLoading: false,
            loadingMsg: '',
            lmsLeadInitializationData: {},
            showStockSelectionModal: false,
            interestedStockData: [],
            stockSelectionPlaceHolder: '',
            stockSelectionTitle: '',
            selectedStock: undefined,
            stockSelectionTask: undefined,
            stockSelectionActive: false,
            showCallOptions: false,
            showCallTypes: false,
            callType: '',
            usreName: '',
            usreEmail: '',
            usrePhone: '',
            dealerName: '',
            dealerAddress: '',
            dealerLat: '',
            dealerLang: '',
            fuleTypeList: [],
            BodyTypeList: [],
            TransmissionList: [],
            favCarIds: [],
            notifId : props.navigation.state.params.notifId ? props.navigation.state.params.notifId : null
        }
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.sendCurrentScreen(AnalyticsConstants.BUYER_LEAD_DETAIL_SCREEN)
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.NAME, Constants.EMAIL,
        Constants.PHONE_NO, Constants.DEALER_NAME, Constants.DEALER_ADDRESS, Constants.DEALER_LAT, Constants.DEALER_LANG,
        Constants.FUEL_TYPE_VALUES, Constants.BODY_TYPE_VALUES, Constants.TRANSMISSION_TYPE_VALUES])
        this.setState({
            usreName: values[0][1],
            usreEmail: values[1][1],
            usrePhone: values[2][1],
            dealerName: values[3][1],
            dealerAddress: values[4][1],
            dealerLat: values[5][1],
            dealerLang: values[6][1],
            fuleTypeList: JSON.parse(values[7][1]),
            BodyTypeList: JSON.parse(values[8][1]),
            TransmissionList: JSON.parse(values[9][1])
        })

        this.getLeadDetail()

        if(this.state.notifId){
            this.updateNotificationStatus(this.state.notifId)
        }
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }

    onRetryClick = () => {
        this.getLeadDetail()
    }

    getLeadDetail = async () => {

        let leadId = await this.props.navigation.state.params.leadId

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            APICalls.getLeadDetail(values[0][1], leadId, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = (response) => {
        Utility.log('onSuccessLeadDetail===> ', JSON.stringify(response.data.stockList))
        if (response && response.data) {
            this.prepareData(response.data);
            if (this.state.stockSelectionActive) {
                this.showCarSelectionUi(this.state.stockSelectionTask);
            }
        } else {
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, isLoading: false })
        }
    }

    updateNotificationStatus = (id) =>{
        try {
            APICalls.updateNotificationStatus(id, this.onUpdateSuccess, this.onUpdateFailure, this.props,true)
        } catch (error) {
            Utility.log(error)
        }
    };

    onUpdateSuccess = (response) => {
        if (response && response.message) {
            
        } else {
            
        }
    }

    onUpdateFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
     }

    prepareData = async (data, clearStockSelection) => {
        var shortHistory = []

        for (var index in data.leadHistory) {
            if (index < 2)
                shortHistory.push(data.leadHistory[index])
        }
        let interestedStockData = [];
        if (data.stockList) {
            interestedStockData = data.stockList.filter((item) => {
                if (item.is_favourite && item.is_favourite == 1) {
                    return item;
                }
            })
        }

        let dbFunctions = new DBFunctions()
        let leadObj = data.leadData
        if (leadObj.model && leadObj.model != 0) {
            await dbFunctions.getMMVByParentModelId(leadObj.model).then((result) => {
                if (result && result.length > 0)
                    leadObj['makeModel'] = result[0][DBConstants.MAKE_MODEL]
                else
                    leadObj['makeModel'] = ''
            })
        }

        var i = 0;
        for (var index in this.state.fuleTypeList) {
            if (this.state.fuleTypeList[i].key == leadObj.fuel_type) {
                leadObj['fuelType'] = this.state.fuleTypeList[i].value
            }
            i++
        }

        var j = 0;
        for (var index in this.state.BodyTypeList) {
            if (this.state.BodyTypeList[j].key == leadObj.body_type) {
                leadObj['bodyType'] = this.state.BodyTypeList[j].value
            }
            j++
        }

        var k = 0;
        for (var index in this.state.TransmissionList) {
            if (this.state.TransmissionList[k].key == leadObj.transmission_type) {
                leadObj['transmissionType'] = this.state.TransmissionList[k].value
            }
            k++
        }

        // updated favCarIds

        let favCarIds = []
        if (data.stockList.length > 0) {
            for (var index in data.stockList) {
                favCarIds.push(data.stockList[index].id)
            }
        }

        this.setState({ favCarIds: favCarIds })

        if (clearStockSelection) {
            this.setState({
                isLoading: false,
                screenState: ScreenStates.NO_ERROR,
                leadData: data.leadData ? leadObj : {},
                historyData: data.leadHistory || [],
                shortHistoryData: shortHistory || [],
                stockData: data.stockList || [],
                carData: data.stockList.length > 0 ? data.stockList[this.state.selectedStockIndex] : undefined,
                interestedStockData: interestedStockData,
                showStockSelectionModal: false,
                stockSelectionPlaceHolder: '',
                stockSelectionTitle: '',
                selectedStock: undefined,
                stockSelectionTask: undefined,
                stockSelectionActive: false
            })
        } else {
            this.setState({
                isLoading: false,
                screenState: ScreenStates.NO_ERROR,
                leadData: data.leadData ? leadObj : {},
                historyData: data.leadHistory || [],
                shortHistoryData: shortHistory || [],
                stockData: data.stockList || [],
                carData: data.stockList.length > 0 ? data.stockList[this.state.selectedStockIndex] : undefined,
                interestedStockData: interestedStockData
            })
        }
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        Utility.log('onFailureLeadDetail===> ', JSON.stringify(response))
        this.setState({ screenState: ScreenStates.SERVER_ERROR, isLoading: false })
    }

    updateFavourite = async (isFavourite, carId) => {

        // 1 for add Favourite and 0 for remove from Favourite
        let leadId = await this.props.navigation.state.params.leadId
        let carIdArr = await [carId]

        var storeObject = new AsyncStore()
        storeObject.getAsyncValueInPersistStore(Constants.DEALER_ID).then((dealer_id) => {
            Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_FAVOURATE_CLICK,{car_id : carId})
            try {
                APICalls.updateFavourite(leadId, isFavourite, carIdArr, this.onSuccessUpdateFav, this.onFailureUpdateFav, this.props)
            } catch (error) {
                Utility.log(error)
            }

            this.setState({ isLoading: true })
            // this.setState({ screenState: ScreenStates.IS_LOADING })

        }).catch((error) => {
            Utility.log("error===> ", error)
        })
    }

    onSuccessUpdateFav = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_FAVOURATE_SUCCESS)
        this.setState({ screenState: ScreenStates.NO_ERROR, isLoading: false }, () => {
            this.getLeadDetail()
        })
    }

    onFailureUpdateFav = (response) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_FAVOURATE_FAILURE)
        if (response && response.message) {
            Utility.showToast(response.message)
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
        }

        this.setState({ screenState: ScreenStates.NO_ERROR, isLoading: false })
    }

    // on updtae customer persnol details
    onEditLead = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_EDIT_CLICK)
        this.props.navigation.navigate('editLead', {
            data: this.state.leadData,
            callBack: this.getLeadDetail
        })
    }

    // on Edit customer requirement
    onEditClick = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CUST_EDIT_CLICK)
        this.props.navigation.navigate('editRequirement', {
            data: this.state.leadData,
            callBack: this.getLeadDetail
        })
    }

    onViewAllHistory = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_HISTORY_VIEW_ALL_CLICK)
        if (this.state.historyData.length > 0) {
            this.props.navigation.navigate('leadHistory', {
                data: this.state.historyData
            })
        }
    }

    onAllStockClick = () => {
        this.setState({
            showStockSelectionModal: false
        });
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_ALL_STOCK_CLICK)
        this.props.navigation.navigate('stockList', {
            leadId: this.props.navigation.state.params.leadId,
            callBack: this.getLeadDetail,
            favCarIds: this.state.favCarIds || []
        })
    }

    onAddCarClick = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_ADD_CAR_CLICK)
        this.setState({ showStockSelectionModal: false })
        this.props.navigation.navigate('stockList', {
            leadId: this.props.navigation.state.params.leadId,
            callBack: this.getLeadDetail,
            favCarIds: this.state.favCarIds || []
        })
    }

    // on Stock Item Click
    onStockItemClick = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_STOCK_CLICK)
        this.props.navigation.navigate('carDetail', {
            leadData: this.state.leadData,
            stockData: this.state.stockData,
            callBack: this.getLeadDetail
        })
    }

    // LMS flow
    onUpdateClick = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_UPDATE_CLICK)
        if (this.state.leadData && this.state.leadData.lead_status_id) {
            let lmsLeadStatusName = Constants.LMS_STATUS_BY_LEAD_STATUS_ID[this.state.leadData.lead_status_id];
            if (lmsLeadStatusName) {
                let lmsLeadInitializationData = {};
                lmsLeadInitializationData['leadStatus'] = lmsLeadStatusName;
                lmsLeadInitializationData['walkInDate'] = this.state.leadData.lead_walkin_date;
                lmsLeadInitializationData['returnStatus'] = '';
                lmsLeadInitializationData['returnTaskId'] = '';
                this.refs['detailScreen'].refs['lms'].initiateLms(lmsLeadInitializationData);
            }
        }
    }

    lmsResetCallBack = () => {

    };

    onLmsCompleted = (flag, lmsData) => {
        // console.log(lmsData);
        this.lmsData = lmsData;
        this.prepareParamsForLmsUpdate(lmsData);
    };

    prepareParamsForLmsUpdate = (lmsData) => {
        this.newLeadStatus = lmsData.NewLeadState;
        this.lmsUpdateParams = {};

        this.lmsUpdateParams[Constants.LEAD_ID_PARAMS] = this.state.leadData.lead_id;
        this.lmsUpdateParams[Constants.OFFLINE_DATETIME] = dateFormat(new Date().toString(), "yyyy-mm-dd HH:MM:ss");
        this.lmsUpdateParams[Constants.LEAD_MOBILE] = this.state.leadData.mobile;
        this.addParamsForTask(lmsData);
    };

    addParamsForTask = (lmsLeadData) => {
        try {
            /**added param walkedIn true if user has confirmed that user has walkedIn*/
            if (lmsLeadData && lmsLeadData.tracking &&
                lmsLeadData.tracking.includes("," + Constants.WALKED_IN_TASK_ID + ",") &&
                this.state.leadData && this.state.leadData.lead_status_id === Constants.WALK_IN_SCHEDULED_STATUS_ID) {
                this.lmsUpdateParams[Constants.WALKED_IN] = "true";
            }


            switch (lmsLeadData.task_perform) {

                case Constants.LMS_TASK_STATUS_FOLLOW_UP:
                    this.lmsUpdateParams[Constants.NEXT_FOLLOW] = Utility.setDateTime(lmsLeadData.date, lmsLeadData.time)

                    if (lmsLeadData.amount && lmsLeadData.amount !== '') {
                        this.lmsUpdateParams[Constants.OFFER_AMOUNT] = lmsLeadData.amount;
                    }
                    if (lmsLeadData.rating != null && lmsLeadData.rating !== '') {
                        if (parseInt(lmsLeadData.rating) === 1) {
                            this.lmsUpdateParams[Constants.RATING] = Constants.LEAD_RATING_HOT;
                        } else if (parseInt(lmsLeadData.rating) === 2) {
                            this.lmsUpdateParams[Constants.RATING] = Constants.LEAD_RATING_WARM;
                        } else if (parseInt(lmsLeadData.rating) === 3) {
                            this.lmsUpdateParams[Constants.RATING] = Constants.LEAD_RATING_COLD;
                        }
                    }
                    if (lmsLeadData.comment && lmsLeadData.comment !== '') {
                        this.lmsUpdateParams[Constants.COMMENT] = lmsLeadData.comment;
                    }
                    this.syncLmsData();
                    break;

                case Constants.LMS_TASK_STATUS_WALK_IN:
                    this.lmsUpdateParams[Constants.NEXT_WALKIN] = Utility.setDateTime(lmsLeadData.date, lmsLeadData.time)
                    if (lmsLeadData.comment && lmsLeadData.comment !== '') {
                        this.lmsUpdateParams[Constants.COMMENT] = lmsLeadData.comment;
                    }
                    if (lmsLeadData.setReminder) {
                        //setting reminder.And remider key is NEXT_FOLLOW
                        this.lmsUpdateParams[Constants.REMINDER_DATE] = lmsLeadData.reminderdate + " " + lmsLeadData.remindertime;
                    }
                    this.syncLmsData();
                    break;

                case Constants.LMS_TASK_STATUS_CLOSE:
                    this.lmsUpdateParams[Constants.FEEDBACK] = lmsLeadData.closeReason;
                    this.lmsUpdateParams[Constants.FEEDBACK_ID] = lmsLeadData.option_id;
                    this.syncLmsData();
                    break;

                case Constants.LMS_TASK_STATUS_WALK_IN_CONFIRMED:
                    let walkInDate = dateFormat(this.state.leadData.lead_walkin_date, "yyyy-mm-dd")
                    let walkInTime = dateFormat(this.state.leadData.lead_walkin_date, "HH:MM:ss")
                    this.lmsUpdateParams[Constants.NEXT_WALKIN] = Utility.setDateTime(walkInDate, walkInTime)
                    //setting reminder.And remider key is NEXT_FOLLOW
                    this.lmsUpdateParams[Constants.NEXT_FOLLOW] = Utility.setDateTime(walkInDate, walkInTime)
                    this.syncLmsData();
                    break;

                case Constants.LMS_TASK_STATUS_BOOKED:
                    this.showCarSelectionUi(Constants.LMS_TASK_STATUS_BOOKED);
                    break;
                case Constants.LMS_TASK_STATUS_CUSTOMER_OFFER:
                    this.showCarSelectionUi(Constants.LMS_TASK_STATUS_CUSTOMER_OFFER);
                    break;
                case Constants.LMS_TASK_STATUS_CONVERTED:
                    this.showCarSelectionUi(Constants.LMS_TASK_STATUS_CONVERTED);
                    break;
                case Constants.LMS_TASK_STATUS_CALL:
                    this.showCallOptionDialog();
                    break;

                case Constants.LMS_TASK_STATUS_SHARE:
                    if (this.lmsData.shareType && this.lmsData.shareType != '') {

                        // this.prepareShareParams(this.lmsData.shareType);
                        if (this.lmsData.shareType == Constants.LOCATION_SHARE_TYPE) {
                            // Location
                            this.shareOnWhatsApp(this.getDealerLocation())
                            this.sharelead('WHATSAPP', Constants.LOCATION_SHARE_TYPE)
                        }
                        else if (this.lmsData.shareType == Constants.CAR_DETAILS_SHARE_TYPE) {
                            // Car details
                            if (this.state.carData == undefined) {
                                Toast.show('Add Car in stock', Toast.SHORT);
                            }
                            else {
                                this.shareOnWhatsApp(this.getCarDetails())
                                this.sharelead('WHATSAPP', Constants.CAR_DETAILS_SHARE_TYPE)
                            }
                        }
                        else if (this.lmsData.shareType == Constants.CAR_PHOTOS_SHARE_TYPE) {
                            // Car photo
                            // Toast.show('Coming soon...', Toast.SHORT);
                            // this.sharelead('WHATSAPP', Constants.CAR_PHOTOS_SHARE_TYPE)

                            if (this.state.carData == undefined) {
                                Toast.show('Add Car in stock', Toast.SHORT);
                            }
                            else {
                                this.shareOnWhatsApp(this.getCarDetails())
                                this.sharelead('WHATSAPP', Constants.CAR_DETAILS_SHARE_TYPE)
                            }
                        }
                    }
                    /* if (CommonUtils.getIntSharedPreference(Constants.MSG_LANGUAGE, -1) == -1) {
                         LanguageSelectDialog
                         selectDialog = new LanguageSelectDialog();
                         Bundle
                         extra = new Bundle();
                         extra.putInt("requestID", LangSelect.RQ_LMS_UD_SHARE);
                         extra.putString("key1", lmsLeadData.shareType);
                         selectDialog.setArguments(extra);
                         if (getSupportFragmentManager() != null)
                             selectDialog.show(getSupportFragmentManager(), null);
                         return;
                     } else {
                         selectedLanguage = CommonUtils.getIntSharedPreference(Constants.MSG_LANGUAGE, -1);
                     }
                     if (selectedLanguage != -1) {
                         resumeLmsInitiatedShare(lmsLeadData.shareType);
                         selectedLanguage = -1;
                         ShareTypeEvent shareTypeEvent = new ShareTypeEvent();
                         shareTypeEvent.setShareType(ShareType.WHATSAPP);
                         final LeadDetailModel carModel = carPagerAdapter.getCurrentModel();
                         if (shareSelectedRadioBtnName.equals("Car Photos Sent") && carModel.getImages().size() <= 0) {
                             Toast.makeText(this, R.string.no_photos_to_share, Toast.LENGTH_SHORT).show();
                             return;
                         }
                         showNumberSelectionDialog(shareTypeEvent);
                     }*/
                    break;
            }
        } catch (error) {
        }
    };

    updateSelectedStock = (index) => {
        this.setState({ selectedStockIndex: index, carData: this.state.stockData[index] })
    }

    getDealerLocation = () => {
            let message = 'Hi ' + this.state.leadData.customer_name + '\n\n' +
            // 'I am ' + this.state.usreName + ' from ' + this.state.dealerName + '. ' +
            'We are located at http://maps.google.com/maps?q=' +
            this.state.dealerLat + ',' + this.state.dealerLang + '.\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    getCarDetails = () => {
        let urls = this.state.carData.web_url
        let urlToBeUsed = ''
        if (urls) {
            if (urls instanceof Array) {
                urlToBeUsed = urls[0].url
                
            } else {
                urlToBeUsed = urls
            }
        }
        let message = 'Hi ' + this.state.leadData.customer_name + '\n\n' +
            'Please check out my ' + this.state.carData.reg_year + ' ' + this.state.carData.make + ' ' + this.state.carData.modelVersion +
            this.state.carData.fuel_type + ' ' + this.state.carData.uc_colour + ' color with ' +
            this.state.carData.km_driven + ' kms driven priced at ' + Utility.displayCurrency(this.state.carData.car_price) + '.\n\n' +
            urlToBeUsed + '\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    shareOnWhatsApp = async (message) => {
        let mobileNo = await this.state.leadData.mobile
        Utility.log('mobileNo===> ', mobileNo)
        Utility.log('message===> ', message)

        let url = `whatsapp://send?text=${message}&phone=${mobileNo}`
        const canOpen = await Linking.canOpenURL(url)

        if (!canOpen) {
            Utility.showToast(Strings.WHATSAPP_NOT_INSTALL)
            throw new Error('Provided URL can not be handled')
        }
        Linking.openURL(url)
    }

    prepareShareParams = (shareType) => {
        switch (shareType) {
            case Constants.WELCOME_SHARE_TYPE:
                return Strings.WELCOME_MSG_SENT

            case Constants.CAR_DETAILS_SHARE_TYPE:
                return Strings.CAR_DETAILS_SENT

            case Constants.CAR_PHOTOS_SHARE_TYPE:
                return Strings.CAR_PHOTOS_SENT

            case Constants.LOCATION_SHARE_TYPE:
                return Strings.DEALERSHIP_LOCATION_SENT

            case Constants.WALKIN_REMINDER_SHARE_TYPE:
                return Strings.WALK_IN_REMINDER_SENT

            case Constants.CALL_REMINDER_SHARE_TYPE:
                return Strings.CALL_REMINDER_SENT
        }
    }

    sharelead = async (shareBy, shareType) => {

        let leadId = await this.props.navigation.state.params.leadId
        let shareItem = await this.prepareShareParams(shareType)

        try {
            APICalls.sharelead(leadId, shareBy, shareItem, this.onSuccessShare, this.onFailureShare, this.props)
        } catch (error) {
            Utility.log(error)
        }

        this.setState({ isLoading: true })
    }

    onSuccessShare = (response) => {

        this.setState({ isLoading: false }, () => {
            this.getLeadDetail()
        })
    }

    onFailureShare = (response) => {
        if (response && response.message) {
            Utility.showToast(response.message)
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
        }

        this.setState({ isLoading: false })
    }

    showCarSelectionUi = (task) => {
        let placeHolder = '';
        let title = '';
        let stockSelectionTask = undefined;
        switch (task) {
            case Constants.LMS_TASK_STATUS_BOOKED:
                stockSelectionTask = Constants.LMS_TASK_STATUS_BOOKED;
                placeHolder = Strings.ENTER_BOOKING_AMOUNT;
                title = Strings.SELECT_THE_CAR + ' ' + (this.state.leadData.customer_name || Strings.NA) + ' ' + Strings.HAS_BOOKED + '.'
                break;
            case Constants.LMS_TASK_STATUS_CUSTOMER_OFFER:
                stockSelectionTask = Constants.LMS_TASK_STATUS_CUSTOMER_OFFER
                placeHolder = Strings.ENTER_CUSTOMER_OFFER;
                title = Strings.SELECT_THE_CAR_ON_WHICH + ' ' + (this.state.leadData.customer_name || Strings.NA) + ' ' + Strings.HAS_GIVEN_AN_OFFER + '.'
                break;
            case Constants.LMS_TASK_STATUS_CONVERTED:
                stockSelectionTask = Constants.LMS_TASK_STATUS_CONVERTED
                placeHolder = Strings.ENTER_SALE_AMOUNT
                title = Strings.SELECT_THE_CAR + ' ' + (this.state.leadData.customer_name || Strings.NA) + ' ' + Strings.HAS_BOUGHT + '.'
                break;

        }

        let interestedStockData = [];
        for (let i = 0; i < this.state.interestedStockData.length; i++) {
            let item = this.state.interestedStockData[i];
            item['selected'] = false;
            interestedStockData.push(item);
        }
        this.refs['detailScreen'].state.amount = '';
        this.setState({
            showStockSelectionModal: true,
            stockSelectionPlaceHolder: placeHolder,
            stockSelectionTitle: title,
            selectedStock: undefined,
            interestedStockData: interestedStockData,
            stockSelectionTask: stockSelectionTask,
            stockSelectionActive: true
        })

    };

    syncLmsData = () => {
        this.changeLeadStatusBeforeSync();
        if (this.state.stockSelectionActive
            && this.state.selectedStock
            && this.refs['detailScreen'].state.amount != '') {
            this.setCarSelectionDetailInParams();
        }
        this.lmsUpdateParams[Constants.LMS_TRACKING] = this.lmsData.tracking;
        this.requestLeadSync();
    };

    requestLeadSync = () => {
        Utility.log('Lead Update Params: ', this.lmsUpdateParams);
        APICalls.updateLeadStatus(this.lmsUpdateParams, (response) => {
            if (response.message) {
                Toast.show(response.message, Toast.SHORT);
            }
            if (response.data) {
                // TODO... Refresh Tab count and lead List apis...
                EventRegister.emit(Constants.LMS_SYNC, response.data)
                this.prepareData(response.data, true)
            } else {
                this.setState({
                    showStockSelectionModal: false,
                    stockSelectionPlaceHolder: '',
                    stockSelectionTitle: '',
                    selectedStock: undefined,
                    stockSelectionTask: undefined,
                    stockSelectionActive: false
                });
            }
            setTimeout(() => this.checkForReturnStatus(), 300);
        }, (error) => {
            this.setState({
                showStockSelectionModal: false,
                stockSelectionPlaceHolder: '',
                stockSelectionTitle: '',
                selectedStock: undefined,
                stockSelectionTask: undefined,
                stockSelectionActive: false
            });
            if (error && error.message) {
                Toast.show(error.message, Toast.SHORT)
            }
        }, this.props);
    };

    checkForReturnStatus = () => {
        Utility.log('check for return status');
        let lmsData = this.lmsData;
        if (lmsData.returnstatus) {
            Utility.log('check for return status 2');
            //need to start lms again
            let return_status_name = lmsData.return_status_name
            let return_task_id = lmsData.return_task_id
            let lmsLeadInitializationData = {};
            lmsLeadInitializationData['leadStatus'] = lmsData.NewLeadState;
            lmsLeadInitializationData['walkInDate'] = this.state.leadData.lead_walkin_date;
            lmsLeadInitializationData['returnStatus'] = return_status_name;
            lmsLeadInitializationData['returnTaskId'] = return_task_id;
            this.refs['detailScreen'].refs['lms'].initiateLms(lmsLeadInitializationData);
        }
    };

    changeLeadStatusBeforeSync = () => {
        let oldLeadStatus = this.state.leadData.status_name;
        let currentParamStatus = Constants.LEAD_STATUS_BY_LMS_STATUS[this.newLeadStatus];
        if (!currentParamStatus) {
            Toast.show(Strings.INVALID_LEAD_STATUS, Toast.SHORT);
            return;
        }
        /* if (oldLeadStatus == currentParamStatus) {
             //Toast.show(Strings.INVALID_LEAD_STATUS, Toast.SHORT);
             return;
         }*/
        this.lmsUpdateParams[Constants.LEAD_STATUS] = currentParamStatus;
    };

    closeBottomSheet = () => {
        this.setState({
            showStockSelectionModal: false,
            stockSelectionPlaceHolder: '',
            stockSelectionTitle: '',
            selectedStock: undefined,
            stockSelectionTask: undefined,
            stockSelectionActive: false
        })
    };

    onStockSelection = (item, index) => {
        let interestedStockData = [];
        let selectedStock = undefined;
        for (let i = 0; i < this.state.interestedStockData.length; i++) {
            let item = this.state.interestedStockData[i];
            if (i === index) {
                if (!item.selected) {
                    selectedStock = item;
                } else {
                    selectedStock = undefined
                }
                item['selected'] = !item.selected;
            } else {
                item['selected'] = false;
            }
            interestedStockData.push(item);
        }
        Utility.log(selectedStock, interestedStockData);
        this.setState({ interestedStockData: interestedStockData, selectedStock: selectedStock })
    };

    onStockSelectionSubmit = () => {
        if (this.refs['detailScreen'].state.amount == '') {
            Toast.show(Strings.ENTER_AMOUNT, Toast.SHORT);
        } else if (!this.state.selectedStock) {
            Toast.show(Strings.SELECT_THE_CAR, Toast.SHORT);
        } else if (parseInt(this.refs['detailScreen'].state.amount) > parseInt(this.state.selectedStock.car_price)) {
            Toast.show(Strings.AMOUNT_SHOULD_NOT_GREATER, Toast.SHORT);
        } else {
            let newLeadStatus = undefined;
            switch (this.state.stockSelectionTask) {
                case Constants.LMS_TASK_STATUS_BOOKED:
                    newLeadStatus = Constants.LMS_TASK_APP_BOOKED;
                    break;
                case Constants.LMS_TASK_STATUS_CUSTOMER_OFFER:
                    newLeadStatus = Constants.LMS_TASK_APP_OFFER;
                    break;
                case Constants.LMS_TASK_STATUS_CONVERTED:
                    newLeadStatus = Constants.LMS_TASK_STATUS_CONVERTED;
                    break;

            }
            this.setState({ showStockSelectionModal: false });
            if (newLeadStatus && newLeadStatus != Constants.LMS_TASK_STATUS_CONVERTED) {
                setTimeout(() => this.startLmsAfterStockSelection(newLeadStatus), 300);
            } else if (newLeadStatus && newLeadStatus == Constants.LMS_TASK_STATUS_CONVERTED) {
                this.syncLmsData();
            }
        }
    };

    startLmsAfterStockSelection = (newStatus) => {
        let lmsLeadInitializationData = {};
        lmsLeadInitializationData['leadStatus'] = newStatus;
        lmsLeadInitializationData['walkInDate'] = this.state.leadData.lead_walkin_date;
        lmsLeadInitializationData['returnStatus'] = '';
        lmsLeadInitializationData['returnTaskId'] = '';
        this.refs['detailScreen'].refs['lms'].initiateLms(lmsLeadInitializationData);
    };

    setCarSelectionDetailInParams = () => {
        this.lmsUpdateParams[Constants.CAR_ID] = this.state.selectedStock.id;
        switch (this.state.stockSelectionTask) {
            case Constants.LMS_TASK_STATUS_BOOKED:
                this.lmsUpdateParams[Constants.BOOKING_AMOUNT] = this.refs['detailScreen'].state.amount;
                break;
            case Constants.LMS_TASK_STATUS_CUSTOMER_OFFER:
                this.lmsUpdateParams[Constants.OFFER_AMOUNT] = this.refs['detailScreen'].state.amount;
                break;
            case Constants.LMS_TASK_STATUS_CONVERTED:
                this.lmsUpdateParams[Constants.SALE_AMOUNT] = this.refs['detailScreen'].state.amount;
                break;
        }
    };

    hideDialog = () => {
        this.setState({ showCallOptions: false })
    }

    hideCallType = () => {
        this.setState({ showCallTypes: false })
    }

    onCallClick = (mobileNo) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CALL_CLICK)
        // First choose phone or whatsApp
        this.setState({ showCallTypes: true, callType: '' })
    }

    onCallTypePress = (type) => {
        this.setState({ callType: type })
        this.hideCallType()
        this.showCallOptionDialog(type)
    }

    showCallOptionDialog = (type) => {
        if (this.state.leadData.customer_alt_mobile && this.state.leadData.customer_alt_mobile != '') {
            this.setState({ showCallOptions: true })
        } else {
            if (type == 'phone') {
                this.onMobileSelected(this.state.leadData.mobile)
            }
            else {
                this.onWhatsAppSelected(this.state.leadData.mobile)
            }
        }
    }

    onNumberPress = (mobile) => {
        if (this.state.callType == 'phone') {
            this.onMobileSelected(mobile)
        }
        else {
            this.onWhatsAppSelected(mobile)
        }
    }

    onMobileSelected = (mobile) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CALL_PHONE_CLICK)
        this.hideDialog()
        Utility.callNumber(mobile)
    }

    onWhatsAppSelected = (mobile) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CALL_WHATSAPP_CLICK)
        this.hideDialog()
        Utility.onWhatsAppCall(mobile)
    }

    onshare = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_SHARE_CLICK)
        this.refs['detailScreen'].refs['share'].initiateShare()
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{
                            title: this.state.leadData.customer_name || Strings.NA,
                            subtitle: this.state.leadData.mobile
                        }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: ImageAssets.icon_share,
                            onPress: () => this.onshare()
                        }, {
                            image: ImageAssets.icon_edit,
                            onPress: () => this.onEditLead()
                        }]}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <LeadDetailScreen
                            isLoading={this.state.isLoading}
                            loadingMsg={this.state.loadingMsg}
                            ref={'detailScreen'}
                            leadData={this.state.leadData}
                            stockData={this.state.stockData}
                            shortHistoryData={this.state.shortHistoryData}
                            onEditClick={this.onEditClick}
                            onViewAllHistory={this.onViewAllHistory}
                            onAllStockPress={this.onAllStockClick}
                            onStockItemPress={this.onStockItemClick}
                            onUpdatePress={this.onUpdateClick}
                            onCallPress={this.onCallClick}
                            updateFavourite={this.updateFavourite}
                            onAddCarClick={this.onAddCarClick}
                            lmsResetCallBack={this.lmsResetCallBack}
                            onLmsCompleted={this.onLmsCompleted}
                            showStockSelectionModal={this.state.showStockSelectionModal}
                            closeBottomSheet={this.closeBottomSheet}
                            interestedStockData={this.state.interestedStockData}
                            onStockSelection={this.onStockSelection}
                            stockSelectionPlaceHolder={this.state.stockSelectionPlaceHolder}
                            stockSelectionTitle={this.state.stockSelectionTitle}
                            onStockSelectionSubmit={this.onStockSelectionSubmit}
                            showCallOptions={this.state.showCallOptions}
                            hideDialog={this.hideDialog}
                            sharelead={this.sharelead}
                            updateSelectedStock={this.updateSelectedStock}
                            showCallTypes={this.state.showCallTypes}
                            hideCallType={this.hideCallType}
                            onCallTypePress={this.onCallTypePress}
                            onNumberPress={this.onNumberPress}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})