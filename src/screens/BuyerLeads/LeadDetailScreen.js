import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    FlatList,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Modal,
    TextInput,
    SafeAreaView, Keyboard
} from 'react-native'
import { Utility, Constants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import LeadDetails from './../../granulars/BuyerLeads/LeadDetails'
import LeadHistory from './../../granulars/BuyerLeads/LeadHistory'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import StockOptionCard from '../../granulars/StockOptionCard'
import LoadingComponent from '../../components/LoadingComponent'
import LMSParser from "../../lms/components/LMSParser"
import Share from '../../granulars/Share/ShareContainer'
import * as Images from "../../lms/values/Images"
import BuyerLeadStockSelectionCard from "../../components/BuyerLeadStockSelectionCard"
import TextInputLayout from "../../granulars/TextInputLayout"

let extraData = false;
export default class LeadDetailScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: 'details', title: Strings.DETAILS },
                { key: 'history', title: Strings.HISTORY }
            ],
            selectedIndex: 0,
            amount: '',
            keyboardHeight: 0,
            inputHeight: 70,
        }
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }


    renderItem = ({ item, index }) => {
        return (
            <StockOptionCard
                item={item}
                updateFavourite={this.props.updateFavourite}
                onStockItemPress={this.props.onStockItemPress}
            />
        )
    }

    renderStockSelectionItem = ({ item, index }) => {
        return (
            <BuyerLeadStockSelectionCard
                item={item}
                index={index}
                onStockItemPress={this.props.onStockSelection}
            />
        )
    }

    renderScene = ({ route, jumpTo }) => {
        switch (route.key) {
            case 'details':
                return (
                    <LeadDetails
                        jumpTo={jumpTo}
                        data={this.props.leadData}
                        onEditClick={this.props.onEditClick}
                    />
                )
            case 'history':
                return (
                    <LeadHistory
                        jumpTo={jumpTo}
                        data={this.props.shortHistoryData}
                        onViewAllHistory={this.props.onViewAllHistory}
                    />
                )
        }
    }

    nullStockView = () => {
        return (
            <View style={{
                flex: 1,
                marginHorizontal: 16,
                backgroundColor: Colors.WHITE,
                padding: 16,
                borderRadius: 5,
                alignItems: 'center'
            }}>

                <Image style={{ width: 75, height: 50, resizeMode: 'contain' }}
                    source={ImageAssets.icon_no_car} />

                <Text style={[Styles.addCarText, { color: Colors.BLACK }]}>{Strings.NO_CAR_ASSIGNED}</Text>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.props.onAddCarClick()}>

                    <Text style={Styles.addCarText}>{Strings.ADD_CAR}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        extraData = !extraData;
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ backgroundColor: Colors.WHITE, margin: Dimens.margin_small, borderRadius: 5 }}>
                        <TabView
                            navigationState={this.state}
                            renderScene={this.renderScene}
                            swipeEnabled={false}
                            renderTabBar={props =>
                                <TabBar
                                    {...props}
                                    style={Styles.containerTab}
                                    pressColor={Colors.BLACK_25}
                                    activeColor={Colors.BLACK}
                                    inactiveColor={Colors.BLACK_54}
                                    indicatorStyle={{ backgroundColor: Colors.DASHBOARD_CARD_TEXT }}
                                />
                            }
                            onIndexChange={index => this.setState({ index })}
                            initialLayout={{ width: Dimensions.get('window').width }}
                        />
                    </View>

                    <View>
                        <View style={{
                            flexDirection: 'row',
                            paddingHorizontal: 16,
                            padding: 10,
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}>
                            <Text style={Styles.availableText}>{Strings.AVAILABLE_OPTIONS}</Text>

                            {this.props.stockData.length > 0 &&
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onAllStockPress()}>

                                    <Text
                                        style={[Styles.normalText, { color: Colors.PRIMARY }]}>{Strings.ALL_STOCK.toUpperCase()}</Text>
                                </TouchableOpacity>
                            }
                        </View>

                        {this.props.stockData.length > 0 ?
                            <FlatList
                                horizontal={true}
                                extraData={this.props}
                                data={this.props.stockData}
                                renderItem={this.renderItem}
                                ref={(ref) => {
                                    this.flatListRef = ref
                                }}
                                keyExtractor={item => item.id.toString()}
                                showsHorizontalScrollIndicator={false}
                                ItemSeparatorComponent={() => <View style={{ width: 8 }} />}
                                onViewableItemsChanged={this.onViewableItemsChanged}
                                viewabilityConfig={{ itemVisiblePercentThreshold: 50 }}
                                contentContainerStyle={{ paddingHorizontal: Dimens.padding_xx_large }}
                            />
                            :
                            this.nullStockView()
                        }
                    </View>

                    {this.props.stockData.length > 0 &&
                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', paddingVertical: 15 }}>
                            {this.props.stockData.map((item, index) => {
                                return (
                                    <View key={index.toString()} style={[Styles.indicatorView, {
                                        backgroundColor: index == this.state.selectedIndex ? Colors.WHITE : Colors.TRANSPARENT
                                    }]} />
                                )
                            })}
                        </View>
                    }

                    {this.props.isLoading &&
                        <LoadingComponent msg={this.props.loadingMsg} />
                    }
                    <LMSParser
                        ref={'lms'}
                        lmsResetCallBack={this.props.lmsResetCallBack}
                        onLmsCompleted={this.props.onLmsCompleted} />

                    <Share
                        ref={'share'}
                        sharelead={this.props.sharelead}
                        leadData={this.props.leadData}
                        carData={this.props.stockData.length > 0 ? this.props.stockData[this.state.selectedIndex] : null}
                    />

                </ScrollView>

                {/* Bottom view */}
                <View style={{ flexDirection: 'row', height: 50 }}>

                    {Constants.LEAD_STATUS_ID_VALUE[this.props.leadData.lead_status_id] == Constants.LEAD_STATUS_CONVERTED ?   // Converted
                        <TouchableOpacity
                            disabled={true}
                            activeOpacity={0.8}
                            style={Styles.containerUpdate}
                            onPress={() => this.props.onUpdatePress()}>

                            <Image style={{ width: Dimens.icon_normal, height: Dimens.icon_normal, tintColor: Colors.BLACK_54 }}
                                source={ImageAssets.icon_convert} />

                            <Text style={Styles.updateText}>{Strings.CONVERTED}</Text>
                        </TouchableOpacity>
                        :
                        Constants.LEAD_STATUS_ID_VALUE[this.props.leadData.lead_status_id] == Constants.LEAD_STATUS_CLOSE ?    // Closed
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={Styles.containerUpdate}
                                onPress={() => this.props.onUpdatePress()}>

                                <Image style={{ width: Dimens.icon_small, height: Dimens.icon_normal, tintColor: Colors.BLACK_40 }}
                                    source={ImageAssets.icon_user} />

                                <Text style={Styles.updateText}>{Strings.REOPEN_LEAD}</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={Styles.containerUpdate}
                                onPress={() => this.props.onUpdatePress()}>

                                <Image style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }}
                                    source={ImageAssets.icon_edit_black} />

                                <Text style={Styles.updateText}>{Strings.UPDATE}</Text>
                            </TouchableOpacity>
                    }

                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={Styles.containerCall}
                        onPress={() => this.props.onCallPress(this.props.leadData.mobile)}>

                        <Image style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }}
                            source={ImageAssets.icon_call} />

                        <Text style={Styles.callText}>{Strings.CALL}</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.props.showStockSelectionModal}
                    onRequestClose={() => {
                        this.props.closeBottomSheet()
                    }}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight : 0 }}>

                        <TouchableOpacity onPress={() => Keyboard.dismiss()} activeOpacity={0.8} style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'flex-end' }}>
                            <View style={Styles.modal}>
                                <TouchableOpacity style={{ alignSelf: 'flex-end' }}
                                    onPress={() => this.props.closeBottomSheet()}>
                                    <Image style={Styles.imageStyle} source={ImageAssets.icon_cancel} />
                                </TouchableOpacity>
                                <Text style={{
                                    marginHorizontal: Dimens.margin_large,
                                    fontSize: Dimens.text_x_large,
                                    fontFamily: Strings.APP_FONT,
                                    fontWeight: 'bold',
                                    color: Colors.BLACK,
                                    marginBottom: Dimens.margin_xx_large
                                }}>
                                    {this.props.stockSelectionTitle}
                                </Text>
                                {this.props.interestedStockData.length > 0 &&
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.onAllStockPress()}>

                                        <Text style={[Styles.normalText, {
                                            color: Colors.PRIMARY,
                                            marginHorizontal: Dimens.margin_large,
                                            marginBottom: Dimens.margin_xx_large
                                        }]}>{Strings.ALL_STOCK.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                }
                                {this.props.interestedStockData.length > 0 ?
                                    <FlatList
                                        horizontal={true}
                                        extraData={extraData}
                                        data={this.props.interestedStockData}
                                        renderItem={this.renderStockSelectionItem}
                                        ref={(ref) => {
                                            this.flatListRef = ref
                                        }}
                                        keyExtractor={item => item.id.toString()}
                                        showsHorizontalScrollIndicator={false}
                                        ItemSeparatorComponent={() => <View style={{ width: 8 }} />}
                                        onViewableItemsChanged={this.onViewableItemsChanged}
                                        viewabilityConfig={{ itemVisiblePercentThreshold: 50 }}
                                        contentContainerStyle={{ paddingHorizontal: Dimens.padding_xx_large }}
                                    />
                                    :
                                    this.nullStockView()
                                }
                                <TextInputLayout
                                    style={{ margin: Dimens.margin_large }}
                                    ref={(input) => this.TL_AMOUNT = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}
                                >

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={true}
                                        selectTextOnFocus={false}
                                        keyboardType={'number-pad'}
                                        value={this.state.amount ? Utility.formatCurrency(this.state.amount) : ''}
                                        placeholder={this.props.stockSelectionPlaceHolder}
                                        ref={(input) => this.amountInput = input}
                                        onChangeText={(text) => this.setState({ amount: Utility.onlyNumeric(text) })}
                                    />
                                </TextInputLayout>
                                <TouchableOpacity style={{
                                    height: 45,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: Colors.PRIMARY
                                }}
                                    onPress={() => this.props.onStockSelectionSubmit()}>
                                    <Text style={{
                                        fontFamily: Strings.APP_FONT,
                                        color: Colors.WHITE,
                                        fontSize: Dimens.text_large
                                    }}>
                                        {Strings.SUBMIT}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                    </SafeAreaView>
                </Modal>
                {this.props.showCallOptions && this.renderCallOptionDialog()}
                {this.props.showCallTypes && this.renderCallTypeDialog()}
            </View>
        )
    }

    renderCallTypeDialog = () => {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.hideCallType} style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                justifyContent: 'center',
                backgroundColor: Colors.BLACK_54,
                paddingVertical: 30,
                paddingHorizontal: 50
            }}>

                <View style={{ padding: 15, backgroundColor: Colors.WHITE, borderRadius: 5, flexDirection: 'row' }}>
                    <TouchableOpacity
                        style={{ flex: 1, alignItems: 'center', padding: 15 }}
                        onPress={() => this.props.onCallTypePress('phone')}
                        underlayColor={Colors.UNDERLAY_COLOR}>

                        <Image source={ImageAssets.icon_call}
                            style={{ width: 30, height: 30, tintColor: Colors.GREEN_CALL }} />

                        <Text style={{ color: Colors.BLACK_85, fontSize: 16, paddingTop: 8, fontFamily: Strings.APP_FONT }}>{Strings.PHONE}</Text>
                    </TouchableOpacity>

                    {Constants.APP_TYPE == Constants.INDONESIA ?
                        <TouchableOpacity
                            style={{ flex: 1, alignItems: 'center', padding: 15 }}
                            onPress={() => this.props.onCallTypePress('whatsapp')}
                            underlayColor={Colors.UNDERLAY_COLOR}>

                            <Image source={ImageAssets.icon_whatsapp_logo}
                                style={{ width: 32, height: 32 }} />

                            <Text style={{ color: Colors.BLACK_85, fontSize: 16, paddingTop: 8, fontFamily: Strings.APP_FONT }}>{Strings.WHATSAPP}</Text>
                        </TouchableOpacity>
                        :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }

    renderCallOptionDialog = () => {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                justifyContent: 'center',
                backgroundColor: Colors.BLACK_54,
                paddingVertical: 50,
                paddingHorizontal: 20
            }}>
                <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                    style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                    <View style={{ padding: 15 }}>
                        <Text style={{ fontSize: Dimens.text_x_large, fontFamily: Strings.APP_FONT, fontWeight: 'bold', color: Colors.BLACK }}>
                            {Strings.CHOOSE_ANY_ONE}
                        </Text>
                        <TouchableOpacity
                            onPress={() => this.props.onNumberPress(this.props.leadData.mobile)}
                            underlayColor={Colors.UNDERLAY_COLOR}>
                            <Text allowFontScaling={false}
                                style={{
                                    color: Colors.BLACK_85,
                                    fontSize: 16,
                                    padding: 8,
                                    fontFamily: Strings.APP_FONT
                                }}>
                                {Strings.PRIMARY} {this.props.leadData.mobile}
                            </Text>

                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.onNumberPress(this.props.leadData.customer_alt_mobile)}
                            underlayColor={Colors.UNDERLAY_COLOR}>
                            <Text allowFontScaling={false}
                                style={{
                                    color: Colors.BLACK_85,
                                    fontSize: 16,
                                    padding: 8,
                                    fontFamily: Strings.APP_FONT
                                }}>
                                {Strings.ALTERNATE} {this.props.leadData.customer_alt_mobile}
                            </Text>

                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </TouchableOpacity>
        );
    }

    onViewableItemsChanged = ({ viewableItems, changed }) => {
        // alert(JSON.stringify(viewableItems[0]))
        this.setState({ selectedIndex: viewableItems[0].index }, () => {
            this.props.updateSelectedStock(viewableItems[0].index)
        })
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    headText: {
        flex: 1,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    containerCall: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.GREEN_CALL
    },
    containerUpdate: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
    },
    updateText: {
        marginLeft: 10,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        fontFamily: Strings.APP_FONT
    },
    callText: {
        marginLeft: 10,
        fontSize: Dimens.text_large,
        color: Colors.WHITE,
        fontFamily: Strings.APP_FONT
    },
    containerTab: {
        elevation: 0,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.DIVIDER,
        backgroundColor: Colors.WHITE
    },
    normalText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    boldText: {
        color: Colors.BLACK_87,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT
    },
    divider: {
        height: 0.9, width: '100%',
        backgroundColor: Colors.DIVIDER,
        marginVertical: Dimens.padding_xx_large
    },
    availableText: {
        color: Colors.WHITE_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    indicatorView: {
        width: 8, height: 8,
        marginRight: Dimens.margin_x_small,
        borderRadius: 4, borderWidth: 0.9,
        borderColor: Colors.WHITE_54,
    },
    containerList: {
        width: Dimensions.get('window').width - 40,
        overflow: 'hidden',
        backgroundColor: Colors.WHITE,
        alignItems: 'center',
        borderRadius: 5, padding: Dimens.padding_xx_large
    },
    containerbadge: {
        width: '100%',
        position: 'absolute',
        top: -8, left: -64,
        backgroundColor: Colors.GREEN,
        transform: [{ rotate: '315deg' }]
    },
    badgeText: {
        color: Colors.WHITE,
        textAlign: 'center',
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_xx_small,
    },
    addCarText: {
        marginTop: 10,
        color: Colors.PRIMARY,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    imageStyle: {
        margin: Dimens.margin_normal,
        width: Dimens.icon_normal,
        height: Dimens.icon_normal
    },
    modal: {
        backgroundColor: "white",
        width: '100%',
        bottom: 0,
        position: 'absolute'
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    }
});
