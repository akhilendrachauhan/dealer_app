import React, { Component } from 'react'
import AddBuyerLeadScreen from './AddBuyerLeadScreen'
import { View, SafeAreaView, StyleSheet, Keyboard, BackHandler } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import ActionBarWrapper from '../../../granulars/ActionBarWrapper'
import { ScreenStates, ScreenName } from '../../../util/Constants'
import * as APICalls from '../../../api/APICalls'
import AsyncStore from '../../../util/AsyncStore'
import DBFunctions from "../../../database/DBFunctions"
import * as DBConstants from '../../../database/DBConstants'
export default class AddBuyerLeadContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            dpBudgetData: [],
            otrBudgetData: [],
            dpBudgetArray: [],
            otrBudgetArray: [],
            statusData: [],
            sourceData: [],
            countryCodeData: [],
            screenState: ScreenStates.NO_ERROR,
        }
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.sendCurrentScreen(AnalyticsConstants.ADD_BUYER_LEAD_SCREEN)
        let dpBudgetArray = [], otrBudgetArray = []

        let dpBudgetData = await Utility.getConfigArrayData(Constants.DP_BUDGET_VALUES)
        let otrBudgetData = await Utility.getConfigArrayData(Constants.OTR_BUDGET_VALUES)
        let statusData = await Utility.getConfigArrayData(Constants.LEAD_STATUS_VALUES)
        let sourceData = await Utility.getConfigArrayData(Constants.LEAD_SOURCE_VALUES)
        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)

        // Utility.log('sourceData==>', sourceData)
        // Utility.log('statusData==>', statusData)

        for (let i = 0; i < dpBudgetData.length; i++) {
            dpBudgetArray.push(i)
        }
        for (let j = 0; j < otrBudgetData.length; j++) {
            otrBudgetArray.push(j)
        }
        this.setState({
            dpBudgetData: dpBudgetData, dpBudgetArray: dpBudgetArray,
            otrBudgetData: otrBudgetData, otrBudgetArray: otrBudgetArray,
            statusData: statusData, sourceData: sourceData,
            countryCodeData: countryCodeData
        })
        this.refs['addBuyerLeadScreen'].setState({
            isShowDPBudget: this.state.dpBudgetArray.length > 0 ? true : false,
            isShowOTRBudget: this.state.otrBudgetArray.length > 0 ? true : false,
            budgetDP: dpBudgetData[0].value,
            budgetOTR: otrBudgetData[0].value,
            // source: sourceData.length > 0 ? sourceData[0].value : '', // Set source auto fill
        })
    }

    addLead = async (fullName, email, countryCode, mobileNumber, altCountryCode, altMobile, locality, localityId, budgetDp, budgetOTR, source, date, time, status, comment) => {
        // Utility.log('data==>', fullName, email, countryCode, mobileNumber, altCountryCode, altMobile, locality, localityId, budgetDp, budgetOTR, source, date, time, status, comment)

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        // Utility.log('DEALER_ID===>' + values[0][1])

        if (Utility.isValueNullOrEmpty(fullName)) {
            this.refs['addBuyerLeadScreen'].TL_FullName.setError(Strings.ENTER_FULL_NAME)
            this.refs['addBuyerLeadScreen'].FullNameInput.focus()
            return
        }
        this.refs['addBuyerLeadScreen'].TL_FullName.setError('')
        this.refs['addBuyerLeadScreen'].FullNameInput.blur()
        if (fullName.length < 3) {
            this.refs['addBuyerLeadScreen'].TL_FullName.setError(Strings.MIN_3_CHAR)
            this.refs['addBuyerLeadScreen'].FullNameInput.focus()
            return
        }
        this.refs['addBuyerLeadScreen'].TL_FullName.setError('')
        this.refs['addBuyerLeadScreen'].FullNameInput.blur()
        if (!Utility.isValueNullOrEmpty(email) && !Utility.validateEmail(email)) {
            this.refs['addBuyerLeadScreen'].TL_Email.setError(Strings.ENTER_EMAIL)
            this.refs['addBuyerLeadScreen'].EmailInput.focus()
            return
        }
        this.refs['addBuyerLeadScreen'].TL_Email.setError('')
        this.refs['addBuyerLeadScreen'].EmailInput.blur()
        if (Utility.isValueNullOrEmpty(mobileNumber)) {
            this.refs['addBuyerLeadScreen'].TL_Mobile.setError(Strings.ENTER_MOBILE_NUMBER)
            this.refs['addBuyerLeadScreen'].MobileInput.focus()
            return
        }
        if (!Utility.validateMobileNo(mobileNumber, countryCode, this.state.countryCodeData)) {
            this.refs['addBuyerLeadScreen'].TL_Mobile.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['addBuyerLeadScreen'].MobileInput.focus()
            return
        }
        this.refs['addBuyerLeadScreen'].TL_Mobile.setError('')
        this.refs['addBuyerLeadScreen'].MobileInput.blur()
        if (!Utility.isValueNullOrEmpty(altMobile) && !Utility.validateMobileNo(altMobile, altCountryCode, this.state.countryCodeData)) {
            this.refs['addBuyerLeadScreen'].TL_AlternateMobile.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['addBuyerLeadScreen'].AlternateMobileInput.focus()
            return
        }
        this.refs['addBuyerLeadScreen'].TL_AlternateMobile.setError('')
        this.refs['addBuyerLeadScreen'].AlternateMobileInput.blur()
        if (parseInt(mobileNumber) == parseInt(altMobile)) {
            Utility.showToast(Strings.MOBILE_NOT_TO_BE_SAME)
            return
        }
        if (Constants.APP_TYPE == Constants.INDONESIA && parseInt(budgetDp) != 0 && parseInt(budgetDp) >= parseInt(budgetOTR)) {
            Utility.showToast(Strings.BUDGET_VALIDATION)
            return
        }
        if (Utility.isValueNullOrEmpty(source)) {
            Utility.showToast(Strings.SELECT_SOURCE)
            return
        } if (Utility.isValueNullOrEmpty(date) && Utility.isValueNullOrEmpty(time)) {
            Utility.showToast(Strings.SELECT_FOLLOW_UP_DATE_TIME)
            return
        } if (Utility.isValueNullOrEmpty(date)) {
            Utility.showToast(Strings.SELECT_FOLLOW_UP_DATE)
            return
        } if (Utility.isValueNullOrEmpty(time)) {
            Utility.showToast(Strings.SELECT_FOLLOW_UP_TIME)
            return
        } if (Utility.isValueNullOrEmpty(status)) {
            Utility.showToast(Strings.SELECT_STATUS)
            return
        }
        Keyboard.dismiss()
        let followupDate = Utility.setDateTime(date, time)
        let mobileNo = countryCode + mobileNumber
        let altMobileNo = altMobile != '' ? altCountryCode + altMobile : ''
        try {
            Utility.sendEvent(AnalyticsConstants.LEAD_ADD_SUBMIT)
            APICalls.addLead(values[0][1], fullName, email, mobileNo, altMobileNo, localityId, budgetDp, budgetOTR, source, followupDate, status, comment, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
        this.setState({ isLoading: true })
    }

    onSuccess = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        Utility.sendEvent(AnalyticsConstants.LEAD_ADD_SUCCESS,{lead_id: response.data.id})
        Utility.log('onSuccessAddLead===> ', JSON.stringify(response))
        this.setState({ isLoading: false }, () => {
            this.props.navigation.state.params &&
                this.props.navigation.state.params.callBack()
            this.props.navigation.goBack()
        })
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        Utility.sendEvent(AnalyticsConstants.LEAD_ADD_FAILURE)
        Utility.log('onFailureAddLead===> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    onRetryClick = () => {

    }

    onLocalityPress = () => {
        let dbFunctions = new DBFunctions()
        this.props.navigation.navigate('searchContainer', {
            dbQuery: dbFunctions.getLocalityQueryText(),
            hintText: 'Search by Locality',
            callback: this.onLocalitySelected
        })
    }

    onLocalitySelected = (result) => {
        Utility.log('SelectedLocality', result)
        this.refs['addBuyerLeadScreen'].setState({
            locality: result[DBConstants.LOCALITY],
            localityId: result[DBConstants.LOCALITY_ID]
        })
    }

    onBackPress = () => {
        Utility.confirmDialog(Strings.ALERT, Strings.GO_BACK_FROM_LOAN, Strings.CANCEL, Strings.YES, this.confirmCallback)
    }

    confirmCallback = (status) => {
        if (status) {
            this.props.navigation.goBack();
        }
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.ADD_LEAD }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                    // onRetry={this.onRetryClick}
                    >

                        <AddBuyerLeadScreen
                            ref={'addBuyerLeadScreen'}
                            addLead={this.addLead}
                            isLoading={this.state.isLoading}
                            loadingMsg={this.state.loadingMsg}
                            onLocalityPress={this.onLocalityPress}
                            dpBudgetData={this.state.dpBudgetData}
                            dpBudgetArray={this.state.dpBudgetArray}
                            otrBudgetData={this.state.otrBudgetData}
                            otrBudgetArray={this.state.otrBudgetArray}
                            statusData={this.state.statusData}
                            sourceData={this.state.sourceData}
                            countryCodeData={this.state.countryCodeData}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})