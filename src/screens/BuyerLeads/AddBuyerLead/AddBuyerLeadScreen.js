import React, { Component } from 'react'
import {
    StyleSheet, Text, View, Image, FlatList, ScrollView,
    TouchableOpacity, Dimensions, Platform, TextInput, Slider
} from 'react-native'
import { Utility, Constants } from '../../../util'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import TextInputLayout from "../../../granulars/TextInputLayout"
import DateTimePicker from "react-native-modal-datetime-picker"
import { Dropdown } from 'react-native-material-dropdown'
import DateFormat from '../../../util/DateFormat'
import LoadingComponent from '../../../components/LoadingComponent'
import MultiSlider from '../../../granulars/MultiSlider/MultiSlider'
const { width: widthScreen, height: heightScreen } = Dimensions.get('window')

export default class AddBuyerLeadScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isShowDPBudget: false,
            isShowOTRBudget: false,
            fullName: '',
            email: '',
            mobile: '',
            alternateMobile: '',
            locality: '',
            localityId: '',
            budgetDP: '',
            budgetIdDP: 0,
            budgetOTR: '',
            budgetIdOTR: 0,
            defaultDPBudget: 0,
            defaultOTRBudget: 0,
            date: '',
            formatedDate: '',
            time: '',
            formatedTime: '',
            source: '',
            status: '',
            comment: '',
            showTimePiker: false,
            showDatePicker: false,
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            altCountryCode: Constants.DEFAULT_COUNTRY_CODE,
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={Styles.containerInner}>
                        <TextInputLayout
                            ref={(input) => this.TL_FullName = input}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.ORANGE}>

                            <TextInput
                                style={Styles.textInput}
                                returnKeyType='next'
                                selectTextOnFocus={false}
                                value={this.state.fullName}
                                placeholder={Strings.FULL_NAME + '*'}
                                ref={(input) => this.FullNameInput = input}
                                onSubmitEditing={() => this.EmailInput.focus()}
                                onChangeText={(text) => this.setState({ fullName: text.replace(/[`~0-9!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '') })} />
                        </TextInputLayout>

                        <TextInputLayout
                            ref={(input) => this.TL_Email = input}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.ORANGE}>

                            <TextInput
                                style={Styles.textInput}
                                returnKeyType='next'
                                keyboardType={'email-address'}
                                selectTextOnFocus={false}
                                value={this.state.email}
                                placeholder={Strings.EMAIL}
                                ref={(input) => this.EmailInput = input}
                                onSubmitEditing={() => this.MobileInput.focus()}
                                onChangeText={(text) => this.setState({ email: text })} />
                        </TextInputLayout>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Dropdown
                                containerStyle={{ flex: 0.4 }}
                                dropdownOffset={{ top: 40, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.countryCodeData}
                                absoluteRTLLayout={true}
                                value={this.state.countryCode}
                                onChangeText={(value, index, data) => {
                                    this.setState({ countryCode: value, selectedItem: data[index] })
                                }}
                            />

                            <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                <TextInputLayout
                                    ref={(input) => this.TL_Mobile = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        keyboardType={'numeric'}
                                        selectTextOnFocus={false}
                                        value={this.state.mobile}
                                        placeholder={Strings.MOBILE + '*'}
                                        ref={(input) => this.MobileInput = input}
                                        maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                        onSubmitEditing={() => this.AlternateMobileInput.focus()}
                                        onChangeText={(text) => this.setState({ mobile: text })} />
                                </TextInputLayout>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                            <Dropdown
                                containerStyle={{ flex: 0.4 }}
                                dropdownOffset={{ top: 40, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.countryCodeData}
                                absoluteRTLLayout={true}
                                value={this.state.altCountryCode}
                                onChangeText={(value, index, data) => {
                                    this.setState({ altCountryCode: value, selectedItem: data[index] })
                                }}
                            />

                            <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                <TextInputLayout
                                    ref={(input) => this.TL_AlternateMobile = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        keyboardType={'numeric'}
                                        selectTextOnFocus={false}
                                        value={this.state.alternateMobile}
                                        placeholder={Strings.ALTERNATE_MOBILE}
                                        maxLength={Utility.maxLengthMobileNo(this.state.altCountryCode)}
                                        ref={(input) => this.AlternateMobileInput = input}
                                        onSubmitEditing={() => this.LocalityInput.focus()}
                                        onChangeText={(text) => this.setState({ alternateMobile: text })} />
                                </TextInputLayout>
                            </View>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onLocalityPress()}>
                            <TextInputLayout
                                ref={(input) => this.TL_Locality = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.ORANGE}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.state.locality}
                                    placeholder={Strings.LOCALITY}
                                    ref={(input) => this.LocalityInput = input}
                                    onSubmitEditing={() => { }}
                                    onChangeText={(text) => this.setState({ locality: text })}
                                    onTouchStart={() => Platform.OS === 'ios' ? this.props.onLocalityPress() : Utility.log('Pressed...')}
                                />
                            </TextInputLayout>
                        </TouchableOpacity>

                        {this.state.isShowDPBudget && Constants.APP_TYPE == Constants.INDONESIA ?
                            <View style={{ marginTop: 20 }}>
                                <Text style={Styles.smallText}>{Strings.DP_BUDGET}</Text>
                                <Text style={{ marginTop: 5, color: Colors.BLACK, fontSize: Dimens.text_large }}>{this.state.budgetDP}</Text>

                                <MultiSlider
                                    markerStyle={{ backgroundColor: Colors.PRIMARY }}
                                    containerStyle={{ alignItems: 'center' }}
                                    sliderLength={widthScreen - 60}
                                    isMarkersSeparated={false}
                                    optionsArray={this.props.dpBudgetArray}
                                    values={[this.state.defaultDPBudget]}
                                    onValuesChangeFinish={(value) => {
                                        // Utility.log('finishValue==>', value)
                                        var index = parseInt(value)
                                        this.setState({
                                            defaultDPBudget: index,
                                            budgetDP: this.props.dpBudgetData[index].value,
                                            budgetIdDP: this.props.dpBudgetData[index].key
                                        })
                                    }}
                                />
                            </View>
                            :
                            null
                        }

                        {this.state.isShowOTRBudget &&
                            <View style={{ marginTop: 15 }}>
                                <Text style={Styles.smallText}>{Strings.OTR_BUDGET}</Text>
                                <Text style={{ marginTop: 5, color: Colors.BLACK, fontSize: Dimens.text_large }}>{this.state.budgetOTR}</Text>

                                <MultiSlider
                                    markerStyle={{ backgroundColor: Colors.PRIMARY }}
                                    containerStyle={{ alignItems: 'center' }}
                                    sliderLength={widthScreen - 60}
                                    isMarkersSeparated={false}
                                    optionsArray={this.props.otrBudgetArray}
                                    values={[this.state.defaultOTRBudget]}
                                    onValuesChangeFinish={(value) => {
                                        // Utility.log('finishValue==>', value)
                                        var index = parseInt(value)
                                        this.setState({
                                            defaultOTRBudget: index,
                                            budgetOTR: this.props.otrBudgetData[index].value,
                                            budgetIdOTR: this.props.otrBudgetData[index].key
                                        })
                                    }}
                                />
                            </View>
                        }
                    </View>

                    <View style={{ backgroundColor: Colors.WHITE, padding: Dimens.padding_xx_large }}>

                        <View>
                            <Text style={Styles.headText}>{Strings.SOURCE + '*'}</Text>

                            <Dropdown
                                containerStyle={{ flex: 1, paddingHorizontal: 15 }}
                                dropdownOffset={{ top: 12, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.sourceData}
                                absoluteRTLLayout={true}
                                value={this.state.source}
                                onChangeText={(value, index, data) => {
                                    this.setState({ source: value, selectedItem: data[index] })
                                }}
                            />
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={Styles.headText}>{Strings.FOLLOW_UP + '*'}</Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.setState({ showDatePicker: true })}>
                                        <TextInputLayout
                                            ref={(input) => this.TL_Date = input}
                                            showRightDrawable={true}
                                            focusColor={Colors.ORANGE}
                                            hintColor={Colors.GRAY}
                                            errorColor={Colors.ORANGE}
                                            onRightClick={() => this.setState({ showDatePicker: true })}
                                            rightDrawable={ImageAssets.icon_date}
                                            rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                            rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                            <TextInput
                                                style={Styles.textInput}
                                                returnKeyType='next'
                                                editable={false}
                                                selectTextOnFocus={false}
                                                value={this.state.date}
                                                placeholder={Strings.DATE}
                                                ref={(input) => this.dateInput = input}
                                                onSubmitEditing={() => this.timeInput.focus()}
                                                onChangeText={(text) => this.setState({ date: text })} />
                                        </TextInputLayout>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, marginLeft: 16 }}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.setState({ showTimePiker: true })}>
                                        <TextInputLayout
                                            ref={(input) => this.TL_Time = input}
                                            showRightDrawable={true}
                                            focusColor={Colors.ORANGE}
                                            hintColor={Colors.GRAY}
                                            errorColor={Colors.ORANGE}
                                            onRightClick={() => this.setState({ showTimePiker: true })}
                                            rightDrawable={ImageAssets.icon_time}
                                            rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                            rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                            <TextInput
                                                style={Styles.textInput}
                                                returnKeyType='next'
                                                editable={false}
                                                selectTextOnFocus={false}
                                                value={this.state.time}
                                                placeholder={Strings.TIME}
                                                ref={(input) => this.timeInput = input}
                                                onSubmitEditing={() => { }}
                                                onChangeText={(text) => this.setState({ time: text })} />
                                        </TextInputLayout>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={Styles.headText}>{Strings.STATUS + '*'}</Text>

                            <FlatList
                                contentContainerStyle={{ marginVertical: 8 }}
                                ref={(ref) => { this.flatListRef = ref; }}
                                extraData={this.state}
                                numColumns={3}
                                data={this.props.statusData}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>

                        <View style={{ marginTop: 8 }}>
                            <TextInputLayout
                                ref={(input) => this.TL_Comment = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.ORANGE}>

                                <TextInput
                                    style={Styles.commentTextInput}
                                    returnKeyType='done'
                                    multiline={true}
                                    selectTextOnFocus={false}
                                    value={this.state.comment}
                                    placeholder={Strings.ADD_COMMENT}
                                    ref={(input) => this.CommentInput = input}
                                    onSubmitEditing={() => { }}
                                    onChangeText={(text) => this.setState({ comment: text })} />
                            </TextInputLayout>
                        </View>
                    </View>
                </ScrollView>

                <View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{ padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                        onPress={() => this.props.addLead(this.state.fullName, this.state.email, this.state.countryCode, this.state.mobile, this.state.altCountryCode,
                            this.state.alternateMobile, this.state.locality, this.state.localityId, this.state.budgetIdDP, this.state.budgetIdOTR, this.state.source,
                            this.state.formatedDate, this.state.formatedTime, this.state.status, this.state.comment)}>

                        <Text style={Styles.buttoText}>{Strings.ADD_LEAD}</Text>
                    </TouchableOpacity>
                </View>

                <DateTimePicker
                    minimumDate={new Date()}
                    isVisible={this.state.showDatePicker}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    mode={'date'}
                />

                <DateTimePicker
                    is24Hour={false}
                    minimumDate={new Date()}
                    isVisible={this.state.showTimePiker}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideDateTimePicker}
                    mode={'time'}
                />

                {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
            </View>
        )
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                style={{ width: '33%', height: 65 }}
                activeOpacity={0.8}
                onPress={() => {
                    if (this.state.status == item.value) {
                        this.setState({ status: '' })
                    }
                    else {
                        this.setState({ status: item.value })
                    }
                }}>

                <View style={[Styles.containerList, {
                    borderColor: item.value == this.state.status ? Colors.PRIMARY : Colors.BLACK_25,
                    backgroundColor: item.value == this.state.status ? Colors.PRIMARY_33 : Colors.WHITE
                }]}>
                    <Text numberOfLines={2} style={[Styles.normalText, { color: item.value == this.state.status ? Colors.PRIMARY : Colors.BLACK_54 }]}>{item.value}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _handleTimePicked = (time) => {
        var currentMS = new Date().getTime()
        var followUpTime = DateFormat(time.toString(), 'HH:MM')
        var followUpDate = DateFormat(this.state.date, 'mmmm dd, yyyy')
        var followUpMS = new Date(followUpDate + ' ' + followUpTime).getTime()

        if (this.state.date == '') {
            Utility.showToast(Strings.SELECT_FOLLOW_UP_DATE)
        }
        else {
            if (followUpMS < currentMS) {
                Utility.showToast(Strings.GREATER_THAN_CURRENT_TIME)
                this.setState({ time: '', formatedTime: '' })
            }
            else {
                // 6:02 PM // 16:00
                let timeFormat = DateFormat(time.toString(), 'hh:MM TT')
                let formatedTime = DateFormat(time.toString(), 'HH:MM')
                this.setState({ time: timeFormat, formatedTime: formatedTime })
            }
        }

        this._hideDateTimePicker();
    }

    _handleDatePicked = (date) => {
        // 23 Oct, 2019 // 2109-10-02
        let dateFormat = DateFormat(date.toString(), 'dd mmm, yyyy')
        let formatedDate = DateFormat(date.toString(), 'yyyy-mm-dd')
        this.setState({ date: dateFormat, formatedDate: formatedDate })

        this._hideDateTimePicker();
    }

    _hideDateTimePicker = () => {
        this.setState({ showDatePicker: false, showTimePiker: false })
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.LEAD_BG
    },
    containerInner: {
        elevation: 5,
        borderRadius: 5,
        margin: Dimens.margin_small,
        padding: Dimens.padding_xx_large,
        backgroundColor: Colors.WHITE,
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    commentTextInput: {
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    headText: {
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    buttoText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        textAlign: 'center',
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    containerList: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 26,
        marginTop: Dimens.margin_small,
        marginHorizontal: Dimens.margin_small,
        padding: Dimens.padding_normal
    }
})
