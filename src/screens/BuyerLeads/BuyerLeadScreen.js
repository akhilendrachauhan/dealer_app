import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { Utility } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import WeekDateStrip from '../../components/WeekDateStrip'

export default class BuyerLeadScreen extends Component {

    constructor(props) {
        super(props);
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.props.onTabPress(item)}>

                <View style={{ flexDirection: 'row', height: 92, alignItems: 'center', borderRadius: 5, margin: Dimens.margin_small, backgroundColor: Colors.WHITE }}>

                    <View style={{ flex: 0.7, paddingLeft: 16 }}>
                        <Text style={Styles.headText}>{item.label}</Text>
                        <Text style={Styles.descText}>{item.label}</Text>
                    </View>

                    <View style={Styles.dashedView} />

                    {/* <View style={{ width: 5, marginVertical: 8 }}>
                        <Image style={{ width: 2, height: '100%' }}
                            source={ImageAssets.icon_dash_separator} />
                    </View> */}

                    <View style={{ flex: 0.3, alignItems: 'center' }}>
                        <Text style={[Styles.countText, { color: item.color }]}>{item.count}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <View style={{ marginHorizontal: 16, marginTop: 8 }}>
                            <WeekDateStrip
                                onDateSelected={(date, position) => this.props.onDateChange(date, position)}
                            />
                        </View>

                        <FlatList
                            style={{ marginHorizontal: Dimens.margin_small }}
                            ref={(ref) => { this.flatListRef = ref; }}
                            extraData={this.props}
                            data={this.props.leadCountData}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => '' + index}
                            showsVerticalScrollIndicator={false}
                            onRefresh={this.props.onRefresh}
                            refreshing={this.props.refreshing}
                        />
                    </View>
                </ScrollView>

                {/* Menu View  */}
                {this.props.showMenu &&
                    <View style={Styles.containerMenu}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onAddLeadMenuClick()}>

                            <Text style={Styles.menuItemText}>{Strings.ADD_LEAD}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ marginTop: Dimens.margin_normal }}
                            activeOpacity={0.8}
                            onPress={() => this.props.onLeadFinderMenuClick()}>

                            <Text style={Styles.menuItemText}>{Strings.LEAD_FINDER}</Text>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    headText: {
        fontSize: Dimens.text_x_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    countText: {
        fontSize: Dimens.text_40,
        fontFamily: Strings.APP_FONT
    },
    containerMenu: {
        width: 140,
        top: -10,
        right: 8,
        elevation: 5,
        borderRadius: 2,
        position: 'absolute',
        padding: Dimens.padding_normal,
        backgroundColor: Colors.GRAY_LIGHT_BG,
    },
    menuItemText: {
        color: Colors.BLACK,
        fontFamily: Strings.APP_FONT,
        fontSize: Dimens.text_large,
        marginTop: Dimens.margin_small
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    },
})
