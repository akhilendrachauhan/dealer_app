import React, { Component } from 'react'
import { FlatList, Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Utility } from '../../../util'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import BuyerLeadTupleComponent from "../../../components/BuyerLeadTupleComponent"

export default class BuyerLeadFinderScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {

    }

    renderItem = ({ item, index }) => {
        return (
            <BuyerLeadTupleComponent
                item={item}
                index={index}
                callback={this.props.onCardPress} />
        )
    }

    renderSortModal = () => {
        let sortIndex = this.props.sortIndex;
        return (<View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => this.props.setModalVisible(false)}
                style={{ flex: 1, backgroundColor: Colors.BLACK_40 }} />
            <View style={Styles.sortContainer}>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Text style={Styles.sortText}>{Strings.SORT_BY}</Text>
                    <TouchableOpacity onPress={() => this.props.setModalVisible(false)}
                        style={Styles.icon}>
                        <Image resizeMode={'cover'} style={Styles.icon}
                            source={require('../../../assets/drawable/cancel_icon.png')} />
                    </TouchableOpacity>

                </View>
                <TouchableOpacity
                    onPress={() => this.props.onSortOptionPress(0)}
                    style={Styles.sortOptionContainer}>
                    <Image resizeMode={'cover'} style={[Styles.icon, { tintColor: sortIndex == 0 ? Colors.PRIMARY : Colors.BLACK_54 }]}
                        source={require('../../../assets/drawable/recent.png')} />
                    <Text style={[Styles.sortOptionText, { color: sortIndex == 0 ? Colors.PRIMARY : Colors.BLACK_54 }]}>{Strings.LAST_UPDATED}</Text>

                </TouchableOpacity>
                <View style={{ height: 1, backgroundColor: Colors.BLACK_11 }} />
                <TouchableOpacity
                    onPress={() => this.props.onSortOptionPress(1)}
                    style={Styles.sortOptionContainer}>
                    <Image resizeMode={'cover'} style={[Styles.icon, { tintColor: sortIndex == 1 ? Colors.PRIMARY : Colors.BLACK_54 }]}
                        source={require('../../../assets/drawable/star_hollow.png')} />
                    <Text style={[Styles.sortOptionText, { color: sortIndex == 1 ? Colors.PRIMARY : Colors.BLACK_54 }]}>{Strings.RECENTLY_CREATED}</Text>

                </TouchableOpacity>

            </View>
        </View>)
    };

    render() {
        return (
            <View style={Styles.container}>
                <FlatList
                    style={{ marginHorizontal: Dimens.margin_small }}
                    ref={(ref) => {
                        this.flatListRef = ref;
                    }}
                    extraData={this.state}
                    data={this.props.data}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.props.onRefresh}
                    refreshing={this.props.refreshing}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.props.handleLoadMore}
                />
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.modalVisible}>
                    {this.renderSortModal()}
                </Modal>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    icon: {
        height: 24,
        width: 24
    },
    sortText: {
        flex: 1,
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK,
        marginLeft: Dimens.margin_large,
        marginTop: 20,
        marginBottom: Dimens.margin_normal,
        fontWeight: '500'
    },
    sortOptionText: {
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_54,
        marginLeft: Dimens.margin_large
    },
    sortOptionContainer: {
        flexDirection: 'row',
        width: '100%',
        paddingVertical: Dimens.padding_x_large
    },
    sortContainer: {
        width: '100%',
        padding: 12,
        flexDirection: 'column',
        backgroundColor: Colors.WHITE
    }
});
