import React, { Component } from 'react'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import BuyerLeadFinderScreen from './BuyerLeadFinderScreen'
import { Utility, Constants,AnalyticsConstants } from '../../../util'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import DBFunctions from "../../../database/DBFunctions"
import * as APICalls from '../../../api/APICalls'
import AsyncStore from '../../../util/AsyncStore'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from '../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import ActionBarWrapper from "../../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../../util/Constants'
import ActionBarSearchComponent from "../../../granulars/ActionBarSearchComponent"

export default class BuyerLeadFinderContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: [],
            leadCount: 0,
            refreshing: false,
            showSearchBar: false,
            screenState: ScreenStates.IS_LOADING,
            modalVisible: false,
            sortIndex: 0,
            filterCount: 0,
            page: 1,
            hasNext: false,
            isApiCalling: false,
            filterData: undefined,
            filterParams: {}
        }
    }

    componentDidMount() {
        this.getLeadFinderData('', true)
        Utility.sendCurrentScreen(AnalyticsConstants.LEAD_FINDER_LIST_SCREEN)
        let dbFunctions = new DBFunctions();
        dbFunctions.getDistinctModels().then(result => {
            Utility.log('MMV', result.length);
        })
    }

    onRetryClick = () => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1, showSearchBar: false }, () => {
            this.getLeadFinderData('', true)
        })
    }

    handleLoadMore = () => {
        if (this.state.hasNext && !this.state.isApiCalling) {
            let nextPage = this.state.page + 1
            this.setState({ refreshing: true, page: nextPage }, () => {
                this.getLeadFinderData('', false)
            })
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true, page: 1, showSearchBar: false }, () => {
            this.getLeadFinderData('', true)
        })
    }

    onSubmitEditing = (text) => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
            this.getLeadFinderData(text, true)
        })
    }

    onSearchTextChange = (text) => {
        // this.setState({ screenState: ScreenStates.IS_LOADING }, () => {
        //     this.getLeadFinderData(text, true)
        // })
    }

    onSearchBackPress = () => {
        this.setState({ screenState: ScreenStates.IS_LOADING, showSearchBar: false }, () => {
            this.getLeadFinderData('', true)
        })
    }

    onSearch = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_FINDER_SEARCH_CLICK)
        this.setState({ showSearchBar: true })
    }

    getLeadFinderData = async (text, isRefresh) => {

        if (isRefresh)
            this.setState({ isApiCalling: true, data: [] })
        else
            this.setState({ isApiCalling: true })

        Utility.log('filterParam===> ', JSON.stringify(this.state.filterParams))

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        this.state.filterParams['dealer_id'] = values[0][1]
        this.state.filterParams['keyword'] = text                   // for Search any data
        this.state.filterParams['sort_by'] = this.state.sortIndex   // for Sorting
        this.state.filterParams['page_no'] = this.state.page        // for Pagination

        try {
            APICalls.getLeadFinderList(this.state.filterParams, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = (response) => {
        if (response && response.data && response.data.lead_data.length > 0)
            this.setState({
                refreshing: false,
                isApiCalling: false,
                hasNext: response.data.next_page,
                screenState: ScreenStates.NO_ERROR,
                leadCount: response.data.total_records,
                // data: response.data.lead_data,
                data: [...this.state.data, ...response.data.lead_data],
                filterData: this.state.filterData == undefined ? response.data.filterData : this.state.filterData
            })
        else
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, refreshing: false, isApiCalling: false, hasNext: false })

        Utility.log('onSuccessLeadListing===> ', JSON.stringify(response))
        Utility.log('filterdataState', this.state.filterData)
        Utility.log('filterdataResponse', response.data.filterData)
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        Utility.log('onFailureLeadListing===> ', JSON.stringify(response))
        this.setState({
            hasNext: false,
            refreshing: false, isApiCalling: false,
            screenState: ScreenStates.SERVER_ERROR
        })
    }

    onFilterPress = () => {
        if (this.state.filterData != undefined) {
            Utility.sendEvent(AnalyticsConstants.LEAD_FINDER_FILTER_CLICK)
            this.props.navigation && this.props.navigation.navigate('filter', {
                onFilterApply: this.onFilterApply,
                filterData: this.state.filterData
            })
        }
        else {
            Utility.showToast(Strings.NO_DATA_FOUND)
        }
    }

    onFilterApply = (filterData, params, count) => {
        this.setState({
            filterCount: count,
            filterData: filterData,
            filterParams: params,
            page: 1,
            screenState: ScreenStates.IS_LOADING
        }, () => {
            this.getLeadFinderData('', true)
        })
        Utility.log(filterData, params, count)
    }

    setModalVisible = (isVisible) => {
        this.setState({ modalVisible: isVisible })
    }

    onCardPress = (item) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_FINDER_LIST_ITEM_CLICK)
        this.props.navigation.navigate('leadDetail', {
            leadId: item.lead_id
        })
    }

    onSortPress = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_FINDER_SORT_CLICK)
        this.setModalVisible(true)
    }

    onSortOptionPress = (index) => {
        this.setState({
            sortIndex: index,
            modalVisible: false
        }, () => {
            this.setState({ screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
                this.getLeadFinderData('', true)
            })
        })
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_LEAD_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    }

    onBackPress = () => {
        this.props.navigation && this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.LEAD_FINDER + ' (' + this.state.leadCount + ')' }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: require('../../../assets/drawable/sort.png'),
                            onPress: () => this.onSortPress()
                        }, {
                            image: require('../../../assets/drawable/filter.png'),
                            badge: this.state.filterCount === 0 ? undefined : this.state.filterCount,
                            onPress: () => this.onFilterPress()
                        }, {
                            image: require('../../../assets/drawable/search.png'),
                            onPress: () => this.onSearch()
                        }]}
                    />

                    {this.state.showSearchBar ? this.openActionSearchBar() : null}

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <BuyerLeadFinderScreen
                            data={this.state.data}
                            onRefresh={this.onRefresh}
                            onCardPress={this.onCardPress}
                            handleLoadMore={this.handleLoadMore}
                            sortIndex={this.state.sortIndex}
                            refreshing={this.state.refreshing}
                            modalVisible={this.state.modalVisible}
                            setModalVisible={this.setModalVisible}
                            onSortOptionPress={this.onSortOptionPress}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})