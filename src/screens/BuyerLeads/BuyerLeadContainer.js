import React, { Component } from 'react'
import BuyerLeadScreen from './BuyerLeadScreen'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import { Utility, Constants,AnalyticsConstants } from '../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../values/Styles'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../util/Constants'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import DateFormat from '../../util/DateFormat'
import { EventRegister } from '../../util/EventRegister'

export default class BuyerLeadContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showMenu: false,
            screenState: ScreenStates.IS_LOADING,
            leadCountData: [],
            data: [],
            datePosition: 0,
            selectedDate: DateFormat(new Date(), 'yyyy-mm-dd'),
            refreshing: false
        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.BUYER_LEAD_SCREEN)
    }

    componentDidMount() {
        EventRegister.addEventListener(Constants.LMS_SYNC, (data) => {
            if (data) {
                this.getLeadCount()
            }
        })

        this.getLeadCount()
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(Constants.LMS_SYNC)
    }

    onRetryClick = () => {
        this.getLeadCount()
    }

    onRefresh = () => {
        this.getLeadCount()
    }

    onDateChange = (date, position) => {
        // Utility.log('date===> ' + JSON.stringify(date) + ' --> ' + position)
        this.setState({
            datePosition: position,
            selectedDate: DateFormat(date, 'yyyy-mm-dd'),
            leadCountData: this.state.data[position].count
        })
    }

    getLeadCount = async () => {

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            APICalls.getLeadCount(values[0][1], this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = (response) => {
        if (response && response.data && response.data.length > 0) {
            this.setState({
                screenState: ScreenStates.NO_ERROR,
                data: response.data,
                leadCountData: response.data[this.state.datePosition].count
            })
        }
        else {
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND })
        }
        Utility.log('onSuccessLeadCount===> ', JSON.stringify(response))
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        this.setState({ screenState: ScreenStates.SERVER_ERROR })
        Utility.log('onFailureLeadCount===> ', JSON.stringify(response))
    }

    onTabPress = (item) => {
        this.onHideMenu()
        if (item.count > 0) {
            Utility.sendEvent(AnalyticsConstants.LEAD_BUCKET_CLICK)
            this.props.navigation.navigate('buyerLeadList', {
                item: item,
                leadDate: this.state.selectedDate
            })
        }
        else {
            Utility.showToast(Strings.NO_LEADS_AVAILABLE)
        }
    }

    onToggleMenu = () => {
        this.setState({ showMenu: !this.state.showMenu })
    }

    onHideMenu = () => {
        this.setState({ showMenu: false })
    }

    onAddLeadMenuClick = () => {
        this.onHideMenu()
        Utility.sendEvent(AnalyticsConstants.LEAD_ADD_CLICK)
        this.props.navigation.navigate('addBuyerLead', {
            callBack: this.getLeadCount
        })
    }

    onLeadFinderMenuClick = () => {
        this.onHideMenu()
        Utility.sendEvent(AnalyticsConstants.LEAD_FINDER_CLICK)
        this.props.navigation.navigate('buyerLeadFinder')
    }

    onBackPress = () => {
        this.onHideMenu()
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.BUYER_LEADS }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            //     image: ImageAssets.icon_search,
                            //     onPress: () => { }
                            // }, {
                            image: ImageAssets.icon_menu,
                            onPress: () => this.onToggleMenu()
                        }]}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <BuyerLeadScreen
                            onDateChange={this.onDateChange}
                            leadCountData={this.state.leadCountData}
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh}
                            showMenu={this.state.showMenu}
                            onTabPress={this.onTabPress}
                            onAddLeadMenuClick={this.onAddLeadMenuClick}
                            onLeadFinderMenuClick={this.onLeadFinderMenuClick}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})