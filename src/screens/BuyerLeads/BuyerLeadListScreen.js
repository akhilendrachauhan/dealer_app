import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { Utility } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import BuyerLeadTupleComponent from "../../components/BuyerLeadTupleComponent"

export default class BuyerLeadListScreen extends Component {

    constructor(props) {
        super(props);
    }

    renderItem = ({ item, index }) => {
        return (
            <BuyerLeadTupleComponent
                item={item}
                index={index}
                callback={this.props.onItemPress}
            />
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                {this.props.leadsData.length > 0 &&
                    <FlatList
                        style={{ marginHorizontal: Dimens.margin_small }}
                        ref={(ref) => { this.flatListRef = ref; }}
                        extraData={this.props}
                        data={this.props.leadsData}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => '' + index}
                        showsVerticalScrollIndicator={false}
                        onRefresh={this.props.onRefresh}
                        refreshing={this.props.refreshing}
                        onEndReachedThreshold={0.5}
                        onEndReached={this.props.handleLoadMore}
                    />
                }
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    containerItem: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 5,
        borderLeftWidth: 5,
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_normal,
        marginVertical: Dimens.margin_x_small
    },
    headText: {
        flex: 1,
        fontWeight: '600',
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
    },
    timeText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    smallText: {
        fontSize: Dimens.text_x_small,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    tagText: {
        position: 'absolute',
        right: 8,
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    },
})
