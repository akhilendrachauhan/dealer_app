import React, { Component } from 'react'
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from 'react-native'
import { Utility, Constants } from '../../../util'
import { Strings, Colors, Dimens } from '../../../values'
import * as ImageAssets from '../../../assets/ImageAssets'
import StockListingCard from '../../../granulars/StockListingCard'

export default class StockListScreen extends Component {
    constructor(props) {
        super(props)
    }

    renderItem = ({ item }) => {
        return (
            <StockListingCard
                item={item}
                isShowFavourites={true}
                onItemMenuClick={this.props.onItemMenuClick}
                onListItemClick={this.props.onListItemClick}
                clickOnMore={this.clickOnMore}
                clickOnEdit={this.clickOnEdit}
                clickOnPostFacebook={this.clickOnPostFacebook}
                clickOnRemoveStock={this.clickOnRemoveStock}
                clickOnWhatsapp={this.clickOnWhatsapp}
                activeStockShow={this.props.activeStockShow}
                clickOnAddToStock={this.clickOnAddToStock}
                favouriteItems={this.props.favouriteItems}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    style={{ flex: 1 }}
                    extraData={this.props}
                    data={this.props.stockList}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.id.toString()}
                    refreshing={this.props.refreshing}
                    onRefresh={this.props.onRefresh}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.props.handleLoadMore}
                    contentContainerStyle={{ padding: Dimens.padding_small }}
                />

                {this.props.favouriteItems.length > 0 &&
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={{ flex: 0.4, alignItems: 'center', justifyContent: 'center' }}
                            activeOpacity={0.8}
                            onPress={() => this.props.onReset()}>

                            <Text style={[styles.buttonText, { color: Colors.PRIMARY }]}>{Strings.RESET}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ flex: 0.6, backgroundColor: Colors.PRIMARY, alignItems: 'center', justifyContent: 'center' }}
                            activeOpacity={0.8}
                            onPress={() => this.props.onLeadAssign()}>

                            <Text style={styles.buttonText}>{Strings.ASSIGNE_LEAD.toUpperCase()}</Text>
                            {/* ASSIGN TO LEAD */}
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    buttonText: {
        color: Colors.WHITE,
        fontWeight: '600',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    buttonContainer: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.WHITE,
    }
})