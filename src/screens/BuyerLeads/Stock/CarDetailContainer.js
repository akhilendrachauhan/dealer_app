import React, { Component } from 'react'
import { View, StyleSheet, SafeAreaView } from 'react-native'
import CarDetailScreen from './CarDetailScreen'
import { Utility, Constants } from '../../../util'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import AsyncStore from '../../../util/AsyncStore'
import * as APICalls from '../../../api/APICalls'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from '../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import ActionBarWrapper from "../../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../../util/Constants'


export default class CarDetailContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            screenState: ScreenStates.NO_ERROR,
            stockData: this.props.navigation.state.params.stockData
        }
    }

    componentDidMount() {
    }

    onRetryClick = () => {
    }

    updateFavourite = async (isFavourite, carId) => {

        // 1 for add Favourite and 0 for remove from Favourite

        let leadId = await this.props.navigation.state.params.leadData.lead_id
        let carIdArr = await [carId]

        var storeObject = new AsyncStore()
        storeObject.getAsyncValueInPersistStore(Constants.DEALER_ID).then((dealer_id) => {

            try {
                APICalls.updateFavourite(leadId, isFavourite, carIdArr, this.onSuccessUpdateFav, this.onFailureUpdateFav, this.props)
            } catch (error) {
                Utility.log(error)
            }

            this.setState({ isLoading: true })
            // this.setState({ screenState: ScreenStates.IS_LOADING })

        }).catch((error) => {
            Utility.log("error===> ", error)
        })
    }

    onSuccessUpdateFav = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.props.navigation.state.params.callBack()
        this.setState({ screenState: ScreenStates.NO_ERROR, isLoading: false })
    }

    onFailureUpdateFav = (response) => {

        if (response && response.message) {
            Utility.showToast(response.message)
        }
        else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
        }
        this.setState({ screenState: ScreenStates.NO_ERROR, isLoading: false })
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: this.props.navigation.state.params.leadData.customer_name }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <CarDetailScreen
                            isLoading={this.state.isLoading}
                            loadingMsg={this.state.loadingMsg}
                            stockData={this.state.stockData}
                            updateFavourite={this.updateFavourite}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}
const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})