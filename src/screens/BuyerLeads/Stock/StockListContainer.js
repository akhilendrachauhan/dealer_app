import React, { Component } from 'react'
import { View, StyleSheet, SafeAreaView } from 'react-native'
import StockListScreen from './StockListScreen'
import { Utility, Constants,AnalyticsConstants } from '../../../util'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import AsyncStore from '../../../util/AsyncStore'
import * as APICalls from '../../../api/APICalls'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from '../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import ActionBarWrapper from "../../../granulars/ActionBarWrapper"
import { ScreenStates, ScreenName } from '../../../util/Constants'
import ActionBarSearchComponent from "../../../granulars/ActionBarSearchComponent"
import { MenuProvider } from 'react-native-popup-menu'


export default class StockListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchBar: false,
            stockList: [],
            favouriteItems: [],
            refreshing: false,
            activeStockShow: true,
            showMenu: false,
            page: 1,
            hasNext: false,
            isApiCalling: false,
            stockType: Constants.ACTIVE_STOCK,
            screenState: ScreenStates.NO_ERROR
        }
    }

    componentDidMount() {
        this.getStockListingData('', false)
    }

    handleLoadMore = () => {
        if (this.state.hasNext && !this.state.isApiCalling) {
            let nextPage = this.state.page + 1
            this.setState({ page: nextPage }, () => {
                this.getStockListingData('', true, true)
            })
        }
    }

    onRefresh = () => {
        this.setState({ page: 1, showSearchBar: false }, () => {
            this.getStockListingData('', true)
        })
    }

    onRetryClick = () => {
        this.setState({ page: 1, showSearchBar: false }, () => {
            this.getStockListingData('', false)
        })
    }

    onSubmitEditing = (text) => {
        this.setState({ page: 1 }, () => {
            this.getStockListingData(text, false)
        })
    }

    onSearchTextChange = (text) => {
        // this.getStockListingData(text, false)
    }

    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
        this.getStockListingData('', false)
    }

    onSearch = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_ASSIGN_SEARCH_CLICK)
        this.setState({ showSearchBar: true })
    }

    getStockListingData = (text, fromRefresh, dataReset) => {

        var favCarIds = this.props.navigation.state.params.favCarIds || []

        this.setState({ isApiCalling: true })

        var storeObject = new AsyncStore()
        storeObject.getAsyncValueInPersistStore(Constants.DEALER_ID).then((dealer_id) => {
            try {
                APICalls.getStockListing(dealer_id, text, this.state.stockType, this.state.page, '', favCarIds, this.onSuccessStockListing, this.onFailureStockListing, this.props);
            } catch (error) {
                Utility.log(error);
            }

            if (!dataReset)
                this.setState({ stockList: [] })

            if (fromRefresh) {
                this.setState({ refreshing: true })
            }
            else {
                this.setState({ screenState: ScreenStates.IS_LOADING })
            }

        }).catch((error) => {
            Utility.log("error===> ", error)
        })
    }

    onSuccessStockListing = (response) => {
        if (response && response.data && response.data.length > 0)
            this.setState({
                refreshing: false,
                isApiCalling: false,
                hasNext: response.pagination.next_page,
                screenState: ScreenStates.NO_ERROR,
                // stockList: response.data,
                stockList: [...this.state.stockList, ...response.data],
            })
        else
            this.setState({
                hasNext: false,
                refreshing: false,
                isApiCalling: false,
                screenState: ScreenStates.NO_DATA_FOUND
            })
    }

    onFailureStockListing = (response) => {
        this.setState({
            hasNext: false,
            refreshing: false,
            isApiCalling: false,
            screenState: ScreenStates.SERVER_ERROR
        })

        if (response && response.message) {
            Utility.showToast(response.message)
        }
        else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
        }
    }

    onListItemClick = (item) => {
        let itemIds = this.state.favouriteItems

        if (this.state.favouriteItems.includes(item.id)) {
            let index = this.state.favouriteItems.indexOf(item.id)
            itemIds.splice(index, 1)
        }
        else {
            itemIds.push(item.id)
        }

        this.setState({ favouriteItems: itemIds })
    }

    onReset = () => {
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_ASSIGN_RESET_CLICK)
        this.setState({ favouriteItems: [] })
    }

    onLeadAssign = async () => {

        let leadId = await this.props.navigation.state.params.leadId
        let isFavourite = await 1 // 1 for add Favourite and 0 for remove from Favourite

        var storeObject = new AsyncStore()
        storeObject.getAsyncValueInPersistStore(Constants.DEALER_ID).then((dealer_id) => {
            Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_ASSIGN_SUBMIT)
            try {
                APICalls.updateFavourite(leadId, isFavourite, this.state.favouriteItems, this.onSuccessUpdateFav, this.onFailureUpdateFav, this.props)
            } catch (error) {
                Utility.log(error)
            }

            this.setState({ screenState: ScreenStates.IS_LOADING })

        }).catch((error) => {
            Utility.log("error===> ", error)
        })
    }

    onSuccessUpdateFav = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_ASSIGN_SUCCESS)
        this.setState({ screenState: ScreenStates.NO_ERROR }, () => {
            this.props.navigation.state.params.callBack()
            this.onBackPress()
        })
    }

    onFailureUpdateFav = (response) => {
        this.setState({ screenState: ScreenStates.NO_ERROR })
        Utility.sendEvent(AnalyticsConstants.LEAD_STOCK_ASSIGN_FAILURE)
        if (response && response.message) {
            Utility.showToast(response.message)
        }
        else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)
        }
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_STOCK_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        var title = this.state.favouriteItems.length > 0 ?
            this.state.favouriteItems.length + ' ' + Strings.SELECTED :
            Strings.ASSIGNE_LEAD + ' (' + this.state.stockList.length + ')'
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={{ title: title }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: ImageAssets.icon_search,
                            onPress: () => this.onSearch()
                        }]}
                    />

                    {this.state.showSearchBar ? this.openActionSearchBar() : null}

                    <MenuProvider>
                        <WithLoading
                            screenState={this.state.screenState}
                            screenName={ScreenName.SCREEN_LEAD}
                            onRetry={this.onRetryClick}>

                            <StockListScreen
                                stockList={this.state.stockList}
                                onListItemClick={this.onListItemClick}
                                onItemMenuClick={this.onItemMenuClick}
                                activeStockShow={this.state.activeStockShow}
                                favouriteItems={this.state.favouriteItems}
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                onReset={this.onReset}
                                onLeadAssign={this.onLeadAssign}
                                handleLoadMore={this.handleLoadMore}
                            />
                        </WithLoading>
                    </MenuProvider>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})