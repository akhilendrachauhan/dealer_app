import React, { Component } from 'react'
import { View, StyleSheet, FlatList, Dimensions, Text, TouchableOpacity } from 'react-native'
import { Utility, Constants } from '../../../util'
import { Strings, Colors, Dimens } from '../../../values'
import * as ImageAssets from '../../../assets/ImageAssets'
import LeadDetails from '../../../granulars/BuyerLeads/LeadDetails'
import LeadHistory from '../../../granulars/BuyerLeads/LeadHistory'
import StockOptionCard from '../../../granulars/StockOptionCard'
import LoadingComponent from '../../../components/LoadingComponent'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import CarInfo from '../../../granulars/Stocks/CarInfo'
import CarFeatures from '../../../granulars/Stocks/CarFeatures'


export default class CarDetailScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
            index: 0,
            routes: [
                { key: 'overview', title: 'OVERVIEW' },
                { key: 'features', title: 'FEATURES' }
            ],
            selectedIndex: 0
        }
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE, borderRadius: 5 }}>
                <StockOptionCard
                    item={item}
                    isCarDetail={true}
                    updateFavourite={this.props.updateFavourite}
                    onStockItemPress={() => { }}
                />

                <View style={{ flex: 1, width: Dimensions.get('window').width - 40 }}>
                    <TabView
                        navigationState={this.state}
                        renderScene={({ route, jumpTo }) => {
                            switch (route.key) {
                                case 'overview':
                                    return (
                                        <CarInfo
                                            jumpTo={jumpTo}
                                            carInfo={item.app_data}
                                        />
                                    )
                                case 'features':
                                    return (
                                        <CarFeatures
                                            jumpTo={jumpTo}
                                            carInfo={item.app_data}
                                        />
                                    )
                            }
                        }}
                        swipeEnabled={false}
                        renderTabBar={props =>
                            <TabBar
                                {...props}
                                style={Styles.containerTab}
                                pressColor={Colors.BLACK_25}
                                activeColor={Colors.BLACK}
                                inactiveColor={Colors.BLACK_54}
                                indicatorStyle={{ backgroundColor: Colors.DASHBOARD_CARD_TEXT }}
                            />
                        }
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width - 40 }}
                    />
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <FlatList
                    horizontal={true}
                    extraData={this.props}
                    data={this.props.stockData}
                    renderItem={this.renderItem}
                    ref={(ref) => { this.flatListRef = ref }}
                    keyExtractor={item => item.id.toString()}
                    showsHorizontalScrollIndicator={false}
                    ItemSeparatorComponent={() => <View style={{ width: 8 }} />}
                    onViewableItemsChanged={this.onViewableItemsChanged}
                    viewabilityConfig={{ itemVisiblePercentThreshold: 50 }}
                    contentContainerStyle={{ padding: Dimens.padding_xx_large }}
                />

                {this.props.stockData.length > 0 &&
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', paddingVertical: 15 }}>
                        {this.props.stockData.map((item, index) => {
                            return (
                                <View key={index.toString()} style={[Styles.indicatorView, {
                                    backgroundColor: index == this.state.selectedIndex ? Colors.WHITE : Colors.TRANSPARENT
                                }]} />
                            )
                        })}
                    </View>
                }

                {this.props.isLoading &&
                    <LoadingComponent msg={this.props.loadingMsg} />
                }
            </View>
        )
    }

    onViewableItemsChanged = ({ viewableItems, changed }) => {
        // alert(JSON.stringify(viewableItems[0]))
        this.setState({ selectedIndex: viewableItems[0].index })
    }
}

let Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    containerTab: {
        elevation: 0,
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: Colors.BLACK_25,
        backgroundColor: Colors.BLACK_12
    },
    indicatorView: {
        width: 8, height: 8,
        marginRight: Dimens.margin_x_small,
        borderRadius: 4, borderWidth: 0.9,
        borderColor: Colors.WHITE_54,
    },
    divider: {
        height: 0.9, width: '100%',
        backgroundColor: Colors.DIVIDER,
        // marginVertical: Dimens.padding_large
    }
})