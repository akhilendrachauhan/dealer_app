import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput } from 'react-native'
import { Strings, Colors, Dimens } from '../../../values'
import TextInputLayout from "../../../granulars/TextInputLayout"
import LoadingComponent from '../../../components/LoadingComponent'
import { Utility, Constants } from '../../../util'
import { Dropdown } from 'react-native-material-dropdown'

export default class EditLeadScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            fullName: '',
            email: '',
            mobile: '',
            altMobile: '',
            locality: '',
            localityId: '',
            countryCode: Constants.DEFAULT_COUNTRY_CODE
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={Styles.containerInner}>
                        <TextInputLayout
                            ref={(input) => this.TL_FullName = input}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_xxx_large}>

                            <TextInput
                                style={Styles.textInput}
                                returnKeyType='next'
                                selectTextOnFocus={false}
                                value={this.state.fullName}
                                placeholder={Strings.FULL_NAME}
                                ref={(input) => this.FullNameInput = input}
                                onSubmitEditing={() => this.EmailInput.focus()}
                                onChangeText={(text) => this.setState({ fullName: text.replace(/[`~0-9!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '') })} />
                        </TextInputLayout>

                        <TextInputLayout
                            style={Styles.containerTextInput}
                            ref={(input) => this.TL_Email = input}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_xxx_large}>

                            <TextInput
                                style={Styles.textInput}
                                returnKeyType='next'
                                keyboardType={'email-address'}
                                selectTextOnFocus={false}
                                value={this.state.email}
                                placeholder={Strings.EMAIL}
                                ref={(input) => this.EmailInput = input}
                                onSubmitEditing={() => this.MobileInput.focus()}
                                onChangeText={(text) => this.setState({ email: text })} />
                        </TextInputLayout>

                        <TextInputLayout
                            style={Styles.containerTextInput}
                            ref={(input) => this.TL_Mobile = input}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_xxx_large}>

                            <TextInput
                                style={Styles.textInput}
                                returnKeyType='next'
                                keyboardType={'numeric'}
                                editable={false}
                                selectTextOnFocus={false}
                                value={this.state.mobile}
                                placeholder={Strings.MOBILE}
                                ref={(input) => this.MobileInput = input}
                                // maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                onSubmitEditing={() => this.AlternateMobileInput.focus()}
                                onChangeText={(text) => this.setState({ mobile: text })} />
                        </TextInputLayout>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Dropdown
                                containerStyle={{ flex: 0.4 }}
                                dropdownOffset={{ top: 38, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.countryCodeData}
                                absoluteRTLLayout={true}
                                value={this.state.countryCode}
                                onChangeText={(value, index, data) => {
                                    this.setState({ countryCode: value, selectedItem: data[index] })
                                }}
                            />

                            <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                <TextInputLayout
                                    // style={Styles.containerTextInput}
                                    ref={(input) => this.TL_AlternateMobile = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_xxx_large}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        keyboardType={'numeric'}
                                        selectTextOnFocus={false}
                                        value={this.state.altMobile}
                                        placeholder={Strings.ALTERNATE_MOBILE}
                                        maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                        ref={(input) => this.AlternateMobileInput = input}
                                        onSubmitEditing={() => this.LocalityInput.focus()}
                                        onChangeText={(text) => this.setState({ altMobile: text })} />
                                </TextInputLayout>
                            </View>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onLocalityPress()}>
                            <TextInputLayout
                                style={Styles.containerTextInput}
                                ref={(input) => this.TL_Locality = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_xxx_large}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.state.locality}
                                    placeholder={Strings.LOCALITY}
                                    ref={(input) => this.LocalityInput = input}
                                    onSubmitEditing={() => { }}
                                    onChangeText={(text) => this.setState({ locality: text })}
                                    onTouchStart={() => Platform.OS === 'ios' ? this.props.onLocalityPress() : Utility.log('Pressed...')}
                                />
                            </TextInputLayout>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

                <TouchableOpacity
                    style={Styles.containerButton}
                    activeOpacity={0.8}
                    onPress={() => this.props.onUpdateClick(this.state.fullName, this.state.email, this.state.mobile, this.state.countryCode, this.state.altMobile, this.state.locality, this.state.localityId)}>

                    <Text style={Styles.buttonText}>{Strings.UPDATE}</Text>
                </TouchableOpacity>

                {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    containerInner: {
        padding: Dimens.padding_xx_large,
    },
    containerTextInput: {
        marginTop: Dimens.margin_xx_large
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    containerButton: {
        backgroundColor: Colors.PRIMARY,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 14, margin: 10, borderRadius: 5
    },
    buttonText: {
        color: Colors.WHITE,
        fontWeight: '500',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    }
})
