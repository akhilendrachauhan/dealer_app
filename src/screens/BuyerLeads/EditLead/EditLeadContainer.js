import React, { Component } from 'react'
import EditLeadScreen from './EditLeadScreen'
import { View, SafeAreaView, StyleSheet, Keyboard } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from '../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import ActionBarWrapper from '../../../granulars/ActionBarWrapper'
import { ScreenStates, ScreenName } from '../../../util/Constants'
import * as APICalls from '../../../api/APICalls'
import AsyncStore from '../../../util/AsyncStore'
import DBFunctions from "../../../database/DBFunctions"
import * as DBConstants from '../../../database/DBConstants'
export default class EditLeadContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            countryCodeData: [],
            screenState: ScreenStates.NO_ERROR,
        }
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.EDIT_BUYER_LEAD_SCREEN)
        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeData: countryCodeData })

        let altMobile = this.props.navigation.state.params.data.customer_alt_mobile
        let countryCode = altMobile.substring(0, 3)
        let altMobileNo = Utility.replaceRange(altMobile, 0, 3, '')
        // Utility.log('countryCode===>', countryCode)
        // Utility.log('altMobileNo===>', altMobileNo)

        this.refs['editLeadScreen'].setState({
            fullName: this.props.navigation.state.params.data.customer_name,
            email: this.props.navigation.state.params.data.customer_email,
            mobile: this.props.navigation.state.params.data.mobile,
            altMobile: altMobileNo == '' ? '' : altMobileNo,
            countryCode: countryCode == '' ? Constants.DEFAULT_COUNTRY_CODE : countryCode
        })

        let dbFunctions = new DBFunctions()
        dbFunctions.getLocalityById(this.props.navigation.state.params.data.location_id).then((result) => {
            let location = result && result[0] && result[0][DBConstants.LOCALITY] ? result[0][DBConstants.LOCALITY] : ''
            this.refs['editLeadScreen'].setState({ locality: location, localityId: this.props.navigation.state.params.data.location_id })
        })
    }

    onUpdateClick = async (fullName, email, mobileNumber, countryCode, altMobile, locality, localityId) => {

        var leadId = await this.props.navigation.state.params.data.lead_id

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        if (Utility.isValueNullOrEmpty(fullName)) {
            this.refs['editLeadScreen'].TL_FullName.setError(Strings.ENTER_FULL_NAME)
            this.refs['editLeadScreen'].FullNameInput.focus()
            return
        } else if (fullName.length < 3) {
            this.refs['editLeadScreen'].TL_FullName.setError(Strings.MIN_3_CHAR)
            this.refs['editLeadScreen'].FullNameInput.focus()
            return
        } else if (!Utility.isValueNullOrEmpty(email) && !Utility.validateEmail(email)) {
            this.refs['editLeadScreen'].TL_Email.setError(Strings.ENTER_EMAIL)
            this.refs['editLeadScreen'].EmailInput.focus()
            return
        } else if (!Utility.isValueNullOrEmpty(altMobile) && !Utility.validateMobileNo(altMobile, countryCode, this.state.countryCodeData)) {
            this.refs['editLeadScreen'].TL_AlternateMobile.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['editLeadScreen'].AlternateMobileInput.focus()
            return
        }
        else {
            Keyboard.dismiss()

            let altMobileNo = await altMobile != '' ? countryCode + altMobile : ''
            Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_EDIT_SUBMIT,{lead_id: leadId})
            try {
                APICalls.updateLeadCustomer(values[0][1], leadId, fullName, email, altMobileNo, localityId, this.onSuccess, this.onFailure, this.props)
            } catch (error) {
                Utility.log(error)
            }
            // this.setState({ screenState: ScreenStates.IS_LOADING })
            this.setState({ isLoading: true })
        }
    }

    onSuccess = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_EDIT_SUCCESS)
        Utility.log('onSuccessUpdateLead===> ', JSON.stringify(response))
        this.setState({ isLoading: false }, () => {
            this.props.navigation.state.params.callBack()
            this.onBackPress()
        })
    }

    onFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_EDIT_FAILURE)
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        Utility.log('onFailureUpdateLead===> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    onRetryClick = () => {

    }

    onLocalityPress = () => {
        let dbFunctions = new DBFunctions()
        this.props.navigation.navigate('searchContainer', {
            dbQuery: dbFunctions.getLocalityQueryText(),
            hintText: 'Search by Locality',
            callback: this.onLocalitySelected
        })
    }

    onLocalitySelected = (result) => {
        Utility.log('SelectedLocality', result)
        this.refs['editLeadScreen'].setState({
            locality: result[DBConstants.LOCALITY],
            localityId: result[DBConstants.LOCALITY_ID]
        })
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.EDIT_LEAD }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                    // onRetry={this.onRetryClick}
                    >

                        <EditLeadScreen
                            ref={'editLeadScreen'}
                            isLoading={this.state.isLoading}
                            loadingMsg={this.state.loadingMsg}
                            onUpdateClick={this.onUpdateClick}
                            onLocalityPress={this.onLocalityPress}
                            countryCodeData={this.state.countryCodeData}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})