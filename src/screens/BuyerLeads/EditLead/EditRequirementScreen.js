import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Slider, TextInput, Dimensions } from 'react-native'
import { Utility, Constants } from '../../../util'
import { Strings, Colors, Dimens } from '../../../values'
import { Dropdown } from 'react-native-material-dropdown'
import MultiSlider from '../../../granulars/MultiSlider/MultiSlider'
import TextInputLayout from "../../../granulars/TextInputLayout"
import LoadingComponent from '../../../components/LoadingComponent'
const { width: widthScreen, height: heightScreen } = Dimensions.get('window')

export default class EditRequirementScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            budgetDP: '',
            budgetIdDP: 0,
            budgetOTR: '',
            budgetIdOTR: 0,
            defaultDPBudget: 0,
            defaultOTRBudget: 0,
            fuelType: '',
            transmission: '',
            makeModel: '',
            bodyType: '',
            budgetValue: '',
            fuelTypeId: 0,
            bodyTypeId: 0,
            transmissionId: 0,
            modelId: 0,
            isShowDPBudget: false,
            isShowOTRBudget: false,
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={Styles.containerInner}>
                        {this.state.isShowDPBudget && Constants.APP_TYPE == Constants.INDONESIA ?
                            <View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={Styles.headText}>{Strings.DP_BUDGET}</Text>
                                    <Text style={Styles.headText}>{this.state.budgetDP}</Text>
                                </View>

                                <MultiSlider
                                    markerStyle={{ backgroundColor: Colors.PRIMARY }}
                                    containerStyle={{ alignItems: 'center' }}
                                    sliderLength={widthScreen - 60}
                                    isMarkersSeparated={true}
                                    optionsArray={this.props.dpBudgetArray}
                                    values={[this.state.defaultDPBudget]}
                                    onValuesChangeFinish={(value) => {
                                        // Utility.log('finishValue==>', value)
                                        var index = parseInt(value)
                                        this.setState({
                                            defaultDPBudget: index,
                                            budgetDP: this.props.dpBudgetData[index].value,
                                            budgetIdDP: this.props.dpBudgetData[index].key
                                        })
                                    }}
                                />
                            </View>
                            :
                            null
                        }

                        {this.state.isShowOTRBudget &&
                            <View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={Styles.headText}>{Strings.OTR_BUDGET}</Text>
                                    <Text style={Styles.headText}>{this.state.budgetOTR}</Text>
                                </View>

                                <MultiSlider
                                    markerStyle={{ backgroundColor: Colors.PRIMARY }}
                                    containerStyle={{ alignItems: 'center' }}
                                    sliderLength={widthScreen - 60}
                                    isMarkersSeparated={true}
                                    optionsArray={this.props.otrBudgetArray}
                                    values={[this.state.defaultOTRBudget]}
                                    onValuesChangeFinish={(value) => {
                                        // Utility.log('finishValue==>', value)
                                        var index = parseInt(value)
                                        this.setState({
                                            defaultOTRBudget: index,
                                            budgetOTR: this.props.otrBudgetData[index].value,
                                            budgetIdOTR: this.props.otrBudgetData[index].key
                                        })
                                    }}
                                />
                            </View>
                        }

                        <View style={{ marginTop: Dimens.margin_xx_large }}>
                            <Text style={Styles.smallText}>{Strings.FULE_TYPE}</Text>

                            <Dropdown
                                containerStyle={{ flex: 1 }}
                                dropdownOffset={{ top: 12, left: 0 }}
                                baseColor={Colors.GRAY}
                                label={this.state.fuelType != '' ? '' : Strings.SELECT_FULE_TYPE}
                                data={this.props.fuleTypeList}
                                absoluteRTLLayout={true}
                                value={this.state.fuelType}
                                onChangeText={(value, index, data) => {
                                    this.setState({ fuelType: value, fuelTypeId: data[index].key, selectedItem: data[index] })
                                }}
                            />
                        </View>

                        <View style={{ marginTop: Dimens.margin_xx_large }}>
                            <Text style={Styles.smallText}>{Strings.TRANSMISSION}</Text>

                            <Dropdown
                                containerStyle={{ flex: 1 }}
                                dropdownOffset={{ top: 12, left: 0 }}
                                baseColor={Colors.GRAY}
                                label={this.state.transmission != '' ? '' : Strings.SELECT_TRANSMISSION}
                                data={this.props.transmissionList}
                                absoluteRTLLayout={true}
                                value={this.state.transmission}
                                onChangeText={(value, index, data) => {
                                    this.setState({ transmission: value, transmissionId: data[index].key, selectedItem: data[index] })
                                }}
                            />
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onMakeModelPress()}>
                            <TextInputLayout
                                ref={(input) => this.TL_MAKE_MODEL = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.ORANGE}
                                errorColorMargin={Dimens.margin_xxx_large}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.state.makeModel}
                                    placeholder={Strings.MAKE__MODEL}
                                    ref={(input) => this.makeModelInput = input}
                                    onTouchStart={() => Platform.OS === 'ios' ? this.props.onMakeModelPress() : Utility.log("Pressed...")}
                                />
                            </TextInputLayout>
                        </TouchableOpacity>

                        <View style={{ marginTop: Dimens.margin_xx_large }}>
                            <Text style={Styles.smallText}>{Strings.BODY_TYPE}</Text>

                            <Dropdown
                                containerStyle={{ flex: 1 }}
                                dropdownOffset={{ top: 12, left: 0 }}
                                baseColor={Colors.GRAY}
                                label={this.state.bodyType != '' ? '' : Strings.SELECT_BODY_TYPE}
                                data={this.props.bodyTypeList}
                                absoluteRTLLayout={true}
                                value={this.state.bodyType}
                                onChangeText={(value, index, data) => {
                                    this.setState({ bodyType: value, bodyTypeId: data[index].key, selectedItem: data[index] })
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>

                <TouchableOpacity
                    style={Styles.containerButton}
                    activeOpacity={0.8}
                    onPress={() => this.props.onSaveClick(this.state.budgetIdDP, this.state.budgetIdOTR, this.state.fuelTypeId, this.state.transmissionId, this.state.modelId, this.state.bodyTypeId)}>

                    <Text style={Styles.buttonText}>{Strings.SAVE}</Text>
                </TouchableOpacity>

                {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    containerInner: {
        padding: Dimens.padding_xx_large,
    },
    containerTextInput: {
        marginTop: Dimens.margin_xx_large
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    containerButton: {
        backgroundColor: Colors.PRIMARY,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 14, margin: 10, borderRadius: 5
    },
    buttonText: {
        color: Colors.WHITE,
        fontWeight: '500',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    headText: {
        fontSize: Dimens.text_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
})
