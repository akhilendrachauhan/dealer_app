import React, { Component } from 'react'
import EditRequirementScreen from './EditRequirementScreen'
import { View, SafeAreaView, StyleSheet, Keyboard } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../../util'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from '../../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../../values/Styles'
import * as ImageAssets from '../../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../../values'
import ActionBarWrapper from '../../../granulars/ActionBarWrapper'
import { ScreenStates, ScreenName } from '../../../util/Constants'
import * as APICalls from '../../../api/APICalls'
import AsyncStore from '../../../util/AsyncStore'
import DBFunctions from "../../../database/DBFunctions"
import * as DBConstants from '../../../database/DBConstants'

export default class EditRequirementContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            screenState: ScreenStates.IS_LOADING,
            fuleTypeList: [],
            bodyTypeList: [],
            transmissionList: [],
            dpBudgetData: [],
            otrBudgetData: [],
            dpBudgetArray: [],
            otrBudgetArray: [],
        }
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.CUSTOMER_REQUIREMENT_SCREEN)
        let dpBudgetArray = []
        let otrBudgetArray = []
        let dpBudgetData = await Utility.getConfigArrayData(Constants.DP_BUDGET_VALUES)
        let otrBudgetData = await Utility.getConfigArrayData(Constants.OTR_BUDGET_VALUES)
        let fuleTypeData = await Utility.getConfigArrayData(Constants.FUEL_TYPE_VALUES)
        let bodyTypeData = await Utility.getConfigArrayData(Constants.BODY_TYPE_VALUES)
        let TransmissionData = await Utility.getConfigArrayData(Constants.TRANSMISSION_TYPE_VALUES)

        for (let i = 0; i < dpBudgetData.length; i++) {
            dpBudgetArray.push(i)
        }
        for (let j = 0; j < otrBudgetData.length; j++) {
            otrBudgetArray.push(j)
        }
        this.setState({
            dpBudgetData: dpBudgetData, dpBudgetArray: dpBudgetArray,
            otrBudgetData: otrBudgetData, otrBudgetArray: otrBudgetArray,
            fuleTypeList: fuleTypeData,
            bodyTypeList: bodyTypeData,
            transmissionList: TransmissionData,
        })

        this.getRequirement()
    }

    onRetryClick = () => {
        this.getRequirement()
    }

    getRequirement = async () => {

        var leadId = await this.props.navigation.state.params.data.lead_id

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        try {
            APICalls.getLeadRequirement(values[0][1], leadId, this.onSuccessRequirement, this.onFailureRequirement, this.props)
        } catch (error) {
            Utility.log(error)
        }
        this.setState({ screenState: ScreenStates.IS_LOADING })
    }

    onSuccessRequirement = async (response) => {
        if (response && response.data)
            this.setState({ screenState: ScreenStates.NO_ERROR })
        else
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND })

        Utility.log('onSuccessUpdateLead===> ', JSON.stringify(response))

        var i = 0
        for (var index in this.state.fuleTypeList) {
            if (this.state.fuleTypeList[i].key == response.data.prefData.fuel_type) {
                this.refs['editRequirementScreen'].setState({
                    fuelType: this.state.fuleTypeList[i].value,
                    fuelTypeId: response.data.prefData.fuel_type
                })
            }
            i++
        }

        var j = 0
        for (var index in this.state.bodyTypeList) {
            if (this.state.bodyTypeList[j].key == response.data.prefData.body_type) {
                this.refs['editRequirementScreen'].setState({
                    bodyType: this.state.bodyTypeList[j].value,
                    bodyTypeId: response.data.prefData.body_type
                })
            }
            j++
        }

        var k = 0
        for (var index in this.state.transmissionList) {
            if (this.state.transmissionList[k].key == response.data.prefData.transmission_type) {
                this.refs['editRequirementScreen'].setState({
                    transmission: this.state.transmissionList[k].value,
                    transmissionId: response.data.prefData.transmission_type
                })
            }
            k++
        }

        for (let l = 0; l < this.state.dpBudgetData.length; l++) {
            if (this.state.dpBudgetData[l].key == response.data.prefData.dp_budget) {
                this.refs['editRequirementScreen'].setState({
                    defaultDPBudget: l,
                    budgetDP: this.state.dpBudgetData[l].value,
                    budgetIdDP: response.data.prefData.dp_budget,
                })
            }
        }
        for (let m = 0; m < this.state.otrBudgetData.length; m++) {
            if (this.state.otrBudgetData[m].key == response.data.prefData.price_from) {
                this.refs['editRequirementScreen'].setState({
                    defaultOTRBudget: m,
                    budgetOTR: this.state.otrBudgetData[m].value,
                    budgetIdOTR: response.data.prefData.price_from,
                })
            }
        }
        this.refs['editRequirementScreen'].setState({
            isShowDPBudget: this.state.dpBudgetArray.length > 0 ? true : false,
            isShowOTRBudget: this.state.otrBudgetArray.length > 0 ? true : false,
        })

        let dbFunctions = new DBFunctions()
        dbFunctions.getMMVByParentModelId(response.data.prefData.model).then((result) => {
            // Utility.log('result===>', result)
            if (result && result.length > 0) {
                this.refs['editRequirementScreen'].setState({
                    makeModel: result[0][DBConstants.MAKE_MODEL],
                    modelId: response.data.prefData.model
                })
            }
        })
    }

    onFailureRequirement = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        Utility.log('onFailureUpdateLead===> ', JSON.stringify(response))
        this.setState({ screenState: ScreenStates.NO_DATA_FOUND })
    }

    onSaveClick = async (budgetDP, budgetOTR, fuelType, transmission, makeModel, bodyType) => {

        // Utility.log('data==>', budgetDP, budgetOTR, fuelType, transmission, makeModel, bodyType)

        var leadId = await this.props.navigation.state.params.data.lead_id

        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])

        if (Constants.APP_TYPE == Constants.INDONESIA && parseInt(budgetDP) != 0 && parseInt(budgetDP) >= parseInt(budgetOTR)) {
            Utility.showToast(Strings.BUDGET_VALIDATION)
            return
        }
        try {
            Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CUST_EDIT_SUBMIT)
            APICalls.updateLeadRequirement(values[0][1], leadId, budgetDP, budgetOTR, fuelType, transmission, makeModel, bodyType, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
        this.setState({ isLoading: true })
    }

    onSuccess = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CUST_EDIT_SUCCESS)
        this.setState({ isLoading: false }, () => {
            this.props.navigation.state.params.callBack()
            this.onBackPress()
        })
        Utility.log('onSuccessUpdateLead===> ', JSON.stringify(response))
    }

    onFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.LEAD_DETAIL_CUST_EDIT_FAILURE)
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        this.setState({ isLoading: false })
        Utility.log('onFailureUpdateLead===> ', JSON.stringify(response))
    }

    onMakeModelPress = () => {
        let dbFunctions = new DBFunctions()
        this.props.navigation.navigate('searchContainer', {
            dbQuery: dbFunctions.getDistinctMakeParentModelsQueryText(),
            hintText: Strings.SEARCH_BY_MAKE_MODEL,
            callback: this.onMakeModelSelected
        })
    }

    onMakeModelSelected = (mmv) => {
        Utility.log('mmv===>', mmv)

        this.refs['editRequirementScreen'].setState({
            makeModel: mmv[DBConstants.MAKE_PARENT_MODEL],
            modelId: mmv[DBConstants.MODEL_PARENT_ID]
        })
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.EDIT_CUSTOMER_REQUIREMENTS }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}
                    >

                        <EditRequirementScreen
                            ref={'editRequirementScreen'}
                            onSaveClick={this.onSaveClick}
                            isLoading={this.state.isLoading}
                            loadingMsg={this.state.loadingMsg}
                            onMakeModelPress={this.onMakeModelPress}
                            fuleTypeList={this.state.fuleTypeList}
                            bodyTypeList={this.state.bodyTypeList}
                            transmissionList={this.state.transmissionList}
                            dpBudgetData={this.state.dpBudgetData}
                            dpBudgetArray={this.state.dpBudgetArray}
                            otrBudgetData={this.state.otrBudgetData}
                            otrBudgetArray={this.state.otrBudgetArray}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})