import React, { Component } from 'react';
import SplashScreen from './SplashScreen'
import { Platform, Alert, Linking, AppState, BackHandler } from 'react-native'
import Config from 'react-native-config'
import AsyncStore from '../../util/AsyncStore'
import * as APICalls from '../../api/APICalls'
import { Constants, Utility, AnalyticsConstants } from "../../util";
import DBFunctions from "../../database/DBFunctions";
import firebase from 'react-native-firebase';
import { registerAppListener, unRegisterNotificationListener } from "../../util/FirebaseListeners";
import DeviceInfo from 'react-native-device-info'
import AsyncStorage from '@react-native-community/async-storage'
import { Strings } from '../../values'
import { StackActions, NavigationActions } from 'react-navigation';
let dbFunctions;

export default class SplashContainer extends Component {

    constructor(props) {
        super(props);
        dbFunctions = new DBFunctions()
        this.state = {
            loading: true,
            appState: AppState.currentState,
            apiCalling: 1,
        }
        
    }

    componentDidMount() {
        this.checkAppVersionForceUpdate()
    }

    checkAppVersionForceUpdate = () => {
        try {
            APICalls.getForceUpdate((response) => {
                Utility.log("checkAppVersionUpdate==> ", response)
                if (response.data && response.data.status === 'T') {

                    this.showAlertForceUpdate(response);
                } else {
                    this.initSplash()
                }
            }, (response) => {
                this.initSplash()
            })
        } catch (error) {
            Utility.log("==================> " + error)
            this.initSplash()
        }
    }

    showAlertForceUpdate = (response) => {
        try {
            let title = response.data.heading
            let forceUpgradeStatus = response.data.force_upgrade
            let actions = []
            var appLink = Constants.APP_TYPE == Constants.INDONESIA ? Constants.APP_LINK : Constants.PH_APP_LINK

            if (forceUpgradeStatus == "soft") {
                actions.push({ text: Strings.CANCEL, onPress: () => this.initSplash(), style: 'cancel' })
            }

            actions.push({
                text: Strings.UPDATE, onPress: () => {
                    Utility.log("GO FOR UPDATE")
                    Utility.openExternalUrl(appLink)
                    BackHandler.exitApp()
                }
            })

            Alert.alert(
                title,
                Strings.UPDATE_APP,
                actions,
                { cancelable: false }
            )
        } catch (error) {
            Utility.log("==================> " + error)
            this.initSplash()
        }
    }

    initSplash = () => {

        if (this.props.navigation.state.params && this.props.navigation.state.params.logintype == 'logout') {
            this.getDataAndLogin('')
        }
        else
            if (this.props.navigation.state.params && this.props.navigation.state.params.url) {
                this.navigate(this.props.navigation.state.params.url);
            }
            else {
                if (Platform.OS === 'android') {
                    Linking.getInitialURL().then(url => {
                        this.navigate(url);
                    });
                } else {
                    Linking.getInitialURL().then(url => {
                        this.navigate(url)
                    });
                    Linking.addEventListener('url', this._handleOpenURL);
                }
            }
    }

    _handleOpenURL = (event) => {
        this.navigate(event.url)
        Linking.removeEventListener('url', this._handleOpenURL);
    }

    componentWillUnmount() {
        Linking.removeEventListener('url', this._handleOpenURL);
    }

    navigate = (url) => {
        Utility.log('checkUrl==>', url)

        if (url == null) {
            this.getDataAndLogin('')
        }
        else {
            // Clear all storage values
            if (Platform.OS == 'android') {
                AsyncStorage.clear()
                this.getDataAndLogin(url)
            }
            else {
                const asyncStorageKeys = AsyncStorage.getAllKeys().then(() => {
                    if (asyncStorageKeys.length > 0) {
                        AsyncStorage.clear();
                        this.getDataAndLogin(url)
                    }
                    else {
                        this.getDataAndLogin(url)
                    }
                });
            }
        }
    }

    getDataAndLogin = (url) => {
        this.checkPermission()
        this.hideSplashScreen(url)
        // const channel = new firebase.notifications.Android.Channel(Constants.NOTIFICATION_CHANNEL_ID, Constants.NOTIFICATION_CHANNEL_DESC, firebase.notifications.Android.Importance.Max)
        //     .setDescription(Constants.NOTIFICATION_CHANNEL_DESC);

        // // Create the channel
        // firebase.notifications().android.createChannel(channel);

        // registerAppListener(this);

        // firebase.notifications().getInitialNotification()
        //     .then((notificationOpen) => {
        //         if (notificationOpen) {
        //             // Get information about the notification that was opened
        //             const notif = notificationOpen.notification;
        //             alert(JSON.stringify(notif))
        //         }
        //     })
    }

    checkPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    requestPermission = async () => {
        try {
            await firebase.messaging().requestPermission();
            this.getToken()
        } catch (e) {
            // console.warn("Failed to grant permission")
        }
    }

    getToken = async () => {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            Utility.log("fcmToken==>", fcmToken)
            // user has a device token
            Utility.setValueInAsyncStorage(Constants.FCM_TOKEN, fcmToken);

            Utility.getValueFromAsyncStorage(Constants.ACCESS_TOKEN).then((access_token) => {
                if (access_token && access_token != '') {
                    this.updateFCMToken()
                }
            }).catch((error) => {
                Utility.log("error==>", error);
            });
        }
    }

    updateFCMToken = () => {
        try {
            let keys = [Constants.FCM_TOKEN, Constants.USER_ID];
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                let device_id = DeviceInfo.getUniqueId()
                APICalls.updateFCMToken(values[1][1], values[0][1], device_id, this.onSuccessFCM, this.onFailureFCM, this.props)
            });
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccessFCM = (response) => {
        Utility.log('onSuccessFCM===> ', JSON.stringify(response))
    }

    onFailureFCM = (response) => {
        Utility.log('onFailureFCM===> ', JSON.stringify(response))
    }

    hideSplashScreen = async (url) => {
        var storeObject = new AsyncStore()
        if (Constants.APP_TYPE == Constants.INDONESIA) {

            // Update all app lang...
            const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.LANGUAGE_CODE])
            // Utility.log('lang===> ', values[0][1])

            if (values[0][1] && values[0][1] != null) {
                Strings.setLanguage(values[0][1])
            }
            else {
                Strings.setLanguage('en')
            }
        }
        else {
            Strings.setLanguage(Constants.LANGUAGE_CODE_PH)
        }

        if (url == '') {
            //Utility.removeItemValue(Constants.SFA_USER_DATA)
            var promise = storeObject.isUserLogined()
            promise.then(status => {
                this.setState({ loading: false }, () => {
                    if (status) {
                        // first check all data(mmv, city, locality, config data) is async....
                        this.checkAndSyncMMV()
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'drawerScreen' })],
                        });
                        this.props.navigation.dispatch(resetAction);
                    } else {
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'login', params: { token: '' } })],
                        });
                        this.props.navigation.dispatch(resetAction);
                    }
                })
            })
        }
        else {
            let route, routeName, token, sfaUserData = '';
            let openPendingLoans = false;
            if (url.split('?').length > 2) {
                if (url.includes('pendingloans')) {
                    Utility.log('URL with loans, token and user data', url);
                    openPendingLoans = true;
                } else {
                    Utility.log('URL with token and user data', url);
                }
                // Utility.log(url.substring(0, url.indexOf('?')));
                let tokenNuser = url.substring(url.indexOf('?') + 1);
                token = tokenNuser.substring(0, tokenNuser.indexOf('?'));
                //use it to send sfa user data to apis
                sfaUserData = tokenNuser.substring(tokenNuser.indexOf('?') + 1);
                // Utility.log(token);
                //alert(decodeURI(sfaUserData));
                if (Platform.OS === 'android')
                    Utility.setValueInAsyncStorage(Constants.SFA_USER_DATA, sfaUserData)
                else
                    Utility.setValueInAsyncStorage(Constants.SFA_USER_DATA, decodeURI(sfaUserData))
            } else {
                if (Platform.OS == 'android') {
                    route = url.replace(/.*?:\/\//g, '');
                    routeName = route.split('/')[0];
                    token = route.match(/\/([^\/]+)\/?$/)[1];
                    // Utility.log('checkid==>', route, routeName, token)
                }
                else {
                    route = url.replace(/.*?:\/\//g, '');
                    routeName = route.split('?')[0];
                    token = route.split('?')[1]
                    // Utility.log('token==>1', route, routeName, token)
                }
            }

            if (token != '') {
                //Linking.removeEventListener('url', this._handleOpenURL);
                // Utility.log('token==>2', token, this.props)
                // this.props.navigation.replace('login', {
                //     token: token
                // })
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'login', params: { token: token, openPendingLoans: openPendingLoans } })],
                });
                this.props.navigation.dispatch(resetAction);
                // Utility.log('token==>3', token)
            }
        }
    }

    checkAndSyncMMV = () => {
        Utility.getMasterData().then((result) => {
            // Utility.log('result===>', result)
            dbFunctions.getMMVTotalCount().then((result) => {
                Utility.log('MMVTotalCount==>', result)
                if (result == 0) {
                    APICalls.getMMV((response) => {
                        if (response.data && response.data.version && response.data.version.length > 0) {
                            dbFunctions.deleteMMV().then(result => {
                                dbFunctions.insertMMV(response.data.version).then((result) => {
                                    Utility.log('MMV insertion count', result)
                                    this.checkAppType()
                                })
                            })
                        } else {
                            this.checkAppType()
                        }
                    }, (error) => {
                        Utility.log('getMMVError==>', error)
                        this.checkAppType()
                    })
                } else {
                    this.checkAppType()
                }
            })
        })
    }

    checkAppType = () => {
        this.checkAndSyncCity()
        // if (Constants.APP_TYPE == Constants.INDONESIA) {
        //     this.checkAndSyncFinancierMMV()
        // }
        // else {
        //     this.checkAndSyncCity()
        // }
    }

    checkAndSyncFinancierMMV = () => {
        dbFunctions.getFinancierMMVTotalCount().then((result) => {
            Utility.log('FinancierMMVTotalCount==>', result)
            if (result == 0) {
                APICalls.getFinancierMMV((response) => {
                    if (response.data && response.data.version && response.data.version.length > 0) {
                        dbFunctions.deleteFinancierMMV().then(result => {
                            dbFunctions.insertFinancierMMV(response.data.version).then((result) => {
                                Utility.log('Financier MMV insertion count', result)
                                dbFunctions.deleteMYP().then(result => {
                                    dbFunctions.insertMYP(response.data.make_year_price).then((result) => {
                                        Utility.log('MYP insertion count', result)
                                        this.checkAndSyncCity()
                                    })
                                })
                            })
                        })
                    } else {
                        this.checkAndSyncCity()
                    }
                }, (error) => {
                    Utility.log('getFinancierMMVError==>', error)
                    this.checkAndSyncCity()
                })
            } else {
                this.checkAndSyncCity()
            }
        })
    }

    checkAndSyncCity = () => {
        dbFunctions.getCityCount().then((result) => {
            Utility.log('CityTotalCount==>', result)
            if (result == 0) {
                APICalls.getAllCity((response) => {
                    if (response.data && response.data.city && response.data.city.length > 0) {
                        dbFunctions.deleteCity().then(rs => {
                            dbFunctions.insertCity(response.data.city).then((result) => {
                                Utility.log('City insertion count', result);
                                this.checkAndSyncLocality()
                            })
                        })
                    } else {
                        this.checkAndSyncLocality()
                    }
                }, (error) => {
                    Utility.log('CityInsertionError==>', error)
                    this.checkAndSyncLocality()
                })
            } else {
                this.checkAndSyncLocality()
            }
        })
    }

    checkAndSyncLocality = () => {
        let cityArr = []
        dbFunctions.getLocalityTotalCount().then((result) => {
            Utility.log('LocalityTotalCount==>', result)
            if (result == 0) {
                Utility.getValueFromAsyncStorage(Constants.DEALER_CITY).then((dealer_city) => {
                    Utility.log('dealer_city==>', dealer_city)
                    cityArr.push(dealer_city)
                    let param = {
                        city: cityArr
                    }
                    APICalls.getDealerLocality(param, (response) => {
                        if (response.data && response.data.length > 0) {
                            dbFunctions.deleteLocality().then(result => {
                                dbFunctions.insertLocality(response.data).then((result) => {
                                    Utility.log('Locality insertion count', result)
                                })
                            })
                        }
                    }, (error) => {
                        Utility.log('LocalityInsertionError==>', error)
                    })
                }).catch((error) => {
                    Utility.log("dealer_city", error)
                })
            }
        })
    }

    render() {
        return (
            <SplashScreen
                loading={this.state.loading}
            />
        )
    }
}
