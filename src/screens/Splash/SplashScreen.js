import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground,
    Modal,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    StatusBar
} from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values/index'
//import LoadingComponent from '../../granulars/LoadingComponent';
import Config from 'react-native-config'

export default class SplashScreen extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <StatusBar backgroundColor={Colors.SCREEN_BACKGROUND_DARKER} />
                    <ImageBackground source={ImageAssets.splash_background}
                        style={{ width: '100%', height: '100%' }} />

                    <View style={{ position: 'absolute' }}>
                        <Image source={Config.APP_TYPE == 'Id' ? ImageAssets.gcloud_logo : ImageAssets.ph_logo}
                            style={{ width: 120, resizeMode: 'contain', tintColor: Config.APP_TYPE == 'Id' ? null : null }} />
                    </View>

                    <View style={Styles.poweredByStyle}>
                        <Text style={Styles.poweredByText}>{Strings.POWERED_BY}</Text>
                        <Text style={Styles.gaadiText}>{Strings.GAADI_COM}</Text>
                    </View>
                    {/* {this.props.loading && <LoadingComponent backgroundColor={Colors.TRANSPARENT} />} */}
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: Dimens.margin_bottom_iphone
    },
    poweredByStyle: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: Dimens.margin_normal
    },
    poweredByText: {
        fontSize: Dimens.text_normal,
        color: Colors.WHITE_54,
        fontFamily: Strings.APP_FONT,
    },
    gaadiText: {
        fontSize: Dimens.text_normal,
        fontWeight: 'bold',
        color: Colors.WHITE_54,
        fontFamily: Strings.APP_FONT,
    }
});