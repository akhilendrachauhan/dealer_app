/*
* filterData     : filter data to be passed through screen params
* follow the below format to send filterCategories to filter screen
* onFilterApply        : callback to be passed through screen params(will return filterData, params, selectedCounts in argument)
* */
/*
* [
                {
                    "title": "Search By",
                    "key": "keyword",
                    "type": "textbox",
                    "multiSelect": false,
                    "searchEnabled": false
                },
                {
                    "title": "Status",
                    "key": "status",
                    "type": "list",
                    "multiSelect": true,
                    "searchEnabled": false,
                    "list": [
                        {
                            "id": 1,
                            "key": "new",
                            "value": "New"
                        },
                        {
                            "id": 2,
                            "value": "Follow Up",
                            "key": "follow_up"
                        },
                        {
                            "id": 3,
                            "value": "Interested",
                            "key": "interested"
                        },
                        {
                            "id": 4,
                            "value": "Walk-in Scheduled",
                            "key": "walk_in_scheduled"
                        },
                        {
                            "id": 9,
                            "value": "Walk-in Done",
                            "key": "walk_in_done"
                        },
                        {
                            "id": 10,
                            "value": "Customer Offer",
                            "key": "customer_offer"
                        },
                        {
                            "id": 11,
                            "value": "Booked",
                            "key": "booked"
                        },
                        {
                            "id": 12,
                            "value": "Converted",
                            "key": "converted"
                        },
                        {
                            "id": 13,
                            "value": "Closed",
                            "key": "closed"
                        }
                    ]
                },
                {
                    "title": "Source",
                    "key": "lead_source",
                    "type": "list",
                    "multiSelect": true,
                    "searchEnabled": false,
                    "list": [
                        {
                            "id": 1,
                            "key": "Gaadi",
                            "value": "Gaadi"
                        },
                        {
                            "id": 2,
                            "key": "Cardekho",
                            "value": "Cardekho"
                        },
                        {
                            "id": 3,
                            "key": "CARTRADE",
                            "value": "Cartrade"
                        },
                        {
                            "id": 4,
                            "key": "CARWAL",
                            "value": "Carwal"
                        },
                        {
                            "key": "OLX",
                            "value": "Olx"
                        },
                        {
                            "id": 5,
                            "key": "QUIKR",
                            "value": "Quikr"
                        },
                        {
                            "id": 6,
                            "key": "website",
                            "value": "website"
                        },
                        {
                            "id": 7,
                            "key": "WALK-IN",
                            "value": "Walk In"
                        },
                        {
                            "id": 8,
                            "key": "cardekho_knowlarity",
                            "value": "Cardekho Knowlarity"
                        },
                        {
                            "id": 9,
                            "key": "zigwheels",
                            "value": "Zigwheels"
                        },
                        {
                            "id": 10,
                            "key": "DealerApp",
                            "value": "Dealerapp"
                        },
                        {
                            "id": 11,
                            "key": "Facebook",
                            "value": "Facebook"
                        },
                        {
                            "id": 12,
                            "key": "dealer_page",
                            "value": "Dealer Page"
                        }
                    ]
                },
                {
                    "title": "Verified",
                    "key": "verified",
                    "type": "list",
                    "multiSelect": true,
                    "searchEnabled": false,
                    "list": [
                        {
                            "id": 1,
                            "value": "Otp Verified",
                            "key": "otp_verified"
                        },
                        {
                            "id": 2,
                            "value": "Call Verified",
                            "key": "call_verified"
                        }
                    ]
                },
                {
                    "title": "Date",
                    "type": "dateList",
                    "key": "date",
                    "multiSelect": false,
                    "searchEnabled": false,
                    "list": [
                        {
                            "id": 1,
                            "value": "Lead Creation",
                            "key": "lead_creation",
                            "monthLimit": 0,
                            "fromKey": "created_from",
                            "fromValue": "From",
                            "toKey": "created_to",
                            "toValue": "To"
                        },
                        {
                            "id": 2,
                            "value": "Lead Updated",
                            "key": "lead_updated",
                            "monthLimit": 0,
                            "fromKey": "updated_from",
                            "fromValue": "From",
                            "toKey": "updated_to",
                            "toValue": "To"
                        },
                        {
                            "id": 3,
                            "value": "Lead Follow Up",
                            "key": "lead_follow_up",
                            "monthLimit": 0,
                            "fromKey": "followup_from",
                            "fromValue": "From",
                            "toKey": "followup_to",
                            "toValue": "To"
                        }
                    ]
                }
            ]
* */


import React, { Component } from 'react';
import { Utility } from "../../util";
import * as Constants from '../../util/Constants';
import FilterScreen from './FilterScreen';
import { Keyboard } from "react-native";
import Toast from "react-native-simple-toast";

var self;
dateFormat = require('../../util/DateFormat');
export default class FilterContainer extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.onBackPress = this.onBackPress.bind(this);
        this.onFilterApply = this.onFilterApply.bind(this);
        this.onResetPress = this.onResetPress.bind(this);
        this.onFilterClick = this.onFilterClick.bind(this);
        this.onFilterOptionClick = this.onFilterOptionClick.bind(this);
        this.setDateValues = this.setDateValues.bind(this);
        let categories = JSON.stringify(props.navigation.state.params.filterData ? props.navigation.state.params.filterData : []);
        this.state = {
            categories: JSON.parse(categories),
            activeIndex: 0,
            activeDateIndex: undefined,
            activeDateIndexType: undefined,
            showDatePicker: false,
            maxDate: new Date(),
            minDate: new Date(new Date().getFullYear(), new Date().getMonth() - (2), 1)
        }
    }

    onBackPress() {
        this.props.navigation.goBack();
    }

    onFilterApply() {
        let params = {};
        let categories = this.state.categories;
        let selectedCount = 0;
        for (let i = 0; i < categories.length; i++) {
            let cat = categories[i];
            if (cat.count && cat.count > 0) {
                selectedCount++;
                if (cat.type === 'dateList') {
                    for (let j = 0; j < cat.list.length; j++) {
                        let option = cat.list[j];
                        if (option.selected && option.selectedFromDate && option.selectedToDate) {
                            params[option.fromKey] = dateFormat(option.selectedFromDate.toString(), 'dd-mm-yyyy');
                            params[option.toKey] = dateFormat(option.selectedToDate.toString(), 'dd-mm-yyyy');
                        }
                    }
                } else if (cat.type === 'list') {
                    if (cat.key == 'verified') {
                        for (let j = 0; j < cat.list.length; j++) {
                            if (cat.list[j].selected) {
                                // selectedValues.push(cat.list[j].id)
                                params[cat.list[j].key] = cat.list[j].id
                            }
                        }
                    }
                    else {
                        let selectedValues = [];
                        for (let j = 0; j < cat.list.length; j++) {
                            if (cat.list[j].selected) {
                                selectedValues.push(cat.list[j].id)
                            }
                        }
                        params[cat.key] = selectedValues
                    }
                }
            }
        }
        Utility.log(params, selectedCount);
        this.props.navigation.state.params.onFilterApply && this.props.navigation.state.params.onFilterApply(this.state.categories, params, selectedCount);
        this.props.navigation.goBack();
    }

    onFilterClick(index) {
        if (this.state.activeIndex !== index) {
            this.setState({
                activeIndex: index
            })
        }
    }

    onFilterOptionClick(index) {
        let categories = this.state.categories;
        categories[this.state.activeIndex].list[index]['selected'] = !categories[this.state.activeIndex].list[index].selected;
        categories[this.state.activeIndex]['count'] = categories[this.state.activeIndex].list.filter((option) => option.selected === true).length;
        this.setState({
            categories: categories
        });
    }

    onResetPress() {
        Keyboard.dismiss();
        //selectedFromDate
        //selectedToDate
        let categories = this.state.categories;
        for (let i = 0; i < categories.length; i++) {
            let cat = categories[i];
            cat['count'] = 0;
            if (cat.type === 'dateList') {
                for (let j = 0; j < cat.list.length; j++) {
                    let option = cat.list[j];
                    option['selected'] = false;
                    option['selectedFromDate'] = undefined;
                    option['selectedToDate'] = undefined;
                }
            } else if (cat.type === 'list') {
                for (let j = 0; j < cat.list.length; j++) {
                    let option = cat.list[j];
                    option['selected'] = false;
                }
            }
        }
        this.setState({
            categories: categories,
            activeIndex: 0,
            activeDateIndex: undefined,
            activeDateIndexType: undefined
        });
        Utility.log('onReset', categories)
    }

    setDateValues(fromDate, toDate) {
        if (fromDate && toDate) {
            this.state.categories[this.state.activeIndex].fromDate = fromDate;
            this.state.categories[this.state.activeIndex].toDate = toDate;
            this.state.categories[this.state.activeIndex].count = 1;
        } else {
            this.state.categories[this.state.activeIndex].count = 0;
        }
    }

    componentDidMount() {

    }

    onDateOptionPress = (item, index) => {
        //Utility.log(index);
        let data = this.state.categories;
        if (!data[this.state.activeIndex].multiSelect) {
            for (let i = 0; i < data[this.state.activeIndex].list.length; i++) {
                data[this.state.activeIndex].list[i]['selected'] = i === index;
            }
        } else {
            data[this.state.activeIndex].list[index]['selected'] = true;
        }
        this.setState({ categories: data })
    };

    _handleDatePicked = (date) => {
        let data = this.state.categories;
        if (this.state.activeDateIndexType === 0) {
            data[this.state.activeIndex].list[this.state.activeDateIndex]['selectedFromDate'] = date;
            data[this.state.activeIndex].list[this.state.activeDateIndex]['selectedToDate'] = undefined;
            data[this.state.activeIndex]['count'] = 0;
        } else if (this.state.activeDateIndexType === 1) {
            data[this.state.activeIndex].list[this.state.activeDateIndex]['selectedToDate'] = date;
            data[this.state.activeIndex]['count'] = 1;
        }
        this.setState({
            showDatePicker: false,
            categories: data
        })
    };

    _hideDateTimePicker = () => {
        this.setState({
            showDatePicker: false
        })
    };


    onDatePickerPress = (index, type) => {
        //selectedFromDate
        //selectedToDate
        let maxDate = new Date();
        let minDate = new Date(new Date().getFullYear(), new Date().getMonth() - (2), 1);
        let data = this.state.categories;
        if (type === 1) {
            if (!data[this.state.activeIndex].list[index].selectedFromDate) {
                Toast.show('Please select from date first');
                return;
            } else {
                minDate = data[this.state.activeIndex].list[index].selectedFromDate;
            }
        }
        this.setState({
            showDatePicker: true,
            maxDate: maxDate,
            minDate: minDate,
            activeDateIndex: index,
            activeDateIndexType: type
        });
    };

    render() {
        return (
            <FilterScreen
                ref={'filterScreen'}
                categories={this.state.categories}
                onBackPress={this.onBackPress}
                onFilterApply={this.onFilterApply}
                activeIndex={this.state.activeIndex}
                onResetPress={this.onResetPress}
                onFilterClick={this.onFilterClick}
                setDateValues={this.setDateValues}
                monthLimit={2}
                onFilterOptionClick={this.onFilterOptionClick}
                maxDate={this.state.maxDate}
                minDate={this.state.minDate}
                showDatePicker={this.state.showDatePicker}
                onDateOptionPress={this.onDateOptionPress}
                _handleDatePicked={this._handleDatePicked}
                _hideDateTimePicker={this._hideDateTimePicker}
                onDatePickerPress={this.onDatePickerPress} />
        )
    }
}