import React, {Component} from 'react';
import {
    FlatList,
    Image,
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
    ScrollView, SafeAreaView
} from 'react-native';
import {Colors, Dimens} from "../../values";
import * as Strings from "../../values/Strings";
import * as Utility from "../../util/Utility";
import FilterChildComponent from "../../components/FilterChildComponent";
import FilterParentComponent from "../../components/FilterParentComponent";
import DateTimePicker from "react-native-modal-datetime-picker";
import ActionBarWrapper from "../../granulars/ActionBarWrapper";

var extraData = true;
let self;
dateFormat = require('../../util/DateFormat');
export default class FilterScreen extends Component {
    constructor(props) {
        super(props);
        self = this;

    }

    componentDidMount() {

    }

    renderDateComponent = (item, index) => {
        return (
            <View style={{backgroundColor: Colors.WHITE}}>
                <TouchableOpacity
                    onPress={() => this.props.onDateOptionPress(item, index)}
                    style={{flexDirection: 'row', paddingTop: 20, paddingHorizontal: 20}}>
                    {item.selected ? <View style={[{borderWidth: 2, marginEnd: 8, padding: 3},
                        {
                            width: 20,
                            height: 20,
                            borderColor: Colors.PRIMARY,
                            borderRadius: 20 / 2,
                        }]}>
                        <View style={[{flex: 1,}, {
                            backgroundColor: Colors.PRIMARY,
                            borderRadius: 20 / 2,
                        }]}/>
                    </View> : <View style={[{borderWidth: 2, marginEnd: 8, padding: 3},
                        {
                            width: 20,
                            height: 20,
                            borderColor: Colors.BLACK_54,
                            borderRadius: 20 / 2,
                        }]}/>}
                    <Text style={{
                        color: item.selected ? Colors.BLACK : Colors.BLACK_70,
                        fontSize: Dimens.text_normal
                    }}>{item.value}</Text>
                </TouchableOpacity>
                {item.selected && <View style={{paddingLeft: 40, paddingRight: 20}}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.props.onDatePickerPress(index, 0)}
                        style={{flexDirection: 'column', paddingTop: 20}}>
                        {item.selectedFromDate &&
                        <Text style={{
                            fontSize: 12,
                            color: Colors.BLACK_40,
                            paddingBottom: 5
                        }}>{item.fromValue}</Text>}
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text
                                style={[{
                                    fontSize: 16,
                                    fontFamily: Strings.APP_FONT,
                                    color: Colors.BLACK_70
                                }]}>{item.selectedFromDate ? dateFormat(item.selectedFromDate.toString(), 'dd mmm yyyy') : item.fromValue}</Text>
                            <Image style={{height: 20, width: 20, resizeMode: 'contain'}}
                                   source={require('../../assets/drawable/calender.png')}/>
                        </View>
                        <View style={{height: 1, backgroundColor: Colors.BLACK_25, marginTop: 10}}/>

                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.props.onDatePickerPress(index, 1)}
                        style={{flexDirection: 'column', paddingTop: 20}}>
                        {item.selectedToDate &&
                        <Text
                            style={{
                                fontSize: 12,
                                color: Colors.BLACK_40,
                                paddingBottom: 5
                            }}>{item.toValue}</Text>}
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text
                                style={[{
                                    fontSize: 16,
                                    fontFamily: Strings.APP_FONT,
                                    color: Colors.BLACK_70
                                }]}>{item.selectedToDate ? dateFormat(item.selectedToDate.toString(), 'dd mmm yyyy') : item.toValue}</Text>
                            <Image style={{height: 20, width: 20, resizeMode: 'contain'}}
                                   source={require('../../assets/drawable/calender.png')}/>
                        </View>
                        <View style={{height: 1, backgroundColor: Colors.BLACK_25, marginTop: 16}}/>

                    </TouchableOpacity>
                </View>}
                <View style={{
                    height: 1,
                    backgroundColor: Colors.BLACK_25,
                    marginHorizontal: 20,
                    marginTop: 20
                }}/>
            </View>
        )
    };

    render() {
        let actionBarProps = {
            values: {
                title: Strings.FILTER
            },
            styleAttr: {
                leftIconImage: require('../../assets/drawable/close.png'),
                disableShadows: true
            },
            actions: {
                onLeftPress: this.props.onBackPress,
                onRightTextPress: this.props.onResetPress
            }
        };
        extraData = !extraData;
        //Utility.log(this.props.categories[this.props.activeIndex])
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <View style={{position: 'absolute', right: 20, top: 0, height: 56}}>
                        <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', height: 56}}
                                          onPress={this.props.onResetPress}>
                            <Text style={{color: Colors.WHITE}}>{Strings.RESET}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.35, backgroundColor: Colors.SCREEN_BACKGROUND}}>
                            <FlatList
                                extraData={extraData}
                                data={this.props.categories}
                                vertical={true}
                                keyExtractor={(item, index) => index.toString()}
                                showsverticalScrollIndicator={false}
                                renderItem={({item, index}) =>
                                    <FilterParentComponent
                                        activeIndex={this.props.activeIndex}
                                        index={index}
                                        data={item}
                                        onFilterClick={this.props.onFilterClick}/>
                                }/>
                        </View>
                        <View style={{flex: 0.65, backgroundColor: Colors.WHITE}}>
                            {this.props.categories[this.props.activeIndex].type === 'list' && <FlatList
                                extraData={extraData}
                                data={this.props.categories[this.props.activeIndex].list}
                                vertical={true}
                                showsverticalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) =>
                                    <FilterChildComponent
                                        index={index}
                                        data={item}
                                        onFilterOptionClick={this.props.onFilterOptionClick}/>
                                }/>}
                            {this.props.categories[this.props.activeIndex].type === 'dateList' && this.props.categories[this.props.activeIndex].list && this.props.categories[this.props.activeIndex].list.length > 0 &&
                            <ScrollView>
                                {this.props.categories[this.props.activeIndex].list.map((item, index) => {
                                    return this.renderDateComponent(item, index)
                                })}
                            </ScrollView>}
                        </View>


                    </View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={this.props.onFilterApply} style={{
                        height: 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.PRIMARY
                    }}>
                        <Text style={{
                            color: Colors.WHITE,
                            fontSize: 16,
                            fontWeight: '500',
                            fontFamily: Strings.APP_FONT
                        }}>APPLY</Text>
                    </TouchableOpacity>
                    <DateTimePicker
                        maximumDate={this.props.maxDate}
                        minimumDate={this.props.minDate}
                        isVisible={this.props.showDatePicker}
                        onConfirm={this.props._handleDatePicked}
                        onCancel={this.props._hideDateTimePicker}
                        mode={'date'}
                    />
                </View>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    headerText: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 18,
        fontFamily: Strings.APP_FONT,
        backgroundColor: Colors.WHITE,
        color: Colors.BLACK
    }
});
