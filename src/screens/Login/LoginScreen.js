import React, { Component } from 'react'
import {
    Platform, StyleSheet, View, Text, Image, TextInput, Keyboard, ImageBackground,
    TouchableOpacity, TouchableWithoutFeedback, KeyboardAvoidingView, StatusBar
} from 'react-native'
import TextInputLayout from "../../granulars/TextInputLayout"
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants } from '../../util'
import Config from 'react-native-config'
import LoadingComponent from '../../components/LoadingComponent'
import { Dropdown } from 'react-native-material-dropdown'

export default class LoginScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            phoneNumber: '',
            email: '',
            password: '',
            otpCode: '',
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            otp5: '',
            otp6: '',
            showProgress: false,
            enterOtpActive: false,
            showIcon: false,
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            countryCodeData: []
        }
    }

    async componentDidMount() {
        if (Config.BUILD === 'DEV') {
            this.setState({
                phoneNumber: '',
                email: "test.demo@oto.com",
                password: "Oto@123",
                showIcon: true
            });
        }
        else if (Config.BUILD === 'DEV_PH') {
            this.setState({
                phoneNumber: '',
                email: "dyamesh_be2@yahoo.com",
                password: "Oto@123",
                showIcon: true
            });
        }

        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeData: countryCodeData.length > 0 ? countryCodeData : Constants.COUNTRY_CODE_LIST })
    }

    render() {
        return (
            <KeyboardAvoidingView
                keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : -500}
                style={Styles.container} behavior="padding">

                <TouchableWithoutFeedback onPress={() => {
                    Keyboard.dismiss()
                }}>
                    <View style={Styles.container}>
                        {this.props.enterOtpActive ? this.otpLoginRender() : this.loginRender()}

                        {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
                    </View>

                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }

    loginRender() {
        return (
            <View style={Styles.container}>
                <StatusBar backgroundColor={Colors.SCREEN_BACKGROUND_DARKER} />
                <ImageBackground style={Styles.container} source={ImageAssets.splash_background} />

                <View style={Styles.outerComponentStyle}>
                    <View style={Styles.logoStyle}>
                        <Image source={Config.APP_TYPE == 'Id' ? ImageAssets.gcloud_logo : ImageAssets.ph_logo}
                            style={{ width: 120, resizeMode: 'contain' }} />
                    </View>
                    <View style={{ flex: 1 }} />

                    <View style={{ paddingBottom: Dimens.padding_xxx_large, height: 320, marginBottom: Dimens.margin_normal }}>
                        {this.props.loginByMobileOtp ? this.mobileLoginRender() : this.emailLoginRender()}
                    </View>
                    {Constants.APP_TYPE == Constants.PHILIPPINES ? <TouchableOpacity activeOpacity={0.8} style={{ marginTop: Dimens.margin_0, marginBottom: Dimens.margin_normal }}
                        onPress={this.props.gotoSignUp}>

                        <View style={{ height: 25, justifyContent: "center", alignItems: 'center' }}>
                            <Text style={Styles.loginTextStyle}>{Strings.JOIN_US}</Text>
                        </View>
                    </TouchableOpacity> : <View></View>}
                    <View style={Styles.poweredByStyle}>
                        <Text style={Styles.poweredByText}>{Strings.POWERED_BY}</Text>
                        <Text style={Styles.gaadiText}>{Strings.GAADI_COM}</Text>
                    </View>
                </View>
            </View>
        )
    }

    mobileLoginRender() {
        return (
            <View style={{ position: 'absolute', alignSelf: 'center', left: 0, right: 0, bottom: 5, }}>

                <View style={{ flexDirection: 'row', marginHorizontal: Dimens.margin_xxx_large, alignItems: 'center', justifyContent: 'center' }}>
                    <Dropdown
                        containerStyle={{ flex: 0.4 }}
                        dropdownOffset={{ top: 58, left: 0 }}
                        baseColor={Colors.WHITE}
                        textColor={Colors.WHITE}
                        selectedItemColor={Colors.PRIMARY}
                        data={this.state.countryCodeData}
                        absoluteRTLLayout={true}
                        value={this.state.countryCode}
                        onChangeText={(value, index, data) => {
                            this.setState({ countryCode: value, selectedItem: data[index] })
                        }}
                    />

                    <View style={{ flex: 1.5, paddingLeft: Dimens.padding_xx_large }}>

                        <Image style={{ top: 52, height: 20 }} source={ImageAssets.icon_mobile} />
                        <TextInputLayout
                            style={{ paddingLeft: Dimens.padding_xxxx_large }}
                            ref={(input) => this.TL_PhoneNumber = input}
                            hintColor={Colors.WHITE}
                            focusColor={Colors.ORANGE}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_xxx_large}>

                            <TextInput style={Styles.textInput}
                                selectTextOnFocus={false}
                                returnKeyType='go'
                                keyboardType='numeric'
                                editable={true}
                                value={this.state.phoneNumber}
                                ref={(input) => this.PhoneNumberInput = input}
                                maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                onSubmitEditing={() => this.props.onLoginWithOTP(this.state.countryCode, this.state.phoneNumber)}
                                placeholder={Strings.MOBILE_NUMBER}
                                onChangeText={(text) => this.setState({ phoneNumber: text })}
                            />
                        </TextInputLayout>
                    </View>
                </View>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.props.onLoginWithOTP(this.state.countryCode, this.state.phoneNumber)}>

                    <View style={Styles.buttonStyle}>
                        <Text style={Styles.loginStyle}>{Strings.LOGIN}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8} style={{ marginTop: Dimens.margin_xxx_large }}
                    onPress={this.props.changeLoginType}>

                    <View style={{ height: 25, justifyContent: "center", alignItems: 'center' }}>
                        <Text style={Styles.loginTextStyle}>{Strings.LOGIN_BY_EMAIL}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    onEmailTextChange = (text) => {
        let showIcon = false;
        if (Utility.validateEmail(text)) {
            showIcon = true;
        }
        this.setState({ email: text, showIcon: showIcon })
    }

    emailLoginRender() {
        return (
            <View style={{ position: 'absolute', alignSelf: 'center', left: 0, right: 0, bottom: 5, }}>
                <View>
                    <Image style={[Styles.iconStyle, { top: this.state.showIcon ? 55 : 50 }]} source={ImageAssets.icon_email} />
                    <TextInputLayout
                        style={Styles.textInputStyle}
                        ref={(input) => this.TL_Email = input}
                        showRightDrawable={true}
                        focusColor={Colors.ORANGE}
                        hintColor={Colors.WHITE}
                        errorColor={Colors.PRIMARY}
                        errorColorMargin={Dimens.margin_xxx_large}
                        rightDrawable={this.state.showIcon ? ImageAssets.icon_right : null}
                        rightDrawableStyle={{ width: 22, resizeMode: 'contain' }}
                        rightDrawableMarginTop={Dimens.margin_xxx_large}>
                        <TextInput style={Styles.textInput}
                            selectTextOnFocus={false}
                            returnKeyType='next'
                            keyboardType='email-address'
                            value={this.state.email}
                            ref={(input) => this.EmailInput = input}
                            onSubmitEditing={() => this.PasswordInput.focus()}
                            placeholder={Strings.EMAIL_ADDRESS}
                            onChangeText={(text) => this.onEmailTextChange(text)} />

                    </TextInputLayout>
                </View>

                <View>
                    <Image style={Styles.iconStyle} source={ImageAssets.icon_lock} />
                    <TextInputLayout
                        ref={(input) => this.TL_Password = input}
                        style={Styles.textInputStyle}
                        showRightDrawable={true}
                        focusColor={Colors.ORANGE}
                        hintColor={Colors.WHITE}
                        errorColor={Colors.PRIMARY}
                        errorColorMargin={Dimens.margin_xxx_large}
                        onRightClick={this.props.hideAndShowPassword}
                        rightDrawable={this.props.hidePassword ? ImageAssets.icon_show_password : ImageAssets.hide_password}
                        rightDrawableStyle={{ width: 24, resizeMode: 'contain',tintColor: Colors.WHITE }}
                        rightDrawableMarginTop={Dimens.margin_xxx_large}>
                        <TextInput
                            style={Styles.textInput}
                            selectTextOnFocus={false}
                            returnKeyType='go'
                            value={this.state.password}
                            ref={(input) => this.PasswordInput = input}
                            secureTextEntry={this.props.hidePassword}
                            onSubmitEditing={() => this.props.onLoginWithEmail(this.state.email, this.state.password, '')}
                            placeholder={Strings.PASSWORD}
                            onChangeText={(text) => this.setState({ password: text })}
                        />

                    </TextInputLayout>
                </View>

                <TouchableOpacity activeOpacity={0.8} style={{ height: 30 }}
                    onPress={this.props.onForgotPasswordClick}>
                    <Text style={Styles.forgotPasswordStyle}>{Strings.FORGOT_PASSWORD}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.props.onLoginWithEmail(this.state.email, this.state.password, '')}>
                    <View style={Styles.buttonStyle}>
                        <Text style={Styles.loginStyle}>{Strings.LOGIN}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={{ marginTop: Dimens.margin_xxx_large }} onPress={this.props.changeLoginType}>
                    <View style={{ height: 25, justifyContent: "center", alignItems: 'center' }}>
                        <Text style={Styles.loginTextStyle}>{Strings.LOGIN_BY_PHONE}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    otpLoginRender() {
        return (
            <View style={Styles.container}>
                <ImageBackground style={Styles.container} source={ImageAssets.splash_background} />

                <TouchableOpacity style={{ position: 'absolute', top: 20, left: 0 }}
                    activeOpacity={0.8}
                    onPress={() => this.props.changeOtpView()}>

                    <Image style={{ width: 50, height: 24, tintColor: Colors.WHITE }}
                        source={ImageAssets.icon_arrow_left} />
                </TouchableOpacity>

                <View style={Styles.outerComponentStyleForOtp}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                        <Image source={ImageAssets.icon_otp} />
                        <Text style={Styles.otpTextStyle}>{Strings.VERIFY_MOBILE_NO}</Text>
                        <Text style={Styles.gaadiText}>{Strings.GET_SMS_CODE}</Text>

                        <View style={Styles.otpViewStyle}>
                            <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                autoFocus={true}
                                returnKeyType='next'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                value={this.state.otp1}
                                blurOnSubmit={false}
                                ref='otpInput1'
                                onSubmitEditing={() => this.otpInput2.focus()}
                                placeholderTextColor={Colors.WHITE}
                                onChangeText={(text) => {
                                    this.setState({ otp1: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput2.focus();
                                    }
                                }} />
                            <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                returnKeyType='next'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                blurOnSubmit={false}
                                placeholderTextColor={Colors.WHITE}
                                value={this.state.otp2}
                                ref='otpInput2'
                                onSubmitEditing={() => this.otpInput3.focus()}
                                onChangeText={(text) => {
                                    this.setState({ otp2: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput3.focus();
                                    } else {
                                        this.refs.otpInput1.focus();
                                    }
                                }} />
                            <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                returnKeyType='next'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                blurOnSubmit={false}
                                placeholderTextColor={Colors.WHITE}
                                value={this.state.otp3}
                                ref='otpInput3'
                                onSubmitEditing={() => this.otpInput4.focus()}
                                onChangeText={(text) => {
                                    this.setState({ otp3: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput4.focus();
                                    } else {
                                        this.refs.otpInput2.focus();
                                    }
                                }} />
                            <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                returnKeyType='done'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                blurOnSubmit={false}
                                placeholderTextColor={Colors.WHITE}
                                value={this.state.otp4}
                                ref='otpInput4'
                                onSubmitEditing={() => this.props.onOtpVerifyAndSubmit(this.state.otp1, this.state.otp2,
                                    this.state.otp3, this.state.otp4, this.state.otp5, this.state.otp6)}
                                onChangeText={(text) => {
                                    this.setState({ otp4: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput4.focus();
                                    } else {
                                        this.refs.otpInput3.focus();
                                    }
                                }} />
                            {/* <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                returnKeyType='next'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                blurOnSubmit={false}
                                placeholderTextColor={Colors.WHITE}
                                value={this.state.otp5}
                                ref='otpInput5'
                                onSubmitEditing={() => this.otpInput6.focus()}
                                onChangeText={(text) => {
                                    this.setState({ otp5: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput6.focus();
                                    } else {
                                        this.refs.otpInput4.focus();
                                    }
                                }} /> */}
                            {/* <TextInput style={Styles.otpTextInput}
                                underlineColorAndroid={Colors.WHITE}
                                returnKeyType='go'
                                keyboardType='numeric'
                                editable={true}
                                maxLength={1}
                                blurOnSubmit={false}
                                placeholderTextColor={Colors.WHITE}
                                value={this.state.otp6}
                                ref='otpInput6'
                                onChangeText={(text) => {
                                    this.setState({ otp6: text });
                                    if (text && text.length === 1) {
                                        this.refs.otpInput6.focus();
                                    } else {
                                        this.refs.otpInput5.focus();
                                    }
                                }} /> */}
                        </View>
                    </View>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onOtpVerifyAndSubmit(this.state.otp1, this.state.otp2,
                                this.state.otp3, this.state.otp4, this.state.otp5, this.state.otp6)}>
                            <View style={Styles.buttonStyle}>
                                <Text style={Styles.loginStyle}>{Strings.VERIFY_NOW}</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{ padding: Dimens.padding_normal, flexDirection: "row", justifyContent: "center", alignItems: 'center', marginTop: 7 }}>
                            <Text style={{
                                flex: 1,
                                marginLeft: Dimens.margin_x_small,
                                padding: Dimens.padding_x_small,
                                color: Colors.WHITE,
                                fontSize: Dimens.text_normal,
                                fontFamily: Strings.APP_FONT
                            }}>{Strings.NOT_RECEIVE_OTP_CODE}</Text>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.resendOTP()}>
                                <Text style={{
                                    marginLeft: Dimens.margin_x_small,
                                    color: Colors.ORANGE,
                                    fontWeight: 'bold',
                                    fontSize: Dimens.text_normal,
                                    fontFamily: Strings.APP_FONT
                                }}>
                                    {Strings.RESEND_CODE}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {(Platform.OS === 'ios') ?
                    <TouchableOpacity
                        style={{ position: 'absolute', left: 20, top: 20, height: 25, width: 25, justifyContent: 'center', alignItems: 'center' }}
                        activeOpacity={0.8}
                        onPress={() => this.props.backButtonAction()}>

                        <Image style={{ height: 20, width: 20 }} source={ImageAssets.icon_back_arrow} />
                    </TouchableOpacity>
                    :
                    null
                }
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    outerComponentStyle: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        height: '100%',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: Colors.transparent,
        paddingBottom: Platform.OS == 'ios' ? Dimens.padding_normal : 0
    },
    outerComponentStyleForOtp: {
        position: 'absolute',
        alignSelf: 'center',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: Colors.transparent,
    },
    logoStyle: {
        height: '40%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInputStyle: {
        paddingLeft: Dimens.padding_xxxx_large,
        marginHorizontal: Dimens.margin_xxx_large
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.WHITE,
        alignSelf: 'stretch',
    },
    loginStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 5,
    },
    forgotPasswordStyle: {
        top: 10,
        color: Colors.ORANGE,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'right',
        marginHorizontal: Dimens.margin_xxx_large,
        fontFamily: Strings.APP_FONT
    },
    poweredByStyle: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: 'center',
        marginBottom: Dimens.margin_normal,
    },
    poweredByText: {
        fontSize: Dimens.text_normal,
        color: Colors.WHITE_54,
        fontFamily: Strings.APP_FONT,
    },
    gaadiText: {
        fontSize: Dimens.text_normal,
        fontWeight: 'bold',
        color: Colors.WHITE_54,
        fontFamily: Strings.APP_FONT,
    },
    iconStyle: {
        top: 48,
        marginLeft: Dimens.margin_xxxx_large,
        resizeMode: 'contain',
        height: 22, width: 22
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: Dimens.padding_normal
    },
    loginTextStyle: {
        color: Colors.ORANGE,
        fontWeight: 'bold',
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    otpTextStyle: {
        color: Colors.WHITE,
        fontWeight: 'bold',
        height: 31,
        fontSize: Dimens.text_xx_large,
        marginTop: Dimens.margin_xxx_large,
        fontFamily: Strings.APP_FONT
    },
    otpTextInput: {
        height: 60,
        marginLeft: Dimens.margin_x_small,
        marginRight: Dimens.margin_x_small,
        width: 40,
        fontSize: Dimens.text_xxxx_large,
        textAlign: 'center',
        color: Colors.WHITE,
        borderBottomColor: Platform.OS == 'ios' ? Colors.WHITE : "transparent",
        borderBottomWidth: Platform.OS == 'ios' ? 1 : 0
    },
    otpViewStyle: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: 'center',
        marginTop: Dimens.margin_xxxx_large,
    },
});