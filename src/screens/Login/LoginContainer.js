import React, { Component } from 'react';
import LoginScreen from './LoginScreen'
import { Keyboard, Platform } from 'react-native';
import { Utility, Constants, AnalyticsConstants } from '../../util'
import { Strings } from '../../values'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import DBFunctions from "../../database/DBFunctions";
import DeviceInfo from 'react-native-device-info';
import Config from 'react-native-config'
let countryCode, mobileNum, isResend, isTokenLogin = false

export default class LoginContainer extends Component {

    constructor(props) {
        super(props)
        this.dbFunctions = new DBFunctions()
        this.state = {
            isLoading: false,
            loadingMsg: '',
            loginByMobileOtp: true,
            hidePassword: true,
            enterOtpActive: false,
            gcloudToken : this.props.navigation.state.params && this.props.navigation.state.params.token ? this.props.navigation.state.params.token : null
        }
        // Utility.log("device_id11:" + DeviceInfo.getUniqueId())
    }

    async componentDidMount() {
        this.dbFunctions.getDbHelper().initDB()
        Utility.sendCurrentScreen(AnalyticsConstants.LOGIN_SCREEN)
        if (this.props.navigation.state.params) {
            let gcloudToken = this.props.navigation.state.params.token

            if (gcloudToken != '') {
                Utility.log('gcloudToken==> ', gcloudToken)
                // this.setState({ loginByMobileOtp: false })
                this.onLoginWithEmail('', '', gcloudToken)
            }
        }
        //alert(await Utility.getValueFromAsyncStorage(Constants.SFA_USER_DATA))
    }

    // componentWillReceiveProps() {
    //     if (this.props.navigation.state.params) {
    //         let gcloudToken = this.props.navigation.state.params.token

    //         if (gcloudToken != '') {
    //             Utility.log('gcloudToken==> ', gcloudToken)
    //             //this.setState({ loginByMobileOtp: false })
    //             this.onLoginWithEmail('', '', gcloudToken)
    //         }
    //     }
    // }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.navigation.state.params && nextProps.navigation.state.params.token){
            if(nextProps.navigation.state.params.token !== prevState.gcloudToken){
                return { gcloudToken : nextProps.navigation.state.params.token};
            }
            else return null;
        }
        else return null;
     }
     
     componentDidUpdate(prevProps, prevState) {
       if(prevState.gcloudToken!==this.state.gcloudToken){
            this.onLoginWithEmail('', '', gcloudToken)
       }
     }

    

    changeLoginType = () => {
        let loginByMobileOtp = this.state.loginByMobileOtp;
        this.setState({ loginByMobileOtp: !loginByMobileOtp })
    }

    changeOtpView = () => {
        let enterOtpActive = this.state.enterOtpActive;
        this.setState({ enterOtpActive: !enterOtpActive })
    }

    hideAndShowPassword = () => {
        this.setState({
            hidePassword: !this.state.hidePassword
        })
    }

    onLoginWithOTP = async (countryCode, phoneNumber, isResendOTP) => {
        countryCode = countryCode;
        mobileNum = phoneNumber;
        isResend = isResendOTP;

        if (!isResend) {
            if (!Utility.validateMobileNo(phoneNumber, countryCode)) {
                this.refs['loginScreen'].TL_PhoneNumber.setError(Strings.INVALID_MOBILE_NUMBER)
                this.refs['loginScreen'].PhoneNumberInput.focus()
                return
            }
        }
        Keyboard.dismiss();
        // let phoneNo = countryCode + phoneNumber
        let phoneNo = phoneNumber
        try {
            APICalls.sendOTP(phoneNo, this.onSuccessSentOTP, this.onFailureSendOTP, this.props)
        } catch (error) {
            Utility.log(error);
        }
        this.setState({ isLoading: true })
    }

    onSuccessSentOTP = (response) => {
        if (response)
            Utility.showToast(response.message)

        Utility.log('onSuccessSentOTP===> ', JSON.stringify(response))
        this.setState({ isLoading: false, enterOtpActive: true })
    }

    onFailureSendOTP = (response) => {
        if (response)
            Utility.showToast(response.message)

        Utility.log('onFailureSendOTP===> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    resendOTP = () => {
        this.refs['loginScreen'].setState({
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: ''
            // otp5: '',
            // otp6: ''
        });
        this.onLoginWithOTP(countryCode, mobileNum, true);
    }

    onOtpVerify = (otp1, otp2, otp3, otp4, otp5, otp6) => {
        Keyboard.dismiss();
        if (Utility.isValueNullOrEmpty(otp1 && otp2 && otp3 && otp4)) {
            Utility.showToast(Strings.ENTER_OTP_NUMBER)
            return true;
        } else {
            let otp = otp1 + otp2 + otp3 + otp4;
            try {
                Utility.sendEvent(AnalyticsConstants.LOGIN_WITH_OTP)
                APICalls.loginWithOTP(mobileNum, otp, this.onSuccessLogin, this.onFailureLogin, this.props);
            } catch (error) {
                Utility.log(error);
            }

            this.setState({ isLoading: true })
        }
    }

    onLoginWithEmail = async (email, password, token) => {
        Keyboard.dismiss();
        let params
        if (token == '') {
            Utility.removeItemValue(Constants.SFA_USER_DATA)
            if (Utility.isValueNullOrEmpty(email)) {
                this.refs['loginScreen'].TL_Email.setError(Strings.ENTER_USERNAME)
                this.refs['loginScreen'].EmailInput.focus()
            }
            else if (!Utility.validateEmail(email)) {
                this.refs['loginScreen'].TL_Email.setError(Strings.ENTER_EMAIL)
                this.refs['loginScreen'].EmailInput.focus()
            }
            else if (Utility.isValueNullOrEmpty(password)) {
                this.refs['loginScreen'].TL_Password.setError(Strings.ENTER_PASSWORD)
                this.refs['loginScreen'].PasswordInput.focus()
            }
            else {
                isTokenLogin = false
                params = {
                    email: email,
                    password: password,
                }
                if (Constants.APP_TYPE == Constants.INDONESIA) {
                    params["_with"] = ["contact"]
                }

                try {
                    APICalls.login(params, this.onSuccessLogin, this.onFailureLogin, this.props);
                } catch (error) {
                    Utility.log(error);
                }
                this.setState({ isLoading: true })
            }
        }
        else {
            isTokenLogin = true
            params = {
                ott: token,
            }
            if (Constants.APP_TYPE == Constants.INDONESIA) {
                params["_with"] = ["contact"]
            }
            try {
                APICalls.login(params, this.onSuccessLogin, this.onFailureLogin, this.props);
            } catch (error) {
                Utility.log(error);
            }
            this.setState({ isLoading: true })
        }
    }

    onSuccessLogin = async (response) => {

        Utility.log('onSuccessLogin===> ', JSON.stringify(response))
        // this.setState({ isLoading: false })

        // save Value To Async
        var storeObject = new AsyncStore()
        storeObject.saveValueInPersistStore(Constants.IS_LOGIN, '1')
        storeObject.saveValueInPersistStore(Constants.USER_ID, response.data.user_data.user_id.toString())
        storeObject.saveValueInPersistStore(Constants.NAME, response.data.user_data.name)
        storeObject.saveValueInPersistStore(Constants.EMAIL, response.data.user_data.email)
        storeObject.saveValueInPersistStore(Constants.PHONE_NO, response.data.user_data.mobile)
        storeObject.saveValueInPersistStore(Constants.USERNAME, response.data.user_data.user_name)
        storeObject.saveValueInPersistStore(Constants.ACCESS_TOKEN, response.data.token)
        storeObject.saveValueInPersistStore(Constants.DEALER_ID, response.data.dealer_data[0].dealer_id.toString())
        storeObject.saveValueInPersistStore(Constants.DEALER_ID_HASH, response.data.dealer_data[0].dealer_id_hash)
        storeObject.saveValueInPersistStore(Constants.DEALER_NAME, response.data.dealer_data[0].organization)
        storeObject.saveValueInPersistStore(Constants.DEALER_WHATSAPP_FINANCE_NO, response.data.dealer_data[0].contact ? response.data.dealer_data[0].contact.WHATSAPP_finance_enq : "")
        storeObject.saveValueInPersistStore(Constants.DEALER_ADDRESS, response.data.dealer_data[0].address)
        storeObject.saveValueInPersistStore(Constants.DEALER_LAT, response.data.dealer_data[0].latitude ? response.data.dealer_data[0].latitude.toString() : '0')
        storeObject.saveValueInPersistStore(Constants.DEALER_LANG, response.data.dealer_data[0].longitude ? response.data.dealer_data[0].longitude.toString() : '0')
        storeObject.saveValueInPersistStore(Constants.GCD_CODE, response.data.dealer_data[0].gcd_code)
        storeObject.saveValueInPersistStore(Constants.DEALER_CITY, response.data.dealer_data[0].city_ids[0].toString())
        // Set Country & Language
        storeObject.saveValueInPersistStore(Constants.LANGUAGE_ID, response.data.user_data.lang.toString())
        if (response.data.user_data.default_lang_code)
            storeObject.saveValueInPersistStore(Constants.LANGUAGE_CODE, response.data.user_data.default_lang_code)
        else
            storeObject.saveValueInPersistStore(Constants.LANGUAGE_CODE, 'en')
        if (response.data.user_data.country_code)
            storeObject.saveValueInPersistStore(Constants.COUNTRY_CODE, response.data.user_data.country_code)
        else
            storeObject.saveValueInPersistStore(Constants.COUNTRY_CODE, Config.APP_TYPE == 'Id' ? 'id' : 'ph')

        // Sync Locality to Local DB
        let param = {
            city: response.data.dealer_data[0].city_ids
        }

        // Get All Master(Config) Data...
        Utility.getMasterData().then((result) => {
            Utility.log('result===>', result)
            if (this.props.navigation.state.params.openPendingLoans) {
                // Land directly on pending loans screen
                this.props.navigation.replace('loanPendingLead', { initialRoute: true });
                Utility.retryLoanDocumentImageUpload();
            } else {
                // Jump to Dashboard Screen
                this.props.navigation.replace('drawerScreen', { initialRoute: true, isSyncCall: true })
            }
        });
        this.insertDealerLocality(param)
        this.updateFCMToken()

        if (isTokenLogin) {
            Utility.sendEvent(AnalyticsConstants.LOGIN_WITH_TOKEN, null);//------- login event
        } else {
            Utility.sendEvent(AnalyticsConstants.LOGIN_WITH_EMAIL, null);//------- login event
        }

        this.setState({ isLoading: false })
    }

    onFailureLogin = (response) => {
        if (response)
            Utility.showToast(response.message)

        Utility.log('onFailureLogin===> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    insertDealerLocality = (param) => {
        try {
            APICalls.getDealerLocality(param, this.onSuccessLocality, this.onFailureLocality, this.props)
        } catch (error) {
            Utility.log(error);
        }
    }

    onSuccessLocality = (response) => {
        Utility.log('onSuccessLocality===> ', JSON.stringify(response))
        // this.setState({ isLoading: false })

        // Check and Sync Locality To DB
        if (response.data && response.data.length > 0) {
            //let dbFunctions = new DBFunctions()

            this.dbFunctions.insertLocality(response.data).then((result) => {
                Utility.log('Locality insertion count', result)
            })
        }
        this.checkAndSyncMMV()
    }

    onFailureLocality = (response) => {
        Utility.log('onFailureLocality===> ', JSON.stringify(response))
        // this.setState({ isLoading: false })
        this.checkAndSyncMMV()
    }

    checkAndSyncMMV = () => {
        //let dbFunctions = new DBFunctions();
        // dbFunctions.getMMVTotalCount().then(count => {
        // if (count === 0) {
        // Utility.log('delete MMV', rs)
        APICalls.getMMV((response) => {
            if (response.data && response.data.version && response.data.version.length > 0) {
                this.dbFunctions.deleteMMV().then(rs => {
                    this.dbFunctions.insertMMV(response.data.version).then((result) => {
                        Utility.log('MMV insertion count', result);
                        this.checkAndSyncCities()
                    })
                })
            } else {
                this.checkAndSyncCities()
            }
        }, (error) => {
            this.checkAndSyncCities()
        })
    }

    checkAppType = () => {
        if (Constants.APP_TYPE == Constants.INDONESIA) {
            let page = 1;
            this.dbFunctions.deleteFinancierMMV().then(result => {
                this.dbFunctions.deleteMYP().then(result => {
                    this.checkAndSyncFinancierMMV(page)
                })
            })
        }
        else {
            this.checkAndSyncCities()
        }
    }

    checkAndSyncFinancierMMV = (page) => {
        APICalls.getFinancierMMV(page, (response) => {
            if (response.data && response.data.version && response.data.version.length > 0) {
                this.dbFunctions.insertFinancierMMV(response.data.version).then((result) => {
                    Utility.log('MMV insertion count', result)
                    this.dbFunctions.insertMYP(response.data.make_year_price).then((result) => {
                        Utility.log('MYP insertion count', result)

                        if (response.data.nextpage) {
                            let nextPage = page + 1;
                            this.checkAndSyncFinancierMMV(nextPage)
                        }
                        else {
                            // this.dbFunctions.getFinancierMMVTotalCount().then((result) => {
                            //     Utility.log('FinancierMMVTotalCount==>', result)
                            this.checkAndSyncCities()
                            // })
                        }
                    })
                })
            } else {
                this.checkAndSyncCities()
            }
        }, (error) => {
            Utility.log('FinancierMMVInsertionError==>', error)
            this.checkAndSyncCities()
        })
    }

    checkAndSyncCities = () => {
        //let dbFunctions = new DBFunctions();
        // dbFunctions.getCityCount().then(count => {

        // if (count === 0) {
        // Utility.log('delete City', rs)
        APICalls.getAllCity((response) => {
            if (response.data && response.data.city && response.data.city.length > 0) {
                this.dbFunctions.deleteCity().then(rs => {
                    this.dbFunctions.insertCity(response.data.city).then((result) => {
                        Utility.log('City insertion count', result);
                    })
                })
            }
        }, (error) => {

        })
    }

    updateFCMToken = () => {
        let device_id = '';
        try {
            let keys = [Constants.FCM_TOKEN, Constants.USER_ID];
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                device_id = DeviceInfo.getUniqueId()
                APICalls.updateFCMToken(values[1][1], values[0][1], device_id, this.onSuccessFCM, this.onFailureFCM, this.props)
            });
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccessFCM = (response) => {
        Utility.log('onSuccessFCM===> ', JSON.stringify(response))

    }

    onFailureFCM = (response) => {
        Utility.log('onFailureFCM===> ', JSON.stringify(response))
    }

    onForgotPasswordClick = () => {
        let screenState = this.refs['loginScreen'].state;
        this.props.navigation.navigate('forgotPassword', {
            email: screenState.email,
        })
    }

    backButtonAction = () => {
        this.refs['loginScreen'].setState({
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: ''
            // otp5: '',
            // otp6: ''
        });
        this.setState({ enterOtpActive: false });
    }
    gotoSignUp = () => {
        this.props.navigation.navigate("signUpScreen")
    }
    render() {
        return (
            <LoginScreen
                ref={'loginScreen'}
                isLoading={this.state.isLoading}
                loadingMsg={this.state.loadingMsg}
                hidePassword={this.state.hidePassword}
                enterOtpActive={this.state.enterOtpActive}
                loginByMobileOtp={this.state.loginByMobileOtp}
                resendOTP={this.resendOTP}
                onLoginWithOTP={this.onLoginWithOTP}
                changeOtpView={this.changeOtpView}
                changeLoginType={this.changeLoginType}
                onLoginWithEmail={this.onLoginWithEmail}
                hideAndShowPassword={this.hideAndShowPassword}
                onOtpVerifyAndSubmit={this.onOtpVerify}
                onForgotPasswordClick={this.onForgotPasswordClick}
                backButtonAction={this.backButtonAction}
                gotoSignUp={this.gotoSignUp}
                hidePassword={this.state.hidePassword}
            />
        )
    }
}
