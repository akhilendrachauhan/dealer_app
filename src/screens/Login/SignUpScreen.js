import React, { Component } from 'react'
import {
    Platform, StyleSheet, View, Text,TextInput,
    TouchableOpacity, KeyboardAvoidingView,SafeAreaView,ScrollView
} from 'react-native'
import TextInputLayout from "../../granulars/TextInputLayout"
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants } from '../../util'
import LoadingComponent from '../../components/LoadingComponent'
import { Dropdown } from 'react-native-material-dropdown'
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import * as APICalls from '../../api/APICalls'
export default class SignUpScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            contactName : '',
            dealerName : '',
            email : '',
            phoneNumber : '',
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            countryCodeData: [],
            showLoading : false
        }
    }

    async componentDidMount() {
        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeData: countryCodeData.length > 0 ? countryCodeData : Constants.COUNTRY_CODE_LIST })
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    clickOnSubmit = () => {
        if (Utility.isValueNullOrEmpty(this.state.contactName)) {
            this.TL_CONTACT_NAME.setError(Strings.ENTER_CONTACT_PERSON_NAME)
            this.contactNameInput.focus()
            return
        }
        // else if (Utility.isValueNullOrEmpty(this.state.dealerName)) {
        //     this.TL_DEALER_NAME.setError(Strings.ENTER_DEALER_NAME)
        //     this.dealerNameInput.focus()
        //     return
        // }
        else if (!Utility.validateEmail(this.state.email)) {
            this.TL_EMAIL.setError(Strings.ENTER_EMAIL)
            this.emailInput.focus()
            return
        }
        else if (!Utility.validateMobileNo(this.state.phoneNumber, this.state.countryCode)) {
            this.TL_PHONE.setError(Strings.INVALID_MOBILE_NUMBER)
            this.phoneInput.focus()
            return
        }
        else{
            try {
                APICalls.signUp(this.state.contactName,this.state.email,this.state.phoneNumber, this.onSuccess, this.onFailure, this.props)
            } catch (error) {
                Utility.log(error);
            }
            this.setState({ showLoading: true })
        }
    }

    onSuccess = (response) => {
        if (response)
            Utility.showAlert("",Strings.THANK_YOU_MSG_SIGNUP)
        this.onBackPress()
        Utility.log('onSuccess===> ', JSON.stringify(response))
        this.setState({ showLoading: false})
    }

    onFailure = (response) => {
        if (response)
            Utility.showToast(response.message)

        Utility.log('onFailure===> ', JSON.stringify(response))
        this.setState({ showLoading: false })
    }

    render() {
        let actionBarProps = {
            values: { title: Strings.JOIN_US },
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.onBackPress
            }
        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    {this.state.showLoading && <LoadingComponent />}
                    <ScrollView style={{ marginBottom: 30 }}
                        keyboardShouldPersistTaps='handled'>
                        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', margin: Dimens.margin_normal, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight }}>
                            <TextInputLayout
                                ref={(input) => this.TL_CONTACT_NAME = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.state.contactName ? this.state.contactName : ''}
                                    placeholder={Strings.CONATCT_NAME}
                                    ref={(input) => this.contactNameInput = input}
                                    onChangeText={(text) =>{ 
                                        this.setState({contactName : text})
                                    }}

                                />
                            </TextInputLayout>
                            {/* <TextInputLayout
                                ref={(input) => this.TL_DEALER_NAME = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.state.dealerName ? this.state.dealerName : ''}
                                    placeholder={Strings.DEALER_NAME}
                                    ref={(input) => this.dealerNameInput = input}
                                    onChangeText={(text) =>{ 
                                        this.setState({dealerName : text})
                                    }}

                                />
                            </TextInputLayout> */}
                            <TextInputLayout
                                ref={(input) => this.TL_EMAIL = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.state.email ? this.state.email : ''}
                                    placeholder={Strings.EMAIL+"*"}
                                    ref={(input) => this.emailInput = input}
                                    onChangeText={(text) =>{ 
                                        this.setState({email : text})
                                    }}

                                />
                            </TextInputLayout>
                            
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Dropdown
                                    containerStyle={{ flex: 0.4 }}
                                    dropdownOffset={{ top: 40, left: 0 }}
                                    baseColor={Colors.BLACK}
                                    textColor={Colors.BLACK}
                                    selectedItemColor={Colors.PRIMARY}
                                    data={this.state.countryCodeData}
                                    absoluteRTLLayout={true}
                                    value={this.state.countryCode}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ countryCode: value, selectedItem: data[index] })
                                    }}
                                />

                                <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_PHONE = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.PRIMARY}
                                        errorColorMargin={Dimens.margin_0}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={true}
                                            selectTextOnFocus={false}
                                            keyboardType={'number-pad'}
                                            placeholder={Strings.PHONE + "*"}
                                            ref={(input) => this.phoneInput = input}
                                            maxLength={Utility.maxLengthMobileNo(this.state.phoneNumber)}
                                            value={this.state.phoneNumber ? this.state.phoneNumber : ''}
                                            onChangeText={(text) => this.setState({ phoneNumber: text })}
                                        />
                                    </TextInputLayout>
                                </View>
                            </View>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={{ borderRadius: 5,width: '100%', padding: 15, marginTop: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                                onPress={() => this.clickOnSubmit()}>
                                <Text style={Styles.buttonText}>{Strings.SUBMIT}</Text>
                            </TouchableOpacity>
                        </KeyboardAvoidingView>

                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },

    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    topContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_x_large,
        //margin: Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed',
        justifyContent: 'space-between'
    },
    checkboxText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
});
