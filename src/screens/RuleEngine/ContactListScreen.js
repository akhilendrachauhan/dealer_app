import React, { Component } from 'react'
import {
  StyleSheet, View, Text, TouchableOpacity, TextInput, Image,
  PermissionsAndroid, FlatList, SafeAreaView, BackHandler
} from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import * as ImageAssets from '../../assets/ImageAssets'
import LoadingComponent from '../../components/LoadingComponent'
import * as Utility from "../../util/Utility";
import { _ActionBarStyle } from '../../values/Styles'
import Contacts from 'react-native-contacts';

export default class ContactListScreen extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      text: '',
      list: [],
    }

    this.contactsHolder = [];
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    if (Platform.OS === 'android') {
      this.requestContactPermission()
    }
    else {
      this.requestContactsPermissionIOS()
    }
  }

  requestContactsPermissionIOS = () => {
    Contacts.checkPermission((err, permission) => {
      //if (err) {throw err;

      // Contacts.PERMISSION_AUTHORIZED || Contacts.PERMISSION_UNDEFINED || Contacts.PERMISSION_DENIED
      if (permission === 'undefined') {
        Contacts.requestPermission((err, permission) => {
          if (permission === 'authorized') {
            Contacts.getAll((err, contacts) => {
              if (err === 'denied') {
                // error
              } else {
                // contacts returned in Array
                this.setState({ list: contacts, isLoading: false })
                this.contactsHolder = contacts
                // Utility.log('arraylist==>', this.contactsHolder)
                // Utility.log('listtt==>', this.state.list)
              }
            })
          }
          if (permission === 'denied') {
            Utility.showAlert("", Strings.ALLOW_CONTACT_PERMISSION)
            this.onBackPress();
          }
        })
      }
      if (permission === 'authorized') {
        Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
            this.onBackPress();
            Utility.showAlert("", Strings.ALLOW_CONTACT_PERMISSION)
          } else {
            // contacts returned in Array
            this.setState({ list: contacts, isLoading: false })
            this.contactsHolder = contacts
            //Utility.log('arraylist==>', this.contactsHolder)
            //Utility.log('listtt==>', this.state.list)
          }
        })
      }
      if (permission === 'denied') {
        Utility.showAlert("", Strings.ALLOW_CONTACT_PERMISSION)
        this.onBackPress();
      }
    })
  }

  requestContactPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: 'Contacts Permission',
          message: Strings.APP_NAME + ' App needs access to your contacts.',
          //buttonNeutral: "Ask Me Later",
          //buttonNegative: "Cancel",
          buttonPositive: Strings.OK
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
            // error
          } else {
            // contacts returned in Array
            this.setState({ list: contacts, isLoading: false })
            this.contactsHolder = contacts
            // Utility.log('arraylist==>', this.contactsHolder)
            // Utility.log('listtt==>', this.state.list)
          }
        })
      } else {
        this.onBackPress();
        //Utility.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }

  handleBackPress = () => {
    this.onBackPress();
    return true;
  }

  searchData(text) {
    let searchText = Utility.onlyNumeric(text);
    let contact;
    const filteredContacts = this.contactsHolder.filter(item => {
      if (item.phoneNumbers.length > 0) {
        // Utility.log('item==>', item.phoneNumbers[0].number)
        // Utility.log('hiiii', item.displayName)
        contact = item.phoneNumbers[0].number
      }

      return contact.indexOf(searchText) > -1
    })

    this.setState({ list: filteredContacts, text: Utility.onlyNumeric(text) })
  }

  onItemPress = (item) => {
    // console.log('hjkkkl', item.phoneNumbers)
    this.props.navigation.state.params.onSelectedMobileNumber(item)
    this.props.navigation.goBack()
  }

  onBackPress = () => {
    this.props.navigation.goBack()
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
        <View style={Styles.container}>

          <View style={Styles.containerHead}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={this.onBackPress}>

              <Image style={{ tintColor: Colors.WHITE, marginHorizontal: 20 }}
                source={ImageAssets.icon_cancel} />
            </TouchableOpacity>

            <Text style={Styles.headText}>{Strings.CONTACT}</Text>
          </View>

          <View style={{
            flexDirection: 'row', marginTop: 20, marginHorizontal: 20, padding: 5,
            borderColor: Colors.BLACK_25, borderWidth: 1, borderRadius: 5, justifyContent: 'space-between', alignItems: 'center'
          }}>
            <TextInput
              style={Styles.textInput}
              ref={(input) => this.PhoneNumberInput = input}
              selectTextOnFocus={false}
              returnKeyType='go'
              keyboardType='numeric'
              value={this.state.text}
              placeholder={Strings.SEARCH_BY_MOBILE_NO}
              onChangeText={(text) => this.searchData(text)}
            />
          </View>

          <FlatList
            style={{ margin: Dimens.margin_small, flex: 1 }}
            data={this.state.list}
            renderItem={({ item }) => (
              item.phoneNumbers.length > 0 ?
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => this.onItemPress(item)}>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={Styles.headerTagView}>

                      <Text style={Styles.headerLargeText}>{Platform.OS == 'android' ? (item.displayName ? Utility.getStrFirstLetter(item.displayName) : '') : (item.givenName ? Utility.getStrFirstLetter(item.givenName + " " + item.familyName) : '')}</Text>
                    </View>

                    <View style={{ minHeight: 40, justifyContent: 'flex-start', marginVertical: 15, marginLeft: 25 }}>

                      <Text style={Styles.contact_details}>{Platform.OS == 'android' ? item.displayName : item.givenName + " " + item.familyName}</Text>

                      <Text style={Styles.phones}>{item.phoneNumbers[0].number}</Text>
                    </View>
                  </View>
                  <View style={{ backgroundColor: Colors.BLACK_11, width: '100%', height: 1, marginLeft: 70 }}></View>
                </TouchableOpacity>
                : null
            )}
            numColumns={1}
            keyExtractor={(item, index) => index}
            extraData={this.state.list}
          />
        </View>

        {this.state.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
      </SafeAreaView>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE
  },
  containerHead: {
    flexDirection: 'row',
    height: 57,
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY
  },
  headText: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.WHITE,
    fontFamily: Strings.APP_FONT
  },
  textInput: {
    width: '95%',
    height: 40,
    fontSize: Dimens.text_large,
    color: Colors.BLACK,
    alignSelf: 'stretch',
  },
  searchInput: {
    marginHorizontal: 20,
    marginTop: 20,
    textAlign: 'center',
  },
  phones: {
    fontSize: 16,
    color: Colors.BLACK_50,
  },
  contact_details: {
    color: Colors.BLACK,
    fontSize: 16,
  },
  headerTagView: {
    width: 46, height: 46,
    borderRadius: 23,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.GRAY_LIGHT_BG
  },
  headerLargeText: {
    color: Colors.PRIMARY,
    fontWeight: 'bold',
    fontSize: Dimens.text_x_large,
    fontFamily: Strings.APP_FONT
  },
  contactText: {
    fontSize: Dimens.text_xxx_large,
    fontFamily: Strings.APP_FONT,
    fontWeight: 'normal',
  }
})

