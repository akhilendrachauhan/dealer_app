import React, { Component } from 'react'
import {
    StyleSheet, View, Text, ScrollView, TouchableOpacity, TextInput, Image, Keyboard, Modal,SafeAreaView,TouchableWithoutFeedback
} from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import * as ImageAssets from '../../assets/ImageAssets'
import LoadingComponent from '../../components/LoadingComponent'
import * as Utility from "../../util/Utility";
import TextInputLayout from "../../granulars/TextInputLayout";
import { icon_cancel } from './../../assets/ImageAssets'
import { _ActionBarStyle } from '../../values/Styles'
import { Dropdown } from 'react-native-material-dropdown'

export default class PaymentDetailScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            DP: '',
            loanNeeded: 0,
            selectedCard: 0,
            keyboardHeight: 0,
            inputHeight: 50,
        }
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }

    onCradPress = (value) => {
        if (this.state.selectedCard == value) {
            this.setState({ selectedCard: 0 })
        }
        else {
            // Utility.log('check==>', value, this.state.DP)
            if (value == 2 & this.state.DP == '') {
                Utility.showToast('Please enter Required DP %')
            }
            else {
                this.setState({ selectedCard: value })
                if (value == 1) {
                    this.setState({ loanNeeded: this.props.stateData.paymentDetail.loan_needed })
                }
                else {
                    this.setState({ loanNeeded: this.props.stateData.reqPaymentDetail.loan_needed })
                }
            }
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {this.props.stateData.paymentDetail ?
                        <View style={{ padding: Dimens.padding_xx_large, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text numberOfLines={1} style={Styles.headText}>{Strings.MIN_DP}</Text>
                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.dp_per ? (this.props.stateData.paymentDetail.dp_per + '%') : 0}</Text>
                            </View>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.onCradPress(1)}>

                                <View style={[Styles.cardContainer, { borderColor: this.state.selectedCard == 1 ? Colors.PRIMARY : Colors.WHITE }]}>
                                    <View style={[Styles.rowContainer, { marginTop: 0 }]}>
                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.MINDP}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.dp_amount ? Utility.formatCurrency(this.props.stateData.paymentDetail.dp_amount) : 0}</Text>
                                        </View>

                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.LTV_}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.ltv_per ? (this.props.stateData.paymentDetail.ltv_per + '%') : 0}</Text>
                                        </View>
                                    </View>

                                    <View style={Styles.rowContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.MIN_TOTAL_DP}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.total_dp ? Utility.formatCurrency(this.props.stateData.paymentDetail.total_dp) : 0}</Text>
                                        </View>

                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.INSTALLMENT_AMOUNT}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.installment ? Utility.formatCurrency(this.props.stateData.paymentDetail.installment) : 0}</Text>
                                        </View>
                                    </View>

                                    <View style={Styles.rowContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.NUMBER_OF_INSTALLMENT}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.tenure ? (this.props.stateData.paymentDetail.tenure - 1) : 0}</Text>
                                        </View>

                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.MAXIMUM_DISBURSEMENT}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.disbursement ? Utility.formatCurrency(this.props.stateData.paymentDetail.disbursement) : 0}</Text>
                                        </View>
                                    </View>

                                    <View style={Styles.rowContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.REFUND_TO_DEALER}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.actual_refund_to_dealer ? Utility.formatCurrency(this.props.stateData.paymentDetail.actual_refund_to_dealer) : 0}</Text>
                                        </View>

                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.BOOSTER_SCHEME}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.booster_scheme ? Utility.formatCurrency(this.props.stateData.paymentDetail.booster_scheme) : 0}</Text>
                                        </View>
                                    </View>

                                    <View style={Styles.rowContainer}>
                                        <View style={{ flex: 1 }}>
                                            <Text numberOfLines={1} style={Styles.headText}>{Strings.Total_Payment_To_Dealer}</Text>
                                            <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.paymentDetail.total_payment_to_dealer ? Utility.formatCurrency(this.props.stateData.paymentDetail.total_payment_to_dealer) : 0}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', marginTop: 25, alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text style={Styles.headText}>{Strings.PAYMENT_NEEDED_DEALER}</Text>

                                <View style={{ flex: 0.75, borderColor: Colors.BLACK_25, borderWidth: 1, borderRadius: 5 }}>
                                    <TextInput style={Styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='next'
                                        keyboardType='decimal-pad'
                                        editable={true}
                                        value={this.state.loanNeeded ? Utility.formatCurrency(this.state.loanNeeded) : ''}
                                        ref={(input) => this.LOANNEEDEDInput = input}
                                        placeholder={"0"}
                                        onChangeText={(text) => this.setState({ loanNeeded: Utility.onlyNumeric(text) })}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 16, alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text style={Styles.headText}>{Strings.REQUIRED_DP}</Text>

                                <View style={{ flex: 0.75, borderColor: Colors.BLACK_25, borderWidth: 1, borderRadius: 5 }}>
                                    <TextInput style={Styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='done'
                                        keyboardType='decimal-pad'
                                        editable={true}
                                        value={this.state.DP}
                                        ref={(input) => this.DPInput = input}
                                        placeholder={"Min DP " + this.props.stateData.paymentDetail.dp_per + '%'}
                                        onChangeText={(text) => this.setState({ DP: Utility.onlyDecimal(text) })}
                                        onSubmitEditing={() => this.props.onSubmitDP(this.state.DP, this.state.loanNeeded)}
                                    />
                                </View>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={{ width: 40, height: 40, backgroundColor: Colors.WHITE, borderRadius: 5, elevation: 6, alignItems: 'center', justifyContent: 'center' }}
                                    onPress={() => this.props.onSubmitDP(this.state.DP, this.state.loanNeeded)}>

                                    <Image source={ImageAssets.icon_right}
                                        style={{ width: 25, height: 25 }}
                                    />
                                </TouchableOpacity>
                            </View>

                            {this.props.stateData.reqPaymentDetail ?
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.onCradPress(2)}>

                                    <View style={[Styles.cardContainer, { borderColor: this.state.selectedCard == 2 ? Colors.PRIMARY : Colors.WHITE }]}>
                                        <View style={[Styles.rowContainer, { marginTop: 0 }]}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.DP}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.dp_amount ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.dp_amount) : 0}</Text>
                                            </View>

                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.LTV_}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.ltv_per ? (this.props.stateData.reqPaymentDetail.ltv_per + '%') : 0}</Text>
                                            </View>
                                        </View>

                                        <View style={Styles.rowContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.TOTAL_DP}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.total_dp ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.total_dp) : 0}</Text>
                                            </View>

                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.INSTALLMENT_AMOUNT}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.installment ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.installment) : 0}</Text>
                                            </View>
                                        </View>

                                        <View style={Styles.rowContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.NUMBER_OF_INSTALLMENT}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.tenure ? (this.props.stateData.reqPaymentDetail.tenure - 1) : 0}</Text>
                                            </View>

                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.DISBURSEMENT}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.disbursement ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.disbursement) : 0}</Text>
                                            </View>
                                        </View>

                                        <View style={Styles.rowContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.REFUND_TO_DEALER}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.actual_refund_to_dealer ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.actual_refund_to_dealer) : 0}</Text>
                                            </View>

                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.BOOSTER_SCHEME}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.booster_scheme ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.booster_scheme) : 0}</Text>
                                            </View>
                                        </View>

                                        <View style={Styles.rowContainer}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} style={Styles.headText}>{Strings.Total_Payment_To_Dealer}</Text>
                                                <Text numberOfLines={1} style={Styles.valueText}>{this.props.stateData.reqPaymentDetail.total_payment_to_dealer ? Utility.formatCurrency(this.props.stateData.reqPaymentDetail.total_payment_to_dealer) : 0}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                :
                                null
                            }
                        </View>
                        :
                        null
                    }
                </ScrollView>

                {!this.props.stateData.isLoanShareQuote ?
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={Styles.buttonStyle}
                        onPress={() => this.props.onNextPress(this.state.selectedCard, this.state.loanNeeded)}>

                        <Text style={Styles.buttonText}>{Strings.NEXT}</Text>
                    </TouchableOpacity>
                    :
                    null
                }

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.props.stateData.isModalVisible}
                    onDismiss={() => this.props.closeModal()}
                    onRequestClose={() => this.props.closeModal()}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)',justifyContent: 'flex-end', marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight : 0 }}>
                        <View style={Styles.modal}>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={() => this.props.closeModal()}>
                                    <Image source={icon_cancel}
                                        style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                                </TouchableOpacity>
                            </View>
                            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                            <View style={{ flexDirection: 'row', marginHorizontal: Dimens.margin_xx_large, alignItems: 'center', justifyContent: 'center' }}>
                                <Dropdown
                                    disabled={true}
                                    containerStyle={{ flex: 0.4 }}
                                    dropdownOffset={{ top: 43, left: 0 }}
                                    baseColor={Colors.BLACK}
                                    textColor={Colors.BLACK}
                                    selectedItemColor={Colors.BLACK}
                                    data={this.props.stateData.countryCodeData}
                                    value={this.props.stateData.countryCode}
                                    onChangeText={(value, index, data) => this.props.onChangeCountryCode(value)}
                                />

                                <View style={{ flex: 1, paddingLeft: Dimens.padding_xx_large }}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_PhoneNumber = input}
                                        showRightDrawable={true}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.BLACK}
                                        errorColor={Colors.PRIMARY}
                                        errorColorMargin={Dimens.margin_0}
                                        onRightClick={this.props.goToContactList}
                                        rightDrawable={ImageAssets.icon_contacts_logo}
                                        rightDrawableStyle={{ width: 24, height: 24, resizeMode: 'contain' }}
                                        rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                        <TextInput
                                            style={Styles.textInput}
                                            selectTextOnFocus={false}
                                            returnKeyType='go'
                                            keyboardType='numeric'
                                            editable={true}
                                            value={this.props.stateData.mobileNumber}
                                            ref={(input) => this.PhoneNumberInput = input}
                                            maxLength={Utility.maxLengthMobileNo(this.props.stateData.countryCode)}
                                            onSubmitEditing={() => this.props.loanShareMRPData(this.state.selectedCard)}
                                            placeholder={Strings.MOBILE_NUMBER}
                                            onChangeText={(text) => { this.props.onChangeMobileNo(text) }}
                                        />
                                    </TextInputLayout>
                                </View>
                            </View>
                            </TouchableWithoutFeedback>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={[Styles.buttonStyle, { marginTop: 20, borderRadius: 5 }]}
                                onPress={() => this.props.loanShareMRPData(this.state.selectedCard)}>

                                <Text style={Styles.buttonText}>{Strings.SEND}</Text>
                            </TouchableOpacity>
                        </View>
                        </SafeAreaView>
                </Modal>

                {this.props.stateData.isLoading ? <LoadingComponent msg={this.props.stateData.loadingMsg} /> : null}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
    },
    cardContainer: {
        padding: 16,
        marginTop: 16,
        elevation: 6,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: Colors.WHITE,
    },
    rowContainer: {
        marginTop: 25,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    valueText: {
        marginTop: 5,
        color: Colors.BLACK_40,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.PRIMARY
    },
    buttonText: {
        color: Colors.WHITE,
        fontWeight: 'bold',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    modal: {
        padding: Dimens.padding_xx_large,
        backgroundColor: Colors.WHITE,
    }
})