import React, { Component } from 'react';
import CalculatorFormScreen from './CalculatorFormScreen'
import DBFunctions from "../../database/DBFunctions";
import * as DBConstants from '../../database/DBConstants';
import { View, SafeAreaView, StyleSheet, BackHandler } from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import * as APICalls from '../../api/APICalls'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"

export default class CalculatorFormContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            configData: this.props.navigation.state.params.configData,
            financierItem: this.props.navigation.state.params.item,
            loanTenure: [],
            selectedMakeModel: undefined,
            versionList: [],
            mypData: [],
            selectedVersion: undefined,
            ruleEngineData: {},
            brandType: '',
            show_make: 0,
            max_tenure: 5,
            minMakeYear: ''
        }
        this.dbFunctions = new DBFunctions();
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_CALCULATOR_SCREEN)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.log('item==>', JSON.stringify(this.props.navigation.state.params.item))
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }

    // get make
    onMakePress = () => {
        this.refs['calculatorFormScreen'].TL_MAKE.setError('')
        this.props.navigation.navigate('searchContainer', {
            dbQuery: this.dbFunctions.getDistinctFinancierMakeQueryText(this.state.financierItem.id),
            hintText: Strings.SEARCH_BRAND,
            callback: this.onMakeSelected
        })
    }

    onMakeSelected = (data) => {
        Utility.log('SelectedFinancierMake==>', data)
        this.refs['calculatorFormScreen'].setState({
            make: data[DBConstants.FINANCIER_MAKE_NAME],
            makeId: data[DBConstants.FINANCIER_MAKE_ID],
            model: '', modelId: 0,
            variantName: '', variantId: 0,
            manufacturingYear: '', tenure: '',
            priceUppingId: 0, priceUpping: '',
            MRP: 0, olxMRP: 0, MRPWithUpping: 0,
        })
        this.setState({ brandType: data[DBConstants.FINANCIER_BRAND_TYPE] ? data[DBConstants.FINANCIER_BRAND_TYPE] : "" })
    }

    // get model
    onModelPress = (mk_id) => {
        Utility.log('makeId==>', mk_id)
        if (mk_id != 0) {
            this.refs['calculatorFormScreen'].TL_MODEL.setError('')
            this.props.navigation.navigate('searchContainer', {
                dbQuery: this.dbFunctions.getDistinctFinancierModelQueryText(mk_id, this.state.financierItem.id),
                hintText: Strings.SEARCH_MODEL,
                callback: this.onModelSelected
            })
        }
        else {
            Utility.showToast(Strings.SELECT_BRAND_FIRST)
        }
    }

    onModelSelected = (data) => {
        Utility.log('SelectedFinancierModel==>', data)
        this.refs['calculatorFormScreen'].setState({
            model: data[DBConstants.FINANCIER_MODEL_NAME],
            modelId: data[DBConstants.FINANCIER_MODEL_ID],
            variantName: '', variantId: 0,
            manufacturingYear: '', tenure: '',
            priceUppingId: 0, priceUpping: '',
            MRP: 0, olxMRP: 0, MRPWithUpping: 0,
        }, () => {
            this.getFinancierVersion(data[DBConstants.FINANCIER_MAKE_ID], data[DBConstants.FINANCIER_MODEL_ID])
        })
    }

    // get version for selected make model
    getFinancierVersion = (mk_id, md_id) => {
        this.dbFunctions.getFinancierDistinctVersion(mk_id, md_id).then(result => {
            // Utility.log('VersionResult==>', result)
            let financierVersion = []
            for (let i = 0; i < result.length; i++) {
                let element = result[i];
                let item = { 'id': element.v_id, 'value': element.v_n, 'v_t_id': element.v_t_id }
                financierVersion.push(item)
            }
            this.setState({ versionList: financierVersion })
        })
    }

    // get year & price for selected make model version
    getMYPByVersion = (versionId, aireaId, vehicleTypeId) => {
        // Utility.log('id==>', versionId, aireaId)
        let brandType = this.state.brandType
        let curYear = new Date().getFullYear()
        let show_make = 0

        if (this.state.financierItem.make_tenure_cond.length > 0) {
            for (let j = 0; j < this.state.financierItem.make_tenure_cond.length; j++) {
                let element = this.state.financierItem.make_tenure_cond[j];
                if (brandType == element.b_type) {
                    show_make = element.show_make
                    this.setState({ show_make: element.show_make, max_tenure: 5 })
                }
            }
        } else {
            for (let j = 0; j < this.state.financierItem.vehicle_make_tenure_cond.length; j++) {
                let element = this.state.financierItem.vehicle_make_tenure_cond[j];
                if (vehicleTypeId == element.v_type) {
                    show_make = element.show_make
                    this.setState({ show_make: element.show_make, max_tenure: element.max_tenure })
                }
            }
        }
        //Utility.log('curYear==>', curYear, show_make, brandType)

        this.dbFunctions.getMYPByVersion(versionId).then(result => {
            // Utility.log('mypResult==>', result)
            let mypData = []
            for (let i = 0; i < result.length; i++) {
                let element = result[i];
                if ((element[DBConstants.MYP_AIREA_ID]) == 0) { // If airea id is not required...
                    if (this.state.financierItem.id == 4 && element.mk_year > 2010) { // For MAF Year start from 2011
                        let item = { 'id': i, 'value': element.mk_year, 'year': element.mk_year, 'price': element.price, 'a_id': element.a_id }
                        mypData.push(item)
                    }
                    else if (element.mk_year >= curYear - show_make) {
                        let item = { 'id': i, 'value': element.mk_year, 'year': element.mk_year, 'price': element.price, 'a_id': element.a_id }
                        mypData.push(item)
                    }
                }
                else if ((element[DBConstants.MYP_AIREA_ID]) == aireaId) { // If airea id is required...
                    if (this.state.financierItem.id == 4 && element.mk_year > 2010) { // For MAF Year start from 2011
                        let item = { 'id': i, 'value': element.mk_year, 'year': element.mk_year, 'price': element.price, 'a_id': element.a_id }
                        mypData.push(item)
                    }
                    else if (element.mk_year >= curYear - show_make) {
                        let item = { 'id': i, 'value': element.mk_year, 'year': element.mk_year, 'price': element.price, 'a_id': element.a_id }
                        mypData.push(item)
                    }
                }
            }
            this.setState({ minMakeYear: mypData[0].year, mypData: mypData.reverse() })
        })
    }

    getTenureData = (makeYear, vehicleTypeId) => {
        // Utility.log('make=======>', makeYear, this.state.minMakeYear)
        let curYear = new Date().getFullYear()
        let show_make = this.state.show_make
        let maxTenure = this.state.max_tenure
        let tenure = ((show_make + 1) - (curYear - makeYear))
        if (this.state.financierItem.id == 4) {  // For MAF Tenure will be 5 for all Years
            tenure = 5
        }
        else if (this.state.financierItem.id == 3 && vehicleTypeId == 6 && this.state.minMakeYear == makeYear) { // For MUF Tenure will be 3 for Min Years if v_type is "Commercial"
            tenure = 3
        }
        else if (this.state.financierItem.id == 3 && vehicleTypeId == 6 && this.state.minMakeYear < makeYear) { // For MUF Tenure will be 4 for rest Years if v_type is "Commercial"
            tenure = maxTenure
        }
        else if (tenure > maxTenure) {
            tenure = maxTenure
        }
        Utility.log('curYear==>', curYear, show_make, tenure)

        // Convert Tenure Month data to Years
        let loanTenure = []
        for (let i = 0; i < this.state.configData.loan_tenure.length; i++) {
            let element = this.state.configData.loan_tenure[i];
            if ((element / 12) <= tenure) {
                let item = { 'id': i, 'value': (element / 12) }
                loanTenure.push(item)
            }
        }
        this.setState({ loanTenure: loanTenure })
    }

    onNextPress = (customerTypeId, branchRegionId, plateNumberRegionId, jakartaPlateNumberId, vehicleTypeId,
        makeId, modelId, variantId, manufacturingYear, insuranceTypeId, tenure, MRP, olxMRP, priceUppingId, MRPWithUpping) => {

        if (this.state.financierItem.customer_type.length > 0 && Utility.isValueNullOrEmpty(customerTypeId.toString())) {
            this.refs['calculatorFormScreen'].TL_CUSTOMER_TYPE.setError(Strings.SELECT_CUSTOMER_TYPE)
            // this.refs['calculatorFormScreen'].CUSTOMER_TYPE_INPUT.focus()
            return
        }
        else if (this.state.financierItem.customer_area.length > 0 && Utility.isValueNullOrEmpty(branchRegionId.toString())) {
            this.refs['calculatorFormScreen'].TL_CUSTOMER_BRANCH_REGION.setError(Strings.SELECT_CUSTOMER_REGION)
            // this.refs['calculatorFormScreen'].CUSTOMER_BRANCH_REGION_INPUT.focus()
            return
        }
        else if (this.state.financierItem.plate_area.length > 0 && Utility.isValueNullOrEmpty(plateNumberRegionId.toString())) {
            this.refs['calculatorFormScreen'].TL_PLATE_NO_REGION.setError(Strings.SELECT_PLATE_NO_REGION)
            // this.refs['calculatorFormScreen'].PLATE_NO_REGION_INPUT.focus()
            return
        }
        else if (this.state.financierItem.jakarta_plate_no.length > 0 && Utility.isValueNullOrEmpty(jakartaPlateNumberId.toString())) {
            this.refs['calculatorFormScreen'].TL_JAKARTA_PLATE_NO.setError(Strings.SELECT_JAKARTA_PLATE_NO)
            // this.refs['calculatorFormScreen'].JAKARTA_PLATE_NO_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(makeId.toString())) {
            this.refs['calculatorFormScreen'].TL_MAKE.setError(Strings.SELECT_BRAND)
            // this.refs['calculatorFormScreen'].MAKE_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(modelId.toString())) {
            this.refs['calculatorFormScreen'].TL_MODEL.setError(Strings.SELECT_MODEL)
            // this.refs['calculatorFormScreen'].MODEL_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(variantId.toString())) {
            this.refs['calculatorFormScreen'].TL_VERSION.setError(Strings.SELECT_VERSION)
            // this.refs['calculatorFormScreen'].VERSION_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(vehicleTypeId.toString())) {
            this.refs['calculatorFormScreen'].TL_VEHICLE_TYPE.setError(Strings.SELECT_VEHICLE_TYPE)
            // this.refs['calculatorFormScreen'].VEHICLE_TYPE_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(manufacturingYear.toString())) {
            this.refs['calculatorFormScreen'].TL_MANUFACTURING_YEAR.setError(Strings.SELECT_MANUF_YEAR)
            // this.refs['calculatorFormScreen'].MANUFACTURING_YEAR_INPUT.focus()
            return
        }
        else if (this.state.financierItem.insurance_type.length > 0 && Utility.isValueNullOrEmpty(insuranceTypeId.toString())) {
            this.refs['calculatorFormScreen'].TL_INSURANCE_TYPE.setError(Strings.SELECT_INSURANCE_TYPE)
            // this.refs['calculatorFormScreen'].INSURANCE_TYPE_INPUT.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(tenure.toString())) {
            this.refs['calculatorFormScreen'].TL_TENURE_YEAR.setError(Strings.SELECT_TENURE)
            // this.refs['calculatorFormScreen'].TENURE_YEAR_INPUT.focus()
            return
        }
        else if (!Utility.isValueNullOrEmpty(manufacturingYear.toString()) && MRP == 0 && Utility.isValueNullOrEmpty(olxMRP.toString())) {
            this.refs['calculatorFormScreen'].TL_OLX_MRP.setError(Strings.ENTER_OLX_MRP)
            // this.refs['calculatorFormScreen'].OLX_MRP_INPUT.focus()
            return
        }
        else if (!Utility.isValueNullOrEmpty(olxMRP.toString()) && (parseInt(olxMRP) < parseInt(this.state.financierItem.non_mrp_min_price))) {
            this.refs['calculatorFormScreen'].TL_OLX_MRP.setError(Strings.OLX_MRP_GREATER + Utility.formatCurrency(this.state.financierItem.non_mrp_min_price))
            // this.refs['calculatorFormScreen'].OLX_MRP_INPUT.focus()
            return
        }
        // else if (Utility.isValueNullOrEmpty(priceUppingId.toString())) {
        //     this.refs['calculatorFormScreen'].TL_PRICE_UPPING.setError(Strings.SELECT_PRICE_UPPING)
        //     // this.refs['calculatorFormScreen'].PRICE_UPPING_INPUT.focus()
        //     return
        // }
        else {
            let ruleEngineData = {
                customerTypeId: customerTypeId,
                branchRegionId: branchRegionId,
                plateNumberRegionId: plateNumberRegionId,
                jakartaPlateNumberId: jakartaPlateNumberId,
                vehicleTypeId: vehicleTypeId,
                makeId: makeId,
                modelId: modelId,
                variantId: variantId,
                manufacturingYear: manufacturingYear,
                insuranceTypeId: insuranceTypeId,
                tenure: tenure,
                MRP: MRP,
                olxMRP: olxMRP,
                priceUppingId: priceUppingId,
                MRPWithUpping: MRPWithUpping
            }
            Utility.log('ruleEngineData==>', JSON.stringify(ruleEngineData))

            try {
                let keys = [Constants.DEALER_ID, Constants.USER_ID, Constants.SFA_USER_DATA]
                const promise = Utility.getMultipleValuesFromAsyncStorage(keys)
                promise.then(values => {
                    let sfa = values[2][1] ? JSON.parse(values[2][1]) : null
                    APICalls.calculateLoan(values[0][1], values[1][1], sfa, ruleEngineData, this.state.financierItem, 0, 0, this.onSuccess, this.onFailure, this.props);
                })
                this.setState({ isLoading: true, ruleEngineData: ruleEngineData })
            } catch (error) {
                Utility.log(error)
            }
        }
    }

    onSuccess = (response) => {
        Utility.log('onSuccessLoan==> ', JSON.stringify(response))
        this.setState({ isLoading: false }, () => {
            Utility.sendEvent(AnalyticsConstants.LOAN_CALCULATION_SUBMIT)
            this.props.navigation.navigate("paymentDetail", {
                paymentDetail: response.data,
                ruleEngineData: this.state.ruleEngineData,
                configData: this.state.configData,
                financierItem: this.state.financierItem,
                stockItem: this.props.navigation.state.params.stockItem,
                isLoanShareQuote: this.props.navigation.state.params.isLoanShareQuote,
            })
        })
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        Utility.log('onFailureLoan==> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    onBackPress = () => {
        Utility.confirmDialog(Strings.ALERT, Strings.GO_BACK_FROM_LOAN, Strings.CANCEL, Strings.YES, this.confirmCallback)
    }

    confirmCallback = (status) => {
        if (status) {
            this.props.navigation.goBack()
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: this.state.financierItem.name }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <CalculatorFormScreen
                        ref={'calculatorFormScreen'}
                        stateData={this.state}
                        onMakePress={this.onMakePress}
                        onModelPress={this.onModelPress}
                        onNextPress={this.onNextPress}
                        getMYPByVersion={this.getMYPByVersion}
                        getTenureData={this.getTenureData}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})