import React, { Component } from 'react'
import {
    StyleSheet, View, Text, TextInput, ScrollView, TouchableOpacity, Platform, Keyboard
} from 'react-native'
import TextInputLayout from "../../granulars/TextInputLayout"
import { Strings, Colors, Dimens } from '../../values'
import { Utility } from '../../util'
import LoadingComponent from '../../components/LoadingComponent'
import { Dropdown } from 'react-native-material-dropdown'

export default class CalculatorFormScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            customerType: '',
            customerTypeId: 0,
            branchRegion: '',
            branchRegionId: 0,
            plateNumberRegion: '',
            plateNumberRegionId: 0,
            jakartaPlateNumber: '',
            jakartaPlateNumberId: 0,
            vehicleType: this.props.stateData.financierItem.vehicle_type.length > 1 ? '' : this.props.stateData.financierItem.vehicle_type[0].value,
            vehicleTypeId: this.props.stateData.financierItem.vehicle_type.length > 1 ? 0 : this.props.stateData.financierItem.vehicle_type[0].id,
            make: '',
            makeId: 0,
            model: '',
            modelId: 0,
            variantName: '',
            variantId: 0,
            manufacturingYear: '',
            insuranceType: '',
            insuranceTypeId: 0,
            tenure: '',
            priceUpping: '',
            priceUppingId: 0,
            MRP: 0,
            olxMRP: 0,
            MRPWithUpping: 0,
            keyboardHeight: 0,
            inputHeight: 50,
        }
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }
    calculatePriceUpping = (value) => {
        let MRP = this.state.MRP != 0 ? this.state.MRP : this.state.olxMRP
        if (MRP != 0) {
            let uppingMRP = parseFloat(parseFloat(MRP) + parseFloat((MRP * value) / 100)).toFixed(2)
            this.setState({ MRPWithUpping: uppingMRP })
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ padding: Dimens.padding_xx_large, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight }}>
                        {this.props.stateData.financierItem.customer_type.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_CUSTOMER_TYPE = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.CUSTOMER_TYPE_INPUT = input}
                                    placeholder={Strings.CUSTOMER_TYPE + '*'}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.customer_type}
                                    absoluteRTLLayout={true}
                                    value={this.state.customerType}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ customerType: value, customerTypeId: data[index].id })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        {this.props.stateData.financierItem.customer_area.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_CUSTOMER_BRANCH_REGION = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.CUSTOMER_BRANCH_REGION_INPUT = input}
                                    placeholder={Strings.CUSTOMER_BRANCH_REGION + '*'}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.customer_area}
                                    absoluteRTLLayout={true}
                                    value={this.state.branchRegion}
                                    onChangeText={(value, index, data) => {
                                        this.setState({
                                            branchRegion: value, branchRegionId: data[index].id,
                                            // make: '', makeId: 0,
                                            // model: '', modelId: 0,
                                            // variantName: '', variantId: 0,
                                            manufacturingYear: '', tenure: '',
                                            priceUppingId: 0, priceUpping: '',
                                            MRP: 0, olxMRP: 0, MRPWithUpping: 0,
                                        }, () => {
                                            if (this.state.variantId != 0 && this.state.vehicleTypeId != 0) {
                                                this.props.getMYPByVersion(this.state.variantId, data[index].id, this.state.vehicleTypeId)
                                            }
                                        })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        {this.props.stateData.financierItem.plate_area.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_PLATE_NO_REGION = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.PLATE_NO_REGION_INPUT = input}
                                    placeholder={Strings.PLATE_NO_REGION + '*'}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.plate_area}
                                    absoluteRTLLayout={true}
                                    value={this.state.plateNumberRegion}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ plateNumberRegion: value, plateNumberRegionId: data[index].id })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        {this.props.stateData.financierItem.jakarta_plate_no.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_JAKARTA_PLATE_NO = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.JAKARTA_PLATE_NO_INPUT = input}
                                    placeholder={Strings.JAKARTA_PLATE_NO + '*'}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.jakarta_plate_no}
                                    absoluteRTLLayout={true}
                                    value={this.state.jakartaPlateNumber}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ jakartaPlateNumber: value, jakartaPlateNumberId: data[index].id })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={{ marginTop: -3, marginBottom: 5 }}
                            onPress={() => this.props.onMakePress()}>
                            <TextInputLayout
                                ref={(input) => this.TL_MAKE = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.state.make}
                                    placeholder={Strings.BRAND + "*"}
                                    ref={(input) => this.MAKE_INPUT = input}
                                    onTouchStart={() => Platform.OS === 'ios' ? this.props.onMakePress() : Utility.log("Pressed...")}
                                />
                            </TextInputLayout>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={{ marginTop: -3, marginBottom: 5 }}
                            onPress={() => this.props.onModelPress(this.state.makeId)}>
                            <TextInputLayout
                                ref={(input) => this.TL_MODEL = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.state.model}
                                    placeholder={Strings.MODEL + "*"}
                                    ref={(input) => this.MODEL_INPUT = input}
                                    onTouchStart={() => Platform.OS === 'ios' ? this.props.onModelPress(this.state.makeId) : Utility.log("Pressed...")}
                                />
                            </TextInputLayout>
                        </TouchableOpacity>

                        <TextInputLayout
                            ref={(input) => this.TL_VERSION = input}
                            isBorderBottomWidth={false}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_0}>

                            <Dropdown
                                style={Styles.dropdownContainer}
                                ref={(input) => this.VERSION_INPUT = input}
                                placeholder={Strings.VERSION + '*'}
                                dropdownOffset={{ top: 0, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.stateData.versionList}
                                absoluteRTLLayout={true}
                                value={this.state.variantName}
                                onChangeText={(value, index, data) => {
                                    for (let i = 0; i < this.props.stateData.financierItem.vehicle_type.length; i++) {
                                        let element = this.props.stateData.financierItem.vehicle_type[i];
                                        if (element.id == data[index].v_t_id) {
                                            this.setState({ vehicleType: element.name, vehicleTypeId: element.id })
                                        }
                                    }
                                    this.setState({
                                        variantName: value,
                                        variantId: data[index].id,
                                        manufacturingYear: '',
                                        priceUppingId: 0, priceUpping: '',
                                        MRP: 0, olxMRP: 0, MRPWithUpping: 0, tenure: ''
                                    }, () => {
                                        this.props.getMYPByVersion(data[index].id, this.state.branchRegionId, this.state.vehicleTypeId)
                                    })
                                }}>
                            </Dropdown>
                        </TextInputLayout>

                        <TextInputLayout
                            ref={(input) => this.TL_VEHICLE_TYPE = input}
                            isBorderBottomWidth={false}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_0}>

                            <Dropdown
                                style={Styles.dropdownContainer}
                                ref={(input) => this.VEHICLE_TYPE_INPUT = input}
                                placeholder={Strings.OBJECT + '*'}
                                dropdownOffset={{ top: 0, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.stateData.financierItem.vehicle_type}
                                absoluteRTLLayout={true}
                                value={this.state.vehicleType}
                                onChangeText={(value, index, data) => {
                                    this.setState({
                                        vehicleType: value, vehicleTypeId: data[index].id,
                                        manufacturingYear: '',
                                        priceUppingId: 0, priceUpping: '',
                                        MRP: 0, olxMRP: 0, MRPWithUpping: 0, tenure: ''
                                    }, () => {
                                        this.props.getMYPByVersion(this.state.variantId, this.state.branchRegionId, data[index].id)
                                    })
                                }}>
                            </Dropdown>
                        </TextInputLayout>

                        <TextInputLayout
                            ref={(input) => this.TL_MANUFACTURING_YEAR = input}
                            isBorderBottomWidth={false}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_0}>

                            <Dropdown
                                style={Styles.dropdownContainer}
                                ref={(input) => this.MANUFACTURING_YEAR_INPUT = input}
                                placeholder={Strings.MANUFACTURING_YEAR + '*'}
                                dropdownOffset={{ top: 0, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.stateData.mypData}
                                absoluteRTLLayout={true}
                                value={this.state.manufacturingYear}
                                onChangeText={(value, index, data) => {
                                    Utility.log('selectedYear==>', data[index])
                                    this.setState({
                                        manufacturingYear: value, MRP: data[index].price,
                                        priceUppingId: 0, priceUpping: '',
                                        olxMRP: 0, MRPWithUpping: data[index].price, tenure: ''
                                    }, () => {
                                        this.props.getTenureData(value, this.state.vehicleTypeId)
                                    })
                                }}>
                            </Dropdown>
                        </TextInputLayout>

                        {this.props.stateData.financierItem.insurance_type.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_INSURANCE_TYPE = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.INSURANCE_TYPE_INPUT = input}
                                    placeholder={Strings.INSURANCE_TYPE + '*'}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.insurance_type}
                                    absoluteRTLLayout={true}
                                    value={this.state.insuranceType}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ insuranceType: value, insuranceTypeId: data[index].id })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        <TextInputLayout
                            ref={(input) => this.TL_TENURE_YEAR = input}
                            isBorderBottomWidth={false}
                            focusColor={Colors.ORANGE}
                            hintColor={Colors.GRAY}
                            errorColor={Colors.PRIMARY}
                            errorColorMargin={Dimens.margin_0}>

                            <Dropdown
                                style={Styles.dropdownContainer}
                                ref={(input) => this.TENURE_YEAR_INPUT = input}
                                placeholder={Strings.TENURE_YEAR + '*'}
                                dropdownOffset={{ top: 0, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.props.stateData.loanTenure}
                                absoluteRTLLayout={true}
                                value={this.state.tenure}
                                onChangeText={(value, index, data) => {
                                    this.setState({ tenure: value })
                                }}>
                            </Dropdown>
                        </TextInputLayout>

                        {this.state.manufacturingYear != '' ?
                            <View style={Styles.rowContainer}>
                                <Text style={Styles.rowHeadText}>{Strings.MRP}</Text>
                                <Text style={Styles.MRPText}>{this.state.MRP == 0 ? Strings.NON_MRP : Utility.formatCurrency(this.state.MRP)}</Text>
                            </View>
                            :
                            null
                        }

                        {this.state.manufacturingYear != '' && this.state.MRP == 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_OLX_MRP = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}>

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    keyboardType='number-pad'
                                    selectTextOnFocus={false}
                                    value={this.state.olxMRP ? Utility.formatCurrency(this.state.olxMRP) : ''}
                                    placeholder={Strings.OLX_MRP + "*"}
                                    ref={(input) => this.OLX_MRP_INPUT = input}
                                    onSubmitEditing={() => { }}
                                    onChangeText={(text) => this.setState({
                                        olxMRP: Utility.onlyNumeric(text),
                                        MRPWithUpping: Utility.onlyNumeric(text),
                                        priceUppingId: 0, priceUpping: ''
                                    })}
                                />
                            </TextInputLayout>
                            :
                            null
                        }

                        {this.props.stateData.financierItem.price_upping.length > 0 ?
                            <TextInputLayout
                                ref={(input) => this.TL_PRICE_UPPING = input}
                                isBorderBottomWidth={false}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}>

                                <Dropdown
                                    style={Styles.dropdownContainer}
                                    ref={(input) => this.PRICE_UPPING_INPUT = input}
                                    placeholder={Strings.PRICE_UPPING}
                                    dropdownOffset={{ top: 0, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.financierItem.price_upping}
                                    absoluteRTLLayout={true}
                                    value={this.state.priceUpping}
                                    onChangeText={(value, index, data) => {
                                        this.setState({ priceUpping: value.toString(), priceUppingId: data[index].id }, () => {
                                            this.calculatePriceUpping(value)
                                        })
                                    }}>
                                </Dropdown>
                            </TextInputLayout>
                            :
                            null
                        }

                        {(this.state.MRP != 0 || this.state.olxMRP != 0) && this.props.stateData.financierItem.price_upping.length > 0 ?
                            <View style={Styles.rowContainer}>
                                <Text style={Styles.rowHeadText}>{Strings.MRP_UPPING}</Text>
                                <Text style={Styles.MRPText}>{Utility.formatCurrency(this.state.MRPWithUpping)}</Text>
                            </View>
                            :
                            null
                        }
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={Styles.buttonStyle}
                        onPress={() => this.props.onNextPress(this.state.customerTypeId, this.state.branchRegionId, this.state.plateNumberRegionId,
                            this.state.jakartaPlateNumberId, this.state.vehicleTypeId, this.state.makeId, this.state.modelId, this.state.variantId, this.state.manufacturingYear,
                            this.state.insuranceTypeId, this.state.tenure, this.state.MRP, this.state.olxMRP, this.state.priceUppingId, this.state.MRPWithUpping)}>

                        <Text style={Styles.buttonText}>{Strings.NEXT}</Text>
                    </TouchableOpacity>
                </ScrollView>

                {this.props.stateData.isLoading ? <LoadingComponent msg={this.props.stateData.loadingMsg} /> : null}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    headText: {
        fontSize: Dimens.text_large,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    textInput: {
        height: 30,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch'
    },
    dropdownContainer: {
        height: 25,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    buttonStyle: {
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.PRIMARY
    },
    buttonText: {
        color: Colors.WHITE,
        fontWeight: 'bold',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    rowContainer: {
        marginTop: 20,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rowHeadText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    MRPText: {
        color: Colors.BLACK_85,
        fontWeight: 'bold',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    }
});