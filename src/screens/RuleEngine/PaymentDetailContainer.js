import React, { Component } from 'react';
import PaymentDetailScreen from './PaymentDetailScreen'
import { View, SafeAreaView, StyleSheet, BackHandler} from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { Utility, Constants, AnalyticsConstants } from '../../util'
import * as APICalls from '../../api/APICalls'
import { Linking } from "react-native";

export default class PaymentDetailContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            mobileNumber: '',
            isModalVisible: false,
            isLoading: false,
            loadingMsg: '',
            configData: this.props.navigation.state.params.configData,
            financierItem: this.props.navigation.state.params.financierItem,
            ruleEngineData: this.props.navigation.state.params.ruleEngineData,
            paymentDetail: this.props.navigation.state.params.paymentDetail,
            isLoanShareQuote: this.props.navigation.state.params.isLoanShareQuote,
            reqPaymentDetail: undefined,
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            countryCodeData: []
        }
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_PAYMENT_SCREEN)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)

        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeData: countryCodeData.length > 0 ? countryCodeData : Constants.COUNTRY_CODE_LIST })

        this.refs['paymentDetailScreen'].setState({
            DP: (this.state.paymentDetail.dp_per).toString(),
            loanNeeded: this.state.paymentDetail.loan_needed,
            selectedCard: 1
        })
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }

    calculateLoan = (reqDP, loanNeeded) => {
        // if (!loanNeeded)
        //     loanNeeded = 0
        // Utility.log('loanNeeded==>', loanNeeded, this.state.paymentDetail.loan_needed)
        // if (loanNeeded == this.state.paymentDetail.loan_needed)
        //     loanNeeded = 0

        let loan_needed = 0
        try {
            let keys = [Constants.DEALER_ID, Constants.USER_ID, Constants.SFA_USER_DATA];
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                let sfa = values[2][1] ? JSON.parse(values[2][1]) : null
                APICalls.calculateLoan(values[0][1], values[1][1], sfa, this.state.ruleEngineData, this.state.financierItem, reqDP, loan_needed, this.onSuccess, this.onFailure, this.props);
            })
            this.setState({ isLoading: true })
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = (response) => {
        Utility.log('onSuccessLoan==> ', JSON.stringify(response))
        this.setState({ isLoading: false, reqPaymentDetail: response.data })
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        Utility.log('onFailureLoan==> ', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    onSubmitDP = (dp, loanNeeded) => {
        // Utility.log('RequiredDP===>', dp, this.state.paymentDetail.dp_per)
        if (dp != '') {
            if (dp.match(/^(\d+)?([,]?\d{0,2})?$/)) {
                dp = dp.replace(",",".")
                if (parseFloat(dp) <= parseFloat(this.state.paymentDetail.dp_per)) {
                    Utility.showToast(Strings.REQ_DP_GREATER_THAN_MIN_DP)
                }
                else if (parseFloat(dp) > 100) {
                    Utility.showToast(Strings.REQ_DP_NOT_GREATER_THAN_100)
                }
                else {
                    this.calculateLoan(dp, loanNeeded)
                }
            } else {
                Utility.showToast(Strings.ENTER_VALID_REQ_DP)
            }
            
        }
        else {
            Utility.showToast(Strings.ENTER_REQ_DP)
        }
    }

    onNextPress = (selectedCard, loanNeeded) => {
        // Utility.log('loan_needed==>', loanNeeded)
        if (selectedCard == 0) {
            Utility.showToast(Strings.SELECT_ANY_CARD)
        }
        else if (selectedCard == 1) {
            Utility.sendEvent(AnalyticsConstants.LOAN_PAYMENT_SUBMIT)
            let newPaymentDetail = this.state.paymentDetail
            newPaymentDetail.updatedLoanNeeded = loanNeeded
            this.setState({ paymentDetail: newPaymentDetail }, () => {
                this.props.navigation.navigate("customerDetail", {
                    approvedQuoteData: null, //approvedQuoteData,
                    item: this.props.navigation.state.params.financierItem,
                    stockItem: this.props.navigation.state.params.stockItem,
                    configData: this.props.navigation.state.params.configData,
                    ruleEngineData: this.state.paymentDetail
                })
            })
        }
        else {
            Utility.sendEvent(AnalyticsConstants.LOAN_PAYMENT_SUBMIT)
            let newPaymentDetail = this.state.reqPaymentDetail
            newPaymentDetail.updatedLoanNeeded = loanNeeded
            this.setState({ reqPaymentDetail: newPaymentDetail }, () => {
                this.props.navigation.navigate("customerDetail", {
                    approvedQuoteData: null, //approvedQuoteData,
                    item: this.props.navigation.state.params.financierItem,
                    stockItem: this.props.navigation.state.params.stockItem,
                    configData: this.props.navigation.state.params.configData,
                    ruleEngineData: this.state.reqPaymentDetail
                })
            })
        }
    }

    onBackPress = () => {
        // this.props.navigation.goBack()
        Utility.confirmDialog(Strings.ALERT, Strings.GO_BACK_FROM_LOAN, Strings.CANCEL, Strings.YES, this.confirmCallback)
    }

    confirmCallback = (status) => {
        if (status) {
            this.props.navigation.goBack()
        }
    }

    closeModal = () => {
        this.setState({ isModalVisible: false, mobileNumber: '' })
    }

    openModal = () => {
        if (this.refs['paymentDetailScreen'].state.selectedCard == 0) {
            Utility.showToast(Strings.SELECT_ANY_CARD)
        }else{
            let keys = [Constants.DEALER_WHATSAPP_FINANCE_NO]
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                let dealerPhone = values[0][1]
                if (dealerPhone.includes("+")) {
                    dealerPhone = Utility.replaceRange(dealerPhone, 0, 3, '')
                }
                this.setState({ isModalVisible: true, mobileNumber: Utility.onlyNumeric(dealerPhone) })
            })
        }
    }

    goToContactList = () => {
        this.setState({ isModalVisible: false }, () => {
            this.props.navigation.navigate('contactList', {
                onSelectedMobileNumber: this.onSelectedMobileNumber
            })
        })
    }

    onSelectedMobileNumber = (item) => {
        Utility.log('item==>', item.phoneNumbers[0].number)
        let mobileNo = item.phoneNumbers[0].number

        if (mobileNo.includes("+")) {
            mobileNo = Utility.replaceRange(item.phoneNumbers[0].number, 0, 3, '')
        }

        this.setState({ isModalVisible: true, mobileNumber: Utility.onlyNumeric(mobileNo) })
    }

    loanShareMRPData = (selectedCard) => {
        Utility.log('MobileNo===>', this.state.countryCode, this.state.mobileNumber)
        let selectedPaymentDetail = ''
        if (selectedCard == 1) {
            selectedPaymentDetail = this.state.paymentDetail
        }
        else {
            selectedPaymentDetail = this.state.reqPaymentDetail
        }

        if (!Utility.validateMobileNo(this.state.mobileNumber, this.state.countryCode)) {
            this.refs['paymentDetailScreen'].TL_PhoneNumber.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['paymentDetailScreen'].PhoneNumberInput.focus()
            return
        }
        else {
            try {
                let keys = [Constants.DEALER_ID, Constants.USER_ID, Constants.SFA_USER_DATA, Constants.NAME, Constants.DEALER_ID_HASH, Constants.PHONE_NO, Constants.DEALER_NAME];
                const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
                promise.then(values => {
                    let sfa = values[2][1] ? JSON.parse(values[2][1]) : null
                    APICalls.loanShareMRPData(values[0][1], values[1][1], sfa, values[3][1], values[5][1], values[6][1], this.state.mobileNumber, values[4][1], selectedPaymentDetail, this.onSuccessShare, this.onFailureShare, this.props);
                })
                this.setState({ isLoading: true })
            } catch (error) {
                Utility.log(error)
            }
        }
    }

    onSuccessShare = (response) => {
        Utility.log('onSuccessLoanShare==>', JSON.stringify(response))
        this.setState({ isLoading: false }, () => {
            this.onWhatsAppShare(response)
        })
    }

    onFailureShare = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        Utility.log('onFailureLoanShare==>', JSON.stringify(response))
        this.setState({ isLoading: false })
    }

    onWhatsAppShare = async (response) => {
        let mobileNo = this.state.countryCode + this.state.mobileNumber
        let url = `whatsapp://send?text=${response.data}&phone=${mobileNo}`

        const canOpen = await Linking.canOpenURL(url)
        if (!canOpen) {
            Utility.showToast(Strings.WHATSAPP_NOT_INSTALL)
            throw new Error('Provided URL can not be handled')
        }

        this.closeModal()
        return Linking.openURL(url)
    }

    onChangeCountryCode = (text) => {
        this.setState({ countryCode: text })
    }

    onChangeMobileNo = (text) => {
        this.setState({ mobileNumber: Utility.onlyNumeric(text) })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.PAYMENT_DETAIL }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: this.state.isLoanShareQuote ? ImageAssets.icon_share : null,
                            onPress: () => this.openModal()
                        }]}
                    />

                    <PaymentDetailScreen
                        ref={'paymentDetailScreen'}
                        stateData={this.state}
                        onNextPress={this.onNextPress}
                        onSubmitDP={this.onSubmitDP}
                        closeModal={this.closeModal}
                        goToContactList={this.goToContactList}
                        loanShareMRPData={this.loanShareMRPData}
                        onChangeCountryCode={this.onChangeCountryCode}
                        onChangeMobileNo={this.onChangeMobileNo}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})
