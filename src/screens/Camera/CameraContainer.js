import React, {Component} from 'react';
import {BackHandler, View, TouchableOpacity, ActivityIndicator, Text,Dimensions, Image, Platform, StatusBar,Alert,Modal,SafeAreaView,KeyboardAvoidingView} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Colors,Strings,Dimens} from '../../values';
import Orientation from 'react-native-orientation';
import * as Utility from "../../util/Utility";
import ImageView from './../../nodeModuleChanges/react_native_Image_View/ImageView';
import RNFetchBlob from 'rn-fetch-blob'
import CameraRoll from "@react-native-community/cameraroll";
//let clickedImage;
let screenWidth = Dimensions.get('window').width
let screenHeight = Dimensions.get('window').height
export default class CameraContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clickedImage : null,
            isPreviewVisible: false,
            isSaving: false,
            images: [],
            usedLength : props.navigation.state.params.selectedDocument ? props.navigation.state.params.selectedDocument.images.length : 0,
            maxSize : props.navigation.state.params.maxSize ? props.navigation.state.params.maxSize : null ,
            selectedDocument : props.navigation.state.params.selectedDocument ? props.navigation.state.params.selectedDocument : null,
            isChild : props.navigation.state.params.isChild ? props.navigation.state.params.isChild : false,
        }
    }

    componentDidMount() {
        Utility.log("hihihihihi"+JSON.stringify(this.state.selectedDocument))
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        //Orientation.lockToPortrait();
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }
    handleBackPress = () => {
        this.props.navigation.goBack();
        return true;
    }
    onImageClick = () =>{
        this.setState({isPreviewVisible: true});
    }
    saveImage = (imagePath) => {
        if(this.state.maxSize && this.state.usedLength >= this.state.maxSize)
        {
            Alert.alert('', Strings.MAX_SIZE_TAKE_MSG1+this.state.maxSize+Strings.MAX_SIZE_TAKE_MSG2);
            return
        }

        this.saveImageToGallery(imagePath)
        //this.state.images.push(imagePath);
        //this.state.usedLength = this.state.usedLength + 1
        //this.setState({usedLength :this.state.usedLength + 1})
        /*let path = '';
        if (imagePath.startsWith('file://')) {
            path = imagePath.slice(7, imagePath.length)
        }*/
        Utility.log('Image', imagePath)
    };

    saveImageToGallery = (tag) => {
        CameraRoll.save(tag)
            .then(r => {
                if(Platform.OS == 'ios'){
                    RNFetchBlob.fs.unlink(tag)
                    //this.state.usedLength = this.state.usedLength + 1
                    this.state.images.push(r);
                    this.setState({
                        clickedImage : r,
                        usedLength : this.state.usedLength + 1
                    });
                }else{
                    RNFetchBlob.fs.stat(r)
                    .then((stats) => {
                        RNFetchBlob.fs.unlink(tag)
                        //this.state.usedLength = this.state.usedLength + 1
                        this.state.images.push("file://"+stats.path);
                        this.setState({
                            clickedImage : "file://"+stats.path,
                            usedLength : this.state.usedLength + 1
                        });
                    })
                    .catch((err) => {})
                }
            })
        .catch((err) => {
            //alert("error")
        });
    }

    onDonePress = () => {
        if (this.state.images.length > 0) {
            if(this.state.selectedDocument)
                this.props.navigation.state.params.cameraCallback && this.props.navigation.state.params.cameraCallback(this.state.images,this.state.selectedDocument,this.state.isChild);
            else
                this.props.navigation.state.params.cameraCallback && this.props.navigation.state.params.cameraCallback(this.state.images);
        }
        this.props.navigation.goBack();
    };

    takePicture = async function () {
        if (this.camera) {
            this.setState({
                isSaving: true
            });
            const options = {
                quality: 0.5,
                base64: false,
                fixOrientation: !(Platform.OS === "ios"),
                //orientation: 'landscapeLeft'
            };
            await this.camera.takePictureAsync(options).then((data) => {
                this.setState({
                    isSaving: false
                });
                if (data && data.uri) {
                    Utility.log('datauri', data.uri);
                    this.saveImage(data.uri);
                    // this.setState({
                    //     clickedImage : data.uri
                    // });
                    
                    //this.onImageClick(data.uri)
                }
            });
        }
    };

    render() {
        const images = [
            {
                source: {
                    uri: this.state.clickedImage,
                },
                title: '',
                width: 806,
                height: 720,
            },
        ];
        return (
            <View style={{flex: 1}}>
                <StatusBar hidden={true}/>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={{flex: 1}}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.auto}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use the camera'}/>
                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    right: 0,
                    left: 0,
                    height: 70,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={this.state.isSaving ? () => console.log('') : this.takePicture.bind(this)}
                        style={{
                            height: 60,
                            width: 60,
                            backgroundColor: 'white',
                            marginBottom: 10,
                            borderRadius: 30,
                            elevation: 2,
                            borderWidth: 2,
                            borderColor: Colors.PRIMARY
                        }}/>
                </View>
                <View style={{
                    position: 'absolute',
                    right: 20,
                    top: 20,
                    height: 32,
                    width: 32,
                    backgroundColor: 'transparent',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.navigation.goBack()}
                        style={{
                            flex: 1,
                            elevation: 2
                        }}>
                        <Image resizeMode={'cover'} style={{
                            height: 32,
                            width: 32,
                            tintColor: Colors.PRIMARY,
                            transform: [{rotate: '90deg'}]
                        }} source={require('../../assets/drawable/cancel_icon.png')}/>
                    </TouchableOpacity>
                </View>

                <View style={{
                    position: 'absolute',
                    right: 20,
                    bottom: 20,
                    height: 32,
                    width: 32,
                    backgroundColor: 'transparent',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.onDonePress()}
                        style={{
                            flex: 1,
                            elevation: 2
                        }}>
                        <Image resizeMode={'cover'} style={{
                            height: 32,
                            width: 32,
                            tintColor: Colors.PRIMARY,
                            transform: [{rotate: '90deg'}]
                        }} source={require('../../assets/drawable/right.png')}/>
                    </TouchableOpacity>
                </View>

                { this.state.clickedImage && <View style={{
                    position: 'absolute',
                    left: 20,
                    top: 20,
                    height: 50,
                    width: 50,
                    borderRadius: 25,
                    
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.onImageClick()}
                        style={{
                            flex: 1,
                            elevation: 2
                        }}>
                        <Image resizeMode={'cover'} style={{
                            height: 50,
                            width: 50,
                            borderRadius: 25,
                            //tintColor: Colors.PRIMARY,
                            transform: [{rotate: '90deg'}]
                        }} source={{uri : this.state.clickedImage}}/>
                    </TouchableOpacity>
                </View>}

                {this.state.isSaving ? <View style={{
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center',
                    bottom: 0,
                    backgroundColor: 'transparent',
                    top: 0,
                    right: 0,
                    left: 0
                }}>
                    <ActivityIndicator size="large" color={Colors.PRIMARY}/>
                </View> : null}

                
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isPreviewVisible}
                    onRequestClose={() => {
                        this.setState({ isPreviewVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isPreviewVisible: false })
                    }}>
                    <SafeAreaView style={{flex: 1, backgroundColor: Colors.BLACK_50,marginBottom : Platform.OS == 'ios' ? this.state.keyboardHeight : 0}}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                        <View style={{flex:1, backgroundColor: Colors.BLACK_50, padding: 0,justifyContent:'center',alignItems:'center' }}>

                            <TouchableOpacity
                                style={{ position: 'absolute',
                                right: 20,
                                top: 20,zIndex: 1000 }}
                                onPress={() => {
                                    this.setState({ isPreviewVisible: false })
                                }}>
                                <Image source={require('../../assets/drawable/cancel_icon.png')}
                                    style={{tintColor: Colors.PRIMARY, width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                            </TouchableOpacity>

                            <Image style={{
                                height: screenWidth,
                                width: screenHeight,
                                //transform: [{ rotate: '90deg' }]
                                }
                                 }
                                source={{ uri : this.state.clickedImage}}
                                resizeMode = {'contain'}    
                            />
                            
                        </View>
                    </KeyboardAvoidingView>
                    </SafeAreaView>
                    
                </Modal>
            </View>
        )
    }
}
