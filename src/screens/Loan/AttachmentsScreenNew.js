import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput,
    Platform
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import * as Utility from '../../util/Utility';
import {icon_no_image, pdf_icon,icon_arrow_right} from '../../assets/ImageAssets'
import * as Constants from '../../util/Constants'
import CameraGalleryButtonsComponent from "../../components/CameraGalleryButtonsComponent";
import {AccordionList} from "accordion-collapse-react-native";
export default class AttachmentsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    
    _head = (item) =>{
        var display_for_financier = false,check_for_mrp  = false
        if(item.required_for_financier){
            var required_for_financier = item.required_for_financier
            var split_financier = required_for_financier.split(",");
            
            if (split_financier.indexOf((this.props.stateData.financier.id).toString()) !== -1) {
                display_for_financier = true
            }
        }
        if(item.non_mrp_has_rule_engine && item.non_mrp_has_rule_engine == "1" && this.props.stateData.check_for_mrp){
            check_for_mrp = true
        }
        let conditionMatch = (item.is_required_gcloud == '1' || display_for_financier || check_for_mrp)
        return(
            item.is_gcloud == 1 && item.origin == Constants.FINAL_DOC_SCREEN  ? item.child && item.child.length > 0 ?
            // item.origin == Constants.FINAL_DOC_SCREEN ? item.child && item.child.length > 0 ?
            <View 
            style={Styles.parentWithChildStyle}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{
                            color:  conditionMatch ? Colors.PRIMARY : Colors.BLACK,
                            fontSize: Dimens.text_normal,
                            fontWeight: '400',
                            fontFamily: Strings.APP_FONT
                        }}>{item.name}</Text>
                    <Text style={{
                            color: Colors.PRIMARY,
                            fontSize: Dimens.text_normal,
                            fontWeight: '400',
                            fontFamily: Strings.APP_FONT
                        }}>{conditionMatch ? "*" : ""} </Text>
                </View>
                <Image style={{width:24,height:24}} source={icon_arrow_right}/>
            </View> :
             <TouchableOpacity onPress={()=> {
                this.props.setTransparentModalVisible(true,item,false)}
                }
                activeOpacity={1} 
                style={Styles.parentOnlyStyle}>
                <View style={{justifyContent:'space-between',flexDirection:'row',alignItems:'center' }}>
                    <View 
                        style={{flexDirection:'row',paddingRight:50}}>
                        <Text style={{
                                color: conditionMatch ? Colors.PRIMARY : Colors.BLACK,
                                fontSize: Dimens.text_normal,
                                fontWeight: '400',
                                fontFamily: Strings.APP_FONT
                            }}>{item.name}</Text>
                        <Text style={{
                                color: Colors.PRIMARY,
                                fontSize: Dimens.text_normal,
                                fontWeight: '400',
                                fontFamily: Strings.APP_FONT
                            }}>{conditionMatch ? "*" : ""} </Text>
                    </View>
                    
                </View>
                <TouchableOpacity 
                        onPress={()=>{ 
                            if(item.selectedImages.length >0)
                                this.props.onViewAllPress(item,false)}
                        } 
                        activeOpacity={1} 
                        style={{zIndex:999, position:'absolute',right:20, justifyContent:'center', width:30,height:30,borderRadius:15,backgroundColor:Colors.PRIMARY,alignItems:'center'}}><Text style={{color:Colors.WHITE,textAlign:'center'}}>{item.selectedImages.length}</Text></TouchableOpacity> 
                <Text style={Styles.minMaxUpload}>{Strings.MINIMUM+": "+item.min_upload+" , "+Strings.MAXIMUM+": "+item.max_upload}</Text>
            </TouchableOpacity> 
             : null
        );
    }

    _body = (item) => {
        
        return (
            item.is_gcloud == 1 && item.origin == Constants.FINAL_DOC_SCREEN ?
            // item.origin == Constants.FINAL_DOC_SCREEN ? 
            <View style={{justifyContent:'center'}} >
                <FlatList
                    contentContainerStyle={{ marginVertical: 2 }}
                    ref={(ref) => { this.flatListRef = ref; }}
                    extraData={this.state}
                    numColumns={1}
                    data={item.child}
                    renderItem={this.renderItem11}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                />
                {/* <Image source={{}} style={{position:'absolute',borderRadius:5,marginLeft:8, right:10,width:60,height:60,borderWidth:2,backgroundColor:'black'}}/> */}
            </View> : null
            
        )
    
}

renderItem11 = ({ item, index }) => {
    var display_for_financier = false,check_for_mrp  = false
    if(item.required_for_financier){
        var required_for_financier = item.required_for_financier
        var split_financier = required_for_financier.split(",");
        
        if (split_financier.indexOf((this.props.stateData.financier.id).toString()) !== -1) {
            display_for_financier = true
        }
    }
    if(item.non_mrp_has_rule_engine && item.non_mrp_has_rule_engine == "1" && this.props.stateData.check_for_mrp){
        check_for_mrp = true
    }
    let conditionMatch = (item.is_required_gcloud == '1' || display_for_financier || check_for_mrp)
    return (
        <TouchableOpacity
            style={{ width: "100%", height: 55,justifyContent:'center',marginVertical:8}}
            activeOpacity={1}
            onPress={() => {
                this.props.setTransparentModalVisible(true,item,true)}
            }
            activeOpacity={1}>
            <View style={ {
                borderColor: Colors.BLACK,
                backgroundColor: Colors.WHITE,
                borderWidth: .5,
                borderRadius: 27,
                marginTop: 0,
                marginHorizontal: 0,
                padding: 12,
                justifyContent:'center',}}>
            <View style={ {
                
                flexDirection:'row',
                //alignItems:'center',
                //justifyContent:'space-between',
                
             }}>
                <Text numberOfLines={2} style={[Styles.mainText, { color: conditionMatch ? Colors.PRIMARY : Colors.BLACK }]}>{item.name}</Text>
                <Text style={{
                    color: Colors.PRIMARY,
                    fontSize: Dimens.text_normal,
                    fontWeight: '400',
                    fontFamily: Strings.APP_FONT
                }}>{conditionMatch ? "*" : ""} </Text>
            </View>
            <TouchableOpacity 
                    onPress = { ()=>{
                        if(item.selectedImages.length >0)
                            this.props.onViewAllPress(item,true)}
                    } 
                    activeOpacity={1} 
                    style={{zIndex:999, position:'absolute',right:20, justifyContent:'center', width:30,height:30,borderRadius:15,backgroundColor:Colors.PRIMARY,alignItems:'center'}}>
                    <Text style={{color:Colors.WHITE,textAlign:'center'}}>{item.selectedImages.length}</Text>
            </TouchableOpacity>
            <Text style={Styles.minMaxUpload}>{Strings.MINIMUM+": "+item.min_upload+" , "+Strings.MAXIMUM+": "+item.max_upload}</Text>
            </View>
        </TouchableOpacity>
    )
}
    
    render() {
        let actionBarProps = {
            values: {title: Strings.ATTACHMENTS},
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <ScrollView >
                        <View style={{flex: 1, paddingBottom:50,flexDirection: 'column', margin: Dimens.margin_normal}}>
                            <AccordionList
                                list={this.props.stateData.document_list}
                                header={this._head}
                                body={this._body}
                            />
                        </View>  
                    </ScrollView>   
                    <TouchableOpacity
                            activeOpacity={0.8}
                            style={{width:'100%',position:'absolute',bottom:0, padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                            onPress={() => this.props.clickOnSubmit()}>

                            <Text style={Styles.buttonText}>{Strings.SUBMIT}</Text>
                    </TouchableOpacity>   
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.stateData.transparentModalVisible}
                        onRequestClose={() => {
                            this.props.setTransparentModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setTransparentModalVisible(false)
                        }}>
                        <View style={{flex: 1}}>
                            <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(false)}
                                              style={{flex: 1, backgroundColor: Colors.BLACK_40}}/>
                            <CameraGalleryButtonsComponent
                                onCameraPress={this.props.onCameraPress}
                                onGalleryPress={this.props.onGalleryPress}
                                onPDFPress = {this.props.onDocumentSelected}
                                />
                        </View>
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        paddingBottom: 50,
    },
    topContainer: {
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_normal,
        margin: Dimens.margin_0,
        marginBottom : Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed',
        //flexDirection: 'row',
        //justifyContent : 'space-between'
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_normal,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    minMaxUpload: {
        fontSize: Dimens.text_small,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },

    parentWithChildStyle :{
        justifyContent:'space-between',
        flexDirection:'row',
        padding:10,
        alignItems:'center',
        marginVertical:10,
        backgroundColor:Colors.GRAY_LIGHT,
        //borderColor: Colors.BLACK,
        //backgroundColor: Colors.WHITE,
        //borderWidth: .5,
        borderRadius: 5,
        elevation: 10,
        //zIndex : 1000,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,

    },
    parentOnlyStyle:{
        padding:10,
        marginVertical:10,
        backgroundColor:Colors.GRAY_LIGHT,
        justifyContent:'center',
        //borderColor: Colors.BLACK,
        //backgroundColor: Colors.WHITE,
        //borderWidth: .5,
        borderRadius: 5,
        elevation : 10,
        //zIndex : 1000,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,

        
    }
});
