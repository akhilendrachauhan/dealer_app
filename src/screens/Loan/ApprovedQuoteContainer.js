import React, { Component } from 'react';
import ApprovedQuoteScreen from './ApprovedQuoteScreen'
import {PermissionsAndroid,BackHandler,Alert} from 'react-native'
import { Utility,Constants,AnalyticsConstants } from '../../util';
import {Strings} from "../../values"
import * as RNFS from "react-native-fs";
import {getOfferDetail} from '../../api/APICalls'


export default class ApprovedQuoteContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stockItem : props.navigation.state.params.stockItem,
            financier : props.navigation.state.params.item,
            configData : props.navigation.state.params.configData,
            ruleEngineData : props.navigation.state.params.ruleEngineData ? props.navigation.state.params.ruleEngineData : null,
            carPrice : props.navigation.state.params.ruleEngineData && props.navigation.state.params.ruleEngineData.carPrice ? props.navigation.state.params.ruleEngineData.carPrice : undefined,
            selectedTenureText : props.navigation.state.params.ruleEngineData && props.navigation.state.params.ruleEngineData.tenure ? props.navigation.state.params.ruleEngineData.tenure.toString() : undefined,
            selectedTenureValue : props.navigation.state.params.ruleEngineData && props.navigation.state.params.ruleEngineData.tenure ? props.navigation.state.params.ruleEngineData.tenure.toString() :  undefined,
            selectedRegionText : undefined,
            selectedRegionValue : undefined,
            disbursement : props.navigation.state.params.ruleEngineData && props.navigation.state.params.ruleEngineData.disbursement ? props.navigation.state.params.ruleEngineData.disbursement : undefined,
            showTenureDialog : false,
            showRegionDialog : false,
            transparentModalVisible: false,
            stockPhotos: [],
            selectedStockImages : [],
            tdpAmount : undefined,
            emiAmount : undefined,
            amountCredited : props.navigation.state.params.ruleEngineData && props.navigation.state.params.ruleEngineData.disbursement ? props.navigation.state.params.ruleEngineData.disbursement : undefined,
            dealerDisbursement : undefined,
            realTDP : undefined,
            isEditableField : true,
            offer_id : 0,
        }
    }

    onBackPress = () => {
        Utility.confirmDialog(Strings.ALERT,Strings.GO_BACK_FROM_LOAN,Strings.CANCEL,Strings.YES,this.confirmCallback)
    }

    confirmCallback = (status) =>{
        if(status){
            let approvedQuoteData = {
                carPrice : this.state.carPrice,
                selectedTenureValue : this.state.selectedTenureValue,
                selectedTenureText : this.state.selectedTenureText,
                tdpAmount : this.state.tdpAmount,
                emiAmount : this.state.emiAmount,
                amountCredited : this.state.amountCredited,
                dealerDisbursement : this.state.dealerDisbursement,
                realTDP : this.state.realTDP,
                selectedStockImages :  this.state.selectedStockImages,
                stockPhotos : this.state.stockPhotos
            }
            Utility.setValueInAsyncStorage(Constants.APPROVED_DATA , JSON.stringify(approvedQuoteData))
            this.props.navigation.goBack();
        }
        else{

        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.APPROVED_QUOTE_SCREEN)
        if(this.state.financier.has_rule_engine == "1"){
            Utility.removeItemValue(Constants.APPROVED_DATA)
            this.getOfferDetail()
        }
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.getValueFromAsyncStorage(Constants.APPROVED_DATA).then((customerData) => {
            if(customerData  && customerData != ''){
                customerData = JSON.parse(customerData)
                this.setState({
                    carPrice : customerData.carPrice,
                    selectedTenureValue : customerData.selectedTenureValue,
                    selectedTenureText : customerData.selectedTenureText,
                    tdpAmount : customerData.tdpAmount,
                    emiAmount : customerData.emiAmount,
                    amountCredited : customerData.amountCredited,
                    dealerDisbursement : customerData.dealerDisbursement,
                    realTDP : customerData.realTDP,
                    selectedStockImages :  customerData.selectedStockImages,
                    stockPhotos : customerData.stockPhotos
                })
        }
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
    }

    getOfferDetail = () => {
        //alert(JSON.stringify(this.state.stockItem))
        let ruleEngineData = this.state.ruleEngineData
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getOfferDetail(this.state.stockItem.version_id,dealer_id,ruleEngineData.carPrice,ruleEngineData.tenure,ruleEngineData.region,ruleEngineData.disbursement,ruleEngineData.object,ruleEngineData.group,this.state.financier.id,ruleEngineData.insurance,this.state.stockItem.make_year,this.onSuccessOfferDetails, this.onFailureOfferDetails,this.props);
            } catch (error) {
                Utility.log(error);
            }
            this.setState({
                showLoading: true,
            })
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
    }
    

    onSuccessOfferDetails = (response) => {
        if(response && response.data   )
        {
            Utility.log("hellloooo"+JSON.stringify(response.data))
            let responseData = response.data
            this.setState({
                carPrice : responseData.car_price.toString(),
                selectedTenureValue : responseData.tenure.toString(),
                selectedTenureText : responseData.tenure.toString(),
                tdpAmount : responseData.first_payment.toString(),
                emiAmount : responseData.emi.toString(),
                amountCredited : responseData.approved_amount.toString(),
                dealerDisbursement : responseData.amount_needed.toString(),
                showLoading : false,
                isEditableField : false,
                offer_id : responseData.offer_id
            })
        }
        else
            this.setState({showLoading : false,})

    }
    onFailureOfferDetails = (response) => {
        this.setState({showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        this.props.navigation.goBack();
        
    };

    handleBackPress = () => {
        
        if(this.state.showTenureDialog || this.state.showRegionDialog )    
        {
            this.setState({
                showTenureDialog : false,
                showRegionDialog : false,
            })
        }
        else{
            this.onBackPress(); 
        }
        
        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }

    onCarPriceChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_CAR_PRICE.setError("")
        this.setState({carPrice : Utility.onlyNumeric(text)})
    }

    onTDPAmountChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_TDP_AMOUNT.setError("")
        this.setState({tdpAmount : Utility.onlyNumeric(text)})
    }

    onEMIAmountChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_EMI_AMOUNT.setError("")
        this.setState({emiAmount : Utility.onlyNumeric(text)})
    }

    onAmountCreditedChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_AMOUNT_CREDITED.setError("")
        this.setState({amountCredited : Utility.onlyNumeric(text)})
    }

    onDealerDisbursementChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_DISBURSEMENT_DEALER.setError("")
        this.setState({dealerDisbursement : Utility.onlyNumeric(text)})
    }

    onRealTDPChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_REAL_TDP.setError("")
        this.setState({realTDP : Utility.onlyNumeric(text)})
    }

    onTenurePress = () => {
        this.setState({showTenureDialog: true})
    }
    
    onTenureSelected = (text,value) => {
        this.refs['approvedQuoteScreen'].TL_TENURE.setError("")
        this.setState({showTenureDialog: false, selectedTenureText: text,selectedTenureValue : value})
    };


    onDisbursementChange = (text) => {
        this.refs['approvedQuoteScreen'].TL_DISBURSEMENT.setError("")
        this.setState({disbursement : Utility.onlyNumeric(text)})
    }

    onRegionPress = () => {
        this.setState({showRegionDialog: true})
    }

    onRegionSelected = (text,value) => {
        this.refs['approvedQuoteScreen'].TL_REGION.setError("")
        this.setState({showRegionDialog: false, selectedRegionText: text,selectedRegionValue : value})
    };

    hideDialog = () => {
        this.setState({
            showTenureDialog : false,
            showRegionDialog : false,
        })
    };

    clickOnNext = () => {
        Utility.log("hihi",this.state.carPrice,this.state.tdpAmount,this.state.emiAmount,this.state.amountCredited,this.state.dealerDisbursement,this.state.realTDP)
        if (Utility.isValueNullOrEmpty(this.state.carPrice) ) {
            this.refs['approvedQuoteScreen'].TL_CAR_PRICE.setError(Strings.ENTER_APPROVED_PRICE_OF_CAR)
            this.refs['approvedQuoteScreen'].carPriceInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.tdpAmount) ) {
            this.refs['approvedQuoteScreen'].TL_TDP_AMOUNT.setError(Strings.ENTER_APPROVED_TDP)
            this.refs['approvedQuoteScreen'].tdpAmountInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.emiAmount) ) {
            this.refs['approvedQuoteScreen'].TL_EMI_AMOUNT.setError(Strings.ENTER_APPROVED_EMI)
            this.refs['approvedQuoteScreen'].emiAmountInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.selectedTenureValue ? this.state.selectedTenureValue.toString() : this.state.selectedTenureValue) ) {
            this.refs['approvedQuoteScreen'].TL_TENURE.setError(Strings.SELECT_APPROVED_TENURE)
            this.refs['approvedQuoteScreen'].tenureInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.amountCredited) ) {
            this.refs['approvedQuoteScreen'].TL_AMOUNT_CREDITED.setError(Strings.ENTER_APPROVED_AMOUNT_TO_BE_CREDITED)
            this.refs['approvedQuoteScreen'].amountCreditedInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.dealerDisbursement) ) {
            this.refs['approvedQuoteScreen'].TL_DISBURSEMENT_DEALER.setError(Strings.ENTER_DISBURSEMENT_NEEDED_BY_DEALER)
            this.refs['approvedQuoteScreen'].dealerDisbursementInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.realTDP) ) {
            this.refs['approvedQuoteScreen'].TL_REAL_TDP.setError(Strings.ENTER_REAL_TDP)
            this.refs['approvedQuoteScreen'].realTDPInput.focus()
            return
        } 
        
        else if(parseInt(this.state.tdpAmount) >= parseInt(this.state.carPrice)){
            this.refs['approvedQuoteScreen'].TL_TDP_AMOUNT.setError(Strings.TDP_AMOUNT_LESS_THAN_PRICE)
            this.refs['approvedQuoteScreen'].tdpAmountInput.focus()
            return
        }
        else if(parseInt(this.state.emiAmount) >= parseInt(this.state.carPrice)){
            this.refs['approvedQuoteScreen'].TL_EMI_AMOUNT.setError(Strings.EMI_AMOUNT_LESS_THAN_PRICE)
            this.refs['approvedQuoteScreen'].emiAmountInput.focus()
            return
        }
        else if(parseInt(this.state.amountCredited) >= parseInt(this.state.carPrice)){
            this.refs['approvedQuoteScreen'].TL_AMOUNT_CREDITED.setError(Strings.CREDITED_AMOUNT_LESS_THAN_PRICE)
            this.refs['approvedQuoteScreen'].amountCreditedInput.focus()
            return
        }
        else if(parseInt(this.state.realTDP) >= parseInt(this.state.carPrice)){
            this.refs['approvedQuoteScreen'].TL_REAL_TDP.setError(Strings.REAL_TDP_AMOUNT_LESS_THAN_PRICE)
            this.refs['approvedQuoteScreen'].realTDPInput.focus()
            return
        }
        else if(parseInt(this.state.amountCredited) <= parseInt(this.state.emiAmount)){
            this.refs['approvedQuoteScreen'].TL_AMOUNT_CREDITED.setError(Strings.EMI_AMOUNT_LESS_THAN_AMOUNT_CREDITED)
            this.refs['approvedQuoteScreen'].amountCreditedInput.focus()
            return
        }
        else if(parseInt(this.state.dealerDisbursement) <= parseInt(this.state.emiAmount) ){
            this.refs['approvedQuoteScreen'].TL_DISBURSEMENT_DEALER.setError(Strings.EMI_AMOUNT_LESS_THAN_DISBURSEMENT_AMOUNT)
            this.refs['approvedQuoteScreen'].dealerDisbursementInput.focus()
            return
        }
        else if (this.state.financier.has_rule_engine != Constants.HAS_RULE_ENGINE && this.state.selectedStockImages.length == 0) {
            Utility.showToast(Strings.UPLOAD_AN_IMAGE)
            return
        }
        else{
            let approvedQuoteData
            // if(this.state.selectedStockImages[0] && this.state.selectedStockImages[0].uri){
            //     let imageUploadFile = new SingleImageUpload()
            //     imageUploadFile.uploadImage(this.state.selectedStockImages[0].uri,this.imageCallback)
            // }
            // else{
            approvedQuoteData = {
                carPrice : this.state.carPrice,
                selectedTenureValue : this.state.selectedTenureValue,
                tdpAmount : this.state.tdpAmount,
                emiAmount : this.state.emiAmount,
                amountCredited : this.state.amountCredited,
                dealerDisbursement : this.state.dealerDisbursement,
                realTDP : this.state.realTDP,
                imageToUpload :  this.state.selectedStockImages[0] ? this.state.selectedStockImages[0].uri : null,
                offer_id : this.state.offer_id
            }
            //}
            Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_APPROVED_QUOTE_SUBMIT);//----- log loan event
            Utility.log("values:"+this.state.carPrice+":"+this.state.selectedTenureValue+":"+this.state.tdpAmount+":"+this.state.emiAmount)
            this.props.navigation.navigate("customerDetail",{approvedQuoteData : approvedQuoteData,ruleEngineData : this.state.ruleEngineData,configData : this.state.configData, item : this.state.financier,stockItem : this.state.stockItem})
            
        }
    }

    imageCallback = async (url)  =>  {
        let approvedQuoteData = {
            carPrice : this.state.carPrice,
            selectedTenureValue : this.state.selectedTenureValue,
            tdpAmount : this.state.tdpAmount,
            emiAmount : this.state.emiAmount,
            amountCredited : this.state.amountCredited,
            dealerDisbursement : this.state.dealerDisbursement,
            realTDP : this.state.realTDP,
            imageToUpload :  url
        }
        this.props.navigation.navigate("customerDetail",{approvedQuoteData : approvedQuoteData,ruleEngineData : this.state.ruleEngineData,configData : this.state.configData, item : this.state.financier})
        //this.setState({imageToUpload : url})
    }

    setTransparentModalVisible = (isVisible) => {
        this.checkPermissionAndOpenModal(isVisible, true);
    };

    checkPermissionAndOpenModal = (isVisible, isTransparent) => {
        if (isVisible) {
            if (Platform.OS === 'android') {
                this.askForPermission().then(value => {
                    isVisible = value;
                    if (!value) {

                    }
                    if (isTransparent) {
                        this.setState({transparentModalVisible: value})
                    } else {
                        this.setState({modalVisible: value})
                    }
                });
            } else {
                if (isTransparent) {
                    this.setState({transparentModalVisible: isVisible})
                } else {
                    this.setState({modalVisible: isVisible})
                }
            }
        } else {
            if (isTransparent) {
                this.setState({transparentModalVisible: isVisible})
            } else {
                this.setState({modalVisible: isVisible})
            }
        }
    };
    async askForPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]);
            return granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED && granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            console.warn(err);
            return false;
        }
    }

    onCameraPress = () => {
        this.props.navigation.navigate('camera', {cameraCallback: this.onCameraDone});
        this.setState({modalVisible: false, transparentModalVisible: false})
    };

    onCameraDone = (photos) => {
        let data = [];
        data = data.concat(photos);
        let selectedItems = []
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: {latitude: 0, longitude: 0},
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.setState({stockPhotos: data,selectedStockImages : selectedItems});
        photos = selectedItems

        const promises = photos.map((item, index) => {
            var pattern = new RegExp('^(https?|ftp)://');
            const {uri} = item;
            if(pattern.test(uri) ) {
                return item
            } else if(uri.startsWith("ph:") || uri.startsWith("PH:")){

            var regex = /:\/\/(.{36})\//i;
            var result = uri.match(regex);
            const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
            promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
            return promise
                .then((resultUri) => {
                    photos[index].uri = resultUri;
            });
            }
            else{
                return item
            }
        })
        Promise.all(promises)
            .then(() => {
                Utility.log(photos)
                this.setState({selectedStockImages : photos})
        });
    };

    onGalleryDone = (photos) => {
        let data = [];
        for (let i = 0; i < photos.length; i++) {
            data.push(photos[i].uri)
        }
        this.setState({stockPhotos: data,selectedStockImages : photos});
        
        Utility.log(photos)
        
        const promises = photos.map((item, index) => {
            var pattern = new RegExp('^(https?|ftp)://');
            const {uri} = item;
            if(pattern.test(uri) ) {
                return item
            } else if(uri.startsWith("ph:") || uri.startsWith("PH:")){

            var regex = /:\/\/(.{36})\//i;
            var result = uri.match(regex);
            const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
            promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
            return promise
                .then((resultUri) => {
                    photos[index].uri = resultUri;
            });
            }
            else{
                return item
            }
        })
        Promise.all(promises)
            .then(() => {
                Utility.log(photos)
                this.setState({selectedStockImages : photos})
        });
    };

    onGalleryPress = () => {
        let data = this.state.stockPhotos;
        let selectedItems = [];
        // for (let i = 0; i < data.length; i++) {
        //     let singleData = {
        //         type: 'image/jpeg',
        //         location: {latitude: 0, longitude: 0},
        //         timestamp: 0,
        //         height: 0,
        //         width: 0,
        //         uri: data[i]
        //     };
        //     selectedItems.push(singleData);
        // }
        this.props.navigation.navigate("galleryFolder", {
            galleryCallback: this.onGalleryDone,
            selectedItems: selectedItems,
            
        });
        this.setState({modalVisible: false, transparentModalVisible: false});
    };

    gotoImagePreview = (uri) => {
        let images = [uri]
        this.props.navigation.navigate("imageReviewScreen", {
            data: images,
            index: 0
        });
    } 
    render() {
        return (
            <ApprovedQuoteScreen
                ref={'approvedQuoteScreen'}
                onBackPress={this.onBackPress}
                stateData = {this.state}
                onCarPriceChange = {this.onCarPriceChange}
                onDisbursementChange = {this.onDisbursementChange}
                onRegionPress = {this.onRegionPress}
                onTenurePress = {this.onTenurePress}
                onTenureSelected = {this.onTenureSelected}
                onRegionSelected = {this.onRegionSelected}
                hideDialog = {this.hideDialog}
                clickOnNext = {this.clickOnNext}
                setTransparentModalVisible = {this.setTransparentModalVisible}
                onCameraPress = {this.onCameraPress}
                onGalleryPress = {this.onGalleryPress}
                onTDPAmountChange = {this.onTDPAmountChange}
                onEMIAmountChange = {this.onEMIAmountChange}
                onAmountCreditedChange = {this.onAmountCreditedChange}
                onDealerDisbursementChange = {this.onDealerDisbursementChange}
                onRealTDPChange = {this.onRealTDPChange}
                gotoImagePreview = {this.gotoImagePreview}
            />
        )
    }
}
