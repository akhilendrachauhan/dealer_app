import React, { Component } from 'react';
import { View,StyleSheet, SafeAreaView} from 'react-native'
import LoanStockListingScreen from './LoanStockListingScreen'
import { Utility, Constants,AnalyticsConstants } from './../../util'
import {getStockListing} from './../../api/APICalls'
import * as ScreenHoc from './../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import { ScreenStates, ScreenName } from '../../util/Constants'
import { icon_back_arrow, icon_search,icon_menu } from './../../assets/ImageAssets'
import {Strings,Colors} from './../../values/'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import ActionBarSearchComponent from "../../granulars/ActionBarSearchComponent";
export default class LoanStockListingContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stockTotalCount : 0,
            showSearchBar: false,
            stockList: [],
            refreshing : false,
            screenState: ScreenStates.NO_ERROR,
            activeStockShow : true,
            showMenu : false,
            showLoading : false,
            page:1,
            stockType : Constants.ACTIVE_STOCK,
            activeStockType : ''
        }
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_STOCK_LIST_SCREEN)
        this.getStockListingData('',false);
        
    }

    clickOnMenu = ()=> {
        this.setState({showMenu:!this.state.showMenu})
    }

    clickOnMenuOption = () => {
        if(this.state.activeStockShow){
            if(this.state.activeStockType != ''){
                this.setState({showMenu:false,stockType: Constants.ACTIVE_STOCK,activeStockType : '',page:1},()=>{
                    this.getStockListingData('',false)
                })
            }
            else {
                this.setState({showMenu:false,activeStockShow: !this.state.activeStockShow,stockType: Constants.REMOVED_STOCK,page:1},()=>{
                    this.getStockListingData('',false)
                })
            }
            
        }
        else{
            this.setState({showMenu:false,activeStockShow: !this.state.activeStockShow,stockType: Constants.ACTIVE_STOCK,page:1},()=>{
                this.getStockListingData('',false)})
        }
    }

    onSubmitEditing = (text) => {
        this.getStockListingData(text,false)
    }
    onSearchTextChange = (text) => {
        this.getStockListingData(text,false)

    }
    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
        this.getStockListingData('',false)
    }
    onSearch = () => {
        if(this.state.stockTotalCount > 0)
            this.setState({ showSearchBar: true })
        else
            Utility.showToast(Strings.NO_RECORDS_TO_SEARCH)
    }

    getStockListingData = (text,fromRefresh) => {
        if(fromRefresh || text != ''){
            this.state.page = 1;
        }
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getStockListing(dealer_id, text,this.state.stockType,this.state.page,this.state.activeStockType,[],this.onSuccessStockListing, this.onFailureStockListing,this.props);
            } catch (error) {
                Utility.log(error);
            }
            if(fromRefresh){
                this.setState({
                    refreshing : true,
                })
            }
            else{
                this.setState({
                    screenState: ScreenStates.IS_LOADING,
                })
            }
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
        
    }
    getStockListingMoreData = () => {
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getStockListing(dealer_id,'',this.state.stockType,this.state.page,this.state.activeStockType,[],this.onSuccessStockListingMore, this.onFailureStockListingMore);
            } catch (error) {
                Utility.log(error);
            }
           
            this.setState({
                showLoading : true,
            })
           
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
        
    }
    handleLoadMore = () => {
        if(this.state.stockTotalCount > this.state.stockList.length){
            this.setState({
                page: this.state.page + 1,
                
            }, () => {
                this.getStockListingMoreData();
            });
        }
    }

    onSuccessStockListing = (response) => {
        if(response && response.data && response.data.length > 0  )
            this.setState({screenState: ScreenStates.NO_ERROR,
                refreshing: false,showLoading : false,
                stockTotalCount : response.pagination.total,
                stockList : this.state.page == 1 ? response.data : [...this.state.stockList, ...response.data]
                })
        else
            this.setState({screenState: ScreenStates.NO_DATA_FOUND,refreshing: false,showLoading : false,})

    }
    onFailureStockListing = (response) => {
        this.setState({screenState: ScreenStates.NO_DATA_FOUND,refreshing: false,showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        
    };

    onSuccessStockListingMore = (response) => {
        if(response && response.data && response.data.length > 0  )
            this.setState({showLoading : false,
                stockTotalCount : response.pagination.total,
                stockList : this.state.page == 1 ? response.data : [...this.state.stockList, ...response.data]
                })
        else
            this.setState({showLoading : false,})

    }
    onFailureStockListingMore = (response) => {
        this.setState({showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
        
    };
    onRetryClick = () => {
        this.getStockListingData('',false)
    }
    clickOutSide = () => {
        this.setState({showMenu : false})
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_STOCK_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    };

    renderTitle = () => {
        return Strings.SELECT_CAR
    }

    onListItemClick = (item) => {
        Utility.removeItemValue(Constants.CUSTOMER_DATA)
        Utility.removeItemValue(Constants.APPROVED_DATA)
        Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_STOCK_SELECTED,{stock_id : item.id});//----- log loan event
        this.props.navigation.navigate("financierList",{ item: item, isLoanShareQuote: false })
    }
    render() {
        this.actionBarProps = {
            values: {
                title: this.renderTitle()
            },
            rightIcons: [
                {
                    image: icon_search,
                    onPress: this.onSearch
                },
                // {
                //     image: icon_menu,
                //     onPress: this.clickOnMenu
                // },

            ],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };
        return (
            
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK  }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={this.actionBarProps.values}
                        actions={this.actionBarProps.actions}
                        iconMap={this.actionBarProps.rightIcons}
                        styleAttributes={this.actionBarProps.styleAttr}
                    />
                    {this.state.showSearchBar ? this.openActionSearchBar() : null}
                    <WithLoading
                            screenState={this.state.screenState}
                            screenName={ScreenName.SCREEN_LEAD}
                            onRetry={this.onRetryClick}>  
                        
                        <LoanStockListingScreen
                            ref={'stockListingScreen'}
                            onBackPress={this.onBackPress}
                            onSubmitEditing={this.onSubmitEditing}
                            onSearchBackPress={this.onSearchBackPress}
                            onSearchTextChange={this.onSearchTextChange}
                            onSearch={this.onSearch}
                            showSearchBar={this.state.showSearchBar}
                            stockList={this.state.stockList}
                            onListItemClick={this.onListItemClick}
                            getStockListingData = {this.getStockListingData}
                            refreshing = {this.state.refreshing}
                            clickOnMenu = {this.clickOnMenu}
                            clickOnMenuOption = {this.clickOnMenuOption}
                            showMenu = {this.state.showMenu}
                            activeStockShow = {this.state.activeStockShow}
                            showLoading = {this.state.showLoading}
                            handleLoadMore = {this.handleLoadMore}
                        />
                           
                    </WithLoading>
                    </View>
                </SafeAreaView>    
            
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})