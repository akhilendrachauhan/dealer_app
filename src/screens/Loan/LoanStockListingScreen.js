import React, { Component } from 'react';
import { View,StyleSheet, FlatList} from 'react-native'
import { Strings, Colors, Dimens } from './../../values'
import { _ActionBarStyle } from '../../values/Styles'
import LoadingComponent from './../../granulars/LoadingComponent'
import LoanStockListingCard from './../../granulars/LoanStockListingCard'
import { Utility } from '../../util'

export default class LoanStockListingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu : false,
            activeStockShow : true,
        }
    }
    renderItem = ({ item }) => {
        return (
            item.is_finance && item.is_finance == 1 ? 
            <LoanStockListingCard
                item={item}
                onListItemClick={this.props.onListItemClick}
            />:
            <View/>
        )
    }
    
    render() {
        return (
        
                <View style={styles.container}>
                    { this.props.stockList.length > 0 && <FlatList
                        style={{ margin: Dimens.margin_small,flex:1 }}
                        data={this.props.stockList}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.id}
                        extraData = {this.state}
                        onRefresh={()=>this.props.getStockListingData('',true)}
                        refreshing={this.props.refreshing}
                        onEndReached={()=>this.props.handleLoadMore()}
                        onEndReachedThreshold={0.5}
                    />}
                    {/* { this.props.showMenu && <TouchableWithoutFeedback onPress={()=> this.props.clickOutSide()}>
                        <View style={{width: '100%', height :'100%', position: 'absolute', left: 0, top: 0,bottom:0,right:0}} />
                    </TouchableWithoutFeedback>
                    } */}
                    { this.props.showLoading && <LoadingComponent/>}
                    
                {/* { this.props.showMenu && <View style={{borderRadius:2,backgroundColor:Colors.GRAY_LIGHT_BG, position:'absolute',top:0,right:10,zIndex:999,paddingVertical:10,paddingHorizontal:20}}>
                    <TouchableOpacity
                        onPress = {()=> this.props.clickOnMenuOption()}
                        activeOpacity = {0.8}
                    >
                        <Text style={styles.menuOptionText}>{this.props.activeStockShow ? this.props.activeStockType != '' ? Strings.ALL_STOCK : Strings.REMOVED_STOCK : Strings.ACTIVE_STOCK}</Text>
                    </TouchableOpacity>
                </View>} */}
                {/* Menu View  */}
                
                </View>
            
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
   menuOptionText:{
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    
});