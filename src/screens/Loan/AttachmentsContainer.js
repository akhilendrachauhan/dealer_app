import React, { Component } from 'react';
import { Platform, PermissionsAndroid, Alert,BackHandler } from 'react-native'
import AttachmentsScreen from './AttachmentsScreenNew'
import { Utility,AnalyticsConstants } from '../../util';
import { Strings } from "../../values"
import * as RNFS from "react-native-fs";
import DBFunctions from "../../database/DBFunctions";
import * as DBConstants from '../../database/DBConstants';
import DocumentImageUploadFile from '../../util/DocumentImageUploadFile'
import * as Constants from '../../util/Constants'
import FilePickerManager from 'react-native-file-picker';
let that
export default class AttachmentsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {

            document_list: props.navigation.state.params.document_list,
            financier: props.navigation.state.params.financier,
            selectedImages: [],
            images: [],
            transparentModalVisible: false,
            selectedDocument: null,
            //lead_id: 230, //props.navigation.state.params.leadId,
            lead_id: props.navigation.state.params.leadId,
            isChild : false,
            check_for_mrp : props.navigation.state.params.check_for_mrp
        }
    }

    onBackPress = () => {
        
        // Alert.alert(Strings.ALERT, Strings.GO_BACK_FROM_ATTACHMENTS, [
        //     {
        //         text: Strings.CANCEL,
        //         onPress: () => {

        //         }
        //     },
        //     {
        //         text: Strings.CONFIRM,
        //         onPress: () => {
        //             if (this.props.navigation.state.params && this.props.navigation.state.params.isGoBack) {
        //                 this.props.navigation.goBack()
        //             }
        //             else {
        //                 this.props.navigation.navigate("loanDashboard")
        //             }
        //         }
        //     }
        // ], { cancelable: true })
        Utility.confirmDialog("",Strings.GO_BACK_FROM_LOAN,Strings.CANCEL,Strings.YES,this.confirmCallback)
    }

    confirmCallback = (status) =>{
        if(status){
            if (this.props.navigation.state.params && this.props.navigation.state.params.isGoBack) {
                this.props.navigation.goBack()
            }
            else {
                this.props.navigation.navigate("loanDashboard")
            }
        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.ATTACHMENT_SCREEN)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        // let filteredDocumentList = this.state.document_list.filter((item) => {
        //     if (item.show_for_financier == 0 || this.state.financier.id == item.show_for_financier) {
        //         return item
        //     }
        //     // if(item.id == 2){
        //     //     item.required_when = '0'
        //     //     item.child[0].required_when = '0'
        //     //     return item
        //     // }
        // })
        // this.setState({document_list : filteredDocumentList})
        Utility.log("financier:" + JSON.stringify(this.state.financier))
        Utility.log("document_list:" + JSON.stringify(this.state.document_list))
    }

    handleBackPress = () => {
        this.onBackPress()
        return true
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    setTransparentModalVisible = (isVisible, item , isChild) => {
        if(item){
            if(item.images.length < item.max_upload){
                this.setState({isChild},()=>{
                    this.checkPermissionAndOpenModal(isVisible, item);
                })
            }
            else{
                Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + item.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
            }
        }
        else{
            this.checkPermissionAndOpenModal(isVisible, item);
        }
        
    };

    checkPermissionAndOpenModal = (isVisible, item) => {
        if (isVisible) {
            if (Platform.OS === 'android') {
                this.askForPermission().then(value => {
                    isVisible = value;
                    if (!value) {

                    }
                    this.setState({ transparentModalVisible: value, selectedDocument: item })
                });
            } else {

                this.setState({ transparentModalVisible: isVisible, selectedDocument: item })
            }
        } else {

            this.setState({ transparentModalVisible: isVisible, selectedDocument: item })

        }
    };

    onCameraPress = () => {
        this.props.navigation.navigate('camera', {
            cameraCallback: this.onCameraDone,
            maxSize: this.state.selectedDocument.max_upload,
            selectedDocument: this.state.selectedDocument,
            isChild : this.state.isChild,

        });
        this.setState({ transparentModalVisible: false })
    };

    onCameraDone = (photos, selectedDocument,isChild) => {
        // let filterData = this.state.document_list.filter((item) => {
        //     if (selectedDocument.id == item.id) {
        //         return item
        //     }
        //     //return item; 
        // })
        let data = selectedDocument.images;//this.state.stockPhotos;
        data = data.concat(photos);
        let selectedItems = []
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: { latitude: 0, longitude: 0 },
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        filterData = this.state.document_list.filter((item) => {
          if(isChild){
            if (selectedDocument.parent_id == item.id) {
              for(let k = 0; k < item.child.length; k++){
                if(selectedDocument.id == item.child[k].id){
                  item.child[k].images = data;
                  item.child[k].selectedImages = selectedItems;
                }
              }
            }
          }else{
            if (selectedDocument.id == item.id) {
                item.selectedImages = selectedItems;
                item.images = data
            }
          }
            return item;
        })
        this.setState({ document_list: filterData })

        //this.setState({images: data,selectedImages : selectedItems});
        Utility.log(photos)
    };

    onGalleryDone = (photos, selectedDocument,isChild) => {
        let data = [];
        for (let i = 0; i < photos.length; i++) {
            data.push(photos[i].uri)
        }

        let filterData = this.state.document_list.filter((item) => {
          if(isChild){
            if (selectedDocument.parent_id == item.id) {
              for(let k = 0; k < item.child.length; k++){
                if(selectedDocument.id == item.child[k].id){
                  item.child[k].images = data;
                  item.child[k].selectedImages = photos;
                }
              }
            }
          }
          else{
            if (selectedDocument.id == item.id) {
                item.images = data;
                item.selectedImages = photos;
            }
          }
            return item;
        })
        this.setState({ document_list: filterData })

        //this.setState({images: data});

        Utility.log(photos)

        // const promises = photos.map((item, index) => {
        //     var pattern = new RegExp('^(https?|ftp)://');
        //     const {uri} = item;
        //     if(pattern.test(uri) ) {
        //         return item
        //     } else if(uri.startsWith("ph:") || uri.startsWith("PH:")){

        //     var regex = /:\/\/(.{36})\//i;
        //     var result = uri.match(regex);
        //     const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
        //     promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
        //     return promise
        //         .then((resultUri) => {
        //             photos[index].uri = resultUri;
        //     });
        //     }
        //     else{
        //         return item
        //     }
        // })
        // Promise.all(promises)
        //     .then(() => {
        //         Utility.log(photos)
        //         let filterData = this.state.document_list.filter((item) => {
        //             if(selectedDocument.id == item.id) {
        //                 item.selectedImages = photos;
        //             }
        //             return item; 
        //         })
        //         this.setState({document_list : filterData})
        // });
    };

    onGalleryPress = () => {
        let data = this.state.selectedDocument.images;
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: { latitude: 0, longitude: 0 },
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.props.navigation.navigate("galleryFolder", {
            galleryCallback: this.onGalleryDone,
            selectedItems: selectedItems,
            maxSize: this.state.selectedDocument.max_upload,
            selectedDocument: this.state.selectedDocument,
            isChild : this.state.isChild,
        });
        this.setState({ transparentModalVisible: false });
    };

    onViewAllPress = (item,isChild) => {
        if (item.selectedImages.length > 0) {
            if (Platform.OS === 'android') {
                this.askForPermission().then(value => {
                    if (!value) {

                    } else {
                        this.props.navigation.navigate('documentImages', {
                            data: item.images,
                            onPhotosEditDone: this.onPhotosEditDone,
                            selectedDocument: item
                        })
                    }
                });
            } else {
                this.props.navigation.navigate('documentImages', {
                    data: item.images,
                    onPhotosEditDone: this.onPhotosEditDone,
                    selectedDocument: item
                })
            }

            this.setState({isChild})
        }

    };

    async askForPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]);
            return granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED && granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            console.warn(err);
            return false;
        }
    }

    onPhotosEditDone = (selectedDocument) => {
        //alert(JSON.stringify(selectedDocument)+""+this.state.isChild)
        let filterData = this.state.document_list.filter((item) => {
          if(this.state.isChild){
            if (selectedDocument.parent_id == item.id) {
              for(let k = 0; k < item.child.length; k++){
                if(selectedDocument.id == item.child[k].id){
                  item.child[k].selectedImages = selectedDocument.selectedImages;
                  item.child[k].images = selectedDocument.images
                }
              }
            }
          }else{
            if (selectedDocument.id == item.id) {
                item.selectedImages = selectedDocument.selectedImages;
                item.images = selectedDocument.images
            }
          }
            return item;
        })
        this.setState({ document_list: filterData })
    };

    clickOnSubmit = async () => {
        //this.saveImagesToDB(this.state.lead_id)
        let item, required = 0 , minRequired = 0, usedItem = null;

        /* For old design */

        // for (let i = 0; i < this.state.document_list.length; i++) {
        //     item = this.state.document_list[i]
        //     if(item.is_gcloud == 1){
        //         if (item.origin == Constants.FINAL_DOC_SCREEN && item.is_required_gcloud == "1") {
        //             if (item.images.length == 0) {
        //                 required = 1
        //             }
        //         }
        //         else if (this.state.financier && item.origin == Constants.FINAL_DOC_SCREEN && this.state.financier.has_rule_engine == Constants.HAS_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_AVAILABLE) {
        //             if (item.images.length == 0) {
        //                 required = 1
        //             }
        //         }
        //         else if (this.state.financier && item.origin == Constants.FINAL_DOC_SCREEN && this.state.financier.has_rule_engine == Constants.HAS_NO_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_NOT_AVAILABLE) {
        //             if (item.images.length == 0) {
        //                 required = 1
        //             }
        //         }
        //     }
        // }

        /* For old design */

        /* For new design */
        var display_for_financier = false,required_for_financier,split_financier,conditionMatch1 = false,conditionMatch2 = false, check_for_mrp = false
        var display_for_financier1 = false,required_for_financier1,split_financier1, check_for_mrp1 = false
        for (let i = 0; i < this.state.document_list.length; i++) {
          item = this.state.document_list[i]
          if(item.is_gcloud == 1){
            display_for_financier = false
            if(item.required_for_financier){
                required_for_financier = item.required_for_financier
                split_financier = required_for_financier.split(",");
                if (split_financier.indexOf((this.state.financier.id).toString()) !== -1) {
                    display_for_financier = true
                }
            }
            if(item.non_mrp_has_rule_engine && item.non_mrp_has_rule_engine == "1" && this.state.check_for_mrp){
                check_for_mrp = true
            }

            conditionMatch1 = (item.origin == Constants.FINAL_DOC_SCREEN && ( item.is_required_gcloud == "1" || display_for_financier || check_for_mrp) && item.child && item.child.length > 0)
            conditionMatch2 = (item.origin == Constants.FINAL_DOC_SCREEN && ( item.is_required_gcloud == "1" || display_for_financier || check_for_mrp))
            if (conditionMatch1) {
                for(let k = 0; k < item.child.length ; k++){
                    childItem = item.child[k]
                    display_for_financier1 = false
                    if(childItem.required_for_financier){
                        required_for_financier1 = childItem.required_for_financier
                        split_financier1 = required_for_financier1.split(",");
                        if (split_financier1.indexOf((this.state.financier.id).toString()) !== -1) {
                            display_for_financier1 = true
                        }
                    }
                    if(childItem.non_mrp_has_rule_engine && childItem.non_mrp_has_rule_engine == "1" && this.state.check_for_mrp){
                        check_for_mrp1 = true
                    }  

                  if((childItem.is_required_gcloud == "1" || display_for_financier1 || check_for_mrp1)  ){
                    if(childItem.images.length == 0 ){
                        required = 1
                        usedItem = childItem
                        break
                    }
                    else if(childItem.images.length != 0 && childItem.images.length < item.child[k].min_upload){
                        minRequired = 1
                        usedItem = childItem
                        break
                    }
                }
                }
            }
            else if(item.origin == Constants.FINAL_DOC_SCREEN && item.child && item.child.length > 0){
                for(let k = 0; k < item.child.length ; k++){
                  if(item.child[k].images.length != 0 && item.child[k].images.length < item.child[k].min_upload ){
                    minRequired = 1
                    usedItem = item.child[k]
                    break
                  }
                }
            }
            else{
            if (conditionMatch2) {
                if (item.images.length == 0) {
                    required = 1
                    usedItem = item
                    break
                }
                else if(item.images.length != 0 && item.images.length < item.min_upload){
                    minRequired = 1
                    usedItem = item
                    break
                }
                
            }
            else if (item.origin == Constants.FINAL_DOC_SCREEN && item.images.length != 0 && item.images.length < item.min_upload) {
                minRequired = 1
                usedItem = item
                break
            }
            
            }
          }
        }
        /* For new design */
        let itemName = ""
        if(usedItem)
            itemName = " for "+usedItem.name
        if (required == 1) {
            Utility.showAlert("",Strings.UPLOAD_REQUIRED_IMAGE)
        }
        else if(minRequired == 1){
            Utility.showAlert("",Strings.UPLOAD_MINIMUM_REQUIRED_IMAGE + itemName)
        }
        else {
            await this.saveImagesToDB(this.state.lead_id)
        }
    }

    saveImagesToDB = (lead_id) => {
        let obj, index, filename, documentItem, j = 0, promises, allImages = [], allDocuments = [], usedIndex = -1,documentToBeUsed
        let dbFunctions = new DBFunctions();
        var pattern = new RegExp('^(https?|ftp)://');
        for (let i = 0; i < this.state.document_list.length; i++) {
            Utility.log("hello" + i)
            documentToBeUsed = this.state.document_list[i]
            if(documentToBeUsed.child && documentToBeUsed.child.length > 0){
              for(let m = 0; m < documentToBeUsed.child.length ; m++){
                documentItem = documentToBeUsed.child[m]
                if (documentItem.selectedImages.length > 0) {
                    let photos = documentItem.selectedImages
                    promises = photos.map((item, index) => {
                        //var pattern = new RegExp('^(https?|ftp)://');
                        usedIndex = usedIndex + 1
                        allImages.push(item)
                        allDocuments.push(documentItem)
                        const { uri } = item;
                        if (pattern.test(uri)) {
                            return item
                        } 
                        // else if (uri.startsWith("ph:") || uri.startsWith("PH:")) {

                        //     var regex = /:\/\/(.{36})\//i;
                        //     var result = uri.match(regex);
                        //     const dest = RNFS.TemporaryDirectoryPath + Math.random().toString(36).substring(7) + ".jpg";
                        //     promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id=" + result[1] + "&ext=JPG", dest, 0, 0);
                        //     return promise
                        //         .then((resultUri) => {
                        //             allImages[usedIndex].uri = resultUri;
                        //         });
                        // }
                        else {
                            return item
                        }

                    })
                }
              }
            }
            else{
              documentItem = documentToBeUsed
              if (documentItem.selectedImages.length > 0) {
                  let photos = documentItem.selectedImages
                  promises = photos.map((item, index) => {
                      //var pattern = new RegExp('^(https?|ftp)://');
                      usedIndex = usedIndex + 1
                      allImages.push(item)
                      allDocuments.push(documentItem)
                      Utility.log("usedIndex:::"+usedIndex)
                      const { uri } = item;
                      if (pattern.test(uri)) {
                          return item
                      } 
                      // else if (uri.startsWith("ph:") || uri.startsWith("PH:")) {

                      //     var regex = /:\/\/(.{36})\//i;
                      //     var result = uri.match(regex);
                      //     const dest = RNFS.TemporaryDirectoryPath + Math.random().toString(36).substring(7) + ".jpg";
                      //     promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id=" + result[1] + "&ext=JPG", dest, 0, 0);
                      //     return promise
                      //         .then((resultUri) => {
                                  
                      //             allImages[usedIndex].uri = resultUri;
                      //         });
                      // }
                      else {
                          return item
                      }

                  })
              }
          }
        }
        Promise.all(promises)
            .then(() => {
                let count = 0
                Utility.log("photos.......", JSON.stringify(allImages))
                
                if (allImages.length > 0) {
                    Utility.getValueFromAsyncStorage(Constants.DEALER_ID_HASH).then((dealer_id_hash) => {
                    for (j = 0; j < allImages.length; j = j + 1) {
                        obj = allImages[j]
                        index = obj.uri.lastIndexOf("/") + 1;
                        filename = obj.uri.substr(index);

                        dbFunctions.addDocumentImage(allDocuments[j].parent_id.toString(),allDocuments[j].id.toString(), lead_id.toString(), filename, obj.uri, Constants.IMAGE_INITIAL_STATUS,dealer_id_hash,Constants.NOT_CALCULATOR_IMAGE).then((result) => {
                            count++
                            Utility.log('Image insertion count', count);
                            if(count == allImages.length){
                                let imageUploadFile = new DocumentImageUploadFile()
                                imageUploadFile.checkAndUpload(lead_id)  
                                Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_ATTACHMENT_SUBMIT,{total_docs : count});//----- log loan event
                                this.props.navigation.navigate("congratulations",{loanId: lead_id})
                            }
                        })
                    }
                })
                    
                }
            })
        
    }


    

    onDocumentSelected = async () => {
        let docs
        if(Platform.OS === 'android'){
            Utility.pickDocument().then((docs) => {
                if(this.checkDocumentDuplicacy(docs))
                    this.processDocuments(docs)
                else
                    Utility.showToast(Strings.PDF_ALREADY_SELECTED)
            })
        }
        else{
            docs = await Utility.pickDocumentIOS()
            if(this.checkDocumentDuplicacy(docs))
                this.processDocuments(docs)
            else
                Utility.showToast(Strings.PDF_ALREADY_SELECTED)
        }
    };

    checkDocumentDuplicacy = (docs) =>{
        let selectedDocument = this.state.selectedDocument
            // let filterData = this.state.document_list.filter((item) => {
            //     if (selectedDocument.id == item.id) {
            //         return item
            //     }
            // })
        
            let data = selectedDocument.images;
            let index = data.indexOf(docs[0].uri);
            Utility.log("Duplicacy::"+JSON.stringify(docs)+":"+JSON.stringify(data)+":"+index)
            if(index != -1){
                return false
            }
            else{
                return true
            }
    }

    processDocuments = (docs) =>{
        let selectedDocument = this.state.selectedDocument
        if(selectedDocument.images.length < selectedDocument.max_upload){
        if(docs && docs.length){
            // let filterData = this.state.document_list.filter((item) => {
            //     if (selectedDocument.id == item.id) {
            //         return item
            //     }
            //     //return item; 
            // })
        
            let data = selectedDocument.images;//this.state.stockPhotos;
            for (let i = 0; i < docs.length; i++) {
                data.push(docs[i].uri)
            }
            
            //data = data.concat(photos);
            let selectedItems = []
            for (let i = 0; i < data.length; i++) {
                let singleData = {
                    type: 'image/jpeg',
                    location: { latitude: 0, longitude: 0 },
                    timestamp: 0,
                    height: 0,
                    width: 0,
                    uri: data[i]
                };
                selectedItems.push(singleData);
            }

            // for (let i = 0; i < docs.length; i++) {
            //     let singleData = {
            //         type: 'image/jpeg',
            //         location: { latitude: 0, longitude: 0 },
            //         timestamp: 0,
            //         height: 0,
            //         width: 0,
            //         uri: docs[i].uri
            //     };
            //     selectedItems.push(singleData);
            // }
            let filterData = this.state.document_list.filter((item) => {
              if(this.state.isChild){
                if (selectedDocument.parent_id == item.id) {
                  for(let k = 0; k < item.child.length; k++){
                    if(selectedDocument.id == item.child[k].id){
                      item.child[k].selectedImages = selectedItems;
                      item.child[k].images = data
                    }
                  }
                }
              }else{
                if (selectedDocument.id == item.id) {
                    item.selectedImages = selectedItems;
                    item.images = data
                }
              }
                return item;
            })
            this.setState({ document_list: filterData,transparentModalVisible: false })
        }
        
        else{
            this.setState({ transparentModalVisible: false })
        }
    }else{
        this.setState({ transparentModalVisible: false })
        if(Platform.OS == "ios"){
          setTimeout(() => {
            Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + selectedDocument.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
          }, 600)
        }
        else
          Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + selectedDocument.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
    }
    }

    render() {
        return (
            <AttachmentsScreen
                ref={'attachmentsScreen'}
                onBackPress={this.onBackPress}
                stateData={this.state}
                clickOnSubmit={this.clickOnSubmit}
                onCameraPress={this.onCameraPress}
                onGalleryPress={this.onGalleryPress}
                setTransparentModalVisible={this.setTransparentModalVisible}
                onViewAllPress={this.onViewAllPress}
                onDocumentSelected = {this.onDocumentSelected}
            />
        )
    }
}
