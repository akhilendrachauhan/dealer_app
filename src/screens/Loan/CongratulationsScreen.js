import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Text,
    StyleSheet,BackHandler
    
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import { Utility,AnalyticsConstants } from '../../util';
export default class CongratulationsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loanId : props.navigation.state.params.loanId ? props.navigation.state.params.loanId : ''
        }
    }

    onBackPress = () => {
        //this.props.navigation.goBack()
        this.props.navigation.navigate("loanDashboard")
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.CONGRATULATIONS_SCREEN)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.onBackPress(); // works best when the goBack is async
        return true;
    }

    
    render() {
        let actionBarProps = {
            values: {title: Strings.REQUEST_SUBMITTED},
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <View style={{flex: 1, justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:Colors.PRIMARY,marginBottom:30, fontFamily: Strings.APP_FONT,fontSize: Dimens.text_xx_large,fontWeight:'bold'}}>{Strings.THANK_YOU_WITH_LOAN_ID} {this.state.loanId}</Text>
                    <Text style={{marginBottom:0,lineHeight:30,textAlign:'center', fontFamily: Strings.APP_FONT,fontSize: Dimens.text_xx_large,fontWeight:'500'}}>{Strings.LOAN_SUBMIT_MESSAGE}</Text>
                    {/* <Text style={{fontFamily: Strings.APP_FONT,lineHeight:30, fontSize: Dimens.text_x_large}}>Documents are uploading... </Text> */}

                    </View>
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        paddingBottom: 50,
    },
    
});
