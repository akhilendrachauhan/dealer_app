import React, { Component } from 'react';
import { BackHandler } from 'react-native'
import CustomerDetailScreen from './CustomerDetailScreen'
import { Utility, Constants, AnalyticsConstants } from '../../util';
import { Strings } from "../../values"
import DocumentImageUploadFile from '../../util/DocumentImageUploadFile'
import { addLoanLead } from '../../api/APICalls'
import ImageResizer from 'react-native-image-resizer';
import * as RNFS from "react-native-fs";
import DBFunctions from "../../database/DBFunctions";
let customerData,leadId;
export default class CustomerDetailContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stockItem: props.navigation.state.params.stockItem,
            financier: props.navigation.state.params.item,
            configData: props.navigation.state.params.configData,
            ruleEngineData: props.navigation.state.params.ruleEngineData ? props.navigation.state.params.ruleEngineData : null,
            approvedQuoteData: props.navigation.state.params.approvedQuoteData ? props.navigation.state.params.approvedQuoteData : null,
            name: undefined,
            phone: undefined,
            altPhone: undefined,
            selectedTypeText: undefined,
            selectedTypeValue: undefined,
            selectedLeadSourceText: undefined,
            selectedLeadSourceValue: undefined,
            selectedAgentNameText: undefined,
            selectedAgentNameValue: undefined,
            selectedFinancierBranchText: undefined,
            selectedFinancierBranchValue: undefined,
            notes: undefined,
            showTypeDialog: false,
            showLeadSourceDialog: false,
            showFinancierBranchDialog: false,
            showAgentNameDialog: false,
            agentListOriginal: props.navigation.state.params.configData.agent_list,
            typeData: [
                { text: 'New Application', value: '1' },
                { text: 'Resubmission', value: '2' },
            ],
            // checkboxList : [
            //     {id:'1',text:'Checkbox1',isChecked : false},
            //     {id:'2',text:'Checkbox2',isChecked : false},
            //     {id:'3',text:'Checkbox3',isChecked : false},
            //     {id:'4',text:'Checkbox4',isChecked : false},

            // ],
            checkboxList: props.navigation.state.params.configData.declaration,
            showLoading: false,
            isAgentListEditable: true,
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            altCountryCode: Constants.DEFAULT_COUNTRY_CODE,
            countryCodeList: [],
            loanTypeList: props.navigation.state.params.configData.loan_type,
            selectedLoanTypeValue: undefined,//props.navigation.state.params.configData.loan_type[0].value,
            selectedLoanTypeText: undefined,//props.navigation.state.params.configData.loan_type[0].label,
            showLoanTypeDialog: false,
            isAllChecked : false,
            isImageBeUploaded : false,
        }
    }

    onBackPress = () => {
        let customerData = {
            name: this.state.name,
            phone: this.state.phone,
            countryCode: this.state.countryCode,
            altCountryCode: this.state.altCountryCode,
            altPhone: this.state.altPhone,
            applicationType: this.state.selectedTypeValue,
            applicationTypeText: this.state.selectedTypeText,
            leadSource: this.state.selectedLeadSourceValue,
            leadSourceText: this.state.selectedLeadSourceText,
            agentName: this.state.selectedAgentNameValue,
            agentNameText: this.state.selectedAgentNameText,
            financierBranch: this.state.selectedFinancierBranchValue,
            financierBranchText: this.state.selectedFinancierBranchText,
            notes: this.state.notes,
            declaration: this.state.checkboxList,
            selectedLoanTypeText: this.state.selectedLoanTypeText,
            selectedLoanTypeValue: this.state.selectedLoanTypeValue,
            isAllChecked : this.state.isAllChecked
        }
        Utility.setValueInAsyncStorage(Constants.CUSTOMER_DATA, JSON.stringify(customerData))
        this.props.navigation.goBack();
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.CUSTOMER_DETAIL_SCREEN)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.getValueFromAsyncStorage(Constants.CUSTOMER_DATA).then((customerData) => {
            if (customerData && customerData != '') {
                customerData = JSON.parse(customerData)
                // let isAllChecked = true
                // let filterData = customerData.declaration.filter((item) => {
                //     if(!item.isChecked){
                //         isAllChecked = false
                //     }
                //     return item;
                // })
                this.setState({
                    name: customerData.name,
                    phone: customerData.phone,
                    countryCode: customerData.countryCode,
                    altCountryCode: customerData.altCountryCode,
                    altPhone: customerData.altPhone,
                    selectedTypeText: customerData.applicationTypeText,
                    selectedTypeValue: customerData.applicationType,
                    selectedLeadSourceText: customerData.leadSourceText,
                    selectedLeadSourceValue: customerData.leadSource,
                    selectedAgentNameText: customerData.agentNameText,
                    selectedAgentNameValue: customerData.agentName,
                    selectedFinancierBranchText: customerData.financierBranchText,
                    selectedFinancierBranchValue: customerData.financierBranch,
                    notes: customerData.notes,
                    checkboxList: customerData.declaration,
                    selectedLoanTypeText: customerData.selectedLoanTypeText,
                    selectedLoanTypeValue: customerData.selectedLoanTypeValue,
                    isAllChecked : customerData.isAllChecked
                })
            }
        }).catch((error) => {
            Utility.log("errrrrrrrrrrrrrrrrrrrrr", 'error');
        });

        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeList: countryCodeData })
        Utility.log("ruleenginedata"+JSON.stringify(this.state.ruleEngineData))
    }

    handleBackPress = () => {

        if (this.state.showTypeDialog || this.state.showLeadSourceDialog || this.state.showFinancierBranchDialog || this.state.showAgentNameDialog) {
            this.setState({
                showTypeDialog: false,
                showLeadSourceDialog: false,
                showFinancierBranchDialog: false,
                showAgentNameDialog: false,
            })
        }
        else {
            this.onBackPress();
        }

        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }
    onNameChange = (text) => {
        this.refs['custometDetailScreen'].TL_NAME.setError("")
        this.setState({ name: Utility.onlyAlphabet(text) })
    }

    onPhoneChange = (text) => {
        this.refs['custometDetailScreen'].TL_PHONE.setError("")
        this.setState({ phone: text })
    }

    onCountryCodeChange = (value) => {
        this.setState({ countryCode: value })
    }

    onAltCountryCodeChange = (value) => {
        this.setState({ altCountryCode: value })
    }

    onAltPhoneChange = (text) => {
        this.refs['custometDetailScreen'].TL_ALT_PHONE.setError("")
        this.setState({ altPhone: text })
    }

    onNotesChange = (text) => {
        this.refs['custometDetailScreen'].TL_NOTES.setError("")
        this.setState({ notes: text })
    }

    onTypePress = () => {
        this.setState({ showTypeDialog: true })
    }

    onTypeSelected = (text, value) => {
        this.refs['custometDetailScreen'].TL_TYPE.setError("")
        this.setState({ showTypeDialog: false, selectedTypeText: text, selectedTypeValue: value })
    };

    onLoanTypePress = () => {
        this.setState({ showLoanTypeDialog: true })
    }

    onLoanTypeSelected = (text, value) => {
        this.refs['custometDetailScreen'].TL_LOAN_TYPE.setError("")
        this.setState({ showLoanTypeDialog: false, selectedLoanTypeText: text, selectedLoanTypeValue: value })
    };

    onLeadSourcePress = () => {

        this.setState({ showLeadSourceDialog: true })
    }

    onLeadSourceSelected = (text, value) => {
        this.refs['custometDetailScreen'].TL_LEAD_SOURCE.setError("")
        let filterData = this.state.agentListOriginal.filter((item) => {
            if (item.lead_source.id == value) {
                return item;
            }
        })
        this.state.configData.agent_list = filterData
        
        // if (value == 1) {
        //     let filterData = this.state.agentListOriginal.filter((item) => {
        //         if (item.id == 1) {
        //             return item;
        //         }
        //     })
        //     this.setState({ selectedAgentNameText: filterData[0].name, selectedAgentNameValue: filterData[0].id, isAgentListEditable: false, showLeadSourceDialog: false, selectedLeadSourceText: text, selectedLeadSourceValue: value })
        // }
        // else {
        //     this.setState({ selectedAgentNameText: undefined, selectedAgentNameValue: undefined, isAgentListEditable: true, showLeadSourceDialog: false, selectedLeadSourceText: text, selectedLeadSourceValue: value })
        // }
        if(this.state.configData.agent_list.length == 1)
            this.setState({ selectedAgentNameText: filterData[0].name, selectedAgentNameValue: filterData[0].id, isAgentListEditable: false, showLeadSourceDialog: false, selectedLeadSourceText: text, selectedLeadSourceValue: value })
        else
            this.setState({ selectedAgentNameText: undefined, selectedAgentNameValue: undefined, isAgentListEditable: true, showLeadSourceDialog: false, selectedLeadSourceText: text, selectedLeadSourceValue: value })

    };

    onAgentNamePress = () => {
        // let filterData = this.state.configData.agent_list.filter((item) => {
        //     if (item.id != 1) {
        //         return item;
        //     }
        // })
        // this.state.configData.agent_list = filterData
        this.setState({ showAgentNameDialog: true })
    }

    onAgentNameSelected = (text, value) => {
        this.refs['custometDetailScreen'].TL_AGENT_NAME.setError("")
        this.setState({ showAgentNameDialog: false, selectedAgentNameText: text, selectedAgentNameValue: value })
    };

    onFinancierBranchPress = () => {
        this.setState({ showFinancierBranchDialog: true })
    }

    onFinancierBranchSelected = (text, value) => {
        this.refs['custometDetailScreen'].TL_FINANCIER_BRANCH.setError("")
        this.setState({ showFinancierBranchDialog: false, selectedFinancierBranchText: text, selectedFinancierBranchValue: value })
    };

    hideDialog = () => {
        this.setState({
            showTypeDialog: false,
            showLeadSourceDialog: false,
            showAgentNameDialog: false,
            showFinancierBranchDialog: false,
            showLoanTypeDialog: false,
        })
    };
    onCheckboxClick = (itemTobeChecked) => {
        //alert(JSON.stringify(itemTobeChecked))
        let isAllChecked = true
        let filterData = this.state.checkboxList.filter((item) => {
            if (itemTobeChecked.name == item.name) {
                item.isChecked = !item.isChecked;
            }
            if(!item.isChecked){
                isAllChecked = false
            }
            return item;
        })

        this.setState({ checkboxList: filterData,isAllChecked : isAllChecked })
    }
    clickOnNext = () => {
        //this.addLoanLead()
        Utility.log(JSON.stringify(this.state.approvedQuoteData))
        let checked = true, checkboxItem;
        for (let i = 0; i < this.state.checkboxList.length; i++) {
            checkboxItem = this.state.checkboxList[i]
            if (checkboxItem.isChecked == false) {
                checked = false
            }
        }
        if (Utility.isValueNullOrEmpty(this.state.name)) {
            this.refs['custometDetailScreen'].TL_NAME.setError(Strings.ENTER_NAME)
            this.refs['custometDetailScreen'].nameInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(this.state.phone)) {
            this.refs['custometDetailScreen'].TL_PHONE.setError(Strings.ENTER_PHONE)
            this.refs['custometDetailScreen'].phoneInput.focus()
            return
        }
        else if (!Utility.validateMobileNo(this.state.phone, this.state.countryCode, this.state.countryCodeList)) {
            this.refs['custometDetailScreen'].TL_PHONE.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['custometDetailScreen'].phoneInput.focus()
            return
        }
        else if (!Utility.isValueNullOrEmpty(this.state.altPhone) && !Utility.validateMobileNo(this.state.altPhone, this.state.altCountryCode, this.state.countryCodeList)) {
            this.refs['custometDetailScreen'].TL_ALT_PHONE.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['custometDetailScreen'].altPhoneInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(this.state.selectedTypeValue)) {
            this.refs['custometDetailScreen'].TL_TYPE.setError(Strings.SELECT_TYPE)
            this.refs['custometDetailScreen'].typeInput.focus()
            return
        }
        else if (this.state.financier.has_loan_type == "1" && Utility.isValueNullOrEmpty(this.state.selectedLoanTypeValue)) {
            this.refs['custometDetailScreen'].TL_LOAN_TYPE.setError(Strings.SELECT_LOAN_TYPE)
            this.refs['custometDetailScreen'].loanTypeInput.focus()
            return
        }

        else if (Utility.isValueNullOrEmpty(this.state.selectedLeadSourceValue ? this.state.selectedLeadSourceValue.toString() : this.state.selectedLeadSourceValue)) {
            this.refs['custometDetailScreen'].TL_LEAD_SOURCE.setError(Strings.SELECT_LEAD_SOURCE)
            this.refs['custometDetailScreen'].leadSourceInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(this.state.selectedAgentNameValue ? this.state.selectedAgentNameValue.toString() : this.state.selectedAgentNameValue)) {
            this.refs['custometDetailScreen'].TL_AGENT_NAME.setError(Strings.SELECT_AGENT_NAME)
            this.refs['custometDetailScreen'].agentNameInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(this.state.selectedFinancierBranchValue ? this.state.selectedFinancierBranchValue.toString() : this.state.selectedFinancierBranchValue)) {
            this.refs['custometDetailScreen'].TL_FINANCIER_BRANCH.setError(Strings.SELECT_FINANCIER_BRANCH)
            this.refs['custometDetailScreen'].financierBranchInput.focus()
            return
        }
        else if (!checked) {
            Utility.showToast(Strings.MARK_ALL_DECLARATIONS)
            return
        }

        else {
            
            let objectItem, objectsItemArray = []
            for (let i = 0; i < this.state.checkboxList.length; i++) {
                item = this.state.checkboxList[i]
                objectItem = {
                    'name': item.name,
                    'isChecked': item.isChecked
                }
                objectsItemArray.push(objectItem)
            }
            customerData = {
                name: this.state.name,
                phone: this.state.phone == undefined ? this.state.phone : this.state.countryCode + this.state.phone,
                altPhone: this.state.altPhone == undefined ? this.state.altPhone : this.state.altCountryCode + this.state.altPhone,
                applicationType: this.state.selectedTypeValue,
                leadSource: this.state.selectedLeadSourceValue,
                agentName: this.state.selectedAgentNameValue,
                financierBranch: this.state.selectedFinancierBranchValue,
                notes: this.state.notes,
                declaration: objectsItemArray,
                loanType: this.state.financier.has_loan_type == "1" ? this.state.selectedLoanTypeValue : this.state.loanTypeList[0].value
            }
            // var pattern = new RegExp('^(https?|ftp)://');
            // if (this.state.approvedQuoteData.imageToUpload && !pattern.test(this.state.approvedQuoteData.imageToUpload)) {
            //     RNFS.exists(this.state.approvedQuoteData.imageToUpload).then((exists) =>{
            //         Utility.log('FILE exist'+exists)
            //         if(exists){
            //             this.setState({
            //                 showLoading: true,
            //             })
            //             ImageResizer.createResizedImage(this.state.approvedQuoteData.imageToUpload, Constants.COMPRESS_WIDTH, Constants.COMPRESS_HEIGHT, 'JPEG', Constants.COMPRESS_RATIO)
            //             .then(({ uri }) => {
            //                 let imageUploadFile = new SingleImageUpload()
            //                 imageUploadFile.uploadImage(this.state.approvedQuoteData.imageToUpload, this.imageCallback)
            //             })
            //         }
            //         else{
            //             Utility.showToast("Selected calculator image not exists. Please select another image to proceed")
            //         }
            //     })
            // }
            // else {
                var pattern = new RegExp('^(https?|ftp)://');
                if (this.state.approvedQuoteData && this.state.approvedQuoteData.imageToUpload && !pattern.test(this.state.approvedQuoteData.imageToUpload)) {
                    RNFS.exists(this.state.approvedQuoteData.imageToUpload).then((exists) =>{
                        Utility.log('FILE exist'+exists)
                        if(exists){
                            this.setState({
                                showLoading: true,
                                isImageBeUploaded : true,
                            },()=>{
                                this.addLoanLead()
                            })
                        }
                        else{
                            Utility.showToast("Selected calculator image not exists. Please select another image to proceed")
                        }
                    })
                }
                else{
                    this.setState({
                        showLoading: true,
                        isImageBeUploaded : false,
                    },()=>{
                        this.addLoanLead()
                    })
                }
            //}
            Utility.log("values:" + this.state.name + ":" + this.state.selectedFinancierBranchValue + ":" + this.state.phone + ":" + this.state.selectedLeadSourceValue)
            Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_CUSTOMER_DETAIL_SUBMIT,{cust_name : this.state.name,cust_phone : this.state.phone ,  financier : this.state.financier.name});//----- log loan event
        }
    }

    imageCallback = async (url,status) => {
        if(status != 200){
            this.setState({
                showLoading: false,
            },()=>{
                Utility.showToast(url)
            })
            
        }
        else{
            let dbFunctions = new DBFunctions();
            this.state.approvedQuoteData.imageToUpload = url
            let filterData = this.state.configData.document_list.filter((item) => {
                if(item.origin == Constants.CALCULATOR_IMAGE_SCREEN) {
                    return item; 
                }
            })
            let selectedDocument = filterData[0]
            let index = url.lastIndexOf("/") + 1;
            let filename = url.substr(index);
            Utility.getValueFromAsyncStorage(Constants.DEALER_ID_HASH).then((dealer_id_hash) => {
                dbFunctions.addDocumentImage(selectedDocument.parent_id.toString(),selectedDocument.id.toString(), leadId.toString(), filename, url, Constants.IMAGE_S3_UPLOAD_STATUS,dealer_id_hash).then((result) => {
                    Utility.log('Image insertion count', result);
                })
            })
            
            
        }
    }

    saveImageToDB = () => {
        let dbFunctions = new DBFunctions();
        let url = this.state.approvedQuoteData.imageToUpload
        let filterData = this.state.configData.document_list.filter((item) => {
            if(item.origin == Constants.CALCULATOR_IMAGE_SCREEN) {
                return item; 
            }
        })
        let selectedDocument = filterData[0]
        let index = url.lastIndexOf("/") + 1;
        let filename = url.substr(index);
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID_HASH).then((dealer_id_hash) => {
            dbFunctions.addDocumentImage(selectedDocument.parent_id.toString(),selectedDocument.id.toString(), leadId.toString(), filename, url, Constants.IMAGE_INITIAL_STATUS,dealer_id_hash,Constants.CALCULATOR_IMAGE).then((result) => {
                Utility.log('Image insertion count', result);
                let imageUploadFile = new DocumentImageUploadFile()
                imageUploadFile.checkAndUpload(leadId,true)
            })
        })
    }

    addLoanLead = () => {

        try {
            let keys = [Constants.USER_ID,Constants.SFA_USER_DATA];
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                //alert(values[1][1]+"::"+JSON.parse(values[1][1]).userId)

                let newRuleEngineData = this.state.ruleEngineData
                if(this.state.ruleEngineData)
                    newRuleEngineData.loan_needed = newRuleEngineData.updatedLoanNeeded
                let sfa = values[1][1] ? JSON.parse(values[1][1]) : null
                addLoanLead(values[0][1],sfa, this.state.stockItem, this.state.financier, newRuleEngineData, this.state.approvedQuoteData, customerData, this.onSuccess, this.onFailure, this.props);
            });
        } catch (error) {
            Utility.log(error)
        }


    }


    onSuccess = (response) => {
        this.setState({ showLoading: false, })

        if (response && response.data) {
            Utility.log("hellloooo" + JSON.stringify(response.data))
            let check_for_mrp = false
            if(this.state.ruleEngineData && this.state.financier.has_rule_engine == "1" && this.state.ruleEngineData.mrp == 0){
                check_for_mrp = true
            }
            this.props.navigation.navigate("attachments", { document_list: this.state.configData.document_list, financier: this.state.financier, leadId: response.data.lead_id , check_for_mrp : check_for_mrp })
            // this.setState({carPrice : response.data.car_price})
            leadId = response.data.lead_id
            Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_SUBMIT_SUCCESS,{ loan_id : response.data.lead_id.toString()});//----- log loan event
            let keys = [Constants.SFA_USER_DATA];
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
            promise.then(values => {
                let sfa = values[0][1] ? JSON.parse(values[0][1]) : null
                if(sfa){
                    Utility.sendEvent(AnalyticsConstants.LOAN_BY_SFA,{ loan_id : response.data.lead_id});//----- log loan event
                }
                else{
                    Utility.sendEvent(AnalyticsConstants.LOAN_BY_DEALER,{ loan_id : response.data.lead_id});//----- log loan event
                }
            });
            if(this.state.isImageBeUploaded)
                this.saveImageToDB()
            
        }


    }

    calculatorImageUpload = () =>{
        ImageResizer.createResizedImage(this.state.approvedQuoteData.imageToUpload, Constants.COMPRESS_WIDTH, Constants.COMPRESS_HEIGHT, 'JPEG', Constants.COMPRESS_RATIO)
        .then(({ uri }) => {
            let imageUploadFile = new SingleImageUpload()
            imageUploadFile.uploadImage(this.state.approvedQuoteData.imageToUpload, this.imageCallback)
        })
    }
    onFailure = (response) => {
        //alert(JSON.stringify(response))
        this.setState({ showLoading: false, })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        Utility.sendEvent(AnalyticsConstants.LOAN_APPLICATION_SUBMIT_FAILED,{ failure_msg : response.message });//----- log loan event
    };

    onAllCheckboxClick = () =>{
        if(this.state.isAllChecked){
            let filterData = this.state.checkboxList.filter((item) => {
                item.isChecked = false
                return item;
            })

            this.setState({ checkboxList: filterData,isAllChecked : false })
        }
        else{
            let filterData = this.state.checkboxList.filter((item) => {
                item.isChecked = true
                return item;
            })

            this.setState({ checkboxList: filterData,isAllChecked : true })
        }
    }

    render() {
        return (
            <CustomerDetailScreen
                ref={'custometDetailScreen'}
                onBackPress={this.onBackPress}
                stateData={this.state}
                onNameChange={this.onNameChange}
                onPhoneChange={this.onPhoneChange}
                onCountryCodeChange={this.onCountryCodeChange}
                onAltCountryCodeChange={this.onAltCountryCodeChange}
                onAltPhoneChange={this.onAltPhoneChange}
                onTypePress={this.onTypePress}
                onTypeSelected={this.onTypeSelected}
                onLeadSourcePress={this.onLeadSourcePress}
                onLeadSourceSelected={this.onLeadSourceSelected}
                onAgentNamePress={this.onAgentNamePress}
                onAgentNameSelected={this.onAgentNameSelected}
                onFinancierBranchPress={this.onFinancierBranchPress}
                onFinancierBranchSelected={this.onFinancierBranchSelected}
                hideDialog={this.hideDialog}
                clickOnNext={this.clickOnNext}
                onNotesChange={this.onNotesChange}
                onCheckboxClick={this.onCheckboxClick}
                onLoanTypePress={this.onLoanTypePress}
                onLoanTypeSelected={this.onLoanTypeSelected}
                onAllCheckboxClick = {this.onAllCheckboxClick}
            />
        )
    }
}
