import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native'
import FinancierListScreen from './FinancierListScreen'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import { getLoanConfig } from '../../api/APICalls'
import * as ScreenHoc from '../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import { ScreenStates, ScreenName } from '../../util/Constants'
import { icon_back_arrow, icon_cd_logo } from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper";

export default class FinancierListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            financierList: [],
            stockItem: props.navigation.state.params.item,
            configData: null,
            screenState: ScreenStates.NO_ERROR,
            showLoading: false,
            isLoanShareQuote: this.props.navigation.state.params.isLoanShareQuote
        }
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.FINANCIER_LIST_SCREEN)
        this.getLoanConfig();
    }

    getLoanConfig = () => {
        try {
            let keys = [Constants.SFA_USER_DATA]
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys)
            promise.then(values => {
                let sfa = values[0][1] ? JSON.parse(values[0][1]) : null
                getLoanConfig(sfa, this.onSuccessLoanConfig, this.onFailureLoanConfig, this.props);
            })
        } catch (error) {
            Utility.log(error);
        }
        this.setState({ screenState: ScreenStates.IS_LOADING })
    }

    onSuccessLoanConfig = (response) => {
        if (response && response.data) {
            Utility.log("hellloooo" + JSON.stringify(response.data.document_list))
            this.setState({
                screenState: ScreenStates.NO_ERROR,
                configData: response.data,
                financierList: response.data.financier_list
            })
        }
        else
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND, showLoading: false })
    }

    onFailureLoanConfig = (response) => {
        this.setState({ screenState: ScreenStates.NO_DATA_FOUND, showLoading: false, })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }

    onRetryClick = () => {
        this.getLoanConfig();
    }

    onListItemClick = (item) => {
        Utility.removeItemValue(Constants.CUSTOMER_DATA)
        Utility.removeItemValue(Constants.APPROVED_DATA)
        if (item.has_rule_engine == "1")
            //this.props.navigation.navigate("ruleEngineInput",{item: item,configData : this.state.configData,stockItem : this.state.stockItem})
            this.props.navigation.navigate("calculatorForm", { item: item, isLoanShareQuote: this.state.isLoanShareQuote, configData: this.state.configData, stockItem: this.state.stockItem })
        else
            this.props.navigation.navigate("approvedQuote", { item: item, configData: this.state.configData, stockItem: this.state.stockItem })
    }

    render() {
        this.actionBarProps = {
            values: {
                title: Strings.SELECT_FINANCIER
            },
            rightIcons: [],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        }
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={this.actionBarProps.values}
                        actions={this.actionBarProps.actions}
                        iconMap={this.actionBarProps.rightIcons}
                        styleAttributes={this.actionBarProps.styleAttr}
                    />
                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>

                        <FinancierListScreen
                            ref={'financerListScreen'}
                            onBackPress={this.onBackPress}
                            financierList={this.state.financierList}
                            onListItemClick={this.onListItemClick}
                            isLoanShareQuote={this.state.isLoanShareQuote}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})