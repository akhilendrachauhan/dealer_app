import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput,
    Platform,KeyboardAvoidingView,Keyboard
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import TextInputLayout from "../../granulars/TextInputLayout"
import * as Utility from '../../util/Utility';
import { Dropdown } from 'react-native-material-dropdown';
import LoadingComponent from './../../granulars/LoadingComponent'
export default class RuleEngineInputScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight : 0,
            inputHeight : 50,
        }
    }
    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }
    renderTenureList = () => {
        if (this.props.stateData.configData.loan_tenure && this.props.stateData.configData.loan_tenure.length > 0) {
            let tenureData = this.props.stateData.configData.loan_tenure;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {tenureData.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onTenureSelected(item.toString(),item)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderRegionList = () => {
        if (this.props.stateData.configData.region_list && this.props.stateData.configData.region_list.length > 0) {
            let regionData = this.props.stateData.configData.region_list;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {regionData.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onRegionSelected(item.name,item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.name}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderInsuranceList = () => {
        if (this.props.stateData.configData.insurance_type && this.props.stateData.configData.insurance_type.length > 0) {
            let insurance_type = this.props.stateData.configData.insurance_type;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {insurance_type.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onInsuranceSelected(item.label,item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.value}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.label}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderObjectList = () => {
        if (this.props.stateData.configData.vehicle_type && this.props.stateData.configData.vehicle_type.length > 0) {
            let vehicle_type = this.props.stateData.configData.vehicle_type;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {vehicle_type.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onObjectSelected(item.object_type,item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.object_type}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderGroupList = () => {
        if (this.props.stateData.configData.vehicle_category && this.props.stateData.configData.vehicle_category.length > 0) {
            let vehicle_category = this.props.stateData.configData.vehicle_category;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {vehicle_category.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onGroupSelected(item.car_group,item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.car_group}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };


    render() {
        let actionBarProps = {
            values: {title: this.props.stateData.financierName},
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <ScrollView  style={{marginBottom: 30}}
                        keyboardShouldPersistTaps='handled'>
                        <KeyboardAvoidingView style={{flex:1,flexDirection: 'column', margin: Dimens.margin_normal, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight}}>
                            <View style={{flex: 1, flexDirection: 'column', margin: Dimens.margin_normal}}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onRegionPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_REGION = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedRegionText ? this.props.stateData.selectedRegionText : ''}
                                        placeholder={Strings.REGION+"*"}
                                        ref={(input) => this.regionInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onRegionPress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TextInputLayout
                                ref={(input) => this.TL_CAR_PRICE = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.carPrice ? Utility.formatCurrency(this.props.stateData.carPrice) : ''}
                                    placeholder={Strings.PRICE_OF_CAR+"*"}
                                    ref={(input) => this.carPriceInput = input}
                                    onChangeText={(text) => this.props.onCarPriceChange(text)}
                                    
                                />
                            </TextInputLayout>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onTenurePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_TENURE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedTenureText ? this.props.stateData.selectedTenureText : ''}
                                        placeholder={Strings.TENURE+"*"}
                                        ref={(input) => this.tenureInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onTenurePress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            

                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onInsurancePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_INSURANCE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedInsuranceText ? this.props.stateData.selectedInsuranceText : ''}
                                        placeholder={Strings.INSURANCE_TYPE+"*"}
                                        ref={(input) => this.insuranceInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onInsurancePress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.stateData.isVehicleCategoryEnable ? this.props.onGroupPress() : Utility.log("")}>
                                <TextInputLayout
                                    ref={(input) => this.TL_GROUP = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedGroupText ? this.props.stateData.selectedGroupText : ''}
                                        placeholder={Strings.GROUP+"*"}
                                        ref={(input) => this.groupInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' && this.props.stateData.isVehicleCategoryEnable ? this.props.onGroupPress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onObjectPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_OBJECT = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedObjectText ? this.props.stateData.selectedObjectText : ''}
                                        placeholder={Strings.OBJECT+"*"}
                                        ref={(input) => this.objectInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onObjectPress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TextInputLayout
                                ref={(input) => this.TL_DISBURSEMENT = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.disbursement ? Utility.formatCurrency(this.props.stateData.disbursement) : ''}
                                    placeholder={Strings.DISBURSEMENT_NEEDED+"*"}
                                    ref={(input) => this.disbursementInput = input}
                                    onChangeText={(text) => this.props.onDisbursementChange(text)}
                                    
                                />
                            </TextInputLayout>
                        </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                    { this.props.stateData.showLoading && <LoadingComponent/>}
                    <TouchableOpacity
                            activeOpacity={0.8}
                            style={{width:'100%',position:'absolute',bottom:0, padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                            onPress={() => this.props.clickOnNext()}>

                            <Text style={Styles.buttonText}>{Strings.NEXT}</Text>
                    </TouchableOpacity>
                    {this.props.stateData.showTenureDialog && this.renderTenureList()}
                    {this.props.stateData.showRegionDialog && this.renderRegionList()}
                    {this.props.stateData.showInsuranceDialog && this.renderInsuranceList()}
                    {this.props.stateData.showGroupDialog && this.renderGroupList()}
                    {this.props.stateData.showObjectDialog && this.renderObjectList()}
                   
                    
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    topContainer: {
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_xxx_large,
        margin: Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed'
    },
    textInput: {
        height: 50,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
});
