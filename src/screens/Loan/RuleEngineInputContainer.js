import React, { Component } from 'react';
import RuleEngineInputScreen from './RuleEngineInputScreen'
import { Utility,AnalyticsConstants } from '../../util';
import {Strings} from "../../values"
import {getCarPrice} from '../../api/APICalls'
export default class RuleEngineInputContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            financierName : props.navigation.state.params.item.name,
            financier : props.navigation.state.params.item,
            configData : props.navigation.state.params.configData,
            stockItem : props.navigation.state.params.stockItem,
            offerData : undefined,
            carPrice : undefined,
            selectedTenureText : undefined,
            selectedTenureValue : undefined,
            selectedRegionText : undefined,
            selectedRegionValue : undefined,
            disbursement : undefined,
            showTenureDialog : false,
            showRegionDialog : false,
            showLoading : false,
            showInsuranceDialog : false,
            showObjectDialog : false,
            showGroupDialog : false,
            selectedInsuranceText : undefined,
            selectedInsuranceValue : undefined,
            selectedObjectText : undefined,
            selectedObjectValue : undefined,
            selectedGroupText : undefined,
            selectedGroupValue : undefined,
            isVehicleCategoryEnable : true,
            
        }
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.RULE_ENGINE_SCREEN)
        this.getOffer();
    }
    

    getOffer = () => {
        //alert(JSON.stringify(this.state.stockItem))
        try {
            getCarPrice(this.state.stockItem.id,this.state.stockItem.version_id,this.state.stockItem.make_year,this.state.financier.id,this.onSuccessCarPrice, this.onFailureCarPrice,this.props);
        } catch (error) {
            Utility.log(error);
        }
        this.setState({
            showLoading: true,
        })
       
    }
    

    onSuccessCarPrice = (response) => {
        let isVehicleCategoryEnable = true
        if(response && response.data   )
        {
            Utility.log("hellloooo"+JSON.stringify(response.data))

            let offerData = response.data
            let objectText = undefined,objectValue = undefined,groupText = undefined,groupValue = undefined
            if(offerData.vehicle_category){
                var category = this.state.configData.vehicle_category.filter(obj => {
                    return obj.id === offerData.vehicle_category
                })

                groupText = category[0].car_group
                groupValue = category[0].id
                isVehicleCategoryEnable = false
            }

            if(offerData.vehicle_type){
                var type = this.state.configData.vehicle_type.filter(obj => {
                    return obj.id === offerData.vehicle_type
                })

                objectText = category[0].object_type
                objectValue = category[0].id
            }


            this.setState({isVehicleCategoryEnable : isVehicleCategoryEnable,offerData : response.data,selectedGroupText: groupText,selectedGroupValue:groupValue,selectedObjectText: objectText,selectedObjectValue: objectValue, showLoading : false})
        }
        else
            this.setState({showLoading : false,})

    }
    onFailureCarPrice = (response) => {
        this.setState({showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        
    };

    onCarPriceChange = (text) => {
        this.refs['ruleEngineInputScreen'].TL_CAR_PRICE.setError("")
        this.setState({carPrice : Utility.onlyNumeric(text)})
    }

    onTenurePress = () => {
        this.setState({showTenureDialog: true})
    }
    
    onTenureSelected = (text,value) => {
        this.refs['ruleEngineInputScreen'].TL_TENURE.setError("")
        this.setState({showTenureDialog: false, selectedTenureText: text,selectedTenureValue : value})
    };


    onDisbursementChange = (text) => {
        this.refs['ruleEngineInputScreen'].TL_DISBURSEMENT.setError("")
        this.setState({disbursement : Utility.onlyNumeric(text)})
    }

    onRegionPress = () => {
        this.setState({showRegionDialog: true})
    }

    onRegionSelected = (text,value) => {
        let offerData = this.state.offerData
        var type = offerData.price_data.filter(obj => {
            return obj.region_id === value
        })
        this.refs['ruleEngineInputScreen'].TL_REGION.setError("")
        this.setState({showRegionDialog: false, selectedRegionText: text,selectedRegionValue : value,carPrice : type && type[0] && type[0].price && type[0].price != "0" ? type[0].price : undefined  })
    };

    onInsurancePress = () => {
        this.setState({showInsuranceDialog: true})
    }

    onInsuranceSelected = (text,value) => {
        this.refs['ruleEngineInputScreen'].TL_INSURANCE.setError("")
        this.setState({showInsuranceDialog: false, selectedInsuranceText: text,selectedInsuranceValue : value})
    };

    onObjectPress = () => {
        this.setState({showObjectDialog: true})
    }

    onObjectSelected = (text,value) => {
        this.refs['ruleEngineInputScreen'].TL_OBJECT.setError("")
        this.setState({showObjectDialog: false, selectedObjectText: text,selectedObjectValue : value})
    };

    onGroupPress = () => {
        this.setState({showGroupDialog: true})
    }

    onGroupSelected = (text,value) => {
        this.refs['ruleEngineInputScreen'].TL_GROUP.setError("")
        this.setState({showGroupDialog: false, selectedGroupText: text,selectedGroupValue : value})
    };

    hideDialog = () => {
        this.setState({
            showTenureDialog : false,
            showRegionDialog : false,
            showGroupDialog: false,
            showObjectDialog: false,
            showInsuranceDialog: false,
        })
    };

    clickOnNext = () => {
        if (Utility.isValueNullOrEmpty(this.state.selectedRegionValue ? this.state.selectedRegionValue.toString() : this.state.selectedRegionValue) ) {
            this.refs['ruleEngineInputScreen'].TL_REGION.setError(Strings.SELECT_REGION)
            this.refs['ruleEngineInputScreen'].regionInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.carPrice) ) {
            this.refs['ruleEngineInputScreen'].TL_CAR_PRICE.setError(Strings.ENTER_CAR_PRICE)
            this.refs['ruleEngineInputScreen'].carPriceInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.selectedTenureValue ? this.state.selectedTenureValue.toString() : this.state.selectedTenureValue) ) {
            this.refs['ruleEngineInputScreen'].TL_TENURE.setError(Strings.SELECT_TENURE)
            this.refs['ruleEngineInputScreen'].tenureInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.selectedInsuranceValue ) ) {
            this.refs['ruleEngineInputScreen'].TL_INSURANCE.setError(Strings.SELECT_INSURANCE_TYPE)
            this.refs['ruleEngineInputScreen'].insuranceInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.selectedGroupValue ? this.state.selectedGroupValue.toString() : this.state.selectedGroupValue)  ) {
            this.refs['ruleEngineInputScreen'].TL_GROUP.setError(Strings.SELECT_GROUP)
            this.refs['ruleEngineInputScreen'].groupInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.selectedObjectValue ? this.state.selectedObjectValue.toString() : this.state.selectedObjectValue)  ) {
            this.refs['ruleEngineInputScreen'].TL_OBJECT.setError(Strings.SELECT_OBJECT)
            this.refs['ruleEngineInputScreen'].objectInput.focus()
            return
        } 
        else if (Utility.isValueNullOrEmpty(this.state.disbursement) ) {
            this.refs['ruleEngineInputScreen'].TL_DISBURSEMENT.setError(Strings.ENTER_DISBURSEMENT_NEEDED)
            this.refs['ruleEngineInputScreen'].disbursementInput.focus()
            return
        } 
        else if (parseInt(this.state.disbursement) > parseInt(this.state.carPrice) ) {
            this.refs['ruleEngineInputScreen'].TL_DISBURSEMENT.setError(Strings.DISBURSEMENT_AMOUNT_LESS_THAN_PRICE)
            this.refs['ruleEngineInputScreen'].disbursementInput.focus()
            return
        } 
        else{
            let ruleEngineData = {
                carPrice : this.state.carPrice,
                tenure : this.state.selectedTenureValue,
                region : this.state.selectedRegionValue,
                disbursement : this.state.disbursement,
                insurance : this.state.selectedInsuranceValue,
                group : this.state.selectedGroupValue,
                object : this.state.selectedObjectValue
            }
            Utility.log("values:"+this.state.carPrice+":"+this.state.selectedTenureValue+":"+this.state.selectedRegionValue+":"+this.state.disbursement)
            this.props.navigation.navigate("approvedQuote",{ruleEngineData : ruleEngineData,configData : this.state.configData, item : this.state.financier,stockItem : this.state.stockItem})
        }
    }
    render() {
        return (
            <RuleEngineInputScreen
                ref={'ruleEngineInputScreen'}
                onBackPress={this.onBackPress}
                stateData = {this.state}
                onCarPriceChange = {this.onCarPriceChange}
                onDisbursementChange = {this.onDisbursementChange}
                onRegionPress = {this.onRegionPress}
                onTenurePress = {this.onTenurePress}
                onTenureSelected = {this.onTenureSelected}
                onRegionSelected = {this.onRegionSelected}
                hideDialog = {this.hideDialog}
                clickOnNext = {this.clickOnNext}
                onInsurancePress = {this.onInsurancePress}
                onInsuranceSelected = {this.onInsuranceSelected}
                onObjectPress = {this.onObjectPress}
                onObjectSelected = {this.onObjectSelected}
                onGroupPress = {this.onGroupPress}
                onGroupSelected = {this.onGroupSelected}
                
            />
        )
    }
}
