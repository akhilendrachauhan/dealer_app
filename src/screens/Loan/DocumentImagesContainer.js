import React, {Component} from 'react'
import {BackHandler, Platform} from 'react-native'
import DocumentImagesScreen from "./DocumentImagesScreen";
import * as Utility from "../../util/Utility";
import * as RNFS from "react-native-fs";
import { Strings } from "../../values"
export default class DocumentImagesContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.navigation.state.params.data ? JSON.parse(JSON.stringify(props.navigation.state.params.data)) : [],
            selectedDocument : JSON.parse(JSON.stringify(props.navigation.state.params.selectedDocument)),
            modalVisible: false,
            order: [],
            selectedImages : []
            
        }
    }

    componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.onBackPress()
        return true
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    setModalVisible = (isVisible) => {
        //if(this.state.selectedDocument.images.length < this.state.selectedDocument.max_upload)
        this.setState({modalVisible: isVisible})
        //else
          //  Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + this.state.selectedDocument.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
    };

    onOrderChange = (itemOrder) => {
        //Utility.log('current order', itemOrder);
        let order = itemOrder.itemOrder;
        let newData = [];
        for (let i = 0; i < order.length; i++) {
            newData.push(order[i].ref)
        }
        //Utility.log('new data', newData);
        this.state.data = newData
    };

    onImageCancelPress = (item, index) => {
        Utility.log('111111111beforeDeleted', this.state.data,index);
        let data = this.state.data;
        data = data.filter((item,index1) => {
            if (index != index1) {
                return item;
            }
        })
        //data.splice(index, 1);
        this.state.selectedDocument.images.splice(index,1)
        this.state.selectedDocument.selectedImages.splice(index,1)
        this.setState({data: data,selectedDocument : this.state.selectedDocument});
        Utility.log('111111111afterDeleted', this.state.selectedDocument);
    };

    onCameraPress = () => {
        this.props.navigation.navigate('camera', {
            cameraCallback: this.onCameraDone,
            maxSize: this.state.selectedDocument.max_upload,
            selectedDocument : this.state.selectedDocument
        });
        this.setState({modalVisible: false})
    };

    onCameraDone = (photos,selectedDocument) => {
        let data = this.state.selectedDocument.images;
        data = data.concat(photos);
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: {latitude: 0, longitude: 0},
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        selectedDocument.images = data
        selectedDocument.selectedImages = selectedItems
        this.setState({data: data,selectedDocument: selectedDocument});
        Utility.log(photos)
    };
    
    onGalleryDone = (photos,selectedDocument) => {
        let data = [];
        for (let i = 0; i < photos.length; i++) {
            data.push(photos[i].uri)
        }

        selectedDocument.images = data
        selectedDocument.selectedImages = photos
        this.setState({data: data,selectedDocument: selectedDocument});
        
        // const promises = photos.map((item, index) => {
        //     var pattern = new RegExp('^(https?|ftp)://');
        //     const {uri} = item;
        //     if(pattern.test(uri) ) {
        //         return item
        //     } else if(uri.startsWith("ph:") || uri.startsWith("PH:")){

        //     var regex = /:\/\/(.{36})\//i;
        //     var result = uri.match(regex);
        //     const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
        //     promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
        //     return promise
        //         .then((resultUri) => {
        //             photos[index].uri = resultUri;
        //     });
        //     }
        //     else{
        //         return item
        //     }
        // })
        // Promise.all(promises)
        //     .then(() => {
        //         Utility.log(photos)
        //         selectedDocument.selectedImages = photos
                
        //         this.setState({selectedDocument: selectedDocument});
        // });
    };

    onGalleryPress = () => {
        let data = this.state.selectedDocument.images;
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: {latitude: 0, longitude: 0},
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.props.navigation.navigate("galleryFolder", {
            galleryCallback: this.onGalleryDone,
            selectedItems: selectedItems,
            maxSize: this.state.selectedDocument.max_upload,
            selectedDocument : this.state.selectedDocument
        });
        this.setState({modalVisible: false});
    };

    
    onBackPress = () => {
        this.props.navigation.goBack();
    };

    onDragCompleted = (data) => {
        Utility.log('after drag', data)
    };

    onDonePress = () => {
        this.props.navigation.state.params.onPhotosEditDone && this.props.navigation.state.params.onPhotosEditDone(this.state.selectedDocument);
        this.props.navigation.goBack();
    };
    onImagePress = (data,index) =>{
        this.props.navigation.navigate("imageReviewScreen", {
            data: data,
            index: index
        });
    }
    onDocumentSelected = async () => {
        let docs
        if(Platform.OS === 'android'){
            Utility.pickDocument().then((docs) => {
                if(this.checkDocumentDuplicacy(docs))
                    this.processDocuments(docs)
                else
                    Utility.showToast(Strings.PDF_ALREADY_SELECTED)
            })
        }
        else{
            docs = await Utility.pickDocumentIOS()
            if(this.checkDocumentDuplicacy(docs))
                this.processDocuments(docs)
            else
                Utility.showToast(Strings.PDF_ALREADY_SELECTED)
        }
    };

    checkDocumentDuplicacy = (docs) =>{
        let data = this.state.selectedDocument.images;
        let index = data.indexOf(docs[0].uri);
        Utility.log("Duplicacy::"+JSON.stringify(docs)+":"+JSON.stringify(data)+":"+index)
        if(index != -1){
            return false
        }
        else{
            return true
        }
    }

    processDocuments = (docs) => {
        let data = this.state.selectedDocument.images;
        if(data.length < this.state.selectedDocument.max_upload){
            if(docs && docs.length){
                let data = this.state.selectedDocument.images;
                for (let i = 0; i < docs.length; i++) {
                    data.push(docs[i].uri)
                }
                let selectedItems = []
                for (let i = 0; i < data.length; i++) {
                    let singleData = {
                        type: 'image/jpeg',
                        location: { latitude: 0, longitude: 0 },
                        timestamp: 0,
                        height: 0,
                        width: 0,
                        uri: data[i]
                    };
                    selectedItems.push(singleData);
                }
                this.state.selectedDocument.images = data
                this.state.selectedDocument.selectedImages = selectedItems
                this.setState({ data: data,selectedDocument: this.state.selectedDocument,modalVisible: false })
            }
            else{
                this.setState({ modalVisible: false })
            }
        }else{
            if(Platform.OS == "ios"){
                setTimeout(() => {
                    Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + this.state.selectedDocument.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
                }, 600)
              }
              else
                Utility.showToast(Strings.MAX_SIZE_CHOOSE_MSG1 + this.state.selectedDocument.max_upload + Strings.MAX_SIZE_CHOOSE_MSG3)
            this.setState({ modalVisible: false })
        }
    }
    render() {
        return (
            <DocumentImagesScreen
                data={this.state.data}
                modalVisible={this.state.modalVisible}
                setModalVisible={this.setModalVisible}
                onGalleryPress={this.onGalleryPress}
                onCameraPress={this.onCameraPress}
                onImageCancelPress={this.onImageCancelPress}
                onBackPress={this.onBackPress}
                onDragCompleted={this.onDragCompleted}
                onDonePress={this.onDonePress}
                onOrderChange={this.onOrderChange}
                onImagePress = {this.onImagePress}
                onDocumentSelected = {this.onDocumentSelected}
                />
        )
    }

}
