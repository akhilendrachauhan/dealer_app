import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity, Modal, SafeAreaView
} from 'react-native'
import {Colors, Strings, Dimens} from "../../values";
import SortableGrid from '../../granulars/SortableGrid'
import CameraGalleryButtonsComponent from "../../components/CameraGalleryButtonsComponent";
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import * as Utility from "../../util/Utility";
import { pdf_icon } from '../../assets/ImageAssets';
import { Constants } from '../../util';

let self;
export default class DocumentImagesScreen extends Component {

    constructor() {
        super();
        self = this;
        this.state = {
            scrollEnabled: true
        }
    }

    onDragStart = () => {
        this.setState({scrollEnabled: false})
    }

    onDragRelease = (itemOrder) => {
        this.setState({scrollEnabled: true})
        this.props.onOrderChange(itemOrder)
    }

    render() {
        let imageDimen = (Dimensions.get('window').width - 40) / 3;
        let actionBarProps = {
            values: {title: Strings.PHOTOS},
            rightIcons: [{
                image: require('../../assets/drawable/right.png'),
                onPress: this.props.onDonePress
            }],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        //Utility.log('data', this.props.data)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK  }}>
            <View style={{flex: 1, backgroundColor: Colors.GRAY_LIGHT_BG}}>
                <ActionBarWrapper
                    values={actionBarProps.values}
                    actions={actionBarProps.actions}
                    iconMap={actionBarProps.rightIcons}
                    styleAttributes={actionBarProps.styleAttr}
                />
                <View style={{flex: 1, paddingVertical: Dimens.padding_x_small}}>
                    <ScrollView scrollEnabled={this.state.scrollEnabled}>
                        
                        <View>
                            <SortableGrid
                                blockTransitionDuration      = { 400 }
                                activeBlockCenteringDuration = { 200 }
                                itemsPerRow                  = { 3 }
                                dragActivationTreshold       = { 200 }
                                onDragRelease                = { (itemOrder) => this.onDragRelease(itemOrder) }
                                onDragStart                  = { () => this.onDragStart() }
                            >
                                {
                                    this.props.data.map( (item, index) =>
                                    <TouchableOpacity onPress={() => self.props.onImagePress(this.props.data, index)}
                                            activeOpacity={0.8} ref={item} key={index} uniqueKey={item} style={[styles.block, {
                                        width: imageDimen,
                                        height: imageDimen,
                                        padding :5,
                                    }]}>
                                    <Image source={Utility.checkFileType(item,Constants.PDF_FILE_TYPE) ? pdf_icon : {uri: item}}
                                                   resizeMode={Utility.checkFileType(item,Constants.PDF_FILE_TYPE) ? 'center' :'cover'} style={{width: '100%', height: '100%'}}/>
                                            
                                        <TouchableOpacity onPress={() => self.props.onImageCancelPress(item, index)}
                                                            activeOpacity={0.8} style={{
                                            position: 'absolute',
                                            top: -8,
                                            right: -8,
                                            height: 20,
                                            width: 20,
                                            elevation: 5,
                                            borderRadius: 10,
                                            backgroundColor: Colors.WHITE,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                            <Image resizeMode={'cover'}
                                                    source={require('../../assets/drawable/cancel_icon.png')}
                                                    style={{height: 12, width: 12}}/>
                                        </TouchableOpacity>
                                            
                                    </TouchableOpacity>
                                    )
                                }
                            </SortableGrid>
                                                                                                            
                        </View>

                    </ScrollView>
                    
                </View>

                <TouchableOpacity onPress={() => this.props.setModalVisible(true)} activeOpacity={0.7} style={{
                    borderRadius: 3,
                    marginHorizontal: Dimens.margin_normal,
                    marginBottom: Dimens.margin_normal,
                    height: 42,
                    backgroundColor: Colors.PRIMARY,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                    <Image source={require('../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                           style={{
                               height: Dimens.icon_normal,
                               width: Dimens.icon_normal,
                               marginRight: Dimens.margin_large
                           }}/>
                    <Text style={{
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.ADD_PHOTOS}</Text>
                </TouchableOpacity>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.modalVisible}
                    onRequestClose={() => {
                        this.props.setModalVisible(false)
                    }}
                    onDismiss={() => {
                        this.props.setModalVisible(false)
                    }}>
                    <View style={{flex: 1}}>
                        <TouchableOpacity onPress={() => this.props.setModalVisible(false)}
                                          style={{flex: 1, backgroundColor: Colors.BLACK_40}}/>
                        <CameraGalleryButtonsComponent
                            onCameraPress={this.props.onCameraPress}
                            onGalleryPress={this.props.onGalleryPress}
                            onPDFPress = {this.props.onDocumentSelected}
                            />
                    </View>
                </Modal>
            </View>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    block: {
        margin: 5,
        elevation: 2,
        borderRadius: 3,
        borderWidth: 5,
        borderColor: Colors.WHITE,
        justifyContent: 'center',
        alignItems: 'center'
    },
    triangle: {
        position: 'absolute',
        bottom: -10,
        left: 25,
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderBottomWidth: 12,
        marginRight: 0,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        transform: [
            {rotate: '-180deg'}
        ]
    }
});
