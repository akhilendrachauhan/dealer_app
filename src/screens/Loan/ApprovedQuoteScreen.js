import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput,
    Platform,Keyboard,KeyboardAvoidingView
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import TextInputLayout from "../../granulars/TextInputLayout"
import * as Utility from '../../util/Utility';
import { Dropdown } from 'react-native-material-dropdown';
import {icon_no_image} from '../../assets/ImageAssets'
import CameraGalleryButtonsComponent from "../../components/CameraGalleryButtonsComponent";
import { Constants } from '../../util';
export default class ApprovedQuoteScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight : 0,
            inputHeight : 70,
        }
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }
    
    renderTenureList = () => {
        if (this.props.stateData.configData.loan_tenure && this.props.stateData.configData.loan_tenure.length > 0) {
            let tenureData = this.props.stateData.configData.loan_tenure;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView >
                            <View style={{padding: 15}}>
                                
                                {tenureData.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onTenureSelected(item.toString(),item)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderRegionList = () => {
        if (this.props.stateData.configData.region_list && this.props.stateData.configData.region_list.length > 0) {
            let regionData = this.props.stateData.configData.region_list;
            
            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent:'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                                      style={{backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5}}>
                        <ScrollView>
                            <View style={{padding: 15}}>
                                
                                {regionData.map((item) => {
                                    return <TouchableOpacity
                                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}
                                        onPress={() => this.props.onRegionSelected(item.name,item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.name}
                                        </Text>
                                        
                                    </TouchableOpacity>
                                })}
                                </View>
                               
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderTopView = () => {
        return (
            <View style={Styles.topContainer}>
                <View style={{flexDirection:'row'}}>
                <Text style={{
                    color: this.props.stateData.financier.has_rule_engine != Constants.HAS_RULE_ENGINE ? Colors.PRIMARY : Colors.BLACK,
                    fontSize: Dimens.text_normal,
                    fontWeight: '400',
                    fontFamily: Strings.APP_FONT
                }}>{Strings.UPLOAD_CALCULATOR_IMAGE}</Text>
                <Text style={{
                        color: Colors.PRIMARY,
                        fontSize: Dimens.text_normal,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{this.props.stateData.financier.has_rule_engine != Constants.HAS_RULE_ENGINE ? "*" : ""} </Text>
                
                </View>
                <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',alignItems: 'center'}}>
                <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(true)} activeOpacity={0.7} style={{
                    borderRadius: 3,
                   // width: '100%',
                    height: 42,
                    paddingLeft:10,paddingRight:10,
                    backgroundColor: Colors.PRIMARY,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                    <Image source={require('../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                           style={{
                               height: Dimens.icon_normal,
                               width: Dimens.icon_normal,
                               marginRight: Dimens.margin_large
                           }}/>
                    <Text style={{
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.UPLOAD_IMAGE}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                if(this.props.stateData.selectedStockImages[0] && this.props.stateData.selectedStockImages[0].uri)
                    this.props.gotoImagePreview(this.props.stateData.selectedStockImages[0].uri)
                }
                } activeOpacity={0.8} >
                    <Image source={this.props.stateData.selectedStockImages[0] && this.props.stateData.selectedStockImages[0].uri  ? { uri:this.props.stateData.selectedStockImages[0].uri} : icon_no_image} resizeMode={'contain'}
                        style={{
                            height: 75,
                            width: 75,
                            marginRight: Dimens.margin_large
                    }}/>
                </TouchableOpacity>
                </View>
            </View>
        )
    };

    render() {
        let actionBarProps = {
            values: {title: Strings.APPROVED_QUOTE},
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <ScrollView  style={{marginBottom: 30}}
                        keyboardShouldPersistTaps='handled'
                        ref='_scrollView'>
                        <KeyboardAvoidingView style={{flex:1,flexDirection: 'column', margin: Dimens.margin_normal, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight}}>
                            {/* <Text style={{textAlign:'center',marginVertical:Dimens.margin_normal ,fontFamily: Strings.APP_FONT,fontSize: Dimens.text_large,fontWeight: 'normal',color: Colors.BLACK}}>{Strings.ENTER_APPROVED_QUOTE_DETAILS}</Text> */}
                            { this.props.stateData.financier.has_rule_engine != Constants.HAS_RULE_ENGINE && this.renderTopView()}
                            <TextInputLayout
                                ref={(input) => this.TL_CAR_PRICE = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={this.props.stateData.isEditableField}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.carPrice ? Utility.formatCurrency(this.props.stateData.carPrice) : ''}
                                    placeholder={Strings.APPROVED_PRICE_OF_CAR+"*"}
                                    ref={(input) => this.carPriceInput = input}
                                    onChangeText={(text) => this.props.onCarPriceChange(text)}
                                    
                                />
                            </TextInputLayout>
                            <TextInputLayout
                                ref={(input) => this.TL_TDP_AMOUNT = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={this.props.stateData.isEditableField}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.tdpAmount ? Utility.formatCurrency(this.props.stateData.tdpAmount) : ''}
                                    placeholder={Strings.APPROVED_TDP+"*"}
                                    ref={(input) => this.tdpAmountInput = input}
                                    onChangeText={(text) => this.props.onTDPAmountChange(text)}
                                    
                                />
                            </TextInputLayout>
                            <TextInputLayout
                                ref={(input) => this.TL_EMI_AMOUNT = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={this.props.stateData.isEditableField}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.emiAmount ? Utility.formatCurrency(this.props.stateData.emiAmount) : ''}
                                    placeholder={Strings.APPROVED_EMI+"*"}
                                    ref={(input) => this.emiAmountInput = input}
                                    onChangeText={(text) => this.props.onEMIAmountChange(text)}
                                    
                                />
                            </TextInputLayout>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.stateData.isEditableField ? this.props.onTenurePress() : Utility.log("")}>
                                <TextInputLayout
                                    ref={(input) => this.TL_TENURE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedTenureText ? this.props.stateData.selectedTenureText : ''}
                                        placeholder={Strings.TENURE+"*"}
                                        ref={(input) => this.tenureInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' && this.props.stateData.isEditableField ? this.props.onTenurePress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity>
                            {/* <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onRegionPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_REGION = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedRegionText ? this.props.stateData.selectedRegionText : ''}
                                        placeholder={Strings.REGION+"*"}
                                        ref={(input) => this.regionInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onRegionPress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity> */}
                            <TextInputLayout
                                ref={(input) => this.TL_AMOUNT_CREDITED= input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={this.props.stateData.isEditableField}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.amountCredited ? Utility.formatCurrency(this.props.stateData.amountCredited) : ''}
                                    placeholder={Strings.APPROVED_AMOUNT_TO_BE_CREDITED+"*"}
                                    ref={(input) => this.amountCreditedInput = input}
                                    onChangeText={(text) => this.props.onAmountCreditedChange(text)}
                                />
                            </TextInputLayout>
                            <TextInputLayout
                                ref={(input) => this.TL_DISBURSEMENT_DEALER= input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.dealerDisbursement ? Utility.formatCurrency(this.props.stateData.dealerDisbursement) : ''}
                                    placeholder={Strings.DISBURSEMENT_NEEDED_BY_DEALER+"*"}
                                    ref={(input) => this.dealerDisbursementInput = input}
                                    onChangeText={(text) => this.props.onDealerDisbursementChange(text)}
                                />
                            </TextInputLayout>
                            <TextInputLayout
                                ref={(input) => this.TL_REAL_TDP = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType = {'number-pad'}
                                    value={this.props.stateData.realTDP ? Utility.formatCurrency(this.props.stateData.realTDP) : ''}
                                    placeholder={Strings.REAL_TDP+"*"}
                                    ref={(input) => this.realTDPInput = input}
                                    onChangeText={(text) => this.props.onRealTDPChange(text)}
                                />
                            </TextInputLayout>
                        </KeyboardAvoidingView>
                        
                    </ScrollView>
                    <TouchableOpacity
                            activeOpacity={0.8}
                            style={{width:'100%',position:'absolute',bottom:0, padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                            onPress={() => {
                                this.refs._scrollView.scrollTo({x: 0, y: 0, animated: true})
                                this.props.clickOnNext()
                            }}>

                            <Text style={Styles.buttonText}>{Strings.NEXT}</Text>
                    </TouchableOpacity>
                    {this.props.stateData.showTenureDialog && this.renderTenureList()}
                    {this.props.stateData.showRegionDialog && this.renderRegionList()}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.stateData.transparentModalVisible}
                        onRequestClose={() => {
                            this.props.setTransparentModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setTransparentModalVisible(false)
                        }}>
                        <View style={{flex: 1}}>
                            <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(false)}
                                              style={{flex: 1, backgroundColor: Colors.BLACK_40}}/>
                            <CameraGalleryButtonsComponent
                                onCameraPress={this.props.onCameraPress}
                                onGalleryPress={this.props.onGalleryPress}/>
                        </View>
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    topContainer: {
        flexDirection : 'column',
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_x_large,
        //margin: Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed',
        justifyContent : 'space-between'
    },
});
