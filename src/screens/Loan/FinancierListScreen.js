import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import FinancierListCard from '../../granulars/FinancierListCard'
import { Utility } from '../../util'

export default class FinancierListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    renderItem = ({ item }) => {
        return (
            !this.props.isLoanShareQuote || item.has_rule_engine == "1" ?
                <FinancierListCard
                    item={item}
                    onListItemClick={this.props.onListItemClick}
                />
                : null
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{ textAlign: 'center', marginVertical: Dimens.margin_normal, fontFamily: Strings.APP_FONT, fontSize: Dimens.text_large, fontWeight: 'normal', color: Colors.WHITE }}>{Strings.CHOOSE_PREFERRED_FINANCING_PARTNER}</Text>
                {this.props.financierList.length > 0 ?
                    <FlatList
                        style={{ margin: Dimens.margin_small, flex: 1 }}
                        data={this.props.financierList}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                    />
                    :
                    null
                }
            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})