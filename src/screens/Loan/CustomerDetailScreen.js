import React, { Component } from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput,
    Platform, Keyboard, KeyboardAvoidingView,
} from 'react-native';
import { Colors, Strings, Dimens } from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import TextInputLayout from "../../granulars/TextInputLayout"
import * as Utility from '../../util/Utility';
import { Dropdown } from 'react-native-material-dropdown';
import { icon_cd_logo, icon_checked, icon_unchecked } from '../../assets/ImageAssets'
import CameraGalleryButtonsComponent from "../../components/CameraGalleryButtonsComponent";
import LoadingComponent from './../../granulars/LoadingComponent'
import * as Constants from '../../util/Constants'
export default class CustomerDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight: 0,
            inputHeight: 50,
        }
    }
    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }
    renderTypeList = () => {
        if (this.props.stateData.typeData && this.props.stateData.typeData.length > 0) {
            let typeData = this.props.stateData.typeData;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {typeData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onTypeSelected(item.text, item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.value}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.text}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderLoanTypeList = () => {
        if (this.props.stateData.loanTypeList && this.props.stateData.loanTypeList.length > 0) {
            let typeData = this.props.stateData.loanTypeList;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {typeData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onLoanTypeSelected(item.label, item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.value}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.label}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderLeadSourceList = () => {
        if (this.props.stateData.configData.lead_source && this.props.stateData.configData.lead_source.length > 0) {
            let leadSourceData = this.props.stateData.configData.lead_source;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {leadSourceData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onLeadSourceSelected(item.name, item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.name}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderAgentNameList = () => {
        if (this.props.stateData.configData.agent_list && this.props.stateData.configData.agent_list.length > 0) {
            let agentNameData = this.props.stateData.configData.agent_list;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {agentNameData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onAgentNameSelected(item.name, item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.name}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderFinancierBranchList = () => {
        if (this.props.stateData.financier.branch_data && this.props.stateData.financier.branch_data.length > 0) {
            let financierBranchData = this.props.stateData.financier.branch_data;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {financierBranchData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onFinancierBranchSelected(item.name, item.id)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.name}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.props.onCheckboxClick(item)}>

                <View style={{ flexDirection: 'row', marginTop: Dimens.margin_x_large, alignItems: 'center' }}>
                    <Image source={item.isChecked ? icon_checked : icon_unchecked}
                        style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                    <Text style={Styles.checkboxText}>{item.label}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        let actionBarProps = {
            values: { title: Strings.CUSTOMER_DETAIL },
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    {this.props.stateData.showLoading && <LoadingComponent />}
                    <ScrollView style={{ marginBottom: 30 }}
                        keyboardShouldPersistTaps='handled'
                        ref='_scrollView'>
                        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', margin: Dimens.margin_normal, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : this.state.inputHeight }}>
                            {/* <Text style={{textAlign:'center',marginVertical:Dimens.margin_normal ,fontFamily: Strings.APP_FONT,fontSize: Dimens.text_large,fontWeight: 'normal',color: Colors.BLACK}}>{Strings.ENTER_APPROVED_QUOTE_DETAILS}</Text> */}
                            <TextInputLayout
                                ref={(input) => this.TL_NAME = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.props.stateData.name ? this.props.stateData.name : ''}
                                    placeholder={Strings.NAME + "*"}
                                    ref={(input) => this.nameInput = input}
                                    onChangeText={(text) => this.props.onNameChange(text)}

                                />
                            </TextInputLayout>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Dropdown
                                    containerStyle={{ flex: 0.4 }}
                                    dropdownOffset={{ top: 40, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.countryCodeList}
                                    absoluteRTLLayout={true}
                                    value={this.props.stateData.countryCode}
                                    onChangeText={(value) => {
                                        this.props.onCountryCodeChange(value)
                                    }}
                                />

                                <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_PHONE = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.PRIMARY}
                                        errorColorMargin={Dimens.margin_0}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={true}
                                            selectTextOnFocus={false}
                                            keyboardType={'number-pad'}
                                            placeholder={Strings.PHONE + "*"}
                                            ref={(input) => this.phoneInput = input}
                                            maxLength={Utility.maxLengthMobileNo(this.props.stateData.countryCode)}
                                            value={this.props.stateData.phone ? this.props.stateData.phone : ''}
                                            onChangeText={(text) => this.props.onPhoneChange(text)}
                                        />
                                    </TextInputLayout>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Dropdown
                                    containerStyle={{ flex: 0.4 }}
                                    dropdownOffset={{ top: 40, left: 0 }}
                                    baseColor={Colors.GRAY}
                                    data={this.props.stateData.countryCodeList}
                                    absoluteRTLLayout={true}
                                    value={this.props.stateData.altCountryCode}
                                    onChangeText={(value) => {
                                        this.props.onAltCountryCodeChange(value)
                                    }}
                                />

                                <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_ALT_PHONE = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.PRIMARY}
                                        errorColorMargin={Dimens.margin_0}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            selectTextOnFocus={false}
                                            keyboardType={'number-pad'}
                                            placeholder={Strings.ALT_PHONE}
                                            ref={(input) => this.altPhoneInput = input}
                                            maxLength={Utility.maxLengthMobileNo(this.props.stateData.altCountryCode)}
                                            value={this.props.stateData.altPhone ? this.props.stateData.altPhone : ''}
                                            onChangeText={(text) => this.props.onAltPhoneChange(text)}
                                        />
                                    </TextInputLayout>
                                </View>
                            </View>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onTypePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_TYPE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedTypeText ? this.props.stateData.selectedTypeText : ''}
                                        placeholder={Strings.TYPE + "*"}
                                        ref={(input) => this.typeInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onTypePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            { this.props.stateData.financier.has_loan_type == "1" && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onLoanTypePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_LOAN_TYPE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedLoanTypeText ? this.props.stateData.selectedLoanTypeText : ''}
                                        placeholder={Strings.LOAN_TYPE + "*"}
                                        ref={(input) => this.loanTypeInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onLoanTypePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            }
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onLeadSourcePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_LEAD_SOURCE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedLeadSourceText ? this.props.stateData.selectedLeadSourceText : ''}
                                        placeholder={Strings.LEAD_SOURCE + "*"}
                                        ref={(input) => this.leadSourceInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onLeadSourcePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.stateData.isAgentListEditable ? this.props.onAgentNamePress() : Utility.log("Pressed...")}>
                                <TextInputLayout
                                    ref={(input) => this.TL_AGENT_NAME = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedAgentNameText ? this.props.stateData.selectedAgentNameText : ''}
                                        placeholder={Strings.AGENT_NAME + "*"}
                                        ref={(input) => this.agentNameInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' && this.props.stateData.isAgentListEditable ? this.props.onAgentNamePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onFinancierBranchPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_FINANCIER_BRANCH = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedFinancierBranchText ? this.props.stateData.selectedFinancierBranchText : ''}
                                        placeholder={Strings.FINANCIER_BRANCH + "*"}
                                        ref={(input) => this.financierBranchInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onFinancierBranchPress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TextInputLayout
                                ref={(input) => this.TL_NOTES = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >
                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.props.stateData.notes ? this.props.stateData.notes : ''}
                                    placeholder={Strings.NOTES}
                                    ref={(input) => this.notesInput = input}
                                    onChangeText={(text) => this.props.onNotesChange(text)}
                                />
                            </TextInputLayout>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onAllCheckboxClick()}
                                style={{marginLeft : Dimens.margin_small}}>

                                <View style={{ flexDirection: 'row', marginTop: Dimens.margin_x_large }}>
                                    <Image source={this.props.stateData.isAllChecked ? icon_checked : icon_unchecked}
                                        style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                    <Text style={Styles.checkboxText}>{Strings.SELECT_ALL}</Text>
                                </View>
                            </TouchableOpacity>
                            <FlatList
                                style={{ margin: Dimens.margin_small, flex: 1 }}
                                data={this.props.stateData.checkboxList}
                                renderItem={this.renderItem}
                                keyExtractor={item => item.name}
                                extraData={this.state}

                            />

                        </KeyboardAvoidingView>

                    </ScrollView>
                    <TouchableOpacity
                        activeOpacity={this.props.stateData.showLoading ? 1.0 : 0.9}
                        style={{ width: '100%', position: 'absolute', bottom: 0, padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.stateData.showLoading ? Colors.GRAY : Colors.PRIMARY }}
                        onPress={() => { 
                            this.refs._scrollView.scrollTo({x: 0, y: 0, animated: true})
                            if(!this.props.stateData.showLoading)
                                this.props.clickOnNext()
                            }
                        }>

                        <Text style={Styles.buttonText}>{Strings.SUBMIT}</Text>
                    </TouchableOpacity>
                    {this.props.stateData.showTypeDialog && this.renderTypeList()}
                    {this.props.stateData.showLeadSourceDialog && this.renderLeadSourceList()}
                    {this.props.stateData.showFinancierBranchDialog && this.renderFinancierBranchList()}
                    {this.props.stateData.showAgentNameDialog && this.renderAgentNameList()}
                    {this.props.stateData.showLoanTypeDialog && this.renderLoanTypeList()}

                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },

    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    topContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_x_large,
        //margin: Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed',
        justifyContent: 'space-between'
    },
    checkboxText: {
        flex: 1,
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
});
