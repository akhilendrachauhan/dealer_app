import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput,
    Platform
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values"
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import * as Utility from '../../util/Utility';
import {icon_no_image, pdf_icon} from '../../assets/ImageAssets'
import * as Constants from '../../util/Constants'
import CameraGalleryButtonsComponent from "../../components/CameraGalleryButtonsComponent";
export default class AttachmentsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    renderItem = ({ item }) => {
        return (
            item.origin == Constants.FINAL_DOC_SCREEN && <View style={Styles.topContainer}>
                <View style={{flexDirection:'row'}}>
                <Text style={{
                        color: item.is_required == '1' ? Colors.PRIMARY : this.props.stateData.financier && this.props.stateData.financier.has_rule_engine == Constants.HAS_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_AVAILABLE ? Colors.PRIMARY : this.props.stateData.financier && this.props.stateData.financier.has_rule_engine == Constants.HAS_NO_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_NOT_AVAILABLE ? Colors.PRIMARY : Colors.BLACK,
                        fontSize: Dimens.text_normal,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{item.name}</Text>
                <Text style={{
                        color: Colors.PRIMARY,
                        fontSize: Dimens.text_normal,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{item.is_required == '1' ? "*" : this.props.stateData.financier && this.props.stateData.financier.has_rule_engine == Constants.HAS_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_AVAILABLE ? "*" : this.props.stateData.financier && this.props.stateData.financier.has_rule_engine == Constants.HAS_NO_RULE_ENGINE && item.required_when == Constants.REQUIRED_WHEN_RULE_ENGINE_NOT_AVAILABLE ? "*" : ""} </Text>
                </View>
                {/* <Text style={{
                        color: Colors.BLACK,
                        fontSize: Dimens.text_normal,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT,
                        textAlign : 'left'
                    }}>({Strings.MAXIMUM + " " + item.max_upload }{item.is_required == '1' ? "*" : ""})</Text> */}
                <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',alignItems: 'center'}}>
                <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(true,item)} activeOpacity={0.7} style={{
                    borderRadius: 3,
                   // width: '100%',
                    height: 42,
                    paddingLeft:10,paddingRight:10,
                    backgroundColor: Colors.PRIMARY,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                
                    <Image source={require('../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                           style={{
                               height: Dimens.icon_normal,
                               width: Dimens.icon_normal,
                               marginRight: Dimens.margin_large
                           }}/>
                    <Text style={{
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.UPLOAD_IMAGE}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.onViewAllPress(item)} activeOpacity={0.7}
                    
                >
                <Image source={item.selectedImages[0] && item.selectedImages[0].uri  ? Utility.checkFileType(item.selectedImages[0].uri,Constants.PDF_FILE_TYPE) ? pdf_icon : { uri:item.selectedImages[0].uri} : icon_no_image } resizeMode={'contain'}
                    style={{
                        height: 75,
                        width: 75,
                        marginRight: Dimens.margin_large
                }}/>
               { item.selectedImages.length > 0 && <View style={{justifyContent:'center',alignItems:'center', position:'absolute',right:5,top:0,backgroundColor: Colors.PRIMARY,height:20,width:20,borderRadius:10}}><Text style={{color:Colors.WHITE}}>{item.selectedImages.length}</Text></View>}
               </TouchableOpacity>
                </View>
                <Text style={{
                        color: Colors.BLACK,
                        fontSize: Dimens.text_normal,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT,
                        textAlign : 'left'
                    }}>({Strings.MAXIMUM + " " + item.max_upload + " "+ Strings.UPLOAD + (item.max_upload > 1 ? "s" : "") })</Text>
            </View>
        )
    }
    
    render() {
        let actionBarProps = {
            values: {title: Strings.ATTACHMENTS},
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <ScrollView>
                        <View style={{flex: 1, flexDirection: 'column', margin: Dimens.margin_normal}}>
                            <FlatList
                                style={{ margin: Dimens.margin_small,flex:1 }}
                                data={this.props.stateData.document_list}
                                renderItem={this.renderItem}
                                keyExtractor={item => item.id}
                                extraData = {this.state}
                            />
                        </View>  
                    </ScrollView>   
                    <TouchableOpacity
                            activeOpacity={0.8}
                            style={{width:'100%',position:'absolute',bottom:0, padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                            onPress={() => this.props.clickOnSubmit()}>

                            <Text style={Styles.buttonText}>{Strings.SUBMIT}</Text>
                    </TouchableOpacity>   
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.stateData.transparentModalVisible}
                        onRequestClose={() => {
                            this.props.setTransparentModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setTransparentModalVisible(false)
                        }}>
                        <View style={{flex: 1}}>
                            <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(false)}
                                              style={{flex: 1, backgroundColor: Colors.BLACK_40}}/>
                            <CameraGalleryButtonsComponent
                                onCameraPress={this.props.onCameraPress}
                                onGalleryPress={this.props.onGalleryPress}
                                onPDFPress = {this.props.onDocumentSelected}
                                />
                        </View>
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        paddingBottom: 50,
    },
    topContainer: {
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_normal,
        margin: Dimens.margin_0,
        marginBottom : Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed',
        //flexDirection: 'row',
        //justifyContent : 'space-between'
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
});
