import React, { Component } from 'react'
import {
    StyleSheet, View, Text, Image, TextInput, ScrollView, TouchableOpacity
} from 'react-native'
import TextInputLayout from "../../granulars/TextInputLayout"
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants } from '../../util'
import LoadingComponent from '../../components/LoadingComponent'
import { RadioButton, Orientation } from "../../granulars/RadioButton"

export default class LoginScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            oldPassword: '',
            newPassword: '',
            defaultLangPos: -1,
            isShowLang: false,
        }
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    {this.profileHead()}
                    {this.changePasswordRender()}
                    {this.props.languageData.length > 1 ? this.languageRender() : null}

                </ScrollView>
                {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
            </View>
        )
    }

    profileHead() {
        return (
            <View style={{ height: 250, alignItems: 'center' }}>
                <Image source={ImageAssets.dashboard_bg}
                    style={{ width: '100%', height: '100%', resizeMode: 'cover' }}
                />

                <View style={{ position: 'absolute', height: '85%', marginTop: 25, alignItems: 'center', justifyContent: 'space-between' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={Styles.nameText}>{this.props.usreName}</Text>

                        <View style={Styles.decorateLine} />

                        <Text style={Styles.normalText}>{Strings.LOGIN_WITH + ': ' + this.props.usrePhone}</Text>
                    </View>
                    <Text style={Styles.normalText}>{Strings.VERSION + ': ' + Constants.VERSION_NAME}</Text>
                </View>
            </View>
        )
    }

    changePasswordRender() {
        return (
            <View>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.props.onEditPassword()}>

                    {!this.props.editPassword ?
                        <View style={Styles.passwordContainer}>
                            <Text style={Styles.blackText}>{Strings.CHANGE_PASSWORD}</Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={Styles.editText}>{Strings.EDIT}</Text>
                                <Image style={{ width: 24, height: 24, resizeMode: 'contain', tintColor: Colors.BLACK_25 }}
                                    source={ImageAssets.icon_edit} />
                            </View>
                        </View>
                        :
                        <View style={Styles.passwordContainer1}>
                            <Text style={[Styles.blackText, { color: Colors.WHITE }]}>{Strings.CHANGE_PASSWORD}</Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[Styles.editText, { color: Colors.WHITE }]}>{Strings.CLOSE}</Text>
                                <Image style={{ width: 20, height: 24, resizeMode: 'contain', tintColor: Colors.WHITE }}
                                    source={ImageAssets.icon_cancel} />
                            </View>
                        </View>
                    }
                </TouchableOpacity>

                {this.props.editPassword &&
                    <View>
                        <Text style={Styles.editPasswordText}>{Strings.EDIT_PASSWORD}</Text>

                        <View>
                            <Image style={Styles.iconStyle} source={ImageAssets.icon_lock} />
                            <TextInputLayout
                                ref={(input) => this.TL_oldPassword = input}
                                style={Styles.textInputStyle}
                                focusColor={Colors.PRIMARY}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_xxx_large}>
                                <TextInput
                                    style={Styles.textInput}
                                    selectTextOnFocus={false}
                                    returnKeyType='next'
                                    secureTextEntry={true}
                                    value={this.state.oldPassword}
                                    ref={(input) => this.oldPasswordInput = input}
                                    onSubmitEditing={() => this.newPasswordInput.focus()}
                                    placeholder={Strings.OLD_PASSWORD}
                                    onChangeText={(text) => this.setState({ oldPassword: text })}
                                />
                            </TextInputLayout>
                        </View>

                        <View>
                            <Image style={Styles.iconStyle} source={ImageAssets.icon_lock} />
                            <TextInputLayout
                                ref={(input) => this.TL_newPassword = input}
                                style={Styles.textInputStyle}
                                focusColor={Colors.PRIMARY}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_xxx_large}>
                                <TextInput
                                    style={Styles.textInput}
                                    selectTextOnFocus={false}
                                    returnKeyType='done'
                                    secureTextEntry={!this.props.showPassword}
                                    value={this.state.newPassword}
                                    ref={(input) => this.newPasswordInput = input}
                                    onSubmitEditing={() => this.props.onChangePassword(this.state.oldPassword, this.state.newPassword)}
                                    placeholder={Strings.NEW_PASSWORD}
                                    onChangeText={(text) => this.setState({ newPassword: text })}
                                />
                            </TextInputLayout>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={this.props.onShowPassword}>

                            <View style={{ flexDirection: 'row', margin: Dimens.margin_xxx_large }}>
                                <Image source={this.props.showPassword ? ImageAssets.icon_checked : ImageAssets.icon_unchecked}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                <Text style={Styles.showPasswordText}>{Strings.SHOW_PASSWORD}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onChangePassword(this.state.oldPassword, this.state.newPassword)}>

                            <View style={Styles.buttonStyle}>
                                <Text style={Styles.buttonText}>{Strings.DONE}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }

    languageRender() {
        return (
            <View style={{ margin: Dimens.margin_xx_large, justifyContent: 'flex-start' }}>
                <Text style={Styles.languageHeadText}>{Strings.CHANGE_LANGUAGE}</Text>

                {this.state.isShowLang ?
                    <RadioButton
                        ref={"language"}
                        list={this.props.languagesName}
                        itemStyle={Styles.radioButtonItem}
                        style={Styles.radioButtonContainer}
                        orientation={Orientation.HORIZONTAL}
                        defaultSelectedPosition={parseInt(this.state.defaultLangPos)}
                        onItemChanged={(newPosition, oldPosition) => {
                            this.props.onChangeLanguage(newPosition, oldPosition, this.props.languageData)
                        }}
                    />
                    :
                    null
                }
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    nameText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_xx_large,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
    },
    decorateLine: {
        width: 60, height: 2,
        backgroundColor: Colors.PRIMARY,
        marginTop: Dimens.margin_x_small,
        marginBottom: Dimens.margin_x_large,
    },
    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.GRAY_LIGHT_BG,
        paddingVertical: Dimens.padding_24,
        paddingHorizontal: Dimens.padding_xx_large
    },
    passwordContainer1: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.PRIMARY,
        paddingVertical: Dimens.padding_24,
        paddingHorizontal: Dimens.padding_xx_large
    },
    blackText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT,
        fontWeight: '600'
    },
    editText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginRight: Dimens.margin_small
    },
    editPasswordText: {
        textAlign: 'center',
        color: Colors.PRIMARY,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_small,
        // marginBottom: -Dimens.margin_xx_large
    },
    iconStyle: {
        top: 32,
        tintColor: Colors.GRAY,
        resizeMode: 'contain',
        height: 22, width: 22,
        marginLeft: Dimens.margin_xx_large,
    },
    textInputStyle: {
        marginTop: -16,
        paddingLeft: 30,
        marginHorizontal: Dimens.margin_xx_large
    },
    textInput: {
        height: 40,
        color: Colors.BLACK,
        fontSize: Dimens.text_normal
    },
    buttonText: {
        fontWeight: 'bold',
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        marginHorizontal: Dimens.margin_xx_large,
    },
    showPasswordText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
    languageHeadText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    languageText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    radioButtonContainer: {
        justifyContent: 'flex-start',
        marginTop: Dimens.margin_xx_large,
    },
    radioButtonItem: {
        marginRight: Dimens.margin_xx_large
    }
});