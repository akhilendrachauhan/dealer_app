import React, { Component } from 'react';
import ProfileScreen from './ProfileScreen'
import { View, SafeAreaView, StyleSheet, Keyboard, Share, Linking } from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"

export default class ProfileContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            loadingMsg: '',
            showPassword: false,
            editPassword: false,
            usreName: '',
            usreEmail: '',
            usrePhone: '',
            languageData: [],
            languagesName: [],
        }
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.PROFILE_SCREEN)
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.NAME, Constants.EMAIL, Constants.PHONE_NO, Constants.LANGUAGE_ID])
        this.setState({
            usreName: values[0][1],
            usreEmail: values[1][1],
            usrePhone: values[2][1],
        })
        let defaultLanguageId = values[3][1]
        let languagesName = []
        let languageData = await Utility.getConfigArrayData(Constants.LANGUAGE_VALUES)
        // Utility.log('languageData==>', languageData)
        for (let i = 0; i < languageData.length; i++) {
            languagesName.push(languageData[i].name)

            if (languageData[i].id == defaultLanguageId) {
                this.refs['profileScreen'].setState({ defaultLangPos: i, isShowLang: true })
                storeObject.saveValueInPersistStore(Constants.LANGUAGE_ID, languageData[i].id.toString())
                storeObject.saveValueInPersistStore(Constants.LANGUAGE_CODE, languageData[i].iso_code)
            }
        }
        this.setState({ languageData: languageData, languagesName: languagesName })
    }

    // onSharePress = async () => {

    //     let imageArr = [
    //         'https://otousedcar.s3.ap-southeast-1.amazonaws.com/PH/cm/1436557/listing_images/PH/upload_5e20dd5d7694f7.30671943.jpg',
    //         'https://otousedcar.s3.ap-southeast-1.amazonaws.com/PH/cm/1436557/listing_images/PH/upload_5e20dd5d7694f7.30671943.jpg']


    //     let urls = 'https://otousedcar.s3.ap-southeast-1.amazonaws.com/PH/cm/1436557/listing_images/PH/upload_5e20dd5d7694f7.30671943.jpg' + 'https://otousedcar.s3.ap-southeast-1.amazonaws.com/PH/cm/1436557/listing_images/PH/upload_5e20dd5d7694f7.30671943.jpg'
    //     let mobileNo = '919650126825'//'919462797441'
    //     let message = 'Hi Viber' + ' ' + urls
    //     let URI = 'girnarsoft'

    //     // let url = `whatsapp://send?text=${message}&phone=${mobileNo}`
    //     // let url = `viber://pa/info?uri=${URI}`
    //     // let url = `viber://pa?chatURI=${URI}`
    //     // let url = `viber://pa?chatURI=${URI}&text=${message}`
    //     // let url = `viber://chat?number=${mobileNo}&text=${message}`
    //     // let url = `viber://add?number=${mobileNo}&text=${message}`
    //     let url = `viber://forward?text=${message}`

    //     // check if we can use this link
    //     const canOpen = await Linking.canOpenURL(url)

    //     if (!canOpen) {
    //         throw new Error('Provided URL can not be handled')
    //     }

    //     return Linking.openURL(url)

    //     // var appLink = Constants.APP_TYPE == Constants.INDONESIA ? Constants.APP_LINK : Constants.PH_APP_LINK
    //     // try {
    //     //     const result = await Share.share({
    //     //         message: Strings.EASIEST_WAY_DEALERSHIP + ':- ' + appLink
    //     //     })

    //     //     if (result.action === Share.sharedAction) {
    //     //         if (result.activityType) {
    //     //             // shared with activity type of result.activityType
    //     //             Utility.log('shared with activity type of result.activityType')
    //     //         } else {
    //     //             // shared
    //     //             Utility.log('shared', JSON.stringify(result))
    //     //         }
    //     //     } else if (result.action === Share.dismissedAction) {
    //     //         // dismissed
    //     //         Utility.log('dismissed')
    //     //     }
    //     // } catch (error) {
    //     //     Utility.log(error.message);
    //     // }
    // }

    onShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword })
    }

    onEditPassword = () => {
        this.setState({ editPassword: !this.state.editPassword })
    }

    onChangePassword = (oldPassword, newPassword) => {

        if (Utility.isValueNullOrEmpty(oldPassword)) {
            this.refs['profileScreen'].TL_oldPassword.setError(Strings.ENTER_OLD_PASSWORD)
            this.refs['profileScreen'].oldPasswordInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(newPassword)) {
            this.refs['profileScreen'].TL_newPassword.setError(Strings.ENTER_NEW_PASSWORD)
            this.refs['profileScreen'].newPasswordInput.focus()
            return
        }
        else if (newPassword == oldPassword) {
            this.refs['profileScreen'].TL_newPassword.setError(Strings.PASSWORD_NOT_SAME)
            this.refs['profileScreen'].newPasswordInput.focus()
            return
        }
        else {
            Keyboard.dismiss()
            try {
                Utility.sendEvent(AnalyticsConstants.CHANGE_PASSWORD_CLICK)
                APICalls.changePassword(oldPassword, newPassword, this.onSuccessChangePass, this.onFailureChangePass, this.props)
            } catch (error) {
                Utility.log(error)
            }
            this.setState({ isLoading: true })
        }
    }

    onSuccessChangePass = (response) => {
        // if (response && response.message)
        //     Utility.showToast(response.message)
        Utility.showToast(Strings.PASSWORD_UPDATE)
        Utility.sendEvent(AnalyticsConstants.CHANGE_PASSWORD_SUCCESS)
        this.setState({ isLoading: false }, () => {
            Utility.logout(this.props)
        })
        Utility.log('onSuccessonFailureChangePass===> ', JSON.stringify(response))
    }

    onFailureChangePass = (response) => {
        Utility.sendEvent(AnalyticsConstants.CHANGE_PASSWORD_FAILURE)
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false })
        Utility.log('onFailureonFailureChangePass====> ', JSON.stringify(response))
    }

    onChangeLanguage = (newPosition, oldPosition, langArr) => {
        // Utility.log('onChangeLanguage==>', newPosition, oldPosition, langArr)
        let languageId = langArr[newPosition].id
        let langCode = langArr[newPosition].iso_code

        if (newPosition != oldPosition) {
            this.updateLang(languageId, langCode)
        }
    }

    updateLang = async (languageId, langCode) => {
        try {
            Utility.sendEvent(AnalyticsConstants.CHANGE_LANGUAGE_CLICK)
            APICalls.SaveInfo(languageId, langCode, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
        this.setState({ isLoading: true })
    }

    onSuccess = (response) => {
        if (response && response.data) {
            Utility.sendEvent(AnalyticsConstants.CHANGE_LANGUAGE_SUCCESS)
            var storeObject = new AsyncStore()
            storeObject.saveValueInPersistStore(Constants.LANGUAGE_ID, response.data.language_id.toString())
            storeObject.saveValueInPersistStore(Constants.LANGUAGE_CODE, response.data.language_code)

            // Update all app lang....
            Strings.setLanguage(response.data.language_code)
            // Utility.log('lang===> ', response.data.language_code)
        }
        for (let i = 0; i < this.state.languageData.length; i++) {
            if (this.state.languageData[i].id == parseInt(response.data.language_id)) {
                this.refs['profileScreen'].setState({ defaultLangPos: i })
            }
        }
        // Get All Master(Config) Data...
        Utility.getMasterData().then((result) => {
            Utility.log('result===>', result)
        })
        this.setState({ isLoading: false })
        Utility.log('onSuccessLanguage===> ', JSON.stringify(response))
    }

    onFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.CHANGE_LANGUAGE_FAILURE)
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false })
        Utility.log('onFailureLanguage====> ', JSON.stringify(response))
    }

    onSharePress = async () => {
        Utility.sendEvent(AnalyticsConstants.PROFILE_SHARE_CLICK)
        var appLink = Constants.APP_TYPE == Constants.INDONESIA ? Constants.APP_LINK : Constants.PH_APP_LINK
        try {
            const result = await Share.share({
                message: Strings.EASIEST_WAY_DEALERSHIP + ':- ' + appLink
            })
            

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                    Utility.log('shared with activity type of result.activityType')
                } else {
                    // shared
                    Utility.sendEvent(AnalyticsConstants.PROFILE_SHARE_DONE)
                    Utility.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
                Utility.log('dismissed')
            }
        } catch (error) {
            Utility.log(error.message);
        }
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.MY_PROFILE }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: ImageAssets.icon_share,
                            onPress: () => this.onSharePress()
                        }]}
                    />

                    <ProfileScreen
                        ref={'profileScreen'}
                        usreName={this.state.usreName}
                        usreEmail={this.state.usreEmail}
                        usrePhone={this.state.usrePhone}
                        isLoading={this.state.isLoading}
                        loadingMsg={this.state.loadingMsg}
                        showPassword={this.state.showPassword}
                        onShowPassword={this.onShowPassword}
                        onChangePassword={this.onChangePassword}
                        onEditPassword={this.onEditPassword}
                        editPassword={this.state.editPassword}
                        onChangeLanguage={this.onChangeLanguage}
                        defaultLanguageIndex={this.state.defaultLanguageIndex}
                        languageData={this.state.languageData}
                        languagesName={this.state.languagesName}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})