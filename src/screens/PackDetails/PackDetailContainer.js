import React, {Component} from 'react';
import {View, BackHandler, StyleSheet, SafeAreaView,Linking} from 'react-native';
import PackDetailScreen from './PackDetailScreen';
import * as ImageAssets from '../../assets/ImageAssets';
import {Utility,Constants} from '../../util';
import {Colors,Strings} from "../../values";
import { _ActionBarStyle } from '../../values/Styles';
import * as APICalls from '../../api/APICalls'
import { ScreenStates, ScreenName } from '../../util/Constants'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import ActionBarWrapper from '../../granulars/ActionBarWrapper';
import { AnalyticsConstants } from '../../util'
let purchasedSku = ""
class PackDetailContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screenState: ScreenStates.IS_LOADING,
            data:[],
            dealer_sku: [],
            dealer_sku_info_url : "",
            isLoading : false,
            purchaseToBeSku : []
        };
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.PACKAGE_DETAIL_CLICK)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        //let dealer_sku = await Utility.getConfigArrayData(Constants.DEALER_SKU)
        const values = await Utility.getMultipleValuesFromAsyncStorage([Constants.DEALER_SKU_INFO_URL])
        this.setState({ dealer_sku_info_url:values[0][1] },()=>{
            this.getData()
        })
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () =>{
        this.onBackPress();
        return true;
    }

    onRetryClick = ()=>{
        this.getData()
    }

    getData = async () => {
        const values = await Utility.getMultipleValuesFromAsyncStorage([Constants.DEALER_ID,Constants.DEALER_ID_HASH])
        Utility.log('DEALER_ID===>' + values[0][1]+":"+values[1][1])

        try {
            this.setState({
                screenState: ScreenStates.IS_LOADING,
            })
            APICalls.getPackageDetail(values[0][1],values[1][1],this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    };

    onSuccess = (response) => {
        if (response) {
            let found = 0
            let data = response.data ? response.data : []
            let dealer_sku = response.dealer_sku ? response.dealer_sku : []
            this.state.purchaseToBeSku = dealer_sku.filter(obj => {
                if(obj.sku_type == "service"){
                    found = 0
                    for(let i=0;i<data.length;i++){
                        if(data[i].b_details_is_active == "1"){
                            if(obj.id === data[i].b_details_sku_id){
                                found = 1
                                break 
                            }
                        }
                    }
                    if(found == 0){
                        return obj
                    }
                }
            }).map(obj => {
                return {id: obj.id,sku_name : obj.sku_name, toPurchase : true,next_buy_req_date : obj.next_buy_req_date ? obj.next_buy_req_date : null,can_buy :obj.can_buy }
            })
            this.setState({ dealer_sku : dealer_sku,screenState: ScreenStates.NO_ERROR,data : [...response.data, ...this.state.purchaseToBeSku] },()=>{
            })
        } else 
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND})
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

        this.setState({screenState: ScreenStates.SERVER_ERROR})
    }

    purchasePackage = async (sku_id) => {
        Utility.sendEvent(AnalyticsConstants.PACKAGE_BUY_CLICK, { sku_id: sku_id })
        purchasedSku = sku_id
        try {
            this.setState({
                isLoading: true,
            })
            APICalls.purchasePackage(sku_id,this.onPurchaseSuccess, this.onPurchaseFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    };

    onPurchaseSuccess = async (response) => {
        if (response && response.data) {
            Utility.sendEvent(AnalyticsConstants.PACKAGE_BUY_SUCCESS, { sku_id: purchasedSku })
            this.state.data = this.state.data.map(obj => {
                if(obj.id && obj.id === purchasedSku){
                    obj.next_buy_req_date = response.data.next_buy_req_date
                }
                return obj
            })
            // let dealer_sku = await Utility.getConfigArrayData(Constants.DEALER_SKU)
            // dealer_sku = dealer_sku.filter(obj => {
            //     if(obj.id && obj.id === purchasedSku){
            //         obj.isRequestSent = true
            //         obj.requestSubmitTime = response.data.next_buy_req_date
            //     }
            //     return obj
            // })

            //Utility.setValueInAsyncStorage(Constants.DEALER_SKU,JSON.stringify(dealer_sku))
            this.setState({isLoading: false,data : this.state.data},()=> {
                
            })
            Utility.showToast(response.message)

        } 
    }

    onPurchaseFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.PACKAGE_BUY_FAILURE, { sku_id: purchasedSku })
        this.setState({isLoading: false,
            //data : response.data
        })
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

    }

    renewPackage = async (sku_id,b_details_id,b_id) => {
        purchasedSku = b_id
        Utility.sendEvent(AnalyticsConstants.PACKAGE_RENEW_CLICK, { b_id: b_id })
        try {
            this.setState({
                isLoading: true,
            })
            APICalls.renewPackage(sku_id,b_details_id,b_id,this.onRenewSuccess, this.onRenewFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    };

    onRenewSuccess = (response) => {
        if (response && response.data) {
            Utility.sendEvent(AnalyticsConstants.PACKAGE_RENEW_SUCCESS, { b_id: purchasedSku })
            this.state.data = this.state.data.map(obj => {
                if(obj.b_id && obj.b_id === purchasedSku){
                    obj.next_renew_date = response.data.next_renew_req_date
                }
                return obj
            })
            this.setState({isLoading: false,
                data : this.state.data
            })

            Utility.showToast(response.message)
        } 
    }

    onRenewFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.PACKAGE_RENEW_FAILURE, { b_id: purchasedSku })
        this.setState({isLoading: false,
            //data : response.data
        })
        if (response && response.message)
            Utility.showToast(response.message)
        else
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG)

    }

    onHelpClick = async ()=>{
        Utility.sendEvent(AnalyticsConstants.PACKAGE_WEB_URL_CLICK)
        let url = this.state.dealer_sku_info_url
        Utility.openExternalUrl(url)
    }

    render() {
        let actionBarProps = {
            values: {
                title: Strings.PACKAGE_DETAIL
            },
            rightIcons: [{
                image: this.state.dealer_sku_info_url && this.state.dealer_sku_info_url != "" ? ImageAssets.question_mark : "",
                onPress: () => this.onHelpClick()
            }],
            styleAttr: {
                leftIconImage: ImageAssets.icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }],
                },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
               <View style={Styles.container}>
               <ActionBarWrapper
                    values={actionBarProps.values}
                    actions={actionBarProps.actions}
                    iconMap={actionBarProps.rightIcons}
                    styleAttributes={actionBarProps.styleAttr}
                />
               <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>
                    <PackDetailScreen
                        data={this.state.data}
                        actionBarProps={actionBarProps}
                        dealer_sku = {this.state.dealer_sku}
                        isLoading= {this.state.isLoading}
                        purchasePackage={this.purchasePackage}
                        renewPackage = {this.renewPackage}
                    />
                </WithLoading>
                </View>
            </SafeAreaView>
            )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})
export default PackDetailContainer;
