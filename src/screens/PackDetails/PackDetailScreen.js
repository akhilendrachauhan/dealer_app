import React, { Component } from 'react';
import {
    SectionList,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    FlatList
} from 'react-native';
import { Colors, Strings, Dimens } from '../../values';
import * as ImageAssets from '../../assets/ImageAssets';
import { Constants} from './../../util'
import PackDetailComponent from './../../components/PackDetailComponent'
import LoadingComponent from './../../granulars/LoadingComponent'
class PackDetailScreen extends Component {
    constructor(props) {
        super(props);
        
    }

    renderItem = ({ item, index }) => {
        return (
            <PackDetailComponent
                item={item}
                index={index}
                dealer_sku = {this.props.dealer_sku}
                purchasePackage = {this.props.purchasePackage}
                renewPackage = {this.props.renewPackage}
            />
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <FlatList
                    style={{ marginHorizontal: Dimens.margin_small }}
                    ref={(ref) => { this.flatListRef = ref }}
                    extraData={this.props}
                    data={this.props.data}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => '' + index}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.props.onRefresh}
                    refreshing={this.props.refreshing}
                    
                />
                {this.props.isLoading && <LoadingComponent />}
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND,
    },
    containerTab: {
        elevation: 0,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.DIVIDER,
        backgroundColor: Colors.WHITE,
    },
});

export default PackDetailScreen;
