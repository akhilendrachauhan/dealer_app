import React, { Component } from 'react';
import { View, StyleSheet, Keyboard, KeyboardAvoidingView, Text, Image, Dimensions, Modal, TextInput, TouchableOpacity, SafeAreaView, ScrollView, RefreshControl, TouchableWithoutFeedback } from 'react-native';
import {
    icon_inspected, icon_back_arrow, icon_edit, icon_share, icon_whatsapp_logo, icon_sms,
    icon_cancel, icon_arrow_right, icon_menu, icon_arrow_left, icon_mail,icon_unchecked,icon_checked
} from './../../assets/ImageAssets'
import { Strings, Colors, Dimens } from './../../values'
import { _ActionBarStyle } from '../../values/Styles'
import DotView from '../../granulars/DotView'
import { Utility, Constants } from '../../util';
import { TabView, TabBar } from 'react-native-tab-view'
import ShareByEmailModalView from './ShareByEmailModalView'
import ShareBySMSModalView from './ShareBySMSModalView'
import ShareByWhatsAppOptionModalView from './ShareByWhatsAppOptionModalView'
import CarInfo from './../../granulars/Stocks/CarInfo'
import LeadsOnStockDetail from './../../granulars/Stocks/LeadsOnStockDetail'
import RemoveStockModalView from './RemoveStockModalView'
import CarSoldModalView from './CarSoldModalView'
import RemoveRenewStockModalView from './RemoveRenewStockModalView'
import LoadingComponent from './../../granulars/LoadingComponent'
import { icon_no_image } from './../../assets/ImageAssets'
import TextInputLayout from "./../../granulars/TextInputLayout"
import ViewPager from './../../nodeModuleChanges/react_native_viewpager/ViewPager'
import CustomViewPageIndicator from "../../granulars/CustomViewPagerIndicator"
import StockWebUrlsModal from "./StockWebUrlsModal";
let dateFormat = require('../../util/DateFormat');
const dataSource = new ViewPager.DataSource({
    pageHasChanged: (p1, p2) => p1 !== p2,
});
var reviewImages;
export default class StockDetailScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            index: 0,
            routes: [
                { key: 'car_info', title: Strings.CAR_INFO },
                { key: 'leads', title: Strings.LEADS },
                // { key: 'tips', title: Strings.TIPS }
            ],
            selectedIndex: 0,
            isShareOptionModalVisible: false,
            isShareByEmailModalVisible: false,
            isShareBySMSModalVisible: false,
            isShareByWhatsAppOptionModalVisible: false,
            emailIds: '',
            mobileNumber: '',
            isRemoveStockModalVisible: false,
            isCarSoldModalVisible: false,
            isRemoveRenewStockModalVisible: false,
            position: 0,
            keyboardHeight: 0,
            inputHeight: 70,
            changeType: 0,
        }

    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }

    renderPage = (item) => {
        return (
            <TouchableOpacity style={{ flex: 1 }} activeOpacity={0.8} onPress={() => this.props.goToReviewScreen(this.state.selectedIndex, reviewImages)}>
                <View style={{ flex: 1, paddingHorizontal: 0 }}>
                    <Image source={{ uri: item.url }}
                        resizeMode={'cover'} style={{ width: '100%', height: 250, backgroundColor: Colors.WHITE }} />
                </View>
            </TouchableOpacity>
        );
    }

    renderPageIndicators = (size) => {
        return (
            <CustomViewPageIndicator
                positionOffset={230}
                dotSize={8}
                dotSpace={6}
                dotBorderWidth={1}
                dotBorderColor={Colors.WHITE} />
        );
    }

    setEmailShareModalVisible = () => {
        this.setState({ isShareByEmailModalVisible: false })
    }

    setSMSShareModalVisible = () => {
        this.setState({ isShareBySMSModalVisible: false })
    }

    setWhatsAppOptionModalVisible = () => {
        this.setState({ isShareByWhatsAppOptionModalVisible: false })
    }

    clickOnRemoveStock = (item) => {
        selectedItem = item
        this.props.hideMenuCallback();
        this.setState({ isRemoveStockModalVisible: true })
    }

    clickOnCarBooked = () => {
        this.setState({ isRemoveStockModalVisible: false, isCarSoldModalVisible: true })
    }

    setCarSoldModalVisible = () => {
        this.setState({ isRemoveStockModalVisible: false, isCarSoldModalVisible: false, isRemoveRenewStockModalVisible: false })
    }

    onCarSoldSubmit = (reason_id, sold_price) => {
        this.props.onCarSoldSubmit(selectedItem.dealer_id, selectedItem.id, reason_id, sold_price, this.setCarSoldModalVisible);
    }

    setRemoveRenewStockModalVisible = () => {
        this.setState({ isRemoveRenewStockModalVisible: false })
    }
    clickOnRenew = () => {
        this.setState({ isRemoveStockModalVisible: false, isRemoveRenewStockModalVisible: true })
    }
    setRemoveStockOptionModalVisible = () =>{
        this.setState({ isRemoveStockModalVisible: false })
    }

    renderScene = ({ route, jumpTo }) => {
        switch (route.key) {
            case 'car_info':
                return (
                    <CarInfo
                        jumpTo={jumpTo}
                        carInfo={this.props.stockDetailData.app_data}
                    />
                )
            case 'leads':
                return (
                    <LeadsOnStockDetail
                        jumpTo={jumpTo}
                        car_id={this.props.stockDetailData.id ? this.props.stockDetailData.id : null}
                        onItemPress={this.props.onLeadItemClick}
                    />
                )

        }
    }
    render() {
        let item = this.props.stockDetailData
        Utility.log("apires:" + JSON.stringify(this.props.stockDetailData))
        let title = '', car_price = '', reg_no = '', make_year = '', certification_status = "0", modelVersion = '',bump_up_date = ''
        if (this.props.stockDetailData) {
            let detail = this.props.stockDetailData;
            title = (detail.make ? detail.make : '') + " " + (detail.modelVersion ? detail.modelVersion : '')
            modelVersion = detail.modelVersion
            car_price = detail.car_price
            reg_no = detail.reg_no || Strings.NA
            make_year = detail.make_year
            certification_status = detail.certification_status
            bump_up_date = detail.bump_up_date ? detail.bump_up_date : ''
        }
        else {
            title = ''

        }
        var images = this.props.stockDetailData.usedCarImage.map((obj, index) => {
            return { url: obj.url }
        })
        reviewImages = this.props.stockDetailData.usedCarImage.map((obj, index) => {
            return obj.url
        })
        let data = dataSource.cloneWithPages(images);
        this.actionBarProps = {
            values: {
                title: title
            },
            rightIcons: [
                {
                    image: icon_share,
                    onPress: this.clickOnShare
                },
                {
                    image: icon_edit,
                    onPress: this.clickOnEdit
                },
                {
                    image: icon_menu,
                    onPress: this.clickOnMenu
                },

            ],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.props.onBackPress,
            }
        };

        // <Slideshow 
        //     dataSource = { images }
        //     indicatorSize = {5}
        //     height = {200}
        //     arrowSize = {12}
        //     position={this.state.position}
        //     onPositionChanged={position => this.setState({ position })}
        // />
        return (
            <View style={styles.container}>
                <ScrollView
                //contentContainerStyle={{flex:1}}
                // refreshControl={
                // <RefreshControl refreshing={this.props.refreshing} onRefresh={this.props.getStockDetailData} />
                // }
                >
                    {images.length > 0 ?
                        <View style={styles.viewPagerContainer}>
                            <View style={styles.viewPager}>
                                <ViewPager
                                    ref={(viewpager) => { this.viewpager = viewpager }}
                                    dataSource={data}
                                    renderPage={this.renderPage}
                                    isLoop={false}
                                    autoPlay={false}
                                    initialPage={this.state.selectedIndex}
                                    changeType={this.state.changeType}
                                    onChangePage={(index) => {
                                        this.setState({ selectedIndex: index, changeType: 0 })
                                    }}
                                    renderPageIndicator={() =>
                                        //this.renderPageIndicators(images.length)
                                        <View />
                                    }
                                />
                                <TouchableOpacity style={{ position: 'absolute', top: 100, left: 10 }} onPress={() => {
                                    if (this.state.selectedIndex > 0) {
                                        //this.viewpager.goToPage(this.state.selectedIndex - 1);
                                        this.setState({ selectedIndex: this.state.selectedIndex - 1, changeType: 1 })

                                    }
                                }}>
                                    <Image source={icon_arrow_left} style={styles.image} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ position: 'absolute', top: 100, right: 10 }} onPress={() => {
                                    if (this.state.selectedIndex < images.length - 1) {
                                        //this.viewpager.goToPage(this.state.selectedIndex + 1);
                                        this.setState({ selectedIndex: this.state.selectedIndex + 1, changeType: 1 })

                                    }
                                }}>
                                    <Image source={icon_arrow_right} style={styles.image} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        <Image source={icon_no_image} style={{ height: 250, width: '100%' }} />
                    }
                    {images.length > 0 &&
                        <View style={{ position: 'absolute', right: 20, top: 200, justifyContent: 'flex-end' }}>
                            <Text style={{ color: Colors.WHITE, fontFamily: Strings.APP_FONT, fontWeight: '500', fontSize: Dimens.text_xx_large }}>{this.state.selectedIndex + 1}/{images.length}</Text>
                        </View>}
                    <View style={{ backgroundColor: Colors.WHITE, borderRadius: 3, marginHorizontal: Dimens.margin_small, paddingHorizontal: Dimens.padding_xxx_large, paddingVertical: Dimens.padding_24, marginTop: -5 }}>
                        <Text style={styles.mmvText}>{title}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 10 }}>
                            <Text style={styles.priceText}>{Utility.displayCurrency(car_price)}</Text>
                            <DotView />
                            <Text style={styles.priceText}>{reg_no}</Text>
                            <DotView />
                            <Text style={styles.priceText}>{make_year}</Text>
                        </View>
                        { bump_up_date != '' ? <Text style={[styles.mmvText,{fontSize : Dimens.text_normal,marginBottom: 10 }]}>{Strings.BUMPUP_DATE}  {dateFormat(bump_up_date, "dd/mm/yyyy HH:MM")}</Text> : null}
                        {certification_status == Constants.STOCK_INSPECTED && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={icon_inspected} style={{ width: 90, height: 18, resizeMode: 'contain', marginRight: 20 }} />
                            {/* <TouchableOpacity
                            onPress = {()=> Utility.log()}
                            activeOpacity = {0.8}
                        >
                            <Text style={styles.viewReportText}>{Strings.VIEW_REPORT}</Text>
                        </TouchableOpacity> */}
                        </View>}
                    </View>
                    {this.props.showLoading && <LoadingComponent />}
                    <View style={{ backgroundColor: Colors.WHITE, borderRadius: 3, marginHorizontal: Dimens.margin_small, paddingHorizontal: Dimens.padding_xxx_large, marginTop: Dimens.margin_small }}>
                        <TabView
                            navigationState={this.state}
                            renderScene={this.renderScene}
                            renderTabBar={props =>
                                <TabBar
                                    {...props}
                                    style={styles.containerTab}
                                    pressColor={Colors.BLACK_25}
                                    activeColor={Colors.BLACK}
                                    inactiveColor={Colors.BLACK_54}
                                    indicatorStyle={{ backgroundColor: Colors.DASHBOARD_CARD_TEXT }}
                                />
                            }
                            onIndexChange={index => this.setState({ index })}
                            initialLayout={{ width: Dimensions.get('window').width }}
                        />
                    </View>
                    {this.props.showMenu && <TouchableWithoutFeedback onPress={() => this.props.clickOutSide()}>
                        <View style={{ width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, bottom: 0, right: 0 }} />
                    </TouchableWithoutFeedback>
                    }
                    {/* Menu View  */}
                    {this.props.showMenu && <View style={{ borderRadius: 2, backgroundColor: Colors.GRAY_LIGHT_BG, position: 'absolute', top: 0, right: 10, zIndex: 999, paddingVertical: 10, paddingHorizontal: 20 }}>
                        {/* <FlatList
                        style={{flex:1}}
                        // ItemSeparatorComponent={ ({highlighted}) => (
                        //     <View style={[styles.separator, highlighted && {marginLeft: 0}]} />
                        // )}
                        showsVerticalScrollIndicator = {false}
                        data={this.props.detailData.menuOptions}
                        renderItem={({item, index, separators}) => (
                            <TouchableOpacity
                                style={{paddingVertical:10}}
                                onPress = {()=> this.clickOnMenuOption()}
                                activeOpacity = {0.8}
                            >
                                <Text style={styles.menuOptionText}>{item.option}</Text>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item.id}
                    /> */}
                        {this.props.stockDetailData && this.props.stockDetailData.car_status != Constants.REMOVED_STOCK + "" ? <View>
                            
                            <TouchableOpacity
                                style={{ paddingVertical: 10 }}
                                onPress={() => {
                                    if (item && item.isclassified == Constants.STOCK_CLASSIFIED)
                                        this.props.setStockPremium(item, 'false', '');
                                    else
                                        this.props.setStockPremium(item, 'true', '');
                                }}
                                activeOpacity={0.8}
                            >
                                <Text style={styles.menuOptionText}>{item.isclassified == Constants.STOCK_CLASSIFIED ? Strings.REMOVE_FROM_CLASSIFIED : Strings.ADD_TO_CLASSIFIED}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ paddingVertical: 10 }}
                                onPress={() => {
                                    if (item && item.ispremium == Constants.STOCK_PREMIUM)
                                        this.props.setStockPremium(item, '', 'false');
                                    else
                                        this.props.setStockPremium(item, '', 'true');
                                }}
                                activeOpacity={0.8}
                            >
                                <Text style={styles.menuOptionText}>{item.ispremium == Constants.STOCK_PREMIUM ? Strings.REMOVE_FEATURED : Strings.ADD_TO_FEATURED}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ paddingVertical: 10 }}
                                onPress={() => {
                                    item.isclassified == Constants.STOCK_CLASSIFIED ? this.props.addToBumpup(item): Utility.log("");
                                }}
                                activeOpacity={0.8}
                            >
                                <Text style={item.isclassified == Constants.STOCK_CLASSIFIED ? styles.menuOptionText: styles.diasbledMenuOptionText}>{Strings.ADD_TO_BUMPUP}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ paddingVertical: 10 }}
                                onPress={() => {
                                    item.show_weburl ? this.props.clickOnSeeOnWeb(item.web_url) : Utility.log("");
                                }}
                                activeOpacity={0.8}
                            >
                                <Text style={item.show_weburl ? styles.menuOptionText : styles.diasbledMenuOptionText}>{Strings.WEB_URL}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={{ paddingVertical: 10 }}
                                onPress={() => this.clickOnRemoveStock(item)}
                                activeOpacity={0.8}
                            >
                                <Text style={styles.menuOptionText}>{Strings.REMOVE_STOCK}</Text>
                            </TouchableOpacity>

                        </View> : <TouchableOpacity
                            style={{ paddingVertical: 0 }}
                            onPress={() => {
                                this.props.AddToActiveStock(item);
                            }}
                            activeOpacity={0.8}
                        >
                                <Text style={styles.menuOptionText}>{Strings.ADD_TO_STOCK}</Text>
                            </TouchableOpacity>}
                    </View>}
                    {/* Menu View  */}

                    {/* start of share  options modal*/}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.isShareOptionModalVisible}
                        onRequestClose={() => {
                            this.props.clickOnShare(false)
                        }}
                        onDismiss={() => {
                            this.props.clickOnShare(false)
                        }}>

                        <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                            <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                                <TouchableOpacity
                                    style={{ alignSelf: 'flex-end', padding: 1 }}
                                    onPress={() => {
                                        this.props.clickOnShare(false)
                                    }}>
                                    <Image source={icon_cancel}
                                        style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                                </TouchableOpacity>

                                <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large, marginBottom: Dimens.margin_normal }}>{Strings.SHARE_BY}</Text>


                                <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                    onPress={() => {
                                        this.props.clickOnShare(false)
                                        this.setState({ isShareByEmailModalVisible: true })
                                    }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={icon_mail}
                                                style={styles.modalImage} />

                                            <Text style={styles.modalText}>{Strings.EMAIL}</Text>
                                        </View>
                                        <Image source={icon_arrow_right}
                                            style={styles.modalImage} />
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.seperator} />
                                <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                    onPress={() => {
                                        this.props.clickOnShare(false)
                                        this.setState({ isShareBySMSModalVisible: true })
                                    }}>

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={icon_sms}
                                                style={styles.modalImage} />

                                            <Text style={styles.modalText}>{Strings.SMS}</Text>
                                        </View>
                                        <Image source={icon_arrow_right}
                                            style={styles.modalImage} />
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.seperator} />

                                {Constants.APP_TYPE == Constants.INDONESIA ?
                                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                        onPress={() => {
                                            this.props.clickOnShare(false)
                                            this.setState({ isShareByWhatsAppOptionModalVisible: true })
                                        }}>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={icon_whatsapp_logo}
                                                    style={styles.modalImage} />

                                                <Text style={styles.modalText}>{Strings.WHATSAPP}</Text>
                                            </View>
                                            <Image source={icon_arrow_right}
                                                style={styles.modalImage} />
                                        </View>
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                                {Constants.APP_TYPE == Constants.INDONESIA ? <View style={styles.seperator} /> : null}
                            </View>
                        </View>
                    </Modal>
                    {/* end of share options modal*/}

                    {/* start of share by emails modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isShareByEmailModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareByEmailModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareByEmailModalVisible: false })
                        }}>

                        <ShareByEmailModalView
                            setModalVisible={this.setEmailShareModalVisible}
                            onEmailShareSubmit={this.props.onEmailShareSubmit}
                            onEmailChange={this.props.onEmailChange}
                            height={this.state.keyboardHeight}
                        />
                    </Modal>
                    {/* end of share by emails modal */}

                    {/* start of share by SMS modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isShareBySMSModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareBySMSModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareBySMSModalVisible: false })
                        }}>

                        <ShareBySMSModalView
                            setModalVisible={this.setSMSShareModalVisible}
                            onSMSShareSubmit={this.props.onSMSShareSubmit}
                            height={this.state.keyboardHeight}
                        />
                    </Modal>
                    {/* end of share by SMS modal */}

                    {/* start of share by WhatsApp options modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isShareByWhatsAppOptionModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareByWhatsAppOptionModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isShareByWhatsAppOptionModalVisible: false })
                        }}>

                        <ShareByWhatsAppOptionModalView
                            setModalVisible={this.setWhatsAppOptionModalVisible}
                            sendCarDetails={this.props.sendCarDetails}
                            shareCarPhotos={this.props.shareCarPhotos}
                            title ={title}
                        />
                    </Modal>
                    {/* end of share by WhatsApp options modal */}
                    {/* start of remove stock options modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isRemoveStockModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isRemoveStockModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isRemoveStockModalVisible: false })
                        }}>
                        <RemoveStockModalView
                            setModalVisible={this.setRemoveStockOptionModalVisible}
                            clickOnCarBooked={this.clickOnCarBooked}
                            onCarSoldSubmit={this.onCarSoldSubmit}
                            clickOnRenew={this.clickOnRenew}

                        />

                    </Modal>
                    {/* end of remove stock options modal */}
                    {/* start of car sold modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isCarSoldModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isCarSoldModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isCarSoldModalVisible: false })
                        }}>
                        <CarSoldModalView
                            setModalVisible={this.setCarSoldModalVisible}
                            onCarSoldSubmit={this.onCarSoldSubmit}
                            height={this.state.keyboardHeight}
                        />

                    </Modal>
                    {/* end of car sold modal */}
                    {/* start of car sold modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isRemoveRenewStockModalVisible}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isRemoveRenewStockModalVisible: false })
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isRemoveRenewStockModalVisible: false })
                        }}>
                        <RemoveRenewStockModalView
                            setModalVisible={this.setRemoveRenewStockModalVisible}
                            onCarSoldSubmit={this.onCarSoldSubmit}
                        />

                    </Modal>
                    {/* end of car sold modal */}
                    {/* start of update car price modal */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.isPriceChangeModalVisible}
                        onRequestClose={() => {
                            this.props.onNewPriceModalVisible(false)
                        }}
                        onDismiss={() => {
                            // Alert.alert('Modal has been closed.');
                            this.setState({ isPriceChangeModalVisible: false })
                        }}>
                        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK_50 }}>
                            <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                                <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                                    <TouchableOpacity
                                        style={{ alignSelf: 'flex-end', padding: 1 }}
                                        onPress={() => {
                                            this.props.onNewPriceModalVisible(false)
                                        }}>
                                        <Image source={icon_cancel}
                                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                                    </TouchableOpacity>

                                    <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large, marginBottom: Dimens.margin_normal }}>{Strings.ONLY_INSPECTED_PRICE_CHANGE}</Text>
                                    <Text style={{ color: Colors.BLACK_54, fontSize: Dimens.text_small, marginTop: 0 }}>{modelVersion + "(" + reg_no + ")"}</Text>

                                    <TextInputLayout style={{ height: 50, marginTop: 10 }}
                                        ref={(input) => this.TL_NEW_PRICE = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        errorColorMargin={Dimens.margin_0}>
                                        <TextInput style={styles.textInput}
                                            selectTextOnFocus={false}
                                            returnKeyType='go'
                                            keyboardType='numeric'
                                            //maxLength={11}
                                            value={this.props.newPrice ? Utility.formatCurrency(this.props.newPrice) : ''}
                                            placeholder={Strings.ENTER_NEW_PRICE}
                                            ref={(input) => this.newPriceInput = input}
                                            onChangeText={(text) => this.props.onNewPriceChange(text)} />
                                    </TextInputLayout>
                                    {!this.props.showPriceUpdating ? <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.onUpdatePriceSubmit()}>

                                        <View style={styles.buttonStyle}>
                                            <Text style={styles.buttonTextStyle}>{Strings.UPDATE}</Text>
                                        </View>
                                    </TouchableOpacity> :
                                        <View style={styles.buttonStyle}>
                                            <LoadingComponent backgroundColor={Colors.TRANSPARENT} />
                                        </View>
                                    }
                                </View>
                            </View>
                        </SafeAreaView>

                    </Modal>
                    {/* end of update car price modal */}
                    {item ? <StockWebUrlsModal visible={this.props.showUrlSelectionModal} onUrlPress={this.props.onWebUrlItemPress} data={item} /> : null}
                    
                </ScrollView>
                { this.props.isAddToStockViewShow && this.renderAddToStockView()}
            </View>

        )
    }

    renderAddToStockView = () => {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                justifyContent: 'center',
                backgroundColor: Colors.BLACK_54,
                paddingVertical: 50,
                paddingHorizontal: 20
            }}>
                <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                            style={{ backgroundColor: Colors.WHITE, padding: Dimens.padding_xx_large,paddingTop:0, borderRadius: 5 }}>
                    <Text style={[styles.modalText, {marginLeft:0,marginTop: 20,fontSize: Dimens.text_large}]}>{Strings.CONFIRM_ADD_TO_STOCK}</Text>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.onClassifiedCheckboxClick()}>

                        <View style={{ flexDirection: 'row', marginTop: Dimens.margin_x_large,alignItems:'center' }}>
                            <Image source={this.props.isClassifiedCheckboxChecked ? icon_checked : icon_unchecked}
                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                            <Text style={styles.checkboxText}>{Strings.MAKE_CLASSIFIED}</Text>
                        </View>
                    </TouchableOpacity> 
                    <View style={{flexDirection: 'row',justifyContent:'space-between',alignItems:'center',marginTop: Dimens.margin_xxx_large}}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{ padding: 5,paddingLeft:0, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.WHITE }}
                        onPress={() => this.props.hideDialog()}>

                        <Text style={styles.buttonText}>{Strings.CANCEL}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{ padding: 5, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.WHITE }}
                        onPress={() => this.props.okPress()}>

                        <Text style={styles.buttonText}>{(Strings.OK).toUpperCase()}</Text>
                    </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </TouchableOpacity>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    modalImage: {
        height: 30,
        width: 30,
    },
    separator: {
        height: 1,
        //width:'100%',
        backgroundColor: Colors.BLACK_12,
        marginRight: 10,
        marginTop: Dimens.margin_xx_large
    },
    menuOptionText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,

    },
    diasbledMenuOptionText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_25,
        fontSize: Dimens.text_normal,
    },
    mmvText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK,
        fontSize: Dimens.text_x_large,
    },
    priceText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
    },
    viewReportText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.PRIMARY,
        fontSize: Dimens.text_normal,
    },
    labelText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
    },
    containerTab: {
        elevation: 0,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.DIVIDER,
        backgroundColor: Colors.WHITE
    },
    modalText: {
        marginLeft: 10,
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    modalImage: {
        height: 30,
        width: 30,
    },
    seperator: {
        height: 1,
        //width:'100%',
        backgroundColor: Colors.BLACK_12,
        marginRight: 10,
        marginTop: Dimens.margin_normal
    },
    menuOptionText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
    viewPagerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewPager: {
        height: 250,
        width: '100%'
    },
    image: {
        width: Dimens.icon_xx_large,
        height: Dimens.icon_xx_large,
        tintColor: Colors.PRIMARY
    },
    checkboxText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
    buttonText: {
        color: Colors.PRIMARY,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        fontWeight: 'bold',
    },
});