import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView, Linking, PermissionsAndroid, Platform,BackHandler } from 'react-native';
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import { icon_back_arrow, icon_edit, icon_share, icon_menu } from './../../assets/ImageAssets'
import { Strings, Colors } from './../../values'
import { _ActionBarStyle } from '../../values/Styles'
import StockDetailScreen from './StockDetailScreen'
import * as ScreenHoc from './../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import { stockDetailMenu1, stockDetailMenu2 } from './../../util/Constants'
import { getStockDetail, makeStockPremium, removeStock, addRemovedStockToActive, updateCarPrice, shareCardetailEmail, addToBumpup } from './../../api/APICalls'
import { Utility, Constants, AnalyticsConstants } from '../../util';
import { ScreenStates, ScreenName } from '../../util/Constants'
import Share from 'react-native-share'
import AsyncStore from '../../util/AsyncStore'
let isClassified, isFeatured, callbackMethod,toBeAddedToStock
export default class StockDetailContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            item: props.navigation.state.params.item,
            backFromDetailPage: props.navigation.state.params.backFromDetailPage,
            detailData: {
                menuOptions: stockDetailMenu2,
                leadList: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            },
            showLoading: false,
            stockDetailData: {
                usedCarImage: [],
                app_data: []
            },
            showMenu: false,
            isShareOptionModalVisible: false,
            screenState: ScreenStates.IS_LOADING,
            refreshing: false,
            isPriceChangeModalVisible: false,
            newPrice: undefined,
            showPriceUpdating: false,
            emailId: '',
            mobileNo: '',
            usrePhone: '',
            usreName: '',
            showUrlSelectionModal: false,
            isAddToStockViewShow : false,
            isClassifiedCheckboxChecked : false,
        }
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.sendCurrentScreen(AnalyticsConstants.STOCK_DETAIL_SCREEN)
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.PHONE_NO, Constants.NAME])
        this.setState({ usrePhone: values[0][1], usreName: values[1][1] })

        this.getStockDetailData();
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.onBackPress()
        return true;
    }

    onLeadItemClick = (item) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_DEATIL_LEAD_CLICK,{lead_id : item.lead_id})
        this.props.navigation.navigate('leadDetail', {
            leadId: item.lead_id
        })
    }
    onRetryClick = () => {
        this.getStockDetailData()
    }
    getStockDetailData = () => {
        try {
            getStockDetail(this.state.item.dealer_id, this.state.item.id, this.onSuccessStockDetail, this.onFailureStockDetail, this.props);
        } catch (error) {
            Utility.log(error);
        }
        this.setState({
            screenState: ScreenStates.IS_LOADING,
            showMenu: false,
        })
    }
    getStockDetailDataRefresh = () => {
        try {
            getStockDetail(this.state.item.dealer_id, this.state.item.id, this.onSuccessStockDetail, this.onFailureStockDetail);
        } catch (error) {
            Utility.log(error);
        }
        this.setState({
            refreshing: true
        })
    }
    hideMenuCallback = () => {
        this.setState({ showMenu: false })
    }

    onSuccessStockDetail = (response) => {
        Utility.log("apires1:" + JSON.stringify(response))
        this.setState({ screenState: ScreenStates.NO_ERROR, stockDetailData: response.data, refreshing: false })

    }
    onFailureStockDetail = (response) => {
        this.setState({ screenState: ScreenStates.NO_DATA_FOUND, refreshing: false })

        if (response && response.msg) {
            Utility.showToast(response.msg);
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }

    };

    clickOnEdit = () => {
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_START,{car_id : this.state.stockDetailData.id})
        this.setState({ newPrice: this.state.stockDetailData.car_price })
        if (this.state.stockDetailData.certification_status == Constants.STOCK_INSPECTED) {
            this.onNewPriceModalVisible(true)
        }
        else {
            this.props.navigation.navigate("addEditStock", { car_id: this.state.stockDetailData.id, backFromAddPage: this.backFromAddPage })
        }
    }

    backFromAddPage = () => {
        this.getStockDetailData()
    }

    clickOnShare = (value) => {

        if (!value)
            this.setState({ isShareOptionModalVisible: value })
        else
            this.setState({ isShareOptionModalVisible: true })
    }

    clickOnMenu = () => {
        this.setState({ showMenu: !this.state.showMenu })
    }

    clickOnMenuOption = () => {
        this.setState({ showMenu: false })
    }

    setStockPremium = (item, classified, featured) => {
        isClassified = classified
        isFeatured = featured
        itemToModify = item.id
        if (featured == 'true' && item.isclassified == Constants.STOCK_NOT_CLASSIFIED) {
            Utility.showToast(Strings.ONLY_CLASSIFIED_CARS_CAN_FEATURED)
            //callbackMethod();
        }
        else {
            if(isClassified == 'true'){
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_CLASSIFIED_CLICK,{car_id: itemToModify})
            }
            else if(isClassified == 'false'){
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FROM_CLASSIFIED_CLICK,{car_id: itemToModify})
            }
            else if(isFeatured == 'true'){
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_FEATURED_CLICK,{car_id: itemToModify})
            }
            else if(isFeatured == 'false'){
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_CLICK,{car_id: itemToModify})
            } 
            try {
                makeStockPremium(item.dealer_id, item.id, classified, featured, this.makeStockPremiumSuccess, this.makeStockPremiumFailure, this.props)
            } catch (error) {
                Utility.log(error);
            }
        }
    }

    makeStockPremiumSuccess = (response) => {
        if (response.status == 200) {
            if (isClassified == 'true') {
                Utility.showToast(Strings.STOCK_ADDED_TO_CLASSIFIED);
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_CLASSIFIED_SUCCESS,{car_id: itemToModify})
                this.state.backFromDetailPage(Constants.ADD_TO_CLASSIFIED, itemToModify);
            }
            else if (isClassified == 'false') {
                Utility.showToast(Strings.STOCK_REMOVED_FROM_CLASSIFIED);
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLASSIFIED_SUCCESS,{car_id: itemToModify})
                this.state.backFromDetailPage(Constants.REMOVE_FROM_CLASSIFIED, itemToModify);
            }
            else if (isFeatured == 'true') {
                Utility.showToast(Strings.STOCK_ADDED_TO_FEATURED);
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_FEATURED_SUCCESS,{car_id: itemToModify})
                this.state.backFromDetailPage(Constants.ADD_TO_FEATURED, itemToModify);
            }
            else if (isFeatured == 'false') {
                Utility.showToast(Strings.STOCK_REMOVED_FROM_FEATURED);
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_SUCCESS,{car_id: itemToModify})
                this.state.backFromDetailPage(Constants.REMOVE_FROM_FEATURED, itemToModify);
            }
            this.getStockDetailData()
        }
        else {
            Utility.showToast(response.message);
        }
    }

    makeStockPremiumFailure = (response) => {
        if(isClassified == 'true'){
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_CLASSIFIED_FAILURE,{car_id: itemToModify})
        }
        else if(isClassified == 'false'){
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLASSIFIED_FAILURE,{car_id: itemToModify})
        }
        else if(isFeatured == 'true'){
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_FEATURED_FAILURE,{car_id: itemToModify})
        }
        else if(isFeatured == 'false'){
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_FAILURE,{car_id: itemToModify})
        } 
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
    }

    onCarSoldSubmit = (dealer_id, id, reason_id, sold_rice, callback) => {
        callbackMethod = callback
        itemToModify = id
        try {
            this.setState({ showLoading: true })
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLICK,{car_id : itemToModify})
            removeStock(dealer_id, id, reason_id, sold_rice, this.removeStockSuccess, this.removeStockFailure, this.props)
        } catch (error) {
            Utility.log(error);
        }
    }

    removeStockSuccess = (response) => {
        this.setState({ showLoading: false })
        if (response.status == 200) {
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_SUCCESS,{car_id : itemToModify})
            callbackMethod()
            Utility.showToast(Strings.STOCK_REMOVED_MSG);
            this.state.backFromDetailPage(Constants.REMOVE_STOCK, itemToModify);
            this.props.navigation.goBack()
        }
        else {

            Utility.showToast(response.message);
        }
    }

    removeStockFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FAILURE,{car_id : itemToModify})
        this.setState({ showLoading: false })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
    }

    AddToActiveStock = (item) => {
        toBeAddedToStock = item
        if(Constants.APP_TYPE == Constants.INDONESIA){
            this.setState({isAddToStockViewShow : true,showMenu: false})
        }
        else{
            try {
                this.setState({ showLoading: true,showMenu: false })
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_CLICK,{car_id : item.id})
                addRemovedStockToActive(item.id, Constants.ACTIVE_STOCK, this.addToActiveStockSuccess, this.addToActiveFailure, this.props)
            } catch (error) {
                Utility.log(error);
            }
        }
    }

    addToActiveStockSuccess = (response) => {
        if (response.status == 200) {
            Utility.showToast(response.message);
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_SUCCESS)
            this.setState({ showLoading: false,isClassifiedCheckboxChecked:false,isAddToStockViewShow:false }, () => {
                this.onBackPress()
                this.state.backFromDetailPage(Constants.FROM_REMOVED_DETAIL);

            })
        }
        else {
            this.setState({ showLoading: false })
            Utility.showToast(response.message);
        }
    }

    addToActiveFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_FAILURE)
        this.setState({ showLoading: false,isClassifiedCheckboxChecked : false })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
    }
    clickOutSide = () => {
        this.setState({ showMenu: false })
    }

    onNewPriceChange = (text) => {
        //this.refs['addEditStockScreen'].TL_STOCK_PRICE.setError("")
        this.setState({ newPrice: Utility.onlyNumeric(text) })
    }

    onUpdatePriceSubmit = () => {
        itemToModify = this.state.stockDetailData.id
        if (this.state.newPrice && this.state.newPrice != "" && parseInt(this.state.newPrice) != 0) {
            Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_CLICK,{car_id : this.state.stockDetailData.id})
            try {
                this.setState({ showPriceUpdating: true })
                updateCarPrice(this.state.stockDetailData.id, this.state.newPrice, this.updatePriceSuccess, this.updatePriceFailure, this.props)
            } catch (error) {
                Utility.log(error);
            }
        }
        else {
            Utility.showToast(Strings.ENTER_STOCK_PRICE)
        }

    }

    updatePriceSuccess = (response) => {
        //this.setState({showPriceUpdating: false, isPriceChangeModalVisible : false})
        Utility.showToast(response.message);
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_SUCCESS,{car_id : this.state.stockDetailData.id})
        this.state.stockDetailData.car_price = this.state.newPrice
        this.setState({ showPriceUpdating: false, isPriceChangeModalVisible: false })
        this.state.backFromDetailPage(Constants.UPDATE_CAR_PRICE, itemToModify, this.state.newPrice);
    }

    updatePriceFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_FAILURE,{car_id : this.state.stockDetailData.id})
        this.setState({ showPriceUpdating: false })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
    }

    onNewPriceModalVisible = (value) => {
        this.setState({ isPriceChangeModalVisible: value })
    }

    onEmailChange = (text) => {
        this.setState({ emailId: text })
    }

    onEmailShareSubmit = () => {
        if (Utility.isValueNullOrEmpty(this.state.emailId) || !Utility.validateEmail(this.state.emailId))
            Utility.showToast(Strings.ENTER_EMAIL)
        else {
            this.refs['stockDetailScreen'].setEmailShareModalVisible()
            Utility.sendEvent(AnalyticsConstants.STOCK_SHARE_BY_EMAIL_CLICK,{car_id : this.state.stockDetailData.id})
            try {
                shareCardetailEmail(this.state.stockDetailData.id, this.state.emailId, this.onSuccessShare, this.onFailureShare, this.props)
            } catch (error) {
                Utility.log(error)
            }

            this.setState({ showLoading: true })
        }
    }

    onSuccessShare = (response) => {
        if (response && response.message) {
            Utility.sendEvent(AnalyticsConstants.STOCK_SHARE_BY_EMAIL_SUCCESS,{car_id : this.state.stockDetailData.id})
            Utility.showToast(response.message)
        }
        this.setState({ showLoading: false })
    }

    onFailureShare = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_SHARE_BY_EMAIL_FAILURE,{car_id : this.state.stockDetailData.id})
        if (response && response.message) {
            Utility.showToast(response.message)
        }
        this.setState({ showLoading: false })
    }

    getMessage = () => {
        let urlToBeUsed = ""
        let urls = this.state.stockDetailData.web_url
        if (urls) {
            if (urls instanceof Array) {
                urlToBeUsed = urls[0].url
            } else {
                urlToBeUsed = urls
            }
        }
        message = 'Dear Customer' + '\n\n' +
            'Please check out my ' + this.state.stockDetailData.make_year + ' ' + this.state.stockDetailData.make + ' ' + this.state.stockDetailData.modelVersion + ' ' +
            this.state.stockDetailData.fuel_type + ' ' + this.state.stockDetailData.uc_colour + ' color with ' +
            this.state.stockDetailData.km_driven + ' kms driven priced at ' + Utility.displayCurrency(this.state.stockDetailData.car_price) + '.\n\n' +
            urlToBeUsed + '\n\n' +
            'Regards, \n' + this.state.usreName + '\n' + this.state.usrePhone

        return message
    }

    onSMSShareSubmit = async (countryCode, mobileNo, data) => {
        Utility.log('message===>', this.getMessage())
        if (Utility.isValueNullOrEmpty(mobileNo)) {
            Utility.showToast(Strings.INVALID_MOBILE_NUMBER)
        }
        else if (!Utility.validateMobileNo(mobileNo, countryCode, data)) {
            Utility.showToast(Strings.INVALID_MOBILE_NUMBER)
        }
        else {
            this.refs['stockDetailScreen'].setSMSShareModalVisible()

            // TODO... Change message
            let message = await this.getMessage()
            let url = Platform.OS == 'ios' ? `sms:${mobileNo}&body=${message}` : `sms:${mobileNo}?body=${message}`

            // check if we can use this link
            const canOpen = await Linking.canOpenURL(url)

            if (!canOpen) {
                throw new Error('Provided URL can not be handled')
            }
            Utility.sendEvent(AnalyticsConstants.STOCK_SHARE_BY_SMS,{car_id : this.state.stockDetailData.id})
            return Linking.openURL(url)
        }
    }

    sendCarDetails = async () => {
        this.refs['stockDetailScreen'].setWhatsAppOptionModalVisible()
        let message = await this.getMessage()
        let url = `whatsapp://send?text=${message}`
        const shareOptions = {
            // title: 'Share via',
            message: '',
            url: message,
            social: Share.Social.WHATSAPP,
            whatsAppNumber: ''
        }


        const canOpen = await Linking.canOpenURL(url)

        if (!canOpen) {
            Utility.showToast(Strings.WHATSAPP_NOT_INSTALL)
            throw new Error('Provided URL can not be handled')
        }
        else{
            Utility.sendEvent(AnalyticsConstants.STOCK_DETAIL_SHARE_BY_WHATSAPP,{car_id : this.state.stockDetailData.id})
            Share.shareSingle(shareOptions)
        }
    }

    request_storage_runtime_permission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'Gcloud Storage Permission',
                    'message': 'Gcloud App needs access to your storage to download Photos.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                Utility.showToast(Strings.STORAGE_PERMISSION_GRANTED)
            }
            else {
                Utility.showToast(Strings.STORAGE_PERMISSION_NOT_GRANTED)
            }
        } catch (err) {
            console.warn(err)
        }
    }

    shareCarPhotos = async () => {
        this.refs['stockDetailScreen'].setWhatsAppOptionModalVisible()
        // await this.request_storage_runtime_permission()

        let imageArr = []
        for (i = 0; i < this.state.stockDetailData.usedCarImage.length; i++) {
            imageArr.push(this.state.stockDetailData.usedCarImage[i].url)
        }

        if (imageArr.length > 0) {
            // this.setState({ showLoading: true })
            // Utility.downloadMultiImage(imageArr).then((images) => {
            //     Utility.log("images===>>>>", images)
            //     Utility.showToast(Strings.IMAGE_DOWNLOADED)
            //     this.setState({ showLoading: false })
            let message = await this.getMessage()
            Utility.convertImageToBase64(imageArr).then((base64Data) => {
                Utility.log('base64Data==>', base64Data)

                const shareOptions = {
                    title: '',
                    //message: "",
                    urls: base64Data,
                    social: Share.Social.WHATSAPP,
                    whatsAppNumber: ''
                    // filename: 'test',
                };
                Utility.sendEvent(AnalyticsConstants.STOCK_PHOTOS_SHARE_BY_WHATSAPP,{car_id : this.state.stockDetailData.id})
                if (Platform.OS == 'android')
                    Share.shareSingle(shareOptions)
                else{
                    Share.open(shareOptions)
                }
                    
                //Share.shareSingle(shareOptions)

            })
            // })
        }
        else {
            Utility.showToast(Strings.NO_IMAGE)
        }
    }

    goToReviewScreen = (index, data) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_DETAIL_IMAGES_CLICK,{car_id : this.state.stockDetailData.id})
        this.props.navigation.navigate("imageReviewScreen", {
            data: data,
            index: index
        });
    }
    clickOnSeeOnWeb = (urls) => {
        this.hideMenuCallback()
        if (urls) {
            Utility.sendEvent(AnalyticsConstants.STOCK_WEB_URL_CLICK)
            if (urls instanceof Array) {
                if (urls.length > 1) {
                    this.setState({ showUrlSelectionModal: true })
                } else {
                    Utility.openExternalUrl(urls[0].url)
                }
            } else {
                Utility.openExternalUrl(urls)
            }
        }
    }

    onWebUrlItemPress = (success, url) => {
        this.setState({ showUrlSelectionModal: false });
        if (success) {
            Utility.openExternalUrl(url)
        }
    };

    addToBumpup = (item) => {
        itemToModify = item
        this.hideMenuCallback()
        try {
            this.setState({ showLoading: true })
            Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_CLICK,{car_id : item.id})
            addToBumpup(item.id, this.onBumpUpSuccess, this.onBumpUpFailure, this.props)
        } catch (error) {
            Utility.log(error);
        }
    }

    onBumpUpSuccess = (response) => {
        if (response.status == 200) {
            Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_SUCCESS,{car_id : itemToModify.id})
            Utility.showToast(response.message);
            this.state.backFromDetailPage(Constants.ADDED_TO_BUMPUP, itemToModify);
            this.getStockDetailData()
        }
        else {
            this.setState({ showLoading: false })
            Utility.showToast(response.message);
        }
    }

    onBumpUpFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_FAILURE,{car_id : itemToModify.id})
        this.setState({ showLoading: false })
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }

    onClassifiedCheckboxClick = () =>{
        this.setState({isClassifiedCheckboxChecked : !this.state.isClassifiedCheckboxChecked})
    }

    hideDialog = () => {
        this.setState({
            isAddToStockViewShow : false
        })
    };

    okPress = ()=>{
        try {
            this.setState({ showLoading: true,isAddToStockViewShow : false })
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_CLICK,{car_id : toBeAddedToStock.id})
            addRemovedStockToActive(toBeAddedToStock.id, Constants.ACTIVE_STOCK, this.addToActiveStockSuccess, this.addToActiveFailure, this.props,this.state.isClassifiedCheckboxChecked)
        } catch (error) {
            Utility.log(error);
        }
    }
    render() {
        let title = '', car_price = '', reg_no = '', make_year = ''
        if (this.state.stockDetailData) {
            let detail = this.state.stockDetailData;
            title = (detail.make ? detail.make : '') + " " + (detail.modelVersion ? detail.modelVersion : '')
            car_price = detail.car_price ? detail.car_price : ''
            reg_no = detail.reg_no ? detail.reg_no : ''
            make_year = detail.make_year ? detail.make_year : ''
        }
        else {
            title = 'NA'

        }
        this.actionBarProps = {
            values: {
                title: title
            },
            rightIcons: [
                {
                    image: icon_share,
                    onPress: this.clickOnShare
                },
                {
                    image: icon_edit,
                    onPress: this.clickOnEdit
                },
                {
                    image: icon_menu,
                    onPress: this.clickOnMenu
                },

            ],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={this.actionBarProps.values}
                        actions={this.actionBarProps.actions}
                        iconMap={this.actionBarProps.rightIcons}
                        styleAttributes={this.actionBarProps.styleAttr}
                    />
                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LEAD}
                        onRetry={this.onRetryClick}>
                        <StockDetailScreen
                            ref={'stockDetailScreen'}
                            onBackPress={this.onBackPress}
                            item={this.state.item}
                            detailData={this.state.detailData}
                            onLeadItemClick={this.onLeadItemClick}
                            stockDetailData={this.state.stockDetailData}
                            isShareOptionModalVisible={this.state.isShareOptionModalVisible}
                            clickOnShare={this.clickOnShare}
                            showMenu={this.state.showMenu}
                            setStockPremium={this.setStockPremium}
                            hideMenuCallback={this.hideMenuCallback}
                            onCarSoldSubmit={this.onCarSoldSubmit}
                            getStockDetailData={this.getStockDetailDataRefresh}
                            refreshing={this.state.refreshing}
                            AddToActiveStock={this.AddToActiveStock}
                            clickOutSide={this.clickOutSide}
                            isPriceChangeModalVisible={this.state.isPriceChangeModalVisible}
                            onNewPriceChange={this.onNewPriceChange}
                            onUpdatePriceSubmit={this.onUpdatePriceSubmit}
                            onNewPriceModalVisible={this.onNewPriceModalVisible}
                            newPrice={this.state.newPrice}
                            showPriceUpdating={this.state.showPriceUpdating}
                            onEmailChange={this.onEmailChange}
                            onEmailShareSubmit={this.onEmailShareSubmit}
                            onSMSShareSubmit={this.onSMSShareSubmit}
                            shareCarPhotos={this.shareCarPhotos}
                            // shareCarPhotos={this.sendCarDetails}
                            sendCarDetails={this.sendCarDetails}
                            goToReviewScreen={this.goToReviewScreen}
                            MobileNo={this.state.MobileNo}
                            clickOnSeeOnWeb={this.clickOnSeeOnWeb}
                            showUrlSelectionModal={this.state.showUrlSelectionModal}
                            onWebUrlItemPress={this.onWebUrlItemPress}
                            addToBumpup={this.addToBumpup}
                            isAddToStockViewShow = {this.state.isAddToStockViewShow}
                            onClassifiedCheckboxClick = {this.onClassifiedCheckboxClick}
                            isClassifiedCheckboxChecked = {this.state.isClassifiedCheckboxChecked}
                            hideDialog = {this.hideDialog}
                            okPress = {this.okPress}
                        />
                    </WithLoading>

                </View>
            </SafeAreaView>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})