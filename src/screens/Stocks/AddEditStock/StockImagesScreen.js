import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity, Modal, SafeAreaView, FlatList, TouchableHighlight
} from 'react-native'
import { Colors, Strings, Dimens } from "../../../values";
import SortableGrid from '../../../granulars/SortableGrid'
import DraggableFlatList from 'react-native-draggable-flatlist'
import SortableGridview from "../../../granulars/SortableGridView";
import TakePhotoModalView from "./TakePhotosModalView";
import CameraGalleryButtonsComponent from "../../../components/CameraGalleryButtonsComponent";
import ActionBarWrapper from "../../../granulars/ActionBarWrapper";
import ActionBar from "react-native-action-bar";
import * as Utility from "../../../util/Utility";

let self;
export default class StockImagesScreen extends Component {

    constructor() {
        super();
        self = this;
        this.state = {
            scrollEnabled: true
        }
    }

    onDragStart = () => {
        this.setState({ scrollEnabled: false })
    }

    onDragRelease = (itemOrder) => {
        this.setState({ scrollEnabled: true })
        this.props.onOrderChange(itemOrder)
    }

    render() {
        let imageDimen = (Dimensions.get('window').width - 40) / 3;
        let actionBarProps = {
            values: { title: Strings.PHOTOS },
            rightIcons: [{
                image: require('../../../assets/drawable/right.png'),
                onPress: this.props.onDonePress
            }],
            styleAttr: {
                leftIconImage: require('../../../assets/drawable/back.png'),
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        //Utility.log('data', this.props.data)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={{ flex: 1, backgroundColor: Colors.GRAY_LIGHT_BG }}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <View style={{ flex: 1, paddingVertical: Dimens.padding_x_small }}>
                        <ScrollView scrollEnabled={this.state.scrollEnabled}>
                            <View style={{
                                marginHorizontal: Dimens.margin_x_small,
                                marginBottom: Dimens.margin_xx_large,
                                marginTop: Dimens.margin_x_small,
                                backgroundColor: Colors.WHITE,
                                borderRadius: 5
                            }}>
                                <View style={{
                                    flex: 1,
                                    padding: Dimens.padding_large,
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}>
                                    <View style={{
                                        height: 46,
                                        width: 46,
                                        borderRadius: 23,
                                        backgroundColor: Colors.SEA_GREEN,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Image source={require('../../../assets/drawable/drag_drop_icon.png')}
                                            resizeMode={'cover'}
                                            style={{ height: 27, width: 27 }} />
                                    </View>
                                    <Text style={{
                                        flex: 1,
                                        fontSize: Dimens.text_normal,
                                        fontFamily: Strings.APP_FONT,
                                        color: Colors.BLACK_87,
                                        marginLeft: Dimens.margin_xx_large
                                    }}>{Strings.DRAG_DROP_TO_MAKE_COVER_PHOTO}</Text>
                                </View>
                                <View style={styles.triangle} />

                            </View>
                            <View>
                                <FlatList
                                    data={this.props.data}
                                    extraData={this.props}
                                    renderItem={({ item }) => {
                                        return (

                                            <TouchableHighlight
                                                underlayColor={Colors.WHITE}
                                                onPress={() => self.props.onImagePress(item)}
                                                onLongPress={() => self.props._onLongPress(item)}
                                            >
                                                <View style={[styles.block, {
                                                    width: imageDimen,
                                                    height: imageDimen
                                                }]} >
                                                    <Image source={{ uri: item }}
                                                        resizeMode={'cover'} style={{ width: '100%', height: '100%' }} />
                                                    <TouchableOpacity onPress={() => self.props.onImageCancelPress(item)}
                                                        activeOpacity={0.8} style={{
                                                            position: 'absolute',
                                                            top: -8,
                                                            right: -8,
                                                            height: 20,
                                                            width: 20,
                                                            elevation: 5,
                                                            borderRadius: 10,
                                                            backgroundColor: Colors.WHITE,
                                                            alignItems: 'center',
                                                            justifyContent: 'center'
                                                        }}>
                                                        <Image resizeMode={'cover'}
                                                            source={require('../../../assets/drawable/cancel_icon.png')}
                                                            style={{ height: 12, width: 12 }} />
                                                    </TouchableOpacity>
                                                    {/*{index === 0 ? <Image resizeMode={'cover'} source={require('../../../assets/drawable/profile_tag.png')}
                                                        style={{height: 50, width: 50, position: 'absolute', top: 0, left: 0}}/> : null}*/}
                                                </View>
                                            </TouchableHighlight>

                                        );
                                    }}
                                    //Setting the number of column
                                    numColumns={3}
                                    keyExtractor={(item, index) => index.toString()}
                                />

                                {this.props.data && this.props.data.length > 0 && this.state.scrollEnabled && <Image resizeMode={'cover'} source={require('../../../assets/drawable/profile_tag.png')}
                                    style={{ height: 50, width: 50, position: 'absolute', top: 5, left: 5 }} />}
                            </View>

                        </ScrollView>
                        {/*<SortableGridview
                        style={{overflow: 'scroll'}}
                        data={this.props.data}
                        useScrollView={true}
                        headerComponent={() => {
                            return <View style={{
                                marginHorizontal: Dimens.margin_x_small,
                                marginBottom: Dimens.margin_xx_large,
                                marginTop: Dimens.margin_x_small,
                                backgroundColor: Colors.WHITE,
                                borderRadius: 5
                            }}>
                                <View style={{
                                    padding: Dimens.padding_large,
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}>
                                    <View style={{
                                        height: 46,
                                        width: 46,
                                        borderRadius: 23,
                                        backgroundColor: Colors.SEA_GREEN,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Image source={require('../../assets/drawable/drag_drop_icon.png')}
                                               resizeMode={'cover'}
                                               style={{height: 27, width: 27}}/>
                                    </View>
                                    <Text style={{
                                        fontSize: Dimens.text_normal,
                                        fontFamily: Strings.APP_FONT,
                                        color: Colors.BLACK_87,
                                        marginLeft: Dimens.margin_xx_large
                                    }}>{Strings.DRAG_DROP_TO_MAKE_COVER_PHOTO}</Text>
                                </View>
                                <View style={styles.triangle}/>

                            </View>

                        }}
                        footerComponent={() => {
                            return null
                        }}
                        onDragStart={() => {
                            console.log('Default onDragStart');
                        }}
                        onDragRelease={(data) => {
                            this.props.onDragCompleted(data)
                        }}
                        renderItem={(item, index) => {
                            return (
                                <View uniqueKey={item} style={[styles.block, {
                                    width: imageDimen,
                                    height: imageDimen
                                }]}>
                                    <Image source={{uri: item}}
                                           resizeMode={'cover'} style={{width: '100%', height: '100%'}}/>
                                    <TouchableOpacity onPress={() => self.props.onImageCancelPress(item, index)}
                                                      activeOpacity={0.8} style={{
                                        position: 'absolute',
                                        top: -8,
                                        right: -8,
                                        height: 20,
                                        width: 20,
                                        elevation: 5,
                                        borderRadius: 10,
                                        backgroundColor: Colors.WHITE,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Image resizeMode={'cover'}
                                               source={require('../../assets/drawable/cancel_icon.png')}
                                               style={{height: 12, width: 12}}/>
                                    </TouchableOpacity>
                                </View>
                            )
                        }}
                    />*/}
                    </View>

                    <TouchableOpacity onPress={() => this.props.setModalVisible(true)} activeOpacity={0.7} style={{
                        borderRadius: 3,
                        marginHorizontal: Dimens.margin_normal,
                        marginBottom: Dimens.margin_normal,
                        height: 42,
                        backgroundColor: Colors.PRIMARY,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row'
                    }}>
                        <Image source={require('../../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                            style={{
                                height: Dimens.icon_normal,
                                width: Dimens.icon_normal,
                                marginRight: Dimens.margin_large
                            }} />
                        <Text style={{
                            color: Colors.WHITE,
                            fontSize: Dimens.text_large,
                            fontWeight: '400',
                            fontFamily: Strings.APP_FONT
                        }}>{Strings.ADD_PHOTOS}</Text>
                    </TouchableOpacity>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.modalVisible}
                        onRequestClose={() => {
                            this.props.setModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setModalVisible(false)
                        }}>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={() => this.props.setModalVisible(false)}
                                style={{ flex: 1, backgroundColor: Colors.BLACK_40 }} />
                            <CameraGalleryButtonsComponent
                                onCameraPress={this.props.onCameraPress}
                                onGalleryPress={this.props.onGalleryPress} />
                        </View>
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    block: {
        margin: 5,
        elevation: 2,
        borderRadius: 3,
        borderWidth: 5,
        borderColor: Colors.WHITE,
        justifyContent: 'center',
        alignItems: 'center'
    },
    triangle: {
        position: 'absolute',
        bottom: -10,
        left: 25,
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderBottomWidth: 12,
        marginRight: 0,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        transform: [
            { rotate: '-180deg' }
        ]
    }
});
