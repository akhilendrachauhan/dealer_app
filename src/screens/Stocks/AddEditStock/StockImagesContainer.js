import React, {Component} from 'react'

import StockImagesScreen from "./StockImagesScreen";
import * as Utility from "../../../util/Utility";
import * as RNFS from "react-native-fs";
import * as AnalyticsConstants from '../../../util/AnalyticsConstants'
export default class StockImagesContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.navigation.state.params.data ? JSON.parse(JSON.stringify(props.navigation.state.params.data)) : [],
            modalVisible: false,
            order: [],
            selectedImages : props.navigation.state.params.selectedStockImages ? JSON.parse(JSON.stringify(props.navigation.state.params.selectedStockImages)) : [],
            
        }
    }

    componentDidMount(){
        Utility.sendCurrentScreen(AnalyticsConstants.STOCK_IMAGES_SCREEN)
    }
    setModalVisible = (isVisible) => {
        this.setState({modalVisible: isVisible})
    };

    onOrderChange = (itemOrder) => {
        //Utility.log('current order', itemOrder);
        let order = itemOrder.itemOrder;
        let newData = [];
        for (let i = 0; i < order.length; i++) {
            newData.push(order[i].ref)
        }
        //Utility.log('new data', newData);
        this.state.data = newData
    };

    onImageCancelPress = (item) => {
        
        Utility.log('before', this.state.data);
        let data = this.state.data;
        let index = this.state.data.indexOf(item)
        data.splice(index, 1);
        let selectedImages = this.state.selectedImages
        selectedImages.splice(index,1)
        this.setState({data: data,selectedImages : selectedImages});
        Utility.log('after', this.state.data);
    };

    onCameraPress = () => {
        this.props.navigation.navigate('camera', {cameraCallback: this.onCameraDone});
        this.setState({modalVisible: false})
    };

    onCameraDone = (photos) => {
        let data = this.state.data;
        data = data.concat(photos);
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: {latitude: 0, longitude: 0},
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.setState({data: data,selectedImages : selectedItems});
        Utility.log(photos)
    };

    onGalleryDone = (photos) => {
        let data = [];
        for (let i = 0; i < photos.length; i++) {
            data.push(photos[i].uri)
        }
        this.setState({data: data,selectedImages: photos});
        Utility.log(photos)
        
    };

    onGalleryPress = () => {
        let data = this.state.data;
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: {latitude: 0, longitude: 0},
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.props.navigation.navigate("galleryFolder", {
            galleryCallback: this.onGalleryDone,
            selectedItems: selectedItems
        });
        this.setState({modalVisible: false});
    };

    onBackPress = () => {
        this.props.navigation.goBack();
    };

    onDragCompleted = (data) => {
        Utility.log('after drag', data)
    };

    onDonePress = () => {
        this.props.navigation.state.params.onPhotosEditDone && this.props.navigation.state.params.onPhotosEditDone(this.state.data,this.state.selectedImages);
        this.props.navigation.goBack();
    };
    onImagePress = (item) =>{
        let index = this.state.data.indexOf(item)
        this.props.navigation.navigate("imageReviewScreen", {
            data: this.state.data,
            index: index
        });
    }

    _onLongPress = (itemToProfile) => {
        let tempData = [],j=0,item,tempSelected = [],item1
        
        for(let i = 0; i<this.state.data.length ; i++){
            item = this.state.data[i]
            item1 = this.state.selectedImages[i]
            if(item != itemToProfile){
                j = j+1
                tempData[j] = item
                tempSelected[j] = item1
            }
            else{
                tempSelected[0] = item1
                tempData[0] = item
            }

        }
        this.setState({data: tempData,selectedImages : tempSelected})
        
    }
    render() {
        return (
            <StockImagesScreen
                data={this.state.data}
                modalVisible={this.state.modalVisible}
                setModalVisible={this.setModalVisible}
                onGalleryPress={this.onGalleryPress}
                onCameraPress={this.onCameraPress}
                onImageCancelPress={this.onImageCancelPress}
                onBackPress={this.onBackPress}
                onDragCompleted={this.onDragCompleted}
                onDonePress={this.onDonePress}
                onOrderChange={this.onOrderChange}
                onImagePress = {this.onImagePress}
                _onLongPress = {this. _onLongPress}
                />
        )
    }

}
