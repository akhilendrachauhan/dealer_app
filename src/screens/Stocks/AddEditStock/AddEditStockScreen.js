import React, { Component } from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Modal,
    Alert,
    FlatList, TextInput, TouchableWithoutFeedback,
    Platform, KeyboardAvoidingView, Keyboard
} from 'react-native';
import { Colors, Strings, Dimens } from "../../../values"
import TakePhotoModalView from "./TakePhotosModalView";
import CameraGalleryButtonsComponent from "../../../components/CameraGalleryButtonsComponent";
import ActionBarWrapper from "../../../granulars/ActionBarWrapper";
import TextInputLayout from "../../../granulars/TextInputLayout"
import TouchableTextInputLayout from "../../../granulars/TouchableTextInputLayout"
import * as ImageAssets from "../../../assets/ImageAssets";
import * as DBConstants from '../../../database/DBConstants';
import * as Utility from '../../../util/Utility';
import { WheelPicker, TimePicker, DatePicker } from 'react-native-wheel-picker-android';
import YearMonthPicker from "../../../granulars/YearMonthPicker";
import { RadioButton, Orientation } from "../../../granulars/RadioButton";
import DateTimePicker from "react-native-modal-datetime-picker"
import * as Constants from '../../../util/Constants'
import { RichEditor, RichToolbar } from 'react-native-pell-rich-editor';

export default class AddEditStockScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            keyboardHeight: 0,
            inputHeight: 0,
            selectedTag: 'body',
            selectedStyles: []
        }

        this.editor = null;
        this.richText = null
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }

    onStyleKeyPress = (toolType) => {
        this.editor.applyToolbar(toolType);
    }

    onSelectedTagChanged = (tag) => {
        this.setState({ selectedTag: tag })
    }

    onSelectedStyleChanged = (styles) => {
        this.setState({ selectedStyles: styles })
    }

    getValueFromText = async () => {
        let html = await this.richText.getContentHtml();
    }

    renderTopViewWithPhotos = () => {
        let photosData = this.props.stateData.stockPhotos.slice();
        let dummyDataNumber = 4 - this.props.stateData.stockPhotos.length;
        if (dummyDataNumber > 0) {
            for (let i = 0; i < dummyDataNumber; i++) {
                photosData.push(undefined);
            }
        }
        return (
            <View style={Styles.topContainer}>
                <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(true)} activeOpacity={0.7}
                    style={{
                        borderRadius: 3,
                        width: '100%',
                        height: 42,
                        backgroundColor: Colors.PRIMARY,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row'
                    }}>
                    <Image source={require('../../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                        style={{
                            height: Dimens.icon_normal,
                            width: Dimens.icon_normal,
                            marginRight: Dimens.margin_large
                        }} />
                    <Text style={{
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.ADD_PHOTOS}</Text>
                </TouchableOpacity>
                <View
                    style={{ width: '100%', marginVertical: Dimens.margin_xx_large }}>
                    <FlatList horizontal={true} keyExtractor={(item, index) => index.toString()} data={photosData}
                        renderItem={({ item, index }) => <View style={{
                            marginHorizontal: Dimens.margin_x_small,
                            padding: Dimens.padding_x_small,
                            backgroundColor: Colors.WHITE,
                            height: 60,
                            width: 60,
                            borderRadius: 3,
                            elevation: 2
                        }}>
                            {item ? <Image
                                key={index.toString()}
                                source={{ uri: item }}
                                style={{ width: '100%', height: '100%' }}
                                resizeMode={'cover'}
                            /> : <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(true)}
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <Image style={{ height: Dimens.icon_normal, width: Dimens.icon_normal }}
                                        resizeMode={'cover'} source={require('../../../assets/drawable/add.png')} />
                                </TouchableOpacity>}

                        </View>} />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.props.onViewAllPress}>
                        <Text
                            style={{ fontSize: Dimens.text_normal, color: Colors.PRIMARY, fontFamily: Strings.APP_FONT }}>{Strings.VIEW_ALL}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };

    renderTopView = () => {
        return (
            <View style={Styles.topContainer}>
                <Text style={{
                    fontSize: Dimens.text_x_large,
                    fontWeight: '400',
                    color: Colors.BLACK,
                    fontFamily: Strings.APP_FONT
                }}>{Strings.UPLOAD_BEAUTIFUL_PICS}</Text>
                <Image source={require('../../../assets/drawable/image_extension.png')} resizeMode={'cover'}
                    style={{ height: 85, width: 140, margin: Dimens.margin_normal }} />
                <TouchableOpacity onPress={() => this.props.setModalVisible(true)} activeOpacity={0.7} style={{
                    borderRadius: 3,
                    width: '100%',
                    height: 42,
                    backgroundColor: Colors.PRIMARY,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                    <Image source={require('../../../assets/drawable/camera_icon.png')} resizeMode={'cover'}
                        style={{
                            height: Dimens.icon_normal,
                            width: Dimens.icon_normal,
                            marginRight: Dimens.margin_large
                        }} />
                    <Text style={{
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontWeight: '400',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.ADD_PHOTOS}</Text>
                </TouchableOpacity>
            </View>
        )
    };

    renderTenureList = () => {
        if (this.props.stateData.emiTenureList && this.props.stateData.emiTenureList.length > 0) {
            let tenureData = this.props.stateData.emiTenureList;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView >
                            <View style={{ padding: 15 }}>

                                {tenureData.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onTenureSelected(item.key.toString(), item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.key}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.value}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderVersionList = () => {
        if (this.props.stateData.versionList && this.props.stateData.versionList.length > 0) {
            let versionsList = this.props.stateData.versionList;
            let fuelTypes = [];

            for (let i = 0; i < versionsList.length; i++) {
                let obj = versionsList[i];
                if (!fuelTypes.includes(obj[DBConstants.FUEL_TYPE]))
                    fuelTypes.push(obj[DBConstants.FUEL_TYPE]);
            }

            // Categorise versions in Fuels
            let rendererArray = fuelTypes.reduce((arr, currentFuelType) => {
                arr.push({
                    fuel: currentFuelType,
                    versions: versionsList.filter(
                        (singleVersion) => singleVersion[DBConstants.FUEL_TYPE] === currentFuelType
                    )
                });
                return arr;
            }, []);
            if (rendererArray && rendererArray.length > 0) {
                Utility.log("rendererArray First:", JSON.stringify(rendererArray[0]));
            }

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>
                                {rendererArray.map((item) => {
                                    return <View key={item.fuel}>
                                        <Text allowFontScaling={false} style={{
                                            width: "100%",
                                            paddingVertical: 10,
                                            paddingHorizontal: 12,
                                            backgroundColor: "#afafaf"
                                        }}>{item.fuel}</Text>
                                        {item.versions.map((v_item) => {
                                            return <TouchableOpacity
                                                onPress={() => this.props.onVersionSelected(v_item)}
                                                underlayColor={Colors.UNDERLAY_COLOR}
                                                key={v_item[DBConstants.VERSION_ID]}>
                                                <Text allowFontScaling={false}
                                                    style={{
                                                        color: Colors.BLACK_85,
                                                        fontSize: 16,
                                                        padding: 8,
                                                        fontFamily: Strings.APP_FONT
                                                    }}>
                                                    {v_item[DBConstants.VERSION]}
                                                </Text>
                                                <View style={{ height: 1, backgroundColor: Colors.BLACK_25 }} />
                                            </TouchableOpacity>
                                        })}
                                    </View>
                                })}
                            </View>
                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderColorList = () => {
        if (this.props.stateData.colorsList && this.props.stateData.colorsList.length > 0) {
            let colorsList = this.props.stateData.colorsList;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {colorsList.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onColorSelected(item.id, item.color, item.color_code)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.id}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.color}
                                        </Text>
                                        <View style={{ alignSelf: 'flex-end', width: 20, borderColor: Colors.BLACK_54, borderWidth: 1, height: 20, backgroundColor: item.color_code }} />
                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderOwnerList = () => {
        if (this.props.stateData.ownerList && this.props.stateData.ownerList.length > 0) {
            let ownerList = this.props.stateData.ownerList;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView>
                            <View style={{ padding: 15 }}>

                                {ownerList.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onOwnerSelected(item.key, item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.key}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.value}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    renderInsuranceList = () => {
        if (this.props.stateData.insuranceTypeList && this.props.stateData.insuranceTypeList.length > 0) {
            let insuranceTypeList = this.props.stateData.insuranceTypeList;

            return (
                <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center',
                    backgroundColor: Colors.BLACK_54,
                    paddingVertical: 50,
                    paddingHorizontal: 20
                }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                        style={{ backgroundColor: Colors.WHITE, padding: 5, borderRadius: 5 }}>
                        <ScrollView style={{ marginBottom: 50 }}>
                            <View style={{ padding: 15 }}>

                                {insuranceTypeList.map((item) => {
                                    return <TouchableOpacity
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                        onPress={() => this.props.onInsuranceSelected(item.key, item.value)}
                                        underlayColor={Colors.UNDERLAY_COLOR}
                                        key={item.key}>
                                        <Text allowFontScaling={false}
                                            style={{
                                                color: Colors.BLACK_85,
                                                fontSize: 16,
                                                padding: 8,
                                                fontFamily: Strings.APP_FONT
                                            }}>
                                            {item.value}
                                        </Text>

                                    </TouchableOpacity>
                                })}
                            </View>

                        </ScrollView>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    };

    render() {
        let actionBarProps = {
            values: { title: this.props.edit ? Strings.EDIT_STOCK : Strings.ADD_STOCK },
            rightIcons: [],
            styleAttr: {
                leftIconImage: require('../../../assets/drawable/back.png'),
                disableShadows: true,
            }
            ,
            actions: {
                onLeftPress: this.props.onBackPress
            }
        };
        // if(this.richText)
        // this.richText.setContentHTML(this.props.stateData.description)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                {/* <TouchableWithoutFeedback onPress={()=> {
                if(this.richText)
                this.richText.blurContentEditor()
                Keyboard.dismiss()
            }}> */}
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                    <ScrollView >
                        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', margin: Dimens.margin_normal, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight + this.state.inputHeight : 50 }}>
                            {this.props.stateData.stockPhotos.length > 0 ? this.renderTopViewWithPhotos() : this.renderTopView()}
                            { Constants.APP_TYPE == Constants.INDONESIA && !this.props.stateData.isEdit ? <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onClassifiedCheckboxClick()}>

                                <View style={{ flexDirection: 'row', marginTop: Dimens.margin_x_large,alignItems:'center' }}>
                                    <Image source={this.props.stateData.isClassifiedCheckboxChecked ? ImageAssets.icon_checked : ImageAssets.icon_unchecked}
                                        style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                    <Text style={Styles.checkboxText}>{Strings.MAKE_CLASSIFIED}</Text>
                                </View>
                            </TouchableOpacity> : <View/>
                            }
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.stateData.isEdit ? Utility.log("Pressed...") : this.props.onMakeModelPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_MAKE_MODEL = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_0}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedMakeModel ? Constants.APP_TYPE == Constants.PHILIPPINES ? this.props.stateData.selectedMakeModel[DBConstants.MAKE_PARENT_MODEL] : this.props.stateData.selectedMakeModel[DBConstants.MAKE_MODEL] : ''}
                                        placeholder={Strings.MAKE_MODEL + "*"}
                                        ref={(input) => this.makeModelInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' && this.props.stateData.isEdit ? Utility.log("Pressed...") : this.props.onMakeModelPress()}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            {this.props.stateData.selectedMakeModel && Constants.APP_TYPE == Constants.PHILIPPINES && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.stateData.isEdit ? Utility.log("Pressed...") : this.props.onMfgDatePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_MFG_DATE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_0}
                                    rightDrawable={require('../../../assets/drawable/calender.png')}
                                    showRightDrawable={true}
                                    rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                    rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedMfgYear ? this.props.stateData.selectedMfgYear.toString() : ''}
                                        placeholder={Strings.MFG_MONTH_YEAR + "*"}
                                        ref={(input) => this.mfgDateInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' && this.props.stateData.isEdit ? Utility.log("Pressed...") : this.props.onMfgDatePress() }
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            }
                            {this.props.stateData.selectedMfgYear && Constants.APP_TYPE == Constants.PHILIPPINES && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onVersionPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_VERSION = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_0}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedVersion ? this.props.stateData.selectedVersion[DBConstants.VERSION] : ''}
                                        placeholder={Strings.VERSION + "*"}
                                        ref={(input) => this.versionInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onVersionPress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>}
                            {this.props.stateData.selectedMakeModel && Constants.APP_TYPE == Constants.INDONESIA && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onVersionPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_VERSION = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_0}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedVersion ? this.props.stateData.selectedVersion[DBConstants.VERSION] : ''}
                                        placeholder={Strings.VERSION + "*"}
                                        ref={(input) => this.versionInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onVersionPress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>}
                            {this.props.stateData.selectedVersion && Constants.APP_TYPE == Constants.INDONESIA && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onMfgDatePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_MFG_DATE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.PRIMARY}
                                    errorColorMargin={Dimens.margin_0}
                                    rightDrawable={require('../../../assets/drawable/calender.png')}
                                    showRightDrawable={true}
                                    rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                    rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedMfgYear ? this.props.stateData.selectedMfgYear.toString() : ''}
                                        placeholder={Strings.MFG_MONTH_YEAR + "*"}
                                        ref={(input) => this.mfgDateInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onMfgDatePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            }


                            {/* { this.props.stateData.selectedVersion && 
                            <TextInputLayout
                                ref={(input) => this.TL_FUEL_TYPE = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={0}
                                >

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={false}
                                    selectTextOnFocus={false}
                                    value={this.props.stateData.selectedVersion ? this.props.stateData.selectedVersion[DBConstants.FUEL_TYPE] : ''}
                                    placeholder={Strings.FUEL_TYPE+"*"}
                                    ref={(input) => this.mfgFuelTypeInput = input}/>
                            </TextInputLayout> } */}
                            {/* { this.props.stateData.selectedVersion &&
                            <View>
                            <Text style={[Styles.mainText, {marginTop: 20}]}>{Strings.CNG_FITTED}</Text>
                            <RadioButton
                                disabledIndices={this.props.isViewOnly ? [0, 1] : []}
                                ref={"cngFitted"}
                                style={{marginTop: 10, justifyContent: 'flex-start'}}
                                onItemChanged={(newPosition, oldPosition) => {
                                    this.props.onCNGFittedChange(newPosition, oldPosition);
                                }}
                                list={[Strings.YES, Strings.NO]}
                                orientation={Orientation.HORIZONTAL}
                                itemStyle={{marginVertical: 5, alignItems: 'flex-start', marginRight: 100}}
                                defaultSelectedPosition={this.props.stateData.selectedCNGPosition}
                            /> 
                            </View>
                            } */}
                            <View>
                                <TouchableOpacity
                                    activeOpacity={0.8}

                                    onPress={() => this.props.onColorPress()}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_COLOR = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        errorColorMargin={Dimens.margin_0}>
                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={this.props.stateData.selectedColor ? this.props.stateData.selectedColor : ''}
                                            placeholder={Strings.COLOR + "*"}
                                            ref={(input) => this.mfgColorInput = input}
                                            onTouchStart={() => Platform.OS === 'ios' ? this.props.onColorPress() : Utility.log("Pressed...")}
                                        />

                                    </TextInputLayout>
                                </TouchableOpacity>
                                <View style={{ position: 'absolute', right: 0, top: 30, borderWidth: 1, borderColor: Colors.BLACK_54, marginRight: 10, width: 25, height: 25, backgroundColor: this.props.stateData.selectedColorCode ? this.props.stateData.selectedColorCode : 'white' }} />

                            </View>
                            {this.props.stateData.showOtherColor && <TextInputLayout
                                ref={(input) => this.TL_OTHER_COLOR = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'default'}
                                    value={this.props.stateData.otherColor ? this.props.stateData.otherColor : ''}
                                    placeholder={Strings.ENTER_COLOR + "*"}
                                    ref={(input) => this.otherColorInput = input}
                                    onChangeText={(text) => this.props.onOtherColorChange(text)}

                                />
                            </TextInputLayout>}
                            <TextInputLayout
                                ref={(input) => this.TL_KM_DRIVEN = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                            >

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'number-pad'}
                                    value={this.props.stateData.kmsDriven ? Utility.formatNumber(this.props.stateData.kmsDriven) : ''}
                                    placeholder={Strings.KMS_DRIVEN + "*"}
                                    ref={(input) => this.mfgKmDrivenInput = input}
                                    onChangeText={(text) => this.props.onKmsDrivenChange(text)}

                                />
                            </TextInputLayout>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onOwnerPress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_OWNERS = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedOwnerText ? this.props.stateData.selectedOwnerText : ''}
                                        placeholder={Strings.NUMBER_OF_OWNERS}
                                        ref={(input) => this.mfgOwnersInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onOwnerPress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            {this.props.stateData.selectedOwner != '-1' ? <View>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onRegDatePress()}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_REG_DATE = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        errorColorMargin={Dimens.margin_0}
                                        rightDrawable={require('../../../assets/drawable/calender.png')}
                                        showRightDrawable={true}
                                        rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                        rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={Constants.APP_TYPE == Constants.INDONESIA ? (this.props.stateData.selectedRegDate ? this.props.stateData.selectedRegDate : '') : (this.props.stateData.selectedRegYear ? this.props.stateData.selectedRegYear.toString() : '')}
                                            placeholder={Strings.REGISTRATION_MONTH_YEAR}
                                            ref={(input) => this.regDateInput = input}
                                            onTouchStart={() => Platform.OS === 'ios' ? this.props.onRegDatePress() : Utility.log("Pressed...")} />
                                    </TextInputLayout>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onRegistrationCityPress()}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_REGISTRATION_CITY = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        errorColorMargin={Dimens.margin_0}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={this.props.stateData.selectedRegistrationCityText ? this.props.stateData.selectedRegistrationCityText : ''}
                                            placeholder={Strings.REGISTRATION_CITY}
                                            ref={(input) => this.registrationCityInput = input}
                                            onTouchStart={() => Platform.OS === 'ios' ? this.props.onRegistrationCityPress() : Utility.log("Pressed...")}
                                        />
                                    </TextInputLayout>
                                </TouchableOpacity>
                                <TextInputLayout
                                    ref={(input) => this.TL_REG_NO = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}
                                >

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        maxLength={Constants.REGISTRATION_MAX_LENGTH}
                                        autoCapitalize={'characters'}
                                        editable={true}
                                        selectTextOnFocus={false}
                                        // keyboardType = {'default'}
                                        value={this.props.stateData.regNo ? (this.props.stateData.regNo) : ''}
                                        placeholder={Strings.REGISTRATION_NUMBER}
                                        ref={(input) => this.regNoInput = input}
                                        onChangeText={(text) => this.props.onRegNoChange(text)}
                                    />
                                </TextInputLayout>
                                { Constants.APP_TYPE == Constants.INDONESIA ? <View>
                                <Text style={[Styles.mainText, {marginTop: 20}]}>{Strings.PLATE_TYPE}</Text>
                                <RadioButton
                                    disabledIndices={[]}
                                    ref={"oddEvenRadio"}
                                    style={{marginTop: 10, justifyContent: 'flex-start'}}
                                    onItemChanged={(newPosition, oldPosition) => {
                                        this.props.onOddEvenChange(newPosition, oldPosition);
                                    }}
                                    list={this.props.stateData.oddEvenRadioArray}
                                    orientation={Orientation.HORIZONTAL}
                                    itemStyle={{marginVertical: 5, alignItems: 'flex-start', marginRight: 50}}
                                    defaultSelectedPosition={this.props.stateData.oddEvenRadioValue}
                                /> 
                                </View> : <View/>
                                }

                                {Constants.APP_TYPE == Constants.INDONESIA ? <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onPlateDatePress(true)}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_NUMBER_PLATE_DATE = input}
                                        showRightDrawable={true}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        onRightClick={() => this.props.onPlateDatePress(true)}
                                        rightDrawable={require('../../../assets/drawable/calender.png')}
                                        rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                        rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={this.props.stateData.selectedPlateDate ? this.props.stateData.selectedPlateDate : ''}
                                            placeholder={Strings.NUMBER_PLATE_VALIDITY_TILL}
                                            ref={(input) => this.numberPlateDateInput = input}
                                            //onSubmitEditing={() => this.timeInput.focus()}
                                            //onChangeText={(text) => this.props.onDateChange(text)}
                                            onTouchStart={() => Platform.OS === 'ios' ? this.props.onPlateDatePress(true) : Utility.log("Pressed...")} />
                                    </TextInputLayout>
                                </TouchableOpacity> : <View></View>}
                            </View> : <View></View>
                            }
                            <TextInputLayout
                                ref={(input) => this.TL_STOCK_PRICE = input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.ORANGE}
                                errorColorMargin={Dimens.margin_0}
                            >

                                <TextInput
                                    style={Styles.textInput}
                                    returnKeyType='next'
                                    editable={true}
                                    selectTextOnFocus={false}
                                    keyboardType={'number-pad'}
                                    value={this.props.stateData.stockPrice ? Utility.formatCurrency(this.props.stateData.stockPrice) : ''}
                                    placeholder={Strings.STOCK_PRICE + "*"}
                                    ref={(input) => this.stockPriceInput = input}
                                    onChangeText={(text) => this.props.onStockPriceChange(text)}
                                />
                            </TextInputLayout>
                            {Constants.APP_TYPE == Constants.INDONESIA ? <View>
                                <TextInputLayout
                                    ref={(input) => this.TL_DOWN_PAYMENT = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}
                                >

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={true}
                                        selectTextOnFocus={false}
                                        keyboardType={'number-pad'}
                                        value={this.props.stateData.downPayment ? Utility.formatCurrency(this.props.stateData.downPayment) : ''}
                                        placeholder={Strings.DOWN_PAYMENT}
                                        ref={(input) => this.downPaymentInput = input}
                                        onChangeText={(text) => this.props.onDownPaymentChange(text)}
                                    />
                                </TextInputLayout>
                                <TextInputLayout
                                    ref={(input) => this.TL_EMI_AMOUNT = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}
                                >

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={true}
                                        selectTextOnFocus={false}
                                        keyboardType={'number-pad'}
                                        value={this.props.stateData.emiAmount ? Utility.formatCurrency(this.props.stateData.emiAmount) : ''}
                                        placeholder={Strings.EMI}
                                        ref={(input) => this.emiInput = input}
                                        onChangeText={(text) => this.props.onEMIChange(text)}
                                    />
                                </TextInputLayout>

                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onTenurePress()}>
                                    <TextInputLayout
                                        ref={(input) => this.TL_NO_OF_EMIS = input}
                                        focusColor={Colors.ORANGE}
                                        hintColor={Colors.GRAY}
                                        errorColor={Colors.ORANGE}
                                        errorColorMargin={Dimens.margin_0}>
                                        <TextInput
                                            style={Styles.textInput}
                                            returnKeyType='next'
                                            editable={false}
                                            selectTextOnFocus={false}
                                            value={this.props.stateData.noOfEmisText ? this.props.stateData.noOfEmisText : ''}
                                            placeholder={Strings.NO_OF_EMIS}
                                            ref={(input) => this.noOfEmisInput = input}
                                            onTouchStart={() => Platform.OS === 'ios' ? this.props.onTenurePress() : Utility.log("Pressed...")}
                                        />
                                    </TextInputLayout>
                                </TouchableOpacity>
                            </View> : <View></View>
                            }
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onInsurancePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_INSURANCE_TYPE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedInsuranceText ? this.props.stateData.selectedInsuranceText : ''}
                                        placeholder={Strings.INSURANCE_TYPE + "*"}
                                        ref={(input) => this.insuranceInput = input}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onInsurancePress() : Utility.log("Pressed...")}
                                    />
                                </TextInputLayout>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onTaxDatePress(true)}>
                                <TextInputLayout
                                    ref={(input) => this.TL_TAX_DATE = input}
                                    showRightDrawable={true}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    onRightClick={() => this.props.onTaxDatePress(true)}
                                    rightDrawable={require('../../../assets/drawable/calender.png')}
                                    rightDrawableStyle={{ width: 24, resizeMode: 'contain' }}
                                    rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedTaxDate ? this.props.stateData.selectedTaxDate : ''}
                                        placeholder={Strings.TAX_EXPIRY}
                                        ref={(input) => this.taxDateInput = input}
                                        //onSubmitEditing={() => this.timeInput.focus()}
                                        //onChangeText={(text) => this.props.onDateChange(text)}
                                        onTouchStart={() => Platform.OS === 'ios' ? this.props.onTaxDatePress(true) : Utility.log("Pressed...")} />
                                </TextInputLayout>
                            </TouchableOpacity>
                            {/* <TextInputLayout
                                ref={(input) => this.TL_NOTES= input}
                                focusColor={Colors.ORANGE}
                                hintColor={Colors.GRAY}
                                errorColor={Colors.PRIMARY}
                                errorColorMargin={Dimens.margin_0}
                                >
                                <TextInput
                                    style={[Styles.textInput,{height: 70,paddingTop:10}]}
                                    returnKeyType='next'
                                    editable={true}
                                    multiline = {true}
                                    numberOfLines = {3}
                                    selectTextOnFocus={false}
                                    keyboardType = {'default'}
                                    value={this.props.stateData.description ? this.props.stateData.description : ''}
                                    placeholder={Strings.DESCRIPTION}
                                    ref={(input) => this.notesInput = input}
                                    onChangeText={(text) => this.props.onDescriptionChange(text)}
                                />
                            </TextInputLayout> */}
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    if (this.richText)
                                        this.richText.blurContentEditor()
                                    Keyboard.dismiss()
                                }}>

                                <Text style={[Styles.mainText, { marginTop: 20, marginBottom: 10 }]}>{Strings.DESCRIPTION}</Text>
                            </TouchableWithoutFeedback>

                            <View style={Styles.editorStyle}>
                                <RichEditor
                                    ref={(r) => this.richText = r}
                                    initialContentHTML={this.props.stateData.description}
                                    style={Styles.rich}
                                    placeholder={Strings.DESCRIPTION}
                                    scrollEnabled={true} />
                            </View>

                            <KeyboardAvoidingView behavior={'padding'}>
                                <RichToolbar
                                    style={Styles.richBar}
                                    getEditor={() => this.richText}
                                    iconTint={'#000033'}
                                    selectedIconTint={'#2095F2'}
                                    selectedButtonStyle={{ backgroundColor: 'transparent' }}
                                //onPressAddImage={that.onPressAddImage}
                                />
                            </KeyboardAvoidingView>

                            {/* { this.props.stateData.selectedInsuranceText && this.props.stateData.selectedInsuranceText !== Strings.NO_INSURANCE && <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.props.onInsuDatePress()}>
                                <TextInputLayout
                                    ref={(input) => this.TL_INSU_DATE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}
                                    rightDrawable={require('../../../assets/drawable/calender.png')}
                                    showRightDrawable={true}
                                    rightDrawableStyle={{width: 24, resizeMode: 'contain'}}
                                    rightDrawableMarginTop={Dimens.margin_xxx_large}>

                                    <TextInput
                                        style={Styles.textInput}
                                        returnKeyType='next'
                                        editable={false}
                                        selectTextOnFocus={false}
                                        value={this.props.stateData.selectedInsuDate ? this.props.stateData.selectedInsuDate : ''}
                                        placeholder={Strings.VALID_TILL}
                                        ref={(input) => this.insuDateInput = input}
                                        onTouchStart={()=> Platform.OS === 'ios' ? this.props.onInsuDatePress() : Utility.log("Pressed...")}
                                        />
                                </TextInputLayout>
                            </TouchableOpacity> } */}
                            {/* <Text style={[Styles.mainText, {marginTop: 20}]}>{Strings.TAX}</Text>
                            <RadioButton
                                disabledIndices={this.props.isViewOnly ? [0, 1] : []}
                                ref={"cngFitted"}
                                style={{marginTop: 10, justifyContent: 'flex-start'}}
                                onItemChanged={(newPosition, oldPosition) => {
                                    this.props.onTaxChange(newPosition, oldPosition);
                                }}
                                list={[Strings.INDIVIDUAL, Strings.CORPORATE]}
                                orientation={Orientation.HORIZONTAL}
                                itemStyle={{marginVertical: 5, alignItems: 'flex-start', marginRight: 50}}
                                defaultSelectedPosition={this.props.stateData.taxPosition}
                            /> */}
                        </KeyboardAvoidingView>
                    </ScrollView>

                    <View>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={{ padding: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.PRIMARY }}
                            onPress={() => this.props.addStock()}>

                            <Text style={Styles.buttonText}>{this.props.edit ? Strings.EDIT_STOCK : Strings.ADD_STOCK}</Text>
                        </TouchableOpacity>
                    </View>
                    {this.props.stateData.showInsuranceTypeDialog && this.renderInsuranceList()}
                    {this.props.stateData.showOwnerDialog && this.renderOwnerList()}
                    {this.props.stateData.showColorDialog && this.renderColorList()}
                    {this.props.stateData.showVersionDialog && this.renderVersionList()}
                    {this.props.stateData.showTenureDialog && this.renderTenureList()}
                    {this.props.stateData.showMfgDateDialog && <YearMonthPicker
                        startDate={Utility.deltaDate(new Date(), 0, 0, this.props.stateData.totalMfgYear)}
                        endDate={Utility.deltaDate(new Date(), 0, 0, 0)}
                        //startDate = { new Date(this.props.stateData.modelStartYear, 2, 1)} 
                        //endDate = {new Date(this.props.stateData.modelEndYear,2,1)}
                        onDateSelected={this.props.onDateSelected}
                        onDateCancel={this.props.onDateCancel}
                        showMonth={false}
                        richText={this.richText}
                    />}
                    {this.props.stateData.showRegDateDialog && <YearMonthPicker
                        onDateSelected={this.props.onRegDateSelected}
                        startDate={new Date(this.props.stateData.selectedMfgYear, this.props.stateData.selectedMfgMonth - 1)}
                        endDate={new Date()}
                        onDateCancel={this.props.onDateCancel}
                        showMonth={Constants.APP_TYPE == Constants.INDONESIA ? true : false}
                        richText={this.richText}
                    />}
                    {this.props.stateData.showPlateDateDialog && <YearMonthPicker
                        startDate={new Date(this.props.stateData.selectedMfgYear, this.props.stateData.selectedMfgMonth - 1)}
                        endDate={Utility.deltaDate(new Date(), 0, 0, 5)}
                        onDateSelected={this.props.onPlateDateSelected}
                        onDateCancel={this.props.onDateCancel}
                        showMonth={true}
                        richText={this.richText}
                    />}
                    {this.props.stateData.showTaxDateDialog && <YearMonthPicker
                        startDate={new Date(this.props.stateData.selectedMfgYear, this.props.stateData.selectedMfgMonth - 1)}
                        endDate={Utility.deltaDate(new Date(), 0, 0, 2)}
                        onDateSelected={this.props.onTaxDateSelected}
                        onDateCancel={this.props.onDateCancel}
                        showMonth={true}
                        richText={this.richText}
                    />}
                    {this.props.stateData.showInsuDateDialog && <YearMonthPicker
                        startDate={Utility.deltaDate(new Date(), 0, 0, -2)}
                        endDate={Utility.deltaDate(new Date(), 0, 0, 5)}
                        onDateSelected={this.props.onInsuDateSelected}
                        onDateCancel={this.props.onDateCancel}
                        showMonth={true}
                        richText={this.richText}
                    />}
                    <DateTimePicker
                        minimumDate={Utility.deltaDate(new Date(), 0, 0, -2)}
                        maximumDate={Utility.deltaDate(new Date(), 0, 0, 5)}
                        isVisible={this.props.stateData.showDatePicker}
                        onConfirm={this.props._handleDatePicked}
                        onCancel={this.props._hideDateTimePicker}
                        mode={'date'}
                    />
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.props.stateData.modalVisible}
                        onRequestClose={() => {
                            this.props.setModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setModalVisible(false)
                        }}>
                        <TakePhotoModalView
                            setModalVisible={this.props.setModalVisible}
                            onCameraPress={this.props.onCameraPress}
                            onGalleryPress={this.props.onGalleryPress} />
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.stateData.transparentModalVisible}
                        onRequestClose={() => {
                            this.props.setTransparentModalVisible(false)
                        }}
                        onDismiss={() => {
                            this.props.setTransparentModalVisible(false)
                        }}>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={() => this.props.setTransparentModalVisible(false)}
                                style={{ flex: 1, backgroundColor: Colors.BLACK_40 }} />
                            <CameraGalleryButtonsComponent
                                onCameraPress={this.props.onCameraPress}
                                onGalleryPress={this.props.onGalleryPress} />
                        </View>
                    </Modal>
                </View>
                {/* </TouchableWithoutFeedback> */}
            </SafeAreaView>
        )
    }

}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    topContainer: {
        backgroundColor: Colors.GRAY_LIGHT,
        alignItems: 'center',
        padding: Dimens.padding_xxx_large,
        margin: Dimens.margin_normal,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.GRAY,
        borderStyle: 'dashed'
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK,
        alignSelf: 'stretch',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    mainText: {
        fontSize: Dimens.text_large,
        //fontWeight: '400',
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    editorStyle: {
        flex: 1,
        height: 200,
        marginTop: 10,
        paddingVertical: 10,
        borderWidth: 0.5,
        borderColor: Colors.BLACK_54,
    },
    richText: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    richBar: {
        height: 50,
        backgroundColor: '#F5FCFF',
    },
    rich: {
        minHeight: 100,
        height: 200,
        flex: 1,

    },
    checkboxText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
});
