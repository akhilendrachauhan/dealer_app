import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity,SafeAreaView} from 'react-native';
import {Colors, Strings, Dimens} from "../../../values";
import CameraGalleryButtonsComponent from "../../../components/CameraGalleryButtonsComponent";

const TakePhotosModalView = ({setModalVisible, onCameraPress, onGalleryPress}) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY_DARK}}>
        <View style={{flex: 1, backgroundColor: Colors.SCREEN_BACKGROUND}}>
            <View style={{flex: 1}}>
                <View style={{
                    backgroundColor: Colors.PRIMARY,
                    height: Dimens.toolbar_height,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={() => {
                        setModalVisible(false)
                    }} activeOpacity={0.7} style={{
                        marginHorizontal: 18,
                        height: '100%',
                        justifyContent: 'center',
                        width: Dimens.icon_normal
                    }}>
                        <Image source={require('../../../assets/drawable/back.png')} resizeMode={'cover'}
                               style={{height: Dimens.icon_x_small, width: Dimens.icon_x_small}}/>
                    </TouchableOpacity>
                    <Text style={{
                        fontSize: Dimens.text_large,
                        color: Colors.WHITE,
                        fontFamily: Strings.APP_FONT,
                        fontWeight: 'normal'
                    }}>{Strings.PHOTOS}</Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center', margin: Dimens.margin_xxx_large}}>
                    <Text style={{
                        fontSize: Dimens.text_xx_large,
                        color: Colors.WHITE,
                        textAlign: 'center',
                        fontFamily: Strings.APP_FONT
                    }}>{Strings.COVER_PHOTO_LOOK_LIKE}</Text>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center', width: '100%', height: 170}}>
                    <View style={{
                        height: 174,
                        width: 300,
                        borderWidth: 2,
                        borderRadius: 3,
                        borderColor: Colors.WHITE
                    }}>
                        <Image resizeMode={'cover'}
                               source={require('../../../assets/drawable/stock_profile_coach.png')}
                               style={{height: 170, width: 296}}/>
                        <Image resizeMode={'cover'} source={require('../../../assets/drawable/profile_tag.png')}
                               style={{height: 70, width: 70, position: 'absolute', top: 0, left: 0}}/>
                    </View>
                </View>
                <Text style={{
                    fontSize: Dimens.text_normal,
                    fontFamily: Strings.APP_FONT,
                    color: Colors.WHITE_54,
                    textAlign: 'center',
                    width: '100%',
                    marginTop: Dimens.margin_xxx_large,
                    marginBottom: Dimens.margin_normal
                }}>
                    {Strings.BENEFITS_OF_FIVE_PLUS_PHOTOS}
                </Text>
                <View style={{flexDirection: 'row'}}>
                    <View style={Styles.coachContainer}>
                        <Image resizeMode={'cover'} source={require('../../../assets/drawable/increase_coach.png')}
                               style={Styles.coachIcon}/>
                        <Text style={Styles.coachTextBold}>{Strings.INCREASE}</Text>
                        <Text style={Styles.coachText}>{Strings.TRUST_IN_BUYERS}</Text>
                    </View>
                    <View style={Styles.coachContainer}>
                        <Image resizeMode={'cover'} source={require('../../../assets/drawable/more_coach.png')}
                               style={Styles.coachIcon}/>
                        <Text style={Styles.coachTextBold}>{Strings.FIVE_TIMES}</Text>
                        <Text style={Styles.coachText}>{Strings.MORE_ENQURIES}</Text>
                    </View>
                    <View style={Styles.coachContainer}>
                        <Image resizeMode={'cover'} source={require('../../../assets/drawable/quick_coach.png')}
                               style={Styles.coachIcon}/>
                        <Text style={Styles.coachTextBold}>{Strings.SELL}</Text>
                        <Text style={Styles.coachText}>{Strings.YOUR_CAR_QUICKLY}</Text>
                    </View>
                </View>
            </View>
            <CameraGalleryButtonsComponent
                onCameraPress={onCameraPress}
                onGalleryPress={onGalleryPress}/>
        </View>
        </SafeAreaView>
    )

};


const Styles = StyleSheet.create({
    coachIcon: {
        width: Dimens.icon_x_large,
        height: Dimens.icon_x_large
    },
    coachTextBold: {
        fontSize: Dimens.text_x_large,
        color: Colors.WHITE_87,
        fontFamily: Strings.APP_FONT,
        fontWeight: '400'
    },
    coachText: {
        fontSize: Dimens.text_normal,
        color: Colors.WHITE_54,
        fontFamily: Strings.APP_FONT
    },
    coachContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default TakePhotosModalView;
