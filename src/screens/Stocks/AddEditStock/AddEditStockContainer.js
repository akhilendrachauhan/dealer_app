import React, { Component } from 'react';
import { PermissionsAndroid, Platform, Keyboard, BackHandler } from 'react-native';
import AddEditStockScreen from "./AddEditStockScreen";
import * as Utility from "../../../util/Utility";
import DBFunctions from "../../../database/DBFunctions";
import * as DBConstants from '../../../database/DBConstants';
import { Strings } from '../../../values';
import * as Constants from '../../../util/Constants'
import { addStock, getColorList, getStockDetail,checkAutoClassified } from '../../../api/APICalls'
import * as RNFS from "react-native-fs";
import { ScreenStates, ScreenName } from '../../../util/Constants'
import ImageUploadFile from '../../../util/ImageUploadFile'
import ImageResizer from 'react-native-image-resizer';
import DateFormat from '../../../util/DateFormat'
import { AnalyticsConstants } from '../../../util'
let isViewOnly = false;
let edit = false;
let uc_color, uc_colour_id

export default class AddEditStockContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            transparentModalVisible: false,
            stockPhotos: [],
            selectedMakeModel: undefined,
            versionList: undefined,
            selectedVersion: undefined,
            showVersionDialog: false,
            showMfgDateDialog: false,
            selectedMfgDate: undefined,
            selectedColor: undefined,
            selectedRegDate: undefined,
            regNo: undefined,
            stockPrice: undefined,
            colorsList: [],
            showColorDialog: false,
            kmsDriven: undefined,
            selectedOwner: '-1',
            selectedOwnerText: undefined,
            showOwnerDialog: false,
            selectedInsurance: undefined,
            selectedInsuranceText: undefined,
            ownerList: [],//Constants.OWNER_LIST,
            showRegDateDialog: false,
            showInsuranceTypeDialog: false,
            insuranceTypeList: [],//Constants.INSURANCE_TYPE_LIST,
            selectedRegistrationCity: undefined,
            selectedRegistrationCityText: undefined,
            showInsuDateDialog: false,
            selectedRegDate: undefined,
            selectedMfgMonth: undefined,
            selectedMfgYear: undefined,
            selectedRegMonth: undefined,
            selectedRegYear: undefined,
            selectedInsuMonth: undefined,
            selectedInsuYear: undefined,
            isCNGFitted: Constants.CNG_NOT_FITTED,
            selectedTax: Constants.CORPORATE,
            selectedColorId: undefined,
            screenState: ScreenStates.NO_ERROR,
            backFromAddPage: props.navigation.state.params ? props.navigation.state.params.backFromAddPage : null,
            car_id: props.navigation.state.params ? props.navigation.state.params.car_id : null,
            selectedCNGPosition: 1,
            taxPosition: 1,
            selectedStockImages: [],
            allUploadedImages: [],
            selectedColorDone: 1,
            selectedColorCode: undefined,
            downPayment: '',
            emiAmount: '',
            noOfEmisText: '',
            noOfEmisValue: '',
            emiTenureList: [],//Constants.EMI_TENURE,
            showTenureDialog: false,
            numberPlateExpiredate: '',
            numberPlateExpireFormattedDate: '',
            showDatePicker: false,
            showOtherColor: false,
            otherColor: '',
            // description: '',
            description: "<p></p>",
            selectedPlateDate: undefined,
            selectedPlateMonth: undefined,
            selectedPlateYear: undefined,
            selectedTaxDate: undefined,
            selectedTaxMonth: undefined,
            selectedTaxYear: undefined,
            showPlateDateDialog: false,
            showTaxDateDialog: false,
            modelStartYear: 0,
            modelEndYear: 0,
            isEdit: false,
            totalMfgYear: -20,
            isClassifiedCheckboxChecked: false,
            oddEvenRadioValue : -1,
            oddEvenRadioText : "",
            oddEvenRadioArray : []
        }

        this.dbFunctions = new DBFunctions();
    }
    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Utility.sendCurrentScreen(AnalyticsConstants.ADD_EDIT_STOCK_SCREEN)
        if (this.state.car_id) {
            edit = true
            this.setState({ isEdit: true }, () => {
                this.getStockDetailData();
            })

        }
        else {
            edit = false
            this.setState({ isEdit: false }, () => {
                if(Constants.APP_TYPE == Constants.INDONESIA){
                    this.getAutoClassifiedCheck()
                }
            })
        }
        let insurance_list = await Utility.getConfigArrayData(Constants.INSURANCE_TYPE_VALUES)
        let owner_list = await Utility.getConfigArrayData(Constants.OWNER_TYPE_VALUES)
        let emi_list = await Utility.getConfigArrayData(Constants.EMI_TENURE_VALUES)
        let oddEvenRadioArray = await Utility.getConfigArrayData(Constants.VEHICLE_NUMBER_TYPE)
        let totalMfgYear = await Utility.getValueFromAsyncStorage(Constants.TOTAL_MFG_YEAR)
        //alert(JSON.stringify(emi_list))
        var oddEvenArray = []
        if(oddEvenRadioArray){
            oddEvenArray = oddEvenRadioArray.map((obj, index) => {
                return obj.value
            })
        }
        this.setState({ totalMfgYear: parseInt(totalMfgYear), ownerList: owner_list, insuranceTypeList: insurance_list, emiTenureList: emi_list, oddEvenRadioArray :oddEvenArray })
    }

    handleBackPress = () => {
        this.onBackPress();
        return true;
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }

    onBackPress = () => {
        Utility.confirmDialog(Strings.ALERT, Strings.GO_BACK_FROM_LOAN, Strings.CANCEL, Strings.YES, this.confirmCallback)
    }

    confirmCallback = (status) => {
        if (status) {
            this.props.navigation.goBack();
        }
    }

    getStockDetailData = () => {
        try {
            getStockDetail('', this.state.car_id, this.onSuccessStockDetail, this.onFailureStockDetail, this.props);
        } catch (error) {
            Utility.log(error);
        }
        this.setState({
            screenState: ScreenStates.IS_LOADING,
            showMenu: false,
        })
    }

    onSuccessStockDetail = (response) => {
        Utility.log("apires1:" + JSON.stringify(response))
        let stockDetailData = response.data
        let own_type = stockDetailData.owner_type && stockDetailData.owner_type != "" ? stockDetailData.owner_type : "-1"
        var owner = this.state.ownerList.filter(obj => {
            return obj.key === own_type
        })
        var insuType = this.state.insuranceTypeList.filter(obj => {
            return obj.key === parseInt(stockDetailData.insurance)
        })
        //stockDetailData.usedCarImage.shift()
        var images = stockDetailData.usedCarImage.map((obj, index) => {
            return obj.url
        })
        edit = true;
        uc_color = stockDetailData.uc_colour
        uc_colour_id = stockDetailData.uc_colour_id
        //let dbFunctions = new DBFunctions();

        this.dbFunctions.getCityById(stockDetailData.reg_place_city_id).then(result => {
            this.setState({ selectedRegistrationCityText: result && result[0] && result[0][DBConstants.CITY] ? result[0][DBConstants.CITY] : undefined })
        });
        this.dbFunctions.getMMVByVersionId(stockDetailData.version_id).then(result => {
            this.getColorList(stockDetailData.version_id);

            this.setState({ selectedMakeModel: result && result[0] ? result[0] : undefined, selectedVersion: result && result[0] ? result[0] : undefined }, () => {
                if (Constants.APP_TYPE == Constants.INDONESIA)
                    this.getVersionList(result[0], true)
                else
                    this.getVersionByMakeParentModel(true)
            })
        });
        let oddEvenRadioValue = -1,oddEvenRadioText=""
        if(stockDetailData.number_type && stockDetailData.number_type != ""){
            var upperCaseNames = this.state.oddEvenRadioArray.map(function(value) {
                return value.toUpperCase();
            });
            let index = upperCaseNames.indexOf((stockDetailData.number_type).toUpperCase())
            oddEvenRadioValue = index
            if(index != -1)
                oddEvenRadioText = this.state.oddEvenRadioArray[index]
        }
        this.setState({
            screenState: ScreenStates.NO_ERROR,
            regNo: stockDetailData.reg_no,
            kmsDriven: stockDetailData.km_driven.toString(),
            stockPrice: stockDetailData.car_price.toString(),
            selectedOwner: own_type,
            selectedOwnerText: owner && owner[0] && owner[0].value ? owner[0].value : undefined,
            selectedCNGPosition: stockDetailData.cngFitted == '1' ? 0 : 1,
            taxPosition: stockDetailData.tax_type == 'individual' ? 0 : 1,
            selectedInsurance: stockDetailData.insurance,
            selectedInsuranceText: insuType && insuType[0] && insuType[0].value ? insuType[0].value : undefined,
            selectedInsuMonth: stockDetailData.valid_month,
            selectedInsuYear: stockDetailData.valid_year,
            selectedInsuDate: Constants.months[stockDetailData.valid_month - 1] + '/' + stockDetailData.valid_year,
            selectedRegMonth: stockDetailData.reg_month && stockDetailData.reg_month != '' ? stockDetailData.reg_month : undefined,
            selectedRegYear: stockDetailData.reg_year && stockDetailData.reg_year != '' ? stockDetailData.reg_year : undefined,
            selectedRegDate: stockDetailData.reg_year && stockDetailData.reg_month && stockDetailData.reg_month != '' && stockDetailData.reg_year != '' ? (Constants.months[stockDetailData.reg_month - 1] + '/' + stockDetailData.reg_year) : undefined,
            selectedMfgMonth: stockDetailData.make_month,
            selectedMfgYear: stockDetailData.make_year,
            selectedMfgDate: Constants.months[stockDetailData.make_month - 1] + '/' + stockDetailData.make_year,
            selectedRegistrationCity: stockDetailData.reg_place_city_id,
            stockPhotos: images,
            allUploadedImages: stockDetailData.usedCarImage,
            downPayment: parseInt(stockDetailData.down_payment),
            emiAmount: parseInt(stockDetailData.emi_amount),
            noOfEmisText: stockDetailData.total_emi ? stockDetailData.total_emi.toString() : '',
            noOfEmisValue: stockDetailData.total_emi ? stockDetailData.total_emi.toString() : '',
            otherColor: stockDetailData.otherColor,
            selectedPlateMonth: stockDetailData.reg_valid_month && stockDetailData.reg_valid_month != '' ? stockDetailData.reg_valid_month : undefined,
            selectedPlateYear: stockDetailData.reg_valid_year && stockDetailData.reg_valid_year != '' ? stockDetailData.reg_valid_year : undefined,
            selectedPlateDate: stockDetailData.reg_valid_month && stockDetailData.reg_valid_year && stockDetailData.reg_valid_month != '' && stockDetailData.reg_valid_year != '' ? (Constants.months[stockDetailData.reg_valid_month - 1] + '/' + stockDetailData.reg_valid_year) : undefined,
            selectedTaxMonth: stockDetailData.tax_expiry_month && stockDetailData.tax_expiry_month != '' ? stockDetailData.tax_expiry_month : undefined,
            selectedTaxYear: stockDetailData.tax_expiry_year && stockDetailData.tax_expiry_year != '' ? stockDetailData.tax_expiry_year : undefined,
            selectedTaxDate: stockDetailData.tax_expiry_month && stockDetailData.tax_expiry_year && stockDetailData.tax_expiry_month != '' && stockDetailData.tax_expiry_year != '' ? (Constants.months[stockDetailData.tax_expiry_month - 1] + '/' + stockDetailData.tax_expiry_year) : undefined,
            //numberPlateExpiredate : stockDetailData.reg_valid_date ? DateFormat(stockDetailData.reg_valid_date, 'dd mmm, yyyy') : '',
            //numberPlateExpireFormattedDate : stockDetailData.reg_valid_date ? stockDetailData.reg_valid_date : ''
            // description: stockDetailData.description
            description: stockDetailData.description ? stockDetailData.description : "",
            oddEvenRadioValue : oddEvenRadioValue,
            oddEvenRadioText : oddEvenRadioText
        }, () => {
            this.refs['addEditStockScreen'].richText.setContentHTML(this.state.description)
        })

    }
    onFailureStockDetail = (response) => {
        this.setState({ screenState: ScreenStates.NO_DATA_FOUND })

        if (response && response.msg) {
            Utility.showToast(response.msg);
        } else {
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }

    };

    async askForPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]);
            return granted[PermissionsAndroid.PERMISSIONS.CAMERA] === PermissionsAndroid.RESULTS.GRANTED && granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            console.warn(err);
            return false;
        }
    }

    onCameraPress = () => {
        this.props.navigation.navigate('camera', { cameraCallback: this.onCameraDone });
        this.setState({ modalVisible: false, transparentModalVisible: false })
    };

    onCameraDone = (photos) => {
        let data = this.state.stockPhotos;
        data = data.concat(photos);
        let selectedItems = []
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: { latitude: 0, longitude: 0 },
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.setState({ stockPhotos: data, selectedStockImages: selectedItems });
        Utility.log(photos)
    };

    onGalleryDone = (photos) => {
        let data = [];
        for (let i = 0; i < photos.length; i++) {
            data.push(photos[i].uri)
        }
        this.setState({ stockPhotos: data, selectedStockImages: photos });

        Utility.log(photos)

        // const promises = photos.map((item, index) => {
        //     var pattern = new RegExp('^(https?|ftp)://');
        //     const {uri} = item;
        //     if(pattern.test(uri) ) {
        //         return item
        //     } else if(uri.startsWith("ph:") || uri.startsWith("PH:")){

        //     var regex = /:\/\/(.{36})\//i;
        //     var result = uri.match(regex);
        //     const dest = RNFS.TemporaryDirectoryPath+Math.random().toString(36).substring(7)+".jpg";
        //     promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id="+result[1]+"&ext=JPG", dest,0,0);         
        //     return promise
        //         .then((resultUri) => {
        //             photos[index].uri = resultUri;
        //     });
        //     }
        //     else{
        //         return item
        //     }
        // })
        // Promise.all(promises)
        //     .then(() => {
        //         Utility.log(photos)
        //         this.setState({selectedStockImages : photos})
        // });
    };

    onGalleryPress = () => {
        let data = this.state.stockPhotos;
        let selectedItems = [];
        for (let i = 0; i < data.length; i++) {
            let singleData = {
                type: 'image/jpeg',
                location: { latitude: 0, longitude: 0 },
                timestamp: 0,
                height: 0,
                width: 0,
                uri: data[i]
            };
            selectedItems.push(singleData);
        }
        this.props.navigation.navigate("galleryFolder", {
            galleryCallback: this.onGalleryDone,
            selectedItems: selectedItems
        });
        this.setState({ modalVisible: false, transparentModalVisible: false });
    };

    setModalVisible = (isVisible) => {
        this.checkPermissionAndOpenModal(isVisible, false);
    };

    setTransparentModalVisible = (isVisible) => {
        this.checkPermissionAndOpenModal(isVisible, true);
    };

    checkPermissionAndOpenModal = (isVisible, isTransparent) => {
        if (isVisible) {
            if (Platform.OS === 'android') {
                this.askForPermission().then(value => {
                    isVisible = value;
                    if (!value) {

                    }
                    if (isTransparent) {
                        this.setState({ transparentModalVisible: value })
                    } else {
                        this.setState({ modalVisible: value })
                    }
                });
            } else {
                if (isTransparent) {
                    this.setState({ transparentModalVisible: isVisible })
                } else {
                    this.setState({ modalVisible: isVisible })
                }
            }
        } else {
            if (isTransparent) {
                this.setState({ transparentModalVisible: isVisible })
            } else {
                this.setState({ modalVisible: isVisible })
            }
        }
    };

    onPhotosEditDone = (data, selectedImages) => {
        this.setState({ stockPhotos: data, selectedStockImages: selectedImages })
    };

    onViewAllPress = () => {
        if (Platform.OS === 'android') {
            this.askForPermission().then(value => {
                if (!value) {

                } else {
                    this.props.navigation.navigate('stockImages', {
                        data: this.state.stockPhotos,
                        selectedStockImages: this.state.selectedStockImages,
                        onPhotosEditDone: this.onPhotosEditDone
                    })
                }
            });
        } else {
            this.props.navigation.navigate('stockImages', {
                data: this.state.stockPhotos,
                selectedStockImages: this.state.selectedStockImages,
                onPhotosEditDone: this.onPhotosEditDone
            })
        }
    };

    onMakeModelPress = () => {
        //let dbFunctions = new DBFunctions();
        if (Constants.APP_TYPE == Constants.PHILIPPINES) {
            this.props.navigation.navigate('searchContainer', {
                dbQuery: this.dbFunctions.getDistinctMakeParentModelsQueryText(),
                hintText: Strings.SEARCH_BY_MAKE_MODEL,
                callback: this.onMakeModelSelected
            })
        }
        else {
            this.props.navigation.navigate('searchContainer', {
                dbQuery: this.dbFunctions.getDistinctMakeModelsQueryText(),
                hintText: Strings.SEARCH_BY_MAKE_MODEL,
                callback: this.onMakeModelSelected
            })
        }
    };

    onMakeModelSelected = (mmv) => {

        Utility.log('Selected MMV', mmv)
        this.refs['addEditStockScreen'].TL_MAKE_MODEL.setError("")
        if (Constants.APP_TYPE == Constants.PHILIPPINES) {
            this.setState({
                selectedMakeModel: mmv,
                //versionList: result
            }, () => {
                //this.getModelMinMaxYear(mmv[DBConstants.MODEL_PARENT_ID])
                if (this.state.selectedMfgDate)
                    this.getVersionByMakeParentModel()
            })
        }
        else {
            //let dbFunctions = new DBFunctions();
            this.state.versionList = undefined;
            this.state.selectedVersion = undefined;
            this.state.selectedMfgDate = undefined
            this.state.selectedMfgMonth = undefined
            this.state.selectedMfgYear = undefined
            this.dbFunctions.getVersionByMakeModel(mmv[DBConstants.MAKE_ID], mmv[DBConstants.MODEL_ID]).then(result => {
                this.setState({
                    selectedMakeModel: mmv,
                    versionList: result
                })
                Utility.log(mmv)
                Utility.log(result)
            });
        }
    };

    getModelMinMaxYear = (parent_model_id) => {
        this.dbFunctions.getModelMinMaxYear(parent_model_id).then(result => {
            if (result && result.length > 0)
                this.setState({ modelStartYear: result[0].minYear, modelEndYear: result[0].maxYear }, () => {
                    //alert(result[0].minYear+":"+result[0].maxYear)
                })
            else
                this.setState({ modelStartYear: 0, modelEndYear: 0 }, () => {
                    //alert(result[0].minYear+":"+result[0].maxYear)
                })

        });
    }

    getVersionList = (mmv, fromEdit) => {
        //let dbFunctions = new DBFunctions();
        this.state.versionList = undefined;
        if (!fromEdit)
            this.state.selectedVersion = undefined;
        this.dbFunctions.getVersionByMakeModel(mmv[DBConstants.MAKE_ID], mmv[DBConstants.MODEL_ID]).then(result => {
            this.setState({
                versionList: result
            })

        });
    }

    onRegistrationCityPress = () => {
        //let dbFunctions = new DBFunctions();
        this.props.navigation.navigate('searchContainer', {
            dbQuery: this.dbFunctions.getCityQueryText(),
            hintText: Strings.SEARCH_BY_CITY,
            callback: this.onRegistrationCitySelected
        })
    };

    onRegistrationCitySelected = (city) => {
        this.refs['addEditStockScreen'].TL_REGISTRATION_CITY.setError("")
        this.setState({
            selectedRegistrationCity: city[DBConstants.CITY_ID],
            selectedRegistrationCityText: city[DBConstants.CITY],
        })
    };

    onVersionPress = () => {
        if (this.state.versionList.length === 0) {
            Utility.showToast(Strings.VERSION_DOES_NOT_EXIST);
        }
        Keyboard.dismiss()
        this.setState({ showVersionDialog: true })

    };

    onVersionSelected = (version) => {
        //alert(JSON.stringify(version))
        this.refs['addEditStockScreen'].TL_VERSION.setError("")
        this.setState({ showVersionDialog: false, selectedVersion: version }, () => {
            this.getColorList(version.vn_id);
        })
    };

    hideDialog = () => {
        this.setState({
            showVersionDialog: false,
            showMfgDateDialog: false,
            showColorDialog: false,
            showOwnerDialog: false,
            showRegDateDialog: false,
            showInsuranceTypeDialog: false,
            showInsuDateDialog: false,
            showTenureDialog: false,
            showPlateDateDialog: false,
            showTaxDateDialog: false,
        })
    };

    onMfgDatePress = () => {
        Keyboard.dismiss()
        this.setState({ showMfgDateDialog: true })
    };

    onRegDatePress = () => {
        Keyboard.dismiss()
        if (this.state.selectedMfgDate)
            this.setState({ showRegDateDialog: true })
        else
            Utility.showToast(Strings.SELECT_MANUFACTERED_DATE_FIRST)
    };

    onInsuDatePress = () => {
        Keyboard.dismiss()
        if (this.state.selectedMfgDate)
            this.setState({ showInsuDateDialog: true })
        else
            Utility.showToast(Strings.SELECT_MANUFACTERED_DATE_FIRST)
    };

    onDateSelected = (year, monthIndex, monthText) => {
        this.refs['addEditStockScreen'].TL_MFG_DATE.setError("")
        Utility.log(year, monthIndex);
        if (Constants.APP_TYPE == Constants.PHILIPPINES) {
            this.setState({
                selectedMfgDate: monthText + '/' + year, selectedMfgMonth: monthIndex + 1, selectedMfgYear: year, selectedRegDate: undefined, selectedRegMonth: undefined, selectedRegYear: undefined, selectedPlateDate: undefined,
                selectedPlateMonth: undefined,
                selectedPlateYear: undefined,
                selectedTaxDate: undefined,
                selectedTaxMonth: undefined,
                selectedTaxYear: undefined,
            }, () => {
                this.getVersionByMakeParentModel()
            })
        }
        else {
            this.setState({
                selectedMfgDate: monthText + '/' + year, selectedMfgMonth: monthIndex + 1, selectedMfgYear: year, selectedRegDate: undefined, selectedRegMonth: undefined, selectedRegYear: undefined, selectedPlateDate: undefined,
                selectedPlateMonth: undefined,
                selectedPlateYear: undefined,
                selectedTaxDate: undefined,
                selectedTaxMonth: undefined,
                selectedTaxYear: undefined,
            })

        }
        this.hideDialog();

    };

    getVersionByMakeParentModel = (fromEdit) => {
        //let dbFunctions = new DBFunctions();
        this.state.versionList = undefined;
        if (!fromEdit)
            this.state.selectedVersion = undefined;
        Utility.log(this.state.selectedMakeModel);
        this.dbFunctions.getVersionByModelParentYear(this.state.selectedMakeModel[DBConstants.MODEL_PARENT_ID], this.state.selectedMfgYear, this.state.selectedMakeModel[DBConstants.YEAR_CONDITION_ENABLED]).then(result => {
            if (result && result.length === 0) {
                Utility.showToast(Strings.VERSION_DOES_NOT_EXIST);
            }
            this.setState({
                //selectedMakeModel: mmv,
                versionList: result
            })
            //Utility.log(mmv)
            Utility.log(result)
        });
    }

    onRegDateSelected = (year, monthIndex, monthText) => {
        this.refs['addEditStockScreen'].TL_REG_DATE.setError("")
        Utility.log(year, monthIndex);
        this.setState({ selectedRegDate: monthText + '/' + year, selectedRegMonth: monthIndex + 1, selectedRegYear: year })
        this.hideDialog();
    };

    onInsuDateSelected = (year, monthIndex, monthText) => {
        this.refs['addEditStockScreen'].TL_INSU_DATE.setError("")
        Utility.log(year, monthIndex);
        this.setState({ selectedInsuDate: monthText + '/' + year, selectedInsuMonth: monthIndex + 1, selectedInsuYear: year })
        this.hideDialog();
    };

    onDateCancel = () => {
        this.hideDialog()
    };

    onCNGFittedChange = (newPos, oldPos) => {
        if (newPos == 1)
            this.setState({ isCNGFitted: Constants.CNG_NOT_FITTED, selectedCNGPosition: 1 })
        else
            this.setState({ isCNGFitted: Constants.CNG_FITTED, selectedCNGPosition: 0 })
    }

    onTaxChange = (newPos, oldPos) => {

        if (newPos == 1)
            this.setState({ selectedTax: 'corporate', taxPosition: 1 })
        else
            this.setState({ selectedTax: 'individual', taxPosition: 0 })
    }

    onColorPress = () => {
        Keyboard.dismiss()
        this.setState({ showColorDialog: true })
    };

    onColorSelected = (id, color, color_code) => {
        this.refs['addEditStockScreen'].TL_COLOR.setError("")
        if (id == 0)
            this.setState({ showOtherColor: true, selectedColorDone: 1, showColorDialog: false, selectedColorId: id, selectedColor: color, selectedColorCode: color_code })
        else
            this.setState({ showOtherColor: false, selectedColorDone: 1, showColorDialog: false, selectedColorId: id, selectedColor: color, selectedColorCode: color_code })

    };

    onOtherColorChange = (text) => {
        this.refs['addEditStockScreen'].TL_OTHER_COLOR.setError("")
        this.setState({ otherColor: text })
    }

    onOwnerPress = () => {
        Keyboard.dismiss()
        this.setState({ showOwnerDialog: true })
    };

    onOwnerSelected = (owner, value) => {
        this.refs['addEditStockScreen'].TL_OWNERS.setError("")
        this.setState({ showOwnerDialog: false, selectedOwner: owner, selectedOwnerText: value })
    };

    onInsurancePress = () => {
        Keyboard.dismiss()
        this.setState({ showInsuranceTypeDialog: true })
    };

    onInsuranceSelected = (ins, value) => {
        // alert(ins + ":" + (ins == Constants.NO_INSURANCE))
        this.refs['addEditStockScreen'].TL_INSURANCE_TYPE.setError("")
        // if(ins == Constants.NO_INSURANCE)
        //     this.setState({showInsuranceTypeDialog: false, selectedInsurance: ins,selectedInsuranceText : value,selectedInsuMonth : '',selectedInsuYear : ''})
        // else
        this.setState({ showInsuranceTypeDialog: false, selectedInsurance: ins, selectedInsuranceText: value })
    };



    onKmsDrivenChange = (text) => {
        this.refs['addEditStockScreen'].TL_KM_DRIVEN.setError("")
        this.setState({ kmsDriven: Utility.onlyNumeric(text) })
    }

    onRegNoChange = (text) => {
        //this.setState({regNo : text})
        let old = this.state.regNo
        if (text != '') {
            this.refs['addEditStockScreen'].TL_REG_NO.setError("")
            if (Utility.registrationNumber(text)) {
                this.setState({ regNo: text })
            }
            else {
                this.setState({ regNo: old })
            }
        }
        else {
            this.setState({ regNo: '' })
        }
    }
    onStockPriceChange = (text) => {
        this.refs['addEditStockScreen'].TL_STOCK_PRICE.setError("")
        this.setState({ stockPrice: Utility.onlyNumeric(text) })
    }

    onDownPaymentChange = (text) => {
        this.setState({ downPayment: Utility.onlyNumeric(text) })
    }

    onEMIChange = (text) => {
        this.setState({ emiAmount: Utility.onlyNumeric(text) })
    }
    onNoOfEmisChange = (text) => {
        this.setState({ noOfEmis: Utility.onlyNumeric(text) })
    }

    getColorList = (vn_id) => {

        getColorList(vn_id, this.onSuccessColor, this.onFailureColor);
    }

    onSuccessColor = (response) => {
        if (response && response.data && response.data.length > 0) {

            this.setState({ colorsList: response.data })
            if (edit) {

                var colors = response.data.filter(obj => {
                    return obj.id === uc_colour_id
                })
                if (uc_colour_id == 0)
                    this.setState({ showOtherColor: true, selectedColorId: colors[0].id, selectedColor: colors[0].color, selectedColorCode: colors[0].color_code, otherColor: uc_color })
                else
                    this.setState({ showOtherColor: false, selectedColorId: colors[0].id, selectedColor: colors[0].color, selectedColorCode: colors[0].color_code })
            }
        }
        else
            Utility.showToast();

    }
    onFailureColor = (response) => {
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }

    };
    saveImagesToDB = (car_id, makeModel) => {

        var pattern = new RegExp('^(https?|ftp)://');
        let photos = this.state.stockPhotos
        const promises = photos.map((item, index) => {
            //var pattern = new RegExp('^(https?|ftp)://');
            const uri = item;
            if (pattern.test(uri)) {
                return item
            } else if (uri.startsWith("ph:") || uri.startsWith("PH:")) {

                var regex = /:\/\/(.{36})\//i;
                var result = uri.match(regex);
                const dest = RNFS.TemporaryDirectoryPath + Math.random().toString(36).substring(7) + ".jpg";
                promise = RNFS.copyAssetsFileIOS("assets-library://asset/asset.JPG?id=" + result[1] + "&ext=JPG", dest, 0, 0);
                return promise
                    .then((resultUri) => {
                        photos[index] = resultUri;
                    });
            }
            else {
                return item
            }

        })
        Promise.all(promises)
            .then(() => {
                Utility.log("photos.......", JSON.stringify(photos))
                //this.setState({selectedStockImages : photos},()=>{
                if (photos.length > 0) {
                    //let dbFunctions = new DBFunctions();
                    this.dbFunctions.deleteImagesByCarId(car_id).then(result => {
                        Utility.log("deleted " + JSON.stringify(result));
                        this.SaveDB(photos, car_id, makeModel)

                    });
                }
                else {
                    let imageUploadFile = new ImageUploadFile()
                    imageUploadFile.checkAndUpload(car_id)
                }
            })
        //});

    };

    SaveDB = (photos, car_id, makeModel) => {

        Utility.log('Image insertion count 1' + JSON.stringify(this.state.allUploadedImages));
        let obj, index, filename, count = 0
        //let dbFunctions = new DBFunctions();
        var pattern = new RegExp('^(https?|ftp)://');
        for (i = 0; i < photos.length; i = i + 1) {
            obj = photos[i]
            index = obj.lastIndexOf("/") + 1;
            filename = obj.substr(index);
            if (pattern.test(obj)) {
                var uploadedImage = this.state.allUploadedImages.filter(obj1 => {
                    return obj1.url === obj
                })
                this.dbFunctions.addImage(car_id.toString(), makeModel, uploadedImage[0].image_name, obj, Constants.IMAGE_S3_UPLOAD_STATUS, photos.length, i).then((result) => {
                    Utility.log('Image insertion count 2', result);
                    count++
                    if (count == photos.length) {
                        Utility.log('Image insertion count 3');
                        let imageUploadFile = new ImageUploadFile()
                        imageUploadFile.checkAndUpload(car_id)
                    }
                })
            } else {
                this.dbFunctions.addImage(car_id.toString(), makeModel, filename, obj, Constants.IMAGE_INITIAL_STATUS, photos.length, i).then((result) => {
                    Utility.log('Image insertion count 2', result);
                    count++
                    if (count == photos.length) {
                        Utility.log('Image insertion count 3');
                        let imageUploadFile = new ImageUploadFile()
                        imageUploadFile.checkAndUpload(car_id)
                    }
                })
            }
        }
    }

    onDescriptionChange = (text) => {
        // this.refs['addEditStockScreen'].TL_NOTES.setError("")
        this.setState({ description: text })
    }

    addStock = async () => {
        let html = ""
        html = await this.refs['addEditStockScreen'].richText.getContentHtml();
        var regex = /(<([^>]+)>)/ig;
        var result = html.replace(regex, "");
        result = result.replace(/&nbsp;/g, "")
        result = result.replace(/\n/g, "")
        result = result.trim()
        let textLength = result.length ? result.length : 0
        const promise = Utility.getMultipleValuesFromAsyncStorage([Constants.DEALER_ID, Constants.DEALER_CITY, Constants.PLATE_NO_VALIDATION]);
        promise.then((values) => {
            try {
                if (this.state.selectedMakeModel) {
                    if ((Constants.APP_TYPE == Constants.PHILIPPINES && this.state.selectedMfgDate) || Constants.APP_TYPE == Constants.INDONESIA) {
                        if (this.state.selectedVersion) {

                            let makeModel = this.state.selectedMakeModel['makeModel']
                            let version = this.state.selectedVersion[DBConstants.VERSION_ID]
                            let mfgMonth = this.state.selectedMfgMonth ? this.state.selectedMfgMonth : ''
                            let mfgYear = this.state.selectedMfgYear ? this.state.selectedMfgYear : ''
                            let cngFitted = this.state.isCNGFitted
                            let color = this.state.selectedColorId == undefined ? '' : this.state.selectedColorId == 0 ? '0' : this.state.selectedColorId
                            let otherColor = this.state.otherColor ? this.state.otherColor : ''
                            let kmsDriven = this.state.kmsDriven
                            let owner = this.state.selectedOwner
                            let regMonth = this.state.selectedRegMonth ? this.state.selectedRegMonth : ''
                            let regYear = this.state.selectedRegYear ? this.state.selectedRegYear : ''
                            let city = this.state.selectedRegistrationCity ? this.state.selectedRegistrationCity : ''
                            let regNo = this.state.regNo ? this.state.regNo : ''
                            let stockPrice = this.state.stockPrice
                            let insuType = this.state.selectedInsurance ? this.state.selectedInsurance : ''
                            let insuMonth = this.state.selectedInsuMonth ? this.state.selectedInsuMonth : ''
                            let insuYear = this.state.selectedInsuYear ? this.state.selectedInsuYear : ''
                            let tax = this.state.selectedTax
                            let downPayment = this.state.downPayment
                            let emiAmount = this.state.emiAmount
                            let noOfEmis = this.state.noOfEmisText
                            // let description = this.state.description
                            // let description = convertToHtmlString(this.state.description)
                            // let textlength = 0
                            // for(let i=0;i<this.state.description[0].content.length;i++){
                            //     textlength = textlength + this.state.description[0].content[i].len
                            // }
                            description = html
                            let plateMonth = this.state.selectedPlateMonth ? this.state.selectedPlateMonth : ''
                            let plateYear = this.state.selectedPlateYear ? this.state.selectedPlateYear : ''
                            let taxMonth = this.state.selectedTaxMonth ? this.state.selectedTaxMonth : ''
                            let taxYear = this.state.selectedTaxYear ? this.state.selectedTaxYear : ''
                            if (Utility.isValueNullOrEmpty(mfgYear.toString())) {
                                this.refs['addEditStockScreen'].TL_MFG_DATE.setError(Strings.SELECT_MFG_DATE)
                                this.refs['addEditStockScreen'].mfgDateInput.focus()
                                return
                            }
                            else if (color == '' && color != '0') {
                                this.setState({ selectedColorDone: 0 })
                                this.refs['addEditStockScreen'].TL_COLOR.setError(Strings.SELECT_COLOR)
                                this.refs['addEditStockScreen'].mfgColorInput.focus()
                                return
                            }
                            else if (color == '0' && Utility.isValueNullOrEmpty(otherColor)) {
                                this.refs['addEditStockScreen'].TL_OTHER_COLOR.setError(Strings.ENTER_COLOR)
                                this.refs['addEditStockScreen'].otherColorInput.focus()
                                return
                            }
                            else if (Utility.isValueNullOrEmpty(kmsDriven)) {
                                this.refs['addEditStockScreen'].TL_KM_DRIVEN.setError(Strings.ENTER_KM_DRIVEN)
                                this.refs['addEditStockScreen'].mfgKmDrivenInput.focus()
                                return
                            }
                            else if (Constants.APP_TYPE == Constants.INDONESIA && (owner == null || owner == undefined)) {
                                this.refs['addEditStockScreen'].TL_OWNERS.setError(Strings.SELECT_OWNER)
                                this.refs['addEditStockScreen'].mfgOwnersInput.focus()
                                return
                            }
                            else if (owner != '-1' && Utility.isValueNullOrEmpty(regYear.toString())) {
                                this.refs['addEditStockScreen'].TL_REG_DATE.setError(Strings.SELECT_REG_DATE)
                                this.refs['addEditStockScreen'].regDateInput.focus()
                                return
                            }
                            else if (owner != '-1' && Utility.isValueNullOrEmpty(city.toString())) {
                                this.refs['addEditStockScreen'].TL_REGISTRATION_CITY.setError(Strings.SELECT_REG_CITY)
                                this.refs['addEditStockScreen'].registrationCityInput.focus()
                                return
                            }
                            else if (Constants.APP_TYPE == Constants.INDONESIA && owner != '-1' && Utility.isValueNullOrEmpty(regNo)) {
                                this.refs['addEditStockScreen'].TL_REG_NO.setError(Strings.ENTER_REGISTRATION_NO)
                                this.refs['addEditStockScreen'].regNoInput.focus()
                                return
                            }
                            // else if (Constants.APP_TYPE == Constants.INDONESIA && owner != '-1' && regNo.length < 3) {
                            //     this.refs['addEditStockScreen'].TL_REG_NO.setError(Strings.ENTER_VALID_REGISTRATION_NO)
                            //     this.refs['addEditStockScreen'].regNoInput.focus()
                            //     return
                            // }
                            // else if (owner != '-1' && Utility.isValueNullOrEmpty(this.state.numberPlateExpiredate) ) {
                            //     this.refs['addEditStockScreen'].TL_NUMBER_PLATE_DATE.setError(Strings.SELECT_NUMBER_PLATE_VALIDITY)
                            //     this.refs['addEditStockScreen'].numberPlateDateInput.focus()
                            //     return
                            // }
                            // else if (Constants.APP_TYPE == Constants.INDONESIA && owner != '-1' && Utility.isValueNullOrEmpty(regNo)) {
                            //     this.refs['addEditStockScreen'].TL_REG_NO.setError(Strings.ENTER_VALID_REGISTRATION_NO)
                            //     this.refs['addEditStockScreen'].regNoInput.focus()
                            //     return
                            // }
                            else if (owner != '-1' && !Utility.isValueNullOrEmpty(regNo) && !Utility.validatePlateNo(regNo, values[2][1])) {
                                this.refs['addEditStockScreen'].TL_REG_NO.setError(Strings.ENTER_VALID_REGISTRATION_NO)
                                this.refs['addEditStockScreen'].regNoInput.focus()
                                return
                            }
                            else if (Utility.isValueNullOrEmpty(stockPrice)) {
                                this.refs['addEditStockScreen'].TL_STOCK_PRICE.setError(Strings.ENTER_STOCK_PRICE)
                                this.refs['addEditStockScreen'].stockPriceInput.focus()
                                return
                            }
                            else if (Utility.isValueNullOrEmpty(insuType.toString())) {
                                this.refs['addEditStockScreen'].TL_INSURANCE_TYPE.setError(Strings.SELECT_INSURANCE_TYPE)
                                this.refs['addEditStockScreen'].insuranceInput.focus()
                                return
                            }
                            else if (Constants.APP_TYPE == Constants.INDONESIA && !Utility.isValueNullOrEmpty(downPayment.toString()) && parseInt(downPayment) >= parseInt(stockPrice)) {
                                Utility.showToast(Strings.DOWN_PAYMENT_NOT_GREATER_THAN_STOCK_PRICE)
                                return
                            }
                            else if (Constants.APP_TYPE == Constants.INDONESIA && !Utility.isValueNullOrEmpty(emiAmount.toString()) && parseInt(emiAmount) >= parseInt(stockPrice)) {
                                Utility.showToast(Strings.EMI_AMOUNT_NOT_GREATER_THAN_STOCK_PRICE)
                                return
                            }
                            else if (textLength != 0 && textLength < 100) {
                                Utility.showToast(Strings.INVALID_DESCRIPTION)
                                return
                                // this.refs['addEditStockScreen'].TL_NOTES.setError(Strings.INVALID_DESCRIPTION)
                                // this.refs['addEditStockScreen'].notesInput.focus()
                                // return
                            }
                            else {
                                Keyboard.dismiss()
                                this.setState({ screenState: ScreenStates.IS_LOADING })
                                try {
                                    if (this.state.car_id) {
                                        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_CLICK, { car_id: this.state.car_id })
                                    } else {
                                        Utility.sendEvent(AnalyticsConstants.STOCK_ADD_CLICK)
                                    }
                                    addStock(this.state.car_id, values[0][1], version, mfgMonth, mfgYear, cngFitted, this.state.selectedColorId, kmsDriven, owner
                                        , regMonth, regYear, city, regNo, stockPrice, insuType, insuMonth, insuYear, tax, downPayment, emiAmount,
                                        noOfEmis, this.state.numberPlateExpireFormattedDate, otherColor, values[1][1], description, plateMonth, plateYear, taxMonth, taxYear,this.state.isClassifiedCheckboxChecked,this.state.oddEvenRadioText,
                                        this.onSuccessAddStock, this.onFailureAddStock, this.props);
                                } catch (error) {
                                    Utility.log(error);
                                }
                            }
                        }
                        else {
                            this.refs['addEditStockScreen'].TL_VERSION.setError(Strings.SELECT_VERSION)
                            this.refs['addEditStockScreen'].versionInput.focus()
                            return
                        }
                    }
                    else {
                        this.refs['addEditStockScreen'].TL_MFG_DATE.setError(Strings.SELECT_MFG_DATE)
                        this.refs['addEditStockScreen'].mfgDateInput.focus()
                        return
                    }
                }
                else {
                    this.refs['addEditStockScreen'].TL_MAKE_MODEL.setError(Strings.SELECT_MAKE_MODEL)
                    this.refs['addEditStockScreen'].makeModelInput.focus()
                    return
                }
            } catch (error) {
                Utility.log(error);
            }
        }).catch((error) => {
            Utility.log("errrrrrrrrrrrrrrrrrrrrr", 'error');
        })
    }


    onSuccessAddStock = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        if (this.state.car_id) {
            Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_SUCCESS, { car_id: this.state.car_id })
        } else {
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_SUCCESS, { car_id: response.data.id })
        }
        Utility.log('onSuccessAddLead===> ', JSON.stringify(response))
        this.setState({ screenState: ScreenStates.NO_ERROR }, () => {
            let makeModel = this.state.selectedMakeModel['makeModel']
            this.saveImagesToDB(edit ? this.state.car_id : response.data.id, makeModel);
            if (this.state.backFromAddPage)
                this.state.backFromAddPage();
            this.props.navigation.goBack()
        })
    }
    onFailureAddStock = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        if (this.state.car_id) {
            Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_FAILURE, { car_id: this.state.car_id })
        } else {
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_FAILURE)
        }

        Utility.log('onFailureAddLead===> ', JSON.stringify(response))
        this.setState({ screenState: ScreenStates.NO_ERROR })
    }


    onTenurePress = () => {
        this.setState({ showTenureDialog: true })
    }

    onTenureSelected = (text, value) => {
        this.refs['addEditStockScreen'].TL_NO_OF_EMIS.setError("")
        this.setState({ showTenureDialog: false, noOfEmisText: text, noOfEmisValue: value })
    };

    onNumberPlateDatePress = (status) => {
        Keyboard.dismiss()
        if (this.state.selectedMfgDate)
            this.setState({ showDatePicker: status })
        else
            Utility.showToast(Strings.SELECT_MANUFACTERED_DATE_FIRST)

    }

    onDateChange = (text) => {
        this.setState({ numberPlateExpiredate: text })
    }

    _handleDatePicked = (date) => {
        // 23 Oct, 2019 // 2109-10-02
        let dateFormat = DateFormat(date.toString(), 'dd mmm, yyyy')
        let formatedDate = DateFormat(date.toString(), 'yyyy-mm-dd')
        this.setState({ numberPlateExpiredate: dateFormat, numberPlateExpireFormattedDate: formatedDate })
        this.refs['addEditStockScreen'].TL_NUMBER_PLATE_DATE.setError('')
        this._hideDateTimePicker();
    }

    _hideDateTimePicker = () => {
        this.setState({ showDatePicker: false })
    }

    onPlateDatePress = () => {
        Keyboard.dismiss()
        if (this.state.selectedMfgDate)
            this.setState({ showPlateDateDialog: true })
        else
            Utility.showToast(Strings.SELECT_MANUFACTERED_DATE_FIRST)
    };

    onPlateDateSelected = (year, monthIndex, monthText) => {
        this.refs['addEditStockScreen'].TL_NUMBER_PLATE_DATE.setError("")
        Utility.log(year, monthIndex);
        this.setState({ selectedPlateDate: monthText + '/' + year, selectedPlateMonth: monthIndex + 1, selectedPlateYear: year })
        this.hideDialog();
    };

    onTaxDatePress = () => {
        Keyboard.dismiss()
        if (this.state.selectedMfgDate)
            this.setState({ showTaxDateDialog: true })
        else
            Utility.showToast(Strings.SELECT_MANUFACTERED_DATE_FIRST)

    };

    onTaxDateSelected = (year, monthIndex, monthText) => {
        this.refs['addEditStockScreen'].TL_TAX_DATE.setError("")
        Utility.log(year, monthIndex);
        this.setState({ selectedTaxDate: monthText + '/' + year, selectedTaxMonth: monthIndex + 1, selectedTaxYear: year })
        this.hideDialog();
    };

    onClassifiedCheckboxClick = () =>{
        this.setState({isClassifiedCheckboxChecked : !this.state.isClassifiedCheckboxChecked})
    }

    onOddEvenChange = (newPos, oldPos) => {
        this.setState({ oddEvenRadioValue : newPos,oddEvenRadioText : this.state.oddEvenRadioArray[newPos]},()=>{
            //alert(this.state.oddEvenRadioText)
        })
    }

    getAutoClassifiedCheck = () =>{
        try {
            checkAutoClassified(this.onSuccessAutoClassifiedCheck, this.onFailureAutoClassifiedCheck, this.props);
        } catch (error) {
            Utility.log(error);
        }
    }
    onSuccessAutoClassifiedCheck = (response)=>{
        if (response && response.data ) {
            this.setState({ isClassifiedCheckboxChecked : response.data.can_classified })
        }
    }

    onFailureAutoClassifiedCheck = (response) =>{
        if (response && response.message) {
            Utility.showToast(response.message);
        } else {
            Utility.showToast(SERVER_API_FAILURE_MSG);
        }
    }

    render() {
        // this.state.ownerList = [
        //     { id: '0', name: '1st ' + Strings.OWNER },
        //     { id: '1', name: '2nd ' + Strings.OWNER },
        //     { id: '2', name: '3rd ' + Strings.OWNER },
        //     { id: '3', name: '4th ' + Strings.OWNER },
        //     { id: '4', name: '4+ ' + Strings.OWNER },
        //     { id: '6', name: Strings.UNREGISTERED },
        // ]
        // this.state.insuranceTypeList = [
        //     { id: '1', name: Strings.NO_INSURANCE },
        //     { id: '2', name: Strings.TLO },
        //     { id: '3', name: Strings.ALL_RISK },
        //     { id: '4', name: Strings.COMBINED },
        // ]
        return (
            <AddEditStockScreen
                ref={'addEditStockScreen'}
                stateData={this.state}
                onViewAllPress={this.onViewAllPress}
                setModalVisible={this.setModalVisible}
                onCameraPress={this.onCameraPress}
                onGalleryPress={this.onGalleryPress}
                setTransparentModalVisible={this.setTransparentModalVisible}
                onBackPress={this.onBackPress}
                onMakeModelPress={this.onMakeModelPress}
                onVersionPress={this.onVersionPress}
                onVersionSelected={this.onVersionSelected}
                hideDialog={this.hideDialog}
                onMfgDatePress={this.onMfgDatePress}
                onDateSelected={this.onDateSelected}
                onDateCancel={this.onDateCancel}
                isViewOnly={isViewOnly}
                onCNGFittedChange={this.onCNGFittedChange}
                onColorSelected={this.onColorSelected}
                onColorPress={this.onColorPress}
                onKmsDrivenChange={this.onKmsDrivenChange}
                onOwnerPress={this.onOwnerPress}
                onOwnerSelected={this.onOwnerSelected}
                onRegDatePress={this.onRegDatePress}
                onRegDateSelected={this.onRegDateSelected}
                onRegNoChange={this.onRegNoChange}
                onStockPriceChange={this.onStockPriceChange}
                onInsurancePress={this.onInsurancePress}
                onInsuranceSelected={this.onInsuranceSelected}
                onTaxChange={this.onTaxChange}
                addStock={this.addStock}
                onRegistrationCityPress={this.onRegistrationCityPress}
                onRegistrationCitySelected={this.onRegistrationCitySelected}
                onInsuDatePress={this.onInsuDatePress}
                onInsuDateSelected={this.onInsuDateSelected}
                getColorList={this.getColorList}
                edit={edit}
                onDownPaymentChange={this.onDownPaymentChange}
                onEMIChange={this.onEMIChange}
                onNoOfEmisChange={this.onNoOfEmisChange}
                onTenurePress={this.onTenurePress}
                onTenureSelected={this.onTenureSelected}
                onNumberPlateDatePress={this.onNumberPlateDatePress}
                _handleDatePicked={this._handleDatePicked}
                _hideDateTimePicker={this._hideDateTimePicker}
                onOtherColorChange={this.onOtherColorChange}
                onDescriptionChange={this.onDescriptionChange}
                onPlateDateSelected={this.onPlateDateSelected}
                onPlateDatePress={this.onPlateDatePress}
                onTaxDatePress={this.onTaxDatePress}
                onTaxDateSelected={this.onTaxDateSelected}
                onEditorInitialized={this.onEditorInitialized}
                onClassifiedCheckboxClick = {this.onClassifiedCheckboxClick}
                onOddEvenChange = {this.onOddEvenChange}
            />
        )
    }

}
