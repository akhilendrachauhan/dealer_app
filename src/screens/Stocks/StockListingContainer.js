import React, { Component } from 'react';
import { View,StyleSheet, SafeAreaView,TouchableOpacity,Image, Text,BackHandler} from 'react-native'
import StockListingScreen from './StockListingScreen'
import { MenuProvider } from 'react-native-popup-menu';
import { Utility, Constants,AnalyticsConstants } from './../../util'
import {getStockListing,makeStockPremium,removeStock,addRemovedStockToActive,updateCarPrice,addToBumpup} from './../../api/APICalls'
import * as ScreenHoc from './../../hoc/ScreenHOC'
const WithLoading = ScreenHoc.WithLoading(View)
import { ScreenStates, ScreenName } from '../../util/Constants'
import { icon_back_arrow, icon_search,icon_menu,fab_icon } from './../../assets/ImageAssets'
import {Strings,Colors,Dimens} from './../../values/'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper";
import ActionBarSearchComponent from "../../granulars/ActionBarSearchComponent";
import { EventRegister } from '../../util/EventRegister'
let itemToModify,isClassified,isFeatured,callbackMethod,carIdTobeUpdated, toBeAddedToStock
export default class StockListingContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stockTotalCount : 0,
            showSearchBar: false,
            stockList: [],
            refreshing : false,
            screenState: ScreenStates.IS_LOADING,
            activeStockShow : true,
            showMenu : false,
            showLoading : false,
            page:1,
            stockType : props.navigation.state.params && props.navigation.state.params.stockType ? props.navigation.state.params.stockType : Constants.ACTIVE_STOCK,
            activeStockType : props.navigation.state.params && props.navigation.state.params.activeStockType ? props.navigation.state.params.activeStockType : '',
            isPriceChangeModalVisible : false,
            newPrice : undefined,
            showPriceUpdating : false,
            showUrlSelectionModal: false,
            isAddToStockViewShow : false,
            isClassifiedCheckboxChecked : false,
        }
    }

    onBackPress = () => {
        if(this.state.activeStockShow)
            this.props.navigation.goBack();
        else{
            this.setState({showMenu:false,activeStockShow: !this.state.activeStockShow,stockType: Constants.ACTIVE_STOCK,page:1},()=>{
                this.getStockListingData('',false)})
        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        EventRegister.addEventListener(Constants.STOCK_LIST_SYNC, (data) => {
            this.getStockListingData('',true);
        })
        Utility.sendCurrentScreen(AnalyticsConstants.STOCK_LISTING_SCREEN)
        this.getStockListingData('',false);
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(Constants.STOCK_LIST_SYNC)
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.onBackPress()
        return true;
    }
    

    clickOnMenu = ()=> {
        this.setState({showMenu:!this.state.showMenu})
    }

    clickOnMenuOption = () => {
        if(this.state.activeStockShow){
            if(this.state.activeStockType != ''){
                this.setState({showMenu:false,stockType: Constants.ACTIVE_STOCK,activeStockType : '',page:1},()=>{
                    this.getStockListingData('',false)
                })
            }
            else {
                this.setState({showMenu:false,activeStockShow: !this.state.activeStockShow,stockType: Constants.REMOVED_STOCK,page:1},()=>{
                    this.getStockListingData('',false)
                })
            }
            
        }
        else{
            this.setState({showMenu:false,activeStockShow: !this.state.activeStockShow,stockType: Constants.ACTIVE_STOCK,page:1},()=>{
                this.getStockListingData('',false)})
        }
    }

    onSubmitEditing = (text) => {
        this.getStockListingData(text,false)
    }
    onSearchTextChange = (text) => {
        this.getStockListingData(text,false)

    }
    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
        this.getStockListingData('',false)
    }
    onSearch = () => {
        if(this.state.stockTotalCount > 0){
            this.setState({ showSearchBar: true })
            Utility.sendEvent(AnalyticsConstants.STOCK_LIST_SEARCH_CLICK)
        }
        else
            Utility.showToast(Strings.NO_RECORDS_TO_SEARCH)
    };

    onListItemClick = (item) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_LIST_ITEM_CLICK,{car_id : item.id})
        this.props.navigation.navigate("stockDetail",{item: item,backFromDetailPage:this.backFromDetailPage})
    }

    backFromDetailPage = (type,itemToModify,value) => {
        if(type == Constants.REMOVE_STOCK){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify != item.id) {
                    return item; 
                }
            })
            
            this.setState({ stockList: filterData, stockTotalCount : this.state.stockTotalCount - 1})
        }
        else if(type == Constants.ADD_TO_CLASSIFIED){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.isclassified = Constants.STOCK_CLASSIFIED;
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
        else if(type == Constants.REMOVE_FROM_CLASSIFIED){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.isclassified = Constants.STOCK_NOT_CLASSIFIED;
                    item.ispremium = Constants.STOCK_NOT_PREMIUM;
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
        else if(type == Constants.ADD_TO_FEATURED){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.ispremium = Constants.STOCK_PREMIUM;
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
        else if(type == Constants.REMOVE_FROM_FEATURED){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.ispremium = Constants.STOCK_NOT_PREMIUM;
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
        else if(type == Constants.FROM_REMOVED_DETAIL){
            this.setState({page:1,activeStockShow : false, stockType : Constants.REMOVED_STOCK},()=>{
                this.getStockListingData('',false)
            })
        }
        else if(type == Constants.UPDATE_CAR_PRICE){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.car_price = value;
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
        else if(type == Constants.ADDED_TO_BUMPUP){
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify == item.id) {
                    item.is_bump_enable = Constants.STOCK_BUMPUP_ENABLE;
                }
                else{
                    item.is_bump_enable = Constants.STOCK_BUMPUP_NOT_ENABLE; 
                }
                return item; 
            })
            
            this.setState({ stockList: filterData})
        }
    }
    moveToAddStock = () => {
        Utility.sendEvent(AnalyticsConstants.STOCK_ADD_START)
        this.props.navigation.navigate("addEditStock",{backFromAddPage:this.backFromAddPage})
    }

    clickOnEdit = (item) => {
        this.setState({newPrice : item.car_price})
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_START,{car_id : item.id})
        if(item.certification_status == Constants.STOCK_INSPECTED)
        {
            this.onNewPriceModalVisible(true)
        }
        else{
            this.props.navigation.navigate("addEditStock",{car_id:item.id ,backFromAddPage:this.backFromAddPage})
        }
    }
    backFromAddPage = () => {
        this.state.page = 1;
        this.getStockListingData('',false);
    }

    onItemMenuClick = () => {
        // alert('menu')
        this.setState({itemMenuClick: !this.state.itemMenuClick})
    }

    setStockPremium = (item,classified,featured,callback) =>{
        itemToModify = item
        isClassified = classified
        isFeatured = featured
        callbackMethod = callback
        if(featured == 'true' && item.isclassified == Constants.STOCK_NOT_CLASSIFIED){
            Utility.showToast(Strings.ONLY_CLASSIFIED_CARS_CAN_FEATURED)
            //callbackMethod();
        }
        else{
            try{
                this.setState({showLoading: true})
                if(isClassified == 'true'){
                    Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_CLASSIFIED_CLICK,{car_id: itemToModify.id})
                }
                else if(isClassified == 'false'){
                    Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FROM_CLASSIFIED_CLICK,{car_id: itemToModify.id})
                }
                else if(isFeatured == 'true'){
                    Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_FEATURED_CLICK,{car_id: itemToModify.id})
                }
                else if(isFeatured == 'false'){
                    Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_CLICK,{car_id: itemToModify.id})
                } 
                
                makeStockPremium(item.dealer_id,item.id,isClassified,isFeatured,this.makeStockPremiumSuccess,this.makeStockPremiumFailure,this.props)
            }catch (error) {
                Utility.log(error);
            }
        }
    }
    

    makeStockPremiumSuccess = (response) => {
        if(response.status == 200)
        {
            
            if(isClassified == 'true'){
                Utility.showToast(Strings.STOCK_ADDED_TO_CLASSIFIED);
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_CLASSIFIED_SUCCESS,{car_id: itemToModify.id})
            }
            else if(isClassified == 'false'){
                Utility.showToast(Strings.STOCK_REMOVED_FROM_CLASSIFIED);
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLASSIFIED_SUCCESS,{car_id: itemToModify.id})
            }
            else if(isFeatured == 'true'){
                Utility.showToast(Strings.STOCK_ADDED_TO_FEATURED);
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_FEATURED_SUCCESS,{car_id: itemToModify.id})
            }
            else if(isFeatured == 'false'){
                Utility.showToast(Strings.STOCK_REMOVED_FROM_FEATURED); 
                Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_SUCCESS,{car_id: itemToModify.id})
            }
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify.id == item.id) {
                    if(isClassified == 'true'){
                        item.isclassified = Constants.STOCK_CLASSIFIED;
                    }
                    else if(isClassified == 'false'){
                        item.isclassified = Constants.STOCK_NOT_CLASSIFIED;
                        item.ispremium = Constants.STOCK_NOT_PREMIUM; 
                        //item.is_bump_enable = 0;
                    }
                    else if(isFeatured == 'true')
                        item.ispremium = Constants.STOCK_PREMIUM;
                    else if(isFeatured == 'false')
                        item.ispremium = Constants.STOCK_NOT_PREMIUM;    
                }
               
                return item;
            })
            //alert(JSON.stringify(filterData))
            this.setState({ stockList: filterData,showLoading: false },()=>{
                //callbackMethod();
            })
        }
        else{
            this.setState({showLoading: false})
            Utility.showToast(response.message);
        }
    }

    makeStockPremiumFailure = (response) => {
        this.setState({showLoading: false})
        if(isClassified == 'true'){
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_CLASSIFIED_FAILURE,{car_id: itemToModify.id})
        }
        else if(isClassified == 'false'){
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLASSIFIED_FAILURE,{car_id: itemToModify.id})
        }
        else if(isFeatured == 'true'){
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_FEATURED_FAILURE,{car_id: itemToModify.id})
        }
        else if(isFeatured == 'false'){
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FEATURED_FAILURE,{car_id: itemToModify.id})
        } 
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }


    getStockListingData = (text,fromRefresh) => {
        if(fromRefresh || text != ''){
            this.state.page = 1;
        }
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getStockListing(dealer_id, text,this.state.stockType,this.state.page,this.state.activeStockType, [], this.onSuccessStockListing, this.onFailureStockListing,this.props);
            } catch (error) {
                Utility.log(error);
            }
            if(fromRefresh){
                this.setState({
                    refreshing : true,
                })
            }
            else{
                this.setState({
                    screenState: ScreenStates.IS_LOADING,
                })
            }
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
        
    }
    getStockListingMoreData = () => {
        Utility.getValueFromAsyncStorage(Constants.DEALER_ID).then((dealer_id) => {
            try {
                getStockListing(dealer_id,'',this.state.stockType,this.state.page,this.state.activeStockType, [], this.onSuccessStockListingMore, this.onFailureStockListingMore, this.props);
            } catch (error) {
                Utility.log(error);
            }
           
            this.setState({
                showLoading : true,
            })
           
        }).catch((error)=>{
            Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        });
        
    }
    handleLoadMore = () => {
        if(this.state.stockTotalCount > this.state.stockList.length){
            this.setState({
                page: this.state.page + 1,
                
            }, () => {
                this.getStockListingMoreData();
            });
        }
    }

    onSuccessStockListing = (response) => {
        if(response && response.data && response.data.length > 0  )
            this.setState({screenState: ScreenStates.NO_ERROR,
                refreshing: false,showLoading : false,
                stockTotalCount : response.pagination.total,
                stockList : this.state.page == 1 ? response.data : [...this.state.stockList, ...response.data]
                })
        else
            this.setState({stockTotalCount : 0,stockList : [], screenState: ScreenStates.NO_DATA_FOUND,refreshing: false,showLoading : false,})

    }
    onFailureStockListing = (response) => {
        this.setState({screenState: ScreenStates.NO_DATA_FOUND,refreshing: false,showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        
    };

    onSuccessStockListingMore = (response) => {
        if(response && response.data && response.data.length > 0  )
            this.setState({showLoading : false,
                stockTotalCount : response.pagination.total,
                stockList : this.state.page == 1 ? response.data : [...this.state.stockList, ...response.data]
                })
        else
            this.setState({showLoading : false,})

    }
    onFailureStockListingMore = (response) => {
        this.setState({showLoading : false,})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
        
    };

    onCarSoldSubmit = (dealer_id,id,reason_id,sold_rice,callback)=>{
        callbackMethod = callback
        itemToModify = id
        try{
            this.setState({showLoading: true})
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_CLICK,{car_id : itemToModify})
            removeStock(dealer_id,id,reason_id,sold_rice,this.removeStockSuccess,this.removeStockFailure,this.props)
        }catch (error) {
            Utility.log(error);
        }
    }

    removeStockSuccess = (response) => {
        if(response.status == 200)
        {
            Utility.showToast(Strings.STOCK_REMOVED_MSG);
            Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_SUCCESS,{car_id : itemToModify})
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify != item.id) {
                    return item; 
                }
            })
            //alert(JSON.stringify(filterData))
            this.setState({ stockList: filterData,stockTotalCount: this.state.stockTotalCount - 1, showLoading: false },()=>{
                callbackMethod();
            })
        }
        else{
            this.setState({showLoading: false})
            Utility.showToast(response.message);
        }
    }

    removeStockFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_REMOVE_FAILURE,{car_id : itemToModify})
        this.setState({showLoading: false})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }
    
    onRetryClick = () => {
        this.getStockListingData('',false)
    }

    clickOnAddToStock =(item) => {
        toBeAddedToStock = item
        if(Constants.APP_TYPE == Constants.INDONESIA){
            this.setState({isAddToStockViewShow : true})
        }
        else{
            try{
                this.setState({showLoading: true})
                Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_CLICK,{car_id : item.id})
                addRemovedStockToActive(item.id,Constants.ACTIVE_STOCK,this.addToActiveStockSuccess,this.addToActiveFailure,this.props)
            }catch (error) {
                Utility.log(error);
            }
        }
    }

    addToActiveStockSuccess = (response) => {
        if(response.status == 200)
        {
            Utility.showToast(response.message);
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_SUCCESS)
            this.setState({showLoading: false, isClassifiedCheckboxChecked:false, page:1,activeStockShow : false, stockType : Constants.REMOVED_STOCK},()=>{
                this.getStockListingData('',false)
            })
            
        }
        else{
            this.setState({showLoading: false})
            Utility.showToast(response.message);
        }
    }

    addToActiveFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_FAILURE)
        this.setState({showLoading: false,isClassifiedCheckboxChecked : false})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }

    clickOutSide = () => {
        this.setState({showMenu : false})
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_STOCK_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    };

    renderTitle = () => {
        if(this.state.activeStockShow ){
            if(this.state.stockTotalCount > 0){
                if(this.state.activeStockType == Constants.NON_CLASSIFIED_STOCK)
                    return Strings.NON_CLASSIFIED + " (" + this.state.stockTotalCount + ")"
                else if(this.state.activeStockType == Constants.WITHOUT_PHOTOS_STOCK)
                    return Strings.WITHOUT_PHOTOS + " (" + this.state.stockTotalCount + ")"
                else if(this.state.activeStockType == Constants.AGE_45D_STOCK)
                    return Strings.OLD_STOCK + " (" + this.state.stockTotalCount + ")"
                else 
                    return Strings.ACTIVE_STOCK + " (" + this.state.stockTotalCount + ")"
            }
            else{
                return Strings.ACTIVE_STOCK
            }
        }
        else{
            if(this.state.stockTotalCount > 0){
                return Strings.REMOVED_STOCK + " (" + this.state.stockTotalCount + ")"
            }
            else{
                return Strings.REMOVED_STOCK
            }
        }
    }

    onNewPriceChange = (text) => {
        //this.refs['addEditStockScreen'].TL_STOCK_PRICE.setError("")
        this.setState({newPrice : Utility.onlyNumeric(text)})
    }

    onUpdatePriceSubmit = (car_id) => {
        carIdTobeUpdated = car_id
        if(this.state.newPrice && this.state.newPrice != "" && parseInt(this.state.newPrice) != 0 )
        {
            Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_START,{car_id : car_id})
            try{
                this.setState({showPriceUpdating: true})
                updateCarPrice(car_id,this.state.newPrice,this.updatePriceSuccess,this.updatePriceFailure,this.props)
            }catch (error) {
                Utility.log(error);
            }
        }
        else{
            Utility.showToast(Strings.ENTER_STOCK_PRICE)
        }
        
    }

    updatePriceSuccess = (response) => {
        //this.setState({showPriceUpdating: false, isPriceChangeModalVisible : false})
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_SUCCESS,{car_id : carIdTobeUpdated})
        Utility.showToast(response.message);
        let filterData = this.state.stockList.filter((item) => {
            if(carIdTobeUpdated == item.id) {
                item.car_price = this.state.newPrice;
            }
            return item; 
        })
        
        this.setState({ stockList: filterData,showPriceUpdating: false, isPriceChangeModalVisible : false})
    }

    updatePriceFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_EDIT_FAILURE,{car_id : carIdTobeUpdated})
        this.setState({showPriceUpdating: false})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }

    onNewPriceModalVisible = (value) =>{
        this.setState({ isPriceChangeModalVisible: value })
    }

    clickOnSeeOnWeb = (urls) => {
        if(urls){
            Utility.sendEvent(AnalyticsConstants.STOCK_WEB_URL_CLICK)
            if(urls instanceof Array){
                if(urls.length > 1){
                    this.setState({showUrlSelectionModal: true})
                }else {
                    Utility.openExternalUrl(urls[0].url)
                }
            }else {
                Utility.openExternalUrl(urls)
            }
        }
    };

    onWebUrlItemPress = (success, url) => {
        this.setState({showUrlSelectionModal: false});
        if(success){
            Utility.openExternalUrl(url)
        }
    };

    addToBumpup = (item) =>{
        itemToModify = item
        if(item.isclassified == Constants.STOCK_CLASSIFIED){
            try{
                this.setState({showLoading: true})
                Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_CLICK,{car_id : item.id})
                addToBumpup(item.id,this.onBumpUpSuccess,this.onBumpUpFailure,this.props)
            }catch (error) {
                Utility.log(error);
            }
        }
        else{
            Utility.showToast(Strings.ONLY_CLASSIFIED_CARS_CAN_BUMPUP)
        }
    }

    onBumpUpSuccess = (response) => {
        if(response.status == 200)
        {
            Utility.showToast(response.message);
            Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_SUCCESS,{car_id : itemToModify.id})
            let filterData = this.state.stockList.filter((item) => {
                if(itemToModify.id == item.id) {
                    item.is_bump_enable = Constants.STOCK_BUMPUP_ENABLE;    
                }
                else{
                    item.is_bump_enable = Constants.STOCK_BUMPUP_NOT_ENABLE;   
                }
               
                return item;
            })
            this.setState({ stockList: filterData,showLoading: false },()=>{
                //callbackMethod();
            })
        }
        else{
            this.setState({showLoading: false})
            Utility.showToast(response.message);
        }
    }

    onBumpUpFailure = (response) => {
        Utility.sendEvent(AnalyticsConstants.STOCK_BUMP_UP_FAILURE,{car_id : itemToModify.id})
        this.setState({showLoading: false})
        if(response && response.message){
            Utility.showToast(response.message);
        }else{
            Utility.showToast(Strings.SERVER_API_FAILURE_MSG);
        }
    }

    onClassifiedCheckboxClick = () =>{
        this.setState({isClassifiedCheckboxChecked : !this.state.isClassifiedCheckboxChecked})
    }

    hideDialog = () => {
        this.setState({
            isAddToStockViewShow : false
        })
    };

    okPress = ()=>{
        try{
            this.setState({showLoading: true,isAddToStockViewShow : false})
            Utility.sendEvent(AnalyticsConstants.STOCK_ADD_TO_ACTIVE_CLICK,{car_id : toBeAddedToStock.id})
            addRemovedStockToActive(toBeAddedToStock.id,Constants.ACTIVE_STOCK,this.addToActiveStockSuccess,this.addToActiveFailure,this.props,this.state.isClassifiedCheckboxChecked)
        }catch (error) {
            Utility.log(error);
        }
    }


    render() {
        this.actionBarProps = {
            values: {
                title: this.renderTitle()
            },
            rightIcons: [
                {
                    image: icon_search,
                    onPress: this.onSearch
                },
                {
                    image: icon_menu,
                    onPress: this.clickOnMenu
                },

            ],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };
        return (
            
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK  }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={this.actionBarProps.values}
                        actions={this.actionBarProps.actions}
                        iconMap={this.actionBarProps.rightIcons}
                        styleAttributes={this.actionBarProps.styleAttr}
                    />
                    {this.state.showSearchBar ? this.openActionSearchBar() : null}
                    <MenuProvider>
                    {this.state.showMenu && <View style={{ borderRadius: 2, backgroundColor: Colors.GRAY_LIGHT_BG, position: 'absolute', top: 0, right: 10, zIndex: 9999, paddingVertical: 10, paddingHorizontal: 20 }}>
                        <TouchableOpacity
                            onPress={() => this.clickOnMenuOption()}
                            activeOpacity={0.8}
                        >
                            <Text style={styles.menuOptionText}>{this.state.activeStockShow ? this.state.activeStockType != '' ? Strings.ALL_STOCK : Strings.REMOVED_STOCK : Strings.ACTIVE_STOCK}</Text>
                        </TouchableOpacity>
                    </View>}
                    <WithLoading
                            screenState={this.state.screenState}
                            screenName={ScreenName.SCREEN_LEAD}
                            onRetry={this.onRetryClick}>  
                        
                        <StockListingScreen
                            ref={'stockListingScreen'}
                            onBackPress={this.onBackPress}
                            onSubmitEditing={this.onSubmitEditing}
                            onSearchBackPress={this.onSearchBackPress}
                            onSearchTextChange={this.onSearchTextChange}
                            onSearch={this.onSearch}
                            showSearchBar={this.state.showSearchBar}
                            stockList={this.state.stockList}
                            onListItemClick={this.onListItemClick}
                            moveToAddStock={this.moveToAddStock}
                            onItemMenuClick={this.onItemMenuClick}
                            getStockListingData = {this.getStockListingData}
                            refreshing = {this.state.refreshing}
                            clickOnMenu = {this.clickOnMenu}
                            clickOnMenuOption = {this.clickOnMenuOption}
                            showMenu = {this.state.showMenu}
                            activeStockShow = {this.state.activeStockShow}
                            setStockPremium = {this.setStockPremium}
                            onCarSoldSubmit = {this.onCarSoldSubmit}
                            showLoading = {this.state.showLoading}
                            clickOnEdit = {this.clickOnEdit}
                            handleLoadMore = {this.handleLoadMore}
                            clickOnAddToStock = {this.clickOnAddToStock}
                            activeStockType = {this.state.activeStockType}
                            clickOutSide = {this.clickOutSide}
                            isPriceChangeModalVisible = {this.state.isPriceChangeModalVisible}
                            onNewPriceChange = {this.onNewPriceChange}
                            onUpdatePriceSubmit = {this.onUpdatePriceSubmit}
                            onNewPriceModalVisible = {this.onNewPriceModalVisible}
                            newPrice = {this.state.newPrice}
                            showPriceUpdating = {this.state.showPriceUpdating}
                            clickOnSeeOnWeb = {this.clickOnSeeOnWeb}
                            showUrlSelectionModal = {this.state.showUrlSelectionModal}
                            onWebUrlItemPress={this.onWebUrlItemPress}
                            addToBumpup = {this.addToBumpup}
                            isAddToStockViewShow = {this.state.isAddToStockViewShow}
                            onClassifiedCheckboxClick = {this.onClassifiedCheckboxClick}
                            isClassifiedCheckboxChecked = {this.state.isClassifiedCheckboxChecked}
                            hideDialog = {this.hideDialog}
                            okPress = {this.okPress}
                        />
                           
                    </WithLoading>
                    </MenuProvider> 
                    { this.state.stockType == Constants.ACTIVE_STOCK && <View style={{ position: 'absolute', bottom: 10, right: 10, width: 58, height: 58, borderRadius: 29 }}>
                        <TouchableOpacity
                            onPress={() => this.moveToAddStock()}
                            activeOpacity={0.8}
                        >
                        <Image source={fab_icon} style={{ width: '100%', height: '100%' }} />
                        </TouchableOpacity>
                    </View>}
                </View>
                </SafeAreaView>    
            
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    menuOptionText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
})