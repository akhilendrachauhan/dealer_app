import React, { Component } from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity,SafeAreaView,TextInput,Platform,KeyboardAvoidingView} from 'react-native';
import {Colors, Strings, Dimens} from "../../values";
import { icon_cancel } from './../../assets/ImageAssets'
import TextInputLayout from "../../granulars/TextInputLayout"
import { Utility, Constants } from '../../util';
export default class CarSoldModalView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            price : ''
        }
    }

    render() {
        return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.BLACK_50,marginBottom : Platform.OS == 'ios' ? this.props.height : 0}}>
            <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
               <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: 1 }}
                        onPress={() => {
                            this.props.setModalVisible(false)
                        }}>
                        <Image source={icon_cancel}
                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                    </TouchableOpacity>

                    <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large,marginBottom: Dimens.margin_normal }}>{Strings.CONGRATULATIONS_ON_YOUR_SALE}</Text>
                    <TextInputLayout style={{ height: 50 }}
                        hintColor={Colors.BLACK_85}
                        focusColor={Colors.ORANGE}>
                        <TextInput style={styles.textInput}
                            selectTextOnFocus={false}
                            returnKeyType='go'
                            keyboardType='number-pad'
                            //maxLength={11}
                            value={ this.state.price !='' ? Utility.formatCurrency(this.state.price) : this.state.price}
                            placeholder={Strings.ENTER_SOLD_PRICE_OF_CAR}
                            //ref={(input) => this.OtpInput = input}
                            //onSubmitEditing={() => this.props.onProceedForOtp(this.state.otp)}
                            onChangeText={(text) => { 
                                this.setState({ price: Utility.onlyNumeric(text) })
                                
                            }} />
                    </TextInputLayout>
                    <Text style={{ color: Colors.BLACK, fontSize: Dimens.text_normal,marginTop: Dimens.margin_normal }}>{Strings.DISCLAIMER_SELLING_PRICE}</Text>
                    
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {
                            if(this.state.price == '' || this.state.price == '0')
                                Utility.showToast(Strings.ENTER_SOLD_PRICE_OF_CAR)
                            else
                            this.props.onCarSoldSubmit(Constants.CAR_IS_BOOKED,this.state.price)
                        }
                        }>

                        <View style={styles.buttonStyle}>
                            <Text style={styles.buttonTextStyle}>{Strings.OK}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}
}

const styles = StyleSheet.create({
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
});

