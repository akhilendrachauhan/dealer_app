import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Image, Modal, TouchableWithoutFeedback, TextInput, TouchableOpacity, SafeAreaView, Keyboard, KeyboardAvoidingView } from 'react-native'
import { fab_icon, icon_cancel, icon_arrow_right, icon_call, icon_car, icon_sold_stock, icon_time, icon_featured,icon_checked,icon_unchecked } from './../../assets/ImageAssets'
import { Strings, Colors, Dimens } from './../../values'
import { _ActionBarStyle } from '../../values/Styles'
import LoadingComponent from './../../granulars/LoadingComponent'
import StockListingCard from './../../granulars/StockListingCard'
import { Utility, Constants } from '../../util'
import ShareByEmailModalView from './ShareByEmailModalView'
import ShareBySMSModalView from './ShareBySMSModalView'
import ShareByWhatsAppOptionModalView from './ShareByWhatsAppOptionModalView'
import RemoveStockModalView from './RemoveStockModalView'
import CarSoldModalView from './CarSoldModalView'
import RemoveRenewStockModalView from './RemoveRenewStockModalView'
import TextInputLayout from "./../../granulars/TextInputLayout"
import StockWebUrlsModal from "./StockWebUrlsModal";

let selectedItem;
let self;
export default class StockListingScreen extends Component {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            isMoreOptionModalVisible: false,
            showMenu: false,
            activeStockShow: true,
            isShareByEmailModalVisible: false,
            emailIds: '',
            mobileNumber: '',
            isShareBySMSModalVisible: false,
            isShareByWhatsAppOptionModalVisible: false,
            isRemoveStockModalVisible: false,
            isCarSoldModalVisible: false,
            isRemoveRenewStockModalVisible: false,
            keyboardHeight: 0,
            inputHeight: 70,
            isShowToolTip: true,
            isFirstToolTip: true,
            isSecondToolTip: false,
            isThirdToolTip: false,
            isForthToolTip: false
        }

    }

    componentDidMount() {
        selectedItem = this.props.stockList[0]
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }


    _keyboardDidShow(e) {
        //alert(e.endCoordinates.height)
        this.setState({ keyboardHeight: e.endCoordinates.height });
    }

    _keyboardDidHide(e) {
        this.setState({ keyboardHeight: 0 });
    }

    clickOnMore = (item) => {
        //alert(JSON.stringify(item))
        selectedItem = item
        this.setState({ isMoreOptionModalVisible: true })
    }
    clickOnEdit = (item) => {
        selectedItem = item
        this.props.clickOnEdit(item)
    }
    clickOnPostFacebook = () => {

    }
    clickOnRemoveStock = (item) => {
        selectedItem = item
        this.setState({ isRemoveStockModalVisible: true })
    }
    clickOnWhatsapp = () => {
        this.setState({ isShareByWhatsAppOptionModalVisible: true })
    }

    addToClassifiedSuccessCallback = () => {
        this.setState({ isMoreOptionModalVisible: false })
    }

    clickOnCarBooked = () => {
        this.setState({ isRemoveStockModalVisible: false, isCarSoldModalVisible: true })
    }



    onEmailShareSubmit = () => {
        if (this.state.emailIds == '' || this.state.emailIds == null)
            Utility.showToast(Strings.PLEASE_ENTER_AT_LEAST_ONE_EMAIL)
        else
            alert("ok")
    }

    setEmailShareModalVisible = () => {
        this.setState({ isShareByEmailModalVisible: false })
    }

    onEmailChange = (text) => {
        this.setState({ emailIds: text })
    }

    onSMSShareSubmit = () => {
        if (Utility.isValueNullOrEmpty(this.state.mobileNumber) || !Utility.validateMobileNo(this.state.mobileNumber))
            alert(Strings.INVALID_MOBILE_NUMBER)
    }
    setSMSShareModalVisible = () => {
        this.setState({ isShareBySMSModalVisible: false })
    }

    onMobileChange = (text) => {
        this.setState({ mobileNumber: text })
    }

    setWhatsAppOptionModalVisible = () => {
        this.setState({ isShareByWhatsAppOptionModalVisible: false })
    }
    setRemoveStockOptionModalVisible = () => {
        this.setState({ isRemoveStockModalVisible: false })
    }

    shareCarPhotos = () => {
        alert()
    }

    sendCarDetails = () => {
        alert()
    }
    onCarSoldSubmit = (reason_id, sold_price) => {
        this.props.onCarSoldSubmit(selectedItem.dealer_id, selectedItem.id, reason_id, sold_price, this.setCarSoldModalVisible);
    }

    clickOnRenew = () => {
        this.setState({ isRemoveStockModalVisible: false, isRemoveRenewStockModalVisible: true })
    }

    setCarSoldModalVisible = () => {
        this.setState({ isRemoveStockModalVisible: false, isCarSoldModalVisible: false, isRemoveRenewStockModalVisible: false })
    }

    setRemoveRenewStockModalVisible = () => {
        this.setState({ isRemoveRenewStockModalVisible: false })
    }

    clickOnSeeOnWeb = (item) =>{
        selectedItem = item
        this.props.clickOnSeeOnWeb(item.web_url)
    }



    renderItem = ({ item }) => {
        return (
            <StockListingCard
                item={item}
                onItemMenuClick={this.props.onItemMenuClick}
                onListItemClick={this.props.onListItemClick}
                clickOnMore={this.clickOnMore}
                clickOnEdit={this.clickOnEdit}
                clickOnPostFacebook={this.clickOnPostFacebook}
                clickOnRemoveStock={this.clickOnRemoveStock}
                clickOnWhatsapp={this.clickOnWhatsapp}
                activeStockShow={this.props.activeStockShow}
                clickOnAddToStock={this.props.clickOnAddToStock}
                clickOnSeeOnWeb = {this.clickOnSeeOnWeb}
                setStockPremium = {this.props.setStockPremium}
                addToBumpup = {this.props.addToBumpup}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>

                {this.state.isShowToolTip && this.props.activeStockType != '' && this.props.stockList.length > 0 ?
                    this.getToolTipView()
                    :
                    null
                }

                {this.props.stockList.length > 0 &&
                    <FlatList
                        style={{ margin: Dimens.margin_small, marginBottom: 0, flex: 1 }}
                        data={this.props.stockList}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item.id}
                        extraData={this.state}
                        onRefresh={() => this.props.getStockListingData('', true)}
                        refreshing={this.props.refreshing}
                        onEndReached={() => this.props.handleLoadMore()}
                        onEndReachedThreshold={0.9}
                    />
                }
                {this.props.showMenu && <TouchableWithoutFeedback onPress={() => this.props.clickOutSide()}>
                    <View style={{ width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, bottom: 0, right: 0 }} />
                </TouchableWithoutFeedback>
                }
                {this.props.showLoading && <LoadingComponent />}
                {/* <View style={{ position: 'absolute', bottom: 10, right: 10, width: 58, height: 58, borderRadius: 29 }}>
                        <TouchableOpacity
                            onPress={() => this.props.moveToAddStock()}

                            activeOpacity={0.8}
                        >

                            <Image source={fab_icon} style={{ width: '100%', height: '100%' }} />

                        </TouchableOpacity>
                    </View> */}
                {/* Menu View  */}
                {/* {this.props.showMenu && <View style={{ borderRadius: 2, backgroundColor: Colors.GRAY_LIGHT_BG, position: 'absolute', top: 0, right: 10, zIndex: 9999, paddingVertical: 10, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => this.props.clickOnMenuOption()}
                        activeOpacity={0.8}
                    >
                        <Text style={styles.menuOptionText}>{this.props.activeStockShow ? this.props.activeStockType != '' ? Strings.ALL_STOCK : Strings.REMOVED_STOCK : Strings.ACTIVE_STOCK}</Text>
                    </TouchableOpacity>
                </View>} */}
                {/* Menu View  */}


                {/* start of More  options modal*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isMoreOptionModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isMoreOptionModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isMoreOptionModalVisible: false })
                    }}>

                    <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                        <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                            <TouchableOpacity
                                style={{ alignSelf: 'flex-end', padding: 1 }}
                                onPress={() => {
                                    this.setState({ isMoreOptionModalVisible: false })
                                }}>
                                <Image source={icon_cancel}
                                    style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                            </TouchableOpacity>

                            <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large }}>{selectedItem ? (selectedItem.make + ' ' + selectedItem.modelVersion) : ''}</Text>
                            <Text style={{ color: Colors.BLACK_54, fontSize: Dimens.text_small, marginTop: 5 }}>{selectedItem ? selectedItem.reg_no : ''}</Text>

                            <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                onPress={() => {
                                    if (selectedItem && selectedItem.isclassified == Constants.STOCK_CLASSIFIED)
                                        this.props.setStockPremium(selectedItem, 'false', '', this.addToClassifiedSuccessCallback);
                                    else
                                        this.props.setStockPremium(selectedItem, 'true', '', this.addToClassifiedSuccessCallback);
                                }}>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={styles.modalText}>{selectedItem ? (selectedItem.isclassified == Constants.STOCK_CLASSIFIED ? Strings.REMOVE_FROM_CLASSIFIED : Strings.ADD_TO_CLASSIFIED) : ''}</Text>
                                    <Image source={icon_arrow_right}
                                        style={styles.modalImage} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.seperator} />
                            <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                onPress={() => {
                                    //alert(JSON.stringify(selectedItem))
                                    if (selectedItem && selectedItem.ispremium == Constants.STOCK_PREMIUM)
                                        this.props.setStockPremium(selectedItem, '', 'false', this.addToClassifiedSuccessCallback);
                                    else
                                        this.props.setStockPremium(selectedItem, '', 'true', this.addToClassifiedSuccessCallback);
                                }}>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={styles.modalText}>{selectedItem ? (selectedItem.ispremium == Constants.STOCK_PREMIUM ? Strings.REMOVE_FEATURED : Strings.ADD_TO_FEATURED) : ''}</Text>
                                    <Image source={icon_arrow_right}
                                        style={styles.modalImage} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.seperator} />
                            {/* <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                onPress={() => {
                                    this.setState({isMoreOptionModalVisible : false,isShareBySMSModalVisible:true})
                                }}>

                                <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                                    <Text style={styles.modalText}>{Strings.SHARE_BY_SMS}</Text>
                                    <Image source={icon_arrow_right}
                                        style={styles.modalImage} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.seperator}/>
                            <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                                onPress={() => {
                                    this.setState({isMoreOptionModalVisible : false,isShareByEmailModalVisible:true})
                                }}>

                                <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                                    <Text style={styles.modalText}>{Strings.SHARE_BY_EMAIL}</Text>
                                    <Image source={icon_arrow_right}
                                        style={styles.modalImage} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.seperator}/> */}


                        </View>
                    </View>
                </Modal>
                {/* end of More  options modal*/}

                {/* start of share by emails modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isShareByEmailModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareByEmailModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareByEmailModalVisible: false })
                    }}>

                    <ShareByEmailModalView
                        setModalVisible={this.setEmailShareModalVisible}
                        onEmailShareSubmit={this.onEmailShareSubmit}
                        onEmailChange={this.onEmailChange}
                    />
                </Modal>
                {/* end of share by emails modal */}

                {/* start of share by SMS modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isShareBySMSModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareBySMSModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareBySMSModalVisible: false })
                    }}>
                    <ShareBySMSModalView
                        setModalVisible={this.setSMSShareModalVisible}
                        onEmailShareSubmit={this.onSMSShareSubmit}
                        onEmailChange={this.onMobileChange}
                    />

                </Modal>
                {/* end of share by SMS modal */}

                {/* start of share by WhatsApp options modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isShareByWhatsAppOptionModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareByWhatsAppOptionModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isShareByWhatsAppOptionModalVisible: false })
                    }}>
                    <ShareByWhatsAppOptionModalView
                        setModalVisible={this.setWhatsAppOptionModalVisible}
                        sendCarDetails={this.sendCarDetails}
                        shareCarPhotos={this.shareCarPhotos}
                    />

                </Modal>
                {/* end of share by WhatsApp options modal */}

                {/* start of remove stock options modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isRemoveStockModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isRemoveStockModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isRemoveStockModalVisible: false })
                    }}>
                    <RemoveStockModalView
                        setModalVisible={this.setRemoveStockOptionModalVisible}
                        clickOnCarBooked={this.clickOnCarBooked}
                        onCarSoldSubmit={this.onCarSoldSubmit}
                        clickOnRenew={this.clickOnRenew}

                    />

                </Modal>
                {/* end of remove stock options modal */}
                {/* start of car sold modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isCarSoldModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isCarSoldModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isCarSoldModalVisible: false })
                    }}>
                    <CarSoldModalView
                        setModalVisible={this.setCarSoldModalVisible}
                        onCarSoldSubmit={this.onCarSoldSubmit}
                        height={this.state.keyboardHeight}
                    />

                </Modal>
                {/* end of car sold modal */}
                {/* start of car sold modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isRemoveRenewStockModalVisible}
                    onRequestClose={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isRemoveRenewStockModalVisible: false })
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isRemoveRenewStockModalVisible: false })
                    }}>
                    <RemoveRenewStockModalView
                        setModalVisible={this.setRemoveRenewStockModalVisible}
                        onCarSoldSubmit={this.onCarSoldSubmit}
                    />

                </Modal>
                {/* end of car sold modal */}
                {/* start of update car price modal */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.isPriceChangeModalVisible}
                    onRequestClose={() => {
                        this.props.onNewPriceModalVisible(false)
                    }}
                    onDismiss={() => {
                        // Alert.alert('Modal has been closed.');
                        this.setState({ isPriceChangeModalVisible: false })
                    }}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK_50, marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight : 0 }}>
                        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                            <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                                <TouchableOpacity
                                    style={{ alignSelf: 'flex-end', padding: 1 }}
                                    onPress={() => {
                                        this.props.onNewPriceModalVisible(false)
                                    }}>
                                    <Image source={icon_cancel}
                                        style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                                </TouchableOpacity>

                                <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large, marginBottom: Dimens.margin_normal }}>{Strings.ONLY_INSPECTED_PRICE_CHANGE}</Text>
                                <Text style={{ color: Colors.BLACK_54, fontSize: Dimens.text_small, marginTop: 0 }}>{selectedItem ? selectedItem.modelVersion + "(" + selectedItem.reg_no + ")" : ''}</Text>

                                <TextInputLayout style={{ height: 50, marginTop: 10 }}
                                    ref={(input) => this.TL_NEW_PRICE = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}
                                    errorColorMargin={Dimens.margin_0}>
                                    <TextInput style={styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='go'
                                        keyboardType='numeric'
                                       // maxLength={11}
                                        value={this.props.newPrice ? Utility.formatCurrency(this.props.newPrice) : ''}
                                        placeholder={Strings.ENTER_NEW_PRICE}
                                        ref={(input) => this.newPriceInput = input}
                                        onChangeText={(text) => this.props.onNewPriceChange(text)} />
                                </TextInputLayout>
                                {!this.props.showPriceUpdating ? <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onUpdatePriceSubmit(selectedItem.id)}>

                                    <View style={styles.buttonStyle}>
                                        <Text style={styles.buttonTextStyle}>{Strings.UPDATE}</Text>
                                    </View>
                                </TouchableOpacity> :
                                    <View style={styles.buttonStyle}>
                                        <LoadingComponent backgroundColor={Colors.TRANSPARENT} />
                                    </View>
                                }
                            </View>
                        </KeyboardAvoidingView>
                    </SafeAreaView>

                </Modal>
                {/* end of update car price modal */}

                {/* start of share by SMS modal */}
                {selectedItem ? <StockWebUrlsModal visible={this.props.showUrlSelectionModal} onUrlPress={self.props.onWebUrlItemPress} data={selectedItem}/> : null}
                { this.props.isAddToStockViewShow && this.renderAddToStockView()}
            </View>
        )
    }

    getToolTipView = () => {
        return (
            <View style={{ borderRadius: 5, margin: Dimens.margin_xx_large, padding: Dimens.padding_large, backgroundColor: Colors.TOOLTIP_COLOR }}>

                {this.props.activeStockType == Constants.NON_CLASSIFIED_STOCK ?
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.tooltipText}>{Strings.CONTACT_ASSIGNED_BM}
                            <Text style={{ fontWeight: 'bold' }}>{Strings.BUY_LISTING}</Text></Text>

                        <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                            <Image source={icon_call}
                                style={{ height: 40, width: 40, resizeMode: 'cover', tintColor: Colors.GREEN_CALL }} />
                        </View>
                    </View>
                    :
                    this.props.activeStockType == Constants.WITHOUT_PHOTOS_STOCK ?
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.tooltipText}>{Strings.CAR_WITH_PHOTOS}
                                <Text style={{ fontWeight: 'bold' }}>{Strings.MORE_RESPONSE}</Text></Text>

                            <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                                <Image source={icon_car}
                                    style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                            </View>
                        </View>
                        :
                        (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isFirstToolTip) ?
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={styles.tooltipText}><Text style={{ fontWeight: 'bold' }}>{Strings.REMOVE_SOLD_STOCK}</Text>
                                    {Strings.MAKE_NEW_CAR_SPACE}</Text>

                                <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                                    <Image source={icon_sold_stock}
                                        style={{ height: 40, width: 40, resizeMode: 'cover' }} />
                                </View>
                            </View>
                            :
                            (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isSecondToolTip) ?
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.tooltipText}><Text style={{ fontWeight: 'bold' }}>{Strings.RIGHT_PRICE}</Text>
                                        {Strings.ATTRACTS_BUYER}</Text>

                                    <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                                        <Text style={{ fontSize: Dimens.text_xxx_large, color: Colors.TOOLTIP_TEXT_COLOR, fontWeight: 'bold' }}>{Strings.RUPEE_SYMBOL}</Text>
                                    </View>
                                </View>
                                :
                                (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isThirdToolTip) ?
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.tooltipText}><Text style={{ fontWeight: 'bold' }}>{Strings.TIMELY_FOLLOW}</Text>
                                            {Strings.BUYER_IS_A_KEY_TO_SELL}</Text>

                                        <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                                            <Image source={icon_time}
                                                style={{ height: 40, width: 40, resizeMode: 'cover', tintColor: Colors.TOOLTIP_TEXT_COLOR }} />
                                        </View>
                                    </View>
                                    :
                                    (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isForthToolTip) ?
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={styles.tooltipText}>{'Try our '}<Text style={{ fontWeight: 'bold' }}>{Strings.FEATURED_LISTING}</Text>
                                                {Strings.CAR_SELL_PACK}</Text>

                                            <View style={{ height: 46, width: 46, borderRadius: 23, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.TOOLTIP_COLOR }}>
                                                <Image source={icon_featured}
                                                    style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                                            </View>
                                        </View>
                                        :
                                        null
                }

                <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ marginTop: Dimens.margin_normal }}
                    onPress={() => {
                        if (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isFirstToolTip) {
                            this.setState({ isFirstToolTip: false, isSecondToolTip: true })
                        }
                        else if (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isSecondToolTip) {
                            this.setState({ isSecondToolTip: false, isThirdToolTip: true })
                        }
                        else if (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isThirdToolTip) {
                            this.setState({ isThirdToolTip: false, isForthToolTip: true })
                        }
                        else if (this.props.activeStockType == Constants.AGE_45D_STOCK && this.state.isForthToolTip) {
                            this.setState({ isForthToolTip: false, isShowToolTip: false })
                        }
                        else {
                            this.setState({ isShowToolTip: false })
                        }
                    }}>

                    <Text style={styles.gotItText}>{Strings.GOT_IT}</Text>
                </TouchableOpacity>
                <View style={styles.triangle} />
            </View>
        )
    }

    renderAddToStockView = () => {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.hideDialog} style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                justifyContent: 'center',
                backgroundColor: Colors.BLACK_54,
                paddingVertical: 50,
                paddingHorizontal: 20
            }}>
                <TouchableOpacity activeOpacity={1} onPress={() => Utility.log('')}
                            style={{ backgroundColor: Colors.WHITE, padding: Dimens.padding_xx_large,paddingTop:0, borderRadius: 5 }}>
                    <Text style={[styles.modalText, {marginTop: 20,fontSize: Dimens.text_large}]}>{Strings.CONFIRM_ADD_TO_STOCK}</Text>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.onClassifiedCheckboxClick()}>

                        <View style={{ flexDirection: 'row', marginTop: Dimens.margin_x_large,alignItems:'center' }}>
                            <Image source={this.props.isClassifiedCheckboxChecked ? icon_checked : icon_unchecked}
                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                            <Text style={styles.checkboxText}>{Strings.MAKE_CLASSIFIED}</Text>
                        </View>
                    </TouchableOpacity> 
                    <View style={{flexDirection: 'row',justifyContent:'space-between',alignItems:'center',marginTop: Dimens.margin_xxx_large}}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{ padding: 5,paddingLeft:0, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.WHITE }}
                        onPress={() => this.props.hideDialog()}>

                        <Text style={styles.buttonText}>{Strings.CANCEL}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{ padding: 5, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.WHITE }}
                        onPress={() => this.props.okPress()}>

                        <Text style={styles.buttonText}>{(Strings.OK).toUpperCase()}</Text>
                    </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </TouchableOpacity>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    modalText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    modalImage: {
        height: 30,
        width: 30,
    },
    seperator: {
        height: 1,
        //width:'100%',
        backgroundColor: Colors.BLACK_12,
        marginRight: 10,
        marginTop: Dimens.margin_normal
    },
    menuOptionText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        //fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
    triangle: {
        position: 'absolute',
        bottom: -10,
        left: 25,
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderBottomWidth: 12,
        marginRight: 0,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: Colors.TOOLTIP_COLOR,
        transform: [
            { rotate: '-180deg' }
        ]
    },
    tooltipText: {
        flex: 1,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        color: Colors.TOOLTIP_TEXT_COLOR,
        marginHorizontal: Dimens.margin_normal
    },
    gotItText: {
        fontWeight: '700',
        color: Colors.PRIMARY,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        marginHorizontal: Dimens.margin_normal
    },
    checkboxText: {
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        marginLeft: Dimens.margin_normal
    },
    buttonText: {
        color: Colors.PRIMARY,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        fontWeight: 'bold',
    },
});