import React from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    SafeAreaView,
    Modal
} from 'react-native';
import {Colors, Strings, Dimens} from "../../values";
import {icon_cancel} from './../../assets/ImageAssets'

const StockWebUrlsModal = ({visible, data, onUrlPress}) => {
    let title = (data.make ? (data.make + ' ') : '') + (data.modelVersion ? data.modelVersion : '');
    let urls = data.web_url && data.web_url instanceof Array  ? data.web_url : [];
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
            onRequestClose={() => onUrlPress(false)}
            onDismiss={() => onUrlPress(false)}>

            <SafeAreaView
                style={{flex: 1}}>
                <View style={{flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end'}}>
                    <View style={{backgroundColor: Colors.WHITE, paddingHorizontal: 20, paddingVertical: 10}}>

                        <TouchableOpacity
                            style={{alignSelf: 'flex-end', padding: 1}}
                            onPress={() => onUrlPress(false)}>
                            <Image source={icon_cancel}
                                   style={{width: Dimens.icon_normal, height: Dimens.icon_normal}}/>
                        </TouchableOpacity>
                        <Text style={styles.headText}>{title}</Text>
                        <View style={{width: '100%'}}>
                            {urls.map((item, index) => {
                                return (
                                    <TouchableOpacity onPress={() => onUrlPress(true, item.url)} activeOpacity={0.7}>
                                        <Text style={styles.rowText}>{item.label}</Text>
                                        <View style={{height: 1, backgroundColor: Colors.BLACK_25}}/>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
    )

};


const styles = StyleSheet.create({
    headText: {
        color: Colors.BLACK,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT,
        paddingBottom: Dimens.padding_normal
    },
    rowText: {
        color: Colors.BLACK,
        fontWeight: '400',
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_normal
    }
});

export default StockWebUrlsModal;