import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity,SafeAreaView,TextInput,KeyboardAvoidingView,Platform} from 'react-native';
import {Colors, Strings, Dimens} from "../../values";
import { icon_cancel } from './../../assets/ImageAssets'
import TextInputLayout from "../../granulars/TextInputLayout"

const ShareByEmailModalView = ({setModalVisible, onEmailShareSubmit,onEmailChange,height}) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.BLACK_50,marginBottom : Platform.OS == 'ios' ? height : 0}}>
            <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: 1 }}
                        onPress={() => {
                            setModalVisible(false)
                        }}>
                        <Image source={icon_cancel}
                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                    </TouchableOpacity>

                    {/* <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large,marginBottom: Dimens.margin_normal }}>{Strings.ENTER_EMAIL_IDS}</Text> */}
                    <TextInputLayout style={{ height: 50 }}
                        hintColor={Colors.BLACK_85}
                        focusColor={Colors.ORANGE}>
                        <TextInput style={styles.textInput}
                            selectTextOnFocus={false}
                            returnKeyType='go'
                            //keyboardType='numeric'
                            //maxLength={6}
                            //value={this.state.emailIds}
                            placeholder={Strings.ENTER_COMMA_SEPERATED_EMAIL_IDS}
                            //ref={(input) => this.OtpInput = input}
                            //onSubmitEditing={() => this.props.onProceedForOtp(this.state.otp)}
                            onChangeText={(text) => onEmailChange(text)} />
                    </TextInputLayout>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => onEmailShareSubmit()}>

                        <View style={styles.buttonStyle}>
                            <Text style={styles.buttonTextStyle}>{Strings.SEND}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )

};


const styles = StyleSheet.create({
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
});

export default ShareByEmailModalView;