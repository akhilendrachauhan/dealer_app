import React, { Component } from 'react';
import { View,StyleSheet,SafeAreaView,Image,TouchableOpacity,Text, Linking,BackHandler} from 'react-native';
import {icon_back_arrow} from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import { Utility, Constants } from '../../util';
import ActionBarWrapper from "./../../granulars/ActionBarWrapper";    
import CustomViewPageIndicator from "../../granulars/CustomViewPagerIndicator" 
import ViewPager from './../../nodeModuleChanges/react_native_viewpager/ViewPager'
import {icon_arrow_left,icon_arrow_right,pdf_icon} from './../../assets/ImageAssets'

const dataSource = new ViewPager.DataSource({
    pageHasChanged: (p1, p2) => p1 !== p2,
});
export default class ImageReviewScreen extends Component {

    constructor(props) {
        super(props)
        
        this.state = {
            position: this.props.navigation.state.params.index,
            images : this.props.navigation.state.params.data,
            changeType : 0,
        }
        
    }

    componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.onBackPress()
        return true
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    };

    renderPage = (item) => {
        return (
            <View style={{ flex: 1, paddingHorizontal: 0 }}>
            <TouchableOpacity 
                onPress={() => {
                    if(Utility.checkFileType(item,Constants.PDF_FILE_TYPE)){
                        //Linking.openURL(item)
                        this.props.navigation.navigate("pdfViewScreen",{item : item})
                    }
                }}
                style={{width:'100%',height:400,backgroundColor: Colors.WHITE,justifyContent:'center',alignItems:'center'}}
                activeOpacity={1}
            >
                <Image source={Utility.checkFileType(item,Constants.PDF_FILE_TYPE) ? pdf_icon : {uri: item}}
                        resizeMode={'contain'} style={{width: '100%', height: Utility.checkFileType(item,Constants.PDF_FILE_TYPE) ? 150 : 400,backgroundColor:Colors.WHITE}}/>
            </TouchableOpacity>
            </View>
        );
    }

    renderPageIndicators = (size) => {
        return (
            <CustomViewPageIndicator
                positionOffset={305}
                dotSize={8}
                dotSpace={6}
                dotBorderWidth={1}
                dotBorderColor={Colors.WHITE} />
        );
    }
    render() {
        let data = dataSource.cloneWithPages(this.state.images);
        let actionBarProps = {
            values: {
                title: Strings.IMAGE_REVIEW
            },
            rightIcons: [],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };


        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK  }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />

                    <View style={{marginTop: Dimens.margin_xx_large,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color: Colors.WHITE,fontWeight: '500',fontFamily : Strings.APP_FONT,fontSize: Dimens.text_xx_large}}>
                            {this.state.position + 1} / {this.state.images.length}
                        </Text>
                    </View>
                    {/* <View style={{flex: 1, justifyContent:'center',alignItems:'center',backgroundColor:Colors.SCREEN_BACKGROUND_DARKER}}>
                        <Slideshow 
                            dataSource = { this.state.images }
                            indicatorSize = {5}
                            //height = {200}
                            arrowSize = {12}
                            position={this.state.position}
                            onPositionChanged={position => this.setState({ position })}
                        />
                    </View> */}
                    <View style={styles.viewPagerContainer}>
                        <View style={styles.viewPager}>
                            <ViewPager
                                ref={(viewpager) => {this.viewpager = viewpager}}
                                dataSource={data}
                                renderPage={this.renderPage}
                                isLoop={false}
                                autoPlay={false}
                                initialPage = {this.state.position}
                                changeType = {this.state.changeType}
                                onChangePage={(index) => {
                                        this.setState({ position: index, changeType : 0 })
                                }}
                                renderPageIndicator={() => 
                                //this.renderPageIndicators(this.state.images.length)
                                <View/>
                                }
                            />
                            <TouchableOpacity style={{position:'absolute',top:200,left:10}} onPress={() => {
                                if(this.state.position != 0){
                                    //this.viewpager.goToPage(this.state.position - 1);
                                    this.setState({position : this.state.position - 1, changeType : 1})
                                }
                            }}>
                            <Image source={icon_arrow_left} style={styles.image}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{position:'absolute',top:200,right:10}} onPress={() => {
                                if(this.state.position < this.state.images.length-1){
                                    //this.viewpager.goToPage(this.state.position + 1);
                                    this.setState({position : this.state.position + 1, changeType : 1})
                                }
                            }}>
                            <Image source={icon_arrow_right} style={styles.image}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
           
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND,
        
    },
    viewPagerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewPager: {
        height: 420,
        width: '100%'
    },
    image :{
        width: Dimens.icon_xx_large,
        height: Dimens.icon_xx_large,
        tintColor : Colors.PRIMARY
    }
   
});