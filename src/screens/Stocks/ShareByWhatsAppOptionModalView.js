import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native';
import { Colors, Strings, Dimens } from "../../values";
import { icon_cancel, icon_arrow_right } from './../../assets/ImageAssets'

const ShareByWhatsAppOptionModalView = ({ setModalVisible, sendCarDetails, shareCarPhotos,title }) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK_50 }}>
            <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: 1 }}
                        onPress={() => {
                            setModalVisible()
                        }}>
                        <Image source={icon_cancel}
                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                    </TouchableOpacity>

                    <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large, marginBottom: Dimens.margin_normal }}>{title}</Text>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            sendCarDetails()
                        }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.SEND_CAR_DETAILS}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.seperator} />
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            shareCarPhotos()
                        }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.SHARE_CAR_PHOTOS}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        </SafeAreaView>
    )

};


const styles = StyleSheet.create({
    modalText: {
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    modalImage: {
        height: 30,
        width: 30,
    },
    seperator: {
        height: 1,
        //width:'100%',
        backgroundColor: Colors.BLACK_12,
        marginRight: 10,
        marginTop: Dimens.margin_normal
    },
});

export default ShareByWhatsAppOptionModalView;
