import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity,SafeAreaView} from 'react-native';
import {Colors, Strings, Dimens} from "../../values";
import { icon_cancel,icon_arrow_right } from './../../assets/ImageAssets'
import { Utility, Constants } from '../../util';

const RemoveStockModalView = ({setModalVisible,clickOnCarBooked,onCarSoldSubmit,clickOnRenew}) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.BLACK_50}}>
            <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: 1 }}
                        onPress={() => {
                            setModalVisible()
                        }}>
                        <Image source={icon_cancel}
                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                    </TouchableOpacity>

                    <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large,marginBottom:Dimens.margin_normal }}>{Strings.SELECT_REASON_FOR_REMOVING_STOCK}</Text>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            clickOnCarBooked()
                        }}>

                        <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.CAR_IS_BOOKED}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.seperator}/>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            onCarSoldSubmit(Constants.CAR_IS_SOLD,'0')   
                        }}>

                        <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.CAR_SOLD_BY_ANOTHER_DEALER}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.seperator}/>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            onCarSoldSubmit(Constants.CAR_IS_SENT,'0')
                        }}>

                        <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.CAR_SENT_FOR_REFURBISHMENT}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.seperator}/>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            onCarSoldSubmit(Constants.CAR_DETAILS_INCORRECT,'1')   
                        }}>

                        <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.CAR_DETAILS_ARE_INCORRECT}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity>
                    {/* <View style={styles.seperator}/>
                    <TouchableOpacity style={{ marginTop: Dimens.margin_normal }}
                        onPress={() => {
                            clickOnRenew()    
                        }}>

                        <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center' }}>
                            <Text style={styles.modalText}>{Strings.LISTING_BECOME_OLD}</Text>
                            <Image source={icon_arrow_right}
                                style={styles.modalImage} />
                        </View>
                    </TouchableOpacity> */}
                    
                </View>
            </View>
        </SafeAreaView>
    )

};


const styles = StyleSheet.create({
    modalText:{
        fontFamily: Strings.APP_FONT,
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
    },
    modalImage:{
        height: 30,
        width: 30,
    },
    seperator:{
        height:1,
        //width:'100%',
        backgroundColor: Colors.BLACK_12,
        marginRight:10,
        marginTop: Dimens.margin_normal
    },
});

export default RemoveStockModalView;
