import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, SafeAreaView, TextInput, Platform, KeyboardAvoidingView, Keyboard } from 'react-native';
import { Colors, Strings, Dimens } from "../../values";
import { icon_cancel } from './../../assets/ImageAssets'
import TextInputLayout from "../../granulars/TextInputLayout"
import { Utility, Constants } from '../../util'
import { Dropdown } from 'react-native-material-dropdown'
export default class ShareBySMSModalView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            mobileNo: '',
            countryCode: Constants.DEFAULT_COUNTRY_CODE,
            countryCodeList: []
        }
    }

    async componentDidMount() {
        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeList: countryCodeData })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK_50, marginBottom: Platform.OS == 'ios' ? this.props.height : 0 }}>
                <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                    <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                        <TouchableOpacity
                            style={{ alignSelf: 'flex-end', padding: 1 }}
                            onPress={() => {
                                this.props.setModalVisible(false)
                            }}>
                            <Image source={icon_cancel}
                                style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Dropdown
                                disabled= {true}
                                containerStyle={{ flex: 0.4 }}
                                dropdownOffset={{ top: 40, left: 0 }}
                                baseColor={Colors.GRAY}
                                data={this.state.countryCodeList}
                                absoluteRTLLayout={true}
                                value={this.state.countryCode}
                                onChangeText={(value, index, data) => {
                                    this.setState({ countryCode: value, selectedItem: data[index] })
                                }}
                            />

                            <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                                <TextInputLayout
                                    ref={(input) => this.TL_Mobile = input}
                                    focusColor={Colors.ORANGE}
                                    hintColor={Colors.GRAY}
                                    errorColor={Colors.ORANGE}>

                                    <TextInput
                                        style={styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='go'
                                        keyboardType='numeric'
                                        value={this.state.mobileNo}
                                        placeholder={Strings.MOBILE_NUMBER}
                                        maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                        onChangeText={(text) => this.setState({ mobileNo: text })} />
                                </TextInputLayout>
                            </View>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => this.props.onSMSShareSubmit(this.state.countryCode, this.state.mobileNo, this.state.countryCodeList)}>

                            <View style={styles.buttonStyle}>
                                <Text style={styles.buttonTextStyle}>{Strings.SEND}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
})