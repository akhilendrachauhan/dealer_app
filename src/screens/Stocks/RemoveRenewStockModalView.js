import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity,SafeAreaView} from 'react-native';
import {Colors, Strings, Dimens} from "../../values";
import { icon_cancel } from './../../assets/ImageAssets'
import { Utility, Constants } from '../../util';

const RemoveRenewStockModalView = ({setModalVisible, onCarSoldSubmit}) => {
    let price = '0';
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.BLACK_50}}>
            <View style={{ flex: 1, backgroundColor: Colors.BLACK_50, justifyContent: 'flex-end' }}>
                <View style={{ backgroundColor: Colors.WHITE, padding: 20 }}>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: 1 }}
                        onPress={() => {
                            setModalVisible(false)
                        }}>
                        <Image source={icon_cancel}
                            style={{ width: Dimens.icon_normal, height: Dimens.icon_normal }} />
                    </TouchableOpacity>

                    <Text style={{ color: Colors.BLACK, fontWeight: '500', fontSize: Dimens.text_x_large,marginBottom: Dimens.margin_normal }}>{Strings.RENEW_YOUR_OLD_LISTING}</Text>
                    
                    <Text style={{ color: Colors.BLACK, fontSize: Dimens.text_normal,marginTop: Dimens.margin_normal }}>{Strings.BOOST_YOUR_STOCK_BY_RENEWING}</Text>
                    <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                        style={{flex:1,marginRight:10}}
                        activeOpacity={0.8}
                        onPress={() => {
                            onCarSoldSubmit(Constants.CAR_LISTING_OLD,'0')
                        }
                        }>

                        <View style={[styles.buttonStyle,{backgroundColor:Colors.WHITE,borderColor: Colors.PRIMARY,borderWidth:1}]}>
                            <Text style={[styles.buttonTextStyle,{color: Colors.PRIMARY}]}>{Strings.REMOVE_STOCK_BUTTON}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{flex:1}}
                        activeOpacity={0.8}
                        onPress={() => {
                            //onCarSoldSubmit(Constants.CAR_LISTING_OLD,'0')
                            Utility.showToast('Coming Soon...')
                        }
                        }>

                        <View style={styles.buttonStyle}>
                            <Text style={styles.buttonTextStyle}>{Strings.RENEW_POST_BUTTON}</Text>
                        </View>
                    </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )

};


const styles = StyleSheet.create({
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',

    },
    textStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.WHITE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        //marginHorizontal: Dimens.margin_xxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
});

export default RemoveRenewStockModalView;
