import React, { Component } from 'react'
import ForgotPasswordScreen from './ForgotPasswordScreen'
import { View, Keyboard } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import { Strings, Colors, Dimens } from '../../values'
import * as APICalls from '../../api/APICalls'
import { NavigationActions, StackActions } from "react-navigation"

export default class ForgotPasswordContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            loadingMsg: '',
            showProgress: false,
            toResetPassword: true,
            otpAlertDialogVisibility: false,
            toResendOtp: false,
            otpToken: '',
            countryCodeData: [],
        }
    }

    async componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.FORGOT_PASSWORD_SCREEN)

        let countryCodeData = await Utility.getConfigArrayData(Constants.COUNTRY_CODE_VALUES)
        this.setState({ countryCodeData: countryCodeData.length > 0 ? countryCodeData : Constants.COUNTRY_CODE_LIST })
    }

    onProceedForOtp = async (countryCode, phoneNumber) => {
        if (!Utility.validateMobileNo(phoneNumber, countryCode)) {
            this.refs['passwordResetScreen'].TL_PhoneNumber.setError(Strings.INVALID_MOBILE_NUMBER)
            this.refs['passwordResetScreen'].PhoneNumberInput.focus()
        }
        else {
            Keyboard.dismiss()

            // let phoneNo = await countryCode + phoneNumber
            let phoneNo = await phoneNumber

            try {
                APICalls.sendOTP(phoneNo, this.onSuccessSentOTP, this.onFailureSendOTP, this.props)
            } catch (error) {
                Utility.log(error);
            }
            this.setState({ isLoading: true })
        }
    }

    onSuccessSentOTP = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false, toResendOtp: true })
        this.otpAlertDialog(true)
        Utility.log('onSuccessSentOTP===> ', JSON.stringify(response))
    }

    onFailureSendOTP = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false })
        Utility.log('onFailureSendOTP===> ', JSON.stringify(response))
    }

    otpAlertDialog = (visible) => {
        this.setState({ otpAlertDialogVisibility: visible })
    }

    onSubmitOTP = (phoneNumber, otp) => {

        if (Utility.isValueNullOrEmpty(otp)) {
            this.refs['passwordResetScreen'].TL_OTP.setError(Strings.ENTER_OTP_NUMBER)
            this.refs['passwordResetScreen'].OtpInput.focus()
            return
        }
        else {
            try {
                APICalls.validateOTP(phoneNumber, otp, this.onSuccessValidateOtp, this.onFailureValidateOtp, this.props)
            } catch (error) {
                Utility.log(error)
            }
            this.setState({ isLoading: true })
        }
    }

    onSuccessValidateOtp = (response) => {
        // if (response && response.message) {
        //     Utility.showToast(response.message)
        // }
        this.refs['passwordResetScreen'].setState({ otp: '' })
        this.otpAlertDialog(!this.state.otpAlertDialogVisibility)

        this.setState({ isLoading: false, toResetPassword: false, otpToken: response.data.token })
        Utility.log('onSuccessValidateOtp===> ', JSON.stringify(response))
    }

    onFailureValidateOtp = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false, toResendOtp: true })
        Utility.log('onFailureValidateOtp===> ', JSON.stringify(response))
    }

    onUpdatePassword = (phoneNumber, newPassword, verifyPassword) => {

        if (Utility.isValueNullOrEmpty(newPassword)) {
            this.refs['passwordResetScreen'].TL_NewPassword.setError(Strings.ENTER_NEW_PASSWORD)
            this.refs['passwordResetScreen'].NewPassWordInput.focus()
            return
        }
        else if (Utility.isValueNullOrEmpty(verifyPassword)) {
            this.refs['passwordResetScreen'].TL_VerifyPassword.setError(Strings.RE_ENTER_PASSWORD)
            this.refs['passwordResetScreen'].VerifyPassWordInput.focus()
            return
        }
        else if ((newPassword && verifyPassword).length < 6) {
            Utility.showToast(Strings.PASSWORD_VALIDATION_MESSAGE)
            return
        }
        else if (newPassword !== verifyPassword) {
            Utility.showToast(Strings.PASSWORD_NOT_MATCHING)
            return
        }
        else {
            Keyboard.dismiss()
            try {
                APICalls.setPassword(phoneNumber, newPassword, this.state.otpToken, this.onSuccessResetPassword, this.onFailureResetPassword, this.props)
            } catch (error) {
                Utility.log(error)
            }
            this.setState({ isLoading: true })
        }
    }

    onSuccessResetPassword = (response) => {
        if (response && response.data)
            Utility.showToast(response.data)

        this.setState({ isLoading: false }, () => {
            this.onUpdatePasswordClick()
        })
        Utility.log('onSuccessResetPassword===> ', JSON.stringify(response))
    }

    onFailureResetPassword = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false })
        Utility.log('onFailureResetPassword===> ', JSON.stringify(response))
    }

    onUpdatePasswordClick = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate(
                {
                    routeName: 'splash',
                }
            )],
        })
        this.props.navigation.dispatch(resetAction)
    }

    onResendOtp = (countryCode, phoneNumber) => {
        this.onProceedForOtp(countryCode, phoneNumber)
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <ForgotPasswordScreen
                ref={'passwordResetScreen'}
                isLoading={this.state.isLoading}
                loadingMsg={this.state.loadingMsg}
                onBackPress={this.onBackPress}
                onProceedForOtp={this.onProceedForOtp}
                onUpdatePassword={this.onUpdatePassword}
                onOtpVerify={this.onOtpVerify}
                otpAlertDialog={this.otpAlertDialog}
                toResetPassword={this.state.toResetPassword}
                otpAlertDialogVisibility={this.state.otpAlertDialogVisibility}
                onSubmitOTP={this.onSubmitOTP}
                toResendOtp={this.state.toResendOtp}
                onResendOtp={this.onResendOtp}
                countryCodeData={this.state.countryCodeData}
            />
        )
    }
}
