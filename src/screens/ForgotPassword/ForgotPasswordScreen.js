import React, { Component } from 'react';
import {
    StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Modal, Platform,
    KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, SafeAreaView
} from 'react-native';
import TextInputLayout from "../../granulars/TextInputLayout"
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { Utility, Constants } from '../../util'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import { _ActionBarStyle } from '../../values/Styles'
import LoadingComponent from '../../components/LoadingComponent'
import { Dropdown } from 'react-native-material-dropdown'

export default class ForgotPasswordScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            otpAlertDialogVisibility: false,
            phoneNumber: '',
            otp: '',
            newPassword: '',
            verifyPassword: '',
            toResetPassword: false,
            toResendOtp: false,
            countryCode: Constants.DEFAULT_COUNTRY_CODE
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <KeyboardAvoidingView
                    style={Styles.container}
                    behavior="padding"
                    keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : -500}>

                    <TouchableWithoutFeedback
                        onPress={() => { Keyboard.dismiss() }}>

                        <View style={Styles.container}>
                            <ActionBarWrapper
                                values={{ title: Strings.F_PASSWORD }}
                                actions={{ onLeftPress: () => this.props.onBackPress() }}
                                styleAttributes={{
                                    leftIconImage: ImageAssets.icon_back_arrow,
                                    disableShadows: true,
                                    tintColor: Colors.PRIMARY_DARK,
                                    containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                                }}
                            />

                            {this.props.toResetPassword ? this.mainRender() : this.verifyPasswordRender()}
                            {this.dialogModelRender()}

                            {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }

    mainRender() {
        return (
            <View style={Styles.mainContainerStyle}>
                <Text style={Styles.messageText}>{Strings.PASSWORD_CHANGE_ACCOUNT_MESSAGE}</Text>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Dropdown
                        containerStyle={{ flex: 0.4 }}
                        dropdownOffset={{ top: 50, left: 0 }}
                        baseColor={Colors.GRAY}
                        data={this.props.countryCodeData}
                        absoluteRTLLayout={true}
                        value={this.state.countryCode}
                        onChangeText={(value, index, data) => {
                            this.setState({ countryCode: value, selectedItem: data[index] })
                        }}
                    />

                    <View style={{ flex: 1.5, marginLeft: Dimens.margin_xx_large }}>
                        <TextInputLayout
                            style={{ marginTop: Dimens.margin_normal }}
                            ref={(input) => this.TL_PhoneNumber = input}
                            hintColor={Colors.BLACK_85}
                            focusColor={Colors.PRIMARY}
                            errorColor={Colors.PRIMARY}>

                            <TextInput style={Styles.textInput}
                                selectTextOnFocus={false}
                                returnKeyType='go'
                                keyboardType='numeric'
                                value={this.state.phoneNumber}
                                placeholderTextColor={Colors.BLACK_85}
                                underlineColorAndroid={Colors.BLACK_85}
                                ref={(input) => this.PhoneNumberInput = input}
                                maxLength={Utility.maxLengthMobileNo(this.state.countryCode)}
                                onSubmitEditing={() => this.props.onProceedForOtp(this.state.countryCode, this.state.phoneNumber)}
                                placeholder={Strings.MOBILE_NUMBER}
                                onChangeText={(text) => this.setState({ phoneNumber: text })} />
                        </TextInputLayout>
                    </View>
                </View>

                {this.props.toResendOtp ?
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {
                            this.props.onResendOtp(this.state.countryCode, this.state.phoneNumber)
                        }}>
                        <View style={Styles.buttonStyle}>
                            <Text style={Styles.buttonText}>{Strings.RESEND_OTP}</Text>
                        </View>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => {
                            this.props.onProceedForOtp(this.state.countryCode, this.state.phoneNumber)
                        }}>
                        <View style={Styles.buttonStyle}>
                            <Text style={Styles.buttonText}>{Strings.PROCEED}</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        )
    }

    verifyPasswordRender() {
        return (
            <View style={Styles.forgotPasswordStyle}>
                {/* <Text style={Styles.accountRegText}>{Strings.ACCOUNT_REG_WITH_EMAIL}
                    <Text style={{ fontWeight: 'bold' }}>{this.state.email}</Text>
                </Text> */}

                <TextInputLayout
                    style={{ marginTop: Dimens.margin_xx_large }}
                    ref={(input) => this.TL_NewPassword = input}
                    hintColor={Colors.BLACK_85}
                    focusColor={Colors.PRIMARY}
                    errorColor={Colors.PRIMARY}>

                    <TextInput
                        style={Styles.textInput}
                        returnKeyType='next'
                        selectTextOnFocus={false}
                        value={this.state.newPassword}
                        placeholderTextColor={Colors.BLACK_85}
                        underlineColorAndroid={Colors.BLACK_85}
                        ref={(input) => this.NewPassWordInput = input}
                        onSubmitEditing={() => this.VerifyPassWordInput.focus()}
                        placeholder={Strings.ENTER_NEW_PASSWORD}
                        onChangeText={(text) => this.setState({ newPassword: text })}
                    />
                </TextInputLayout>

                <TextInputLayout style={{ marginTop: Dimens.margin_xx_large }}
                    ref={(input) => this.TL_VerifyPassword = input}
                    hintColor={Colors.BLACK_85}
                    focusColor={Colors.PRIMARY}
                    errorColor={Colors.PRIMARY}>

                    <TextInput
                        style={Styles.textInput}
                        selectTextOnFocus={false}
                        returnKeyType='go'
                        value={this.state.verifyPassword}
                        placeholderTextColor={Colors.BLACK_85}
                        underlineColorAndroid={Colors.BLACK_85}
                        ref={(input) => this.VerifyPassWordInput = input}
                        onSubmitEditing={() => this.props.onUpdatePassword(this.state.phoneNumber, this.state.newPassword, this.state.verifyPassword)}
                        placeholder={Strings.RE_ENTER_PASSWORD}
                        onChangeText={(text) => this.setState({ verifyPassword: text })}
                    />
                </TextInputLayout>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.props.onUpdatePassword(this.state.phoneNumber, this.state.newPassword, this.state.verifyPassword)}>
                    <View style={Styles.buttonStyle}>
                        <Text style={Styles.buttonText}>{Strings.UPDATE_PASSWORD}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    dialogModelRender() {
        return (
            <View>
                <Modal
                    visible={this.props.otpAlertDialogVisibility}
                    transparent={true}
                    animationType={"fade"}
                    onRequestClose={() => {
                        this.props.otpAlertDialog(!this.props.otpAlertDialogVisibility)
                    }}>
                    <TouchableWithoutFeedback onPress={() => {
                        Keyboard.dismiss()
                    }}>
                        <View style={Styles.Alert_Main_View}>
                            <View style={Styles.Alert_Inner_View}>
                                <Text style={Styles.Alert_Title}>{Strings.ENTER_OTP1}</Text>

                                <Text style={Styles.Alert_Message}> {Strings.OTP_DIALOG_MESSAGE1}
                                    <Text style={{ fontWeight: 'bold' }}>{this.state.phoneNumber}</Text>
                                    {Strings.OTP_DIALOG_MESSAGE2}</Text>

                                <TextInputLayout
                                    style={{ height: 50, marginTop: Dimens.margin_xx_large }}
                                    ref={(input) => this.TL_OTP = input}
                                    hintColor={Colors.BLACK_85}
                                    focusColor={Colors.PRIMARY}
                                    errorColor={Colors.PRIMARY}>

                                    <TextInput style={Styles.textInput}
                                        selectTextOnFocus={false}
                                        returnKeyType='go'
                                        keyboardType='numeric'
                                        maxLength={6}
                                        value={this.state.otp}
                                        placeholder={Strings.ENTER_OTP}
                                        ref={(input) => this.OtpInput = input}
                                        onSubmitEditing={() => this.props.onSubmitOTP(this.state.phoneNumber, this.state.otp)}
                                        onChangeText={(text) => this.setState({ otp: text })} />
                                </TextInputLayout>

                                <View style={{ flexDirection: 'row', height: 50, marginTop: 10, justifyContent: 'flex-end', }}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => {
                                            this.props.otpAlertDialog(!this.props.otpAlertDialogVisibility);
                                            this.setState({ otp: '' });
                                        }}>
                                        <Text style={[Styles.TextStyle, { marginRight: Dimens.margin_xxx_large }]}>{Strings.CANCEL}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.onSubmitOTP(this.state.phoneNumber, this.state.otp)}>

                                        <Text style={Styles.TextStyle}>{Strings.SUBMIT}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* {this.getProgressView()} */}
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    mainContainerStyle: {
        flex: 1,
        flexDirection: "column",
        marginTop: Dimens.margin_normal,
        marginLeft: Dimens.margin_xx_large,
        marginRight: Dimens.margin_xx_large,
    },
    forgotPasswordStyle: {
        flex: 1,
        flexDirection: "column",
        marginTop: Dimens.margin_normal,
        marginLeft: Dimens.margin_xx_large,
        marginRight: Dimens.margin_xx_large,

    },
    buttonStyle: {
        height: 50,
        marginTop: Dimens.margin_xxxxx_large,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        borderRadius: 7,
    },
    textInput: {
        height: 40,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_85,
        alignSelf: 'stretch',
    },
    Alert_Main_View: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLACK_54,
    },
    Alert_Inner_View: {
        backgroundColor: Colors.WHITE,
        marginHorizontal: Dimens.margin_xx_large,
        padding: Dimens.padding_xx_large,
        borderRadius: Dimens.border_radius
    },
    Alert_Title: {
        fontSize: Dimens.text_xx_large,
        color: Colors.BLACK_85,
        textAlign: 'left',
        justifyContent: 'center',
        padding: Dimens.padding_x_small,
        height: 40,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    Alert_Message: {
        fontSize: Dimens.text_normal,
        color: Colors.BLACK_85,
        textAlign: 'left',
        fontFamily: Strings.APP_FONT
    },
    TextStyle: {
        textAlign: 'center',
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        color: Colors.ORANGE,
        marginTop: Dimens.margin_normal,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    },
    accountRegText: {
        fontSize: Dimens.text_large,
        height: 50,
        color: Colors.BLACK_85,
        textAlign: 'center',
        fontFamily: Strings.APP_FONT
    },
    buttonText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_large,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    messageText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_large,
        padding: Dimens.padding_x_small,
        fontFamily: Strings.APP_FONT
    }
});
