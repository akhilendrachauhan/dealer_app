import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import * as ImageAssets from '../../assets/ImageAssets'

export default class LoanFilterScreen extends Component {

    constructor(props) {
        super(props)
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                style={Styles.containerItem}
                activeOpacity={0.8}
                onPress={() => this.props.onItemClick(item, index)}>

                <Text style={Styles.itemText}>{item.sub_status_name}</Text>

                <Image style={{ width: 24, height: 24, resizeMode: 'contain' }}
                    source={item.isSelected ? ImageAssets.icon_checked : ImageAssets.icon_unchecked} />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <View style={Styles.containerHead}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity activeOpacity={0.8} onPress={this.props.onBackPress}>
                            <Image style={{ tintColor: Colors.WHITE, marginHorizontal: 20 }}
                                source={ImageAssets.icon_cancel} />
                        </TouchableOpacity>

                        <Text style={Styles.headText}>{Strings.FILTER}</Text>
                    </View>

                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.onResetPress()}>
                        <Text style={Styles.resetText}>{Strings.RESET}</Text>
                    </TouchableOpacity>
                </View>

                <FlatList
                    style={{ marginHorizontal: Dimens.margin_small }}
                    ref={(ref) => { this.flatListRef = ref }}
                    extraData={this.props}
                    data={this.props.filterData}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => '' + index}
                    showsVerticalScrollIndicator={false}
                />

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.props.onSubmitClick}>

                    <View style={Styles.buttonStyle}>
                        <Text style={Styles.loginStyle}>{Strings.SUBMIT}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    containerHead: {
        flexDirection: 'row',
        height: 57,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.PRIMARY
    },
    containerItem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: Dimens.padding_xx_large,
    },
    headText: {
        fontSize: 20,
        fontWeight: '500',
        color: Colors.WHITE,
        fontFamily: Strings.APP_FONT
    },
    resetText: {
        fontSize: 14,
        color: Colors.WHITE,
        paddingRight: Dimens.padding_xxx_large,
        fontFamily: Strings.APP_FONT
    },
    itemText: {
        flex: 1,
        color: Colors.BLACK_85,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        fontSize: Dimens.text_x_small,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    tagText: {
        position: 'absolute',
        right: 8,
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    },
    loginStyle: {
        color: Colors.WHITE,
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: Strings.APP_FONT
    },
    buttonStyle: {
        height: 50,
        marginTop: 30,
        // marginHorizontal: 20,
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
        alignItems: 'center',
        // borderRadius: 7,
    },
})
