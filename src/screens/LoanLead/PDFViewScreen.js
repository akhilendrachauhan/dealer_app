import React, { Component } from 'react';
import { View,StyleSheet,SafeAreaView,Dimensions,BackHandler} from 'react-native';
import {icon_back_arrow} from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import { Utility, Constants } from '../../util';
import ActionBarWrapper from "./../../granulars/ActionBarWrapper"; 
import Pdf from 'react-native-pdf';  
export default class PDFViewScreen extends Component {

    constructor(props) {
        super(props)
        
        this.state = {
            item: this.props.navigation.state.params.item,
        }
        
    }

    componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.onBackPress()
        return true
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    };

    render() {
        let actionBarProps = {
            values: {
                title: Strings.PDF_VIEW
            },
            rightIcons: [],
            styleAttr: {
                leftIconImage: icon_back_arrow,
                disableShadows: true,
                tintColor: Colors.PRIMARY_DARK,
                containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
            },
            actions: {
                onLeftPress: this.onBackPress,
            }
        };

        const source = {uri : this.state.item,cache : true};
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.BLACK  }}>
                <View style={styles.container}>
                    <ActionBarWrapper
                        values={actionBarProps.values}
                        actions={actionBarProps.actions}
                        iconMap={actionBarProps.rightIcons}
                        styleAttributes={actionBarProps.styleAttr}
                    />
                <Pdf
                    source={source}
                    maxScale={3}
                    style={styles.pdf}/>
                </View>
            </SafeAreaView>
           
        )
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
});