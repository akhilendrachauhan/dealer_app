import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { Colors, Dimens } from '../../values'
import { _ActionBarStyle } from '../../values/Styles'
import Timeline from '../../components/TimelineComponent'
import LoadingComponent from "../../granulars/LoadingComponent"

export default class LoanLeadHistoryScreen extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={Styles.container}>
                <Timeline
                    data={this.props.historyData}
                    isLoanTimeline={true}
                />

                {this.props.isLoading && <LoadingComponent backgroundColor={Colors.TRANSPARENT} />}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        paddingHorizontal: Dimens.padding_xx_large
    }
})