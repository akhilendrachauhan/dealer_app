import React, { Component } from 'react'
import LoanLeadScreen from './LoanLeadScreen'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import { Utility, Constants,AnalyticsConstants } from '../../util'
import { _ActionBarStyle } from '../../values/Styles'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { ScreenStates, ScreenName } from '../../util/Constants'

export default class LoanLeadContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            screenState: ScreenStates.IS_LOADING,
            data: [],
            refreshing: false,
            isLoading: false,
        }

        this.subs = [
            this.props.navigation.addListener('didFocus', () => {
                this.getLoanCount()
            })
         ]
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_LEAD_SCREEN)
        //this.getLoanCount()
    }

    onRefresh = () => {
        this.setState({ refreshing: true }, () => {
            this.getLoanCount()
        })
    }

    onRetryClick = () => {
        this.getLoanCount()
    }

    onItemClick = (item) => {
        //if (item.LeadCount > 0) {
        Utility.sendEvent(AnalyticsConstants.LOAN_STATUS_BUCKET_CLICK,{name : item.name})
        this.props.navigation.navigate('loanLeadList', {
            selectedItem: item
        })
        // }
        // else {
        //     Utility.showToast(Strings.NO_LEADS_AVAILABLE)
        // }
    }

    getLoanCount = async () => {
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID, Constants.USER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])
        Utility.log('USER_ID===>' + values[1][1])
        this.setState({ isLoading: true})
        try {
            APICalls.getLoanLeadCount(values[1][1], [values[0][1]], this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = (response) => {
        if (response && response.data && response.data.length > 0) {
            this.setState({
                refreshing: false,
                isLoading: false,
                data: response.data,
                screenState: ScreenStates.NO_ERROR
            })
        }
        else {
            this.setState({
                isLoading: false, refreshing: false,
                screenState: ScreenStates.NO_DATA_FOUND
            })
        }
        Utility.log('onSuccessLeadCount===> ', JSON.stringify(response))
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ screenState: ScreenStates.SERVER_ERROR, isLoading: false, refreshing: false })
        Utility.log('onFailureLeadCount===> ', JSON.stringify(response))
    }

    onBackPress = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.LEAD_DASHBOARD }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LOAN}
                        onRetry={this.onRetryClick}>

                        <LoanLeadScreen
                            data={this.state.data}
                            onRefresh={this.onRefresh}
                            refreshing={this.state.refreshing}
                            isLoading={this.state.isLoading}
                            onItemClick={this.onItemClick}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})