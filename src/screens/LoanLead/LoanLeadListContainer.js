import React, { Component } from 'react'
import LoanLeadListScreen from './LoanLeadListScreen'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../values/Styles'
import { ScreenStates, ScreenName } from '../../util/Constants'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import ActionBarSearchComponent from "../../granulars/ActionBarSearchComponent"

export default class LoanLeadListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leadsData: [],
            filterData: undefined,
            refreshing: false,
            showSearchBar: false,
            page: 1,
            hasNext: false,
            isApiCalling: false,
            isLoading: false,
            screenState: ScreenStates.IS_LOADING,
            totalRecords : 0,
            isFilterSet : false,
            subStatusIds : this.props.navigation.state.params.selectedItem.sub_status_id && this.props.navigation.state.params.selectedItem.sub_status_id != "" ? this.props.navigation.state.params.selectedItem.sub_status_id : []
        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_LEAD_LIST_SCREEN)
        this.getLoanLeadData('', true)
    }

    onRetryClick = () => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1, showSearchBar: false }, () => {
            this.getLoanLeadData('', true)
            
        })
    }

    handleLoadMore = () => {
        if (!this.state.refreshing && this.state.hasNext && !this.state.isApiCalling) {
            let nextPage = this.state.page + 1
            this.setState({ refreshing: true, page: nextPage }, () => {
                this.getLoanLeadData('', false)
                
            })
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true, page: 1, showSearchBar: false }, () => {
            this.getLoanLeadData('', true)
        })
    }

    onSubmitEditing = (text) => {
        this.setState({ refreshing: false,screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
            this.getLoanLeadData(text, true)
        })
    }

    onSearchTextChange = (text) => {
        // this.setState({ screenState: ScreenStates.IS_LOADING }, () => {
        //     this.getLoanLeadData(text, true, '')
        // })
    }

    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
    }

    onSearch = () => {
        Utility.sendEvent(AnalyticsConstants.LOAN_LIST_SEARCH_CLICK)
        this.setState({ showSearchBar: true })
    }

    getLoanLeadData = async (text, isRefresh) => {
        let statusId = []
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID, Constants.USER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])
        Utility.log('USER_ID===>' + values[1][1])

        statusId.push(this.props.navigation.state.params.selectedItem.status_id)
        
        if (isRefresh)
            this.setState({ isApiCalling: true, leadsData: [] })
        else
            this.setState({ isApiCalling: true })

        try {
            APICalls.getLoanLeadListData(values[1][1], [values[0][1]], statusId, this.state.page, this.state.subStatusIds, text, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = async (response) => {
        if (response && response.data && response.data.lead_info && response.data.lead_info.length > 0) {
            this.setState({
                refreshing: false,
                isApiCalling: false,
                hasNext: response.nextpage,
                isLoading: false,
                screenState: ScreenStates.NO_ERROR,
                leadsData: [...this.state.leadsData, ...response.data.lead_info],
                totalRecords : response.totalrecords
            })
        }
        else if (this.state.page > 1) {
            this.setState({
                isLoading: false, refreshing: false,
                isApiCalling: false, hasNext: false,
                screenState: ScreenStates.NO_ERROR,
                totalRecords : 0,
            })
        }
        else {
            this.setState({
                isLoading: false, refreshing: false,
                isApiCalling: false, hasNext: false,
                screenState: ScreenStates.NO_DATA_FOUND,
                totalRecords : 0,
            })
        }

        if (response.data.sub_status.length > 0) {
            // for (var index in response.data.sub_status) {
            //     var itemTemp = response.data.sub_status[index]
            //     itemTemp['isSelected'] = itemTemp['isSelected'] ? true : false
            //     response.data.sub_status[index] = itemTemp
            // }
            let subStatusData = response.data.sub_status
            subStatusData.map((item) => {
                if(item.isSelected){
                    item.isSelected = true
                }
                else{
                    item.isSelected = false
                }
                return item
            });
            //this.setState({ filterData: this.state.filterData == undefined ? response.data.sub_status : this.state.filterData })
            this.setState({ filterData: subStatusData })
        }
        else{
            this.setState({ filterData: undefined })
        }

        Utility.log('onSuccessLeadListing===> ', JSON.stringify(response.data.lead_info))
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({
            isLoading: false,
            hasNext: false,
            refreshing: false,
            isApiCalling: false,
            screenState: ScreenStates.SERVER_ERROR,
            totalRecords : 0,
        })
        Utility.log('onFailureLeadListing===> ', JSON.stringify(response))
    }

    onItemClick = (item) => {
        //TODO:
    }

    onViewDocument = (item) => {
        // Go To Image Preview
        let images = []
        if (item.documents.length > 0) {
            Utility.sendEvent(AnalyticsConstants.LOAN_VIEW_DOCUMENTS_CLICK,{loan_id: item.id})
            for (var index in item.documents) {
                images.push(item.documents[index].doc_aws_path)
            }
            this.props.navigation.navigate("imageReviewScreen", {
                data: images,
                index: 0
            })
        }
    }

    onCallPress = (item) => {
        Utility.sendEvent(AnalyticsConstants.LOAN_CALL_CLICK,{loan_id: item.id})
        Utility.callNumber(item.customer.phone)
    }

    onHistoryPress = (item) => {
        // Utility.log('item==>', JSON.stringify(item))
        this.props.navigation.navigate('loanLeadHistory', {
            item: item
        })
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_LOAN_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    }

    onFilterPress = () => {
        if(this.state.filterData && this.state.filterData.length != 0){
            Utility.sendEvent(AnalyticsConstants.LOAN_LIST_FILTER_CLICK)
            this.props.navigation.navigate('loanFilter', {
                filterData: this.state.filterData,
                filterCallback: this.filterCallback
            })
        }
        else{
            Utility.showToast(Strings.FILTER_DATA_NOT_AVAILABLE)
        }
    }

    filterCallback = (data) => {
        Utility.log('filteredData==>', data)
        var selectedIds = []
        for (var index in data) {
            if (data[index]['isSelected']) {
                selectedIds.push(data[index]['sub_status_id'])
            }
        }

        Utility.log('selectedIds==>', selectedIds)
        if(selectedIds.length > 0){
            this.setState({ isFilterSet : true,subStatusIds : selectedIds, screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
                this.getLoanLeadData('', true)
            })
            
        }
        else{
            this.setState({ isFilterSet : false,subStatusIds : [], screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
                this.getLoanLeadData('', true)
            })
        }
        

    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        let title
        if(this.state.totalRecords > 0)
            title = this.props.navigation.state.params.selectedItem.name + ' (' + this.state.totalRecords + ')'
        else
            title = this.props.navigation.state.params.selectedItem.name
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: title }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: this.state.filterData == undefined ? undefined : ImageAssets.icon_filter,
                            onPress: () => this.onFilterPress(),
                            badge: this.state.filterData == undefined ? undefined : this.state.subStatusIds.length
                        }, {
                            image: ImageAssets.icon_search,
                            onPress: () => this.onSearch()
                        }]}
                    />

                    {this.state.showSearchBar ? this.openActionSearchBar() : null}

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LOAN}
                        onRetry={this.onRetryClick}>
                        <LoanLeadListScreen
                            isLoading={this.state.isLoading}
                            leadsData={this.state.leadsData}
                            refreshing={this.state.refreshing}
                            onItemPress={this.onItemClick}
                            onRefresh={this.onRefresh}
                            handleLoadMore={this.handleLoadMore}
                            onViewDocument={this.onViewDocument}
                            onCallPress={this.onCallPress}
                            isApiCalling = {this.state.isApiCalling}
                            totalRecords = {this.state.totalRecords}
                            onHistoryPress={this.onHistoryPress}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})