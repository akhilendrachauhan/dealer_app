import React, { Component } from 'react'
import LoanLeadHistoryScreen from './LoanLeadHistoryScreen'
import { View, SafeAreaView, StyleSheet } from 'react-native'
import { Utility, AnalyticsConstants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import * as APICalls from '../../api/APICalls'
import { _ActionBarStyle } from '../../values/Styles'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { ScreenStates, ScreenName } from '../../util/Constants'

export default class LoanLeadHistoryContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            screenState: ScreenStates.IS_LOADING,
            isLoading: false,
            historyData: []
        }
    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_LEAD_HISTORY_SCREEN)
        this.getLoanLeadTimeLine()
    }

    onRetryClick = () => {
        this.getLoanLeadTimeLine()
    }

    getLoanLeadTimeLine = () => {
        let leadId = this.props.navigation.state.params.item.id
        let customerId = this.props.navigation.state.params.item.customer ? this.props.navigation.state.params.item.customer.id : ''
        let dealerIdHash = this.props.navigation.state.params.item.dealerinfo ? this.props.navigation.state.params.item.dealerinfo.dealer_id_hash : ''

        try {
            APICalls.getLoanLeadTimeLine(leadId, customerId, dealerIdHash, this.onSuccessTimeline, this.onFailureTimeline, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccessTimeline = (response) => {
        Utility.log('onSuccessTimeline==>', JSON.stringify(response))
        if (response.data && response.data.timelines.length > 0) {
            this.setState({ screenState: ScreenStates.NO_ERROR, historyData: response.data.timelines })
        }
        else {
            this.setState({ screenState: ScreenStates.NO_DATA_FOUND })
        }
    }

    onFailureTimeline = (response) => {
        this.setState({ screenState: ScreenStates.SERVER_ERROR })
        Utility.log('onFailureTimeline==>', JSON.stringify(response))
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: Strings.HISTORY }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                    />

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LOAN}
                        onRetry={this.onRetryClick}>

                        <LoanLeadHistoryScreen
                            isLoading={this.state.isLoading}
                            historyData={this.state.historyData}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})