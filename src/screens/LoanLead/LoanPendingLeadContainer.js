import React, { Component } from 'react'
import LoanPendingLeadScreen from './LoanPendingLeadScreen'
import { View, SafeAreaView, StyleSheet, Platform } from 'react-native'
import { Utility, Constants, AnalyticsConstants } from '../../util'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors } from '../../values'
import * as APICalls from '../../api/APICalls'
import AsyncStore from '../../util/AsyncStore'
const WithLoading = ScreenHoc.WithLoading(View)
import * as ScreenHoc from './../../hoc/ScreenHOC'
import { _ActionBarStyle } from '../../values/Styles'
import { ScreenStates, ScreenName } from '../../util/Constants'
import ActionBarWrapper from "../../granulars/ActionBarWrapper"
import ActionBarSearchComponent from "../../granulars/ActionBarSearchComponent"
import DBFunctions from "../../database/DBFunctions";
import DocumentImageUploadFile from '../../util/DocumentImageUploadFile'
import { EventRegister } from '../../util/EventRegister'
import AndroidNativeModules from '../../util/AndroidNativeModules'

export default class LoanPendingLeadContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leadsData: [],
            refreshing: false,
            showSearchBar: false,
            page: 1,
            hasNext: false,
            isApiCalling: false,
            isLoading: false,
            screenState: ScreenStates.IS_LOADING,
            leadId: '',
            documentList: '',
            financierList: '',
            financier_id: '',
            totalRecords: 0,
            item : null
        }

    }

    componentDidMount() {
        Utility.sendCurrentScreen(AnalyticsConstants.LOAN_PENDING_SCREEN)
        EventRegister.addEventListener(Constants.LOAN_PENDING_SYNC, (data) => {
            this.getLoanLeadData('', true)
        })
        this.getLoanLeadData('', true)
    }

    onRetryClick = () => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1, showSearchBar: false }, () => {
            this.getLoanLeadData('', true)
        })
    }

    handleLoadMore = () => {
        if (this.state.hasNext && !this.state.isApiCalling) {
            let nextPage = this.state.page + 1
            this.setState({ refreshing: true, page: nextPage, isApiCalling: true }, () => {
                this.getLoanLeadData('', false)
            })
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true, page: 1, showSearchBar: false }, () => {
            this.getLoanLeadData('', true)
        })
    }

    onSubmitEditing = (text) => {
        this.setState({ screenState: ScreenStates.IS_LOADING, page: 1 }, () => {
            this.getLoanLeadData(text, true)
        })
    }

    onSearchTextChange = (text) => {
        // this.setState({ screenState: ScreenStates.IS_LOADING }, () => {
        //     this.getLoanLeadData(text, true)
        // })
    }

    onSearchBackPress = () => {
        this.setState({ showSearchBar: false })
    }

    onSearch = () => {
        Utility.sendEvent(AnalyticsConstants.LOAN_PENDING_SEARCH_CLICK)
        this.setState({ showSearchBar: true })
    }

    getLoanLeadData = async (text, isRefresh) => {
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.DEALER_ID, Constants.USER_ID])
        Utility.log('DEALER_ID===>' + values[0][1])
        Utility.log('USER_ID===>' + values[1][1])
        // Loan Status and subStatus are fixed for Pending cases
        let loanStatus = 1;
        let subStatus = [1];

        if (isRefresh)
            this.setState({ isApiCalling: true, leadsData: [] })
        else
            this.setState({ isApiCalling: true })

        try {
            APICalls.getLoanLeadListData(values[1][1], [values[0][1]], loanStatus, this.state.page, subStatus, text, this.onSuccess, this.onFailure, this.props)
        } catch (error) {
            Utility.log(error)
        }
    }

    onSuccess = async (response) => {
        let leadData = []
        if (response && response.data && response.data.lead_info && response.data.lead_info.length > 0) {
            this.setState({
                refreshing: false,
                isApiCalling: false,
                hasNext: response.nextpage,
                isLoading: false,
                totalRecords: response.totalrecords,
                screenState: ScreenStates.NO_ERROR,
                leadsData: [...this.state.leadsData, ...response.data.lead_info]
            })
            // for (var index in response.data.lead_info) {
            //     itemTemp = response.data.lead_info[index]
            //     if (itemTemp.is_pending) {
            //         leadData.push(itemTemp)
            //     }
            // }
            // this.setState({ leadsData: [...this.state.leadsData, ...leadData] }, () => {
            //     if (!this.state.leadsData.length > 0) {
            //         this.setState({ screenState: ScreenStates.NO_DATA_FOUND })
            //     }
            // })
        }
        else {
            this.setState({
                isLoading: false, refreshing: false,
                isApiCalling: false, hasNext: false,
                totalRecords: 0,
                screenState: ScreenStates.NO_DATA_FOUND
            })
        }
        Utility.log('onSuccessLeadListing===> ', JSON.stringify(response.data.lead_info))
    }

    onFailure = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({
            isLoading: false,
            hasNext: false,
            refreshing: false,
            isApiCalling: false,
            totalRecords: 0,
            screenState: ScreenStates.SERVER_ERROR
        })
        Utility.log('onFailureLeadListing===> ', JSON.stringify(response))
    }

    onItemClick = (item) => {
        //TODO:
    }

    onCallPress = (item) => {
        Utility.sendEvent(AnalyticsConstants.LOAN_PENDING_CALL_CLICK)
        Utility.callNumber(item.customer.phone)
    }

    onHistoryPress = (item) => {
        // Utility.log('item==>', JSON.stringify(item))
        this.props.navigation.navigate('loanLeadHistory', {
            item: item
        })
    }

    onViewDocument = (item) => {
        Utility.sendEvent(AnalyticsConstants.LOAN_PENDING_UPLOAD_DOCUMENTS_CLICK)
        let financier_id = item.financier_id
        let leadId = item.id
        let dbFunctions = new DBFunctions()
        let imageUploadFile = new DocumentImageUploadFile()
        this.setState({ leadId: leadId, financier_id: financier_id, item : item }, () => {
            dbFunctions.getAllImagesByLeadId(leadId,Constants.NOT_CALCULATOR_IMAGE).then(result => {
                //alert(JSON.stringify(result))
                if (result.length > 0) {
                    imageUploadFile.checkAndUpload(leadId)
                    Utility.showToast(Strings.PENDING_LOAN_SUBMIT_MESSAGE)
                }
                else {
                    this.getLoanConfig()
                }
            })
        })
    }

    getLoanConfig = () => {
        try {
            let keys = [Constants.SFA_USER_DATA]
            const promise = Utility.getMultipleValuesFromAsyncStorage(keys)
            promise.then(values => {
                let sfa = values[0][1] ? JSON.parse(values[0][1]) : null
                APICalls.getLoanConfig(sfa, this.onSuccessLoanConfig, this.onFailureLoanConfig, this.props)
            })
        } catch (error) {
            Utility.log(error)
        }
        this.setState({ isLoading: true })
    }

    onSuccessLoanConfig = (response) => {
        if (response && response.data) {
            this.setState({
                isLoading: false,
                documentList: response.data.document_list,
                financierList: response.data.financier_list
            }, () => {
                this.goToUploadDocument()
            })
            Utility.log("onSuccessLoanConfig==> " + JSON.stringify(response))
        }
        else
            this.setState({ isLoading: false })
    }

    onFailureLoanConfig = (response) => {
        if (response && response.message)
            Utility.showToast(response.message)

        this.setState({ isLoading: false })
        Utility.log("onFailureLoanConfig==> " + JSON.stringify(response))
    }

    goToUploadDocument = () => {
        var financier = this.state.financierList.filter(obj => {
            return obj.id === this.state.financier_id
        })

        let check_for_mrp = false
        if(this.state.item && this.state.item.has_rule_engine && this.state.item.is_mrp && this.state.item.has_rule_engine == "1" && this.state.item.is_mrp == "0"){
            check_for_mrp = true
        }
        if(financier && financier[0]){
            this.props.navigation.navigate("attachments", {
                document_list: this.state.documentList,
                financier: financier[0],
                leadId: this.state.leadId,
                isGoBack: true,
                check_for_mrp : check_for_mrp
            })
        }
        else{
            Utility.showToast(Strings.FINANCIER_NOT_ACTIVE)
        }
    }

    openActionSearchBar = () => {
        return (
            <ActionBarSearchComponent
                text={Strings.SEARCH_ON_LEAD_LISTING}
                onSubmitEditing={this.onSubmitEditing}
                OnSearchTextChange={this.onSearchTextChange}
                onSearchBackPress={this.onSearchBackPress}
                tintColor={Colors.BLACK}
            />
        )
    }

    onBackPress = () => {
        if (this.props.navigation.state.params && this.props.navigation.state.params.initialRoute) {
            if (Platform.OS !== 'ios') {
                AndroidNativeModules.exitApp();
            } else {
                this.props.navigation.goBack();
            }
        } else {
            this.props.navigation.goBack()
        }
    }

    render() {
        let title
        if (this.state.totalRecords > 0)
            title = Strings.CASE_LIST + ' (' + this.state.totalRecords + ')'
        else
            title = Strings.CASE_LIST
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.PRIMARY_DARK }}>
                <View style={Styles.container}>
                    <ActionBarWrapper
                        values={{ title: title }}
                        actions={{ onLeftPress: () => this.onBackPress() }}
                        styleAttributes={{
                            leftIconImage: ImageAssets.icon_back_arrow,
                            disableShadows: true,
                            tintColor: Colors.PRIMARY_DARK,
                            containerStyle: [_ActionBarStyle.content, { backgroundColor: Colors.PRIMARY }]
                        }}
                        iconMap={[{
                            image: ImageAssets.icon_search,
                            onPress: () => this.onSearch()
                        }]}
                    />

                    {this.state.showSearchBar ? this.openActionSearchBar() : null}

                    <WithLoading
                        screenState={this.state.screenState}
                        screenName={ScreenName.SCREEN_LOAN}
                        onRetry={this.onRetryClick}>

                        <LoanPendingLeadScreen
                            isLoading={this.state.isLoading}
                            leadsData={this.state.leadsData}
                            refreshing={this.state.refreshing}
                            onItemPress={this.onItemClick}
                            onRefresh={this.onRefresh}
                            handleLoadMore={this.handleLoadMore}
                            onViewDocument={this.onViewDocument}
                            onCallPress={this.onCallPress}
                            onHistoryPress={this.onHistoryPress}
                        />
                    </WithLoading>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    }
})
