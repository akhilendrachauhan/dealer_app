import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import LoadingComponent from "../../granulars/LoadingComponent"

export default class LoanLeadScreen extends Component {

    constructor(props) {
        super(props);
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.props.onItemClick(item)}>

                <View style={{ flexDirection: 'row', height: 92, alignItems: 'center', borderRadius: 5, margin: Dimens.margin_small, backgroundColor: Colors.WHITE }}>

                    <View style={{ flex: 0.7, paddingLeft: 16 }}>
                        <Text style={Styles.headText}>{item.name}</Text>
                        {/* <Text style={Styles.descText}>{item.name}</Text> */}
                    </View>

                    <View style={Styles.dashedView} />

                    <View style={{ flex: 0.3, alignItems: 'center' }}>
                        <Text style={[Styles.countText, { color: Colors.PRIMARY }]}>{item.LeadCount}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <FlatList
                            style={{ marginHorizontal: Dimens.margin_small }}
                            ref={(ref) => { this.flatListRef = ref; }}
                            extraData={this.props}
                            data={this.props.data}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => '' + index}
                            showsVerticalScrollIndicator={false}
                            onRefresh={()=> this.props.onRefresh()}
                            refreshing={this.props.refreshing}
                        />

                        {this.props.isLoading ? <LoadingComponent msg={this.props.loadingMsg} /> : null}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background_blue
    },
    headText: {
        fontSize: Dimens.text_x_large,
        color: Colors.BLACK_85,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    countText: {
        fontSize: Dimens.text_40,
        fontFamily: Strings.APP_FONT
    },
    containerMenu: {
        width: 140,
        top: -10,
        right: 8,
        elevation: 5,
        borderRadius: 2,
        position: 'absolute',
        padding: Dimens.padding_normal,
        backgroundColor: Colors.GRAY_LIGHT_BG,
    },
    menuItemText: {
        color: Colors.BLACK,
        fontFamily: Strings.APP_FONT,
        fontSize: Dimens.text_large,
        marginTop: Dimens.margin_small
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    },
});
