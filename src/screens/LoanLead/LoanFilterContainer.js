import React, { Component } from 'react'
import LoanFilterScreen from './LoanFilterScreen'
import { SafeAreaView, StyleSheet } from 'react-native'
import { Strings, Colors } from '../../values'
import { Utility, AnalyticsConstants } from '../../util'

export default class LoanFilterContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            filterData: JSON.parse(JSON.stringify(this.props.navigation.state.params.filterData)) || [],
            isLoading: false,
        }
    }

    onItemClick = (item, index) => {
        let data = this.state.filterData
        data[index]['isSelected'] = !data[index]['isSelected']
        this.setState({ filterData: data })
    }

    onResetPress = () => {
        let data = this.state.filterData
        for (var index in data) {
            var itemTemp = data[index]
            itemTemp['isSelected'] = false

            data[index] = itemTemp
        }

        this.setState({ filterData: data })
    }

    onSubmitClick = () => {
        Utility.sendEvent(AnalyticsConstants.LOAN_FILTER_SUBMIT_CLICK)
        this.props.navigation.state.params.filterCallback(this.state.filterData)
        this.props.navigation.goBack()
    }

    onBackPress = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={Styles.container}>
                <LoanFilterScreen
                    isLoading={this.state.isLoading}
                    filterData={this.state.filterData}
                    onResetPress={this.onResetPress}
                    onBackPress={this.onBackPress}
                    onItemClick={this.onItemClick}
                    onSubmitClick={this.onSubmitClick}
                />
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.PRIMARY_DARK
    }
});