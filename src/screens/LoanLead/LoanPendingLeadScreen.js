import React, { Component } from 'react';
import { StyleSheet, View, FlatList } from 'react-native'
import { Strings, Colors, Dimens } from '../../values'
import LoadingComponent from "../../granulars/LoadingComponent"
import LoanLeadTupleComponent from "../../components/loan/LoanLeadTupleComponent"

export default class LoanPendingLeadScreen extends Component {

    constructor(props) {
        super(props)
    }

    renderItem = ({ item, index }) => {
        return (
            <LoanLeadTupleComponent
                item={item}
                index={index}
                callback={this.props.onItemPress}
                onViewDocument={this.props.onViewDocument}
                onCallPress={this.props.onCallPress}
                onHistoryPress={this.props.onHistoryPress}
                isPending={true}
            />
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <FlatList
                    style={{ marginHorizontal: Dimens.margin_small }}
                    ref={(ref) => { this.flatListRef = ref }}
                    extraData={this.props}
                    data={this.props.leadsData}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => '' + index}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.props.onRefresh}
                    refreshing={this.props.refreshing}
                    onEndReachedThreshold={0.2}
                    onEndReached={this.props.handleLoadMore}
                />

                {this.props.isLoading && <LoadingComponent backgroundColor={Colors.transparent} />}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background_blue
    }
})
