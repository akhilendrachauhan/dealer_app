import React, { Component } from "react";
import { Platform, StyleSheet } from "react-native";
import CardView from "react-native-cardview";



const CardViewHOC = (WrappedComponent) => {

    return class CardViewHOC extends Component {

        static defaultProps = {
            flex: 1
        }

        render() {
            return (
                <CardView style={styles.container}>
                    <WrappedComponent {...this.props} />
                </CardView>
            );
        }
    }
}

const styles = StyleSheet.create({

    container: {
        marginLeft: (Platform.OS === 'ios') ? 8 : 5,
        marginRight: (Platform.OS === 'ios') ? 8 : 5,
        marginTop: (Platform.OS === 'ios') ? 5 : 0,
        marginBottom: (Platform.OS === 'ios') ? 5 : 0,
        paddingRight: (Platform.OS === 'ios') ? 5 : 0,
        paddingBottom: (Platform.OS === 'ios') ? 0 : 10,
    },
});

export default CardViewHOC;
