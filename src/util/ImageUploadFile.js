import Upload from "react-native-background-upload";
import * as Utility from "./Utility";
import * as APIConstant from "../api/APIConstants";
import {Strings} from "../values";
import DBFunctions from "../database/DBFunctions";
import * as APICalls from "../api/APICalls";
import * as DBConstants from "../database/DBConstants";
import * as RNFS from "react-native-fs";
import * as Constants from '../util/Constants'
import {Platform} from 'react-native'
import {showNotification} from './FirebaseListeners'
import ImageResizer from 'react-native-image-resizer';
import { EventRegister } from '../util/EventRegister'
let carId;
let dbFunctions;
export default class ImageUploadFile {
    constructor() {
        this.carId = 0;
        this.totalImages = 0;
        this.uploadCount = 1;
        dbFunctions = new DBFunctions()
    }

    async getFields(object) {
        Utility.log("File Data" + JSON.stringify(object[DBConstants.IMAGE_PATH]));
       
        let imageName = (new Date).getTime() + ".jpg";
        
        carId = object.carId;
        let value = {
            upload_type : 'inventory_car_images'
        };
        let fPath = object[DBConstants.IMAGE_PATH]
        if (Platform.OS == "android") {
            fPath = fPath.replace("file://", "");
        }
       return params = {
            url: APIConstant.UPLOAD_TO_S3,
            path: fPath,
            method: 'POST',
            type: 'multipart',
            field: 'images',
            parameters: value,
            headers: {
                'content-type': 'multipart/form-data'
            },
            notification: {
                enabled: true,
                notificationChannel: 'classified-notification'
            }
        };
    }

    async uploadImage(object) {

        Utility.getNetInfo().then(async isConnected => {
            if (!isConnected) {
                Utility.showNoInternetDialog();
                return;
            }
        let params = await this.getFields(object);
        Upload.startUpload(params).then((uploadId) => {
            Utility.log('Upload started');
            Upload.addListener('progress', uploadId, (data) => {
                Utility.log(`Progress: ${data.progress}%`)
            });
            Upload.addListener('error', uploadId, (data) => {
                Utility.log(`Error: ${data.error}%`);
                this.checkAndUpload(this.carId);
            });
            Upload.addListener('cancelled', uploadId, (data) => {
                Utility.log(`Cancelled!`);
                
            });
            Upload.addListener('completed', uploadId, (data) => {
                let responseValue = data["responseBody"];
                Utility.log('ResCompleted!',responseValue);
                responseValue = JSON.parse(responseValue)
                if (responseValue.status === 200) {
                    
                    Utility.log('Completed!');
                    //let dbFunctions = new DBFunctions();
                    dbFunctions.updateImage(object[DBConstants.IMAGE_ID],responseValue.data[0].file_name,responseValue.data[0].file_url,Constants.IMAGE_S3_UPLOAD_STATUS).then(result => {
                        Utility.log("updated " + JSON.stringify(result));
                        RNFS.unlink(object[DBConstants.IMAGE_PATH]).then(() => Utility.log('FILE DELETED'))
                        if(this.uploadCount < this.totalImages)
                            this.uploadCount = this.uploadCount + 1
                        this.checkAndUpload(this.carId);

                    });
                    
                } else {
                    Utility.showToast(responseValue.message)
                    this.fireNotificationForAllUpload(object[DBConstants.MAKE_MODEL],Strings.ALL_IMAGES_CAN_NOT_BE_UPLOADED)
                    //this.checkAndUpload(this.carId);
                }
            })
        }).catch((err) => {
            Utility.log('Upload error!', err)
        })
    });
    }

    async checkAndUpload(carId) {
        this.carId = carId;
        let makeModel
        Utility.log("Upload Data" + JSON.stringify(carId));
        try {
            //let dbFunctions = new DBFunctions();
            dbFunctions.getImagesByCarId(carId,Constants.IMAGE_INITIAL_STATUS).then(result => {
                Utility.log("DDDDDDDDDDDDDDDDDD" + JSON.stringify(result));
                if (result.length > 0) {
                    //let imageToBeDeleted = result[0][DBConstants.IMAGE_PATH]
                    // RNFS.exists(result[0][DBConstants.IMAGE_PATH]).then((exists) =>{
                    //     Utility.log('FILE Exists'+exists)
                    //     if(exists){
                            ImageResizer.createResizedImage(result[0][DBConstants.IMAGE_PATH], Constants.COMPRESS_WIDTH, Constants.COMPRESS_HEIGHT, 'JPEG',Constants.COMPRESS_RATIO)
                            .then(({uri}) => {
                                Utility.log("hihihihi")
                                result[0][DBConstants.IMAGE_PATH] = uri
                                //RNFS.unlink(imageToBeDeleted).then(() => Utility.log('FILE DELETED NEW'))
                               
                                if(this.totalImages == 0)
                                {
                                    this.totalImages = result.length
                                }
                                //this.uploadCount = this.uploadCount + 1
                                this.fireNotification(result[0][DBConstants.MAKE_MODEL])
                                this.uploadImage(result[0]);
                            })
                    //     }
                    //     else{
                    //         dbFunctions.updateImage(result[0][DBConstants.IMAGE_ID],result[0][DBConstants.IMAGE_NAME],result[0][DBConstants.IMAGE_PATH],Constants.IMAGE_NOT_EXISTS_STATUS).then(result => {
                    //             Utility.log("updated " + JSON.stringify(result));
                                
                    //             this.checkAndUpload(this.carId);
        
                    //         });
                    //     }

                    // })
                    
                    
                } else {
                    var images_names = [],images_urls = [], i;
                    dbFunctions.getImagesByCarId(carId,Constants.IMAGE_S3_UPLOAD_STATUS).then(result => {
                        Utility.log("DDDDDDDDDDDDDDDDDD1" + JSON.stringify(result));
                        if (result.length > 0) {
                            makeModel = result[0][DBConstants.MAKE_MODEL]
                            for(i=0; i< result.length ; i++){
                                images_names[i] = result[i][DBConstants.IMAGE_NAME]
                                images_urls[i] = result[i][DBConstants.IMAGE_PATH]
                            }
                            APICalls.sendImageToServer(this.carId,images_names,images_urls, (response) => {
                                Utility.log("Uploaded to server"+JSON.stringify(response))
                                EventRegister.emit(Constants.STOCK_LIST_SYNC, response.data)
                                dbFunctions.deleteImagesByCarId(this.carId).then(result => {
                                    Utility.log("deleted images::" + JSON.stringify(result));
                                    //Utility.retryImageUpload()
                                    if(this.uploadCount != 0)
                                        this.fireNotificationForAllUpload(makeModel,Strings.ALL_IMAGES_UPLOADED)
                                          
                                });
                            }, (response) => {
                                Utility.retryImageUpload()
                            })
                        }
                        else{
                            APICalls.deleteImageFromServer(this.carId, (response) => {
                                Utility.log("delete from server"+JSON.stringify(response))
                                EventRegister.emit(Constants.STOCK_LIST_SYNC, response.data)
                                dbFunctions.deleteImagesByCarId(this.carId).then(result => {
                                    Utility.log("deleted " + JSON.stringify(result));
                                });
                            }, (response) => {
                            })
                        }
                        
                });
                }
            });
            
        } catch (error) {
            Utility.log("==================> " + error);
        }
    }
    fireNotification(makeModel) {
        let msg = Strings.UPLOADING_IMAGE + this.uploadCount + '/' + this.totalImages;
        let notification = {
        notificationId: this.carId.toString(),
        title: makeModel,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    }  

    fireNotificationForAllUpload(makeModel,msg) {
        //let msg = Strings.ALL_IMAGES_UPLOADED;
        let notification = {
        notificationId: this.carId.toString(),
        title: makeModel,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    }  
}