import Upload from "react-native-background-upload";
import * as Utility from "./Utility";
import * as APIConstant from "../api/APIConstants";
import {Strings} from "../values";
import DBFunctions from "../database/DBFunctions";
import * as APICalls from "../api/APICalls";
import * as DBConstants from "../database/DBConstants";
import * as RNFS from "react-native-fs";
import * as Constants from '../util/Constants'
import {Platform} from 'react-native'
import {showNotification} from './FirebaseListeners'
import ImageResizer from 'react-native-image-resizer';
import { EventRegister } from '../util/EventRegister'
let leadId;
let dbFunctions;
export default class DocumentImageUploadFile {
    constructor() {
        this.leadId = 0;
        this.totalImages = 0;
        this.uploadCount = 1;
        this.calculatorImage = false;
        dbFunctions = new DBFunctions()
    }

    async getFields(object) {
        
        let imageName = (new Date).getTime() + ".jpg";
        
        leadId = object[DBConstants.LEAD_ID];
        let value = {
            'upload_type' : 'finance_docs',
            'lead_id' : this.leadId.toString()
        };
        let fPath = object[DBConstants.IMAGE_PATH]
        if (Platform.OS == "android") {
            fPath = fPath.replace("file://", "");
        }
        else{
            if(fPath.startsWith("file://"))
                fPath = fPath.replace("file://", "");
        }

        Utility.log("File Data" + fPath);
       
        
        
       return params = {
            url: APIConstant.UPLOAD_TO_S3,
            path: fPath,
            method: 'POST',
            type: 'multipart',
            field: 'images',
            parameters: value,
            headers: {
                'content-type': 'multipart/form-data'
            },
            notification: {
                enabled: true,
                notificationChannel: 'classified-notification'
            }
        };
    }

    async uploadImage(object) {

        Utility.getNetInfo().then(async isConnected => {
            if (!isConnected) {
                Utility.showNoInternetDialog();
                return;
            }
        let params = await this.getFields(object);
        Upload.startUpload(params).then((uploadId) => {
            Utility.log('Upload started');
            Upload.addListener('progress', uploadId, (data) => {
                Utility.log(`Progress:1 ${data.progress}%`)
            });
            Upload.addListener('error', uploadId, (data) => {
                Utility.log(`Error: ${data.error}%`);
                this.checkAndUpload(this.leadId,this.calculatorImage);
            });
            Upload.addListener('cancelled', uploadId, (data) => {
                Utility.log(`Cancelled!`);
                
            });
            Upload.addListener('completed', uploadId, (data) => {
                try{
                let responseValue = data["responseBody"];
                Utility.log('ResCompleted!',data["responseBody"]);
                responseValue = JSON.parse(responseValue)
                if (responseValue.status === 200) {
                    Utility.log('Completed!');
                    //let dbFunctions = new DBFunctions();
                    dbFunctions.updateDocumentImage(object[DBConstants.IMAGE_ID],responseValue.data[0].file_name,responseValue.data[0].file_url,Constants.IMAGE_S3_UPLOAD_STATUS).then(result => {
                        Utility.log("updated " + JSON.stringify(result));
                        //RNFS.unlink(object[DBConstants.IMAGE_PATH]).then(() => Utility.log('FILE DELETED'))
                        // APICalls.sendImageToServer(this.leadId,responseValue.data[0].file_name,responseValue.data[0].file_url,object[DBConstants.SEQUENCE]+1, (response) => {
                        //     Utility.log("Uploaded to server"+JSON.stringify(response))
                        //     RNFS.unlink(object[DBConstants.IMAGE_PATH]).then(() => Utility.log('FILE DELETED'))
                        // }, (response) => {
                        // })
                        if(this.uploadCount < this.totalImages)
                            this.uploadCount = this.uploadCount + 1
                        this.checkAndUpload(this.leadId,this.calculatorImage);

                    });
                    
                } else {
                    Utility.showToast(responseValue.message)
                    this.fireNotificationForAllUpload(Strings.DOCUMENTS_CAN_NOT_BE_UPLOADED)
                    //this.checkAndUpload(this.leadId);
                }
            } catch (error) {
                Utility.log("==================> " + error);
                dbFunctions.updateDocumentImage(object[DBConstants.IMAGE_ID],object[DBConstants.IMAGE_NAME],object[DBConstants.IMAGE_PATH],Constants.IMAGE_NOT_EXISTS_STATUS).then(result => {
                    this.checkAndUpload(this.leadId,this.calculatorImage);

                });
            }
            })
        }).catch((err) => {
            Utility.log('Upload error!', err)
        })
    
    });
    }

    async checkAndUpload(leadId,calculatorImage) {
        this.leadId = leadId;
        this.calculatorImage = calculatorImage ? calculatorImage : false
        Utility.log("Upload Data" + JSON.stringify(leadId));
        try {
            dbFunctions.getImagesByLeadId(leadId,Constants.IMAGE_INITIAL_STATUS).then(result => {
                Utility.log("DDDDDDDDDDDDDDDDDD" + JSON.stringify(result));
                if (result.length > 0) {
                    // RNFS.exists(result[0][DBConstants.IMAGE_PATH]).then((exists) =>{
                    //     Utility.log('FILE Exists'+exists)
                    //     if(exists){
                            if(Utility.checkFileType(result[0][DBConstants.IMAGE_PATH],Constants.PDF_FILE_TYPE)){
                                if(this.totalImages == 0)
                                {
                                    this.totalImages = result.length
                                }
                                if(this.totalImages == 1 && result[0][DBConstants.IS_CALCULATOR_IMAGE] == "1"){
                                    this.calculatorImage = true
                                }
                                //this.uploadCount = this.uploadCount + 1
                                this.fireNotification()
                                this.uploadImage(result[0]);
                            }else{
                                ImageResizer.createResizedImage(result[0][DBConstants.IMAGE_PATH], Constants.COMPRESS_WIDTH, Constants.COMPRESS_HEIGHT, 'JPEG',Constants.COMPRESS_RATIO)
                                .then(({uri}) => {
                                    Utility.log("hihihihi")
                                    result[0][DBConstants.IMAGE_PATH] = uri
                                    if(this.totalImages == 0)
                                    {
                                        this.totalImages = result.length
                                    }
                                    if(this.totalImages == 1 && result[0][DBConstants.IS_CALCULATOR_IMAGE] == "1"){
                                        this.calculatorImage = true
                                    }
                                    //this.uploadCount = this.uploadCount + 1
                                    if(this.calculatorImage)
                                        this.fireCalculatorNotification(Strings.PLEASE_WAIT_UPLOADING)
                                    else
                                        this.fireNotification()
                                    this.uploadImage(result[0]);
                                })
                            }
                    //     }
                    //     else{
                    //         dbFunctions.updateDocumentImage(result[0][DBConstants.IMAGE_ID],result[0][DBConstants.IMAGE_NAME],result[0][DBConstants.IMAGE_PATH],Constants.IMAGE_NOT_EXISTS_STATUS).then(result => {
                    //             Utility.log("updated " + JSON.stringify(result));
                                
                    //             this.checkAndUpload(this.leadId);
        
                    //         });
                    //     }
                    // })
                    
                } else {
                    dbFunctions.getImagesByLeadId(leadId,Constants.IMAGE_S3_UPLOAD_STATUS).then(result => {
                        Utility.log("DDDDDDDDDDDDDDDDDD1" + JSON.stringify(result));
                        if (result.length > 0) {
                            if(result.length == 1 && result[0][DBConstants.IS_CALCULATOR_IMAGE] == "1"){
                                this.calculatorImage = true
                            }
                            var imageObject,imagesObjectArray = [], i,dealer_id_hash ;
                            for(i=0; i< result.length ; i++){
                                imageObject = { 
                                    finance_lead_id : leadId,
                                    doc_id : result[i][DBConstants.DOCUMENT_ID],
                                    parent_id : result[i][DBConstants.DOCUMENT_PARENT_ID] ? result[i][DBConstants.DOCUMENT_PARENT_ID] : '0',
                                    doc_path : result[i][DBConstants.IMAGE_NAME],
                                    doc_aws_path : result[i][DBConstants.IMAGE_PATH],
                                }

                                imagesObjectArray.push(imageObject)
                                  
                            }

                            Utility.log("imageObjects::"+JSON.stringify(imagesObjectArray))
                            // dbFunctions.deleteDocumentImagesByLeadId(this.leadId).then(result => {
                            //     Utility.log("deleted " + JSON.stringify(result));
                            // });
                            let keys = [Constants.DEALER_ID_HASH,Constants.USER_ID,Constants.SFA_USER_DATA];
                            const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
                            promise.then(values => {
                                let sfa = values[1][1] ? JSON.parse(values[2][1]) : null
                                dealer_id_hash = result[0][DBConstants.DEALER_ID_HASH] ? result[0][DBConstants.DEALER_ID_HASH] : values[0][1]
                                APICalls.saveLoanDoc(dealer_id_hash,values[1][1],sfa,this.leadId, imagesObjectArray,this.calculatorImage, (response) => {
                                    Utility.log("Uploaded to server"+JSON.stringify(response))
                                    EventRegister.emit(Constants.LOAN_PENDING_SYNC, response.data)
                                    dbFunctions.deleteDocumentImagesByLeadId(this.leadId).then(result => {
                                        Utility.log("deleted images" + JSON.stringify(result)+ ":"+this.calculatorImage);
                                            if(this.uploadCount != 0)
                                                if(this.calculatorImage)
                                                    this.fireCalculatorNotification(Strings.UPLOADING_COMPLETED)
                                                else
                                                    this.fireNotificationForAllUpload(Strings.ALL_DOCUMENT_UPLOADED)
                                    });
                                }, (response) => {
                                    //Utility.retryLoanDocumentImageUpload()
                                    Utility.showToast(response.message)
                                })
                            });
                            
                        }
                    });
                }
            });
            
        } catch (error) {
            Utility.log("==================> " + error);
        }
    }
   
    fireNotification(makeModel) {
        let msg = Strings.UPLOADING_DOCUMENT + this.uploadCount + '/' + this.totalImages;
        let notification = {
        notificationId: this.leadId.toString(),
        title: Strings.LOAN_LEAD,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    }  

    fireNotificationForAllUpload(msg) {
        //let msg = Strings.ALL_DOCUMENT_UPLOADED;
        let notification = {
        notificationId: this.leadId.toString(),
        title: Strings.LOAN_LEAD,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    } 
    
    fireCalculatorNotification(msg) {
        
        let notification = {
        notificationId: this.leadId.toString(),
        title: Strings.UPLOAD_CALCULATOR_IMAGE,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    }  
}