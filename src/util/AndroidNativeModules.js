'use strict';
import {NativeModules} from 'react-native';
const AndroidNativeModule = NativeModules.AndroidBridge;

function checkIfInitialized() {
    return AndroidNativeModule != null;
}

export default class NativeModuleAndroid {
    static exitApp(){
        if(!checkIfInitialized()) return;
        AndroidNativeModule.exitApp()
    }
}
