import NetInfo from "@react-native-community/netinfo"
import AsyncStorage from '@react-native-community/async-storage'
import ImageResizer from 'react-native-image-resizer';
import React from 'react';
import {
    Alert,
    Linking,
    Platform,
    Text
} from 'react-native';
import * as APIConstants from '../api/APIConstants';
import Toast from 'react-native-simple-toast'
import * as APICalls from "../api/APICalls";
import DBFunctions from "../database/DBFunctions";
import DateFormat from "../util/DateFormat"
import NumberFormat from 'react-number-format'
import { Strings, Colors, Dimens } from '../values'
import RNFetchBlob from 'rn-fetch-blob'
import firebase from 'react-native-firebase';
import DocumentPicker from 'react-native-document-picker';
import * as RNFS from 'react-native-fs';
import Config from 'react-native-config'
import FilePickerManager from 'react-native-file-picker';
import { Constants, Utility, AnalyticsConstants } from ".";
import AsyncStore from '../util/AsyncStore'
import DeviceInfo from 'react-native-device-info';
import DocumentImageUploadFile from "./DocumentImageUploadFile";
import ImageUploadFile from "./ImageUploadFile"
import * as DBConstants from "../database/DBConstants";
export async function getNetInfo() {
    // if (Platform.OS === "ios") {
    //     try {
    //         const res = await fetch("https://www.google.com");
    //         if (res.status === 200) {
    //             return true;
    //         }
    //     } catch (e) {
    //         return false;
    //     }
    //     return false;
    // } else {
        // return NetInfo.isConnected.fetch().then(
        //     (isConnected) => {
        //         return isConnected;
        //     });
            return NetInfo.fetch().then(state => {
                return state.isConnected;
                
            });
    //}
}

export function setValueInAsyncStorage(key, item) {
    try {
        AsyncStorage.setItem(key, item);
    } catch (error) {
        log(error);
    }
}

export async function getValueFromAsyncStorage(key) {
    try {
        const data = await AsyncStorage.getItem(key);
        return data;
    } catch (error) {
        log(error);
    }
}

export async function getMultipleValuesFromAsyncStorage(keys) {
    log('multiGetAsyncValueInPersistStore  KEYS :' + keys);
    const values = await AsyncStorage.multiGet(keys);
    let text = 'After Text multiget :' + values;
    log(text);
    return values
}

export async function removeItemValue(key) {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch (exception) {
        return false;
    }
}


export function showNoInternetDialog() {
    Alert.alert(Strings.NO_INTERNET, Strings.NO_INTERNET_MESSAGE, [
        // {
        //     text: 'Settings',
        //     onPress: () => {
        //         openSettings();
        //     }
        // },
        {
            text: 'OK',
            onPress: () => {
            }
        }
    ], { cancelable: true });
}
export function showAlert(title,msg){
    Alert.alert(title,msg, [
        {
            text: 'OK',
            onPress: () => {
            }
        }
    ], { cancelable: true });
}


export function openSettings() {
    if (Platform.OS === 'ios') {
        Linking.canOpenURL('app-settings:').then(supported => {
            if (!supported) {
                console.warn('Can\'t handle settings url');
            } else {
                return Linking.openURL('app-settings:');
            }
        }).catch(err => console.warn('An error occurred', err));
    } else {
        DeviceSettings.open();
    }
}

export function showToast(message) {
    Toast.show(message);
}

export function log() {
    if (APIConstants.CONSOLE_ENABLED) {
        console.log(...arguments);
    }
}

export function validateEmail(value) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(value) === false) {
        return false;
    } else {
        return true;
    }
}

export function validatePhoneNumber(value) {
    let reg = /^[6-9]\d{9}$/;
    if (reg.test(value) === false) {
        return false;
    } else {
        return true;
    }
}

// Allowing only alphabet letters in TextInput
export function restrict(text) {
    text.replace(/[`~0-9!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
}

export function isValueNullOrEmpty(value) {
    // log("helloo:" + value)
    let isValue = false;
    if (value === undefined || value == null || value === '' || value === '.' || value === 'null' || value.trim().length === 0 || value == 0) {
        isValue = true;
    }
    return isValue;
}

export function openScreen(props, screen) {
    props.navigation.closeDrawer()
    props.navigation.navigate(screen)
}

export async function logout(props) {
    let keys = [Constants.USER_ID, Constants.DEALER_ID,Constants.TOTAL_FINANCIER_MMV];
    const promise = getMultipleValuesFromAsyncStorage(keys);
    promise.then(async (values) => {
        let device_id = DeviceInfo.getUniqueId()

        if (props.navigation) {
            deleteFCMToken(props, values[0][1], values[1][1], device_id)
            await AsyncStorage.clear()

            setValueInAsyncStorage(Constants.TOTAL_FINANCIER_MMV,values[2][1] ? values[2][1] : '0')
            props.navigation.replace('splash', {
                logintype: 'logout'
            })
        }
    });
}
export function deleteFCMToken(props, userId, dealerId, deviceId) {
    try {
        APICalls.deleteFCMToken(userId, dealerId, deviceId, onSuccessFCM, onFailureFCM, props)
    } catch (error) {
        Utility.log(error)
    }
}

export function onSuccessFCM(response) {
    Utility.log('onSuccessFCM===> ', JSON.stringify(response))

}

export function onFailureFCM(response) {
    Utility.log('onSuccessFCM===>1 ', JSON.stringify(response))
}

export function syncMMV() {
    let dbFunctions = new DBFunctions();
    APICalls.getMMV((response) => {
        if (response.data && response.data.version && response.data.version.length > 0) {
            dbFunctions.insertMMV(response.data.version).then((result) => {
                log('MMV insertion count', result);
            })
        }
    }, (error) => {

    });
}

export function setDateTime(date, time) {
    let dateString = date + " " + time
    // log('dateString==>', dateString)
    return dateString
}

// TODO... New date time(ISO) format.............................
// export function setDateTime(date, time) {
//     let dateString = new Date(DateFormat(date, 'mm/dd/yyyy') + ' ' + time)
//     // log('dateString==>', dateString)
//     let isoDate = new Date(dateString).toISOString()

//     return isoDate
// }

export function getDateTime(dateString) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString.replace(' ', 'T'));
        let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
        let val = new Date(date.getTime() - offset);
        // log('DateTime==>', dateString, val);

        var msDiff = new Date(date).getTime() - new Date().getTime()  //Future date - current date
        var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))

        if (totalDays == 0) {
            shownDate = DateFormat(val, 'hh:MM TT')
        }
        else if (totalDays == -1) {
            shownDate = Strings.YESTERDAY
        }
        // else if (totalDays == 1) {
        //     shownDate = Strings.TOMORROW
        // }
        else {
            shownDate = DateFormat(val, 'mmm dd, ddd')
        }

        return shownDate
        // return DateFormat(val, 'hh:MM TT');
        //return new Date(dateString.replace(' ', 'T') + '+05:30');
    } else {
        return Strings.NA
    }
}

export function getDateTimeColor(dateString) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString.replace(' ', 'T'));
        let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
        let val = new Date(date.getTime() - offset);
        // log('DateTime==>', dateString, val);

        var msDiff = new Date(date).getTime() - new Date().getTime()   //Future date - current date
        var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))

        if (totalDays < 0) {
            dateColor = Colors.BUYER_LEAD_RED
        }
        else {
            dateColor = Colors.GREEN
        }
        return dateColor
    }
    else {
        return Colors.BUYER_LEAD_RED
    }
}

export function getFormatedDateTime(dateString, newFormate) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString.replace(' ', 'T'));
        let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
        let val = new Date(date.getTime() - offset);
        // log('DateTime==>', dateString, val);

        var formatedDate = DateFormat(val, newFormate)

        return formatedDate
    }
    else {
        return Strings.NA
    }
}

export function getValidFormatedDateTime(dateString, newFormate) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString)
        var formatedDate = DateFormat(date, newFormate)
        return formatedDate
    } else {
        return Strings.NA
    }
}

// TODO... New date time(ISO) format.............................

export function getDateTimeISO(dateString) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString)
        // let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
        // let val = new Date(date.getTime() - offset);
        log('DateTime==>', date)
        let futureDate = DateFormat(date, 'mm/dd/yyyy')
        let currentDate = DateFormat(new Date(), 'mm/dd/yyyy')
        var msDiff = new Date(futureDate).getTime() - new Date(currentDate).getTime()  //Future date - current date
        var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))
        // log('totalDaysDiff==>', totalDays)

        if (totalDays == 0) {
            shownDate = DateFormat(date, 'hh:MM TT')
        }
        else if (totalDays == -1) {
            shownDate = Strings.YESTERDAY
        }
        // else if (totalDays == 1) {
        //     shownDate = Strings.TOMORROW
        // }
        else {
            shownDate = DateFormat(date, 'mmm dd, ddd')
        }

        return shownDate
        // return DateFormat(date, 'hh:MM TT');
        //return new Date(dateString.replace(' ', 'T') + '+05:30');
    } else {
        return Strings.NA
    }
}

// export function getDateTimeColor(dateString) {
//     if (dateString && dateString.length > 0) {
//         let date = new Date(dateString)
//         // let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
//         // let val = new Date(date.getTime() - offset);
//         // log('DateTime==>', date)
//         let futureDate = DateFormat(date, 'mm/dd/yyyy')
//         let currentDate = DateFormat(new Date(), 'mm/dd/yyyy')
//         var msDiff = new Date(futureDate).getTime() - new Date(currentDate).getTime()  //Future date - current date
//         var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))

//         if (totalDays < 0) {
//             dateColor = Colors.BUYER_LEAD_RED
//         }
//         else {
//             dateColor = Colors.GREEN
//         }
//         return dateColor
//     }
//     else {
//         return Colors.BUYER_LEAD_RED
//     }
// }

// export function getFormatedDateTime(dateString, newFormate) {
//     if (dateString && dateString.length > 0) {
//         let date = new Date(dateString)
//         // let offset = -1 * date.getTimezoneOffset() * 60 * 1000;
//         // let val = new Date(date.getTime() - offset);
//         // log('DateTime==>', date)

//         var formatedDate = DateFormat(date, newFormate)

//         return formatedDate
//     }
//     else {
//         return Strings.NA
//     }
// }

// export function getDateTime(date) {

//     curDate = new Date().toISOString()

//     var shownDate;
//     // DateFormat(date, 'hh:MM TT') // If date is today date
//     // Yesterday                    // If date is Yesterday
//     // DateFormat(date, 'mmm dd, ddd') // If date is Other

//     var msDiff = new Date().getTime() - new Date(date).getTime()    //Future date - current date
//     var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))

//     if (totalDays == 0) {
//     shownDate = DateFormat(date, 'hh:MM TT')
//     }
//     else if (totalDays == 1) {
//         shownDate = 'Yesterday'
//     }
//     else {
//         shownDate = DateFormat(date, 'mmm dd, ddd')
//     }
//     return shownDate
// }

// export function getDateTimeColor(date) {

//     curDate = new Date().toISOString()

//     var shownDate, dateColor;
//     // DateFormat(date, 'hh:MM TT') // If date is today date
//     // Yesterday                    // If date is Yesterday
//     // DateFormat(date, 'mmm dd, ddd') // If date is Other

//     var msDiff = new Date().getTime() - new Date(date).getTime()    //Future date - current date
//     var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))

//     if (totalDays == 0) {
//         shownDate = DateFormat(date, 'hh:MM TT')
//         dateColor = Colors.GREEN
//     }
//     else if (totalDays == 1) {
//         shownDate = 'Yesterday'
//         dateColor = Colors.BUYER_LEAD_RED
//     }
//     else {
//         shownDate = DateFormat(date, 'mmm dd, ddd')
//         dateColor = Colors.BUYER_LEAD_RED
//     }
//     return dateColor
// }

export function getHighlightedText(highlighted, completeText) {
    if (completeText) {
        let indexOfQuery = completeText.toLowerCase().indexOf(highlighted.toLowerCase());
        let p1 = completeText.slice(0, indexOfQuery),
            p2 = completeText.slice(indexOfQuery, indexOfQuery + highlighted.length),
            p3 = completeText.slice(indexOfQuery + highlighted.length, completeText.length);
        return <Text allowFontScaling={false}>{p1}<Text allowFontScaling={false}
            style={{ fontWeight: 'bold', color: "#000" }}>{p2}</Text>{p3}
        </Text>
    }
}

export function getFormatedNumber(number) {
    return (
        <NumberFormat
            value={number}
            displayType={'text'}
            thousandSeparator={true}
            prefix={Strings.CURRENCY_SYMBOL}
            thousandsGroupStyle={Strings.CURRENCY_TYPE}
            renderText={value => <Text>{value}</Text>}
        />
    )
}

export function callNumber(mobileNo) {
    log("cal===> ", mobileNo)
    let callingUrl = "tel:" + mobileNo

    Linking.canOpenURL(callingUrl).then(supported => {
        if (!supported) {
            showToast('Can\'t handle url: ' + callingUrl)
        } else {
            return Linking.openURL(callingUrl)
        }
    }).catch(err => log('An error occurred', err))
}

export function onWhatsAppCall(mobileNo) {
    log('mobileNo===> ', mobileNo)

    // let url = `whatsapp://send?text=${''}&phone=${mobileNo}`
    let url = `whatsapp://send?phone=${mobileNo}`

    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            showToast(Strings.WHATSAPP_NOT_INSTALL)
        }
        else {
            return Linking.openURL(url)
        }
    })
        .catch(err => log('An error occurred', err))
}

// Convert String to Title Case(First letter in Caps rest in Lower)
export function strToTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
}

// Convert String to Upper Case(All letter in Caps)
export function strToUpperCase(str) {
    return str.toUpperCase()
}

// Convert String to Lower Case(All letter in Lower)
export function strToLowerCase(str) {
    return str.toLowerCase()
}

// Convert String to SubString
export function getStrFirstLetter(str) {
    let L1 = '', L2 = '';
    let str1 = str.split(" ")[0]
    let str2 = str.split(" ")[1]

    if (str1 && str1 != '') {
        L1 = str1.charAt(0).toUpperCase()
    }
    if (str2 && str2 != '') {
        L2 = str2.charAt(0).toUpperCase()
    }
    return L1 + L2
}

export function deltaDate(input, days, months, years) {
    var date = new Date(input);
    date.setDate(date.getDate() + days);
    date.setMonth(date.getMonth() + months);
    date.setFullYear(date.getFullYear() + years);
    return date;
}

export function onlyNumeric(text) {
    return text.replace(/[^0-9]/g, '')
}

export function onlyDecimal(text) {
    return text.replace(/[^0-9,]/g, '')
}

export function onlyAlphabet(text) {
    return text.replace(/[`~0-9!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
}

export function displayCurrency(labelValue) {
    //return labelValue
    // Nine Zeroes for Billions
    let value = Math.abs(Number(labelValue)) >= 1.0e+9

        ? Math.abs(Number(labelValue)) / 1.0e+9 + " Billion"
        // Six Zeroes for Millions
        : Math.abs(Number(labelValue)) >= 1.0e+6

            ? Math.abs(Number(labelValue)) / 1.0e+6 + " Million"
            // Three Zeroes for Thousands
            : Math.abs(Number(labelValue)) >= 1.0e+3

                ? Math.abs(Number(labelValue)) / 1.0e+3 + " Thousand"

                : Math.abs(Number(labelValue));
    //log("value::::"+value)
    value = value.toString()
    if (Constants.APP_TYPE == Constants.INDONESIA && !isValueNullOrEmpty(value)) {
        if (value.includes("."))
            return value.replace(".", ",")
        else
            return value
    }
    else if (Constants.APP_TYPE == Constants.PHILIPPINES && !isValueNullOrEmpty(value))
        return formatCurrency(labelValue)
    else
        return value

}

export function registrationNumber(text) {
    var pattern = new RegExp('^[a-zA-Z]+[a-zA-Z0-9]*$');
    return pattern.test(text)
    //return text.match(/^[a-zA-Z]+[a-zA-Z0-9]*$/g)
}

export function formatCurrency(amount) {
    // console.log("hello::"+amount)
    if (amount != '' && amount != '0' && amount != 0) {
        if (!Number.isInteger(amount)) {
            amount = parseInt(amount)
        }
        if (Constants.APP_TYPE == Constants.INDONESIA)
            return Strings.RUPEE_SYMBOL + " " + amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        else
            return Strings.RUPEE_SYMBOL + " " + amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    else {
        return amount
    }
}

export function formatNumber(amount) {
    // console.log("hello::"+amount)
    if (amount != '' && amount != '0' && amount != 0) {
        if (!Number.isInteger(amount)) {
            amount = parseInt(amount)
        }
        if (Constants.APP_TYPE == Constants.INDONESIA)
            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        else
            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    else {
        return amount
    }
}

export function downloadSingleImage(image_URL) {
    var date = new Date();
    var ext = getExtention(image_URL);

    if (ext != undefined) {
        ext = "." + ext[0];
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: PictureDir + "/image_" + Math.floor(date.getTime()
                    + date.getSeconds() / 2) + ext,
                description: 'Image'
            }
        }
        config(options).fetch('GET', image_URL).then((res) => {
            log('res==> ', res)
            showToast("Image Downloaded Successfully.")
        })
    }
}

export function downloadMultiImage(imageArr) {
    let imagePath = []

    return new Promise((resolve) => {
        log('inResolve')
        for (var i = 0; i < imageArr.length; i++) {
            var date = new Date();
            let image_URL = imageArr[i]
            var ext = getExtention(image_URL);

            if (ext != undefined) {
                ext = "." + ext[0];
                const { config, fs } = RNFetchBlob;
                let PictureDir = fs.dirs.PictureDir
                let options = {
                    fileCache: true,
                    addAndroidDownloads: {
                        useDownloadManager: true,
                        notification: true,
                        path: PictureDir + "/image_" + Math.floor(date.getTime()
                            + date.getSeconds() / 2) + ext,
                        description: 'Image'
                    }
                }
                config(options).fetch('GET', image_URL).then((res) => {
                    // log('res==> ', res)
                    imagePath.push(res.data)
                    if (imageArr.length == imagePath.length) {
                        convertImageToBase64(imagePath[0])
                        resolve(imagePath)
                    }
                })
            }
        }
    })
}

export function convertImageToBase64(imageArr) {
    let base64Images = []
    // log('imageArr==>', imageArr)

    return new Promise((resolve) => {
        for (var i = 0; i < imageArr.length; i++) {
            RNFetchBlob.fetch('GET', imageArr[i])
                .then((res) => {
                    log('convertImageToBase64==>', res.data)

                    base64Images.push("data:image/jpeg;base64," + res.data)

                    if (imageArr.length == base64Images.length) {
                        resolve(base64Images)
                    }
                    // resolve(res.data)
                    // return res.data
                })
                // Something went wrong:
                .catch((errorMessage, statusCode) => {
                    // error handling
                    log('errorMessage==>', errorMessage)
                    // log('statusCode==>', statusCode)
                })
        }
    })
}

export function getExtention(filename) {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined
}

export async function getResizedImage(imageUri) {
    try {
        ImageResizer.createResizedImage(imageUri, 80, 60, 'JPEG', 100)
            .then(({ uri }) => {
                return uri
            })
    } catch (error) {
        log(error);
    }
}

export function validateMobileNumber(mobile) {
    var pattern = new RegExp('^[0-9]+$');
    //alert(pattern.test(mobile))
    if (mobile != '') {
        if (mobile.startsWith("8") && pattern.test(mobile)) {
            return true
        }
        else {
            return false
        }
    }
    return true
}

export function maxLengthMobileNo(countryCode) {
    if (countryCode == '+91') {         // India
        return 10
    }
    else if (countryCode == '+62') {    // Indonesia
        return 12
    }
    else if (countryCode == '+63') {    // Philippines
        return 10
    }
    else {
        return 10
    }
}

export function validateMobileNo(mobile, countryCode, data) {
    var pattern
    try {
        if (mobile != '') {
            if (data && data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if (countryCode == data[i].value) {
                        pattern = RegExp(data[i].mobile_validation)  //data[i].mobile_validation // "^(9)\\d{8,9}$"
                    }
                }
            }
            else if (countryCode == '+91') {     // India
                pattern = /^[6-9]\d{9}$/
            }
            else if (countryCode == '+62') {    // Indonesia
                pattern = /^[8]\d{8,11}$/
            }
            else if (countryCode == '+63') {    // Philippines
                pattern = /^[9]\d{9}$/
                // pattern = /^[0-9]\d{6,14}$/
            }
            else {
                pattern = /^[6-9]\d{9}$/
            }

            if (pattern.test(mobile)) {
                return true
            }
            else {
                return false
            }
        }
        return false
    } catch (err) {
        return false
    }
}

export function validatePlateNo(plateNo, regex) {
    // log('validatePlateNo==>', plateNo, regex)
    // For Indonesia:- ^[A-Za-z][0-9a-zA-Z]{2,8}$
    // For Philippines:- "^[A-Za-z][0-9a-zA-Z]{0,5}\\d$"

    var pattern
    try {
        if (plateNo != '') {
            pattern = RegExp(regex)
        }

        if (pattern.test(plateNo)) {
            return true
        }
        else {
            return false
        }
    } catch (err) {
        return true
    }
}


export function openExternalUrl(url) {
    log("cal", url);
    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            showToast(Strings.PROVIDED_URL_CAN_NOT_BE_OPENED)
        } else {
            return Linking.openURL(url);
        }
    }).catch(err => error('An error occurred', err))
}

export async function sendEvent(eventType, eventParams) {
        
    if (Platform.OS === 'android' || (Config.BUILD !== 'DEV' && Config.BUILD !== 'DEV_PH')) {
        let keys = [Constants.USER_ID,Constants.NAME];
        const promise = Utility.getMultipleValuesFromAsyncStorage(keys);
        promise.then(async (values) => {
            if (eventParams){
                eventParams["user_id"] = parseInt(values[0][1]);
                eventParams["user_name"] = values[1][1];
            }else{
                eventParams = {"user_id": parseInt(values[0][1]),"user_name": values[1][1]};
            }
            await firebase.analytics().logEvent(eventType, eventParams);
            //Utility.log('----- Analytics Event logged ${eventType} -----');
            Utility.log('----- Analytics Event logged: '+eventType+ ' -----'+JSON.stringify(eventParams));
        });
        
    }
    return;
    
}

export async function sendCurrentScreen(screenName) {
    if (Platform.OS === 'android' || (Config.BUILD !== 'DEV' && Config.BUILD !== 'DEV_PH')) {
        await firebase.analytics().setCurrentScreen(screenName);
        Utility.log('----- Analytics Event logged: '+screenName+ ' -----');
    }
    return;
}
export async function sendUserId(userId) {
    userId = userId.toString()
    if (Platform.OS === 'android' || (Config.BUILD !== 'DEV' && Config.BUILD !== 'DEV_PH')) {
        //await firebase.analytics().setAnalyticsCollectionEnabled(true)
        //await firebase.analytics().setUserId(userId);
        await Promise.all([
            firebase.analytics().setAnalyticsCollectionEnabled(true),
            firebase.analytics().setUserId(userId),
        ]);
        Utility.log('----- Analytics Event logged: '+userId+ ' -----');
    }
    return;
}

export async function setUserProperties(properties) {
    if (Platform.OS === 'android' || (Config.BUILD !== 'DEV' && Config.BUILD !== 'DEV_PH')) {
        await Promise.all([
            firebase.analytics().setAnalyticsCollectionEnabled(true),
            firebase.analytics().setUserProperties(properties),
        ]);
        Utility.log('----- Analytics Event logged: '+properties+ ' -----');
        //await firebase.analytics().setAnalyticsCollectionEnabled(true)
        //await firebase.analytics().setUserProperties(properties)
    }
    return;
}

export async function setUserProperty(name,value) {
    if (Platform.OS === 'android' || (Config.BUILD !== 'DEV' && Config.BUILD !== 'DEV_PH')) {
        await Promise.all([
            firebase.analytics().setAnalyticsCollectionEnabled(true),
            firebase.analytics().setUserProperty(name, value),
        ]);
        //await firebase.analytics().setAnalyticsCollectionEnabled(true)
        //await firebase.analytics().setUserProperty(name, value)
    }
    return;
}

export async function onLogout() {
    await firebase.analytics().resetAnalyticsData();
}


export function replaceRange(s, start, end, substitute) {
    return s.substring(0, start) + substitute + s.substring(end)
}

export function pickDocument() {
    let selectedDocuments = []
    let doc
    try {
        return new Promise((resolve) => {

            FilePickerManager.showFilePicker(null, (response) => {
                log('Response = ', response);

                if (response.didCancel) {
                    //console.log('User cancelled file picker');
                }
                else if (response.error) {
                    //console.log('FilePickerManager Error: ', response.error);
                }
                else {
                    if (checkFileType(response.path, Constants.PDF_FILE_TYPE)) {
                        doc = { uri: response.path, type: response.type, name: response.fileName, size: response.size }
                        selectedDocuments.push(doc)
                        resolve(selectedDocuments)
                    }
                    else {
                        showToast(Strings.SELECT_PDF_ONLY)
                    }
                }
            });
        });
    } catch (err) {

    }

}

export async function pickDocumentIOS() {
    let selectedDocuments = []
    try {
        let doc
        const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.pdf],
        });
        doc = { uri: res.uri, type: res.type, name: res.name, size: res.size }
        selectedDocuments.push(doc)
        return selectedDocuments
    } catch (err) {

    }
}

export function checkFileType(fileName, type) {
    var ext = fileName.split('.').reverse()[0]
    ext = ext.toLowerCase()
    if (ext == type) {
        return true
    }
    return false

}

export async function getConfigArrayData(key) {
    let data = [];

    var storeObject = new AsyncStore()
    const values = await storeObject.multiGetAsyncValueInPersistStore([key])
    if (values[0][1] && values[0][1] != null) {
        data = JSON.parse(values[0][1])
    }
    // log('values[0][1]==> ', data)
    return data
}

export function confirmDialog(title, message, cancelText, confirmText, confirmCallback) {
    Alert.alert(title, message, [
        {
            text: cancelText,
            onPress: () => {
                confirmCallback(false)
            }
        },
        {
            text: confirmText,
            onPress: () => {
                confirmCallback(true)
            }
        }
    ], { cancelable: true })
}


export async function isShowFinance() {
    var storeObject = new AsyncStore()
    const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.IS_FINANCE])
    Utility.log('isShowFinance===>>>11', values[0][1])
    if (values[0][1] == '1') {
        return true
    }
    else {
        return false
    }
}

export async function getMasterData() {
    return new Promise((resolve) => {
        APICalls.getMasterData(async (response) => {
            Utility.log('onSuccessConfig===>', JSON.stringify(response.data.dealer_sku))

            if (response && response.data && response.status === 200) {
                var storeObject = new AsyncStore()

                if (response.data.lang && response.data.lang.length > 0) {
                    storeObject.saveValueInPersistStore(Constants.LANGUAGE_VALUES, JSON.stringify(response.data.lang))
                    const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.LANGUAGE_ID])
                    for (let i = 0; i < response.data.lang.length; i++) {
                        if (response.data.lang[i].id == values[0][1]) {
                            storeObject.saveValueInPersistStore(Constants.LANGUAGE_ID, response.data.lang[i].id.toString())
                            storeObject.saveValueInPersistStore(Constants.LANGUAGE_CODE, response.data.lang[i].iso_code)
                            // Set app lang....
                            if(Constants.APP_TYPE == Constants.INDONESIA)
                                Strings.setLanguage(response.data.lang[i].iso_code)
                            else
                                Strings.setLanguage(Constants.LANGUAGE_CODE_PH)
                        }
                    }
                }

                if (response.data.is_finance) {
                    storeObject.saveValueInPersistStore(Constants.IS_FINANCE, response.data.is_finance.toString())
                }
                else {
                    storeObject.saveValueInPersistStore(Constants.IS_FINANCE, '0')
                }
                if (response.data.total_mfg_year) {
                    storeObject.saveValueInPersistStore(Constants.TOTAL_MFG_YEAR, response.data.total_mfg_year.toString())
                }
                else {
                    storeObject.saveValueInPersistStore(Constants.TOTAL_MFG_YEAR, '-25')
                }

                if (response.data.inventory_fuel_type && response.data.inventory_fuel_type.length > 0)
                    storeObject.saveValueInPersistStore(Constants.FUEL_TYPE_VALUES, JSON.stringify(response.data.inventory_fuel_type))

                if (response.data.inventory_transmission_type && response.data.inventory_transmission_type.length > 0)
                    storeObject.saveValueInPersistStore(Constants.TRANSMISSION_TYPE_VALUES, JSON.stringify(response.data.inventory_transmission_type))

                if (response.data.inventory_body_type && response.data.inventory_body_type.length > 0)
                    storeObject.saveValueInPersistStore(Constants.BODY_TYPE_VALUES, JSON.stringify(response.data.inventory_body_type))

                if (response.data.lead_dp_budget && response.data.lead_dp_budget.length > 0) {
                    let dpBudget = [...[{ "key": "0", "value": "0" }], ...response.data.lead_dp_budget]
                    storeObject.saveValueInPersistStore(Constants.DP_BUDGET_VALUES, JSON.stringify(dpBudget))
                }

                if (response.data.lead_otr_budget && response.data.lead_otr_budget.length > 0) {
                    let otrBudget = [...[{ "key": "0", "value": "0" }], ...response.data.lead_otr_budget]
                    storeObject.saveValueInPersistStore(Constants.OTR_BUDGET_VALUES, JSON.stringify(otrBudget))
                }

                if (response.data.lead_status_list && response.data.lead_status_list.length > 0) {
                    let addLeadStatus = []
                    for (var i = 0; i < response.data.lead_status_list.length; i++) {
                        if (response.data.lead_status_list[i].id != 1 && response.data.lead_status_list[i].id != 2 && response.data.lead_status_list[i].id != 13)
                            addLeadStatus.push(response.data.lead_status_list[i])
                    }
                    storeObject.saveValueInPersistStore(Constants.LEAD_STATUS_VALUES, JSON.stringify(addLeadStatus))
                }

                if (response.data.lead_source_list && response.data.lead_source_list.length > 0)
                    storeObject.saveValueInPersistStore(Constants.LEAD_SOURCE_VALUES, JSON.stringify(response.data.lead_source_list))

                if (response.data.country_code && response.data.country_code.length > 0) {
                    storeObject.saveValueInPersistStore(Constants.COUNTRY_CODE_VALUES, JSON.stringify(response.data.country_code))
                }
                else {
                    storeObject.saveValueInPersistStore(Constants.COUNTRY_CODE_VALUES, JSON.stringify(Constants.COUNTRY_CODE_LIST))
                }

                if (response.data.insurance_type && response.data.insurance_type.length > 0)
                    storeObject.saveValueInPersistStore(Constants.INSURANCE_TYPE_VALUES, JSON.stringify(response.data.insurance_type))

                if (response.data.inventory_owner_type && response.data.inventory_owner_type.length > 0)
                    storeObject.saveValueInPersistStore(Constants.OWNER_TYPE_VALUES, JSON.stringify(response.data.inventory_owner_type))

                if (response.data.emi_tenure_list && response.data.emi_tenure_list.length > 0)
                    storeObject.saveValueInPersistStore(Constants.EMI_TENURE_VALUES, JSON.stringify(response.data.emi_tenure_list))

                if (response.data.plate_number_validation && response.data.plate_number_validation.length > 0)
                    storeObject.saveValueInPersistStore(Constants.PLATE_NO_VALIDATION, response.data.plate_number_validation)

                // if (response.data.dealer_sku && response.data.dealer_sku.length > 0){
                //     let dealer_sku = await getConfigArrayData(Constants.DEALER_SKU)
                //     if(dealer_sku){
                //         let dealer_sku_new = response.data.dealer_sku.map(obj => {
                //             for(let i=0;i < dealer_sku.length;i++){
                //                 if(obj.id === dealer_sku[i].id){
                //                     obj.isRequestSent = dealer_sku[i].isRequestSent
                //                     obj.requestSubmitTime = dealer_sku[i].requestSubmitTime
                //                 }
                //             }
                //             return obj
                //         })
                //         storeObject.saveValueInPersistStore(Constants.DEALER_SKU, JSON.stringify(dealer_sku_new))
                //     }else{
                //         storeObject.saveValueInPersistStore(Constants.DEALER_SKU, JSON.stringify(response.data.dealer_sku))
                //     }
                // }
                if (response.data.dealer_sku_info_url)
                    storeObject.saveValueInPersistStore(Constants.DEALER_SKU_INFO_URL, response.data.dealer_sku_info_url)
                if (response.data.vehicle_number_type)
                    storeObject.saveValueInPersistStore(Constants.VEHICLE_NUMBER_TYPE, JSON.stringify(response.data.vehicle_number_type))
                    
                resolve(true)
            }
        }, (error) => {
            Utility.log('onFailureConf===> ', JSON.stringify(error))
            resolve(false)
        })

    })
}

export function retryLoanDocumentImageUpload(){
    let imageUploadFile = new DocumentImageUploadFile();
    let dbFunctions = new DBFunctions();
    try {
        dbFunctions.getDistintLeadIds().then(result => {
            Utility.log("Distinct Lead Ids:" + JSON.stringify(result));
            //let i = 0;
            if (result.length > 0) {
                //for (i = 0; i < result.length; i++) {
                    Utility.log("Distinct Ids:" + result[0][DBConstants.LEAD_ID]);
                    imageUploadFile.checkAndUpload(result[0][DBConstants.LEAD_ID])
                //}
            }
        });

    } catch (error) {
        Utility.log("==================> " + error);
    }
}

export function retryImageUpload()  {
    let imageUploadFile = new ImageUploadFile()
    let dbFunctions = new DBFunctions();
    try {
       dbFunctions.getDistintCarIds().then(result => {
          Utility.log("Distinct Ids:1" + JSON.stringify(result));
          //let i = 0;
          if (result.length > 0) {
             //for (i = 0; i < result.length; i++) {
                Utility.log("Distinct Ids:" + result[0][DBConstants.CAR_ID]);
                imageUploadFile.checkAndUpload(result[0][DBConstants.CAR_ID])
             //}
          }
       });

    } catch (error) {
       Utility.log("==================> " + error);
    }
 }

 export function checkDateForToday(dateString) {
    if (dateString && dateString.length > 0) {
        let date = new Date(dateString)
        let futureDate = DateFormat(date, 'mm/dd/yyyy')
        let currentDate = DateFormat(new Date(), 'mm/dd/yyyy')
        var msDiff = new Date(futureDate).getTime() - new Date(currentDate).getTime()  //Future date - current date
        var totalDays = Math.floor(msDiff / (1000 * 60 * 60 * 24))
       
        if (totalDays == 0) {
            return 0
        }
        else {
            return -1
        }
    }
        return ""

}






