import AsyncStorage from '@react-native-community/async-storage';
import { Constants, Utility } from '../util'
export default class AsyncStore {

    constructor() {
        this.saveValueInPersistStore = this.saveValueInPersistStore.bind(this);
        this.getValueInPersistStore = this.getValueInPersistStore.bind(this);
    }

    async getAsyncValueInPersistStore(key) {
        var value = await AsyncStorage.getItem(key);
        return value
    }

    getItemByKey(key) {
        return AsyncStorage.getItem(key);
    }

    async isUserLogined() {
        var value = await AsyncStorage.getItem(Constants.ACCESS_TOKEN);
        // Utility.log('access_token===>', value)

        if (value) {
            return true
        }
        return false
    }

    async multiGetAsyncValueInPersistStore(keys) {
        const values = await AsyncStorage.multiGet(keys)
        var text = 'After Text multiget :' + values
        return values
    }

    saveValueInPersistStore(key, value) {
        if (value) {
            AsyncStorage.setItem(key, value)
        }
    }

    // getValueInPersistStore(key, callback) {
    //     AsyncStorage.getItem(key).then((value) => {
    //         callback(value)
    //     });
    // }


    getValueInPersistStore = (key) => {
        AsyncStorage.getItem(key).then((value) => {
            // callback(value)
            return value
        });
        // return null
    }
}
