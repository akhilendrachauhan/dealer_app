import Upload from "react-native-background-upload";
import * as Utility from "./Utility";
import * as APIConstant from "../api/APIConstants";
import {Platform} from 'react-native'
import {showNotification} from './FirebaseListeners'
import {Strings} from "../values";
import DBFunctions from "../database/DBFunctions";
export default class SingleImageUpload {
    constructor() {
        this.leadId = 0;
        this.dbFunctions = new DBFunctions()
    }

    async getFields(url) {
        Utility.log("File Data" + url);
       
        let value = {
            upload_type : 'inventory_car_images'
        };
        let fPath = url
        if (Platform.OS == "android") {
            fPath = fPath.replace("file://", "");
        }
       return params = {
            url: APIConstant.UPLOAD_TO_S3,
            path: fPath,
            method: 'POST',
            type: 'multipart',
            field: 'images',
            parameters: value,
            headers: {
                'content-type': 'multipart/form-data'
            },
            notification: {
                enabled: true,
                notificationChannel: 'classified-notification'
            }
        };
    }

    async uploadImage(leadId) {
        this.leadId = leadId
        Utility.getNetInfo().then(async isConnected => {
            if (!isConnected) {
                Utility.showNoInternetDialog();
                return;
            }
        let params = await this.getFields(url);
        Upload.startUpload(params).then((uploadId) => {
            Utility.log('Upload started');
            this.fireNotification(Strings.PLEASE_WAIT_UPLOADING)
            Upload.addListener('progress', uploadId, (data) => {
                Utility.log(`Progress: ${data.progress}%`)
                
            });
            Upload.addListener('error', uploadId, (data) => {
                Utility.log(`Error: ${data.error}%`);
                //this.checkAndUpload(this.carId);
                this.uploadImage(url,callback);
                
            });
            Upload.addListener('cancelled', uploadId, (data) => {
                Utility.log(`Cancelled!`);
                
            });
            Upload.addListener('completed', uploadId, (data) => {
                let responseValue = data["responseBody"];
                Utility.log('ResCompleted!',responseValue);
                responseValue = JSON.parse(responseValue)
                if (responseValue.status === 200) {
                    Utility.log('Completed!');
                    this.fireNotification(Strings.UPLOADING_COMPLETED)
                    callback(responseValue.data[0].file_url,responseValue.status)
                } else {
                    callback(responseValue.message,responseValue.status)
                    //this.uploadImage(url,callback);
                }
            })
        }).catch((err) => {
            Utility.log('Upload error!', err)
        })
    });
    }

    fireNotification(msg) {
        
        let notification = {
        notificationId: '11',
        title: Strings.UPLOAD_CALCULATOR_IMAGE,
        body: msg,
        autoCancel: true,
        };
        showNotification(notification);
    }    
    
}