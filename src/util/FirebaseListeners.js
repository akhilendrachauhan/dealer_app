import { Platform, AsyncStorage, AppState,Linking } from 'react-native';
import * as Utility from './Utility'
import firebase, { notifications } from 'react-native-firebase';
import * as Constants from "./Constants";
import * as Colors from '../values/Colors'

//example for showing local notification
export function showLocalNotification(notification) {


    let customizedNotification = new firebase.notifications.Notification({
        show_in_foreground: true,
    });
    customizedNotification = customizedNotification.setNotificationId(notification.notificationId)
        .setTitle(notification.title === undefined ? 'NA' : notification.title.toString())
        .setBody(notification.body.toString());
    customizedNotification.android.setAutoCancel(notification.android.autoCancel === undefined ? true : notification.android.autoCancel);
    customizedNotification.android.setPriority(firebase.notifications.Android.Priority.High);

    if (notification.data && notification.data.auc_ic_sml) {
        //customizedNotification.android.setBigPicture(notification.data.auc_ic_sml);
        //customizedNotification.android.setSmallIcon(notification.data.auc_ic_sml);
    }
    
    customizedNotification.android.setColor(Colors.PRIMARY);
    customizedNotification.setData(notification.data);
    if(Platform.OS =='android'){
        customizedNotification.setNotificationId(notification.notificationId);   
        customizedNotification.android.setChannelId(Constants.NOTIFICATION_CHANNEL_ID);
    }
    firebase.notifications().displayNotification(customizedNotification)
        .catch(err => {
           // console.log('showLocalNotification err=>', err)
    });
}

export async function registerAppListener(context){ 
    context.notificationListener = firebase.notifications().onNotification((notification) => {
        //notification.ios.badge = 0
        showLocalNotification(notification);
    });
  
    context.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        let notif = notificationOpen.notification
        Utility.log(notificationOpen);
        let lead_id = notif._data.lead_id ? notif._data.lead_id : 0
        let source = notif._data.source ? notif._data.source : ""
        if(lead_id){
            context.props.navigation.navigate('leadDetail',{leadId : lead_id,notifId : notif._data.fcm_unique_key})
        }
        if(source == "packDetail"){
            context.props.navigation.navigate('packDetail')
        }
    });
  
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        let notif = notificationOpen.notification
        let lead_id = notif._data.lead_id ? notif._data.lead_id : 0
        let source = notif._data.source ? notif._data.source : ""
        if(lead_id){
            context.props.navigation.navigate('leadDetail',{leadId : lead_id,notifId : notif._data.fcm_unique_key})
        }
        if(source == "packDetail"){
            context.props.navigation.navigate('packDetail')
        }
        
    }
  
    context.messageListener = firebase.messaging().onMessage((message) => {
      Utility.log(JSON.stringify(message));
      
    });
    context.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
        firebase.notifications().getBadge()
          .then( count => {
            count++
            //firebase.notifications().setBadge(count)
         })
         .then(() => {
           //console.log('Doing great')
         })
         .catch( error => {
           //console.log('fail to count')
         })

})
  }

// these callback will be triggered only when app is foreground or background
// export function registerAppListener(context){
//     context.notificationListener = firebase.notifications().onNotification(notification => {
//         Utility.log('onNotificationRa---------------',notification);
//         showLocalNotification(notification);
//         //firebase.notifications().displayNotification(notification);
//     });
//     context.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
//         // called when app is in background
//         Utility.log('onNotificationOpened---------------', notificationOpen.notification._notificationId);
//         Utility.log(notificationOpen.notification);
        

//     });

//     context.onTokenRefreshListener = firebase.messaging().onTokenRefresh(token => {
//         Utility.log("TOKEN (refreshUnsubscribe)-----------", token);
//         Utility.setValueInAsyncStorage(Constants.FCM_TOKEN, token)
//     });


//     //required for silent notification(data only messages)
//     /* this.messageListener = firebase.messaging().onMessage((message) => {
//          displayNotificationFromCustomData(message);
//      });*/

// }

export function unRegisterNotificationListener(context) {
    context.notificationListener();
    context.notificationOpenedListener();
    context.onTokenRefreshListener();
}



export function showNotification(notification) {
    try {
    let customizedNotification = new firebase.notifications.Notification({
    sound: 'default',
    show_in_foreground: true
    });
    customizedNotification = customizedNotification.setNotificationId(notification.notificationId)
    .setTitle(notification.title === undefined ? 'NA' : notification.title.toString())
    .setBody(notification.body.toString());
    customizedNotification.android.setAutoCancel(notification.autoCancel === undefined ? true : notification.autoCancel);
    customizedNotification.android.setColor(Colors.PRIMARY);
    customizedNotification.android.setPriority(firebase.notifications.Android.Priority.High);
    customizedNotification.android.setChannelId(Constants.NOTIFICATION_CHANNEL_ID);
    firebase.notifications().displayNotification(customizedNotification);
    } catch (exception) {
    Utility.log("Exception: ---> " + exception);
    }
}

export function removeAllNotification(){
    firebase.notifications().removeAllDeliveredNotifications()
}