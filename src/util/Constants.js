import { Strings } from './../values'
import { Platform } from 'react-native'
import Config from 'react-native-config'

export const APP_TYPE = Config.APP_TYPE
export const INDONESIA = 'Id'
export const PHILIPPINES = 'Ph'
export const VERSION_CODE = Platform.OS == 'android' ? Config.V_CODE : Config.IOS_V_CODE
export const VERSION_NAME = Platform.OS == 'android' ? Config.V_NAME : Config.IOS_V_NAME
export const APP_LINK = Platform.OS == 'android' ? 'http://play.google.com/store/apps/details?id=com.oto.dealer.id' : 'https://itunes.apple.com/us/app/oto-dealer/id1489698884?ls=1&mt=8'
export const PH_APP_LINK = Platform.OS == 'android' ? 'http://play.google.com/store/apps/details?id=com.carmudi.dealer.ph' : 'https://itunes.apple.com/us/app/carmudi-ph-dealer/id1495061578?ls=1&mt=8'
export const IS_LOGIN = 'is_login'
export const USER_ID = 'user_id'
export const USERNAME = 'user_name'
export const EMAIL = 'email'
export const NAME = 'name'
export const EMP_CODE = 'emp_code'
export const PHONE_NO = 'phone_no'
export const ACCESS_TOKEN = 'access_token'
export const FIREBASE_TOKEN = 'firebase_token'
export const DEALER_ID = 'dealer_id'
export const DEALER_ID_HASH = 'dealer_id_hash'
export const DEALER_NAME = 'dealer_name'
export const DEALER_WHATSAPP_FINANCE_NO = 'dealer_whatsapp_finance_no'
export const DEALER_ADDRESS = 'dealer_address'
export const DEALER_LAT = 'dealer_lat'
export const DEALER_LANG = 'dealer_lang'
export const DEALER_CITY = 'dealer_city'
export const GCD_CODE = 'gcd_code'
export const LANGUAGE_ID = 'language_id'
export const LANGUAGE_CODE = 'language_code'
export const COUNTRY_CODE = 'country_code'
export const SELECTED_COUNTRY_LANGUAGE_JSON = 'selectedCountryLanguageJson'
export const FCM_TOKEN = 'fcmToken'
export const NOTIFICATION_CHANNEL_ID = 'classified-notification';
export const NOTIFICATION_CHANNEL_DESC = 'Classified Notification';
export const BUDGET_VALUES = 'budget_list'
export const DP_BUDGET_VALUES = 'dp_budget_list'
export const OTR_BUDGET_VALUES = 'lead_dp_budget'
export const LEAD_STATUS_VALUES = 'lead_status_list'
export const LEAD_SOURCE_VALUES = 'lead_source_list'
export const FUEL_TYPE_KEY = 'fuel_type_id'
export const FUEL_TYPE_VALUES = 'fuel_type_list'
export const TRANSMISSION_TYPE_KEY = 'uc_transmission_id'
export const TRANSMISSION_TYPE_VALUES = 'transmission_type_list'
export const BODY_TYPE_KEY = 'body_type'
export const BODY_TYPE_VALUES = 'body_type_list'
export const LMS_SYNC = 'lms_sync'
export const LANGUAGE_CODE_BHASA = 'id';
export const LANGUAGE_CODE_PH = 'en-ph';
export const IS_FINANCE = 'is_finance';
export const FINANCE_URL = 'finance_url';
export const CONFIG_JSON = 'config_json'
export const COUNTRY_CODE_VALUES = 'country_code_list'
export const LANGUAGE_VALUES = 'language_list'
export const INSURANCE_TYPE_VALUES = 'insurance_type_list'
export const OWNER_TYPE_VALUES = 'owner_type_list'
export const EMI_TENURE_VALUES = 'emi_tenure_list'
export const SFA_USER_DATA = 'sfa_user_data'
export const PLATE_NO_VALIDATION = 'plate_no_validation'
export const TOTAL_MFG_YEAR = 'total_mfg_year'
export const stockDetailMenu1 = [
    { id: '1', option: Strings.MAKE_FEATURED },
    { id: '2', option: Strings.REMOVE_FROM_CLASSIFIED },
    //{ id: '3', option: Strings.RENEW_YOUR_POST_NOW },
    { id: '4', option: Strings.REMOVE_FROM_STOCK },
    //{ id: '5', option: Strings.ISSUE_WARRANTY },
]

export const stockDetailMenu2 = [
    { id: '1', option: Strings.MAKE_FEATURED },
    { id: '2', option: Strings.REMOVE_FROM_CLASSIFIED },
    //{ id: '3', option: Strings.RENEW },
    { id: '4', option: Strings.REMOVE_FROM_STOCK },
    // { id: '5', option: Strings.ISSUE_WARRANTY },
]

export const ScreenStates = Object.freeze({
    NO_ERROR: 0,
    IS_LOADING: 1,
    NO_DATA_FOUND: 2,
    INTERNET_NOT_AVAILABLE: 3,
    SERVER_ERROR: 4,
    IS_REFRESHING: 5,
    NO_SEARCH_DATA_FOUND: 6,
})

export const ScreenName = Object.freeze({
    SCREEN_DASHBOARD: 0,
    SCREEN_LEAD: 1,
    SCREEN_STOCK: 2,
    SCREEN_LOAN: 3,
})

/* Stock remove constants */
export const CAR_IS_BOOKED = 1
export const CAR_IS_SOLD = 2
export const CAR_IS_SENT = 3
export const CAR_DETAILS_INCORRECT = 4
export const CAR_LISTING_OLD = 5
/* Stock remove constants */

/* Stock update constants */
export const ADD_TO_CLASSIFIED = 1
export const REMOVE_FROM_CLASSIFIED = 2
export const ADD_TO_FEATURED = 3
export const REMOVE_FROM_FEATURED = 4
export const REMOVE_STOCK = 5
export const FROM_ADD_STOCK = 6
export const FROM_REMOVED_DETAIL = 7
export const UPDATE_CAR_PRICE = 8
export const ADDED_TO_BUMPUP = 9
/* Stock update constants */

/* Stock dashboard constants */
export const DASHBOARD_CARD_CAR = 'DASHBOARD_CARD_CAR'
export const DASHBOARD_CARD_CAR_COUNT = 'DASHBOARD_CARD_CAR_COUNT'
export const DASHBOARD_CARD_CAR_NON_CLASSIFIED = 'DASHBOARD_CARD_CAR_NON_CLASSIFYED'
export const DASHBOARD_CARD_CAR_WITHOUT_PHOTOS = 'DASHBOARD_CARD_CAR_WITHOUT_PHOTOS'
export const DASHBOARD_CARD_CAR_45D_OLD = 'DASHBOARD_CARD_CAR_45D_OLD'
export const DASHBOARD_CARD_LEAD = 'DASHBOARD_CARD_LEAD'
export const DASHBOARD_CARD_LEAD_CONVERSION = 'DASHBOARD_CARD_LEAD_CONVERSION'
export const DASHBOARD_CARD_LEAD_RECEIVED = 'DASHBOARD_CARD_LEAD_RECEIVED'
export const DASHBOARD_CARD_LEAD_WALKIN = 'DASHBOARD_CARD_LEAD_WALKIN'
export const DASHBOARD_CARD_LEAD_PENDING = 'DASHBOARD_CARD_LEAD_PENDING'
export const DASHBOARD_CARD_FINANCE = 'DASHBOARD_CARD_FINANCE'
export const DASHBOARD_CARD_FINANCE_DISBURSED_AMOUNT = 'DASHBOARD_CARD_FINANCE_DISBURSED_AMOUNT'
export const DASHBOARD_CARD_FINANCE_LOGINS = 'DASHBOARD_CARD_FINANCE_LOGINS'
export const DASHBOARD_CARD_FINANCE_APPROVAL = 'DASHBOARD_CARD_FINANCE_APPROVAL'
export const DASHBOARD_CARD_FINANCE_DISVERTION = 'DASHBOARD_CARD_FINANCE_DISVERTION'
/* Stock dashboard constants */

/* GET STOCKS constants */
export const ACTIVE_STOCK = 1
export const REMOVED_STOCK = 2
export const NON_CLASSIFIED_STOCK = 3
export const WITHOUT_PHOTOS_STOCK = 4
export const AGE_45D_STOCK = 5
export const ALL_STOCK = 6
export const CAR_AGE_BEFORE = '45'
/* GET STOCKS constants */

export const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
/*public static final String STATUS_BUYER_LEAD_NEW = "New";
public static final String STATUS_BUYER_LEAD_FOLLOW_UP = "Follow Up";
public static final String STATUS_BUYER_LEAD_INTERESTED = "Interested";
public static final String STATUS_BUYER_LEAD_WALK_IN = "Walk-in Scheduled";
public static final String STATUS_BUYER_LEAD_WALKED_IN = "Walk-in Done";
public static final String STATUS_BUYER_LEAD_CUSTOMER_OFFER = "Customer Offer";
public static final String STATUS_BUYER_LEAD_BOOKED = "Booked";
public static final String STATUS_BUYER_LEAD_CONVERTED = "Converted";
public static final String STATUS_BUYER_LEAD_CLOSED = "Closed";*/
export const LMS_TASK_STATUS_NEW = "New"
export const LMS_TASK_STATUS_FOLLOW_UP = "Follow_up"
export const LMS_TASK_STATUS_INTERESTED = "Interested"
export const LMS_TASK_STATUS_WALKED_IN = "WalkedIn"
export const LMS_TASK_STATUS_WALK_IN_CONFIRMED = "WalkInConfirmed"
export const LMS_TASK_STATUS_WALK_IN = "WalkIn"
export const LMS_TASK_STATUS_BOOKED = "Booked"
export const LMS_TASK_STATUS_CUSTOMER_OFFER = "CustomerOffer"
export const LMS_TASK_STATUS_CONVERTED = "Converted"
export const LMS_TASK_STATUS_CLOSE = "Close"
export const LMS_TASK_STATUS_CALL = "Call"
export const LMS_TASK_STATUS_SHARE = "Share"
export const LMS_TASK_APP_BOOKED = "AppBooked"
export const LMS_TASK_APP_OFFER = "AppOffer"

export const LEAD_STATUS_NEW = "New"
export const LEAD_STATUS_FOLLOW_UP = "Follow Up"
export const LEAD_STATUS_INTERESTED = "Interested"
export const LEAD_STATUS_WALKED_IN = "Walk-in Done"
export const LEAD_STATUS_WALK_IN = "Walk-in"
// export const LEAD_STATUS_BOOKED = "Booked"
// export const LEAD_STATUS_CUSTOMER_OFFER = "Customer Offer"
// export const LEAD_STATUS_CONVERTED = "Converted"
// export const LEAD_STATUS_CLOSE = "Closed"
// export const LEAD_STATUS_WALK_IN_SCHEDULED = "Walk-in Scheduled"

export const LEAD_STATUS_BOOKED = APP_TYPE == PHILIPPINES ? "Reserved" : "Booked"
export const LEAD_STATUS_CUSTOMER_OFFER = "Customer Offer"
export const LEAD_STATUS_CONVERTED = APP_TYPE == PHILIPPINES ? "Sold" : "Converted"
export const LEAD_STATUS_CLOSE = APP_TYPE == PHILIPPINES ? "Lost" : "Closed"
export const LEAD_STATUS_WALK_IN_SCHEDULED = "Walk-in Scheduled"

export const LEAD_STATUS_ID_VALUE = {
    1: LEAD_STATUS_NEW,
    2: LEAD_STATUS_FOLLOW_UP,
    3: LEAD_STATUS_INTERESTED,
    4: LEAD_STATUS_WALK_IN_SCHEDULED,
    9: LEAD_STATUS_WALKED_IN,
    10: LEAD_STATUS_CUSTOMER_OFFER,
    11: LEAD_STATUS_BOOKED,
    12: LEAD_STATUS_CONVERTED,
    13: LEAD_STATUS_CLOSE
}

export const LMS_STATUS_BY_LEAD_STATUS_ID = {
    1: LMS_TASK_STATUS_NEW,
    2: LMS_TASK_STATUS_FOLLOW_UP,
    3: LMS_TASK_STATUS_INTERESTED,
    4: LMS_TASK_STATUS_WALK_IN,
    9: LMS_TASK_STATUS_WALKED_IN,
    10: LMS_TASK_STATUS_CUSTOMER_OFFER,
    11: LMS_TASK_STATUS_BOOKED,
    12: LMS_TASK_STATUS_CONVERTED,
    13: LMS_TASK_STATUS_CLOSE
}

export const LEAD_STATUS_BY_LMS_STATUS = {
    [LMS_TASK_STATUS_NEW]: LEAD_STATUS_NEW,
    [LMS_TASK_STATUS_FOLLOW_UP]: LEAD_STATUS_FOLLOW_UP,
    [LMS_TASK_STATUS_INTERESTED]: LEAD_STATUS_INTERESTED,
    [LMS_TASK_STATUS_WALKED_IN]: LEAD_STATUS_WALKED_IN,
    [LMS_TASK_STATUS_WALK_IN]: LEAD_STATUS_WALK_IN_SCHEDULED,
    [LMS_TASK_STATUS_CUSTOMER_OFFER]: LEAD_STATUS_CUSTOMER_OFFER,
    [LMS_TASK_STATUS_BOOKED]: LEAD_STATUS_BOOKED,
    [LMS_TASK_STATUS_CONVERTED]: LEAD_STATUS_CONVERTED,
    [LMS_TASK_STATUS_CLOSE]: LEAD_STATUS_CLOSE
};

/* Image upload status */
export const IMAGE_INITIAL_STATUS = 0
export const IMAGE_S3_UPLOAD_STATUS = 1
export const IMAGE_SEND_TO_SERVER = 2
export const IMAGE_NOT_EXISTS_STATUS = 3
export const IMAGE_CALCULATOR_STATUS = 4
//LMS update params keys
export const LMS_TRACKING = "lms_tracking";
export const NEXT_WALKIN = "walkin_date";
export const WALKED_IN = "walked_in";
export const OFFER_AMOUNT = "offer_amount";
export const RATING = "lead_rating";
export const COMMENT = "lead_comment";
export const FEEDBACK = "feedback";
export const FEEDBACK_ID = "feedback_id";
export const NEXT_FOLLOW = "followup_date";
export const METHOD_LABEL = "method";
export const SHARED_BY = "shared_by";
export const KEY_SHARED_ITEM = "shared_item";
export const REMINDER_DATE = 'reminder_date';
export const CAR_ID = "car_id";
export const LEAD_ID_PARAMS = "lead_id";
export const LEAD_MOBILE = "mobile";
export const FAVOURITE = "favourite";
export const OFFLINE_DATETIME = "offline_datetime";
export const CALL_DURATION = "call_duration";
export const CALL_TIME = "call_time";
export const CALL_TYPE = "call_type";
export const LEAD_STATUS = "lead_status";
export const BOOKING_AMOUNT = "booking_amount";
export const SALE_AMOUNT = "sale_amount";
export const EDIT_LEAD_API = "leadedit";
export const LEAD_RATING_COLD = "Cold";
export const LEAD_RATING_WARM = "Warm";
export const LEAD_RATING_HOT = "Hot";
export const WALKED_IN_TASK_ID = "2";
export const WALK_IN_SCHEDULED_STATUS_ID = 4;

//Lms share types
export const WELCOME_SHARE_TYPE = "Welcome_message";
export const CALL_REMINDER_SHARE_TYPE = "Call_reminder";
export const CAR_DETAILS_SHARE_TYPE = "Car_details";
export const LOCATION_SHARE_TYPE = "Location";
export const WALKIN_REMINDER_SHARE_TYPE = "Walkin_reminder";
export const CAR_PHOTOS_SHARE_TYPE = "Car_photos";

export const ADD_LEAD_STATUS = ['Interested', 'Walk-in Scheduled', 'Walk-in Done', 'Customer Offer', 'Booked', 'Converted']
export const ADD_LEAD_SOURCE = [
    { value: 'OTO' }, { value: 'Walk-in' }
    // { value: 'Zigwheels' }, { value: 'G-Cloud' }
]

export const DEFAULT_COUNTRY_CODE = Config.APP_TYPE == 'Id' ? '+62' : '+63'
export const COUNTRY_CODE_LIST = Config.APP_TYPE == 'Id' ? [{ value: '+62' }] : [{ value: '+63' }]
// [{ value: '+91' }, { value: '+62' }, { value: '+63' }]

export const FULE_TYPE = [
    { id: 0, value: 'Petrol' },
    { id: 1, value: 'Diesel' },
    { id: 2, value: 'CNG' },
    { id: 3, value: 'LPG' },
    { id: 4, value: 'Electric' }
]

export const TRANSMISSION_TYPE = [
    { id: 0, value: 'Automatic' },
    { id: 1, value: 'Manual' }
]

export const BODY_TYPE = [
    { id: 0, value: 'Convertibles' },
    { id: 1, value: 'Coupe' },
    { id: 2, value: 'Hatchback' },
    { id: 3, value: 'Hybrids' },
    { id: 4, value: 'Luxury' },
    { id: 5, value: 'Sedan' },
    { id: 6, value: 'SUV' },
    { id: 7, value: 'MUV' },
    { id: 8, value: 'Minivans' }
]

export const LEAD_DATA = [
    { id: 1, color: '#28b797', value: 'New' },
    { id: 2, color: '#ffd341', value: 'Follow Up' },
    { id: 3, color: '#e26531', value: 'Interested' },
    { id: 4, color: '#4fb6e7', value: 'Walk-in Scheduled' },
    { id: 5, color: '#4fb6e7', value: 'Walk-in Done' },
    { id: 6, color: '#5a9e11', value: 'Customer Offer' },
    { id: 7, color: '#5a9e11', value: 'Booked' },
    { id: 8, color: '#e26531', value: 'Converted' },
    { id: 9, color: '#e26531', value: 'Closed' },
    { id: 10, color: '#e26531', value: 'All Lead' }
]

export const OWNER_LIST = [
    { id: '0', name: '1st ' + Strings.OWNER },
    { id: '1', name: '2nd ' + Strings.OWNER },
    { id: '2', name: '3rd ' + Strings.OWNER },
    { id: '3', name: '4th ' + Strings.OWNER },
    { id: '4', name: '4+ ' + Strings.OWNER },
    { id: '6', name: Strings.UNREGISTERED },
]

export const NO_INSURANCE = 'no_insurance'
export const CORPORATE = 'corporate'
export const COMPREHENSIVE = 'comprehensive'
export const INSURANCE_TYPE_LIST = [
    { id: '1', name: Strings.NO_INSURANCE },
    { id: '2', name: Strings.TLO },
    { id: '3', name: Strings.ALL_RISK },
    { id: '4', name: Strings.COMBINED },
]

export const CNG_FITTED = 1
export const CNG_NOT_FITTED = 0

export const STOCK_CAR_STATUS = {
    CAR_INACTIVE: 0,
    CAR_ACTIVE: 1,
    CAR_REMOVED: 2,
    CAR_SOLD: 3,
    CAR_COMING_SOON: 7
}

export const CONNECTION_CHANGE = 'connectionChange'
export const FINAL_DOC_SCREEN = "final_docs_screen"
export const CALCULATOR_IMAGE_SCREEN = 'loan_detail_screen'
export const REQUIRED_WHEN_RULE_ENGINE_AVAILABLE = "2"
export const REQUIRED_WHEN_RULE_ENGINE_NOT_AVAILABLE = "1"
export const HAS_RULE_ENGINE = "1"
export const HAS_NO_RULE_ENGINE = "0"

export const COMPRESS_HEIGHT = 800;
export const COMPRESS_WIDTH = 1000;
export const COMPRESS_RATIO = 80;

export const MOBILE_MIN_LENGTH = APP_TYPE == INDONESIA ? 9 : 7
export const MOBILE_MAX_LENGTH = APP_TYPE == INDONESIA ? 12 : 15
export const REGISTRATION_MAX_LENGTH = APP_TYPE == INDONESIA ? 9 : 7

export const EMI_TENURE = [
    12,
    24,
    36,
    48,
    60
]

export const CUSTOMER_DATA = 'customerData'
export const APPROVED_DATA = 'approvedData'
export const PDF_FILE_TYPE = 'pdf'
export const STOCK_LIST_SYNC = 'stock_list_sync'
export const LOAN_PENDING_SYNC = 'loan_pending_sync'

export const STOCK_INSPECTED = '1'
export const STOCK_NOT_INSPECTED = '0'

export const CALCULATOR_IMAGE = '1'
export const NOT_CALCULATOR_IMAGE = '0'
export const STOCK_CLASSIFIED = '1'
export const STOCK_NOT_CLASSIFIED = '0'
export const STOCK_PREMIUM = '1'
export const STOCK_NOT_PREMIUM = '0'
export const STOCK_BUMPUP_ENABLE = 1
export const STOCK_BUMPUP_NOT_ENABLE = 0
export const IS_UNREAD = 'is_unread'
export const IS_READ = 'is_read'
export const DEALER_SKU = 'dealer_sku'
export const DEALER_SKU_INFO_URL = 'dealer_sku_info_url'
export const VEHICLE_NUMBER_TYPE = 'vehicle_number_type'
export const TOTAL_FINANCIER_MMV = 'total_financier_mmv'