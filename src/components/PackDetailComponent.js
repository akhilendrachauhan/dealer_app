import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import * as ImageAssets from '../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../values'
import { Utility } from '../util'
const PackDetailComponent = ({ item, index,dealer_sku,purchasePackage,renewPackage}) => {
    let detailHeadText= Strings.DETAIL_HEAD_TEXT,detailText= Strings.LISTING,quotaText = Strings.QUOTA,usedText = Strings.USED, remainingText = Strings.REMAINING,showEndDate = true,showBuyButton = true,showRenewButton = false 
    if(!item.toPurchase){
        var sku = dealer_sku.filter(obj => {
            return obj.id === item.b_details_sku_id
        })
        if(sku[0].id == 1){
            if(item.bd_attr_attribute_value == "POST_LIST"){
                detailText = Strings.NUMBER_OF_LISTINGS
                showEndDate = false
            }
            else if(item.bd_attr_attribute_value == "ACTIVE_LIST"){
                detailText = Strings.NUMBER_OF_ACTIVE_LISTINGS
                
            }
            else if(item.bd_attr_attribute_value == "COMMITMENT_LIST"){
                detailText = Strings.SALES_COMMITMENT
                quotaText = Strings.COMMITMENT
                usedText = Strings.CARS_SOLD
                showEndDate = false
            }
        }
        else if(sku[0].id == 6){
            detailText = Strings.NUMBER_OF_FEATURED_LISTINGS
        }
        else if(sku[0].id == 35){
            detailText = Strings.NUMBER_OF_BUMPUP
        }

        let nextReNewDate = item.next_renew_date
        if(nextReNewDate && nextReNewDate !=""){
            let nextReNewTime = new Date(nextReNewDate).getTime()
            let currentTime = new Date().getTime()
            //alert(item.next_renew_date +":"+new Date().toISOString())
            if(currentTime > nextReNewTime){
                showRenewButton = true
            }
            else{
                showRenewButton = false
            }
        }
    }
    else{
        
        let nextReNewDate = item.next_buy_req_date
        if(nextReNewDate && nextReNewDate !=""){
            let nextReNewTime = new Date(nextReNewDate).getTime()
            let currentTime = new Date().getTime()
            //alert(item.next_buy_req_date +":"+new Date().toISOString())
            if(currentTime > nextReNewTime){
                showBuyButton = true
            }
            else{
                showBuyButton = false
            }
            
        }
        // let requestSentTime = item.requestSubmitTime
        // let TimeForNextRequest = 24*60*60
        // let currentTime = parseInt(new Date().getTime()/1000)
        // if(requestSentTime == 0){
        //     showBuyButton = true
        // }
        // else if((requestSentTime + TimeForNextRequest) < currentTime){
        //     showBuyButton = true
        // }
        //alert(JSON.stringify(item)+":"+(requestSentTime + TimeForNextRequest)+":"+TimeForNextRequest+":"+currentTime)
    }
    
    return (
        item.toPurchase ? 
        <View style={Styles.container}>
            <View style={[Styles.containerInner,{paddingTop : 0}]}>
                <View style={{alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[Styles.normalText,{fontWeight:'bold',fontSize:Dimens.text_large,color: Colors.BLACK}]}>{item.sku_name}</Text>
                </View>

                <View style={Styles.dashedView} />

            <View style={{justifyContent:'center',alignItems:'center'}}>
                <Text style={[Styles.headText,{textAlign: 'justify'}]}>{item.can_buy && item.next_buy_req_date && item.next_buy_req_date != "" &&  !showBuyButton ? Strings.PACK_BUY_REQUEST_SUBMITTED  :Strings.NO_ACTIVE_PACK_AVAILABLE}</Text>
            </View>

                
            </View>
            

            { item.can_buy && showBuyButton ? <View style={{ alignItems: 'flex-end', justifyContent: 'center',marginTop: 10}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ paddingHorizontal: Dimens.padding_x_large,paddingVertical: Dimens.padding_small, alignItems: 'center', justifyContent: 'center',borderRadius: 0,borderWidth:1,borderColor: Colors.PRIMARY }}
                    onPress={() => purchasePackage(item.id)}>

                    <Text style={Styles.buttonText}>{Strings.PURCHAGE_PACKAGE}</Text>
                </TouchableOpacity>
            </View> : <View/>}
        </View>
        :
        item.sku_sku_type == 'service' && item.b_details_is_active == "1" ? <View style={Styles.container}>
            <View style={[Styles.containerInner,{paddingTop : 0}]}>
                <View style={{alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[Styles.normalText,{fontWeight:'bold',fontSize:Dimens.text_large,color: Colors.BLACK}]}>{sku[0].sku_name}</Text>
                </View>

                <View style={Styles.dashedView} />

            <View style={{flexDirection: 'row'}}>
                <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{Strings.PACK_START_DATE}</Text>
                    <Text style={[Styles.labelText,{ color: Colors.PRIMARY }]}>{Utility.getValidFormatedDateTime(item.b_details_validity_from, 'dd-mmm-yyyy')}</Text>
                </View>
                { showEndDate ? <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{Strings.PACK_END_DATE}</Text>
                    <Text style={[Styles.labelText,{ color: Colors.PRIMARY }]}>{Utility.getValidFormatedDateTime(item.b_details_validity_to, 'dd-mmm-yyyy')}</Text>
                </View>: <View/>}
            </View>

                {/* <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between',paddingLeft:Dimens.padding_large,paddingRight:Dimens.padding_large }}>
                    <Text numberOfLines={1} style={Styles.headText}>{Strings.PACK_START_DATE}</Text>
                    <Text numberOfLines={1} style={[Styles.normlText, { color: Colors.PRIMARY }]}>{Utility.getValidFormatedDateTime(item.b_details_validity_from, 'dd-mmm-yyyy')}</Text>
                </View>
                <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between',paddingLeft:Dimens.padding_large,paddingRight:Dimens.padding_large }}>
                    <Text numberOfLines={1} style={Styles.headText}>{Strings.PACK_END_DATE}</Text>
                    <Text numberOfLines={1} style={[Styles.normlText, { color: Colors.PRIMARY }]}>{ Utility.getValidFormatedDateTime(item.b_details_validity_to, 'dd-mmm-yyyy')}</Text>
                </View> */}
            </View>
            <View style={{flexDirection: 'row'}}>
                <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{detailHeadText}</Text>
                    <Text style={Styles.labelText}>{detailText}</Text>
                </View>
                <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{quotaText}</Text>
                    <Text style={Styles.labelText}>{item.quota ? item.quota : 0}</Text>
                </View>
             </View>
             <View style={{flexDirection: 'row'}}>   
                <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{usedText}</Text>
                    <Text style={Styles.labelText}>{item.quota_used ? item.quota_used : 0}</Text>
                </View>
                <View style={{ flex: 1, margin: Dimens.margin_small }}>
                    <Text style={Styles.modalText}>{remainingText}</Text>
                    <Text style={Styles.labelText}>{item.quota_remaining ? item.quota_remaining : 0}</Text>
                </View>
            </View>
            <View style={{justifyContent:'center',alignItems:'center'}}>
                <Text style={[Styles.headText,{textAlign: 'justify'}]}>{item.next_renew_date && item.next_renew_date !="" && !showRenewButton ? Strings.PACK_RENEW_REQUEST_SUBMITTED : ""}</Text>
            </View>
            { item.can_renew && showRenewButton ? <View style={{ alignItems: 'flex-end', justifyContent: 'center',marginTop: 10}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ paddingHorizontal: Dimens.padding_x_large,paddingVertical: Dimens.padding_small, alignItems: 'center', justifyContent: 'center',borderRadius: 0,borderWidth:1,borderColor: Colors.PRIMARY }}
                    onPress={() => renewPackage(sku[0].id,item.b_details_id,item.b_id)}>

                    <Text style={Styles.buttonText}>{Strings.RENEW}</Text>
                </TouchableOpacity>
            </View> : <View/>}
        </View> : <View/>
        
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        overflow: 'hidden',
        borderRadius: 5,
        backgroundColor: Colors.WHITE,
        marginVertical: Dimens.margin_x_small,
        padding : Dimens.padding_xx_large
    },
    containerInner: {
        flex: 1,
        //flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 0,
        padding: Dimens.padding_normal
    },
    
    headText: {
        flex:1,
        fontWeight: 'bold',
        color: Colors.BLACK_85,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        marginRight: Dimens.margin_x_small,
        textAlign:'left'
    },
    normlText: {
        flex:1,
        textAlign: 'left',
        color: Colors.PRIMARY,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
    },
    
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    
    dashedView: {
        height: 2,
        width: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER,
        marginVertical : 10,
    },
    modalText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
        fontWeight:'bold'
    },
    labelText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    buttonText: {
        color: Colors.PRIMARY,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT
    },
})

export default PackDetailComponent;