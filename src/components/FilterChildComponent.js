import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native'
import {Colors, Strings} from "../values";

let checkedIcon = require('../assets/drawable/checked.png');
let unCheckedIcon = require('../assets/drawable/unchecked.png');
export default class FilterChildComponent extends Component {
    constructor() {
        super();
        this.onPress = this.onPress.bind(this);
    }

    onPress() {
        this.props.onFilterOptionClick(this.props.index);
    }

    render() {
        let data = this.props.data;
        return (
            <View style={{paddingHorizontal: 20}}>
                <TouchableOpacity activeOpacity={1} onPress={this.onPress}
                                  style={{flexDirection: 'row', paddingVertical: 16, alignItems: 'center'}}>
                    <Image style={{height: 18, width: 18}} source={data.selected ? checkedIcon : unCheckedIcon}/>
                    <Text style={{
                        paddingLeft: 20,
                        fontSize: 16,
                        color: Colors.BLACK_70,
                        fontFamily: Strings.APP_FONT
                    }}>{data.value}</Text>
                </TouchableOpacity>
                <View style={{height: 1, backgroundColor: Colors.BLACK_25}}/>
            </View>

        )
    }
}