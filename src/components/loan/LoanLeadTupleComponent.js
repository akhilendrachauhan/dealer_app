import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import * as ImageAssets from '../../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../../values'

const LoanLeadTupleComponent = ({ item, index, callback, onViewDocument, onCallPress, onHistoryPress, isPending }) => {
    return (
        // <TouchableOpacity
        //     activeOpacity={0.8}
        //     onPress={() => callback(item)}>

        <View style={Styles.container}>
            <View style={Styles.containerInner}>
                <View style={{ flex: 0.25, alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: 25, height: 25, resizeMode: 'contain', tintColor: Colors.BLACK }}
                        source={ImageAssets.icon_loan_box} />
                    <Text style={Styles.normalText}>{item.id}</Text>
                    <Text style={Styles.smallText}>{Strings.LOAN_ID}</Text>
                </View>

                <View style={Styles.dashedView} />

                <View style={{ flex: 0.75, paddingLeft: Dimens.padding_xx_large }}>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text numberOfLines={2} style={Styles.headText}>{item.customer.name}</Text>
                        <Text numberOfLines={1} style={[Styles.normlText, { color: Colors.PRIMARY }]}>{item.sub_loan_status.name}</Text>
                    </View>

                    <Text style={[Styles.normalText, { marginTop: 8 }]}>{item.carinfo ? item.carinfo.reg_number + ' (' + item.carinfo.mmv_name + ')' : Strings.NA}</Text>
                    <Text style={[Styles.normalText, { marginTop: 5 }]}>{item.companyInfo && item.companyInfo.name ? Strings.FINANCIER + " : " + item.companyInfo.name : Strings.NA}</Text>
                    <Text style={[Styles.normalText, { marginTop: 5 }]}>{item.created_date ? Strings.ADDED_ON + item.created_date : Strings.NA}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', marginHorizontal: 16, alignItems: 'center', justifyContent: 'space-between', borderTopWidth: 1, borderTopColor: Colors.BLACK_25, }}>

                {isPending ?
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => onViewDocument(item)}>

                        <Text style={Styles.documentText}>{Strings.UPLOAD_DOCUMENTS}</Text>
                    </TouchableOpacity>
                    :
                    item.documents.length > 0 ?
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => onViewDocument(item)}>

                            <Text style={Styles.documentText}>{Strings.VIEW_DOCUMENTS}</Text>
                        </TouchableOpacity>
                        :
                        <View style={{ width: 100 }}></View>
                }

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => onHistoryPress(item)}>

                    <Text style={Styles.documentText}>{Strings.HISTORY}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => onCallPress(item)}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 10 }}>
                        <Image style={{ width: 22, height: 22, tintColor: Colors.GREEN_CALL, marginRight: 5 }}
                            source={ImageAssets.icon_call}
                        />
                        <Text style={Styles.buttonStyle}>{Strings.CALL}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
        // </TouchableOpacity>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        overflow: 'hidden',
        borderRadius: 5,
        backgroundColor: Colors.WHITE,
        marginVertical: Dimens.margin_x_small
    },
    containerInner: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 0,
        padding: Dimens.padding_normal
    },
    buttonStyle: {
        fontWeight: '600',
        color: Colors.GREEN_CALL,
        fontSize: Dimens.text_13,
        fontFamily: Strings.APP_FONT,
    },
    headText: {
        flex: 0.8,
        fontWeight: 'bold',
        color: Colors.BLACK_85,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
        marginRight: Dimens.margin_x_small,
    },
    normlText: {
        flex: 1.25,
        textAlign: 'right',
        color: Colors.PRIMARY,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
    },
    documentText: {
        fontSize: Dimens.text_13,
        color: Colors.PRIMARY,
        fontFamily: Strings.APP_FONT
    },
    timeText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_85,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    smallText: {
        fontSize: Dimens.text_x_small,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    statusText: {
        position: 'absolute',
        right: 6,
        textAlign: 'center',
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    containerTag: {
        borderRadius: 8,
        marginLeft: Dimens.margin_x_small,
        paddingHorizontal: Dimens.padding_small,
        paddingVertical: Dimens.padding_xx_small,
    },
    tagText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    containerbadge: {
        width: '100%',
        position: 'absolute',
        top: 0, left: -35,
        backgroundColor: Colors.GREEN_CALL,
        transform: [{ rotate: '315deg' }]
    },
    badgeText: {
        color: Colors.WHITE,
        textAlign: 'center',
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_xx_small,
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    }
})

export default LoanLeadTupleComponent;