import React, { Component } from 'react'
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native'
import { Colors, Strings, Dimens } from "../values/index"

class LoadingComponent extends Component {

    constructor(props) {

        super(props);

    }

    render() {
        return (
            <View style={{
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                bottom: 0,
                backgroundColor: this.props.backgroundColor || Colors.BLACK_40,
                top: 0,
                right: 0,
                left: 0,
                elevation: this.props.elevation || 0
            }}>
                <ActivityIndicator size="large" color={Colors.PRIMARY} />
                {this.props.msg
                    ?
                    <Text style={{
                        textAlign: 'center',
                        color: Colors.WHITE,
                        fontSize: Dimens.text_large,
                        fontFamily: Strings.APP_FONT,
                        padding: Dimens.padding_large,
                    }}>{this.props.msg}</Text>
                    :
                    null
                }
            </View>
        )
    }
}

export default LoadingComponent