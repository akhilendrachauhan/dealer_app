import React, { Component } from "react"
import { StyleSheet, FlatList, Image, View, Text } from "react-native"
import * as ImageAssets from '../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../values'
import { Utility } from '../util'

export default class Timeline extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: this.props.data,
        }
    }

    UNSAFE_componentWillReceiveProps() {
        this.setState({
            data: this.props.data
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    ref={(ref) => { this.flatListRef = ref }}
                    extraData={this.state}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => '' + index}
                    showsVerticalScrollIndicator={false}
                    automaticallyAdjustContentInsets={false}
                />
            </View>
        )
    }

    _renderItem = ({ item, index }) => {
        return (
            <View style={styles.containerItem}>
                {/* Dotted line view */}
                <View style={{ width: 16, marginLeft: 5, alignItems: 'center' }}>
                    <View style={styles.dottedView} />

                    <View style={styles.containerWhite} />

                    <View style={styles.circleView} />
                </View>

                <View style={styles.containerInnerItem}>

                    {/* Date Time View */}
                    {!this.props.isLoanTimeline ?
                        <View>
                            <Text style={styles.titleText}>{Utility.getFormatedDateTime(item.datetime, 'mmm dd')}</Text>
                            <Text style={styles.descText}>{Utility.getFormatedDateTime(item.datetime, 'hh:MM TT')}</Text>
                        </View>
                        :
                        <View>
                            <Text style={styles.titleText}>{Utility.getValidFormatedDateTime(item.created_date, 'mmm dd')}</Text>
                            <Text style={styles.descText}>{Utility.getValidFormatedDateTime(item.created_date, 'hh:MM TT')}</Text>
                        </View>
                    }

                    {/* Title View */}
                    {!this.props.isLoanTimeline ?
                        <View style={{ flex: 1, marginLeft: 25 }}>
                            <Text style={styles.titleText}>{item.activity_text}</Text>
                            <Text style={styles.descText}>{item.comment}</Text>
                        </View>
                        :
                        <View style={{ flex: 1, marginLeft: 25 }}>
                            <Text style={styles.titleText}>{item.loan_status ? item.loan_status.name : Strings.NA}</Text>
                            <Text style={styles.descText}>{item.loan_sub_status ? '(' + item.loan_sub_status.name + ')' : Strings.NA}</Text>
                        </View>
                    }

                    {/* Icon View */}
                    {!this.props.isLoanTimeline ?
                        <View>
                            <Image source={this.getIcon(item.shared_by)}
                                style={{ width: 24, height: 24, resizeMode: 'contain' }}
                            />
                        </View>
                        :
                        <View style={{ flex: 0.65 }}>
                            <Text numberOfLines={1} style={[styles.descText, { marginTop: 0 }]}>{item.user ? item.user.name : Strings.NA}</Text>
                        </View>
                    }
                </View>
            </View>
        )
    }

    getIcon = (sharedBy) => {
        if (sharedBy == 'SMS') {
            return ImageAssets.icon_sms
        }
        else if (sharedBy == 'WHATSAPP') {
            return ImageAssets.icon_whatsapp_logo
        }
        else if (sharedBy == 'EMAIL') {
            return ImageAssets.icon_mail
        }
        else {
            return null
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerItem: {
        flex: 1,
        flexDirection: 'row'
    },
    containerInnerItem: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 0.6,
        justifyContent: 'space-between',
        borderBottomColor: Colors.BLACK_25,
        marginLeft: Dimens.margin_xxxx_large,
        paddingVertical: Dimens.padding_xx_large
    },
    titleText: {
        color: Colors.BLACK_87,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT,
    },
    descText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    dottedView: {
        borderWidth: 3,
        borderRadius: 1,
        width: 6,
        height: '100%',
        borderStyle: 'dotted',
        borderColor: Colors.BLACK_40,
        backgroundColor: Colors.WHITE,
    },
    containerWhite: {
        top: 0,
        right: 0,
        bottom: 0,
        width: 9,
        height: '100%',
        position: 'absolute',
        backgroundColor: Colors.WHITE,
    },
    circleView: {
        width: 14,
        height: 14,
        right: 0,
        top: 17,
        left: -1,
        position: 'absolute',
        borderRadius: 7,
        borderWidth: 2,
        borderColor: Colors.PRIMARY,
        backgroundColor: Colors.WHITE,
    }
});