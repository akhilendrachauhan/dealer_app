import React, { Component } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, } from 'react-native';
import WeekDateComponent from './WeekDateComponent'
import { Colors, Strings } from '../values'
import * as Utility from "../util/Utility"
import * as ImageAssets from '../assets/ImageAssets'
import PropTypes from 'prop-types'
/**  
 *
 *    CUSTOMIZABLE ITEMS :
 *    barColor: bottom bar color in slected item, default
 *   Colors.DEFAULT_DATE_BAR_COLOR
 *    dayTextColor: default
 *   Colors.BLACK_OPACITY_54
 *    dateTextColor: default Colors.BLACK_OPACITY_80,
 *   background: default Colors.WHITE
 *   arrowColor: Color of the arrow, default : Colors.WHITE
 *   arrowBgColor: Background color of the arrow circle, default : Colors.WHITE
 *    DEPENDENCY :
 *    Props -
 *
 *   onDateSelected: function to handle selected date
 *
 */

const monthNames = [Strings.JAN, Strings.FEB, Strings.MAR, Strings.APR, Strings.MAY,
Strings.Jun, Strings.JUL, Strings.AUG, Strings.SEP, Strings.OCT, Strings.NOV, Strings.DEC]

const days = [Strings.SUNDAY, Strings.MONDAY, Strings.TUESDAY,
Strings.WEDNESDAY, Strings.THURSDAY, Strings.FRIDAY, Strings.SATURDAY]

class WeekDateStrip extends Component {

    static defaultProps = {
        barColor: Colors.PRIMARY,
        dayTextColor: Colors.WHITE_54,
        dateTextColor: Colors.WHITE_54,
        background: Colors.SCREEN_BACKGROUND,
        arrowColor: Colors.WHITE,
        arrowBgColor: Colors.SCREEN_BACKGROUND,
        onDateSelected: function (date) {
            Utility.log("Unhandled Callback", date);
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            positionSelected: 0,
            selected: false,
        };
    }

    getDates111() {
        let firstday;
        let totalDays = 0;
        let defaultDate = new Date();
        if (this.props.startDate) {
            firstday = this.props.startDate;
            let endDate = this.props.endDate ? this.props.endDate :
                new Date(defaultDate.getFullYear(), defaultDate.getMonth(), defaultDate.getDate());
            let datediff = this.datediff(this.props.startDate, endDate);
            totalDays = datediff + 1; //Since we need to include the last day as well
            defaultDate = this.props.defaultDate ? this.props.defaultDate : defaultDate;
        } else {
            let curr = new Date(); // get current date
            let first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            firstday = curr.setDate(first)
            firstday = new Date(firstday.getFullYear(), firstday.getMonth(), firstday.getDate() + 1);
            totalDays = 7;
        }

        let items = [];
        let positionSelected = 0;
        defaultDate = new Date(defaultDate.getFullYear(), defaultDate.getMonth(), defaultDate.getDate());//To set the time in the date to 000 to compare the dates equality
        for (let i = 0; i < totalDays; i++) {
            items.push(firstday);
            if (this.isEqualDate(firstday, defaultDate)) {
                positionSelected = i;
            }
            firstday = this.getNextDate(firstday);
        }

        this.setState({
            data: items,
            positionSelected: positionSelected
        });
    }

    getDates() {
        let firstday;
        let totalDays = 7;
        let items = [];

        firstday = new Date()

        for (let i = 0; i < totalDays; i++) {
            items.push(firstday)
            firstday = this.getNextDate(firstday)
        }

        this.setState({ data: items })
        // console.log('Dateitems==>', JSON.stringify(items))
    }

    datediff(startDate, endDate) {
        return Math.round((endDate - startDate) / (1000 * 60 * 60 * 24));
    }

    isEqualDate(first, second) {
        return !(first < second) && !(first > second);
    }

    getNextDate(date) {
        var d2 = new Date(date.getTime() + 86400000)
        return d2;
    }

    UNSAFE_componentWillMount() {
        this.getDates();
    }

    render() {
        return (
            <View style={styles.parent}>
                {this.getDateStrip()}
            </View>
        );
    }

    getDateStrip() {
        let selectionArray = [true, false, false];
        let positionSelected = this.state.positionSelected;
        // if (this.state.positionSelected == 0) {
        //     selectionArray[0] = true;
        //     positionSelected = 0;
        // } else if (this.state.positionSelected == this.state.data.length - 1) {
        //     selectionArray[2] = true;
        //     positionSelected = this.state.data.length - 2;
        // } else {
        //     selectionArray[1] = true;
        // }
        // console.log('positionSelected==> ' + positionSelected)
        // console.log('positionSelectedDate==> ' + this.state.data[positionSelected])
        return (
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.arrowsContainer}
                    onPress={() => this.onLeftClick()}>

                    <Image style={[styles.arrow, { tintColor: this.state.positionSelected > 0 ? Colors.WHITE : Colors.WHITE_25 }]}
                        source={ImageAssets.icon_arrow_left} />
                </TouchableOpacity>

                <WeekDateComponent
                    position={0}
                    selected={selectionArray[0]}
                    action={(position) => { this.onDateSelected(position) }}
                    dayText={days[this.state.data[positionSelected].getDay()]}
                    dateText={monthNames[this.state.data[positionSelected].getMonth()] + " " +
                        this.state.data[positionSelected].getDate() + ", " +
                        this.state.data[positionSelected].getFullYear()}
                />

                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.arrowsContainer}
                    onPress={() => this.onRightClick()}>

                    <Image style={[styles.arrow, { tintColor: this.state.positionSelected < this.state.data.length - 1 ? Colors.WHITE : Colors.WHITE_25 }]}
                        source={ImageAssets.icon_arrow_right} />
                </TouchableOpacity>
            </View>
        )
    }

    onDateSelected(position) {
        if (position > 1) {
            this.onRightClick();
            return;
        }
        if (position < 1) {
            this.onLeftClick();
            return;
        }
        if (position == 1) {
            if (this.state.positionSelected == this.state.data.length - 1) {
                this.onLeftClick();
                return;
            }
            if (this.state.positionSelected == 0) {
                this.onRightClick();
            }
        }
    }

    onDateChanged(position) {
        var date = this.state.data[position]
        this.props.onDateSelected(date, position);
        // console.log('onDateChangedPos==> '+ position)
        // console.log('onDateChangedDate==> '+ JSON.stringify(date))
    }

    onLeftClick() {
        if (this.state.positionSelected > 0) {
            this.setState({ positionSelected: this.state.positionSelected - 1 }, () => {
                this.onDateChanged(this.state.positionSelected);
            })
        }
    }

    onRightClick() {
        if (this.state.positionSelected < this.state.data.length - 1) {
            this.setState({ positionSelected: this.state.positionSelected + 1 }, () => {
                this.onDateChanged(this.state.positionSelected);
            })
        }
    }
}

WeekDateStrip.propTypes = {
    barColor: PropTypes.string,
    dayTextColor: PropTypes.string,
    dateTextColor: PropTypes.string,
    background: PropTypes.string,
    arrowColor: PropTypes.string,
    arrowBgColor: PropTypes.string,
    onDateSelected: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    parent: {
        flexDirection: 'row',
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    arrowsContainer: {
        height: 30,
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    arrow: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        tintColor: Colors.WHITE
        // tintColor: Colors.WHITE_25
    }
});

export default WeekDateStrip;
