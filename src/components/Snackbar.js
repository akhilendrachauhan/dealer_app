import React, { Component } from 'react';
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native';
import { Colors, Strings } from "../values";

class Snackbar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let text = this.props.text || 'Updating data...';
        return (
            <View style={{
                position: 'absolute',
                bottom: 0,
                backgroundColor: this.props.backgroundColor || Colors.BLACK_85,
                right: 0,
                left: 0,
                height: 40,
                elevation: 5
            }}>
                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ color: Colors.WHITE_90, fontSize: 12, paddingHorizontal: 15, fontFamily: Strings.APP_FONT }}>{text}</Text>
                    <ActivityIndicator style={{ paddingHorizontal: 15 }} size="small" color={Colors.PRIMARY} />
                </View>
            </View>
        );
    }
}

export default Snackbar;