import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Colors, Strings } from '../values/index'

/* *
*
*    CUSTOMIZABLE ITEMS :
*    barColor: bottom
*    bar color in slected item, default Colors.DEFAULT_DATE_BAR_COLOR
*
*    dayTextColor: default Colors.BLACK_OPACITY_54
*    dateTextColor: default
*    Colors.BLACK_OPACITY_80
*
*    DEPENDENCIES :
*    dayText : day of the week
*    dateText : date of the month
*    action : onClickAction
*    position :
*    position of component among calendar dates.
*/

class WeekDateComponent extends Component {

  static defaultProps = {
    selected: false,
    // barColor: Colors.color_primary,
    // dayTextColor: Colors.DAY_TEXT_COLOR,
    // dateTextColor: Colors.DAY_TEXT_COLOR,
    barColor: Colors.PRIMARY,
  }

  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    this.state.selected = nextProps.selected;
    return true;
  }

  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.containerStyle}
        onPress={() => this.props.action(this.props.position)}>

        <View style={styles.innerContainer}>
          <Text allowFontScaling={false} style={styles.dayStyle}>{this.props.dayText}, </Text>
          <Text allowFontScaling={false} style={styles.dateStyle}>{this.props.dateText}</Text>

          {/* {this.getSelector()} */}
        </View>
      </TouchableOpacity>
    );
  }

  toggleSelected() {
    if (!this.state.selected) {
      this.setState({
        selected: !this.state.selected
      });
    }
  }

  getSelector() {
    var barColor = '#ff000000';
    if (this.state.selected) {
      barColor = Colors.PRIMARY;
    }
    return (<View
      style={[
        styles.barStyle, {
          backgroundColor: barColor
        }
      ]} />);
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },

  innerContainer: {
    // justifyContent: 'center',
    flexDirection: 'row',
    // alignItems: 'center',
  },

  barStyle: {
    height: 2,
    marginLeft: 10,
    marginRight: 10
  },

  dayStyle: {
    color: Colors.WHITE_54,
    fontSize: 14,
    fontFamily: Strings.APP_FONT
  },

  dateStyle: {
    color: Colors.WHITE_54,
    fontSize: 14,
    fontFamily: Strings.APP_FONT
  }
});

export default WeekDateComponent;

