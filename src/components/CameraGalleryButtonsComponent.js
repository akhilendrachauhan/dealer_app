import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import {Colors, Strings, Dimens} from "../values";

const CameraGalleryButtonsComponent = ({onCameraPress, onGalleryPress,onPDFPress}) => {
    return (
        <View style={{
            width: '100%',
            padding: 20,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: Colors.WHITE,
            justifyContent: 'space-between'
        }}>
            <TouchableOpacity onPress={onCameraPress} style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image resizeMode={'cover'} style={{height: 42, width: 42}}
                       source={require('../assets/drawable/camera.png')}/>
                <Text style={{
                    fontSize: Dimens.text_large,
                    fontFamily: Strings.APP_FONT,
                    color: Colors.BLACK,
                    marginLeft: Dimens.margin_normal
                }}>{Strings.CAMERA}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onGalleryPress} style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image resizeMode={'cover'} style={{height: 42, width: 42}}
                       source={require('../assets/drawable/gallery_icon.png')}/>
                <Text style={{
                    fontSize: Dimens.text_large,
                    fontFamily: Strings.APP_FONT,
                    color: Colors.BLACK,
                    marginLeft: Dimens.margin_normal
                }}>{Strings.GALLERY}</Text>
            </TouchableOpacity>
            { onPDFPress && <TouchableOpacity onPress={onPDFPress} style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image resizeMode={'contain'} style={{height: 42, width: 42}}
                       source={require('../assets/drawable/pdf_color.png')}/>
                <Text style={{
                    fontSize: Dimens.text_large,
                    fontFamily: Strings.APP_FONT,
                    color: Colors.BLACK,
                    marginLeft: Dimens.margin_normal
                }}>{Strings.PDF}</Text>
            </TouchableOpacity>
            }

        </View>
    )

};

export default CameraGalleryButtonsComponent;
