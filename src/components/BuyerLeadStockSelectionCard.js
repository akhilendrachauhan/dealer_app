import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native'
import { Utility } from '../util'
import { Colors, Strings, Dimens } from '../values'
import * as ImageAssets from '../assets/ImageAssets'
import DotView from "../granulars/DotView";

export default class BuyerLeadStockSelectionCard extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let item = this.props.item;
        let index = this.props.index;
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.props.onStockItemPress(item, index)}>

                <View style={[Styles.containerItem, { borderColor: item.selected ? Colors.PRIMARY : Colors.BLACK_25 }]}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View>
                            <View style={Styles.imageContainer}>
                                <Image style={Styles.image}
                                    source={item.car_profile_image == '' ? ImageAssets.icon_no_image : { uri: item.car_profile_image }} />
                            </View>
                        </View>

                        <View style={Styles.detailContainer}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text numberOfLines={2}
                                    style={Styles.headText}>{item.make + ' ' + item.modelVersion}</Text>

                                <View style={{ marginLeft: Dimens.margin_normal }}>

                                    <Image style={{ width: 18, height: 18, resizeMode: 'contain' }}
                                        source={item.selected ? ImageAssets.icon_checked : ImageAssets.icon_unchecked} />
                                </View>
                            </View>

                            <View>
                                <Text numberOfLines={1}
                                    style={Styles.boldText}>{Utility.formatCurrency(item.car_price)}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={Styles.divider} />


                    <View style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <Text
                            style={Styles.descText}>{item.km_driven == '' ? Strings.NA : item.km_driven + ' ' + Strings.KM}</Text>
                        <DotView />
                        <Text style={Styles.descText}>{item.make_year || Strings.NA}</Text>
                        <DotView />
                        <Text style={Styles.descText}>{item.fuel_type || Strings.NA}</Text>
                        <DotView />
                        <Text style={Styles.descText}>{item.reg_no || Strings.NA}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.SCREEN_BACKGROUND
    },
    containerItem: {
        overflow: 'hidden',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_xx_large,
        borderRadius: Dimens.border_radius,
        borderWidth: 2,
        width: Dimensions.get('window').width - 40
    },
    containerItem1: { // bottom Radius 0
        overflow: 'hidden',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: Dimens.padding_xx_large,
        borderTopEndRadius: Dimens.border_radius,
        borderTopStartRadius: Dimens.border_radius,
        width: Dimensions.get('window').width - 40
    },
    detailContainer: {
        flex: 1,
        height: 88,
        justifyContent: 'space-between',
        marginLeft: Dimens.margin_xx_large,
    },
    imageContainer: {
        width: 131,
        height: 88
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    },
    soldContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLACK_54
    },
    soldText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.WHITE,
        fontFamily: Strings.APP_FONT
    },
    containerbadge: {
        width: '100%',
        position: 'absolute',
        top: -8, left: -64,
        transform: [{ rotate: '315deg' }]
    },
    badgeText: {
        color: Colors.WHITE,
        textAlign: 'center',
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_xx_small,
    },
    headText: {
        flex: 1,
        fontSize: Dimens.text_large,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT
    },
    descText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_54,
        fontFamily: Strings.APP_FONT
    },
    smallText: {
        color: Colors.BLACK_40,
        fontSize: Dimens.text_small,
        fontFamily: Strings.APP_FONT
    },
    boldText: {
        color: Colors.BLACK_87,
        fontWeight: 'bold',
        fontSize: Dimens.text_x_large,
        fontFamily: Strings.APP_FONT
    },
    divider: {
        height: 0.9, width: '100%',
        backgroundColor: Colors.DIVIDER,
        marginVertical: Dimens.padding_large
    }
})