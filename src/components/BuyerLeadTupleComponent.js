import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { Utility, Constants } from '../util'
import * as ImageAssets from '../assets/ImageAssets'
import { Strings, Colors, Dimens } from '../values'

const BuyerLeadTupleComponent = ({ item, index, callback }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => callback(item)}>

            <View style={Styles.container}>

                <View style={{ width: 5, backgroundColor: item.color_code || Colors.WHITE }} />

                <View style={Styles.containerInner}>
                    <View style={{ flex: 0.25, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: 75, height: 60, alignItems: 'center', justifyContent: 'center' }}>
                            {Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_CONVERTED ?   // Converted
                                <Image style={{ width: '90%', height: '90%', resizeMode: 'contain' }}
                                    source={ImageAssets.icon_convert} />
                                :
                                Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_CLOSE ?    // Closed
                                    <Image style={{ width: '90%', height: '90%', resizeMode: 'contain' }}
                                        source={ImageAssets.icon_closed} />
                                    :
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <Image style={{ width: Dimens.icon_normal, height: Dimens.icon_normal, tintColor: Utility.getDateTimeColor(item.lead_follow_date) }}
                                            source={ImageAssets.icon_time} />

                                        <Text numberOfLines={1} style={Styles.timeText}>{Utility.getDateTime(item.lead_follow_date)}</Text>
                                        {/* <Text numberOfLines={1} style={Styles.timeText}>{Utility.getDateTime(item.lead_follow_date_formated)}</Text> */}
                                        <Text numberOfLines={1} style={Styles.smallText}>{item.status_name}</Text>
                                    </View>
                            }
                        </View>

                        {item.opt_verified == 1 &&
                            <View style={Styles.containerbadge}>
                                <Text numberOfLines={1} style={Styles.badgeText}>{Strings.OTP_VERIFIED}</Text>
                            </View>
                        }
                    </View>

                    <View style={Styles.dashedView} />

                    <View style={{ flex: 0.75, paddingLeft: Dimens.padding_xx_large }}>
                        <View style={{ flexDirection: 'row', marginBottom: Dimens.margin_x_small, justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text numberOfLines={1} style={Styles.headText}>{item.customer_name || Strings.NA}</Text>

                            {Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_CUSTOMER_OFFER ?          // Customer Offer
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Image style={{ width: 95, height: 25, tintColor: Colors.LEAD_BADGE_YELLOW, resizeMode: 'stretch' }}
                                        source={ImageAssets.icon_lg_status} />
                                    <Text numberOfLines={1} style={Styles.statusText}>{item.status_name}</Text>
                                </View>
                                :
                                Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_BOOKED ?               // Booked
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <Image style={{ width: 60, height: 25, tintColor: Colors.LEAD_BADGE_GREEN, resizeMode: 'stretch' }}
                                            source={ImageAssets.icon_lg_status} />
                                        <Text numberOfLines={1} style={Styles.statusText}>{item.status_name}</Text>
                                    </View>
                                    :
                                    Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_WALKED_IN ?        // Walk-in Done
                                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <Image style={{ width: 90, height: 25, tintColor: Colors.LEAD_BADGE_BLUE, resizeMode: 'stretch' }}
                                                source={ImageAssets.icon_lg_status} />
                                            <Text numberOfLines={1} style={Styles.statusText}>{item.status_name}</Text>
                                        </View>
                                        :
                                        null
                            }
                            {/* TODO lead_rating Color should be change aftre fix */}
                            {item.lead_rating != '' &&
                                <View style={[Styles.containerTag, { backgroundColor: Colors.BUYER_LEAD_RED }]}>
                                    <Text numberOfLines={1} style={Styles.tagText}>{item.lead_rating}</Text>
                                </View>
                            }
                        </View>
                        <Text numberOfLines={1} style={Styles.normalText}>{item.model ? item.makeModel : 'N/A'}</Text>

                        {Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_CUSTOMER_OFFER ?          // Customer Offer
                            <Text numberOfLines={1} style={Styles.normalText}>{Utility.strToTitleCase(Strings.OFFER) + ': ' + (item.offer_amount ? Utility.formatCurrency(item.offer_amount) : Strings.NA)}</Text>
                            :
                            Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_BOOKED ?               // Booked
                                <Text numberOfLines={1} style={Styles.normalText}>{Strings.BOOK_AMOUNT + ': ' + (item.booking_amount ? Utility.formatCurrency(item.booking_amount) : Strings.NA)}</Text>
                                :
                                Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_WALKED_IN ?        // Walk-in Done
                                    <Text numberOfLines={1} style={Styles.normalText}>{Strings.VISITED_ON + ' ' + Utility.getFormatedDateTime(item.lead_walkin_date, 'mmm dd')}</Text>
                                    :
                                    Constants.LEAD_STATUS_ID_VALUE[item.lead_status_id] == Constants.LEAD_STATUS_FOLLOW_UP ?    // follow up
                                        <Text numberOfLines={1} style={Styles.normalText}>{Strings.BUDGET + ': ' + (item.price_from ? Utility.formatCurrency(item.price_from) : Strings.NA)}</Text>
                                        :
                                        <Text numberOfLines={1} style={Styles.normalText}>{item.location_id ? item.locality : Strings.LOCATION + ': ' + Strings.NA}</Text>
                        }
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        overflow: 'hidden',
        flexDirection: 'row',
        borderRadius: 5,
        backgroundColor: Colors.WHITE,
        marginVertical: Dimens.margin_x_small
    },
    containerInner: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: Dimens.padding_normal
    },
    headText: {
        flex: 1,
        fontWeight: '600',
        color: Colors.BLACK,
        fontSize: Dimens.text_large,
        fontFamily: Strings.APP_FONT,
    },
    timeText: {
        fontSize: Dimens.text_small,
        color: Colors.BLACK_87,
        fontFamily: Strings.APP_FONT,
        marginTop: Dimens.margin_x_small
    },
    smallText: {
        fontSize: Dimens.text_x_small,
        color: Colors.BLACK_40,
        fontFamily: Strings.APP_FONT
    },
    normalText: {
        color: Colors.BLACK_54,
        fontSize: Dimens.text_normal,
        fontFamily: Strings.APP_FONT
    },
    statusText: {
        position: 'absolute',
        right: 6,
        textAlign: 'center',
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    containerTag: {
        borderRadius: 8,
        marginLeft: Dimens.margin_x_small,
        paddingHorizontal: Dimens.padding_small,
        paddingVertical: Dimens.padding_xx_small,
    },
    tagText: {
        color: Colors.WHITE,
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT
    },
    containerbadge: {
        width: '100%',
        position: 'absolute',
        top: 0, left: -35,
        backgroundColor: Colors.GREEN,
        transform: [{ rotate: '315deg' }]
    },
    badgeText: {
        color: Colors.WHITE,
        textAlign: 'center',
        fontSize: Dimens.text_x_small,
        fontFamily: Strings.APP_FONT,
        paddingVertical: Dimens.padding_xx_small,
    },
    dashedView: {
        height: '100%',
        borderWidth: 1,
        borderRadius: 2,
        borderStyle: 'dashed',
        borderColor: Colors.DIVIDER
    },
})

export default BuyerLeadTupleComponent;