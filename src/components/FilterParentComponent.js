import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native'
import {Colors, Strings} from "../values";

export default class FilterParentComponent extends Component {
    constructor() {
        super();
    }

    render() {
        let data = this.props.data;
        let isActive = this.props.activeIndex === this.props.index;
        return (
            <View>
                <TouchableOpacity activeOpacity={1} onPress={() => this.props.onFilterClick(this.props.index)} style={{
                    height: 70,
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: 18,
                    justifyContent: 'space-between'
                }}>
                    <View style={{flexDirection: 'row', alignItems: 'center', flex:1}}>
                        <Text style={{
                            fontSize: 16,
                            color: isActive ? Colors.WHITE : Colors.WHITE_54,
                            fontFamily: Strings.APP_FONT,
                            fontWeight: isActive ? '500' : '400',
                            flex:1
                        }}>{data.title}</Text>
                        {(!isActive && data.count && data.count !== 0) ? <View style={{
                            height: 18,
                            width: 18,
                            borderRadius: 9,
                            backgroundColor: Colors.WHITE_25,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginHorizontal: 10
                        }}>
                            <Text style={{color: Colors.WHITE_54}}>{data.count}</Text>
                        </View> : null}
                    </View>
                    {isActive && <View style={styles.triangle}/>}
                </TouchableOpacity>
                <View style={{height: 1, backgroundColor: Colors.WHITE_11}}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 8,
        borderRightWidth: 8,
        borderBottomWidth: 12,
        marginRight:-3,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        transform: [
            {rotate: '-90deg'}
        ]
    }
});