import React from 'react';
import { Platform } from 'react-native';
import { Utility, Constants } from '../util'
import * as Strings from "../values/Strings"
import * as APIConstant from "./APIConstants"
import * as DBConstants from '../database/DBConstants'
import axios from 'axios';
import AsyncStore from '../util/AsyncStore'
import DeviceInfo from 'react-native-device-info';
let FETCH_TIMEOUT = 60000;

export async function apiRequest(method,
    url,
    header,
    params,
    callback,
    callbackFailure,
    props,
    showDialog,
    loadingMsg,
    errorMsg,
    isDealerId) {
    Utility.getNetInfo().then(async isConnected => {
        if (!isConnected) {
            if (showDialog) {
                Utility.showNoInternetDialog();
                callbackFailure && callbackFailure();
            }
            return;
        }
        let body;
        let queryParams;
        let query = '';
        var headers = {
            'device-type': Platform.OS == 'ios' ? 'ios' : 'android',
            'device-Id': DeviceInfo.getUniqueId(),
            'device-brand' : DeviceInfo.getBrand(),
            'device-build' : DeviceInfo.getBuildNumber(),
            'device-version' : DeviceInfo.getSystemVersion(),
            'app-version' : DeviceInfo.getVersion(),
            'apiv': APIConstant.API_VERSION,
            // 'lang_id': 1,
            //'apikey': 'd77bc90f-6824-47da-94c2-2930de73daad', // for Finance
        }
        if(Platform.OS == 'android'){
            headers['device-model'] = DeviceInfo.getModel();
        }
        DeviceInfo.getDeviceName().then(deviceName => {
            headers['device-name'] = deviceName
        });
        DeviceInfo.getIpAddress().then(ip => {
            headers['ip-address'] = ip
        })
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.ACCESS_TOKEN, Constants.COUNTRY_CODE, Constants.LANGUAGE_CODE, Constants.DEALER_ID])
        // Utility.log('access_token ===>' + values[0][1])
        // Utility.log('countryCode===>' + values[1][1])
        // Utility.log('languageCode===>' + values[2][1])
        if (header) {
            if (values[1][1]) {
                headers['Accept-Country'] = values[1][1]
            }
            if (values[2][1]) {
                headers['Accept-Language'] = values[2][1]
            }
            if (values[0][1]) {
                headers['Authorization'] = values[0][1]
            }
        }
        // else {
        //     headers = header
        // }

        if (!isDealerId && params && values[3][1]) {
            params['dealer_id'] = values[3][1]
        }


        if (method === 'POST') {
            body = paramsToBody(params);
        } else if (method === 'GET') {
            queryParams = params;
            // query = paramsToUrlQueryParams(params);
        } else if (method === 'JSON_POST') {
            body = params;
            method = 'POST';
        }

        Utility.log("method : ", method);
        Utility.log("url : ", url + query);
        Utility.log("Params : ", JSON.stringify(params));
        Utility.log("BODY : ", JSON.stringify(body));
        Utility.log("headers : ", JSON.stringify(headers));

        axios({
            method: method,
            url: url,
            data: body,
            timeout: FETCH_TIMEOUT,
            params: queryParams,
            headers: headers
        }).then(function (response) {
            let resJson;
            Utility.log("response :" + JSON.stringify(response));
            if (response.status === 200) {
                try {
                    resJson = response.data;
                } catch (e) {
                    resJson = '';
                    callbackFailure && callbackFailure();
                }
            } else {
                resJson = undefined;
                callbackFailure && callbackFailure();
            }
            return resJson;

        }).then((responseJson) => {
            if (!responseJson) {
                return;
            }
            Utility.log("response :" + JSON.stringify(responseJson));
            if (responseJson.status === 200 && callback) { // Success
                callback(responseJson);
                return;
            } else if (responseJson.status === 422 && callbackFailure) { // Validation Failure
                callbackFailure(responseJson);
                return;
            } else if (responseJson.status == 400 && callbackFailure) { // 
                callbackFailure(responseJson);
                return;
            } else if (responseJson.status == 401 && callbackFailure) { // Auth Failed
                callbackFailure(responseJson);
                return;
            }
            else {
                callbackFailure(responseJson);
                return
            }
            // if (callback) {
            //     callback(responseJson);
            // }
        }).catch((error) => {
            Utility.log("API Err===>", error.response);
            if (callbackFailure) {
                callbackFailure(error.response);
            }

            if (props && error.response.data.status === 401) {       // Auth Failed
                Utility.showToast(error.response.data.message)
                Utility.logout(props)
                return
            }
            else if (error.response.status === 503) {
                Utility.showToast(error.response.data)
            }
        })

    });
}

export async function financeApiRequest(method,
    url,
    header,
    params,
    callback,
    callbackFailure,
    props,
    showDialog,
    loadingMsg,
    errorMsg,
    isDealerId) {
    Utility.getNetInfo().then(async isConnected => {
        if (!isConnected) {
            if (showDialog) {
                Utility.showNoInternetDialog();
                callbackFailure && callbackFailure();
            }
            return;
        }
        let body;
        let query = '';
        var headers = {
            'device-type': Platform.OS == 'ios' ? 'ios' : 'android',
            'device-Id': DeviceInfo.getUniqueId(),
            'device-brand' : DeviceInfo.getBrand(),
            'device-build' : DeviceInfo.getBuildNumber(),
            'device-version' : DeviceInfo.getSystemVersion(),
            'app-version' : DeviceInfo.getVersion(),
            'apiv': APIConstant.API_VERSION,
            'apikey': 'd77bc90f-6824-47da-94c2-2930de73daad', // for Finance
        }
        if(Platform.OS == 'android'){
            headers['device-model'] = DeviceInfo.getModel();
        }
        DeviceInfo.getDeviceName().then(deviceName => {
            headers['device-name'] = deviceName
        });
        DeviceInfo.getIpAddress().then(ip => {
            headers['ip-address'] = ip
        })
        var storeObject = new AsyncStore()
        const values = await storeObject.multiGetAsyncValueInPersistStore([Constants.ACCESS_TOKEN, Constants.COUNTRY_CODE, Constants.LANGUAGE_CODE, Constants.DEALER_ID])
        // Utility.log('access_token ===>' + values[0][1])
        // Utility.log('countryCode===>' + values[1][1])
        // Utility.log('languageCode===>' + values[2][1])
        if (header) {
            if (values[1][1]) {
                headers['Accept-Country'] = values[1][1]
            }
            if (values[2][1]) {
                headers['Accept-Language'] = values[2][1]
            }
            if (values[0][1]) {
                headers['Authorization'] = values[0][1]
            }
        }
        // else {
        //     headers = header
        // }

        if (!isDealerId && params && values[3][1]) {
            params['dealer_id'] = values[3][1]
        }

        if (method === 'POST') {
            body = paramsToBody(params);
        } else if (method === 'GET') {
            query = paramsToUrlQueryParams(params);
        } else if (method === 'JSON_POST') {
            body = params;
            method = 'POST';
        }

        Utility.log("method : ", method);
        Utility.log("url : ", url + query);
        Utility.log("Params : ", JSON.stringify(params));
        Utility.log("BODY : ", JSON.stringify(body));
        Utility.log("headers : ", JSON.stringify(headers));

        axios({
            method: method,
            url: url + query,
            data: body,
            timeout: FETCH_TIMEOUT,
            headers: headers
        }).then(function (response) {
            let resJson;
            Utility.log("response :" + JSON.stringify(response));
            if (response.status === 200) {
                try {
                    resJson = response.data;
                } catch (e) {
                    resJson = '';
                    callbackFailure && callbackFailure();
                }
            } else {
                resJson = undefined;
                callbackFailure && callbackFailure();
            }
            return resJson;

        }).then((responseJson) => {
            if (!responseJson) {
                return;
            }
            Utility.log("response :" + JSON.stringify(responseJson));
            if (responseJson.status === 200 && callback) { // Success
                callback(responseJson);
                return;
            } else if (responseJson.status === 422 && callbackFailure) { // Validation Failure
                callbackFailure(responseJson);
                return;
            } else if (responseJson.status == 400 && callbackFailure) { // 
                callbackFailure(responseJson);
                return;
            } else if (responseJson.status == 401 && callbackFailure) { // Auth Failed
                callbackFailure(responseJson);
                return;
            }
            else {
                callbackFailure(responseJson);
                return
            }
            // if (callback) {
            //     callback(responseJson);
            // }
        }).catch((error) => {
            Utility.log("API Err:", error);
            if (callbackFailure) {
                callbackFailure();
            }

            if (props && error.response.data.status === 401) {       // Auth Failed
                Utility.showToast(error.response.data.message)
                Utility.logout(props)
                return
            }
            else if (error.response.status === 503) {
                Utility.showToast(error.response.data)
            }
        })

    });
}


function paramsToBody(params) {
    if (!params || params.length < 1) {
        console.warn("response : empty params");
        return null;
    }

    const body = new FormData();
    for (let k in params) {
        body.append(k, params[k]);
    }
    return body;
}

function paramsToUrlQueryParams(params) {
    const esc = encodeURIComponent;
    let query = "";
    if (params) {
        query = '?';
        query += Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&');
    }
    return query;
}


//method, url, header, params, callback, callbackFailure, showDialog, loadingMsg, errorMsg

/* Check Force Update API Call */
export function getForceUpdate(callbackSuccess, callbackFailure, props) {

    let params = {
        app_version: Constants.VERSION_CODE
    }
    apiRequest(
        'JSON_POST', APIConstant.CHECK_FORCE_UPDATE,
        null, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

/**
 *
 * @param email
 * @param password
 * @param callbackSuccess
 * @param callbackFailure
 */

/* Login with email and password API Call */
export function login(params, callbackSuccess, callbackFailure, props) {

    // let params = {
    //     email: email,
    //     password: password
    // };
    let header = {}
    apiRequest(
        'JSON_POST', APIConstant.LOGIN,
        null, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

export function signUp(name, email, phone, callbackSuccess, callbackFailure, props) {

    let params = {
        name: name,
        mobile_no: phone,
        email: email,

    };
    apiRequest(
        'JSON_POST', APIConstant.SIGNUP,
        null, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

/**
 *
 * @param mobileNum
 * @param callbackSuccess
 * @param callbackFailure
 */

/* Send OTP API Call */
export function sendOTP(mobileNum, callbackSuccess, callbackFailure, props) {

    let params = {
        mobile: mobileNum
    };
    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.SEND_OTP,
        null, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Verify OTP API Call */
export function loginWithOTP(mobileNum, otp, callbackSuccess, callbackFailure, props) {

    let params = {
        mobile: mobileNum,
        otp: otp,
        _with: ["contact"]
    };
    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.OTP_LOGIN,
        null, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function validateOTP(mobileNum, otp, callbackSuccess, callbackFailure, props) {

    let params = {
        mobile: mobileNum,
        otp: otp,
        kind: 'reset-password'
    }
    apiRequest(
        'JSON_POST', APIConstant.VALIDATE_OTP,
        null, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function setPassword(mobileNum, newPassword, token, callbackSuccess, callbackFailure, props) {

    let params = {
        mobile: mobileNum,
        new_password: newPassword,
        token: token
    }
    apiRequest(
        'JSON_POST', APIConstant.SET_PASSWORD,
        null, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Get dealer locality and sync with dataBase
export function getDealerLocality(params, callbackSuccess, callbackFailure, props) {
    apiRequest(
        'JSON_POST', APIConstant.DEALER_LOCALITY,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Get all config data
export function getDealerConfig(callbackSuccess, callbackFailure, props) {
    let params = {}
    apiRequest(
        'JSON_POST', APIConstant.GET_DEALER_CONFIG,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Get all config data
export function getUserConfig(callbackSuccess, callbackFailure, props) {
    let params = {}
    apiRequest(
        'GET', APIConstant.GET_CONFIG,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Get all Budget data
export function getBudgetData(callbackSuccess, callbackFailure, props) {
    let params = {}
    apiRequest(
        'GET', APIConstant.GET_BUDGET,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Stock Listing API Call */
export function getStockListing(dealer_id, text, stockType, page, activeStockType, favCarIds, callbackSuccess, callbackFailure, props) {
    let car_status = [stockType]
    let params = {
        dealer_id: dealer_id,
        search_text: text,
        car_status: JSON.stringify(car_status),
        page_no: page,
        not_included_car_id: favCarIds.length > 0 ? favCarIds : []
    }
    if (activeStockType != '') {
        if (activeStockType == Constants.NON_CLASSIFIED_STOCK) {
            params["non_classified"] = '0'
        }
        else if (activeStockType == Constants.WITHOUT_PHOTOS_STOCK) {
            params["without_photos"] = "0"
        }
        else if (activeStockType == Constants.AGE_45D_STOCK) {
            params["car_age_before"] = Constants.CAR_AGE_BEFORE
        }
    }

    // if (favCarIds.length > 0) {
    //     params["not_included_car_id"] = favCarIds
    // }

    apiRequest(
        'JSON_POST', APIConstant.GET_STOCK_LISTING,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

/* Get Lead Count API Call */
export function getLeadCount(dealerId, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_LEAD_COUNT,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Add Lead API Call */
export function addLead(dealerId, fullName, email, mobileNumber, altMobile, locality, budgetDp, budgetOTR, source, followupDate, status, comment, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        cust_name: fullName,
        cust_email: email,
        cust_mobile: mobileNumber,
        cust_alt_mobile: altMobile,
        cust_location: locality,
        lead_source: source,
        dp_budget: budgetDp,
        budget: budgetOTR,
        lead_status: status,
        followup_date: followupDate,
        lead_comment: comment,
        // car_id: '[101, 102, 103]'
    };
    apiRequest(
        'JSON_POST', APIConstant.ADD_LEAD,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Get Lead Catogery List API Call */
export function getLeadListing(dealerId, leadKey, leadDate, dashTabKey, text, page, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        tab_clicked: leadKey,
        date: leadDate,
        dashboard_tab: dashTabKey,
        keyword: text,
        page_no: page
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_LEAD_LISTING,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Get Lead Finder List API Call */
export function getLeadFinderList(params, callbackSuccess, callbackFailure, props) {

    apiRequest(
        'JSON_POST', APIConstant.GET_LEAD_LISTING,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Get Lead Detail API Call */
export function getLeadDetail(dealerId, leadId, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        lead_id: leadId
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_LEAD_DETAIL,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Update Lead Customer Details API Call */
export function updateLeadCustomer(dealerId, leadId, name, email, altMobile, location, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        lead_id: leadId,
        name: name,
        email: email,
        alt_mobile_number: altMobile,
        location: location
    }
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_LEAD_CUSTOMER,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* get Lead Customer Requirement API Call */
export function getLeadRequirement(dealerId, leadId, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        lead_id: leadId
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_LEAD_REQUIREMENT,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Update Lead Customer Requirement API Call */
export function updateLeadRequirement(dealerId, leadId, budgetDP, budgetOTR, fuelType, transmission, makeModel, bodyType, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        lead_id: leadId,
        dp_budget: budgetDP,
        budget: budgetOTR,
        fuel_type: fuelType,
        transmission: transmission,
        body_type: bodyType,
        model: makeModel
    }
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_LEAD_REQUIREMENT,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

/* Update Lead Status API Call */
export function updateLeadStatus(params, callbackSuccess, callbackFailure, props) {

    /*let params = {
        dealer_id: dealerId,
        lead_id: leadId,
        lead_status: leadStatus,
        lead_rating: leadRating,
        followup_date: followupDate,
        reminder_date: reminderDate,
        lead_comment: leadComment
    }*/
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_LEAD_STATUS,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    )
}

/* Update Favourite API Call */
export function updateFavourite(leadId, isFavourite, favouriteItems, callbackSuccess, callbackFailure, props) {

    let params = {
        lead_id: leadId,
        is_favourite: isFavourite,
        car_id: favouriteItems
    }
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_FAVOURITE,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

/* Update Lead TimeLine By Share Lead API Call */
export function sharelead(leadId, shareBy, shareItem, callbackSuccess, callbackFailure, props) {

    let params = {
        lead_id: leadId,
        share_by: shareBy,
        share_item: shareItem
    }
    apiRequest(
        'JSON_POST', APIConstant.SHARE_LEAD,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

/* Lead share by Email API Call */
export function shareLeadEmail(leadId, carId, shareType, emailId, callbackSuccess, callbackFailure, props) {

    let params = {
        lead_id: leadId,
        car_id: [carId],
        share_type: shareType,
        email: [emailId]
    }
    apiRequest(
        'JSON_POST', APIConstant.SHARE_LEAD_EMAIL,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

/* Lead share by Email API Call */
export function shareCardetailEmail(carId, emailId, callbackSuccess, callbackFailure, props) {

    let params = {
        stock_used_car_id: carId,
        email_id: emailId
    }
    apiRequest(
        'JSON_POST', APIConstant.SHARE_CARDETAIL_EMAIL,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

/* Stock Detail API Call */
export function getStockDetail(dealer_id, stock_used_car_id, callbackSuccess, callbackFailure, props) {

    let params = {
        //dealer_id: dealer_id,
        stock_used_car_id: stock_used_car_id.toString()
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_STOCK_DETAIL,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}


export function getMMV(callbackSuccess, callbackFailure, props) {

    let params = {};
    let header = {};
    apiRequest(
        'GET', APIConstant.GET_MMV,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function makeStockPremium(dealer_id, stock_used_car_id, classified, featuted, callbackSuccess, callbackFailure, props) {
    let params
    if (classified != '') {
        params = {
            dealer_id: dealer_id,
            stock_used_car_id: stock_used_car_id,
            classified: classified,
            //featuted : featuted,
            //type : type,

        };
    } else {
        params = {
            dealer_id: dealer_id,
            stock_used_car_id: stock_used_car_id,
            //classified : classified,
            featuted: featuted,
            //type : type,

        };
    }
    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.MAKE_STOCK_PREMIUM,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function removeStock(dealer_id, stock_used_car_id, stock_removal_reason_id, sold_price, callbackSuccess, callbackFailure, props) {
    let params = {
        dealer_id: dealer_id,
        stock_used_car_id: stock_used_car_id,
        stock_removal_reason_id: stock_removal_reason_id,
        sold_price: sold_price

    };

    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.REMOVE_STOCK,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function getDashboardData(dealerId, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId
    }
    apiRequest(
        'GET', APIConstant.GET_DASHBOARD,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function getAllCountry(callbackSuccess, callbackFailure) {
    let params = {};
    let header = {};
    apiRequest(
        'GET', APIConstant.ALL_COUNTRY,
        true, params, callbackSuccess, callbackFailure, true, null, null
    );
}

export function getAllCity(callbackSuccess, callbackFailure) {
    let params = {};

    let header = {};
    apiRequest(
        'GET', APIConstant.ALL_CITY,
        true, params, callbackSuccess, callbackFailure, true, null, null
    );
}

export function addStock(stock_used_car_id, dealer_id, version, mfgMonth, mfgYear, cngFitted, color, kmsDriven, owner
    , regMonth, regYear, city, regNo, stockPrice, insuType, insuMonth, insuYear, tax, downPayment, emiAmount, noOfEmis,
    numberPlateExpireFormattedDate, otherColor, dealer_city, description, plateMonth, plateYear, taxMonth, taxYear,isClassifiedCheckboxChecked,oddEvenRadioText,callbackSuccess, callbackFailure, props) {
    let url = APIConstant.ADD_STOCK
    let params = {
        dealer_id: dealer_id,
        version_id: parseInt(version),
        make_month: parseInt(mfgMonth),
        make_year: parseInt(mfgYear),
        is_cng_fitted: cngFitted,
        uc_colour_id: color,
        km_driven: parseInt(kmsDriven),
        owner_type: owner != '-1' ? owner : '',
        reg_month: parseInt(regMonth),
        reg_year: parseInt(regYear),
        reg_place_city_id: city,
        reg_no: regNo,
        car_price: parseInt(stockPrice),
        insurance_type: insuType,
        //tax_type: tax,
        valid_month: parseInt(insuMonth),
        valid_year: parseInt(insuYear),
        downpayment: downPayment,
        emi: emiAmount,
        emi_count: noOfEmis,
        //reg_valid_date: numberPlateExpireFormattedDate,
        other_colour: otherColor,
        is_registered_car: owner != '-1' ? 1 : 0,
        dealer_city_id: dealer_city,
        is_reg_no_show: '0',
        description: description,
        reg_valid_month: parseInt(plateMonth),
        reg_valid_year: parseInt(plateYear),
        tax_expiry_month: parseInt(taxMonth),
        tax_expiry_year: parseInt(taxYear),
    };

    // if (insuType != Constants.NO_INSURANCE) {
    //     params['insurance_exp_year'] = parseInt(insuMonth)
    //     params['insurance_exp_month'] = parseInt(insuYear)
    // }
    if(Constants.APP_TYPE == Constants.INDONESIA){
        if(!stock_used_car_id)
            params["classifiedStock"] = isClassifiedCheckboxChecked ? 1 : 0
        if(oddEvenRadioText != "")
            params["number_type"] = oddEvenRadioText
    }
    if (stock_used_car_id) {
        params["stock_used_car_id"] = stock_used_car_id
        url = APIConstant.EDIT_STOCK

    }
    apiRequest(
        'JSON_POST', url,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function getColorList(vn_id, callbackSuccess, callbackFailure) {
    let params = {
        version_id: vn_id
    };

    let header = {};
    apiRequest(
        'GET', APIConstant.GET_COLOR_LIST,
        header, params, callbackSuccess, callbackFailure, true, null, null
    );
}

export function sendImageToServer(stock_used_car_id, imageNames, imageUrls, callbackSuccess, callbackFailure, props) {
    let params = {
        'usedcar_id': stock_used_car_id,
        'image_file_names': imageNames,
        'image_file_urls': imageUrls,

    }

    apiRequest(
        'JSON_POST', APIConstant.SEND_IMAGE_TO_SERVER,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function deleteImageFromServer(stock_used_car_id, callbackSuccess, callbackFailure, props) {
    let params = {
        usedcar_id: stock_used_car_id,
    }
    apiRequest(
        'JSON_POST', APIConstant.DELETE_IMAGE_FROM_SERVER,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

/* Get Lead List By Car Id API Call */
export function getLeadListByCarId(dealer_id, car_id, callbackSuccess, callbackFailure, props) {
    let car_ids = [car_id]
    let params = {
        dealer_id: dealer_id,
        car_id: car_ids,
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_LEADS_BY_CAR_ID,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function addRemovedStockToActive(stock_used_car_id, status, callbackSuccess, callbackFailure, props,isClassifiedCheckboxChecked) {
    let params = {
        stock_used_car_id: stock_used_car_id,
        car_status: status.toString(),
    };

    if(Constants.APP_TYPE == Constants.INDONESIA){
        params['is_classified'] = isClassifiedCheckboxChecked ? "1" : "0"
    }


    apiRequest(
        'JSON_POST', APIConstant.EDIT_STOCK,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Update App Language
export function SaveInfo(languageId, langCode, callbackSuccess, callbackFailure, props) {
    let params = {
        language_id: languageId,
        lang_code: langCode
    }
    apiRequest(
        'JSON_POST', APIConstant.SAVE_INFO,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

// Update delear password
export function changePassword(oldPassword, newPassword, callbackSuccess, callbackFailure, props) {
    let params = {
        old_password: oldPassword,
        new_password: newPassword
    }
    apiRequest(
        'JSON_POST', APIConstant.CHANGE_PASSWORD,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    );
}

export function updateCarPrice(stock_used_car_id, price, callbackSuccess, callbackFailure, props) {
    let params = {
        stock_used_car_id: stock_used_car_id,
        car_price: price,

    }
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_CAR_PRICE,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function updateFCMToken(user_id, fcmToken, deviceId, callbackSuccess, callbackFailure, props) {
    let params = {
        user_id: user_id,
        fcm_token: fcmToken,
        device_id: deviceId
    };

    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.UPDATE_FCM_TOKEN,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function deleteFCMToken(user_id, dealerId, deviceId, callbackSuccess, callbackFailure, props) {
    let params = {
        user_id: user_id,
        dealer_id: dealerId,
        device_id: deviceId
    };

    let header = {};
    apiRequest(
        'JSON_POST', APIConstant.DELETE_FCM_TOKEN,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}


//////////////////////////////////// Finance /////////////////////////////////////////////////////////////////////


/* Get Loan Dashboard API Call */
export function getLoanDashboard(dealerId, userId, callbackSuccess, callbackFailure, props) {
    let params = {
        user_id: userId,
        dealer_id: dealerId
    }

    financeApiRequest(
        'JSON_POST', APIConstant.GET_LOAN_DASHBOARD,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )

}

/* Get Loan Lead Count API Call */
export function getLoanLeadCount(userId, dealerIds, callbackSuccess, callbackFailure, props) {

    let params = {
        user_id: userId,
        dealer_id: dealerIds,
        source: APIConstant.LOAN_SOURCE
    }

    let header = {}

    financeApiRequest(
        'JSON_POST', APIConstant.LOAN_LEAD_COUNT,
        true, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )

}

/* Get Loan Lead List API Call */
export function getLoanLeadListData(userId, dealerIds, loanStatus, pageNumber, subStatus, text, callbackSuccess, callbackFailure, props) {

    let params = {
        user_id: userId,
        dealer_id: dealerIds,
        page_num: pageNumber,
        source: APIConstant.LOAN_SOURCE
    }

    if (loanStatus != '') {
        params['loan_status'] = loanStatus
    }
    if (subStatus != '') {
        params['sub_status'] = subStatus
    }
    if (text != '') {
        params['search_text'] = text
    }

    let header = {}
    financeApiRequest(
        'JSON_POST', APIConstant.GET_LOAN_LEAD,
        true, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )

}

export function getLoanConfig(sfaData, callbackSuccess, callbackFailure, props) {
    let config_data = ['agent_list', 'status_list', "financier_list", 'lead_source', 'region_list', 'document_list',
        'language_list', 'loan_tenure', 'declaration', 'insurance_type', 'loan_type', //'vehicle_category', 'vehicle_type',
        'jakarta_plate_no', 'price_upping']

    let params = {
        'config_data': config_data
    }
    if (sfaData) {
        params['source'] = APIConstant.LOAN_SOURCE_SFA
    }
    else {
        params['source'] = APIConstant.LOAN_SOURCE
    }
    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.GET_LOAN_CONFIG,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

export function getCarPrice(car_id, version_id, make_year, financier_id, callbackSuccess, callbackFailure, props) {

    let params = {
        'car_id': car_id,
        'version_id': version_id,
        'make_year': make_year,
        'financier_id': financier_id
    };
    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.GET_CAR_PRICE,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    )

}

export function addLoanLead(user_id, sfaData, stockItem, financier, ruleEngineData, approvedQuoteData, customerData, callbackSuccess, callbackFailure, props) {
    // let filename = '', path = '';
    // if (approvedQuoteData.imageToUpload) {
    //     filename = approvedQuoteData.imageToUpload.replace(/^.*[\\\/]/, '')
    //     path = approvedQuoteData.imageToUpload
    // }
    // let document = {
    //     "doc_id": 7,
    //     "doc_path": filename,
    //     "doc_aws_path": path,
    // }
    let make = stockItem.make ? stockItem.make + " " : ''
    let params = {

        name: customerData.name,
        phone: customerData.phone,
        alt_phone: customerData.altPhone,
        application_type: customerData.applicationType,// need to discusss
        lead_source_id: customerData.leadSource,
        agent_id: customerData.agentName,
        financier_id: financier.id,
        branch_id: customerData.financierBranch,
        notes: customerData.notes,
        //declaration: customerData.declaration,// need to discusss
        car_id: stockItem.id,
        make_id: stockItem.make_id ? stockItem.make_id : '',  // need to discusss
        model_id: stockItem.model_id ? stockItem.model_id : '',
        version_id: stockItem.version_id ? stockItem.version_id : '',
        mmv_name: stockItem.modelVersion ? make + stockItem.modelVersion : '',
        reg_number: stockItem.reg_no ? stockItem.reg_no : '',
        region_id: ruleEngineData && ruleEngineData.region ? ruleEngineData.region : 0,
        reg_date: (stockItem.reg_year ? stockItem.reg_year.toString() : '') + "-" + (stockItem.reg_month ? stockItem.reg_month.toString() : ''),
        make_year: stockItem.make_year,
        //user_id: user_id,
        //user_type: 'dealer',
        created_on_gcloud: 'Y',
        //fuel_type": "Diesel",
        transmission: stockItem.transmission,
        //car_body_type: stockItem.car_body_type,
        loan_type: customerData.loanType

    };
    if (sfaData) {
        params['user_id'] = parseInt(sfaData.userId)
        params['user_type'] = APIConstant.LOAN_USER_TYPE_SFA
    }
    else {
        params['user_id'] = user_id
        params['user_type'] = APIConstant.LOAN_USER_YUPE_DEALER
    }

    if (stockItem.car_body_type) {
        params['car_body_type'] = stockItem.car_body_type
    }
    if (approvedQuoteData) {
        if (approvedQuoteData.offer_id) {
            params['offer_id'] = approvedQuoteData.offer_id
        }
        params['car_price'] = approvedQuoteData.carPrice
        params['first_tdp'] = approvedQuoteData.tdpAmount
        params['emi'] = approvedQuoteData.emiAmount
        params['tenure'] = approvedQuoteData.selectedTenureValue
        params['approved_amount'] = approvedQuoteData.amountCredited
        params['amount_needed'] = approvedQuoteData.dealerDisbursement
        params['real_tdp'] = approvedQuoteData.realTDP
        params['has_rule_engine'] = 0
        params['is_mrp'] = 0
    }
    else if (ruleEngineData) {
        params['rule_engine'] = ruleEngineData
        params['car_price'] = ruleEngineData.mrp_with_upping
        params['first_tdp'] = ruleEngineData.total_dp
        params['emi'] = ruleEngineData.installment
        params['tenure'] = ruleEngineData.tenure
        params['approved_amount'] = ruleEngineData.total_payment_to_dealer
        params['amount_needed'] = ruleEngineData.updatedLoanNeeded
        params['real_tdp'] = 0
        params['has_rule_engine'] = 1
        params['is_deviation'] = ruleEngineData.is_deviation
        params['deviation_percentage'] = ruleEngineData.deviation_per
        params['is_mrp'] = ruleEngineData.mrp == 0 ? 0 : 1
    }

    // if (financier.has_rule_engine != Constants.HAS_RULE_ENGINE) {
    //     params['document'] = document
    // }

    Utility.log("params::::" + JSON.stringify(params))
    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.ADD_LOAN_LEAD,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    )

}

export function saveLoanDoc(dealer_id_hash, user_id, sfaData, leadId, documentToUpload, is_calculator_image, callbackSuccess, callbackFailure, props) {
    let params = {
        lead_docs: documentToUpload,
        finance_lead_id: leadId,
        dealer_id_hash: dealer_id_hash
    }
    if (is_calculator_image) {
        params["is_calculator_image"] = "1"
    }
    if (sfaData) {
        params['user_id'] = parseInt(sfaData.userId)
        params['user_type'] = APIConstant.LOAN_USER_TYPE_SFA
        params['source'] = APIConstant.LOAN_SOURCE_SFA
    }
    else {
        params['user_id'] = user_id
        params['user_type'] = APIConstant.LOAN_USER_YUPE_DEALER
        params['source'] = APIConstant.LOAN_SOURCE
    }

    let header = {}
    financeApiRequest(
        'JSON_POST', APIConstant.LOAN_DOC_SAVE,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    )

}

export function getOfferDetail(version_id, dealer_id, car_price, tenure, region_id, disbursement_amount, object_type, car_group, financier_id, insurance, make_year, callbackSuccess, callbackFailure, props) {

    let params = {
        version_id: version_id,
        dealer_id: dealer_id,
        car_price: car_price,
        tenure: tenure,
        region_id: region_id,
        disbursement_amount: disbursement_amount,
        object_type: object_type.toString(),
        car_group: car_group.toString(),
        financier_id: financier_id,
        insurance: insurance,
        make_year: make_year,
    };
    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.GET_OFFER_DETAIL,
        header, params, callbackSuccess, callbackFailure, props, true, null, null
    )

}

export function getMasterData(callbackSuccess, callbackFailure, props) {
    let config_data = [
        //'dealer_sku',
        'dealer_sku_info_url',
        'vehicle_number_type',
        // 'dealer_role',
        // 'kyc_master',
        // 'data-for-sfa',
        // 'dealer_premium_type',
        'lang',
        'is_finance',
        'lead_dp_budget',
        'lead_otr_budget',
        'lead_status_list',
        'lead_source_list',
        'inventory_fuel_type',
        'inventory_body_type',
        'inventory_transmission_type',
        'inventory_owner_type',
        'insurance_type',
        'plate_number_validation',
        // 'number_plate_validity',
        // 'tax_expiry_validity',
        'emi_tenure_list',
        'country_code',
        'total_mfg_year',
    ]

    let params = {
        'master': config_data
    }

    let header = {}
    apiRequest(
        'GET', APIConstant.GET_MASTER_DATA,
        header, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

export function addToBumpup(car_id, callbackSuccess, callbackFailure, props) {

    let params = {
        'car_id': car_id
    }
    apiRequest(
        'JSON_POST', APIConstant.ADD_TO_BUMPUP,
        true, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

export function getFinancierMMV(page, callbackSuccess, callbackFailure, props) {
    let params = {
        page: page,
        rpp: 500
    };
    let header = {};
    financeApiRequest(
        'GET', APIConstant.GET_RULE_ENGINE_MMV,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    );
}

export function calculateLoan(dealerId, userId, sfaData, ruleEngineData, financierItem, reqDP, loanNeeded, callbackSuccess, callbackFailure, props) {

    let params = {
        "dealer_id": dealerId,
        "make_id": ruleEngineData.makeId,
        "model_id": ruleEngineData.modelId,
        "version_id": ruleEngineData.variantId,
        "make_year": ruleEngineData.manufacturingYear,
        "financier_id": financierItem.id,
        "applicant_type_id": ruleEngineData.customerTypeId,
        "customer_area_id": ruleEngineData.branchRegionId,
        "plate_area_id": ruleEngineData.plateNumberRegionId,
        // "jakarta_plate": ruleEngineData.jakartaPlateNumberId == 1 ? 'Y' : 'N',
        "vehicle_type_id": ruleEngineData.vehicleTypeId,
        "insurance_type_id": ruleEngineData.insuranceTypeId,
        "mrp": ruleEngineData.MRP,
        "price_upping_id": ruleEngineData.priceUppingId,
        "mrp_with_upping": ruleEngineData.MRPWithUpping,
        "olx_mrp": ruleEngineData.olxMRP,
        "tenure": (parseInt(ruleEngineData.tenure) * 12),
        "loan_needed": loanNeeded,
        "actual_dp_per": reqDP
    }

    if (sfaData) {
        params['user_id'] = parseInt(sfaData.userId)
        // params['user_type'] = 'sfa'
        // params['source'] = APIConstant.LOAN_SOURCE_SFA
    }
    else {
        params['user_id'] = userId
        // params['user_type'] = 'dealer'
        // params['source'] = APIConstant.LOAN_SOURCE
    }

    if (ruleEngineData.jakartaPlateNumberId != 0) {
        params['jakarta_plate'] = ruleEngineData.jakartaPlateNumberId == 1 ? 'Y' : 'N'
    }

    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.CALCULATE_LOAN,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

export function getLoanLeadTimeLine(leadId, customerId, dealerIdHash, callbackSuccess, callbackFailure, props) {

    let params = {
        'lead_id': leadId,
        'customer_id': customerId,
        'source': APIConstant.LOAN_SOURCE_GCLOUD
    }
    if (dealerIdHash) {
        params['dealer_id_hash'] = dealerIdHash
    }
    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.GET_LOAN_TIMELINE,
        true, params, callbackSuccess, callbackFailure, props, true, null, null, true
    )
}

export function getNotificationData(dealerId, page_no, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id: dealerId,
        page_number: page_no
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_NOTIFICATION_DATA,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    )
}

export function updateNotificationStatus(id, callbackSuccess, callbackFailure, props, fromDetail) {
    let params
    if (fromDetail) {
        params = {
            fcm_unique_key: id
        }
    }
    else {
        params = {
            id: id
        }
    }

    apiRequest(
        'JSON_POST', APIConstant.UPDATE_NOTIFICATION_STATUS,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    )
}

export function loanShareMRPData(dealerId, userId, sfaData, userName, userMobile, dealerName, mobileNo, dealerIdHash, paymentDetail, callbackSuccess, callbackFailure, props) {

    let params = {
        "dealer_id": dealerId,
        "dealer_name": dealerName,
        "mobile": mobileNo,
        "loan_detail": paymentDetail
    }

    if (sfaData) {
        params['user_id'] = parseInt(sfaData.userId)
        params['user_name'] = sfaData.name
        params['user_mobile'] = sfaData.mobile
        params['source'] = APIConstant.LOAN_SOURCE_SFA
    }
    else {
        params['user_id'] = userId
        params['user_name'] = userName
        params['user_mobile'] = userMobile
        params['source'] = APIConstant.LOAN_SOURCE
    }
    if (dealerIdHash) {
        params['dealer_id_hash'] = dealerIdHash
    }

    let header = {};
    financeApiRequest(
        'JSON_POST', APIConstant.LOAN_SHARE_MRP_DATA,
        true, params, callbackSuccess, callbackFailure, props, true, null, null
    )
}

export function getPackageDetail(dealerId, dealer_id_hash, callbackSuccess, callbackFailure, props) {

    let params = {
        dealer_id_hash: dealer_id_hash,
    }
    apiRequest(
        'JSON_POST', APIConstant.GET_PACKAGE_DETAILS,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null
    )
}

export function purchasePackage(sku_id, callbackSuccess, callbackFailure, props) {

    let params = {
        sku_id: sku_id,
    }
    apiRequest(
        'JSON_POST', APIConstant.PURCHASE_PACKAGE,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null,true
    )
}

export function renewPackage(sku_id,b_details_id,b_id, callbackSuccess, callbackFailure, props) {

    let params = {
        sku_id: sku_id,
        b_details_id : b_details_id,
        b_id : b_id
    }
    apiRequest(
        'JSON_POST', APIConstant.RENEW_PACKAGE,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null,true
    )
}

export function checkAutoClassified(callbackSuccess, callbackFailure, props) {

    let params = {}
    apiRequest(
        'GET', APIConstant.AUTO_CLASSIFIED_CHECK,
        true, params, callbackSuccess, callbackFailure, props, true,
        null, null,true
    )
}
