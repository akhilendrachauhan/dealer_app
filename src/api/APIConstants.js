import { Platform } from 'react-native'
import Config from 'react-native-config'

export const CONSOLE_ENABLED = Config.CONSOLE_ENABLED == 'true'
export const BASE_URL = Config.BASE_URL
export const API_VERSION = Config.API_VERSION
//export const CONSOLE_ENABLED = true
// export const BASE_URL = 'https://api-id.oto.com/'
//export const BASE_URL = 'http://int-gcloud-stage.gaadi.com/'

export const USER_DIRECTORY = 'account/user/'
export const GCLOUD_DIRECTORY = 'gcloud/gc/'
export const CONFIG_DIRECTORY = 'gcloud/config/'
export const COMMON_DIRECTORY = 'core/commonservice/'
export const LEAD_DIRECTORY = 'lead/lead/'
export const INVENTORY_DIRECTORY = 'inventory/inventory/'
export const INVENTORY_COMMON_SERVICES_DIRECTORY = 'inventory/commonservice/'
export const DEALER_DIRECTORY = 'dealer/dealer/'
export const DEALER_MANAGER_DIRECTORY = 'dealer/user-manager/'
export const LOAN_DIRECTORY = 'loan/'
export const LOAN_LEAD_DIRECTORY = 'loan/lead/'
export const NOTIFICATION_DIRECTORY = 'notification/'
export const LOAN_SOURCE = "GCLOUD"
export const LOAN_SOURCE_GCLOUD = "gcloud"
export const LOAN_SOURCE_SFA = "SFA"
export const RULE_ENGINE_DIRECTORY = 'loan/rule_engine/'
export const LOAN_USER_TYPE_SFA = "sfa"
export const LOAN_USER_YUPE_DEALER = "dealer"
export const DEALER_SUBSCRIPTION = "dealer/subscription/"



//methods
export const METHOD_CHECK_FORCE_UPDATE = 'gc_version'
export const METHOD_LOGIN = 'login'
export const METHOD_SEND_OTP = 'send_otp'
export const METHOD_OTP_LOGIN = 'login-otp'
export const METHOD_VALIDATE_OTP = 'validate_otp'
export const METHOD_SET_PASSWORD = 'set-password'
export const METHOD_DEALER_LOCALITY = 'locality_list'
export const METHOD_DEALER_CONFIG = 'get_config'
export const METHOD_GET_CONFIG = 'getfilter'
export const METHOD_GET_BUDGET = 'getAllBudget'
export const METHOD_STOCK_LISTING = 'stocklist'
export const METHOD_LEAD_COUNT = 'leadTabCount'
export const METHOD_ADD_LEAD = 'createLead'
export const METHOD_LEAD_LISTING = 'listLead'
export const METHOD_LEAD_DETAIL = 'getLeadDetails'
export const METHOD_UPDATE_LEAD_CUSTOMER = 'updateLeadCustomer'
export const METHOD_GET_LEAD_REQUIREMENT = 'getPreference'
export const METHOD_UPDATE_LEAD_REQUIREMENT = 'updatePreference'
export const METHOD_UPDATE_LEAD_STATUS = 'updateLead'
export const METHOD_SHARE_LEAD = 'sharelead'
export const METHOD_SHARE_LEAD_EMAIL = 'shareLeadEmail'
export const METHOD_SHARE_CARDETAIL_EMAIL = 'share_cardetail_via_email'
export const METHOD_UPDATE_FAVOURITE = 'updateFavourite'
export const METHOD_STOCK_DETAIL = 'usedstockcardetails'
export const METHOD_MMV = 'core/commonservice/mmv_all'
export const METHOD_MAKE_STOCK_PREMIUM = 'makeCarPremium'
export const METHOD_REMOVE_STOCK = 'removeusedstockcar'
export const METHOD_DASHBOARD = 'dashboard'
export const METHOD_ALL_COUNTRY = 'core/commonservice/country_list'
export const METHOD_ALL_CITY = 'core/commonservice/state_city_all'
export const METHOD_ADD_STOCK = 'saveusedstockcar'
export const METHOD_GET_COLOR = 'core/commonservice/color_list'
export const METHOD_UPLOAD_TO_S3 = 'core/commonservice/docs_upload'
export const METHOD_SEND_IMAGE_TO_SERVER = 'uploadstockusedcarimage'
export const METHOD_EDIT_STOCK = 'updateusedstockcar'
export const METHOD_GET_LEADS_BY_CAR_ID = 'getLeadByCarId'
export const METHOD_SAVE_INFO = 'save_other_info'
export const METHOD_CHANGE_PASSWORD = 'change-password'
export const METHOD_UPDATE_CAR_PRICE = "updatecarprice"
export const METHOD_LOAN_CONFIG = 'config/get'
export const METHOD_GET_CAR_PRICE = 'finance/offer'
export const METHOD_ADD_LOAN_LEAD = 'lead/addlead'
export const METHOD_LOAN_DOC_SAVE = 'lead/save_doc'
export const METHOD_DELETE_IMAGE_FROM_SERVER = 'delete_all_images'
export const METHOD_UPDATE_FCM_TOKEN = 'fcm/update_fcm_token'
export const METHOD_LOAN_LEAD_COUNT = "lead_dashboard"
export const METHOD_GET_LOAN_LEAD = "getleads"
export const METHOD_GET_LOAN_DASHBOARD = "dashboard"
export const METHOD_GET_OFFER_DETAIL = 'finance/offer_details'
export const METHOD_GET_MASTER_DATA = 'master'
export const METHOD_DELETE_FCM_TOKEN = 'fcm/disable_fcm_token'
export const METHOD_SIGNUP = 'lead/enquiry/saveEnquiry'
export const METHOD_ADD_TO_BUMPUP = 'bump_up_stock'
export const METHOD_RULE_ENGINE_MMV = 'sync_mmv_all'
export const METHOD_CALCULATE_LOAN = 'calculate_loan'
export const METHOD_GET_LOAN_TIMELINE = 'lead_timeline'
export const METHOD_GET_NOTIFICATION_DATA = 'gcloud/get_notification'
export const METHOD_UPDATE_NOTIFICATION_STATUS = 'gcloud/read_notification'
export const METHOD_LOAN_SHARE_MRP_DATA = 'share/mrp_data'
export const METHOD_GET_PACKAGE_DETAILS = 'list'
export const METHOD_PURCHASE_PACKAGE = 'buy'
export const METHOD_RENEW_PACKAGE = 'renew'
export const METHOD_AUTO_CLASSIFIED_CHECK = 'auto_classified_check'

//urls
export const CHECK_FORCE_UPDATE = BASE_URL + GCLOUD_DIRECTORY + METHOD_CHECK_FORCE_UPDATE
export const LOGIN = BASE_URL + USER_DIRECTORY + METHOD_LOGIN
export const SEND_OTP = BASE_URL + USER_DIRECTORY + METHOD_SEND_OTP
export const OTP_LOGIN = BASE_URL + USER_DIRECTORY + METHOD_OTP_LOGIN
export const VALIDATE_OTP = BASE_URL + USER_DIRECTORY + METHOD_VALIDATE_OTP
export const SET_PASSWORD = BASE_URL + USER_DIRECTORY + METHOD_SET_PASSWORD
export const DEALER_LOCALITY = BASE_URL + COMMON_DIRECTORY + METHOD_DEALER_LOCALITY
export const GET_DEALER_CONFIG = BASE_URL + CONFIG_DIRECTORY + METHOD_DEALER_CONFIG
export const GET_CONFIG = BASE_URL + INVENTORY_DIRECTORY + METHOD_GET_CONFIG
export const GET_BUDGET = BASE_URL + LEAD_DIRECTORY + METHOD_GET_BUDGET
export const GET_STOCK_LISTING = BASE_URL + INVENTORY_DIRECTORY + METHOD_STOCK_LISTING
export const GET_LEAD_COUNT = BASE_URL + LEAD_DIRECTORY + METHOD_LEAD_COUNT
export const ADD_LEAD = BASE_URL + LEAD_DIRECTORY + METHOD_ADD_LEAD
export const GET_LEAD_LISTING = BASE_URL + LEAD_DIRECTORY + METHOD_LEAD_LISTING
export const GET_LEAD_DETAIL = BASE_URL + LEAD_DIRECTORY + METHOD_LEAD_DETAIL
export const UPDATE_LEAD_CUSTOMER = BASE_URL + LEAD_DIRECTORY + METHOD_UPDATE_LEAD_CUSTOMER
export const GET_LEAD_REQUIREMENT = BASE_URL + LEAD_DIRECTORY + METHOD_GET_LEAD_REQUIREMENT
export const UPDATE_LEAD_REQUIREMENT = BASE_URL + LEAD_DIRECTORY + METHOD_UPDATE_LEAD_REQUIREMENT
export const UPDATE_LEAD_STATUS = BASE_URL + LEAD_DIRECTORY + METHOD_UPDATE_LEAD_STATUS
export const SHARE_LEAD = BASE_URL + LEAD_DIRECTORY + METHOD_SHARE_LEAD
export const SHARE_LEAD_EMAIL = BASE_URL + LEAD_DIRECTORY + METHOD_SHARE_LEAD_EMAIL
export const SHARE_CARDETAIL_EMAIL = BASE_URL + INVENTORY_DIRECTORY + METHOD_SHARE_CARDETAIL_EMAIL
export const UPDATE_FAVOURITE = BASE_URL + LEAD_DIRECTORY + METHOD_UPDATE_FAVOURITE
export const GET_STOCK_DETAIL = BASE_URL + INVENTORY_DIRECTORY + METHOD_STOCK_DETAIL
export const GET_MMV = BASE_URL + METHOD_MMV
export const MAKE_STOCK_PREMIUM = BASE_URL + INVENTORY_DIRECTORY + METHOD_MAKE_STOCK_PREMIUM
export const REMOVE_STOCK = BASE_URL + INVENTORY_DIRECTORY + METHOD_REMOVE_STOCK
export const GET_DASHBOARD = BASE_URL + DEALER_DIRECTORY + METHOD_DASHBOARD
export const ALL_COUNTRY = BASE_URL + METHOD_ALL_COUNTRY
export const ALL_CITY = BASE_URL + METHOD_ALL_CITY
export const ADD_STOCK = BASE_URL + INVENTORY_DIRECTORY + METHOD_ADD_STOCK
export const GET_COLOR_LIST = BASE_URL + METHOD_GET_COLOR
export const UPLOAD_TO_S3 = BASE_URL + METHOD_UPLOAD_TO_S3
export const SEND_IMAGE_TO_SERVER = BASE_URL + INVENTORY_DIRECTORY + METHOD_SEND_IMAGE_TO_SERVER
export const EDIT_STOCK = BASE_URL + INVENTORY_DIRECTORY + METHOD_EDIT_STOCK
export const GET_LEADS_BY_CAR_ID = BASE_URL + LEAD_DIRECTORY + METHOD_GET_LEADS_BY_CAR_ID
export const SAVE_INFO = BASE_URL + DEALER_DIRECTORY + METHOD_SAVE_INFO
export const CHANGE_PASSWORD = BASE_URL + DEALER_MANAGER_DIRECTORY + METHOD_CHANGE_PASSWORD
export const UPDATE_CAR_PRICE = BASE_URL + INVENTORY_DIRECTORY + METHOD_UPDATE_CAR_PRICE
export const GET_LOAN_CONFIG = BASE_URL + LOAN_DIRECTORY + METHOD_LOAN_CONFIG
export const GET_CAR_PRICE = BASE_URL + LOAN_DIRECTORY + METHOD_GET_CAR_PRICE
export const ADD_LOAN_LEAD = BASE_URL + LOAN_DIRECTORY + METHOD_ADD_LOAN_LEAD
export const LOAN_DOC_SAVE = BASE_URL + LOAN_DIRECTORY + METHOD_LOAN_DOC_SAVE
export const DELETE_IMAGE_FROM_SERVER = BASE_URL + INVENTORY_DIRECTORY + METHOD_DELETE_IMAGE_FROM_SERVER
export const UPDATE_FCM_TOKEN = BASE_URL + NOTIFICATION_DIRECTORY + METHOD_UPDATE_FCM_TOKEN
export const LOAN_LEAD_COUNT = BASE_URL + LOAN_LEAD_DIRECTORY + METHOD_LOAN_LEAD_COUNT
export const GET_LOAN_LEAD = BASE_URL + LOAN_LEAD_DIRECTORY + METHOD_GET_LOAN_LEAD
export const GET_LOAN_DASHBOARD = BASE_URL + LOAN_LEAD_DIRECTORY + METHOD_GET_LOAN_DASHBOARD
export const GET_OFFER_DETAIL = BASE_URL + LOAN_DIRECTORY + METHOD_GET_OFFER_DETAIL
export const GET_MASTER_DATA = BASE_URL + COMMON_DIRECTORY + METHOD_GET_MASTER_DATA
export const DELETE_FCM_TOKEN = BASE_URL + NOTIFICATION_DIRECTORY + METHOD_DELETE_FCM_TOKEN
export const SIGNUP = BASE_URL + METHOD_SIGNUP
export const ADD_TO_BUMPUP = BASE_URL + INVENTORY_DIRECTORY + METHOD_ADD_TO_BUMPUP
export const GET_LOAN_TIMELINE = BASE_URL + LOAN_LEAD_DIRECTORY + METHOD_GET_LOAN_TIMELINE
export const GET_NOTIFICATION_DATA = BASE_URL + NOTIFICATION_DIRECTORY + METHOD_GET_NOTIFICATION_DATA
export const UPDATE_NOTIFICATION_STATUS = BASE_URL + NOTIFICATION_DIRECTORY + METHOD_UPDATE_NOTIFICATION_STATUS
export const GET_RULE_ENGINE_MMV = BASE_URL + RULE_ENGINE_DIRECTORY + METHOD_RULE_ENGINE_MMV
export const CALCULATE_LOAN = BASE_URL + RULE_ENGINE_DIRECTORY + METHOD_CALCULATE_LOAN
export const LOAN_SHARE_MRP_DATA = BASE_URL + LOAN_DIRECTORY + METHOD_LOAN_SHARE_MRP_DATA
export const GET_PACKAGE_DETAILS = BASE_URL + DEALER_SUBSCRIPTION + METHOD_GET_PACKAGE_DETAILS
export const PURCHASE_PACKAGE = BASE_URL + DEALER_SUBSCRIPTION + METHOD_PURCHASE_PACKAGE
export const RENEW_PACKAGE = BASE_URL + DEALER_SUBSCRIPTION + METHOD_RENEW_PACKAGE
export const AUTO_CLASSIFIED_CHECK = BASE_URL + INVENTORY_DIRECTORY + METHOD_AUTO_CLASSIFIED_CHECK