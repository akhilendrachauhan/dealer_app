import {APP_TYPE,INDONESIA} from '../util/Constants';
export const DATABASE_NAME = (APP_TYPE == INDONESIA ? 'AppDatabase.db' : 'CarmudiDatabase.db');
export const DATABASE_VERSION = 6;
export const DATABASE_DISPLAY_NAME = 'Classified Database';
export const DATABASE_SIZE = 200000;
//Version Table
export const DB_VERSION_TABLE = 'dbVersionTable';
export const KEY_DB_VERSION = 'version';

//MMV Table
export const MMV_TABLE = 'mmvTable';
export const VERSION = 'vn';
export const VERSION_ID = 'vn_id';
export const MAKE_ID = 'mk_id';
export const MODEL_ID = 'md_id';
export const VERSION_START_YEAR = 's_year';
export const VERSION_END_YEAR = 'e_year';
export const FUEL_TYPE = 'f_type';
export const BODY_TYPE = 'c_b_type';
export const TRANSMISSION = 'tms';
export const MAKE = 'mk';
export const MODEL = 'md';
export const MAKE_MODEL = 'makeModel';
export const MODEL_PARENT_ID = 'p_m_id'
export const MAKE_PARENT_MODEL = 'make_parent_model'
export const YEAR_CONDITION_ENABLED = 'yc'
export const MODEL_PARENT_NAME = 'p_m_n'

//Country Table
export const COUNTRY_TABLE = 'countryTable';
export const COUNTRY = 'country_name';
export const COUNTRY_FLAG = 'flag_image';
export const COUNTRY_CODE = 'country_code';
export const COUNTRY_ID = 'country_id';
export const COUNTRY_CURRENCY = 'country_currency';
export const COUNTRY_BASE_URL = 'country_url';

//Language Table
export const LANGUAGE_TABLE = 'languageTable';
export const LANGUAGE = 'language_name';
export const LANGUAGE_CODE = 'language_code';
export const DATE_FORMAT = 'date_format'
export const DATE_FORMAT_FULL = 'date_format_full'
export const LANGUAGE_ID = 'lang_id'
export const UNIQUE_ID = 'uniqueId'

//City Table
export const CITY_TABLE = 'cityTable';
export const CITY = 'name';
export const CITY_ID = 'id';
export const STATE_ID = 'state_id';

//Image Table
export const IMAGE_TABLE = 'imageTable'
export const CAR_ID = 'car_id'
//export const MAKE_MODEL = 'make_model'
export const IMAGE_ID = 'image_id'
export const IMAGE_NAME = 'image_name'
export const IMAGE_PATH = 'image_path'
export const STATUS = 'status'
export const TOTAL_IMAGES = 'total_images'
export const SEQUENCE = 'sequence'

//Image Table
export const LOCALITY_TABLE = 'localityTable'
export const LOCALITY_ID = 'id'
export const CITY__ID = 'city_id'
export const LOCALITY = 'name'

//Document Image Table
export const DOCUMENT_IMAGE_TABLE = 'documentImageTable'
export const LEAD_ID = 'lead_id'
export const DOCUMENT_ID = 'document_id'
export const DOCUMENT_PARENT_ID = 'document_parent_id'
export const IS_CALCULATOR_IMAGE = 'is_calculator_image'
// export const IMAGE_ID = 'image_id'
// export const IMAGE_NAME = 'image_name'
// export const IMAGE_PATH = 'image_path'
// export const STATUS = 'status'
export const DEALER_ID_HASH = 'dealer_id_hash'

// Financier MMV Table
export const FINANCIER_MMV_TABLE = 'financier_mmv_table'
export const FINANCIER_VERSION_ID = 'v_id'
export const FINANCIER_MAKE_ID = 'mk_id'
export const FINANCIER_MODEL_ID = 'md_id'
export const FINANCIER_MAKE_NAME = 'mk_n'
export const FINANCIER_BRAND_TYPE = 'b_type'
export const FINANCIER_MODEL_NAME = 'md_n'
export const FINANCIER_MAKE_MODEL_NAME = 'mk_md_n'
export const FINANCIER_VERSION_NAME = 'v_n'
export const FINANCIER_VEHICLE_TYPE_ID = 'v_t_id'
export const FINANCIER_AREA_ID = 'a_id'
export const FINANCIER_ID = 'f_id'

// MAKE Year Price Table 

export const MYP_TABLE = 'make_year_price_table'
export const MYP_ID = 'id'
export const MYP_VERSION_ID = 'v_id'
export const MYP_MAKE_YEAR = 'mk_year'
export const MYP_PRICE = 'price'
export const MYP_AIREA_ID = 'a_id'

// 

export const MIGRATION_DONE = 'migration_done'
