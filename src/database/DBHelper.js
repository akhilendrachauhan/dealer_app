import SQLite from 'react-native-sqlite-storage';
import * as DBConstants from './DBConstants';
import * as Utility from '../util/Utility';
import * as APIConstant from "../api/APIConstants"

export default class DBHelper {
    initDB() {
        let db;
        SQLite.DEBUG(APIConstant.CONSOLE_ENABLED);
        SQLite.enablePromise(true);
        return new Promise((resolve) => {
            /*Utility.log("Plugin integrity check ...");
            SQLite.echoTest()
                .then(() => {*/
            Utility.log("Integrity check passed ...");
            Utility.log("Opening database ...");
            SQLite.openDatabase(
                DBConstants.DATABASE_NAME,
                2,
                DBConstants.DATABASE_DISPLAY_NAME,
                DBConstants.DATABASE_SIZE).then(DB => {
                    db = DB;
                    Utility.log("Version", db);
                    this.getDatabaseVersion(db).then(version => {
                        Utility.log("Version", version);
                        db.transaction((tx) => {
                            if (version < DBConstants.DATABASE_VERSION) {
                                this.createTables(tx, true, version)

                            } else {
                                this.createTables(tx, false, version)
                            }
                        }).then(() => {
                            resolve(db);
                        }).catch(error => {
                            Utility.log(error);
                        });
                    }).catch(error => {
                        Utility.log(error + "hello");
                    });;
                    resolve(db);
                    Utility.log("Database OPEN");
                })
                .catch(error => {
                    Utility.log("database version problem:" + error);
                });
        });
    };

    getDatabaseVersion(database): Promise<number> {
        // Select the highest version number from the version table
        return database
            .executeSql('SELECT ' + DBConstants.KEY_DB_VERSION + ' FROM ' + DBConstants.DB_VERSION_TABLE + ' ORDER BY ' + DBConstants.KEY_DB_VERSION + ' DESC LIMIT 1')
            .then(([results]) => {
                if (results.rows && results.rows.length > 0) {
                    return results.rows.item(0).version;
                } else {
                    return 0;
                }
            })
            .catch(error => {
                Utility.log(`No version set. Returning 0. Details: ${error}`);
                return 0;
            });
    }

    alterTable(transaction) {
        transaction.executeSql('ALTER TABLE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' ADD COLUMN ' + DBConstants.DEALER_ID_HASH + ' TEXT');
        // Utility.getValueFromAsyncStorage(DBConstants.MIGRATION_DONE).then((status) => {
        //     if(status && status == '0'){
        //         transaction.executeSql('ALTER TABLE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' ADD COLUMN '+ DBConstants.DEALER_ID_HASH +' TEXT'); 
        //     }
        // }).catch((error)=>{
        //     Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        // });
    }

    migration4To5(transaction) {
        transaction.executeSql('ALTER TABLE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' ADD COLUMN ' + DBConstants.DOCUMENT_PARENT_ID + ' TEXT');
        transaction.executeSql('ALTER TABLE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' ADD COLUMN ' + DBConstants.IS_CALCULATOR_IMAGE + ' TEXT');
        // Utility.getValueFromAsyncStorage(DBConstants.MIGRATION_DONE).then((status) => {
        //     if(status && status == '0'){
        //         transaction.executeSql('ALTER TABLE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' ADD COLUMN '+ DBConstants.DOCUMENT_PARENT_ID +' TEXT'); 
        //         Utility.setValueInAsyncStorage(DBConstants.MIGRATION_DONE, '1')
        //     }
        // }).catch((error)=>{
        //     Utility.log("errrrrrrrrrrrrrrrrrrrrr",'error');
        // });
    }

    createTables(transaction, dropTables, version) {
        if (dropTables) {
            transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.DB_VERSION_TABLE);
            transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.MMV_TABLE);
            // transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.COUNTRY_TABLE);
            // transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.LANGUAGE_TABLE);
            // transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.CITY_TABLE);
            // transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.IMAGE_TABLE);
            // transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.LOCALITY_TABLE);
            //transaction.executeSql('DROP TABLE IF EXISTS ' + DBConstants.DOCUMENT_IMAGE_TABLE);
            //this.alterTable(transaction)
        }

        // List table
        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.DB_VERSION_TABLE + '('
            + DBConstants.KEY_DB_VERSION + ' INTEGER NOT NULL)');
        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.MMV_TABLE + '('
            + DBConstants.VERSION_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.VERSION + ' TEXT, '
            + DBConstants.MAKE + ' TEXT, '
            + DBConstants.MAKE_ID + ' TEXT, '
            + DBConstants.MODEL + ' TEXT, '
            + DBConstants.MODEL_ID + ' TEXT, '
            + DBConstants.VERSION_START_YEAR + ' INTEGER, '
            + DBConstants.VERSION_END_YEAR + ' INTEGER, '
            + DBConstants.YEAR_CONDITION_ENABLED + ' INTEGER, '
            + DBConstants.FUEL_TYPE + ' TEXT, '
            + DBConstants.TRANSMISSION + ' TEXT, '
            + DBConstants.MAKE_MODEL + ' TEXT, '
            + DBConstants.BODY_TYPE + ' TEXT, '
            + DBConstants.MODEL_PARENT_ID + ' TEXT, '
            + DBConstants.MAKE_PARENT_MODEL + ' TEXT)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.COUNTRY_TABLE + '('
            + DBConstants.COUNTRY_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.COUNTRY_CODE + ' TEXT, '
            + DBConstants.COUNTRY + ' TEXT, '
            + DBConstants.COUNTRY_FLAG + ' TEXT, '
            + DBConstants.COUNTRY_BASE_URL + ' TEXT, '
            + DBConstants.COUNTRY_CURRENCY + ' TEXT)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.LANGUAGE_TABLE + '('
            + DBConstants.UNIQUE_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.LANGUAGE_ID + ' TEXT, '
            + DBConstants.LANGUAGE_CODE + ' TEXT, '
            + DBConstants.LANGUAGE + ' TEXT, '
            + DBConstants.DATE_FORMAT + ' TEXT, '
            + DBConstants.COUNTRY_ID + ' TEXT, '
            + DBConstants.DATE_FORMAT_FULL + ' TEXT)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.CITY_TABLE + '('
            + DBConstants.CITY_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.CITY + ' TEXT, '
            + DBConstants.STATE_ID + ' TEXT)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.IMAGE_TABLE + '('
            + DBConstants.IMAGE_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.CAR_ID + ' TEXT, '
            + DBConstants.MAKE_MODEL + ' TEXT, '
            + DBConstants.IMAGE_NAME + ' TEXT, '
            + DBConstants.IMAGE_PATH + ' TEXT, '
            + DBConstants.STATUS + ' INTEGER, '
            + DBConstants.TOTAL_IMAGES + ' INTEGER, '
            + DBConstants.SEQUENCE + ' INTEGER)');


        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.LOCALITY_TABLE + '('
            + DBConstants.LOCALITY_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.CITY__ID + ' TEXT, '
            + DBConstants.LOCALITY + ' TEXT)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.DOCUMENT_IMAGE_TABLE + '('
            + DBConstants.IMAGE_ID + ' TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.LEAD_ID + ' TEXT, '
            + DBConstants.DOCUMENT_ID + ' TEXT, '
            + DBConstants.IMAGE_NAME + ' TEXT, '
            + DBConstants.IMAGE_PATH + ' TEXT, '
            + DBConstants.STATUS + ' INTEGER)');

        if (version < 3) {
            this.alterTable(transaction)
        }

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.FINANCIER_MMV_TABLE + '('
            + DBConstants.FINANCIER_VERSION_ID + ' INTEGER NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.FINANCIER_MAKE_ID + ' INTEGER, '
            + DBConstants.FINANCIER_MODEL_ID + ' INTEGER, '
            + DBConstants.FINANCIER_VERSION_NAME + ' TEXT, '
            + DBConstants.FINANCIER_MAKE_NAME + ' TEXT, '
            + DBConstants.FINANCIER_BRAND_TYPE + ' TEXT, '
            + DBConstants.FINANCIER_MODEL_NAME + ' TEXT, '
            + DBConstants.FINANCIER_MAKE_MODEL_NAME + ' TEXT, '
            + DBConstants.FINANCIER_VEHICLE_TYPE_ID + ' INTEGER, '
            + DBConstants.FINANCIER_AREA_ID + ' INTEGER, '
            + DBConstants.FINANCIER_ID + ' INTEGER)');

        transaction.executeSql('CREATE TABLE IF NOT EXISTS ' + DBConstants.MYP_TABLE + '('
            + DBConstants.MYP_ID + ' INTEGER NOT NULL PRIMARY KEY ON CONFLICT REPLACE, '
            + DBConstants.MYP_VERSION_ID + ' INTEGER, '
            + DBConstants.MYP_MAKE_YEAR + ' INTEGER, '
            + DBConstants.MYP_PRICE + ' TEXT, '
            + DBConstants.MYP_AIREA_ID + ' INTEGER, '
            + 'FOREIGN KEY ( ' +DBConstants.MYP_VERSION_ID +') REFERENCES '+ DBConstants.FINANCIER_MMV_TABLE+'('+ DBConstants.FINANCIER_VERSION_ID +'))');
        
        if (version < 5) {
            Utility.setValueInAsyncStorage(DBConstants.MIGRATION_DONE, '0')
            this.migration4To5(transaction)
        }

        transaction.executeSql('INSERT INTO ' + DBConstants.DB_VERSION_TABLE + ' VALUES (?)', [DBConstants.DATABASE_VERSION]);

    }
}