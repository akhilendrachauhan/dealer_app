import DBHelper from "./DBHelper";
import * as DBConstants from './DBConstants';
import * as Utility from '../util/Utility';
import { LANGUAGE_ID } from "./DBConstants";
import { Constants } from "../util";
import AsyncStore from '../util/AsyncStore'

let dbHelper = undefined;
export default class DBFunctions {
    getDbHelper() {
        if (!dbHelper) {
            dbHelper = new DBHelper();
        }
        return dbHelper;
    }

    closeDatabase(db) {
        if (db) {
            Utility.log("Closing DB");
            db.close()
                .then(status => {
                    Utility.log("Database CLOSED");
                })
                .catch(error => {
                    Utility.log(error);
                });
        } else {
            Utility.log("Database was not OPENED");
        }
    };

    getDataByQuery(query) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(query, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    deleteMMV() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM ' + DBConstants.MMV_TABLE, []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertMMV(data) {
        //inserting 80 data in a batch
        //12 columns in the table
        let parameters = [], bigqery = "", count = 0 , noOfRecords = 60, noOfColumns = 15;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    let subArrayCount = 0;
                    if (data.length > 0) {
                        subArrayCount = (data.length) / noOfRecords;
                        if ((subArrayCount % 1) !== 0) {
                            subArrayCount = parseInt(subArrayCount.toString().split(".")[0]) + 1;
                        }
                    }
                    for (let i = 0; i < subArrayCount; i++) {
                        let dataToProcess = [];
                        if (data.length > noOfRecords) {
                            dataToProcess = data.splice(0, noOfRecords)
                        } else {
                            dataToProcess = data
                        }
                        Utility.log('data to process', dataToProcess.length);
                        dataToProcess.forEach((item) => {
                            bigqery += "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?),";
                            if(Constants.APP_TYPE == Constants.PHILIPPINES)
                                parameters.push(item[DBConstants.VERSION_ID].toString(), item[DBConstants.VERSION], item[DBConstants.MAKE_ID].toString(), item[DBConstants.MAKE], item[DBConstants.MODEL_ID].toString(), item[DBConstants.MODEL], item[DBConstants.VERSION_START_YEAR], item[DBConstants.VERSION_END_YEAR], item[DBConstants.FUEL_TYPE], item[DBConstants.TRANSMISSION], item[DBConstants.BODY_TYPE], (item[DBConstants.MAKE] + ' ' + item[DBConstants.MODEL]),item[DBConstants.MODEL_PARENT_ID], (item[DBConstants.MAKE] + ' ' + item[DBConstants.MODEL_PARENT_NAME]), (item[DBConstants.YEAR_CONDITION_ENABLED] ? item[DBConstants.YEAR_CONDITION_ENABLED] : 0));
                            else
                                parameters.push(item[DBConstants.VERSION_ID].toString(), item[DBConstants.VERSION], item[DBConstants.MAKE_ID].toString(), item[DBConstants.MAKE], item[DBConstants.MODEL_ID].toString(), item[DBConstants.MODEL], item[DBConstants.VERSION_START_YEAR], item[DBConstants.VERSION_END_YEAR], item[DBConstants.FUEL_TYPE], item[DBConstants.TRANSMISSION], item[DBConstants.BODY_TYPE], (item[DBConstants.MAKE] + ' ' + item[DBConstants.MODEL]),item[DBConstants.MODEL_ID], (item[DBConstants.MAKE] + ' ' + item[DBConstants.MODEL]), (item[DBConstants.YEAR_CONDITION_ENABLED] ? item[DBConstants.YEAR_CONDITION_ENABLED] : 0));
                            if (parameters.length === ((dataToProcess.length) * noOfColumns)) {
                                bigqery = bigqery.slice(0, -1);
                                tx.executeSql('INSERT INTO ' + DBConstants.MMV_TABLE + ' (' + DBConstants.VERSION_ID + ', ' + DBConstants.VERSION + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL_ID + ', ' + DBConstants.MODEL + ', ' + DBConstants.VERSION_START_YEAR + ', ' + DBConstants.VERSION_END_YEAR + ', ' + DBConstants.FUEL_TYPE + ', ' + DBConstants.TRANSMISSION + ', ' + DBConstants.BODY_TYPE + ', ' + DBConstants.MAKE_MODEL+ ', ' + DBConstants.MODEL_PARENT_ID+ ', ' + DBConstants.MAKE_PARENT_MODEL + ', ' + DBConstants.YEAR_CONDITION_ENABLED + ')' + ' VALUES ' + bigqery + ";", parameters).then(([tx, results]) => {
                                    count = count + dataToProcess.length;
                                });
                                parameters = [];
                                bigqery = '';
                            }
                        });
                    }
                }).then((result) => {
                    Utility.log('MMV Inserted', count);

                    resolve(count);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMMV() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM ' + DBConstants.MMV_TABLE, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getDistinctModels() {
        /*"Select "+allColumns+" from (SELECT DISTINCT "+allColumns+" FROM version WHERE mpm LIKE '"+searchQuery+"'  ORDER BY CAST(popularity AS INTEGER) DESC) as new_tbl group by mpm order by CAST(popularity AS INTEGER) ASC "*/
        let allColumns = DBConstants.MODEL_ID + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL + ', ' + DBConstants.MAKE_MODEL+ ', ' + DBConstants.YEAR_CONDITION_ENABLED;
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT " + allColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " ) as new_tbl group by " + DBConstants.MODEL_ID, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getDistinctMakeModelsQueryText() {
        let allColumns = DBConstants.MODEL_ID + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL + ', ' + DBConstants.MAKE_MODEL+ ', ' + DBConstants.YEAR_CONDITION_ENABLED;
        let newColumns = allColumns + ', ' + DBConstants.MAKE_MODEL + ' as text';
        return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " ) as new_tbl group by " + DBConstants.MODEL_ID);
    }

    getDistinctMakeParentModelsQueryText() {
        let allColumns = DBConstants.VERSION_START_YEAR + ', ' + DBConstants.VERSION_END_YEAR + ', ' + DBConstants.MODEL_PARENT_ID  + ', ' + DBConstants.MAKE_PARENT_MODEL+ ', ' + DBConstants.YEAR_CONDITION_ENABLED;
        let newColumns = allColumns + ', ' + DBConstants.MAKE_PARENT_MODEL + ' as text';
        return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " ) as new_tbl group by " + DBConstants.MODEL_PARENT_ID);
    }

    getCityQueryText() {
        let allColumns = DBConstants.CITY_ID + ', ' + DBConstants.CITY + ', ' + DBConstants.STATE_ID;
        let newColumns = allColumns + ', ' + DBConstants.CITY + ' as text';
        return ("SELECT " + newColumns + " FROM " + DBConstants.CITY_TABLE);
    }

    getLocalityQueryText() {
        let allColumns = DBConstants.LOCALITY_ID + ', ' + DBConstants.CITY__ID + ', ' + DBConstants.LOCALITY;
        let newColumns = allColumns + ', ' + DBConstants.LOCALITY + ' as text';
        return ("SELECT " + newColumns + " FROM " + DBConstants.LOCALITY_TABLE);
    }

    getDistinctFinancierMakeModelsQueryText(financierId) {
        let allColumns = DBConstants.FINANCIER_ID + ', ' + DBConstants.FINANCIER_MODEL_ID + ', ' + DBConstants.FINANCIER_MAKE_ID + ', ' + DBConstants.FINANCIER_MAKE_NAME + ', ' + DBConstants.FINANCIER_MODEL_NAME + ', ' + DBConstants.FINANCIER_MAKE_MODEL_NAME;
        let newColumns = allColumns + ', ' + DBConstants.FINANCIER_MAKE_MODEL_NAME + ' as text';
        // return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " ) as new_tbl group by " + DBConstants.FINANCIER_MODEL_ID);
        return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " WHERE "+ DBConstants.FINANCIER_ID + " = " + financierId + " ) as new_tbl group by " + DBConstants.FINANCIER_MODEL_ID);
    }

    getDistinctFinancierMakeQueryText(financierId) {
        let allColumns = DBConstants.FINANCIER_ID + ', ' + DBConstants.FINANCIER_MAKE_ID + ', ' + DBConstants.FINANCIER_MAKE_NAME + ', ' + DBConstants.FINANCIER_BRAND_TYPE;
        let newColumns = allColumns + ', ' + DBConstants.FINANCIER_MAKE_NAME + ' as text';
        // return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " ) as new_tbl group by " + DBConstants.FINANCIER_MODEL_ID);
        return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " WHERE "+ DBConstants.FINANCIER_ID + " = " + financierId + " ) as new_tbl group by " + DBConstants.FINANCIER_MAKE_ID);
    }

    getDistinctFinancierModelQueryText(makeId, financierId) {
        let allColumns = DBConstants.FINANCIER_ID + ', ' + DBConstants.FINANCIER_MAKE_ID + ', ' + DBConstants.FINANCIER_MODEL_ID + ', ' + DBConstants.FINANCIER_MODEL_NAME;
        let newColumns = allColumns + ', ' + DBConstants.FINANCIER_MODEL_NAME + ' as text';
        // return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " ) as new_tbl group by " + DBConstants.FINANCIER_MODEL_ID);
        return ("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.FINANCIER_MMV_TABLE + " WHERE "+ DBConstants.FINANCIER_ID + " = " + financierId + " AND " + DBConstants.FINANCIER_MAKE_ID + " = " + makeId + " ) as new_tbl group by " + DBConstants.FINANCIER_MODEL_ID);
    }

    // 'SELECT * FROM ' + DBConstants.FINANCIER_MMV_TABLE + ' WHERE '+ DBConstants.FINANCIER_ID + ' = '+financier_id, []

    /*getDistinctMakeModels() {
        /!*"Select "+allColumns+" from (SELECT DISTINCT "+allColumns+" FROM version WHERE mpm LIKE '"+searchQuery+"'  ORDER BY CAST(popularity AS INTEGER) DESC) as new_tbl group by mpm order by CAST(popularity AS INTEGER) ASC "*!/
        let allColumns = DBConstants.MODEL_ID + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL + ', ' + DBConstants.MAKE_MODEL;
        let newColumns = allColumns + ', '+DBConstants.MAKE_MODEL + ' as text';
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT " + newColumns + " FROM (SELECT DISTINCT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " ) as new_tbl group by " + DBConstants.MODEL_ID, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        resolve(products);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }*/

    getVersionByMakeModel(makeId, modelId) {
        /*"Select "+allColumns+" from (SELECT DISTINCT "+allColumns+" FROM version WHERE mpm LIKE '"+searchQuery+"'  ORDER BY CAST(popularity AS INTEGER) DESC) as new_tbl group by mpm order by CAST(popularity AS INTEGER) ASC "*/
        let allColumns = DBConstants.MODEL_ID + ', ' + DBConstants.VERSION_ID + ', ' + DBConstants.VERSION + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL + ', ' + DBConstants.VERSION_START_YEAR + ', ' + DBConstants.VERSION_END_YEAR + ', ' + DBConstants.FUEL_TYPE + ', ' + DBConstants.TRANSMISSION + ', ' + DBConstants.BODY_TYPE + ', ' + DBConstants.MAKE_MODEL;
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MAKE_ID + " = '" + makeId + "' AND " + DBConstants.MODEL_ID + " = '" + modelId + "' ORDER BY " + DBConstants.FUEL_TYPE + " DESC", []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getVersionByModelParentYear(modelParentId, makeYear, yearFilterEnabled) {
        /*"Select "+allColumns+" from (SELECT DISTINCT "+allColumns+" FROM version WHERE mpm LIKE '"+searchQuery+"'  ORDER BY CAST(popularity AS INTEGER) DESC) as new_tbl group by mpm order by CAST(popularity AS INTEGER) ASC "*/
        let allColumns = DBConstants.MODEL_ID + ', ' + DBConstants.VERSION_ID + ', ' + DBConstants.VERSION + ', ' + DBConstants.MAKE_ID + ', ' + DBConstants.MAKE + ', ' + DBConstants.MODEL + ', ' + DBConstants.VERSION_START_YEAR + ', ' + DBConstants.VERSION_END_YEAR + ', ' + DBConstants.FUEL_TYPE + ', ' + DBConstants.TRANSMISSION + ', ' + DBConstants.BODY_TYPE + ', ' + DBConstants.MAKE_MODEL;
        let query;
        if(yearFilterEnabled && yearFilterEnabled == 1){
            query = "SELECT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MODEL_PARENT_ID + " = '" + modelParentId + "' AND (" + DBConstants.VERSION_START_YEAR + " <= " + makeYear + " AND "+DBConstants.VERSION_END_YEAR + " >= " + makeYear+") ORDER BY " + DBConstants.FUEL_TYPE + " DESC";
        }else {
            query = "SELECT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MODEL_PARENT_ID + " = '" + modelParentId + "'" +" group by " + DBConstants.VERSION +" ORDER BY "+ DBConstants.FUEL_TYPE + " DESC"  ;
        }

        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    Utility.log('DB Version Query', query)

                    //Utility.log('DB Version Query', "SELECT " + allColumns + " FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MODEL_PARENT_ID + " = '" + modelParentId + "' AND (" + DBConstants.VERSION_START_YEAR + " <= " + makeYear + " AND "+DBConstants.VERSION_END_YEAR + " >= " + makeYear+") ORDER BY " + DBConstants.FUEL_TYPE + " DESC")
                    tx.executeSql(query, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMMVTotalCount() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT COUNT(*) as count FROM ' + DBConstants.MMV_TABLE + ';', []).then(([tx, results]) => {
                        let count = 0;
                        if (results.rows.length > 0) {
                            count = results.rows.item(0).count
                        }

                        resolve(count);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getModelMinMaxYear(parent_model_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT MIN('+DBConstants.VERSION_START_YEAR+') as minYear, MAX('+DBConstants.VERSION_END_YEAR+') as maxYear FROM ' + DBConstants.MMV_TABLE + ' WHERE '+DBConstants.MODEL_PARENT_ID +'='+parent_model_id, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    deleteLocality() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM ' + DBConstants.LOCALITY_TABLE, []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertLocality(data) {
        Utility.log('dataLength', data.length)
        //inserting 80 data in a batch
        //3 columns in the table
        let parameters = [], query = "", localityCount = 0;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    let subArrayCount = 0;
                    if (data.length > 0) {
                        subArrayCount = (data.length) / 80;
                        if ((subArrayCount % 1) !== 0) {
                            subArrayCount = parseInt(subArrayCount.toString().split(".")[0]) + 1;
                        }
                    }
                    for (let i = 0; i < subArrayCount; i++) {
                        let dataToProcess = [];
                        if (data.length > 80) {
                            dataToProcess = data.splice(0, 80)
                        } else {
                            dataToProcess = data
                        }
                        Utility.log('data to process', dataToProcess.length)
                        dataToProcess.forEach((item) => {
                            query += "(?, ?, ?),";
                            parameters.push(item[DBConstants.LOCALITY_ID].toString(), item[DBConstants.CITY__ID].toString(), item[DBConstants.LOCALITY].toString());
                            if (parameters.length === ((dataToProcess.length) * 3)) {
                                query = query.slice(0, -1);
                                tx.executeSql('INSERT INTO ' + DBConstants.LOCALITY_TABLE + ' (' + DBConstants.LOCALITY_ID + ', ' + DBConstants.CITY__ID + ', ' + DBConstants.LOCALITY + ')' + ' VALUES ' + query + ";", parameters).then(([tx, results]) => {
                                    localityCount = localityCount + dataToProcess.length;
                                });
                                parameters = [];
                                query = '';
                            }
                        });
                    }
                }).then((result) => {
                    Utility.log('locality Inserted', localityCount);

                    resolve(localityCount);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getLocalityById(id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.LOCALITY_TABLE + " WHERE " + DBConstants.LOCALITY_ID + " = " + id, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getLocalityTotalCount() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT COUNT(*) as count FROM ' + DBConstants.LOCALITY_TABLE + ';', []).then(([tx, results]) => {
                        let count = 0;
                        if (results.rows.length > 0) {
                            count = results.rows.item(0).count
                        }

                        resolve(count);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    deleteCity() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM ' + DBConstants.CITY_TABLE, []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertCity(data) {
        //inserting 300 data in a batch
        //3 columns in the table
        let parameters = [], bigqery = "", count = 0;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    let subArrayCount = 0;
                    if (data.length > 0) {
                        subArrayCount = (data.length) / 300;
                        if ((subArrayCount % 1) !== 0) {
                            subArrayCount = parseInt(subArrayCount.toString().split(".")[0]) + 1;
                        }
                    }
                    for (let i = 0; i < subArrayCount; i++) {
                        let dataToProcess = [];
                        if (data.length > 300) {
                            dataToProcess = data.splice(0, 300)
                        } else {
                            dataToProcess = data
                        }
                        Utility.log('data to process', dataToProcess.length)
                        dataToProcess.forEach((item) => {
                            bigqery += "(?,?,?),";
                            parameters.push(item[DBConstants.CITY], item[DBConstants.CITY_ID].toString(), item[DBConstants.STATE_ID].toString());
                            if (parameters.length === ((dataToProcess.length) * 3)) {
                                bigqery = bigqery.slice(0, -1);
                                tx.executeSql('INSERT INTO ' + DBConstants.CITY_TABLE + ' (' + DBConstants.CITY + ', ' + DBConstants.CITY_ID + ', ' + DBConstants.STATE_ID + ')' + ' VALUES ' + bigqery + ";", parameters).then(([tx, results]) => {
                                    count = count + dataToProcess.length;
                                });
                                parameters = [];
                                bigqery = '';
                            }
                        });
                    }
                }).then((result) => {
                    Utility.log('City Inserted', count);

                    resolve(count);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getCityCount() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT COUNT(*) as count FROM ' + DBConstants.CITY_TABLE + ';', []).then(([tx, results]) => {
                        let count = 0;
                        if (results.rows.length > 0) {
                            count = results.rows.item(0).count
                        }

                        resolve(count);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getCities(searchText) {
        return new Promise((resolve) => {
            const cities = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM ' + DBConstants.CITY_TABLE, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            cities.push(row);
                        }

                        Utility.log(cities);
                        resolve(cities);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getCityById(id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.CITY_TABLE + " WHERE " + DBConstants.CITY_ID + " = " + id, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMMVByVersionId(id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.VERSION_ID + " = '" + id + "'", []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMMVByModelId(id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MODEL_ID + " = " + id, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMMVByParentModelId(id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.MMV_TABLE + " WHERE " + DBConstants.MODEL_PARENT_ID + " = " + id, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertCountryAndLanguage(data) {
        let countryCount = 0, langCount = 0;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    data.forEach((country) => {
                        let query = "(?,?,?,?,?,?)";
                        tx.executeSql('INSERT INTO ' + DBConstants.COUNTRY_TABLE + ' (' + DBConstants.COUNTRY_ID + ', ' + DBConstants.COUNTRY_CODE + ', ' + DBConstants.COUNTRY + ', ' + DBConstants.COUNTRY_FLAG + ', ' + DBConstants.COUNTRY_CURRENCY + ', ' + DBConstants.COUNTRY_BASE_URL + ')' + ' VALUES ' + query + ";", [country[DBConstants.COUNTRY_ID], country[DBConstants.COUNTRY_CODE], country[DBConstants.COUNTRY], country[DBConstants.COUNTRY_FLAG], country[DBConstants.COUNTRY_CURRENCY], country[DBConstants.COUNTRY_BASE_URL]]).then(([tx, results]) => {
                            countryCount = countryCount++;
                        });
                        if (country.language && country.language.length > 0) {
                            country.language.forEach((language) => {
                                let query = "(?,?,?,?,?,?,?)";
                                tx.executeSql('INSERT INTO ' + DBConstants.LANGUAGE_TABLE + ' (' + DBConstants.UNIQUE_ID + ', ' + DBConstants.LANGUAGE_ID + ', ' + DBConstants.LANGUAGE_CODE + ', ' + DBConstants.LANGUAGE + ', ' + DBConstants.DATE_FORMAT + ', ' + DBConstants.DATE_FORMAT_FULL + ', ' + DBConstants.COUNTRY_ID + ')' + ' VALUES ' + query + ";", [(country[DBConstants.COUNTRY_ID] + '_' + language[DBConstants.LANGUAGE_ID]), language[DBConstants.LANGUAGE_ID], language[DBConstants.LANGUAGE_CODE], language[DBConstants.LANGUAGE], language[DBConstants.DATE_FORMAT], language[DBConstants.DATE_FORMAT_FULL], country[DBConstants.COUNTRY_ID]]).then(([tx, results]) => {
                                    langCount = langCount++;
                                });
                            })
                        }
                    });
                }).then((result) => {
                    Utility.log('Country Inserted', countryCount);
                    Utility.log('Language Inserted', langCount);

                    resolve(countryCount, langCount);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getLanguagesByCountryId(countryId) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.LANGUAGE_TABLE + " WHERE " + DBConstants.COUNTRY_ID + " = '" + countryId + "'", []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getCountries() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.COUNTRY_TABLE, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getCountriesCount() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT COUNT(*) as count FROM ' + DBConstants.COUNTRY_TABLE + ';', []).then(([tx, results]) => {
                        let count = 0;
                        if (results.rows.length > 0) {
                            count = results.rows.item(0).count
                        }

                        resolve(count);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    addImage(car_id, make_model, image_name, image_path, status, total_images, sequence) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO ' + DBConstants.IMAGE_TABLE + ' VALUES (?, ?, ?, ?, ?, ?, ?,?)', [new Date().getTime() + '', car_id, make_model, image_name, image_path, status, total_images, sequence]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }
    getImagesByCarId(id, status) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.IMAGE_TABLE + " where " + DBConstants.CAR_ID + " = '" + id + "' AND " + DBConstants.STATUS + " = " + status, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }


    updateImage(id, image_name, image_url, status) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('UPDATE ' + DBConstants.IMAGE_TABLE + ' SET ' + DBConstants.STATUS + ' = ? ,' + DBConstants.IMAGE_NAME + ' = ? ,' + DBConstants.IMAGE_PATH + ' = ? WHERE ' + DBConstants.IMAGE_ID + ' = ?', [status, image_name, image_url, id]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    uploadImagesSuccess(carId, status) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('UPDATE ' + DBConstants.IMAGE_TABLE + ' SET ' + DBConstants.STATUS + ' = ?  WHERE ' + DBConstants.CAR_ID + ' = ?', [status, carId]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    deleteImagesByCarId(carId) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("DELETE FROM " + DBConstants.IMAGE_TABLE + " WHERE " + DBConstants.CAR_ID + " = '"+carId+"'", []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getDistintCarIds() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT DISTINCT " + DBConstants.CAR_ID + " FROM " + DBConstants.IMAGE_TABLE , []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    /* document image table functions */
    addDocumentImage(document_parent_id,document_id, lead_id, image_name, image_path, status,dealer_id_hash,is_calculator_image) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' VALUES (?, ?, ?, ?, ?, ?,?,?,?)', [new Date().getTime() + '', lead_id, document_id, image_name, image_path, status,dealer_id_hash,document_parent_id,is_calculator_image]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }
    // addDocumentImage(document_id, lead_id, image_name, image_path, status,dealer_id_hash) {
    //     return new Promise((resolve) => {
    //         this.getDbHelper().initDB().then((db) => {
    //             db.transaction((tx) => {
    //                 tx.executeSql('INSERT INTO ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' VALUES (?, ?, ?, ?, ?, ?)', [new Date().getTime() + '', lead_id, document_id, image_name, image_path, status]).then(([tx, results]) => {

    //                     resolve(results);
    //                     //this.closeDatabase(db);
    //                 });
    //             }).then((result) => {
    //                 this.closeDatabase(db);
    //             }).catch((err) => {
    //                 Utility.log(err);
    //             });
    //         }).catch((err) => {
    //             Utility.log(err);
    //         });
    //     });
    // }
    getImagesByLeadId(id, status) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.DOCUMENT_IMAGE_TABLE + " where " + DBConstants.LEAD_ID + " = '" + id + "' AND " + DBConstants.STATUS + " = " + status, []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getAllImagesByLeadId(id,is_calculator_image) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT * FROM " + DBConstants.DOCUMENT_IMAGE_TABLE + " where " + DBConstants.LEAD_ID + " = '" + id + "' AND " + DBConstants.IS_CALCULATOR_IMAGE + " = '"+ is_calculator_image + "'", []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }


    updateDocumentImage(id, image_name, image_url, status) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('UPDATE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' SET ' + DBConstants.STATUS + ' = ? ,' + DBConstants.IMAGE_NAME + ' = ? ,' + DBConstants.IMAGE_PATH + ' = ? WHERE ' + DBConstants.IMAGE_ID + ' = ?', [status, image_name, image_url, id]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    uploadDocumentImagesSuccess(lead_id, status) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('UPDATE ' + DBConstants.DOCUMENT_IMAGE_TABLE + ' SET ' + DBConstants.STATUS + ' = ?  WHERE ' + DBConstants.LEAD_ID + ' = ?', [status, lead_id]).then(([tx, results]) => {

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    deleteDocumentImagesByLeadId(lead_id) {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("DELETE FROM " + DBConstants.DOCUMENT_IMAGE_TABLE + " WHERE " + DBConstants.LEAD_ID + " = '"+lead_id+"'", []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getDistintLeadIds() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql("SELECT DISTINCT " + DBConstants.LEAD_ID + " FROM " + DBConstants.DOCUMENT_IMAGE_TABLE , []).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }
    /* document image table functions */

    /* Financier MMV Table Functions  */

    deleteFinancierMMV() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM ' + DBConstants.FINANCIER_MMV_TABLE, []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertFinancierMMV(data) {
        //inserting 80 data in a batch
        //12 columns in the table
        let parameters = [], bigqery = "", count = 0 , noOfRecords = 90, noOfColumns = 11;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    let subArrayCount = 0;
                    if (data.length > 0) {
                        subArrayCount = (data.length) / noOfRecords;
                        if ((subArrayCount % 1) !== 0) {
                            subArrayCount = parseInt(subArrayCount.toString().split(".")[0]) + 1;
                        }
                    }
                    for (let i = 0; i < subArrayCount; i++) {
                        let dataToProcess = [];
                        if (data.length > noOfRecords) {
                            dataToProcess = data.splice(0, noOfRecords)
                        } else {
                            dataToProcess = data
                        }
                        Utility.log('data to process', dataToProcess.length);
                        dataToProcess.forEach((item) => {
                            bigqery += "(?,?,?,?,?,?,?,?,?,?,?),";
                            parameters.push(item[DBConstants.FINANCIER_VERSION_ID], item[DBConstants.FINANCIER_MAKE_ID], item[DBConstants.FINANCIER_MODEL_ID], item[DBConstants.FINANCIER_VERSION_NAME], item[DBConstants.FINANCIER_MAKE_NAME], item[DBConstants.FINANCIER_BRAND_TYPE], item[DBConstants.FINANCIER_MODEL_NAME], (item[DBConstants.FINANCIER_MAKE_NAME] + ' ' + item[DBConstants.FINANCIER_MODEL_NAME]), item[DBConstants.FINANCIER_VEHICLE_TYPE_ID], item[DBConstants.FINANCIER_AREA_ID], item[DBConstants.FINANCIER_ID]);
                            if (parameters.length === ((dataToProcess.length) * noOfColumns)) {
                                bigqery = bigqery.slice(0, -1);
                                tx.executeSql('INSERT INTO ' + DBConstants.FINANCIER_MMV_TABLE + ' (' + DBConstants.FINANCIER_VERSION_ID + ', ' + DBConstants.FINANCIER_MAKE_ID + ', ' + DBConstants.FINANCIER_MODEL_ID + ', ' + DBConstants.FINANCIER_VERSION_NAME + ', ' + DBConstants.FINANCIER_MAKE_NAME + ', ' + DBConstants.FINANCIER_BRAND_TYPE + ', ' + DBConstants.FINANCIER_MODEL_NAME + ', ' + DBConstants.FINANCIER_MAKE_MODEL_NAME + ', ' + DBConstants.FINANCIER_VEHICLE_TYPE_ID + ', ' + DBConstants.FINANCIER_AREA_ID + ', ' + DBConstants.FINANCIER_ID + ')' + ' VALUES ' + bigqery + ";", parameters).then(([tx, results]) => {
                                    count = count + dataToProcess.length;
                                });
                                parameters = [];
                                bigqery = '';
                            }
                        });
                    }
                }).then((result) => {
                    Utility.log('Financier MMV Inserted', count);

                    resolve(count);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getAllFinancierMMV() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM ' + DBConstants.FINANCIER_MMV_TABLE , []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }
    getFinancierMMV(financier_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM ' + DBConstants.FINANCIER_MMV_TABLE + ' WHERE '+ DBConstants.FINANCIER_ID + ' = '+financier_id, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getFinancierDistinctMake(financier_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT DISTINCT '+ DBConstants.FINANCIER_MAKE_ID+','+DBConstants.FINANCIER_MAKE_NAME+' FROM ' + DBConstants.FINANCIER_MMV_TABLE + ' WHERE '+ DBConstants.FINANCIER_ID + ' = '+financier_id, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getFinancierDistinctModels(make_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT DISTINCT '+ DBConstants.FINANCIER_MODEL_ID+','+DBConstants.FINANCIER_MODEL_NAME+' FROM ' + DBConstants.FINANCIER_MMV_TABLE + ' WHERE '+ DBConstants.FINANCIER_MAKE_ID + ' = '+make_id, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getFinancierDistinctVersion(make_id,model_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT DISTINCT '+ DBConstants.FINANCIER_VERSION_ID+','+DBConstants.FINANCIER_VERSION_NAME + ',' + DBConstants.FINANCIER_VEHICLE_TYPE_ID +' FROM ' + DBConstants.FINANCIER_MMV_TABLE + ' WHERE '+ DBConstants.FINANCIER_MAKE_ID + ' = '+make_id + ' AND '+ DBConstants.FINANCIER_MODEL_ID + ' = '+model_id, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getFinancierMMVTotalCount() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT COUNT(*) as count FROM ' + DBConstants.FINANCIER_MMV_TABLE + ';', []).then(([tx, results]) => {
                        let count = 0;
                        if (results.rows.length > 0) {
                            count = results.rows.item(0).count
                        }

                        resolve(count);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    /* Financier MMV Table Functions  */

    /* Make Year Price Table Functions  */

    deleteMYP() {
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM ' + DBConstants.MYP_TABLE, []).then(([tx, results]) => {
                        Utility.log(results);

                        resolve(results);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    insertMYP(data) {
        //inserting 80 data in a batch
        //12 columns in the table
        let parameters = [], bigqery = "", count = 0 , noOfRecords = 180, noOfColumns = 5;
        return new Promise((resolve) => {
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    let subArrayCount = 0;
                    if (data.length > 0) {
                        subArrayCount = (data.length) / noOfRecords;
                        if ((subArrayCount % 1) !== 0) {
                            subArrayCount = parseInt(subArrayCount.toString().split(".")[0]) + 1;
                        }
                    }
                    for (let i = 0; i < subArrayCount; i++) {
                        let dataToProcess = [];
                        if (data.length > noOfRecords) {
                            dataToProcess = data.splice(0, noOfRecords)
                        } else {
                            dataToProcess = data
                        }
                        Utility.log('data to process', dataToProcess.length);
                        dataToProcess.forEach((item) => {
                            bigqery += "(?,?,?,?,?),";
                            parameters.push(item[DBConstants.MYP_ID], item[DBConstants.MYP_VERSION_ID], item[DBConstants.MYP_MAKE_YEAR], item[DBConstants.MYP_PRICE].toString(), item[DBConstants.MYP_AIREA_ID]);
                            if (parameters.length === ((dataToProcess.length) * noOfColumns)) {
                                bigqery = bigqery.slice(0, -1);
                                tx.executeSql('INSERT INTO ' + DBConstants.MYP_TABLE + ' (' + DBConstants.MYP_ID + ', ' + DBConstants.MYP_VERSION_ID + ', ' + DBConstants.MYP_MAKE_YEAR + ', ' + DBConstants.MYP_PRICE + ', ' + DBConstants.MYP_AIREA_ID +  ')' + ' VALUES ' + bigqery + ";", parameters).then(([tx, results]) => {
                                    count = count + dataToProcess.length;
                                });
                                parameters = [];
                                bigqery = '';
                            }
                        });
                    }
                }).then((result) => {
                    Utility.log('MYP Inserted', count);

                    resolve(count);
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getMYPByVersion(version_id) {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT '+DBConstants.MYP_MAKE_YEAR+','+ DBConstants.MYP_PRICE+','+ DBConstants.MYP_AIREA_ID+' FROM ' + DBConstants.MYP_TABLE + ' WHERE '+ DBConstants.MYP_VERSION_ID + ' = '+version_id, []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

    getAllMYP() {
        return new Promise((resolve) => {
            const products = [];
            this.getDbHelper().initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM ' + DBConstants.MYP_TABLE , []).then(([tx, results]) => {
                        Utility.log("Query completed");
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            products.push(row);
                        }
                        Utility.log(products);

                        resolve(products);
                        //this.closeDatabase(db);
                    });
                }).then((result) => {
                    this.closeDatabase(db);
                }).catch((err) => {
                    Utility.log(err);
                });
            }).catch((err) => {
                Utility.log(err);
            });
        });
    }

/* Make Year Price Table Functions  */
    
}
