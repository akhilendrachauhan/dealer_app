import * as Colors from "./Colors";
import Strings from "./Strings";
import * as Styles from "./Styles";
import * as Dimens from "./Dimens";

export { Colors, Strings, Styles, Dimens }
