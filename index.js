/**
 * @format
 */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {
    SplashContainer,
    LoginContainer,
    DashboardContainer,
    NotificationContainer,
    AddEditStockContainer,
    StockImagesContainer,
    ForgotPasswordContainer,
    CameraContainer,
    BuyerLeadContainer,
    BuyerLeadListContainer,
    StockListingContainer,
    GalleryFolderList,
    GalleryFilesList,
    StockDetailContainer,
    LeadDetailContainer,
    BuyerLeadFinderContainer,
    AddBuyerLeadContainer,
    FilterContainer,
    EditLeadContainer,
    EditRequirementContainer,
    MakeModelSearchContainer,
    LeadHistoryAll,
    SearchContainer,
    StockListContainer,
    CarDetailContainer,
    ImageReviewScreen,
    ProfileContainer,
    LoanStockListingContainer,
    FinancierListContainer,
    RuleEngineInputContainer,
    ApprovedQuoteContainer,
    CustomerDetailContainer,
    AttachmentsContainer,
    DocumentImagesContainer,
    LoanDashboardContainer,
    LoanLeadContainer,
    LoanLeadListContainer,
    LoanFilterContainer,
    CongratulationsScreen,
    LoanPendingLeadContainer,
    PDFViewScreen,
    SignUpScreen,
    CalculatorFormContainer,
    PaymentDetailContainer,
    LoanLeadHistoryContainer,
    ContactListScreen,
    PackDetailContainer
} from './src/screens'
import { LeftDrawer } from './src/screens/Drawer/LeftDrawer'
//import { LeadHistoryAll } from './src/granulars/BuyerLeads/LeadsHistoryAll'

const stackNavigator = createStackNavigator({
    //attachments: { screen: AttachmentsContainer, navigationOptions: { headerShown: false } },
    splash: { screen: SplashContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    addEditStock: { screen: AddEditStockContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    camera: { screen: CameraContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    stockImages: { screen: StockImagesContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    login: { screen: LoginContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    forgotPassword: { screen: ForgotPasswordContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    buyerLead: { screen: BuyerLeadContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    buyerLeadList: { screen: BuyerLeadListContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    leadDetail: { screen: LeadDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    leadHistory: { screen: LeadHistoryAll, navigationOptions: { headerShown: false ,animationEnabled : false} },
    stockList: { screen: StockListContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    carDetail: { screen: CarDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    profile: { screen: ProfileContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    //dashboard: { screen: DashboardContainer, navigationOptions: { headerShown: false } },
    drawerScreen: {
        screen: LeftDrawer,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,animationEnabled : false
        })
    },
    stockListing: { screen: StockListingContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    galleryFolder: { screen: GalleryFolderList, navigationOptions: { headerShown: false,animationEnabled : false } },
    galleryFiles: { screen: GalleryFilesList, navigationOptions: { headerShown: false,animationEnabled : false } },
    stockDetail: { screen: StockDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    buyerLeadFinder: { screen: BuyerLeadFinderContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    addBuyerLead: { screen: AddBuyerLeadContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    filter: { screen: FilterContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    editLead: { screen: EditLeadContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    editRequirement: { screen: EditRequirementContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    makeModelSearch: { screen: MakeModelSearchContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    searchContainer: { screen: SearchContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    imageReviewScreen: { screen: ImageReviewScreen, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanStockListing: { screen: LoanStockListingContainer, navigationOptions: { headerShown: false ,animationEnabled : false} },
    financierList: { screen: FinancierListContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    ruleEngineInput: { screen: RuleEngineInputContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    approvedQuote: { screen: ApprovedQuoteContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    customerDetail: { screen: CustomerDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    
    contactList: { screen: ContactListScreen, navigationOptions: { headerShown: false,animationEnabled : false } },
    
    attachments: { screen: AttachmentsContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    documentImages: { screen: DocumentImagesContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanDashboard: { screen: LoanDashboardContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanLead: { screen: LoanLeadContainer, navigationOptions: { headerShown: false ,animationEnabled : false} },
    loanLeadList: { screen: LoanLeadListContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanFilter: { screen: LoanFilterContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    congratulations: { screen: CongratulationsScreen, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanPendingLead: { screen: LoanPendingLeadContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    pdfViewScreen: { screen: PDFViewScreen, navigationOptions: { headerShown: false,animationEnabled : false } },
    signUpScreen: { screen: SignUpScreen, navigationOptions: { headerShown: false,animationEnabled : false } },
    calculatorForm: { screen: CalculatorFormContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    paymentDetail: { screen: PaymentDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    loanLeadHistory: { screen: LoanLeadHistoryContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
    notificationListing: { screen: NotificationContainer, navigationOptions: { headerShown: false ,animationEnabled : false} },
    packDetail: { screen: PackDetailContainer, navigationOptions: { headerShown: false,animationEnabled : false } },
});


const appContainer = createAppContainer(stackNavigator);
AppRegistry.registerComponent(appName, () => appContainer);
//AppRegistry.registerComponent(appName, () => TradeInExample);
